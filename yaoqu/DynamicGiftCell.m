//
//  DynamicGiftCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/29.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicGiftCell.h"
#import "ImageCollectionViewCell.h"
#import "UserHomeDynamicCell.h"

@interface DynamicGiftCell()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UIImageView *giftImage;
@property(nonatomic,strong) UILabel *giftLbl;
@property(nonatomic,strong) UIImageView *accessImage;
@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) NSArray *datas;
@property(nonatomic,strong) UIView *tipView;


@end

@implementation DynamicGiftCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self commonInit];
        [self rac_prepareForReuseSignal];
        [RACObserve(self , model) subscribeNext:^(DynamicModel *x) {
            self.giftLbl.text = [NSString stringWithFormat:@"收到礼物(%ld)",[x.gift_qty integerValue]];
            self.datas = [x.gift_data JSONObject];

            self.tipView.hidden = [self.datas count] > 0;
            self.collectionView.hidden = self.datas.count == 0;

            [self.collectionView reloadData];
        }];
    }
    return self;
}

-(void)commonInit{
    
    
    [self.giftImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.equalTo(self.contentView).offset(10);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    [self.giftLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.left.equalTo(self.giftImage.mas_right).offset(10);
        make.centerY.equalTo(self.giftImage.mas_centerY);
    }];
    
    
    
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left);
        make.top.equalTo(self.giftImage.mas_bottom).offset(10);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-10);
        make.right.equalTo(self.contentView.mas_right);
    }];
    

    [self.tipView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left);
        make.top.equalTo(self.giftImage.mas_bottom).offset(35);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-35);
        make.right.equalTo(self.contentView.mas_right);
    }];
    
    [self.accessImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.top.equalTo(self.giftImage.mas_top).offset(-5);
        make.bottom.equalTo(self.giftImage.mas_bottom).offset(5);
        make.width.mas_equalTo(RightAccessWidth);
    }];

}

#pragma mark -
#pragma mark lazy layout

-(UIView *)tipView{
    if (!_tipView) {
        _tipView = [[UIView alloc]init];
        

        UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_dynamic_detail_gift"]];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [_tipView addSubview:imageView];
        
        UILabel *label = [ControllerHelper autoFitLabel];
        label.textColor = AppBlack;
        label.textAlignment = NSTextAlignmentCenter;
        label.font = SystemFont(14);
        label.text = @"暂时还没有收到礼物哦";
        
        [_tipView addSubview:label];
        

        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_tipView.mas_left);
            make.right.equalTo(_tipView.mas_right);
            make.bottom.equalTo(_tipView.mas_bottom).offset(-10);

        }];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_tipView.mas_left);
            make.right.equalTo(_tipView.mas_right);
            make.top.equalTo(_tipView.mas_top);
            make.bottom.equalTo(label.mas_top).offset(-5);
        }];
        
        [self.contentView addSubview:_tipView];

        
    }
    return _tipView;
}

-(UIImageView *)giftImage{
    if (!_giftImage) {
        _giftImage = [[UIImageView alloc]init];
        _giftImage.image = [UIImage imageNamed:@"ic_gift"];
        [self.contentView addSubview:_giftImage];
    }
    return _giftImage;
}

-(UILabel *)giftLbl{
    if (!_giftLbl) {
        _giftLbl = [[UILabel alloc]init];
        _giftLbl.text = @"送礼";
        [_giftLbl setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical | UILayoutConstraintAxisHorizontal];
        [self.contentView addSubview:_giftLbl];
    }
    return _giftLbl;
}

-(UIImageView *)accessImage{
    if (!_accessImage) {
        _accessImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_forward"]];
        _accessImage.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_accessImage];
    }
    return _accessImage;
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        UserHomeDynamicCellLayout *layout = [[UserHomeDynamicCellLayout alloc]init];
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[ImageCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
        _collectionView.backgroundColor = [UIColor clearColor];
        [self addSubview:_collectionView];
    }
    return _collectionView;
}


#pragma mark -
#pragma mark collection view

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return MIN(self.datas.count, 4);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dic = self.datas[indexPath.item];
    [cell.imageView setImageName:[dic objectForKey:@"gift_image"] placeholder:[UIImage imageNamed:@"ic_gift_icon"]];
    
    return cell;
}



@end
