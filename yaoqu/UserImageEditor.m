//
//  UserImageEditor.m
//  yaoqu
//
//  Created by ridchan on 2017/6/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserImageEditor.h"
#define MaxImageCount 8
#import "SRActionSheet.h"
#import "DynamicUploadManager.h"
#import "RouteController.h"
#import "LLUtils.h"
@implementation UserImageEditorCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}



-(void)commonInit{
    self.backgroundColor = [UIColor clearColor];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

-(UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc]init];
        _imageView.layer.masksToBounds = YES;
        _imageView.layer.cornerRadius = 3;
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:_imageView];
    }
    return _imageView;
}

@end

#pragma mark -
#pragma mark editor -

@interface UserImageEditor ()<UICollectionViewDelegate,UICollectionViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic, strong) UILongPressGestureRecognizer *longPress;

@end


@implementation UserImageEditor

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.dataCount = @"0";
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    self.backgroundColor = [UIColor clearColor];
    
    self.datas = [NSMutableArray array];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

-(void)setDatas:(NSMutableArray *)datas{
    _datas = datas;
    self.dataCount = [NSString intValue:[_datas count]];
    [self.collectionView reloadData];
    [self resetFrame];
}

#pragma mark -
#pragma mark collection view

-(UICollectionViewFlowLayout *)layout{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.minimumLineSpacing = 5;
    layout.minimumInteritemSpacing = 5;
    layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
    CGFloat width = (APP_WIDTH - 5.0 * 5.0 ) / 4.0 ;
    layout.itemSize = CGSizeMake(width, width);
    return layout;
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:[self layout]];
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.scrollEnabled = NO;
        [_collectionView registerClass:[UserImageEditorCell class] forCellWithReuseIdentifier:@"Cell"];
        [self addSubview:_collectionView];
        
        _longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(lonePressMoving:)];
        [_collectionView addGestureRecognizer:_longPress];
    }
    
    return _collectionView;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _hideAction ? self.datas.count : MIN(self.datas.count + 1 , MaxImageCount);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UserImageEditorCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (_hideAction) {
        [cell.imageView setImageName:self.datas[indexPath.item] placeholder:nil];
    }else{
        if (indexPath.item  == self.datas.count){
            cell.imageView.image = [UIImage imageNamed:@"ic_user_info_add"];
        }else{
            [cell.imageView setImageName:self.datas[indexPath.item] placeholder:nil];
        }
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (_hideAction) return;
    BOOL isDelete = (self.datas.count == MaxImageCount);
    
    if (!isDelete){
        isDelete = indexPath.item < self.datas.count;
    }
    
    
    WEAK_SELF;
    if (isDelete) {
        [[SRActionSheet sr_actionSheetViewWithTitle:nil cancelTitle:LS(@"取消") destructiveTitle:nil otherTitles:@[LS(@"删除")] otherImages:nil selectSheetBlock:^(SRActionSheet *actionSheet, NSInteger index) {
            if (index == 0){
                if (weakSelf.datas.count ==  MaxImageCount){
                    [weakSelf.datas removeObjectAtIndex:indexPath.item];
                    [weakSelf.collectionView reloadData];
                }else{
                    [weakSelf.datas removeObjectAtIndex:indexPath.item];
                    [weakSelf.collectionView deleteItemsAtIndexPaths:@[indexPath]];
                }
                
                [weakSelf resetFrame];
            }
        }] show];
    }else{
        
        [[SRActionSheet sr_actionSheetViewWithTitle:nil cancelTitle:LS(@"取消") destructiveTitle:nil otherTitles:@[LS(@"相机"),LS(@"相册")] otherImages:nil selectSheetBlock:^(SRActionSheet *actionSheet, NSInteger index) {
            if (index < 0) return ;
            UIImagePickerController *picker = [[UIImagePickerController alloc]init];
            picker.delegate = weakSelf;
            picker.allowsEditing = YES;
            picker.modalPresentationStyle= UIModalPresentationOverFullScreen;
//            picker.modalPresentationCapturesStatusBarAppearance = YES;
            picker.sourceType =  index == 0 ? UIImagePickerControllerSourceTypeCamera :UIImagePickerControllerSourceTypePhotoLibrary;
            [[RouteController sharedManager] presentVC:picker];
            
        }] show];
    }
}

-(BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath{
    if (_hideAction) return NO;
    if (self.datas.count < 8) {
        return indexPath.item != self.datas.count ;
    }else{
        return YES;
    }
}



-(void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    NSLog(@"交换");
    [self.datas exchangeObjectAtIndex:sourceIndexPath.item withObjectAtIndex:destinationIndexPath.item];
    
}



#pragma mark -
#pragma mark image picker

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];

    WEAK_SELF;
    [[DynamicUploadManager sharedManager] putImage:image block:^(id responseObj) {
        if (responseObj){
            [weakSelf.datas addObject:responseObj];
            [weakSelf.collectionView reloadData];
            [weakSelf resetFrame];
        }else{
            [LLUtils showCenterTextHUD:@"上传图片失败"];
        }
    }];
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)resetFrame{
    if (!_hideAction){
        if ([self.datas count] > 3)
            [self setFrameSizeHeight:APP_WIDTH / 2.0];
        else
            [self setFrameSizeHeight:APP_WIDTH / 4.0];
    }else{
        if ([self.datas count] > 4)
            [self setFrameSizeHeight:APP_WIDTH / 2.0];
        else if ([self.datas count] > 0)
            [self setFrameSizeHeight:APP_WIDTH / 4.0];
        else{
            [self setFrameSizeHeight:0.0];
        }
    }
    
    self.dataCount = [NSString intValue:self.datas.count];
        
}


#pragma mark -
#pragma mark long press





-(CABasicAnimation *)scaleTo:(CGFloat)scale{
    CABasicAnimation* shake = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    
    //设置抖动幅度
    //    shake.fromValue = [NSNumber numberWithFloat:-0.05];
    
    shake.toValue = [NSNumber numberWithFloat:scale];
    
    shake.duration = 0.3;
    
    shake.fillMode = kCAFillModeForwards;
    
    shake.removedOnCompletion = NO;
    //    shake.autoreverses = YES; //是否重复
    
    
    
    return shake;
}



- (void)lonePressMoving:(UILongPressGestureRecognizer *)longPress
{
    
    if (_hideAction) return;
    
    switch (_longPress.state) {
        case UIGestureRecognizerStateBegan: {
            {
            
          
                NSIndexPath *selectIndexPath = [self.collectionView indexPathForItemAtPoint:[_longPress locationInView:self.collectionView]];
                // 找到当前的cell
                UserImageEditorCell *cell = (UserImageEditorCell *)[self.collectionView cellForItemAtIndexPath:selectIndexPath];
                
                [cell.layer addAnimation:[self scaleTo:1.2] forKey:@"Scale"];
                [self.collectionView bringSubviewToFront:cell];
                [_collectionView beginInteractiveMovementForItemAtIndexPath:selectIndexPath];
            }
            break;
        }
        case UIGestureRecognizerStateChanged: {
            [self.collectionView updateInteractiveMovementTargetPosition:[longPress locationInView:_longPress.view]];
            break;
        }
        case UIGestureRecognizerStateEnded: {
            
            NSIndexPath *selectIndexPath = [self.collectionView indexPathForItemAtPoint:[_longPress locationInView:self.collectionView]];
            // 找到当前的cell
            UserImageEditorCell *cell = (UserImageEditorCell *)[self.collectionView cellForItemAtIndexPath:selectIndexPath];
            [cell.layer addAnimation:[self scaleTo:1.0] forKey:@"Scale"];
            
            
            [self.collectionView endInteractiveMovement];
            [self.collectionView reloadData];

            break;
        }
        default: [self.collectionView cancelInteractiveMovement];
            break;
    }
}

//-(void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
//    [self.datas exchangeObjectAtIndex:sourceIndexPath.item withObjectAtIndex:destinationIndexPath.item];
//}
//
//-(BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath{
//    return (indexPath.item == self.datas.count - 1);
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
