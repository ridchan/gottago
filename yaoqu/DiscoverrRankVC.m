//
//  DiscoverrRankVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DiscoverrRankVC.h"
#import "DiscoverRankTitleView.h"
#import "DiscoverRankSubVC.h"
@interface DiscoverrRankVC ()<UIScrollViewDelegate>

@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) DiscoverRankTitleView *titleView;

@property(nonatomic,strong) NSMutableArray *viewControllers;

@end

@implementation DiscoverrRankVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    
    self.titleView = [[DiscoverRankTitleView alloc]init];
    self.titleView.frame = CGRectMake(0, 0, APP_WIDTH, 44);
    self.titleView.titles = @[@"周赞榜",@"人气榜",@"收礼榜",@"土豪榜"];
    
    WEAK_SELF;
    self.titleView.selectBlock = ^(id responseObj) {
        CGPoint point = weakSelf.scrollView.contentOffset;
//        [weakSelf.scrollView scrollRectToVisible:CGRectMake(APP_WIDTH * [responseObj integerValue], point.y, APP_WIDTH, APP_HEIGHT - 108) animated:YES];
        weakSelf.scrollView.contentOffset = CGPointMake(APP_WIDTH * [responseObj integerValue],point.y);

    };
    [self.view addSubview:self.titleView];

    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.titleView.mas_bottom);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    NSArray *types = @[@(DiscoverRankType_Like),@(DiscoverRankType_Follow),@(DiscoverRankType_Gift),@(DiscoverRankType_Currency)];
    
    CGFloat offset = 0;
    for (id type in types){
        DiscoverRankSubVC *vc = [[DiscoverRankSubVC alloc]init];
        vc.rankType = [type integerValue];
        [self addChildViewController:vc];
        
        [self.scrollView addSubview:vc.view];
        [vc.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.scrollView.mas_left).offset(offset);
            make.top.equalTo(self.scrollView.mas_top);
            make.size.mas_equalTo(CGSizeMake(APP_WIDTH, APP_HEIGHT - 108 - 50));
        }];
        offset += APP_WIDTH;
    };

    
    // Do any additional setup after loading the view.
}

-(UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]init];
        _scrollView.pagingEnabled = YES;
        _scrollView.contentSize = CGSizeMake(APP_WIDTH * 4, 1);
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.bounces = NO;
        _scrollView.delegate = self;
        [self.view addSubview:_scrollView];
    }
    
    return _scrollView;
}



-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    self.titleView.contentOffset = scrollView.contentOffset.x / scrollView.frame.size.width;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
