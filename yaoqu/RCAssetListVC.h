//
//  RCAssetListVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/26.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseViewController.h"
#import "LLAssetModel.h"
#import "LLAssetsGroupModel.h"

@interface RCAssetListVC : BaseViewController

NS_ASSUME_NONNULL_BEGIN

@property (nullable, nonatomic,strong) NSMutableArray<LLAssetModel *> *selectAssets;
@property (nonatomic) NSMutableArray<LLAssetModel*> *allSelectdAssets;
@property (nonatomic) LLAssetsGroupModel *groupModel;


@property (nonatomic) RCImagePickerType pickerType;

- (void)fetchData;

NS_ASSUME_NONNULL_END

@end
