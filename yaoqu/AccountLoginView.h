//
//  AccountLoginView.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/8.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginPostModel.h"

@interface AccountLoginView : UIView

@property(nonatomic,strong) UILabel *forgetBtn;
@property(nonatomic,strong) UILabel *registBtn;
@property(nonatomic,strong) LoginPostModel *postModel;

@property(nonatomic,copy) BaseBlock loginFailBlock;


@end
