//
//  LevelView.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/16.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LevelView : UIView

@property(nonatomic,strong) UIImageView *levelImg;
@property(nonatomic,strong) UILabel *levelLbl;

@property(nonatomic,strong) NSString *level;

@end
