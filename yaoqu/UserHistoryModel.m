//
//  UserHistoryModel.m
//  yaoqu
//
//  Created by ridchan on 2017/9/7.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserHistoryModel.h"

@implementation UserHistoryModel

+(BOOL) isContainParent{
    return YES;
}
/**
 *  @author wjy, 15-01-26 14:01:01
 *
 *  @brief  设定表名
 *  @return 返回表名
 */
+(NSString *)getTableName
{
    return NSStringFromClass([self class]);
}
/**
 *  @author wjy, 15-01-26 14:01:22
 *
 *  @brief  设定表的单个主键
 *  @return 返回主键表
 */
+(NSString *)getPrimaryKey
{
    return @"private_id";
}

@end
