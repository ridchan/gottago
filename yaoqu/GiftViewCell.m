//
//  GiftViewCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "GiftViewCell.h"
#import "GiftCell.h"
#import "NormalCellLayout.h"

@interface GiftViewCell()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UICollectionView *collectionView;

@end

@implementation GiftViewCell


#pragma mark -
#pragma mark collection view

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return self;
}


-(UICollectionView *)collectionView{
    if (!_collectionView) {
        NormalCellLayout *layout = [[NormalCellLayout alloc]init];
        layout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10);
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        layout.numOfColumn = 4;
        layout.numOfRow = 2;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.scrollEnabled = NO;
        [_collectionView registerClass:[GiftCell class] forCellWithReuseIdentifier:@"Cell"];
        [self addSubview:_collectionView];
    }
    return _collectionView;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.delegate respondsToSelector:@selector(giftView:didSelectObj:)]) {
        [self.delegate giftView:nil didSelectObj:self.datas[indexPath.item]];
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GiftCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.model = self.datas[indexPath.item];

    return cell;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return  self.datas.count;
}

-(void)setDatas:(NSArray *)datas{
    _datas = datas;
    [self.collectionView reloadData];
}

@end
