//
//  MineBlacklistVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/31.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineBlacklistVC.h"
#import "BlacklistApi.h"
#import "UserModel.h"
#import "UserNormalCell.h"
#import "EMSDK.h"

@interface MineBlacklistVC ()

@end

@implementation MineBlacklistVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"黑名单");
    
    [self startRefresh];
    // Do any additional setup after loading the view.
}

-(void)startRefresh{
    WEAK_SELF;
    self.page = 1;
    
    NSDictionary *dict = @{@"page":@(self.page),
                           @"size":@(self.size)
                           };
    [[[BlacklistApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [weakSelf refreshComplete:info response:[UserModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]]];
    }];
    
}

-(void)startloadMore{
    WEAK_SELF;
    NSDictionary *dict = @{@"page":@(++self.page),
                           @"size":@(self.size)
                           };
    
    [[[BlacklistApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        NSArray *array = [UserModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        [weakSelf loadMoreComplete:info response:array];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        UserModel *model = [self.datas objectAtIndex:indexPath.row];
        
        [[EMClient sharedClient].contactManager removeUserFromBlackList:model.huanXinID];
        
        NSDictionary *dict =  @{@"member_id":model.member_id};
        BlacklistApi *api = [[BlacklistApi alloc]initWitObject:dict];
        api.method = YTKRequestMethodDELETE;
        [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            [LLUtils showActionSuccessHUD:info.message];
        }];
        
        
        [self.datas removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
        
        
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserNormalCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ImageCell"];
    if (cell == nil) {
        cell = [UserNormalCell normalCellType:UserCellType_Normal identify:@"ImageCell"];
    }
    
    UserModel *model = [self.datas objectAtIndex:indexPath.row];
    
    cell.nameLbl.text = model.username;
    cell.sexView.sex = [model.sex integerValue];
    cell.sexView.age = [model.age integerValue];
    cell.sexView.content = nil;
    cell.userImage.userModel = model;
    
    
    return cell;
}/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
