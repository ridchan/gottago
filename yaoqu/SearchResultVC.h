//
//  SearchResultVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/11.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseViewController.h"


@protocol SearchResultVCDelegate <NSObject>

@optional
-(void)searchResult:(id)obj didSelectPOI:(id)poi;

@end

@interface SearchResultVC : UIViewController


@property(nonatomic) id<SearchResultVCDelegate>delegate;

@property(nonatomic,strong) CLLocation *location;
-(void)searchWithText:(NSString *)text;


@end
