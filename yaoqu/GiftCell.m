//
//  GiftCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/30.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "GiftCell.h"


@interface GiftCell()

@property(nonatomic,strong) UIImageView *giftImage;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) ImageButton *priceImage;
@property(nonatomic,strong) UILabel *priceLbl;
@property(nonatomic,strong) UILabel *countLbl;
@property(nonatomic,strong) UIView *selectBg;


@end

@implementation GiftCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
        
        [self rac_prepareForReuseSignal];
        [RACObserve(self, model) subscribeNext:^(GiftModel *x) {
            [self.giftImage setImageName:x.gift_image placeholder:nil];
            self.nameLbl.text = x.gift_name;
            self.priceLbl.text = [NSString stringWithFormat:@"%@券",x.money];
            self.countLbl.text =  x.qty ;
            self.countLbl.hidden = [x.qty integerValue] == 0 ;
            self.selectBg.hidden = ![x.isGiftSelected boolValue];
            

        }];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    
    [self.selectBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.giftImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.countLbl.mas_bottom);
        make.bottom.equalTo(self.nameLbl.mas_top);
    }];
    
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.priceLbl.mas_top).offset(-5);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(12);
    }];
    
    [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(8);
        make.bottom.equalTo(self.mas_bottom).offset(-5);
    }];
    
    [self.countLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-3);
        make.top.equalTo(self.mas_top).offset(3);
        make.size.mas_equalTo(CGSizeMake(14, 14));
    }];
    
    self.countLbl.layer.masksToBounds = YES;
    self.countLbl.layer.cornerRadius = 7;
}

-(UIView *)selectBg{
    if (!_selectBg) {
        _selectBg = [[UIView alloc]init];
        _selectBg.layer.borderWidth = 1.0;
        _selectBg.layer.borderColor = AppOrange.CGColor;
        [self addSubview:_selectBg];
    }
    return _selectBg;
}

-(UIImageView *)giftImage{
    if (!_giftImage) {
        _giftImage = [[UIImageView alloc]init];
        _giftImage.clipsToBounds = YES;
        _giftImage.contentMode = UIViewContentModeScaleAspectFit;
        _giftImage.image = [UIImage imageNamed:@"ic_gift_icon"];
        [self addSubview:_giftImage];
    }
    return _giftImage;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        _nameLbl.text = @"afda";
        _nameLbl.font = SystemFont(12);
        _nameLbl.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(ImageButton *)priceImage{
    if (!_priceImage) {
        _priceImage = [[ImageButton alloc]init];
        _priceImage.contentImage.image = [UIImage imageNamed:@"ic_travel_icon"];
        _priceImage.titleLbl.text = @"1234";
        [self addSubview:_priceImage];
    }
    
    return _priceImage;
}

-(UILabel *)countLbl{
    if (!_countLbl) {
        _countLbl = [[UILabel alloc]init];
        _countLbl.textColor = [UIColor whiteColor];
        _countLbl.textAlignment = NSTextAlignmentCenter;
        _countLbl.backgroundColor = AppOrange;
        _countLbl.text = @"1";
        _countLbl.font = SystemFont(10);
        _countLbl.minimumScaleFactor = 0.8;
        [self addSubview:_countLbl];
    }
    return _countLbl;
}

-(UILabel *)priceLbl{
    if (!_priceLbl) {
        _priceLbl = [ControllerHelper autoFitLabel];
        _priceLbl.textColor = AppGray;
        _priceLbl.font = SystemFont(10);
        _priceLbl.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_priceLbl];
    }
    return _priceLbl;
}



@end
