//
//  NormalTableViewCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/22.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NormalTableViewCell.h"

@interface NormalTableViewCell()



@property(nonatomic,strong) UIView *topLine;
@property(nonatomic,strong) UIView *bottomLine;
@property(nonatomic,strong) UIImageView *rightAccess;


@end

@implementation NormalTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self commonInit];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.textLabel.font = SystemFont(16);
    }
    return self;
}

+(instancetype)cellWithType:(NormalCellType)cellType identifier:(NSString *)identifier tableView:(UITableView *)tableView{
    NormalTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[NormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.celltype = cellType;
        cell.type = NormalCellType_None;
    }
    
    
    return cell;
}

-(void)setType:(NormalCellLineType)type{
    _type  = type;
    self.topLine.hidden = YES;
    self.bottomLine.hidden = YES;
    switch (type) {
        case NormalCellType_None:
            break;
        case NormalCellType_TopLine:
            self.topLine.hidden = NO;
            break;
        case NormalCellType_BottomLine:
            self.bottomLine.hidden = NO;
            break;
        case NormalCellType_Either:
            self.topLine.hidden = NO;
            self.bottomLine.hidden = NO;
        default:
            break;
    }
}

-(void)setCelltype:(NormalCellType)celltype{
    _celltype = celltype;
    self.rightAccess.hidden = YES;
    self.detailLbl.hidden = YES;
    self.accessoryView = nil;
    self.checkImage.hidden = YES;
    switch (celltype) {
        case NormalCellType_Access:
            self.rightAccess.hidden = NO;
            break;
        case NormalCellType_Access_Detail:
            self.rightAccess.hidden = NO;
            self.detailLbl.hidden = NO;
            break;
        case NormalCellType_Switch:
            self.accessoryView = self.switchBtn;
            break;
        case NormalCellType_Detail:
            self.detailLbl.hidden = NO;
            break;
        case NormalCellType_Checked:
            self.checkImage.hidden = NO;
            break;
        default:
            break;
    }
}

-(void)commonInit{
    [self topLine];
    [self bottomLine];
    [self rightAccess];
    [self detailLbl];
    [self switchBtn];
    [self contentImage];
    [self textLbl];
    [self checkImage];
}

-(UIView *)topLine{
    if (!_topLine) {
        _topLine = [[UIView alloc]init];
        _topLine.backgroundColor = AppLineColor;
        [self addSubview:_topLine];
        [_topLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.mas_top);
            make.left.equalTo(self.mas_left);
            make.right.equalTo(self.mas_right);
            make.height.mas_equalTo(0.5);
        }];
    }
    return _topLine;
}

-(UIView *)bottomLine{
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc]init];
        _bottomLine.backgroundColor = AppLineColor;
        [self addSubview:_bottomLine];
        [_bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.mas_bottom);
            make.left.equalTo(self.mas_left);
            make.right.equalTo(self.mas_right);
            make.height.mas_equalTo(0.5);
        }];
    }
    return _bottomLine;
}

-(UIImageView *)rightAccess{
    if (!_rightAccess) {
        _rightAccess = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_forward"]];
        _rightAccess.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_rightAccess];
        [_rightAccess mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.mas_right).offset(-10);
            make.top.equalTo(self.mas_top);
            make.bottom.equalTo(self.mas_bottom);
            make.width.mas_equalTo(RightAccessWidth);
        }];
    }
    return _rightAccess;
}

-(UILabel *)detailLbl{
    if (!_detailLbl) {
        _detailLbl = [[UILabel alloc]init];
        [_detailLbl setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        _detailLbl.font = SystemFont(14);
        _detailLbl.textColor = AppGray;
        
        [self addSubview:_detailLbl];
        
        [self.detailLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.rightAccess.mas_left).offset(-10);
            make.top.equalTo(self.mas_top);
            make.bottom.equalTo(self.mas_bottom);
        }];

    }
    return _detailLbl;
}

-(UISwitch *)switchBtn{
    if (!_switchBtn) {
        _switchBtn = [[UISwitch alloc]init];
        [_switchBtn addTarget:self action:@selector(valueChange:) forControlEvents:UIControlEventValueChanged];
    }
    return _switchBtn;
}

-(void)valueChange:(id)sender{
    self.isSwitch = [NSString boolValue:self.switchBtn.on];
}

-(void)setIsSwitch:(NSString *)isSwitch{
    _isSwitch = isSwitch;
    _switchBtn.on = [_isSwitch boolValue];
}


-(UIImageView *)contentImage{
    if (!_contentImage) {
        _contentImage = [[UIImageView alloc]init];
        _contentImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_contentImage];
        
        [_contentImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(10);
            make.top.equalTo(self.mas_top).offset(10);
            make.bottom.equalTo(self.mas_bottom).offset(-10);
            make.width.equalTo(_contentImage.mas_height);
        }];
    }
    return _contentImage;
}


-(UILabel *)textLbl{
    if (!_textLbl) {
        _textLbl = [ControllerHelper autoFitLabel];
        _textLbl.font = SystemFont(16);
        _textLbl.textColor = [UIColor blackColor];
        _textLbl.text = @"";
        [self addSubview:_textLbl];
        
        [_textLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentImage.mas_right).offset(10);
            make.centerY.equalTo(self.contentImage.mas_centerY);
        }];
    }
    return _textLbl;
}

-(void)setRightView:(UIView *)rightView{
    [_rightView removeFromSuperview];
    _rightView = rightView;
    [self addSubview:_rightView];
    [_rightView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.rightAccess.mas_left);
        make.centerY.equalTo(self.mas_centerY);
        make.height.equalTo(self.mas_height).multipliedBy(0.5);
    }];
}


-(UIImageView *)checkImage{
    if (!_checkImage) {
        _checkImage = [[UIImageView alloc]init];
        _checkImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_checkImage];
        [_checkImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.mas_right).offset(-10);
            make.top.equalTo(self.mas_top);
            make.bottom.equalTo(self.mas_bottom);
            make.width.mas_equalTo(25);
        }];
    }
    return _checkImage;
}

-(void)setBChecked:(BOOL)bChecked{
    _bChecked = bChecked;
    self.checkImage.image = !_bChecked ? [UIImage imageNamed:@"ic_not_checked"] : [UIImage imageNamed:@"ic_checked_yellow"];
}

@end
