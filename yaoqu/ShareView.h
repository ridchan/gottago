//
//  ShareView.h
//  shiyi
//
//  Created by 陳景雲 on 16/5/1.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger ,ShareType){
    ShareType_Report = 1,
    ShareType_Copy = 2,
    ShareType_Delete = 3,
    ShareType_Setting = 4,
    ShareType_Friend = 5,
    ShareType_WX = 6,
    ShareType_QQ = 7,
    ShareType_QQZone = 8,
    ShareType_Sina = 9,
    ShareType_HuanXin = 10
};

typedef NS_ENUM(NSInteger ,ActionModeType){
    ActionType_Album_Self = 0,
    ActionType_Album_Other = 1,
    ActionType_Publish_Self = 2,
    ActionType_Publish_Other = 3,
    ActionType_Memory = 4,
    ActionType_None = 5,
};




@interface ShareView : UIView

@property(nonatomic,strong) UIView *bgView;

@property(nonatomic) BOOL deleteMode;

@property(nonatomic) ActionModeType modeType;

@property(nonatomic,copy) BaseBlock handleBlock;


@property(nonatomic,strong) NSDictionary *shareInfo;

-(void)show;
-(void)dismiss:(BaseBlock)block;

@end
