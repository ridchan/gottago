//
//  TravelContactAddApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelContactAddApi.h"

@implementation TravelContactAddApi{
    TravelContactModel *_model;
}


-(id)initWithModel:(TravelContactModel *)model{
    if (self = [super init]) {
        [self.params addEntriesFromDictionary:[model keyValues]];
    }
    return self;
}


-(NSString *)requestUrl{
    return TravelContactAddUrl;
}



@end
