//
//  TravelEnrollDetailView.h
//  yaoqu
//
//  Created by ridchan on 2017/7/16.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TravelScheduleModel.h"

@interface TravelEnrollDetailView : UIView

@property(nonatomic,strong) TravelScheduleModel *scheduleModel;

@property(nonatomic,strong) NSString *payTotal;
@property(nonatomic,strong) NSString *integral;
@property(nonatomic,strong) NSString *currency;

@end
