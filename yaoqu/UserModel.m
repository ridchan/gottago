//
//  UserModel.m
//  yaoqu
//
//  Created by ridchan on 2017/6/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

//
//+ (NSDictionary *)objectClassInArray{
//    return @{ @"images" : @"NSString"
//              };
//}

-(NSString *)huanXinID{
    return [NSString stringWithFormat:@"%@0000",_member_id];
}

-(NSString *)level{
    if (!_level) {
        _level = @"1";
    }
    return _level;
}

-(NSString *)home_pro_name{
    if (!_home_pro_name) {
        _home_pro_name = @"";
    }
    return _home_pro_name;
}

-(NSString *)home_city_name{
    if (!_home_city_name) {
        _home_city_name = @"";
    }
    return _home_city_name;
}


-(NSString *)pro_name{
    if (!_pro_name) {
        _pro_name = @"";
    }
    return _pro_name;
}

-(NSString *)city_name{
    if (!_city_name) {
        _city_name = @"";
    }
    return _city_name;
}

@end


@implementation UserInfoModel

+(BOOL) isContainParent{
    return YES;
}
/**
 *  @author wjy, 15-01-26 14:01:01
 *
 *  @brief  设定表名
 *  @return 返回表名
 */
+(NSString *)getTableName
{
    return @"UserInfoModel";
}
/**
 *  @author wjy, 15-01-26 14:01:22
 *
 *  @brief  设定表的单个主键
 *  @return 返回主键表
 */
+(NSString *)getPrimaryKey
{
    return @"private_id";
}




@end
