//
//  MessageNotificationModel.h
//  yaoqu
//
//  Created by ridchan on 2017/7/23.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface MessageNotificationModel : BaseModel

@property(nonatomic,strong) NSString *message_system_id;
@property(nonatomic,strong) NSString *member_id;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *desc;
@property(nonatomic,strong) NSString *link;
@property(nonatomic) long long time;

@end
