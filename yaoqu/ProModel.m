//
//  ProModel.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/4.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "ProModel.h"

@implementation DisModel



@end

@implementation ProModel

+(NSDictionary *)objectClassInArray{
    return @{@"city_list":@"CityModel"};
}


@end


@implementation CityModel

-(NSString *)city_name{
    if (!_city_name) {
        return _city;
    }
    return _city_name;
}

+(NSDictionary *)objectClassInArray{
    return @{@"dis_list":@"DisModel"};
}

+(BOOL) isContainParent{
    return YES;
}
/**
 *  @author wjy, 15-01-26 14:01:01
 *
 *  @brief  设定表名
 *  @return 返回表名
 */
+(NSString *)getTableName
{
    return @"CityModel";
}
/**
 *  @author wjy, 15-01-26 14:01:22
 *
 *  @brief  设定表的单个主键
 *  @return 返回主键表
 */
+(NSString *)getPrimaryKey
{
    return @"private_id";
}

@end
