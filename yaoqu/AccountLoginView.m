//
//  AccountLoginView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/8.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "AccountLoginView.h"
#import "ImageTextField.h"
#import <UMSocialCore/UMSocialCore.h>
#import "RCUserCacheManager.h"
#import "RouteController.h"
#import "MemberApi.h"
#import "LoginApi.h"
#import "CommonApi.h"
#import "LoginInfoModel.h"
#import "AccessTokenModel.h"
#import "LLUtils.h"
@interface AccountLoginView()


@property(nonatomic,strong) ImageTextField *accountFld;
@property(nonatomic,strong) ImageTextField *passwordFld;


@property(nonatomic,strong) UIView *tipView;


@property(nonatomic,strong) NormalButton *loginBtn;
@property(nonatomic,strong) UIButton *wxBtn;
@property(nonatomic,strong) UIButton *qqBtn;

@end

@implementation AccountLoginView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    
    [self.accountFld mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.right.equalTo(self.mas_right).offset(-20);
        make.top.equalTo(self.mas_top).offset(20);
        make.height.mas_equalTo(50);
    }];
    
    [self.passwordFld mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.right.equalTo(self.mas_right).offset(-20);
        make.top.equalTo(self.accountFld.mas_bottom).offset(20);
        make.height.mas_equalTo(50);
    }];
    
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.right.equalTo(self.mas_right).offset(-20);
        make.top.equalTo(self.passwordFld.mas_bottom).offset(50);
        make.height.mas_equalTo(50);
    }];
    
    [self.forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.top.equalTo(self.loginBtn.mas_bottom).offset(10);
    }];
    
    [self.registBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-20);
        make.top.equalTo(self.loginBtn.mas_bottom).offset(10);
    }];
    
    [self.tipView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.loginBtn.mas_left).offset(30);
        make.right.equalTo(self.loginBtn.mas_right).offset(-30);
        make.top.equalTo(self.loginBtn.mas_bottom).offset(100);
        make.height.mas_equalTo(10);
    }];
    
    [self.qqBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tipView.mas_bottom).offset(20);
        make.size.mas_equalTo(CGSizeMake(50, 50));
        make.right.equalTo(self.mas_centerX).offset(-10);
        make.bottom.equalTo(self.mas_bottom).offset(-20);
    }];
    
    [self.wxBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_centerX).offset(10);
        make.top.equalTo(self.tipView.mas_bottom).offset(20);
        make.size.mas_equalTo(CGSizeMake(50, 50));
        make.bottom.equalTo(self.mas_bottom).offset(-20);
    }];
    
    
}


-(void)loginBtnClick:(id)sender{
    NSDictionary *dict = @{@"type":@"account",
                           @"phone":self.accountFld.text,
                           @"account":self.accountFld.text,
                           @"email":self.accountFld.text,
                           @"password":self.passwordFld.text
                           };
    [self loginWithInfo:dict failBlock:nil];
}


-(void)otherLoginBtnClick:(id)sender{
    WEAK_SELF;
    UMSocialPlatformType type;
    type = sender == self.qqBtn ? UMSocialPlatformType_QQ : UMSocialPlatformType_WechatSession;
    
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:type currentViewController:[self ex_viewController] completion:^(id result, NSError *error) {
        if (!error) {
            [weakSelf userAouth:result];
        }else{
//            [LLUtils showCenterTextHUD:error.description];
        }
        
    }];

}


-(void)userAouth:(UMSocialUserInfoResponse *)response{
    NSDictionary *dic = nil;// @{@"type":@"wechat",@"openid":@"1B6D68D1E8E5277585392BD7559CC9DA"};
//    dic = @{@"type":@"wechat",@"openid":@"ouc6O1PYA9X4WLrSTYXlsJv_d6z4",@"unionid":@"oBcJR1HelD7ofxxRP4BSDcAyvcAI"};
    
    if (response.platformType == UMSocialPlatformType_QQ) {
        dic = @{@"type":@"qq",@"qq_openid":response.openid};
    }else{
        dic = @{@"type":@"wechat",@"openid":response.openid,@"unionid":response.uid};
    }
    [self changeToPostModel:response];
    [self loginWithInfo:dic failBlock:self.loginFailBlock];
    
}

-(void)loginWithInfo:(NSDictionary *)dic failBlock:(BaseBlock)block{

    
    __block MBProgressHUD *hud = [LLUtils showActivityIndicatiorHUDWithTitle:nil];
    
    [[[LoginApi alloc]initWitObject:dic] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        
        if (info.error != ErrorCodeType_None){
            [hud hideAnimated:NO];
            if (block) {
                block(nil);
            }else{
                [LLUtils showCenterTextHUD:info.message];
            }
            return ;
        }
        
        LoginInfoModel *model = [LoginInfoModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
        
        [[RCUserCacheManager sharedManager]setCurrentLoginInfo:model];
        
        [[[CommonApi alloc]initWithModel:@"accessToken" object:model] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            
            AccessTokenModel *model = [AccessTokenModel objectWithKeyValues:responseObj];
            [[RCUserCacheManager sharedManager] setCurrentToken:model];
            
            [[[MemberApi alloc]initWitObject:responseObj] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                
                UserModel *userModel = [UserModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
                [[RCUserCacheManager sharedManager] setCurrentUser:userModel];
                [[RouteController sharedManager] openMainController];
                [hud hideAnimated:YES];
            }];
            
        }];
        
    }];

}


-(void)changeToPostModel:(UMSocialUserInfoResponse *)response{
    
    self.postModel.username = response.name;
    self.postModel.sex = [response.gender isEqualToString:@"男"] ? @"1" : @"0";
    
    
    if (response.platformType == UMSocialPlatformType_QQ){
        self.postModel.qq_openid = response.openid;
        self.postModel.unionid = nil;
    }else{
        self.postModel.openid = response.openid;
        self.postModel.unionid = response.uid;
    }
    
    self.postModel.imagelink = response.iconurl;
    
    
}


#pragma mark -




-(ImageTextField *)accountFld{
    if (!_accountFld) {
        _accountFld = [ImageTextField imageText:@"ic_login_user" placeHolder:@"输入手机或邮箱"];
        [self addSubview:_accountFld];
    }
    return _accountFld;
}

-(ImageTextField *)passwordFld{
    if (!_passwordFld) {
        _passwordFld = [ImageTextField imageText:@"ic_login_password" placeHolder:@"输入帐号密码"];
        _passwordFld.secureTextEntry = YES;
        [self addSubview:_passwordFld];
    }
    return _passwordFld;
}

-(NormalButton *)loginBtn{
    if (!_loginBtn) {
        _loginBtn = [NormalButton normalButton:@"登 陆"];
        [_loginBtn addTarget:self action:@selector(loginBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_loginBtn];
    }
    return _loginBtn;
}

-(UILabel *)forgetBtn{
    if (!_forgetBtn) {
        _forgetBtn = [ControllerHelper autoFitLabel];
        _forgetBtn.textColor = AppOrange;
        _forgetBtn.text = @"忘记密码？";
        _forgetBtn.font = SystemFont(11);
        
        [self addSubview:_forgetBtn];
    }
    return _forgetBtn;
}

-(UILabel *)registBtn{
    if (!_registBtn) {
        _registBtn = [ControllerHelper autoFitLabel];
        _registBtn.textColor = [UIColor whiteColor];
        _registBtn.text = @"新用户注册";
        _registBtn.font = SystemFont(11);
        
        [self addSubview:_registBtn];
    }
    return _registBtn;
}

-(UIButton *)wxBtn{
    if (!_wxBtn) {
        _wxBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_wxBtn setImage:[UIImage imageNamed:@"ic_login_wechat-2"] forState:UIControlStateNormal];
        [_wxBtn addTarget:self action:@selector(otherLoginBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_wxBtn];
    }
    return _wxBtn;
}

-(UIButton *)qqBtn{
    if (!_qqBtn) {
        _qqBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_qqBtn setImage:[UIImage imageNamed:@"ic_login_qq-2"] forState:UIControlStateNormal];
        [_qqBtn addTarget:self action:@selector(otherLoginBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_qqBtn];
    }
    return _qqBtn;
}


-(UIView *)tipView{
    if (!_tipView) {
        _tipView = [[UIView alloc]init];
        [self addSubview:_tipView];

        UILabel *label = [ControllerHelper autoFitLabel];
        label.font = SystemFont(10);
        label.text = LS(@"第三方登陆");
        label.textAlignment = NSTextAlignmentCenter;
        label.minimumScaleFactor = 0.5;
        label.textColor = [UIColor whiteColor];
        [_tipView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_tipView.mas_centerX);
            make.top.equalTo(_tipView.mas_top);
            make.bottom.equalTo(_tipView.mas_bottom);
        }];

        
        [[_tipView lineInColor:[UIColor whiteColor]] mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_tipView.mas_left).offset(20);
            make.right.equalTo(label.mas_left).offset(-5);
            make.centerY.equalTo(_tipView.mas_centerY);
            make.height.mas_equalTo(0.5);
        }];

        
        [[_tipView lineInColor:[UIColor whiteColor]] mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(label.mas_right).offset(5);
            make.right.equalTo(_tipView.mas_right).offset(-20);
            make.centerY.equalTo(_tipView.mas_centerY);
            make.height.mas_equalTo(0.5);
        }];

        
    }
    return _tipView;
}


@end
