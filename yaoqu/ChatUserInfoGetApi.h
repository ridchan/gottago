//
//  ChatUserInfoGetApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/12.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface ChatUserInfoGetApi : BaseApi

-(id)initWithMemberID:(NSString *)member_id;

@end
