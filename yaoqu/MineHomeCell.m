//
//  MineHomeCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/28.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineHomeCell.h"


@interface MineHomeCell()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UICollectionView *collectionView;


@end

@implementation MineHomeCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    self.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
}




-(void)setDatas:(NSArray *)datas{
    _datas = datas;
    [self.collectionView reloadData];
}





#pragma mark -

-(UICollectionViewFlowLayout *)layout{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake((APP_WIDTH - 1.0) / 3.0, (APP_WIDTH - 1.0) / 3.0);
    layout.minimumLineSpacing = 0.5;
    layout.minimumInteritemSpacing = 0.5;
    //    layout.headerReferenceSize = CGSizeMake(APP_WIDTH, 280);
    return layout;
}



-(UICollectionView *)collectionView{
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:[self layout]];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor clearColor];
        [_collectionView registerClass:[MineCollectionCell class] forCellWithReuseIdentifier:@"Cell"];
         
        
        [self.contentView addSubview:_collectionView];
    }
    return _collectionView;
}
         
 -(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
     return 1;
 }
 
 -(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
     return [self.datas count];
 }
 
 -(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
     MineCollectionCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
     NSDictionary *dict = self.datas[indexPath.item];
     cell.imageView.image = [UIImage imageNamed:dict[@"img"]];
     cell.label.text = dict[@"name"];
     cell.detailLbl.text = dict[@"desc"];
     return cell;
 }
 
 -(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
     NSDictionary *dict = self.datas[indexPath.item];
     [[RouteController sharedManager] openClassVC:[dict objectForKey:@"vc"] withObj:[dict objectForKey:@"param"]];
     if ([self.delegate respondsToSelector:@selector(homeCellDidSelectAtIndex:)]){
         [self.delegate homeCellDidSelectAtIndex:indexPath.item];
     }
//     [self pushViewControllerWithName:[dict objectForKey:@"vc"]];
 }


-(MineCollectionCell *)detailCellAtIndex:(NSInteger)index{
    return (MineCollectionCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
}
         

@end
