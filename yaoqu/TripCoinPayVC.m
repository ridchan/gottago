//
//  TripCoinPayVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TripCoinPayVC.h"
#import "UserChargeAddApi.h"
#import "RCUserCacheManager.h"
#import "RechargeModel.h"
#import <AlipaySDK/AlipaySDK.h>
#import "RechargeApi.h"
#import "MemberApi.h"
#import "TravelApi.h"
#import "WeiXinPayObj.h"
#import "CommonConfigManager.h"
@interface TripCoinPayVC ()

@property(nonatomic,strong) UIView *totalView;
@property(nonatomic,strong) UIView *payTypeView;

@property(nonatomic,strong) UIImageView *wechatImg;
@property(nonatomic,strong) UIImageView *aliImg;

@property(nonatomic,strong) ImageButton *checkBtn;

@property(nonatomic) PayType payType;

@property(nonatomic,strong) RechargeModel *rechargeModel;

@end

@implementation TripCoinPayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if (!self.rechargeModel) self.rechargeModel = self.paramObj;
    
    self.title = LS(@"确认支付");
    
    
    [self.totalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(10);
        make.right.equalTo(self.view.mas_right).offset(-10);
        make.top.equalTo(self.view.mas_top).offset(74);
    }];
    
    [self.payTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(10);
        make.right.equalTo(self.view.mas_right).offset(-10);
        make.top.equalTo(self.totalView.mas_bottom).offset(10);
    }];
    
    [self.checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.payTypeView.mas_left);
        make.right.equalTo(self.payTypeView.mas_right);
        make.top.equalTo(self.payTypeView.mas_bottom).offset(5);
        make.height.mas_equalTo(30);
    }];
    
    
    UIButton *payBtn = [NormalButton normalButton:@"确定支付"];
    [payBtn addTarget:self action:@selector(payBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:payBtn];
    
    [payBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(10);
        make.right.equalTo(self.view.mas_right).offset(-10);
        make.top.equalTo(self.checkBtn.mas_bottom).offset(10);
        make.height.mas_equalTo(50);
    }];
    
    
    self.payType = PayType_AliPay;
    
    
    switch ([self.rechargeModel.type integerValue]) {
        case RechargeType_TravelEnroll:
        case RechargeType_OpenMember:
        case RechargeType_Money:
            self.checkBtn.hidden = YES;
            break;
        case RechargeType_Currency:
        case RechargeType_Integral:
        case RechargeType_Rollgift:
            self.checkBtn.hidden = NO;
            break;
        
        
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aliPayResult:) name:AliPayNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aliPayResult:) name:WeChatPayNotification object:nil];
    
    // Do any additional setup after loading the view.
}

-(void)aliPayResult:(NSNotification *)notification{
    NSDictionary *resultDic = notification.object;
    if ([[resultDic objectForKey:@"resultStatus"] integerValue] == 9000) {
        if ([self.rechargeModel.actionType integerValue] == RechargeType_OpenMember){
            MemberApi *api = [[MemberApi alloc]initWitObject:@{@"val":self.rechargeModel.val}];
            api.method = YTKRequestMethodPUT;
            [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                if (info.error == ErrorCodeType_None) {
                    [[RCUserCacheManager sharedManager] renewAccount];
                    [self returnViewControllerWithName:@"MemberCenterVC"];
                }else{
                    [LLUtils showCenterTextHUD:info.message];
                }
            }];
        }else{
            [[RCUserCacheManager sharedManager] renewAccount];
            if([self.rechargeModel.actionType integerValue] == RechargeType_TravelEnroll){
                [self.navigationController popToRootViewControllerAnimated:YES];
                NSString *link = [[CommonConfigManager sharedManager].api_host stringByAppendingPathComponent:@"order.html"];
                [[RouteController sharedManager]openClassVC:@"YQWebViewVC" withObj:link];
            }else{
                [self popToHomePageWithTabIndex:1 completion:NULL];
//                [self returnViewControllerWithName:@"MineWalletVC"];
            }
            
            [LLUtils showActionSuccessHUD:@"支付成功"];

        }
    }else{
        [LLUtils showCenterTextHUD:[resultDic objectForKey:@"memo"]];
    }

}


-(void)payBtnClick:(id)sender{

    WEAK_SELF;
    if ([self.rechargeModel.actionType integerValue] == RechargeType_TravelEnroll) {
        self.enrollModel.pay_type = self.payType;
        
        
        [self.enrollObj setValue:@(self.payType) forKey:@"pay_type"];
        

        
        TravelApi *api = [[TravelApi alloc]initWitObject:self.enrollObj];
        api.method = YTKRequestMethodPOST;
        [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            if (info.error == ErrorCodeType_None){
                NSDictionary *data = [responseObj objectForKey:@"data"];
                if ([[[data objectForKey:@"order"] objectForKey:@"pay_total"] floatValue] == 0){
                    PostNotification(AliPayNotification, @{@"resultStatus":@"9000"})
                }else{
                    if (weakSelf.payType == PayType_AliPay){
                        [[AlipaySDK defaultService] payOrder:[data objectForKey:@"pay_data"] fromScheme:@"yaoqu" callback:^(NSDictionary *resultDic) {
                        }];
                    
                    }else{
                        
                        NSDictionary *dict = [data objectForKey:@"pay_data"];
                        PayReq* req             = [[PayReq alloc] init];
                        req.openID              = [dict objectForKey:@"appid"];
                        req.partnerId           = [dict objectForKey:@"partnerid"];
                        req.prepayId            = [dict objectForKey:@"prepayid"];
                        req.nonceStr            = [dict objectForKey:@"noncestr"];
                        req.timeStamp           = [[dict objectForKey:@"timestamp"] intValue];
                        req.package             = [dict objectForKey:@"package"];
                        req.sign                = [dict objectForKey:@"sign"];
                        
                        NSLog(@"req %@",[req keyValues]);
                        
                        [WXApi sendReq:req];
                    }

                }
            }else{
                [LLUtils showCenterTextHUD:info.message];
            }
        }];
    }else{
        
        self.rechargeModel.pay_type = [NSString intValue:self.payType];
        if (self.checkBtn.selected){
            NSString *money = [RCUserCacheManager sharedManager].currentUser.money;
            if ([self.rechargeModel.pay_total floatValue] > [money floatValue]) {
                self.rechargeModel.money = money;
            }else{
                self.rechargeModel.money = self.rechargeModel.pay_total;
            }
        }else{
            self.rechargeModel.money = @"0";
        }
        
        
        self.rechargeModel.log_desc = self.rechargeModel.title_desc;
        RechargeApi *api = [[RechargeApi alloc]initWitObject:self.rechargeModel];
        api.method = YTKRequestMethodPOST;
        
        [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            
            if (info.error == ErrorCodeType_None){
                NSDictionary *data = [responseObj objectForKey:@"data"];
                if ([[[data objectForKey:@"order"] objectForKey:@"pay_total"] floatValue] == 0){
                    PostNotification(AliPayNotification, @{@"resultStatus":@"9000"})
                }else{
                    if (weakSelf.payType == PayType_AliPay){
                        [[AlipaySDK defaultService] payOrder:[data objectForKey:@"pay_data"] fromScheme:@"yaoqu" callback:^(NSDictionary *resultDic) {
                        }];
                        
                    }else{
                        
                        NSDictionary *dict = [data objectForKey:@"pay_data"];
                        PayReq* req             = [[PayReq alloc] init];
                        req.openID              = [dict objectForKey:@"appid"];
                        req.partnerId           = [dict objectForKey:@"partnerid"];
                        req.prepayId            = [dict objectForKey:@"prepayid"];
                        req.nonceStr            = [dict objectForKey:@"noncestr"];
                        req.timeStamp           = [[dict objectForKey:@"timestamp"] intValue];
                        req.package             = [dict objectForKey:@"package"];
                        req.sign                = [dict objectForKey:@"sign"];
                    
                        
                        [WXApi sendReq:req];
                    }
                    
                }
            }else{
                [LLUtils showCenterTextHUD:info.message];
            }
        }];
    }
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *)totalView{
    
    if (_totalView) return _totalView;
    
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor whiteColor];
    
    UILabel *titleLbl = [ControllerHelper autoFitLabel];
    titleLbl.font = SystemFont(14);
    titleLbl.text = self.rechargeModel.title_desc ? self.rechargeModel.title_desc : LS(@"总计");
    [view addSubview:titleLbl];
    
    
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left).offset(10);
        make.top.equalTo(view.mas_top);
        make.height.mas_equalTo(50);
    }];
    
    
    UILabel *valueLbl = [ControllerHelper autoFitLabel];
    valueLbl.textColor = AppGray;
    valueLbl.font = SystemFont(14);
    valueLbl.text = self.rechargeModel.title_value;
    
    [view addSubview:valueLbl];
    
    [valueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view.mas_right).offset(-10);
        make.top.equalTo(view.mas_top);
        make.height.mas_equalTo(50);
    }];
    
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = AppLineColor;
    [view addSubview:line];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left).offset(10);
        make.top.equalTo(titleLbl.mas_bottom);
        make.right.equalTo(view.mas_right).offset(-10);
        make.height.mas_equalTo(0.5);
    }];
    
    
    UILabel *payLbl = [ControllerHelper autoFitLabel];
    payLbl.text = LS(@"支付金额 ");
    payLbl.font = SystemFont(14);
    [view addSubview:payLbl];
    
    
    [payLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left).offset(10);
        make.top.equalTo(titleLbl.mas_bottom);
        make.height.mas_equalTo(50);
    }];
    
    
    UILabel *totalLbl = [ControllerHelper autoFitLabel];
    totalLbl.textColor = [UIColor orangeColor];
    totalLbl.font = SystemFont(14);
    totalLbl.text = [NSString stringWithFormat:@"%@%@",MoneySign,self.rechargeModel.pay_total];
    
    [view addSubview:totalLbl];
    
    [totalLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view.mas_right).offset(-10);
        make.top.equalTo(titleLbl.mas_bottom);
        make.height.mas_equalTo(50);
        make.bottom.equalTo(view.mas_bottom);
    }];
    
    _totalView = view;
    
    [self.view addSubview:_totalView];
    
    return view;
}


-(UIView *)payTypeView{
    if (!_payTypeView) {
        _payTypeView = [[UIView alloc]init];
        _payTypeView.backgroundColor = [UIColor whiteColor];
        
        UILabel *tipLbl = [ControllerHelper autoFitLabel];
        tipLbl.text = LS(@"请选择支付的方式");
        tipLbl.font = SystemFont(14);
        
        [_payTypeView addSubview:tipLbl];
        
        [tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_payTypeView.mas_left).offset(10);
            make.top.equalTo(_payTypeView.mas_top);
            make.height.mas_equalTo(40);
        }];
        
        UIView *line = [[UIView alloc]init];
        line.backgroundColor = AppLineColor;
        [_payTypeView addSubview:line];
        
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_payTypeView.mas_left).offset(10);
            make.top.equalTo(tipLbl.mas_bottom);
            make.right.equalTo(_payTypeView.mas_right).offset(-10);
            make.height.mas_equalTo(0.5);
        }];
        
        UIView *wechatView = [self papTypeView:@"ic_orderpay_wechat" title:LS(@"微信支付") payType:PayType_WeChat];
        self.wechatImg.image = [UIImage imageNamed:@"ic_checked_yellow"];
        [_payTypeView addSubview:wechatView];
        [wechatView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_payTypeView.mas_left).offset(10);
            make.right.equalTo(_payTypeView.mas_right).offset(-10);
            make.top.equalTo(line.mas_bottom);
            make.height.mas_equalTo(50);
        }];
        
        
        UIView *line2 = [[UIView alloc]init];
        line2.backgroundColor = AppLineColor;
        [_payTypeView addSubview:line2];
        
        [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_payTypeView.mas_left).offset(10);
            make.top.equalTo(wechatView.mas_bottom);
            make.right.equalTo(_payTypeView.mas_right).offset(-10);
            make.height.mas_equalTo(0.5);
        }];
        
        
        
        UIView *aliView = [self papTypeView:@"ic_orderpay_zhifubao" title:LS(@"支付宝") payType:PayType_AliPay];
        self.aliImg.image = [UIImage imageNamed:@"ic_not_checked"];
        [_payTypeView addSubview:aliView];

        [aliView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_payTypeView.mas_left).offset(10);
            make.right.equalTo(_payTypeView.mas_right).offset(-10);
            make.top.equalTo(line2.mas_bottom);
            make.height.mas_equalTo(50);
            make.bottom.equalTo(_payTypeView);
        }];

        [self.view addSubview:_payTypeView];
    }
    
    return _payTypeView;
}


-(UIView *)papTypeView:(NSString *)image title:(NSString *)title payType:(PayType)payType{
    UIView *view  = [[UIView alloc]init];
    view.backgroundColor = [UIColor whiteColor];
    view.tag = payType;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(payTypeTap:)];
    [view addGestureRecognizer:tap];
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:image]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [view addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left);
        make.top.equalTo(view.mas_top);
        make.bottom.equalTo(view.mas_bottom);
        make.width.mas_equalTo(30);
    }];
    
    UILabel *titleLbl = [ControllerHelper autoFitLabel];
    titleLbl.text = title;
    titleLbl.font = SystemFont(14);
    [view addSubview:titleLbl];
    
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageView.mas_right).offset(10);
        make.centerY.equalTo(view);
    }];
    
    UIImageView *checkBtn = [[UIImageView alloc]init];
    [view addSubview:checkBtn];
    [checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(view.mas_right).offset(-10);
        make.centerY.equalTo(view);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    if (payType == PayType_AliPay) {
        self.aliImg = checkBtn;
    }else{
        self.wechatImg = checkBtn;
    }
    
    return view;
}



-(void)payTypeTap:(UITapGestureRecognizer *)sender{
//    if (sender.view.tag == PayType_AliPay){
//        self.aliImg.image = [UIImage imageNamed:@"ic_checked_yellow"];
//        self.wechatImg.image = [UIImage imageNamed:@"ic_not_checked"];
//    }else{
//        self.aliImg.image = [UIImage imageNamed:@"ic_not_checked"];
//        self.wechatImg.image = [UIImage imageNamed:@"ic_checked_yellow"];
//    }
    self.payType = sender.view.tag;
}

-(void)setPayType:(PayType)payType{
    _payType = payType;
    if (_payType == PayType_AliPay){
        self.aliImg.image = [UIImage imageNamed:@"ic_checked_yellow"];
        self.wechatImg.image = [UIImage imageNamed:@"ic_not_checked"];
    }else{
        self.aliImg.image = [UIImage imageNamed:@"ic_not_checked"];
        self.wechatImg.image = [UIImage imageNamed:@"ic_checked_yellow"];
    }
}


-(ImageButton *)checkBtn{
    if (!_checkBtn) {
        _checkBtn = [[ImageButton alloc]init];
        _checkBtn.contentImage.image = [UIImage imageNamed:@"ic_not_checked"];
        _checkBtn.titleLbl.font = SystemFont(12);
        _checkBtn.titleLbl.text = [NSString stringWithFormat:@"使用余额支付(剩余余额￥%0.0f)",[[RCUserCacheManager sharedManager].currentUser.money floatValue]];
        [_checkBtn addTarget:self action:@selector(checkBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        _checkBtn.hidden = YES;
        [self.view addSubview:_checkBtn];
    }
    return _checkBtn;
}

-(void)checkBtnClick:(id)sender{
    _checkBtn.selected = !_checkBtn.selected;
    _checkBtn.contentImage.image = _checkBtn.selected ? [UIImage imageNamed:@"ic_checked_yellow"] : [UIImage imageNamed:@"ic_not_checked"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
