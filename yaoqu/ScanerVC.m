//
//  ScanerVC.m
//  QBH
//
//  Created by 陳景雲 on 2017/1/9.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "ScanerVC.h"
#import <AVFoundation/AVFoundation.h>
#import "UserModel.h"
@interface ScanerVC ()<AVCaptureMetadataOutputObjectsDelegate>{
    AVCaptureSession *session;
    AVCaptureDevice *device;
    AVCaptureVideoPreviewLayer *previewLayer;
    UIView *qrCodeFrameView;
    
    UIImageView *scanImg;
}
@end

@implementation ScanerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self wr_setNavBarBackgroundAlpha:0];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupScaner];
    [session startRunning];
}


-(void)setupScaner{
    device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
    
    AVCaptureMetadataOutput *output = [[AVCaptureMetadataOutput alloc]init];
    
    [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    
    //    [output setRectOfInterest:CGRectMake(0, 0, 100, 100)];
    
    session = [[AVCaptureSession alloc]init];
    
    [session setSessionPreset:AVCaptureSessionPresetHigh];
    
    [session addInput:input];
    [session addOutput:output];
    
    
    output.metadataObjectTypes = @[AVMetadataObjectTypeQRCode,AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode128Code];
    
    previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
    previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    previewLayer.frame = CGRectMake(0, 0, APP_WIDTH, APP_HEIGHT );
    
    [self.view.layer insertSublayer:previewLayer above:0];
    
    scanImg = [[UIImageView alloc]init];
    scanImg.image = [UIImage imageNamed:@"scansize"];
    [self.view addSubview:scanImg];
    [scanImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.view.mas_centerY).offset(-10);
        make.centerX.equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(APP_WIDTH * 0.6, APP_WIDTH * 0.6));
    }];
    
    [self setOverView];

}


-(void)createViews{

}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    if (metadataObjects.count>0) {
        [session stopRunning];
        AVMetadataMachineReadableCodeObject *metadataObject = [metadataObjects objectAtIndex :0];
        //输出扫描字符串
        NSLog(@"code %@  %@",metadataObject.stringValue,metadataObjects);
        NSString *member_id = [metadataObject.stringValue substringFromString:@"member_id="];
        if ([member_id length] > 0) {
            UserModel *model = [[UserModel alloc]init];
            model.member_id = member_id;
            [[RouteController sharedManager]openHomePageVC:model];
        }
        
    }
}



#pragma mark - 添加模糊效果
- (void)setOverView {
    
    //上
    [[self creatView] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(scanImg.mas_top);
    }];
    //左
    [[self creatView] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(scanImg.mas_top);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(scanImg.mas_left);
        make.bottom.equalTo(scanImg.mas_bottom);
    }];
    //右
    [[self creatView] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(scanImg.mas_top);
        make.left.equalTo(scanImg.mas_right);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(scanImg.mas_bottom);
    }];
    
    //下
    [[self creatView] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(scanImg.mas_bottom);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
}

- (UIView *)creatView {
    //    CGFloat alpha = 0.5;
    UIColor *backColor = RGBA(33, 36, 41 , 0.6);
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = backColor;
    
    [self.view addSubview:view];
    return view;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
