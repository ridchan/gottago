//
//  UserIdentifyCell.h
//  yaoqu
//
//  Created by ridchan on 2017/7/8.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UserIdentifyDetailCell : UITableViewCell

@property(nonatomic,strong) UIImageView *selectImage;
@property(nonatomic,strong) UILabel *descLbl;

@end

@interface UserIdentifyCell : UITableViewCell

@end
