//
//  MineGiftStoreVC.m
//  yaoqu
//
//  Created by ridchan on 2017/9/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineGiftStoreVC.h"
#import "GiftCell.h"
#import "GiftApi.h"
#import "GiftPostModel.h"
#import "NormalCellLayout.h"
#import "CommonConfigManager.h"

@interface MineGiftStoreVC ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) NSMutableArray *datas;
@property(nonatomic,strong) UIButton *receiveBtn;
@property(nonatomic,strong) UIButton *sendBtn;

@end

@implementation MineGiftStoreVC
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self commonInit];
    
    
    
    
    [[CommonConfigManager sharedManager] getCommonConfig];
    [RACObserve([CommonConfigManager sharedManager], hostModel) subscribeNext:^(HostModel *x) {
        self.datas = (NSMutableArray *)x.gift;
    }];
    
    //    [self btnClick:self.receiveBtn];
    
    // Do any additional setup after loading the view.
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        NormalCellLayout *layout = [[NormalCellLayout alloc]init];
        layout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10);
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        layout.numOfColumn = 4;
        layout.cellHeight = 100;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        
        [_collectionView registerClass:[GiftCell class] forCellWithReuseIdentifier:@"Cell"];
        [self.view addSubview:_collectionView];
        
    }
    return _collectionView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)commonInit{
    self.title = LS(@"礼物商城");
    
    //    [self setRightItemWithIcon:[UIImage imageNamed:@"ic_question"] selector:nil];
    
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top).offset(0);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GiftCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.model = self.datas[indexPath.item];
    return cell;
}


@end
