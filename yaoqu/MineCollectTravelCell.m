//
//  MineCollectTravelCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/24.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineCollectTravelCell.h"

@interface MineCollectTravelCell()

@property(nonatomic,strong) UIImageView *contentImage;
@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) UIView *bgView;

@end

@implementation MineCollectTravelCell



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self commonInit];
    }
    return self;
}

-(void)setModel:(TravelModel *)model{
    _model = model;
    [self.contentImage setImageName:model.image placeholder:nil];
    self.titleLbl.text = model.title;
}


-(void)commonInit{
    [self.contentImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.height.mas_equalTo(40);
    }];
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.centerY.equalTo(self.bgView.mas_centerY);
    }];
    
}

-(UIView *)bgView{
    if (!_bgView) {
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = RGBA(0, 0, 0, 0.3);
        [self.contentView addSubview:_bgView];
    }
    return _bgView;
}

-(UIImageView *)contentImage{
    if (!_contentImage) {
        _contentImage = [[UIImageView alloc]init];
        _contentImage.clipsToBounds = YES;
        _contentImage.contentMode = UIViewContentModeScaleAspectFill;
        _contentImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.contentView addSubview:_contentImage];
    }
    return _contentImage;
}

-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        _titleLbl.textColor = [UIColor whiteColor];
        _titleLbl.font = SystemFont(14);
        [self.contentView addSubview:_titleLbl];
    }
    return _titleLbl;
}


@end
