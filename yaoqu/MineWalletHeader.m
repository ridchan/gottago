//
//  MineWalletHeader.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/1.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineWalletHeader.h"
#import "RCUserCacheManager.h"
@interface MineWalletHeader()

@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) UILabel *totalLbl;
@property(nonatomic,strong) UIButton *payBtn;
@property(nonatomic,strong) UIView *line;
@property(nonatomic,strong) UILabel *detailLbl;
@property(nonatomic,strong) UIImageView *accessImage;
@property(nonatomic,strong) UIButton *detailBtn;

@end

@implementation MineWalletHeader


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self commonInit];
        [RACObserve([RCUserCacheManager sharedManager], currentUser.money) subscribeNext:^(id x) {
            self.totalLbl.text = [NSString stringWithFormat:@"¥%0.0f",[x floatValue]];
        }];
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)commonInit{
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(10);
        make.left.equalTo(self.mas_left).offset(10);
    }];
    
    [self.totalLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.titleLbl.mas_bottom);
        make.height.mas_equalTo(80);
    }];
    
    [self.payBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.centerY.equalTo(self.totalLbl);
        make.size.mas_equalTo(CGSizeMake(80, 30));
    }];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.totalLbl.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];
    
    [self.detailBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.line.mas_bottom);
        make.height.mas_equalTo(40);
        make.bottom.equalTo(self.mas_bottom);
    }];
    
    [self.detailLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.centerY.equalTo(self.detailBtn.mas_centerY);
    }];
    
    [self.accessImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.centerY.equalTo(self.detailLbl.mas_centerY);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(RightAccessWidth);
    }];
    
}

-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        _titleLbl.textColor = AppGray;
        _titleLbl.font = SystemFont(12);
        _titleLbl.text = LS(@"余额");
        [self addSubview:_titleLbl];
    }
    return _titleLbl;
}

-(UILabel *)totalLbl{
    if (!_totalLbl) {
        _totalLbl = [ControllerHelper autoFitLabel];
        _totalLbl.font = SystemFont(22);
        _totalLbl.textColor = [UIColor orangeColor];
        [self addSubview:_totalLbl];
    }
    return _totalLbl;
}

-(UIButton *)payBtn{
    if (!_payBtn) {
        _payBtn = [NormalButton normalButton:LS(@"充值")];
        [_payBtn addTarget:self action:@selector(payBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        _payBtn.titleLabel.font = SystemFont(14);
        [self addSubview:_payBtn];
    }
    return _payBtn;
}

-(UIView *)line{
    if (!_line) {
        _line = [[UIView alloc]init];
        _line.backgroundColor = AppLineColor;
        [self addSubview:_line];
    }
    return _line;
}

-(UILabel *)detailLbl{
    if (!_detailLbl) {
        _detailLbl = [ControllerHelper autoFitLabel];
        _detailLbl.font = SystemFont(16);
        _detailLbl.text = LS(@"余额明细");
        [self addSubview:_detailLbl];
    }
    return _detailLbl;
}

-(UIImageView *)accessImage{
    if (!_accessImage) {
        _accessImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_forward"]];
        _accessImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_accessImage];
    }
    return _accessImage;
}

-(UIButton *)detailBtn{
    if (!_detailBtn) {
        _detailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_detailBtn addTarget:self action:@selector(detailBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_detailBtn];
    }
    return _detailBtn;
}


#pragma mark -
#pragma mark action

-(void)payBtnClick:(id)sender{
    [[RouteController sharedManager] openClassVC:@"MineMoneyRechargeVC"];
}

-(void)detailBtnClick:(id)sender{
    [[RouteController sharedManager] openClassVC:@"MineWalletDetailVC" withObj:@(RechargeType_Money)];
}

@end
