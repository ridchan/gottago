//
//  CommonApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/1.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface CommonApi : BaseApi

-(id)initWithModel:(NSString *)model object:(id)obj;

@end
