//
//  BaseModel.h
//  yaoqu
//
//  Created by ridchan on 2017/6/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LKDBHelper.h"
#import <MJExtension/MJExtension.h>


@interface BaseModel : NSObject

@property(nonatomic,strong) NSString *private_id;

//拷贝自己
-(id)deepCopy;

//转换类型

-(id)objectWithClass:(NSString *)className;

@end
