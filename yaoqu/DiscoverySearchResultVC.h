//
//  DiscoverySearchResultVC.h
//  yaoqu
//
//  Created by ridchan on 2017/9/4.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NormalTableViewController.h"

@interface DiscoverySearchResultVC : NormalTableViewController

@property(nonatomic,strong) NSString *searchType;

-(void)showAtIndex:(NSInteger)index;

@end
