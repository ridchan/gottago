//
//  MineRewardHeader.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineRewardHeader.h"
#import "RCUserCacheManager.h"
#import "PGDatePicker.h"
@interface MineRewardHeader()<PGDatePickerDelegate>

@property(nonatomic,strong) UILabel *dataLbl;
@property(nonatomic,strong) UIImageView *tipImage;
@property(nonatomic,strong) UILabel *tipLbl;

@property(nonatomic,strong) UIButton *filterBtn;

@property(nonatomic,strong) UIView *line;
@property(nonatomic,strong) ImageButton *totalBtn1;
@property(nonatomic,strong) ImageButton *totalBtn2;


@end

@implementation MineRewardHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self commonInit];
    }
    return self;
}


-(void)commonInit{
    [self.dataLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.mas_top).offset(10);
    }];
    
    [self.tipImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.centerY.equalTo(self.tipLbl.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    [self.tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.tipImage.mas_right).offset(5);
        make.centerY.equalTo(self.tipImage.mas_centerY);
    }];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.totalBtn1.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(0.5, 40));
    }];
    
    [self.totalBtn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tipLbl.mas_bottom).offset(40);
        make.centerX.equalTo(self.mas_centerX).multipliedBy(0.5);
        make.height.mas_equalTo(30);
        make.bottom.equalTo(self.mas_bottom).offset(-20);
    }];
    
    [self.totalBtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tipLbl.mas_bottom).offset(40);
        make.centerX.equalTo(self.mas_centerX).multipliedBy(1.5);
        make.height.mas_equalTo(30);
        make.bottom.equalTo(self.mas_bottom).offset(-20);
    }];
    
    [self.filterBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.top.equalTo(self.mas_top).offset(5);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    
    
    
    
    
}


-(UILabel *)dataLbl{
    if (!_dataLbl) {
        _dataLbl = [ControllerHelper autoFitLabel];
        
        _dataLbl.text = [NSString stringFromTimeMark:[RCUserCacheManager sharedManager].currentUser.reg_time format:kDateFormatTypeYYYYMM];
        _dataLbl.font = SystemFont(11);
        [self addSubview:_dataLbl];
    }
    return _dataLbl;
}

-(UIImageView *)tipImage{
    if (!_tipImage) {
        _tipImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_user_month_reward"]];
        _tipImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_tipImage];
    }
    return _tipImage;
}

-(UILabel *)tipLbl{
    if (!_tipLbl) {
        _tipLbl = [ControllerHelper autoFitLabel];
        _tipLbl.text = @"本月累积";
        [self addSubview:_tipLbl];
    }
    return _tipLbl;
}

-(UIButton *)filterBtn{
    if (!_filterBtn) {
        _filterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_filterBtn setImage:[UIImage imageNamed:@"ic_calendar"] forState:UIControlStateNormal];
        [_filterBtn addTarget:self action:@selector(monthBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_filterBtn];
    }
    return _filterBtn;
}

-(ImageButton *)totalBtn1{
    if (!_totalBtn1) {
        _totalBtn1 = [[ImageButton alloc]init];
        _totalBtn1.contentImage.image = [UIImage imageNamed:@"ic_coin"];
        _totalBtn1.titleLbl.textColor = [UIColor orangeColor];
        _totalBtn1.titleLbl.font = SystemFont(20);
        _totalBtn1.titleLbl.text = @"0";
        [self addSubview:_totalBtn1];
    }
    return _totalBtn1;
}

-(ImageButton *)totalBtn2{
    if (!_totalBtn2) {
        _totalBtn2 = [[ImageButton alloc]init];
        _totalBtn2.contentImage.image = [UIImage imageNamed:@"ic_score"];
        _totalBtn2.titleLbl.font = SystemFont(20);
        _totalBtn2.titleLbl.text = @"0";
        [self addSubview:_totalBtn2];
    }
    return _totalBtn2;
}

-(UIView *)line{
    if (!_line) {
        _line = [[UIView alloc]init];
        _line.backgroundColor = AppLineColor;
        [self addSubview:_line];
    }
    return _line;
}

-(void)setDict:(NSDictionary *)dict{
    _dict = dict;
//    self.dataLbl.text = [dict objectForKey:@"date"];
    self.totalBtn1.titleLbl.text = [[[dict objectForKey:@"total"] objectForKey:@"currency"] stringValue];
    self.totalBtn2.titleLbl.text = [[[dict objectForKey:@"total"] objectForKey:@"integral"] stringValue];
}

-(void)monthBtnClick:(id)sender{
    PGDatePicker *datePicker = [[PGDatePicker alloc]init];
    datePicker.delegate = self;
    [datePicker show];
    datePicker.datePickerMode = PGDatePickerModeYearAndMonth;
}

-(void)datePicker:(PGDatePicker *)datePicker didSelectDate:(NSDateComponents *)dateComponents{
    NSLog(@"%@",dateComponents);
    self.YearAndMonth = [NSString stringWithFormat:@"%ld-%ld-1",dateComponents.year,dateComponents.month];
    
}

@end
