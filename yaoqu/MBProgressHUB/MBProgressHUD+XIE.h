//
//  MBProgressHUD+XIE.h
//  Showcase
//
//  Created by 谢晋叶 on 2017/2/6.
//  Copyright © 2017年 谢晋叶. All rights reserved.
//

#import "MBProgressHUD.h"

@interface MBProgressHUD (XIE)


/**
 *  以下方法省略UIView参数， 默认UIView = nil. 
 *  对应调用 [self xxxxx:tip view:nil];
 */
+ (void)showInfoTip     :(NSString*) tip;
+ (void)showErrorTip    :(NSString*) tip;
+ (void)showSuccessTip  :(NSString*) tip;


+ (void)showInfoTip     :(NSString*) tip delay:(float)delay withCompleteBlock:(MBProgressHUDCompletionBlock)block;
+ (void)showErrorTip    :(NSString*) tip delay:(float)delay withCompleteBlock:(MBProgressHUDCompletionBlock)block;
+ (void)showSuccessTip  :(NSString*) tip delay:(float)delay withCompleteBlock:(MBProgressHUDCompletionBlock)block;



/**
 *  最终执行的方法
 */
+ (void)show:(NSString *)text icon:(NSString *)icon view:(UIView *)view
       delay:(float)delay completeBlock:(MBProgressHUDCompletionBlock)complete;





+ (void)showIndeterminate;
+ (NSProgress*)showProgressWithText:(NSString*)text;




/**
 *  隐藏ProgressHUD
 */
+ (UIViewController*)topViewController;
+ (UIView *)getLastView;
+ (void)hideHUD;
+ (void)hideHUDForView:(UIView*)view;


// Assert

+ (BOOL)assertTrue:(BOOL)condition msg:(NSString*)msg;


@end
