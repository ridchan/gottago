//
//  MBProgressHUD+XIE.m
//  Showcase
//
//  Created by 谢晋叶 on 2017/2/6.
//  Copyright © 2017年 谢晋叶. All rights reserved.
//

#import "MBProgressHUD+XIE.h"

static MBProgressHUD* _hud = nil;

@implementation MBProgressHUD (XIE)

/**
 *  以下方法省略UIView参数， 默认UIView = nil.
 *  对应调用 [self xxxxx:tip view:nil];
 */
+ (void)showInfoTip     :(NSString*) tip {
    [self showInfoTip:tip delay:2.0 withCompleteBlock:nil];
}

+ (void)showErrorTip    :(NSString*) tip {
    [self showErrorTip:tip delay:2.0 withCompleteBlock:nil];
}

+ (void)showSuccessTip  :(NSString*) tip {
    [self showSuccessTip:tip delay:2.0 withCompleteBlock:nil];
}


/**
 *  以下的每个方法对应不同的背景icon.
 *  再执行[self show:tip icon:@[variable icon] view:view];
 */
+ (void)showInfoTip     :(NSString*) tip delay:(float)delay withCompleteBlock:(MBProgressHUDCompletionBlock)block {
    [self show:tip icon:@"info.png"    view:nil delay:delay completeBlock:block];
}

+ (void)showErrorTip    :(NSString*) tip delay:(float)delay withCompleteBlock:(MBProgressHUDCompletionBlock)block {
    [self show:tip icon:@"error.png"   view:nil delay:delay completeBlock:block];
}

+ (void)showSuccessTip  :(NSString*) tip delay:(float)delay withCompleteBlock:(MBProgressHUDCompletionBlock)block {
    [self show:tip icon:@"success.png" view:nil delay:delay completeBlock:block];
}


/**
 *  最终执行的方法
 */
+ (void)show:(NSString *)text icon:(NSString *)icon view:(UIView *)view
       delay:(float)delay completeBlock:(MBProgressHUDCompletionBlock)complete {
    [self hideHUD];
    
    if (view == nil) {
        view = [self getLastView];
    }
    
    if (nil != view) {
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        
        hud.mode       = MBProgressHUDModeCustomView;
        
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"MBProgressHUD.bundle/%@", icon]]];
        if ([text length] < 20)
            hud.label.text = text;
        else
            hud.detailsLabel.text = text;
        hud.completionBlock = complete;
        [hud hideAnimated:YES afterDelay:delay];
    }
}


+ (BOOL)assertTrue:(BOOL)condition msg:(NSString*)msg {
    if (condition) {
        [MBProgressHUD showErrorTip:msg];
    }
    return condition;
}


+ (void)showIndeterminate {
    _hud = [MBProgressHUD showHUDAddedTo:[self getLastView] animated:YES];
    
//    BOOL isLegacy = kCFCoreFoundationVersionNumber < kCFCoreFoundationVersionNumber_iOS_7_0;
//    _contentColor = isLegacy ? [UIColor whiteColor] : [UIColor colorWithWhite:0.f alpha:0.7f];
    // Transparent background
    _hud.opaque = NO;
    _hud.backgroundColor = [UIColor clearColor];
    // Make it invisible for now
    _hud.alpha = 0.0f;
    _hud.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _hud.layer.allowsGroupOpacity = NO;
    
    _hud.mode = MBProgressHUDModeIndeterminate;
}

+ (NSProgress*)showProgressWithText:(NSString*)text {
    NSProgress* progress = [NSProgress progressWithTotalUnitCount:100];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[self getLastView] animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.progressObject = progress;
    hud.label.text = text;
    
    return progress;
}

+ (UIViewController*)topViewController {
    UIViewController *resultVC = [self _topViewController:[[UIApplication sharedApplication].keyWindow rootViewController]];
    while (resultVC.presentedViewController) {
        resultVC = [self _topViewController:resultVC.presentedViewController];
    }
    return resultVC;
}

+ (UIViewController *)_topViewController:(UIViewController *)vc {
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [self _topViewController:[(UINavigationController *)vc topViewController]];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        return [self _topViewController:[(UITabBarController *)vc selectedViewController]];
    } else {
        return vc;
    }
    return nil;
}

+ (UIView *)getLastView{
    __block id lastView;
    [[UIApplication sharedApplication].windows enumerateObjectsUsingBlock:^(__kindof UIWindow * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isMemberOfClass:[UIWindow class]]) {
            lastView = obj;
        }
    }];
    return lastView;
}

/**
 *  隐藏ProgressHUD
 */
+ (void)hideHUD {
    [self hideHUDForView:[self getLastView]];
}

+ (void)hideHUDForView:(UIView*)view {
    [self hideHUDForView:view animated:NO];
    if (_hud) {
        [_hud hideAnimated:YES];
        _hud = nil;
    }
}

@end
