//
//  RightBarButtonItem.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RightBarButtonItem : UIBarButtonItem

@property(nonatomic,strong) UIButton *btn;


+ (RightBarButtonItem *)itemRightItemWithIcon:(UIImage *)icon;
+ (RightBarButtonItem *)itemRightItemWithTitle:(NSString *)title;

-(id)initWithTitle:(NSString *)title;
-(void)intiWithImage:(UIImage *)icon;


-(void)setTitleColor:(UIColor *)color;
-(void)setImageColor:(UIColor *)color;

@end
