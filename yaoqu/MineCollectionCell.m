//
//  MineCollectionCell.m
//  yaoqu
//
//  Created by ridchan on 2017/7/31.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineCollectionCell.h"

@implementation MineCollectionCell

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}


-(void)commonInit{
    self.backgroundColor = [UIColor whiteColor];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_centerY).offset(-5);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(25);
    }];
    
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_centerY).offset(5);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
    }];
    
    [self.detailLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.label.mas_bottom).offset(5);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
    }];
}

-(UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc]init];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_imageView];
    }
    return _imageView;
}

-(UILabel *)label{
    if (!_label) {
        _label = [ControllerHelper autoFitLabel];
        _label.font = SystemFont(14);
        _label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_label];
    }
    return _label;
}

-(UILabel *)detailLbl{
    if (!_detailLbl) {
        _detailLbl = [ControllerHelper autoFitLabel];
        _detailLbl.textAlignment = NSTextAlignmentCenter;
        _detailLbl.font = DetailFont;
        _detailLbl.textColor = AppGray;
        [self addSubview:_detailLbl];
    }
    return _detailLbl;
}

@end
