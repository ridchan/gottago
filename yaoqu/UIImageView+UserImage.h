//
//  UIImageView+UserImage.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIImageView (UserImage)

-(void)setImageName:(NSString *)name placeholder:(UIImage *)placeholder;

@end
