//
//  UserImageEditor.h
//  yaoqu
//
//  Created by ridchan on 2017/6/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserImageEditorLayout : UICollectionViewFlowLayout


@end

@interface UserImageEditorCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView *imageView;

@end

@interface UserImageEditor : UIView

@property(nonatomic,strong) NSMutableArray *datas;
@property(nonatomic,strong) NSString *dataCount;
@property(nonatomic) BOOL hideAction;


@end
