//
//  TravelEnrollContactView.h
//  yaoqu
//
//  Created by ridchan on 2017/7/17.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TravelEnrollContactView : UIView

@property(nonatomic,strong) NSMutableArray *contacts;
@property(nonatomic,strong) NSString *contactNum;


@end
