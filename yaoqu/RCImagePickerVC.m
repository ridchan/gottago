//
//  RCImagePickerVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/26.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "RCImagePickerVC.h"

#import "LLAuthorizationDeniedController.h"
#import "LLAlbumListController.h"
#import "LLAssetListController.h"
#import "LLAssetManager.h"
#import "UIKit+LLExt.h"
#import "LLUtils.h"
#import "LLImagePickerConfig.h"
#import "RCAlbumListVC.h"
#import "RCAssetListVC.h"

static NSString *lastAssertGroupIdentifier;

@interface RCImagePickerVC ()

@property (nonatomic) RCAlbumListVC *albumVC;

@end

@implementation RCImagePickerVC

- (instancetype)init {
    self = [super init];
    if (self) {
//        self.navigationBar.translucent = YES;
//        self.navigationBar.barAlpha = DEFAULT_NAVIGATION_BAR_ALPHA;
    }
    
    return self;
}

- (void)chechAuthorizationStatus {
    WEAK_SELF;
    LLCheckAuthorizationCompletionBlock block = ^(LLAuthorizationType type) {
        if (!weakSelf)return;
        
        switch (type) {
            case kLLAuthorizationTypeAuthorized:
            {
                //ImagePicker打开时尚未获取照片库权限，请求权限后用户允许访问照片库
                if (weakSelf.albumVC) {
                    if ([LLUtils canUsePhotiKit])
                        [weakSelf fetchAlbumData];
                }else {
                    //ImagePicker打开时就获取了访问照片库的权限
                    weakSelf.albumVC = [[RCAlbumListVC alloc] init];
                    weakSelf.albumVC.pickerType = weakSelf.pickerType;
                    weakSelf.albumVC.navigationItem.title = @"返回";
                    
                    RCAssetListVC *assetListVC = [[RCAssetListVC alloc] init];
                    assetListVC.groupModel = nil;
                    assetListVC.pickerType = weakSelf.pickerType;
                    [weakSelf setViewControllers:@[weakSelf.albumVC, assetListVC] animated:NO];
                    
                    [weakSelf fetchAlbumData];
                }
                
            }
                break;
            case kLLAuthorizationTypeDenied:
            case kLLAuthorizationTypeRestricted:
            {
                LLAuthorizationDeniedController *vc = [[LLAuthorizationDeniedController alloc] initWithNibName:nil bundle:nil];
                [weakSelf setViewControllers:@[vc] animated:NO];
            }
                break;
            case kLLAuthorizationTypeNotDetermined:
            {
                weakSelf.albumVC = [[RCAlbumListVC alloc] init];
                weakSelf.albumVC.pickerType = weakSelf.pickerType;
                [weakSelf setViewControllers:@[weakSelf.albumVC] animated:NO];
                if (![LLUtils canUsePhotiKit])
                    [weakSelf fetchAlbumData];
            }
                break;
            default:
                break;
        }
    };
    
    [[LLAssetManager sharedAssetManager] chechAuthorizationStatus:block];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [LLAssetManager sharedAssetManager].pickerType = self.pickerType;
    [self chechAuthorizationStatus];
}

//该方法异步获取全部相册
- (void)fetchAlbumData {
    WEAK_SELF;
    LLFetchAssetsGroupsSuccessBlock successBlock = ^() {
        if ([[weakSelf.childViewControllers lastObject] isKindOfClass:[RCAssetListVC class]]) {
            LLAssetsGroupModel *model = [[LLAssetManager sharedAssetManager] assetsGroupModelForLocalIdentifier:lastAssertGroupIdentifier];
            
            RCAssetListVC *assetListVC = (RCAssetListVC *)[weakSelf.childViewControllers lastObject];
            assetListVC.groupModel = model;
            [assetListVC fetchData];
        }else {
            [weakSelf.albumVC refresh];
        }
    };
    
    LLFetchAssetsGroupsFailureBlock failureBlock = ^(NSError * _Nullable error) {
    };
    
    [[LLAssetManager sharedAssetManager] fetchAllAssetsGroups:successBlock failureBlock:failureBlock];
}

- (void)didFinishPickingImages:(NSArray<LLAssetModel *> *)assets WithError:(NSError *)error assetGroupModel:(LLAssetsGroupModel *)assetGroupModel {
    lastAssertGroupIdentifier = assetGroupModel.localIdentifier;
    
    [self.pickerDelegate imagePickerController:self didFinishPickingImages:assets withError:error];
    [self cleanAfterDismiss];
}

- (void)didCancelPickingImages {
    [self.pickerDelegate imagePickerControllerDidCancel:self];
    
    [self cleanAfterDismiss];
}

- (void)didFinishPickingVideo:(NSString *)videoPath assetGroupModel:(LLAssetsGroupModel *)assetGroupModel {
    lastAssertGroupIdentifier = assetGroupModel.localIdentifier;
    [self.pickerDelegate imagePickerController:self didFinishPickingVideo:videoPath];
    
    [self cleanAfterDismiss];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)cleanAfterDismiss {
    [LLAssetManager destroyAssetManager];
    [LLAssetModel finalize_LL];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
