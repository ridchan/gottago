//
//  RouteController.m
//  yaoqu
//
//  Created by ridchan on 2017/6/16.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "RouteController.h"
#import "LoginVC.h"
#import "BaseTabBarController.h"
#import "NewDynamicVC.h"
#import "RCUserCacheManager.h"
#import "BaseNavigationController.h"
#import "LLClientManager.h"
#import "LLChatManager.h"
#import "LLMessageCacheManager.h"
#import "LLUtils.h"
#import "UserModel.h"
#import "AccountLoginVC.h"


@interface RouteController (){
    UINavigationController *nav;
}

@property(nonatomic,strong) LLChatViewController *chatViewController;
@property(nonatomic,strong) BaseNavigationController *prepareNav;

@end

@implementation RouteController
    
    
CREATE_SHARED_MANAGER(RouteController)

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.prepareNav = [[BaseNavigationController alloc]init];
        
    }
    return self;
}

    
-(UIViewController *)rootViewController{
    UIViewController *vc = nil;
    
    BOOL navBarHidden = NO;
    [LLClientManager sharedManager];
    if ([RCUserCacheManager sharedManager].currentUser){
        vc = [[BaseTabBarController alloc]init];
        [self setViewControllers:(BaseTabBarController *)vc];
        [[RCUserCacheManager sharedManager] huanXinAutoLogin];
        navBarHidden = YES;
    }else{
//        vc = [[LoginVC alloc]init];
        vc = [[AccountLoginVC alloc]init];
        navBarHidden = NO;
    }
//    return vc;
    nav = [[BaseNavigationController alloc]initWithRootViewController:vc];
    nav.navigationBarHidden = navBarHidden;
    return nav;
}

-(UINavigationController *)currentNav{
    if ([[[nav viewControllers] firstObject] isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tab = (UITabBarController *)[[nav viewControllers] firstObject];
        UINavigationController *currentNav = tab.viewControllers[tab.selectedIndex];
        return currentNav;
    }
    return nil;
}

-(void)openMainController{
    BaseTabBarController *baseVC = [[BaseTabBarController alloc]init];
    [self setViewControllers:(BaseTabBarController *)baseVC];
    nav.navigationBarHidden = YES;
    [nav setViewControllers:@[baseVC] animated:YES];
}

-(void)openLoginController{
//    LoginVC *vc = [[LoginVC alloc]init];
    AccountLoginVC *vc = [[AccountLoginVC alloc]init];
    nav.navigationBarHidden = NO;
    [nav setViewControllers:@[vc] animated:YES];
}

-(void)openNewDynamicController{
    NewDynamicVC *vc = [[NewDynamicVC alloc]init];
//    BaseNavigationController *navc = [[BaseNavigationController alloc]initWithRootViewController:vc];
    [self.prepareNav setViewControllers:@[vc]];
    [[self currentNav] presentViewController:self.prepareNav animated:YES completion:NULL];
}

-(void)openSetting{
    id vc = [[NSClassFromString(@"MineSettingVC") alloc]init];
    [[self currentNav] pushViewController:vc animated:YES];
}

-(void)openHomePageVC:(UserModel *)userModel{
    ;
    if ([userModel.member_id integerValue] == [[RCUserCacheManager sharedManager].currentUser.member_id integerValue]) {
        [self openClassVC:@"UserHomePageVC"];
    }else{
        [self openClassVC:@"OtherHomePageVC" withObj:userModel];
    }
}

-(void)openClassVC:(NSString *)vcName{
    id vc = [[NSClassFromString(vcName) alloc]init];
    [[self currentNav] pushViewController:vc animated:YES];
}

-(void)openClassVC:(NSString *)vcName withObj:(id)obj{
    id vc = [[NSClassFromString(vcName) alloc]init];
    [vc performSelector:@selector(setParamObj:) withObject:obj];
    [[self currentNav] pushViewController:vc animated:YES];
}


-(void)pushVC:(UIViewController *)vc{
    [[self currentNav] pushViewController:vc animated:YES];
}

-(void)presentVC:(UIViewController *)vc{
    [[self currentNav] presentViewController:vc animated:YES completion:NULL];
}

-(void)presentNavVC:(NSString *)vcName{
    id vc = [[NSClassFromString(vcName) alloc]init];
    UINavigationController *navc = [[UINavigationController alloc]initWithRootViewController:vc];
    [[self currentNav] presentViewController:navc animated:YES completion:NULL];
}

- (void)chatWithContact:(NSString *)userName userName:(NSString *)name{
    
    
    
    LLConversationModel *conversationModel = [[LLChatManager sharedManager]
                                              getConversationWithConversationChatter:userName
                                              conversationType:kLLConversationTypeChat];
    

    
    [[LLMessageCacheManager sharedManager] prepareCacheWhenConversationBegin:conversationModel];
    
    self.chatViewController.conversationModel = conversationModel;
    [self.chatViewController fetchMessageList];
    [self.chatViewController refreshChatControllerForReuse];
//    [self.chatViewController.conversationModel getInfo];
    [[self currentNav] pushViewController:self.chatViewController animated:YES];
    
    
}




- (void)chatWithConversationModel:(LLConversationModel *)conversationModel {
//    for (UIViewController *vc in self.viewControllers) {
//        if ([vc isKindOfClass:[LLChatViewController class]]) {
//            return;
//        }
//    }
//    
//    if([LLClientManager sharedManager].error){
//        [MBProgressHUD showMessage:@"连接环信服务器失败" delay:1.0];
//        return;
//    }
    
    [[LLMessageCacheManager sharedManager] prepareCacheWhenConversationBegin:conversationModel];
    
    self.chatViewController.conversationModel = conversationModel;
    [self.chatViewController fetchMessageList];
    [self.chatViewController refreshChatControllerForReuse];
    
    [[self currentNav] pushViewController:self.chatViewController animated:YES];
    
}

- (LLChatViewController *)chatViewController {
    if (!_chatViewController) {
        _chatViewController = [[LLUtils mainStoryboard] instantiateViewControllerWithIdentifier:SB_CHAT_VC_ID];
        _chatViewController.hidesBottomBarWhenPushed = YES;
        _chatViewController.view.frame = [UIScreen mainScreen].bounds;
    }
    
    return _chatViewController;
}



#pragma mark -
#pragma mark view controller


- (void)setViewControllers:(BaseTabBarController *)tbc {
    
    
    NSArray *dataAry = @[@{@"title":LS(@"旅游"),@"image":@"tab_trip_gray",@"selectImage":@"tab_trip_blue",@"class":@"TravelHomePageWebVC"},
                         @{@"title":LS(@"发现"),@"image":@"tab_discovery_gray",@"selectImage":@"tab_discovery_blue",@"class":@"DiscoverVC"},
                         @{@"title":@"",@"image":@"",@"selectImage":@"",@"class":@"UIViewController"},
                         @{@"title":LS(@"聊天"),@"image":@"tab_chat_gray",@"selectImage":@"tab_chat_blue",@"class":@"ConversationListVC"},
                         @{@"title":LS(@"我的"),@"image":@"tab_mine_gray",@"selectImage":@"tab_mine_blue",@"class":@"MineVC"}
                         ];
    
    
    for (NSDictionary *dataDic in dataAry) {
        //每个tabar的数据
        Class classs = NSClassFromString(dataDic[@"class"]);
        NSString *title = dataDic[@"title"];
        NSString *imageName = dataDic[@"image"];
        NSString *selectedImage = dataDic[@"selectedImage"];
        //        NSString *badgeValue = dataDic[@"badgeValue"];
        
        [tbc addChildViewController:[self ittemChildViewController:classs title:title imageName:imageName selectedImage:selectedImage badgeValue:@"0"]];
    }
}

- (BaseNavigationController *)ittemChildViewController:(Class)classs title:(NSString *)title imageName:(NSString *)imageName selectedImage:(NSString *)selectedImage badgeValue:(NSString *)badgeValue {
    
    BaseViewController *vc = [[classs alloc]init];
    vc.tabBarItem.image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    vc.tabBarItem.selectedImage = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    //起点-8图标才会到顶，然后加上计算出来的y坐标
    //    float origin = -9 + 6;
    //    vc.tabBarItem.imageInsets = UIEdgeInsetsMake(origin, 0, -origin,0);
    //    vc.tabBarItem.titlePositionAdjustment = UIOffsetMake(-2 + 8, 2-8);
    //title设置
    [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName:[UIFont systemFontOfSize:10]} forState:UIControlStateNormal];
    [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor orangeColor],NSFontAttributeName:[UIFont systemFontOfSize:10]} forState:UIControlStateSelected];
    vc.tabBarItem.title = title;
    vc.title = title;
    //小红点
    vc.tabBarItem.badgeValue = badgeValue.intValue > 0 ? badgeValue : nil;
    //导航
    BaseNavigationController *subNav = [[BaseNavigationController alloc] initWithRootViewController:vc];
    subNav.navigationBar.topItem.title = title;
    [subNav.rootVcAry addObject:classs];
    return subNav;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
