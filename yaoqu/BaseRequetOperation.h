//
//  BaseRequetOperation.h
//  QBH
//
//  Created by 陳景雲 on 2016/12/28.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "Responese.h"
#import <MJExtension/MJExtension.h>
#import "MsgModel.h"

typedef void(^RequestDataSuccess)(NSURLSessionDataTask *task, id responseObject,Responese *respone);//成功
typedef void(^RequestDataFailure)(NSURLSessionDataTask *task, NSString *error,Responese *respone);  //失败


@interface BaseRequetOperation : NSObject



@property (nonatomic,strong)NSOperationQueue *queue;
@property (nonatomic,strong)AFHTTPSessionManager *manager;
@property (nonatomic,strong)NSURLSessionDataTask *task;




+(BOOL)isRequestDataSuccess:(Responese *)respone;//判断是否成功
-(BOOL)beforeExecute:(Responese*)response;//检测网络状态


-(void)execute_POST:(NSString*)uri params:(NSDictionary*)params RequestSuccess:(RequestDataSuccess )_success failure:(RequestDataFailure )_failure; //Post请求

-(void)paramList_POST:(NSString*)uri params:(NSDictionary*)params RequestSuccess:(RequestDataSuccess )_success failure:(RequestDataFailure )_failure; //加密解密过程的Post请求


-(void)cancelAllOperations; //取消请求

//判断 IP 是否可用

- (void)isConnection;


@end
