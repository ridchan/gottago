//
//  UserNameEditVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/22.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserNameEditVC.h"

@interface UserNameEditVC ()

@property(nonatomic,strong) UITextField *nameFld;

@end

@implementation UserNameEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self commonInit];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)commonInit{
    self.title = LS(@"修改昵称");
    
    self.editModel = self.paramObj;
    
    [self setRightItemWithTitle:LS(@"保存") selector:@selector(save)];
    
    [self.nameFld mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(74);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.height.mas_equalTo(44);
    }];
    
    self.nameFld.text = self.editModel.username;
//    RAC(self.editModel,username) = [self.nameFld.rac_textSignal distinctUntilChanged];
}


-(UITextField *)nameFld{
    if (!_nameFld) {
        _nameFld = [[UITextField alloc]init];
        _nameFld.backgroundColor = [UIColor whiteColor];
        _nameFld.leftViewMode = UITextFieldViewModeAlways;
        _nameFld.clearButtonMode = UITextFieldViewModeWhileEditing;
        _nameFld.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 15, 1)];
        [self.view addSubview:_nameFld];
    }
    return _nameFld;
}


-(void)save{
    self.editModel.username = self.nameFld.text;
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
