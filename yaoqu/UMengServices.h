//
//  UMengServices.h
//  QBH
//
//  Created by 陳景雲 on 2017/1/8.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UMengServices : NSObject<UIApplicationDelegate>


-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

@end
