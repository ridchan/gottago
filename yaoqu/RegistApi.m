//
//  RegistApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/15.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "RegistApi.h"
#import <JPUSHService.h>
@interface RegistApi()
@end

@implementation RegistApi

-(id)initWithPhone:(NSString *)phone code:(NSString *)code name:(NSString *)name sex:(NSString *)sex openid:(NSString *)openid unionid:(NSString *)unionid image:(NSString *)image{
    if (self = [super init]) {
 
        NSLog(@"abcde %@",[JPUSHService registrationID]);
        
        [self.params setValue:phone forKey:@"phone"];
        [self.params setValue:code  forKey:@"code"];
        [self.params setValue:name forKey:@"username"];
        [self.params setValue:sex forKey:@"sex"];
        [self.params setValue:openid forKey:@"openid"];
        [self.params setValue:unionid forKey:@"unionid"];
        [self.params setValue:image forKey:@"image"];
        [self.params setValue:@"9999" forKey:@"registration_id"];
    }
    return self;
}

-(NSString *)requestUrl{
    return RegistUrl;
}



@end
