//
//  ShareView.m
//  shiyi
//
//  Created by 陳景雲 on 16/5/1.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import "ShareView.h"
//#import "MBProgressHUD+View.h"
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <UMSocialCore/UMSocialCore.h>
#import "CommonConfigManager.h"
#import <UMSocialCore/UMSocialCore.h>
#import "RouteController.h"
#define ToolBarHeight 180
#define ToolButtonGap 15
#define CancelButtonHeight 50

@interface ShareView(){
    CGFloat buttonGap;
}

@end

@implementation ShareView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = RGBA(0, 0, 0, 0.7);
        self.frame = CGRectMake(0, 0, APP_WIDTH, APP_HEIGHT);
    }
    return self;
}

-(void)createViews{
    
    
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.9];
    
    [self addSubview:self.bgView];
    
    
    UIView *dismissView = [[UIView alloc]init];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeBtnClick:)];
    [dismissView addGestureRecognizer:tap];
    [self addSubview:dismissView];
    
    
    CGFloat width = (APP_WIDTH - ToolButtonGap * 6) / 5;
    
    buttonGap = (APP_WIDTH - width * 4.0) / 5.0;
    
    CGSize size = CGSizeMake(width,width + 20);
    
//    UIButton *shareBtn1 = [self createButton:@"icon_share_app" title:@"分享给朋友" size:size];
//    shareBtn1.tag = ShareType_HuanXin;

    
    UIButton *shareBtn2 =[self createButton:@"icon_share_qq" title:@"分享到QQ" size:size];
    shareBtn2.tag = ShareType_QQ;
    UIButton *shareBtn3 = [self createButton:@"icon_share_wechat" title:@"分享到微信" size:size];
    shareBtn3.tag = ShareType_WX;
    UIButton *shareBtn4 = [self createButton:@"icon_share_time_line" title:@"分享到朋友圈" size:size];
    shareBtn4.tag = ShareType_Friend;
    
    UIButton *shareBtn5 = [self createButton:@"icon_share_link" title:@"复制链接" size:size];
    shareBtn5.tag = ShareType_Copy;
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setBackgroundColor:[UIColor whiteColor]];
    [cancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(dismiss:) forControlEvents:UIControlEventTouchUpInside];
    [cancelBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    
    [self.bgView addSubview:cancelBtn];
    
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bgView.mas_left);
        make.right.equalTo(self.bgView.mas_right);
        make.bottom.equalTo(self.bgView.mas_bottom);
        make.height.mas_equalTo(CancelButtonHeight);
    }];
    
    
    [dismissView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.top.equalTo(self.mas_top);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.bgView.mas_top);
    }];
    
    

    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(ToolBarHeight);
    }];
    
    
    
//    [shareBtn1 mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(self.bgView).offset(-CancelButtonHeight / 2);
//    }];
    
    [shareBtn2 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.bgView).offset(-CancelButtonHeight / 2);
    }];
    
    [shareBtn3 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.bgView).offset(-CancelButtonHeight / 2);
    }];
    
    [shareBtn4 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.bgView).offset(-CancelButtonHeight / 2);
    }];
    
    [shareBtn5 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.bgView).offset(-CancelButtonHeight / 2);
    }];

    
    
//    [shareBtn1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.bgView.mas_left).offset(ToolButtonGap);
//    }];
    
    [shareBtn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bgView.mas_left).offset(buttonGap);
//        make.left.equalTo(shareBtn1.mas_right).offset(ToolButtonGap);
    }];
    
    [shareBtn3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shareBtn2.mas_right).offset(buttonGap);
    }];
    
    [shareBtn4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shareBtn3.mas_right).offset(buttonGap);
    }];

    
    [shareBtn5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.bgView.mas_right).offset(-buttonGap);
    }];
    
    
}

-(UIButton *)createShareButton:(NSString *)BtnTittle size:(CGSize)size{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:BtnTittle forState:UIControlStateNormal];
    button.layer.masksToBounds = YES;
    button.layer.cornerRadius = 1.5;
    button.layer.borderWidth = 1.0;
    [button setTitleColor:AppBlue forState:UIControlStateNormal];
    button.layer.borderColor = AppBlue.CGColor;
    
    [button addTarget:self action:@selector(itemBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:button];
    
    [button mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(size);
    }];
    button.hidden = YES;
    return button;
}

-(UIButton *)createButton:(NSString *)imageName title:(NSString *)title size:(CGSize)size{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.clipsToBounds = NO;
    
    UIImageView *bgImage = [[UIImageView alloc]init];
    bgImage.image = [UIImage imageNamed:@"share_btn_bg"];
    [button addSubview:bgImage];
    
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imageName]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [button addSubview:imageView];
    
    UILabel *label = [[UILabel alloc]init];
    label.textColor = [UIColor darkGrayColor];
    label.font = [UIFont systemFontOfSize:10];
    label.text = title;
    label.textAlignment = NSTextAlignmentCenter;
    [button addSubview:label];
    
    [bgImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(button.mas_left);
        make.top.equalTo(button.mas_top);
        make.right.equalTo(button.mas_right);
        make.height.equalTo(imageView.mas_width);
    }];
    

    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(button.mas_left);
        make.top.equalTo(button.mas_top);
        make.right.equalTo(button.mas_right);
        make.height.equalTo(imageView.mas_width);
    }];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(button.mas_centerX);
        make.bottom.equalTo(button.mas_bottom);
        make.top.equalTo(imageView.mas_bottom);
    }];
    
    

    
    [button addTarget:self action:@selector(shareBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:button];
    
    [button mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(size);
    }];
    
    
    return button;

}

-(void)closeBtnClick:(id)sender{
    [self dismiss:nil];
}

-(void)showMessage:(NSString *)msg{
//    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = msg;
    hud.label.textColor = [UIColor whiteColor];
    [hud hideAnimated:YES afterDelay:1.0];

}

-(void)shareBtnClick:(UIButton *)button{
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    //创建网页内容对象
//    NSString* thumbURL =  [_shareInfo objectForKey:@"image"];
    UMShareObject *shareObject = nil;
    //设置网页地址
    
    
    if ([_shareInfo objectForKey:@"link"]){
        UMShareWebpageObject *webObject = [UMShareWebpageObject shareObjectWithTitle:[_shareInfo objectForKey:@"title"] descr:[_shareInfo objectForKey:@"desc"] thumImage:[_shareInfo objectForKey:@"imgUrl"]];
        webObject.webpageUrl =  [_shareInfo objectForKey:@"link"];
        shareObject = webObject;
    }else{
        UMShareObject *shareobj = [UMShareObject shareObjectWithTitle:[_shareInfo objectForKey:@"title"] descr:[_shareInfo objectForKey:@"desc"] thumImage:[_shareInfo objectForKey:@"img"]];
        shareObject = shareobj;
    }
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    
    
    UMSocialPlatformType platformType;
    
    switch (button.tag) {
        case ShareType_QQ:
            platformType = UMSocialPlatformType_QQ;
            break;
        case ShareType_WX:
            platformType = UMSocialPlatformType_WechatSession;
            break;
        case ShareType_QQZone:
            platformType = UMSocialPlatformType_Qzone;
            break;
        case ShareType_Friend:
            platformType = UMSocialPlatformType_WechatTimeLine;
            break;
        default:
            platformType = UMSocialPlatformType_UnKnown;
            [self itemBtnClick:button];
            return;
            break;
    }

        //调用分享接口
        [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:[[RouteController sharedManager] currentNav] completion:^(id data, NSError *error) {

            if (error) {
                UMSocialLogInfo(@"************Share fail with error %@*********",error);
            }else{
                if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                    UMSocialShareResponse *resp = data;
                    //分享结果消息
                    UMSocialLogInfo(@"response message is %@",resp.message);
                    //第三方原始返回的数据
                    UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                    
                }else{
                    UMSocialLogInfo(@"response data is %@",data);
                }
            }
            [self showMessage:error.description];
        }];
}

-(void)shareBtnClick2:(UIButton *)button{
    
    
    if (button.tag == ShareType_Sina){
        [self showMessage:@"新浪微博暂未开通"];
        return;
    }
    
    UIImage *showImage = nil;
    if ([[self.shareInfo objectForKey:@"img"] isKindOfClass:[NSString class]]){
        
        NSString *link = [[CommonConfigManager sharedManager].hostModel.image_host stringByAppendingPathComponent:[self.shareInfo objectForKey:@"img"]];
        NSURL *url = [NSURL URLWithString:link];
        NSData *data = [[NSData alloc]initWithContentsOfURL:url];
        UIImage *image = [UIImage imageWithData:data];
        if (image){
            UIImage *newImg = [image imageCompressForSize:image targetSize:CGSizeMake(50, 50)];
            showImage = newImg;
        }else{
//            showImage = [UIImage imageNamed:@"Icon-60"];
        }
        
        
    }else{
        
//        UIImage *newImg = [BaseHelp imageCompressForSize:[self.shareInfo objectForKey:@"img"] targetSize:CGSizeMake(50, 50)];
//        showImage = newImg;
        
    }

    
    if (button.tag == ShareType_WX || button.tag == ShareType_Friend){
        if (![WXApi isWXAppInstalled]){
            [self showMessage:@"微信未安装"];
            return;
        }
        WXMediaMessage *message = [WXMediaMessage message];
        

        [message setThumbImage:showImage];
    
        
        
        [message setTitle:[self.shareInfo objectForKey:@"title"]];
        [message setDescription:[self.shareInfo objectForKey:@"desc"]];
        
        WXWebpageObject *webObj = [[WXWebpageObject alloc] init];
        webObj.webpageUrl = [self.shareInfo objectForKey:@"link"];
        
        message.mediaObject = webObj;
        
        SendMessageToWXReq *req = [[SendMessageToWXReq alloc]init];
        req.bText = NO;
        req.message = message;
        
        if (button.tag == ShareType_WX)
            req.scene = WXSceneSession;
        else
            req.scene = WXSceneTimeline;
        
        
        [WXApi sendReq:req];
        [self dismiss:^(id obj) {
            
        }];
        
        
    }else if (button.tag == ShareType_QQ || button.tag == ShareType_QQZone){
        if (![QQApiInterface isQQInstalled]){
            [self showMessage:@"QQ未安装"];
            return;;
        }
        NSString *url = [self.shareInfo objectForKey:@"link"];
        
        UIImage *image = showImage;
        
        NSString *title = [self.shareInfo objectForKey:@"title"];
        if ([title length] == 0) title = @"成双成对";
        
        QQApiNewsObject *newsObj = [QQApiNewsObject objectWithURL:[NSURL URLWithString:url] title:title description:[self.shareInfo objectForKey:@"desc"] previewImageData:UIImageJPEGRepresentation(image, 1.0)];
        
        
        __block typeof(newsObj) tempObj = newsObj;
        
        

        if (button.tag == ShareType_QQ){
            SendMessageToQQReq *req = [SendMessageToQQReq reqWithContent:tempObj];
            [QQApiInterface sendReq:req];
            
            
            
        }else{
            SendMessageToQQReq *req = [SendMessageToQQReq reqWithContent:tempObj];
            [QQApiInterface SendReqToQZone:req];
            
        }
        [self dismiss:^(id obj) {
            
        }];
        
    }else{
        [self itemBtnClick:button];
    }

}

-(void)itemBtnClick:(UIButton *)button{
//    __weak typeof(self) weakSelf = self;
//    [self dismiss:^(id obj) {
    
        if (self.handleBlock){
            self.handleBlock(@(button.tag ));
        }
    
    [self dismiss:nil];
//    }];
    
}


-(void)show{
    
    [self createViews];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview:self];
    
    __weak typeof(self) weakSelf = self;
    
    self.alpha = 0.3;
    self.bgView.transform = CGAffineTransformMakeTranslation(0, ToolBarHeight);
    [UIView animateWithDuration:0.25 animations:^{
        weakSelf.alpha = 1.0;
        weakSelf.bgView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
    
}

-(void)dismiss:(BaseBlock)block{
    __weak typeof(self) weakSelf = self;
    
    [UIView animateWithDuration:0.25 animations:^{
        weakSelf.bgView.transform = CGAffineTransformMakeTranslation(0, ToolBarHeight);
        weakSelf.alpha = 0;
    } completion:^(BOOL finished) {
//        if (block) block(nil);
        [weakSelf removeFromSuperview];
    }];
}

@end
