//
//  DynamicCommentListVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/30.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicCommentListVC.h"
#import "DynamicCommentListCell.h"
#import "DynamicCommentListHeader.h"
#import "QZTopTextView.h"
#import "DynamicCommentApi.h"
#import "DynamicDetailApi.h"
#import "CommentApi.h"
#import "ChatManagerView.h"
@interface DynamicCommentListVC ()<UITableViewDelegate,UITableViewDataSource,QZTopTextViewDelegate,ChatManagerViewDelegate>


@property(nonatomic,strong) DynamicCommentListHeader *header;
@property(nonatomic,strong) UIView *bottomView;
@property(nonatomic,strong) QZTopTextView *textView;
@property(nonatomic,strong) ChatManagerView *chatView;

@end

@implementation DynamicCommentListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = LS(@"评论详情");
    
    [self wr_setNavBarShadowImageHidden:NO];
    
    _textView =[QZTopTextView topTextView];
    _textView.delegate = self;
    [self.view addSubview:_textView];
    
    self.header = [[DynamicCommentListHeader alloc]init];
    self.header.model = self.model;
//    [self.view addSubview:self.header];
    
    
    self.tableView.tableHeaderView = self.header;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.tableView registerClass:[DynamicCommentListCell class] forCellReuseIdentifier:@"DynamicCommentListCell"];
    
//    [self.header mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.view.mas_left);
//        make.top.equalTo(self.view.mas_top);
//        make.right.equalTo(self.view.mas_right);
//    }];
    
    
    [self.view addSubview:self.chatView];
    self.chatView.frame = CGRectMake(0, APP_HEIGHT - 44, APP_WIDTH, APP_HEIGHT);
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(50);
    }];
    
    
    [self getAllComment];
    
    self.isloading = NO;
    
//    self.tableView.tableHeaderView = self.header;
    

    // Do any additional setup after loading the view.
}


-(void)getAllComment{
    WEAK_SELF;
    
    [[[CommentApi alloc]initWitObject:@{@"dynamic_id":self.dynamic_id,@"comment_id":self.model.comment_id}] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            weakSelf.model.comment_data = [DynamicCommentModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
            [weakSelf.tableView reloadData];
        }
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView{
    return YES;
}

#pragma mark -
#pragma mark  lazy layout

-(UIView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = [UIColor whiteColor];
        
        UITextField *textField = [[UITextField alloc]init];
        textField.placeholder = @"说一下";
        textField.enabled = NO;
        [_bottomView addSubview:textField];
        
        [textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_bottomView).insets(UIEdgeInsetsMake(5, 10, 5, 10));
        }];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(bottomTap:)];
        [_bottomView addGestureRecognizer:tap];
        [self.view addSubview:_bottomView];
    }
    return _bottomView;
}


-(void)bottomTap:(id)sender{
    [self.chatView.inputView.textField becomeFirstResponder];
//    [self.textView.countNumTextView becomeFirstResponder];
}

#pragma mark -
#pragma mark table view




-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    id model = self.model.comment_data[indexPath.row];
    return [tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[DynamicCommentListCell class] contentViewWidth:APP_WIDTH];
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.model.comment_data.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    DynamicCommentListCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"DynamicCommentListCell" forIndexPath:indexPath];
    cell.model = self.model.comment_data[indexPath.row];
    return cell;
}



-(void)sendComment{
    WEAK_SELF;
    NSString *text = self.textView.countNumTextView.text;
    self.textView.countNumTextView.text = @"";
    [self.textView.countNumTextView resignFirstResponder];
    
    NSDictionary *dict = @{@"dynamic_id":self.dynamic_id,
                           @"comment_id":self.model.comment_id,
                           @"desc":text
                           };
    CommentApi *api = [[CommentApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            [weakSelf getAllComment];
        }else{
            [LLUtils showCenterTextHUD:info.message];
        }
    }];
    
//    [[[DynamicCommentApi alloc]initWithDynamic_id:self.dynamic_id comment_id:self.model.comment_id desc:text] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
//        if (info.state) {
////            [weakSelf getAllComment];
//        }
//    }];
    
}

#pragma mark -
#pragma mark  chat view

-(ChatManagerView *)chatView{
    if (!_chatView) {
        _chatView = [[ChatManagerView alloc] initWithType:ChatViewType_Publish];
        _chatView.inputView.textField.placeholder = @"添加评论";
        _chatView.delegate = self;
    }
    return _chatView;
}

-(void)dismissWithInfo:(id)obj{
    _chatView.inputView.textField.placeholder = @"添加评论";
}

-(void)sendWithInfo:(NSString *)message{
    
    self.chatView.inputView.textField.text = @"";
    
    WEAK_SELF;
    NSString *text = message;
    self.textView.countNumTextView.text = @"";
    [self.textView.countNumTextView resignFirstResponder];
    
    NSDictionary *dict = @{@"dynamic_id":self.dynamic_id,
                           @"comment_id":self.model.comment_id,
                           @"desc":text
                           };
    CommentApi *api = [[CommentApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            [weakSelf getAllComment];
        }else{
            [LLUtils showCenterTextHUD:info.message];
        }
    }];

    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
