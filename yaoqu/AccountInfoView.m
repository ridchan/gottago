//
//  AccountInfoView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/8.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "AccountInfoView.h"
#import "ImageTextField.h"
#import "SexChooseView.h"
#import "LLUtils.h"
#import "CommonApi.h"
#import "QiniuSDK.h"
#import "LoginApi.h"
#import "MemberApi.h"
#import "LoginApi.h"
#import "CommonApi.h"
#import "LoginInfoModel.h"
#import "AccessTokenModel.h"
#import "LLUtils.h"
#import "RCUserCacheManager.h"
#import "SRActionSheet.h"
#import <UMSocialCore/UMSocialCore.h>

@interface AccountInfoView()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>


@property(nonatomic,strong) UIImageView *userImage;
@property(nonatomic,strong) UILabel *tipLbl1;
@property(nonatomic,strong) UILabel *tipLbl2;
@property(nonatomic,strong) ImageTextField *nameFld;
@property(nonatomic,strong) SexChooseView *sexView;

@property(nonatomic,strong) UIImagePickerController *picker;



@end

@implementation AccountInfoView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}



-(void)commonInit{
    [self.userImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.mas_top).offset(20);
        make.size.mas_equalTo(CGSizeMake(100, 100));
    }];
    self.userImage.layer.cornerRadius = 50;
    self.userImage.layer.masksToBounds = YES;
    
    [self.tipLbl1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userImage.mas_bottom).offset(20);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    [self.nameFld mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tipLbl1.mas_top).offset(30);
        make.height.mas_equalTo(50);
        make.left.equalTo(self.mas_left).offset(60);
        make.right.equalTo(self.mas_right).offset(-60);
    }];
    
    [self.tipLbl2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameFld.mas_bottom).offset(60);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    [self.sexView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tipLbl2.mas_top).offset(30);
        make.centerX.equalTo(self.mas_centerX);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(APP_WIDTH / 2.0);
    }];
    
    [self.doneBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sexView.mas_bottom).offset(30);
        make.height.mas_equalTo(50);
        make.left.equalTo(self.mas_left).offset(20);
        make.right.equalTo(self.mas_right).offset(-20);
        make.bottom.equalTo(self.mas_bottom).offset(-50);
    }];
    
    RAC(self,postModel.username) = [self.nameFld.rac_textSignal distinctUntilChanged];
    
    
}




-(void)doneBtnClick:(id)sender{
    self.postModel.sex = [NSString intValue:self.sexView.sex];
    
    WEAK_SELF;
    __block MBProgressHUD *hud =  [LLUtils showActivityIndicatiorHUDWithTitle:nil];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        __block NSString *token = nil;
        
        [[[CommonApi alloc]initWithModel:@"common/uptoken" object:nil]startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            if (info.error == ErrorCodeType_None){
                token = [responseObj objectForKey:@"data"];
            }else{
                token = @"";
            }
        }];
        
        while (token == nil) {
            [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }
        
        
        
        NSString *key = [[NSString randomString] stringByAppendingString:@".jpg"];
        __block NSString *imageName = nil;
        [[[QNUploadManager alloc]init] putData:UIImageJPEGRepresentation(weakSelf.userImage.image, 1.0) key:key token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
            if (!info.error) {
                imageName = key;
            }else{
                imageName = @"";
            }
        } option:nil];
        
        while (imageName == nil) {
            [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }
        
        //        WEAK_SELF;
        weakSelf.postModel.image = imageName;
        LoginApi *loginApi = [[LoginApi alloc]initWitObject:weakSelf.postModel];
        loginApi.method = YTKRequestMethodPOST;
        
        
        [loginApi startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            [hud hideAnimated:NO];
            
            if (info.error != ErrorCodeType_None){
                [LLUtils showCenterTextHUD:info.message];
                return ;
            }
            LoginInfoModel *model = [LoginInfoModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
            
            [[RCUserCacheManager sharedManager]setCurrentLoginInfo:model];
            
            
            
            [[[CommonApi alloc]initWithModel:@"accessToken" object:model] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                
                AccessTokenModel *model = [AccessTokenModel objectWithKeyValues:responseObj];
                [[RCUserCacheManager sharedManager] setCurrentToken:model];
                
                [[[MemberApi alloc]initWitObject:responseObj] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                    
                    UserModel *userModel = [UserModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
                    [[RCUserCacheManager sharedManager] setCurrentUser:userModel];
                    [[RouteController sharedManager] openMainController];
                    
                }];
                
            }];
        }];
        
        
    });

}


-(UIImageView *)userImage{
    if (!_userImage) {
        _userImage = [[UIImageView alloc]init];
        _userImage.image = [UIImage imageNamed:@"ic_login_upload"];
        _userImage.backgroundColor = RGBA(0, 0, 0, 0.3);
        _userImage.contentMode = UIViewContentModeScaleAspectFill;
        _userImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _userImage.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
        [_userImage addGestureRecognizer:tap];
        [self addSubview:_userImage];
    }
    return _userImage;
}


-(ImageTextField *)nameFld{
    if (!_nameFld) {
        _nameFld = [ImageTextField imageText:@"ic_login_user" placeHolder:@"填写属于您的昵称"];
        [self addSubview:_nameFld];
    }
    return _nameFld;
}

-(UILabel *)tipLbl1{
    if (!_tipLbl1) {
        _tipLbl1 = [ControllerHelper autoFitLabel];
        _tipLbl1.textColor = [UIColor whiteColor];
        _tipLbl1.text = @"1.点击选择头像";
        [self addSubview:_tipLbl1];
    }
    return _tipLbl1;
}

-(UILabel *)tipLbl2{
    if (!_tipLbl2) {
        _tipLbl2 = [ControllerHelper autoFitLabel];
        _tipLbl2.textColor = [UIColor whiteColor];
        _tipLbl2.text = @"2.选择您的性别";
        [self addSubview:_tipLbl2];
    }
    return _tipLbl2;
}

-(SexChooseView *)sexView{
    if (!_sexView) {
        _sexView = [[SexChooseView alloc]init];
        [self addSubview:_sexView];
    }
    return _sexView;
}

-(NormalButton *)doneBtn{
    if (!_doneBtn) {
        _doneBtn = [NormalButton normalButton:@"注册完成"];
        [_doneBtn addTarget:self action:@selector(doneBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_doneBtn];
    }
    return _doneBtn;
}

#pragma mark -
#pragma mark  picker

-(UIImagePickerController *)picker{
    if (!_picker) {
        _picker = [[UIImagePickerController alloc]init];
        _picker.modalPresentationStyle= UIModalPresentationOverFullScreen;
        //        _picker.modalPresentationCapturesStatusBarAppearance = YES;
        _picker.delegate = self;
    }
    return _picker;
}



-(void)tap:(id)sender{
    WEAK_SELF;
    
    
    [[SRActionSheet sr_actionSheetViewWithTitle:nil cancelTitle:LS(@"取消") destructiveTitle:nil otherTitles:@[LS(@"相机"),LS(@"相册")] otherImages:nil selectSheetBlock:^(SRActionSheet *actionSheet, NSInteger index) {
        if (index < 0) return ;
        
        weakSelf.picker.sourceType =  index == 0 ? UIImagePickerControllerSourceTypeCamera :UIImagePickerControllerSourceTypePhotoLibrary;
        weakSelf.picker.editing = NO;
        [[weakSelf ex_viewController] presentViewController:weakSelf.picker animated:YES completion:NULL];
        
    }] show];
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *image =  [info objectForKey:UIImagePickerControllerOriginalImage];
    self.userImage.image = image;
    
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
