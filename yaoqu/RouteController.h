//
//  RouteController.h
//  yaoqu
//
//  Created by ridchan on 2017/6/16.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LLConversationModel;
@interface RouteController : NSObject
    
    
+(instancetype)sharedManager;
    
-(UIViewController *)rootViewController;

-(UINavigationController *)currentNav;
-(void)openMainController;
-(void)openLoginController;
-(void)openNewDynamicController;
-(void)openSetting;

-(void)openHomePageVC:(id)userModel;

-(void)openClassVC:(NSString *)vcName;

-(void)openClassVC:(NSString *)vcName withObj:(id)obj;

-(void)pushVC:(UIViewController *)vc;

-(void)presentVC:(UIViewController *)vc;

-(void)presentNavVC:(NSString *)vcName;

- (void)chatWithContact:(NSString *)userName userName:(NSString *)name;
- (void)chatWithConversationModel:(LLConversationModel *)conversationModel;

@end
