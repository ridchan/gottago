//
//  RegisterVC.h
//  QBH
//
//  Created by 陳景雲 on 2016/12/25.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import "BaseViewController.h"
#import "RegisterViewModel.h"
@interface RegisterVC : BaseViewController

@property(nonatomic) BOOL isFindPassword;
@property(nonatomic,strong) RegisterViewModel *viewModel;

@end
