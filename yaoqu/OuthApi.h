//
//  OuthApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/15.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface OuthApi : BaseApi

/*
 
  openid ,wechat id , unionid qq id
 
 */

-(id)initWithOpenID:(NSString *)open_id unionidID:(NSString *)union_id;

@end
