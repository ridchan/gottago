//
//  MineDepositVC.m
//  yaoqu
//
//  Created by ridchan on 2017/7/9.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineDepositVC.h"
#import "RCUserCacheManager.h"
#import "WalletApi.h"
#import "RechargeModel.h"
@interface MineDepositVC ()

@property(nonatomic,strong) UIView *headerView;
@property(nonatomic,strong) UIView *headerView2;

@end

@implementation MineDepositVC

-(void)viewDidLoad{
    [super viewDidLoad];
    [self commonInit];
}

-(void)commonInit{
    
    self.title = LS(@"我的积分");
    
    
    [self setRightItemWithIcon:[UIImage imageNamed:@"ic_question"] selector:nil];
    
    
    self.tableView.tableHeaderView = [self totalView];
    //    [self.tableView registerClass:[NormalTableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    
    WEAK_SELF;
    [[[WalletApi alloc]initWitObject:@{@"type":@(RechargeType_Integral)}] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        weakSelf.datas = [RechargeModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        [weakSelf.tableView reloadData];
    }];
    
}

#pragma mark -
#pragma mark lazy layout

-(UIView *)totalView{
    UIView *view = [[UIView alloc]init];
    [view addSubview:self.headerView];
    
    [view addSubview:self.headerView2];
    
    
    self.headerView2.sd_layout
    .leftEqualToView(view)
    .topSpaceToView(self.headerView, 10)
    .rightEqualToView(view)
    .autoHeightRatio(0);
    
    [view setupAutoHeightWithBottomView:_headerView2 bottomMargin:10];
    [view layoutSubviews];
    
    return view;
}

-(UIView *)headerView{
    if (!_headerView) {
        _headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 150)];
        _headerView.backgroundColor = [UIColor orangeColor];
        
        UIImage *image = [UIImage imageNamed:@"bg_yellow_buble"];
        UIImageView *bgView = [[UIImageView alloc]initWithImage:image];
        [_headerView addSubview:bgView];
        
        CGFloat height = APP_WIDTH  * image.size.height / image.size.width;
        _headerView.frame = CGRectMake(0, 0, APP_WIDTH, height);
        bgView.frame = CGRectMake(0, 0, APP_WIDTH, height);

        
        
        UILabel *priceLbl = [ControllerHelper autoFitLabel];
        priceLbl.font = SystemFont(18);
        priceLbl.textAlignment = NSTextAlignmentCenter;
        priceLbl.textColor = [UIColor whiteColor];
        priceLbl.text = @"200";
        
        [RACObserve([RCUserCacheManager sharedManager], currentUser.integral) subscribeNext:^(NSString *x) {
            NSString *str =[NSString stringWithFormat:@"当前 %@ 个",x];
            NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:str];
            [att addAttribute:NSFontAttributeName value:SystemFont(28) range:[str rangeOfString:x]];
            priceLbl.attributedText = att;
        }];
        [_headerView addSubview:priceLbl];
        
        
        UILabel *tipLbl = [ControllerHelper autoFitLabel];
        tipLbl.font = SystemFont(11);
        tipLbl.textColor = [UIColor whiteColor];
        tipLbl.text = @"积分可以作为购买旅游套餐，可通过旅游币兑现，或奖励获取积分";
        [_headerView addSubview:tipLbl];
        
        [priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_headerView.mas_centerX);
            make.centerY.equalTo(_headerView.mas_centerY);
        }];
        
        
        UIView *line1 = [[UIView alloc]init];
        line1.backgroundColor = [UIColor whiteColor];
        [_headerView addSubview:line1];
        
        UIView *line2 = [[UIView alloc]init];
        line2.backgroundColor = [UIColor whiteColor];
        [_headerView addSubview:line2];
        
        
        [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(priceLbl.mas_left).offset(-5);
            make.centerY.equalTo(priceLbl.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(60, 0.5));
        }];
        
        [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(priceLbl.mas_right).offset(5);
            make.centerY.equalTo(priceLbl.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(60, 0.5));
        }];
        
        [tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_headerView.mas_centerX);
            make.bottom.equalTo(_headerView.mas_bottom).offset(-20);
        }];
        
    }
    return _headerView;
}

-(UIView *)headerView2{
    if (!_headerView2) {
        _headerView2 = [[UIView alloc]init];
        _headerView2.backgroundColor = [UIColor whiteColor];
        
        
        UILabel *label = [[UILabel alloc]init];
        label.text = @"兑换积分";
        label.textColor = AppGray;
        label.font = SystemFont(11);
        [_headerView2 addSubview:label];
        
        UIView *line = [[UIView alloc]init];
        line.backgroundColor = AppLineColor;
        [_headerView2 addSubview:line];
        

        
        UILabel *tipLbl = [ControllerHelper autoFitLabel];
        tipLbl.text = @"当前";
        tipLbl.textColor = [UIColor blackColor];
        tipLbl.font = SystemFont(10);
        [_headerView2 addSubview:tipLbl];
        
        UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_travel_icon"]];
        [_headerView2 addSubview:imageView];
        
        UILabel *priceLbl = [ControllerHelper autoFitLabel];
        priceLbl.text = @"600";
        RAC(priceLbl,text) = RACObserve([RCUserCacheManager sharedManager], currentUser.integral);
        priceLbl.textColor = [UIColor orangeColor];
        priceLbl.font = SystemFont(10);
        [_headerView2 addSubview:priceLbl];
        
        UILabel *rateLbl = [ControllerHelper autoFitLabel];
        rateLbl.textAlignment = NSTextAlignmentRight;
        rateLbl.text = @"交换比例是1:10";
        rateLbl.textColor = AppGray;
        rateLbl.font = SystemFont(10);
        [_headerView2 addSubview:rateLbl];
        
        
        UITextField *textField = [[UITextField alloc]init];
        textField.font = SystemFont(12);
        textField.backgroundColor = [UIColor groupTableViewBackgroundColor];
        textField.placeholder = @"要兑现的旅游币";
        textField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, 5)];
        textField.leftViewMode = UITextFieldViewModeAlways;
        [_headerView2 addSubview:textField];
        
        UILabel *showLbl = [ControllerHelper autoFitLabel];
        showLbl.text = @"可兑现1000积分";
        showLbl.textColor = [UIColor blackColor];
        showLbl.font = SystemFont(11);
        [_headerView2 addSubview:showLbl];
        
        UIButton *comfirnBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [comfirnBtn setTitle:@"确认兑换" forState:UIControlStateNormal];
        comfirnBtn.backgroundColor = [UIColor orangeColor];// RGB16(0x1493db);
        comfirnBtn.titleLabel.font = SystemFont(12);
        comfirnBtn.layer.cornerRadius = 5;
        comfirnBtn.layer.masksToBounds = YES;
        [_headerView2 addSubview:comfirnBtn];
        
        label.sd_layout
        .leftSpaceToView(_headerView2, 10)
        .topSpaceToView(_headerView2, 10)
        .heightIs(20)
        .widthIs(200);
        
        line.sd_layout
        .leftSpaceToView(_headerView2, 10)
        .rightSpaceToView(_headerView2, 10)
        .topSpaceToView(label, 10)
        .heightIs(0.5);
        
        tipLbl.sd_layout
        .leftSpaceToView(_headerView2, 10)
        .topSpaceToView(line, 5)
        .heightIs(30)
        .autoWidthRatio(0);
        [tipLbl setSingleLineAutoResizeWithMaxWidth:50];
        
        imageView.sd_layout
        .leftSpaceToView(tipLbl, 5)
        .centerYEqualToView(tipLbl)
        .widthIs(15)
        .heightIs(15);
        
        priceLbl.sd_layout
        .leftSpaceToView(imageView, 5)
        .centerYEqualToView(tipLbl)
        .widthIs(100)
        .heightIs(30);
        
        rateLbl.sd_layout
        .rightSpaceToView(_headerView2, 10)
        .centerYEqualToView(tipLbl)
        .widthIs(200)
        .heightIs(30);
        
        textField.sd_layout
        .leftSpaceToView(_headerView2, 10)
        .topSpaceToView(tipLbl, 10)
        .heightIs(30)
        .widthIs(200);
        
        showLbl.sd_layout
        .leftSpaceToView(textField, 10)
        .centerYEqualToView(textField)
        .heightIs(30)
        .widthIs(200);
        
        comfirnBtn.sd_layout
        .leftSpaceToView(_headerView2, 10)
        .topSpaceToView(textField, 10)
        .heightIs(25)
        .widthIs(80);
        
        [_headerView2 setupAutoHeightWithBottomView:comfirnBtn bottomMargin:10];
        [_headerView2 layoutSubviews];
    }
    
    return _headerView2;
}


-(UIView *)sectionTipView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 40)];
    
    UIView *bgView =[[UIView alloc]initWithFrame:CGRectMake(0, 10, APP_WIDTH, 30)];
    bgView.backgroundColor = [UIColor whiteColor];
    [view addSubview:bgView];
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 39.5, APP_WIDTH, 0.5)];
    line.backgroundColor = AppLineColor;
    [view addSubview:line];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"积分明细";
    label.textColor = AppGray;
    label.font = SystemFont(11);
    label.frame = CGRectMake(10, 10, APP_WIDTH - 10, 30);
    [view addSubview:label];
    
    return view;
}

-(UIView *)sectionBottomView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 40)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = view.bounds;
    [button addTarget:self action:@selector(moreBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *label = [[UILabel alloc]init];
    label.text = @"查看更多>>";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = AppGray;
    label.font = SystemFont(11);
    label.frame = CGRectMake(10, 0, APP_WIDTH - 20, 40);
    
    [view addSubview:label];
    [view addSubview:button];
    return view;
}


#pragma mark -

-(void)moreBtnClick:(id)sender{
    [self pushViewControllerWithName:@"MineWalletDetailVC" params:@(RechargeType_Integral)];
}

#pragma mark -
#pragma mark table view

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return self.datas.count > 0 ? [self sectionTipView] : nil;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return self.datas.count > 0 ?  [self sectionBottomView] : nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 40;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
    return [tableView cellHeightForIndexPath:indexPath model:nil keyPath:nil cellClass:[NormalTableViewCell class] contentViewWidth:APP_WIDTH - 20];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return MIN(self.datas.count,10);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NormalTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil ) {
        cell = [[NormalTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        cell.textLabel.font = UserNameFont;
        cell.detailTextLabel.font = DetailFont;
        cell.detailTextLabel.textColor = AppGray;
        cell.celltype = NormalCellType_Detail;
    }
    RechargeModel *model = [self.datas objectAtIndex:indexPath.row];
    cell.textLabel.text = model.desc;
    cell.detailTextLabel.text = model.time;
    cell.detailLbl.text = model.val;
    
    return cell;
}

@end
