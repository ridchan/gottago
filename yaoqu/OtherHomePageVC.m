//
//  OtherHomePageVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/24.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "OtherHomePageVC.h"
#import "OtherHomePageFootView.h"
#import "OtherHomePageDynamicView.h"
#import "OtherHomePageTripView.h"
#import "OtherHomePageHeader.h"
#import "UserHomePageGetApi.h"
#import "UserModel.h"
#import "HomePageHeaderView.h"
#import "UserHomeDynamicCell.h"
#import "DynamicModel.h"
#import "OtherHomePageTripCell.h"
#import "HomePageApi.h"
#import "DynamicApi.h"
#import "CommonConfigManager.h"
#import "MemberApi.h"
#import "ShareView.h"
#import "GiftView.h"
#import "GiftPostModel.h"
#import "GiftApi.h"
#import "HomePageApi.h"
#import "BlacklistApi.h"
#import "SRActionSheet.h"
#import "RelationApi.h"
#import "RCUserCacheManager.h"
@interface OtherHomePageVC ()<GiftViewDelegate>

@property(nonatomic,strong) OtherHomePageHeader *headerView;
@property(nonatomic,strong) OtherHomePageFootView *footView;
@property(nonatomic,strong) OtherHomePageDynamicView *dynamicView;
@property(nonatomic,strong) OtherHomePageTripView *tripView;
@property(nonatomic,strong) UserModel *userModel;
@property(nonatomic,strong) UIView *bottomView;

@property(nonatomic,strong) UIButton *followBtn;



@property(nonatomic,strong) NSMutableArray *dynamic;

@end

@implementation OtherHomePageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    


    [self setRightItemWithIcon:[UIImage imageNamed:@"ic_more"] selector:@selector(moreBtnClick:)];
    
    self.headerView = [[OtherHomePageHeader alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 400)];
    self.tableView.tableHeaderView = self.headerView;
    
    self.datas = [@[LS(@"身份标识"),LS(@"等级"),LS(@"财富等级")]mutableCopy];
    
    self.userModel = self.paramObj;
    
    if (!self.userModel){
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    self.headerView.userModel = self.userModel;
    
    self.title = self.userModel.username;
    
    
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top);
        make.bottom.equalTo(self.view.mas_bottom).offset(-50);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.tableView.mas_bottom);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    
    [RACObserve(self.headerView, frameHeight) subscribeNext:^(id x) {
        [self.tableView reloadData];
    }];
    
    WEAK_SELF;
    [[[HomePageApi alloc]initWitObject:@{@"member_id":self.userModel.member_id}] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        NSLog(@"用户个人主页 %@",[responseObj keyValues]);
        if (info.error == ErrorCodeType_None){
            weakSelf.userModel = [UserModel objectWithKeyValues:[[responseObj objectForKey:@"data"] objectForKey:@"member"]];
            weakSelf.headerView.userModel = weakSelf.userModel;
            
            
            NSMutableArray *array = [DynamicModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"dynamic"]];
            weakSelf.dynamic = array ;//[weakSelf resloveDynamicImage:array];
        }
    }];
    
    
    // Do any additional setup after loading the view.
}

-(UIView *)rightView{
    UIView *view = [[UIView alloc]init];
    
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_user_level_1"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:imageView];
    
    UILabel *label = [ControllerHelper autoFitLabel];
    label.font = SystemFont(12);
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"达人";
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = AppBlue;
    label.layer.cornerRadius = 3;
    label.layer.masksToBounds = YES;
    
    [view addSubview:label];
    
    [RACObserve(self, userModel) subscribeNext:^(UserModel *x) {
        [imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(view.mas_top);
            make.bottom.equalTo(view.mas_bottom);
            if (x.is_member){
                make.right.equalTo(view.mas_right).offset(-5);
                make.width.mas_equalTo(20);
            }else{
                make.right.equalTo(view.mas_right);
                make.width.mas_equalTo(0);
            }
        }];
        
        [label mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(imageView.mas_left).offset(-5);
            make.top.equalTo(view.mas_top).offset(2);
            make.bottom.equalTo(view.mas_bottom).offset(-2);
            if (x.is_talent){
                make.width.mas_equalTo(40);
            }else{
                make.width.mas_equalTo(0);
            }
        }];
    }];
    
    return view;
}



-(void)moreBtnClick:(id)sender{
    
    
    WEAK_SELF;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//    [self addActionTarget:alert title:@"分享" color:AppBlack action:^(UIAlertAction *action) {
//        ShareView *shareView = [[ShareView alloc]init];
//        [shareView show];
//
//    }];
    
    [self addActionTarget:alert title:@"举报" color:AppBlack action:^(UIAlertAction *action) {
        
        NSArray *array = [CommonConfigManager sharedManager].hostModel.report_desc;
        if ([array count] == 0) return ;
        [[SRActionSheet sr_actionSheetViewWithTitle:nil cancelTitle:@"取消" destructiveTitle:nil otherTitles:array  otherImages:nil selectSheetBlock:^(SRActionSheet *actionSheet, NSInteger index) {
            if (index >= 0){
                HomePageApi *api = [[HomePageApi alloc]initWitObject:@{@"member_id":weakSelf.userModel.member_id,
                                                                       @"desc":array[index]
                                                                       }];
                api.method = YTKRequestMethodPUT;
                [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                    [LLUtils showActionSuccessHUD:info.message];
                }];
            }
        }] show];

    }];
    
    [self addActionTarget:alert title:@"加入黑名单" color:AppBlack action:^(UIAlertAction *action) {
        BlacklistApi *api = [[BlacklistApi alloc]initWitObject:@{@"member_id":weakSelf.userModel.member_id}];
        api.method = YTKRequestMethodPOST;
                             
        [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            if (info.error == ErrorCodeType_None){
                [LLUtils showActionSuccessHUD:@"操作成功"];
            }else{
                [LLUtils showCenterTextHUD:info.message];
            }
        }];

    }];
    
    
    [self addCancelActionTarget:alert title:@"取消"];
    [self presentViewController:alert animated:YES completion:nil];
    

    

    
}


// 取消按钮
-(void)addCancelActionTarget:(UIAlertController*)alertController title:(NSString *)title
{
    UIAlertAction *action = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }];
    [action setValue:AppOrange forKey:@"_titleTextColor"];
    [alertController addAction:action];
}
//添加对应的title    这个方法也可以传进一个数组的titles  我只传一个是为了方便实现每个title的对应的响应事件不同的需求不同的方法
- (void)addActionTarget:(UIAlertController *)alertController title:(NSString *)title color:(UIColor *)color action:(void(^)(UIAlertAction *action))actionTarget
{
    UIAlertAction *action = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        actionTarget(action);
    }];
    [action setValue:color forKey:@"_titleTextColor"];
    [alertController addAction:action];
}




-(NSMutableArray *)resloveDynamicImage:(NSArray *)dynamicList{
    NSMutableArray *images = [NSMutableArray array];
    for (DynamicModel *model in dynamicList){
        NSArray *imgs = nil;
        if ([model.content_images isKindOfClass:[NSArray class]]){
            imgs = (NSArray *)model.content_images;
        }else{
            imgs = [model.content_images JSONObject];
        }
        if ([imgs count] > 0) {
            [images addObject:imgs[0]];
        }
    }
    return images;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)editBtnClick:(id)sender{
    [[RouteController sharedManager] openClassVC:@"UserEditVC"];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return section == 0 ? 3 : 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
            return 50;
        case 1:
            return 120;
        case 2:
            return 250;
        default:
            break;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        NormalTableViewCell *cell = [NormalTableViewCell cellWithType:NormalCellType_Access_Detail identifier:@"Cell" tableView:tableView];
        cell.textLabel.text = self.datas[indexPath.row];
        cell.textLabel.textColor = RGB16(0x737373);
        cell.type = indexPath.row != 0 ? NormalCellType_TopLine : NormalCellType_None;
        [cell rac_prepareForReuseSignal];
        
        
        if (indexPath.row == 0) {
            cell.rightView = [self rightView];
        }else{
            [RACObserve(self, userModel) subscribeNext:^(UserModel *x) {
                cell.detailLbl.text = [NSString stringWithFormat:@"%@",x.level];
            }];
        }
        
        return cell;
    }else if (indexPath.section == 1){
        UserHomeDynamicCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell2"];
        if (cell == nil) {
            cell = [[UserHomeDynamicCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell2"];
            cell.titleLbl.text = @"他的动态";
            [cell rac_prepareForReuseSignal];
        }
        [RACObserve(self, dynamic) subscribeNext:^(id x) {
            cell.datas = x;
        }];
        return cell;
    }else if(indexPath.section == 2){
        
        OtherHomePageTripCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell3"];
        if (cell == nil) {
            cell = [[OtherHomePageTripCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell3"];
        }
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        [[RouteController sharedManager] openClassVC:@"DynamicListVC" withObj:self.userModel];
    }else if (indexPath.section == 0){
//        if (indexPath.row == 1)
//            [self pushViewControllerWithName:@"MineLevelVC"];
//        else
//            [self pushViewControllerWithName:@"UserIdentifyVC"];
    }
}



-(UIView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = [UIColor whiteColor];
        
        [[_bottomView lineInColor:AppLineColor] mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bottomView.mas_left);
            make.right.equalTo(_bottomView.mas_right);
            make.top.equalTo(_bottomView.mas_top);
            make.height.mas_equalTo(0.5);
        }];
        
        [_bottomView addSubview:self.followBtn];
        
        [self.followBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerY.equalTo(_bottomView.mas_centerY);
            make.left.mas_equalTo(_bottomView.mas_left);
            make.top.equalTo(_bottomView.mas_top);
            make.bottom.equalTo(_bottomView.mas_bottom);
            make.width.mas_equalTo(APP_WIDTH / 3.0);
        }];
        
        
        
        ImageButton *btn1 = [[ImageButton alloc]init];
        btn1.titleLbl.text = LS(@"聊天");
        btn1.titleLbl.font = SystemFont(16);
        btn1.titleLbl.textColor = RGB16(0x707070);
        btn1.contentImage.image = [[UIImage imageNamed:@"ic_homepage_chat"] imageToColor:RGB16(0xa3a3a3)];
        [btn1 setFixImage:CGSizeMake(20, 20)];
        [btn1 addTarget:self action:@selector(chatBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_bottomView addSubview:btn1];
        [btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(APP_WIDTH / 3.0);
            make.top.equalTo(_bottomView.mas_top);
            make.bottom.equalTo(_bottomView.mas_bottom);
            make.width.mas_equalTo(APP_WIDTH / 3.0);
        }];
        
        
        
        ImageButton *btn2 = [[ImageButton alloc]init];
        btn2.titleLbl.text = LS(@"送礼");
        btn2.titleLbl.textColor = RGB16(0x707070);
        btn2.titleLbl.font = SystemFont(16);
        btn2.contentImage.image = [[UIImage imageNamed:@"ic_homepage_gift"] imageToColor:RGB16(0xa3a3a3)];
        [btn2 setFixImage:CGSizeMake(20, 20)];
        [btn2 addTarget:self action:@selector(giftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_bottomView addSubview:btn2];
        [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_bottomView.mas_right);
            make.top.equalTo(_bottomView.mas_top);
            make.bottom.equalTo(_bottomView.mas_bottom);
            make.left.equalTo(btn1.mas_right);
        }];
        
        
        [[_bottomView lineInColor:AppLineColor] mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(btn1.mas_left);
            make.bottom.equalTo(_bottomView.mas_bottom);
            make.top.equalTo(_bottomView.mas_top);
            make.width.mas_equalTo(0.5);
        }];
        
        [[_bottomView lineInColor:AppLineColor] mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(btn2.mas_left);
            make.bottom.equalTo(_bottomView.mas_bottom);
            make.top.equalTo(_bottomView.mas_top);
            make.width.mas_equalTo(0.5);
        }];
        
        
        
        [self.view addSubview:_bottomView];
    }
    return _bottomView;
    
}

-(void)setFollowStatus{
    [RACObserve(self,userModel.is_follow) subscribeNext:^(id x) {
        if ([x boolValue]){
            
            [_followBtn setTitle:@"取消关注" forState:UIControlStateNormal];
            [_followBtn setTitleColor:AppGray forState:UIControlStateNormal];
//            _followBtn.titleLbl.text = @"取消关注";
//            _followBtn.titleLbl.textColor = AppGray;
//            _followBtn.layer.borderColor = AppGray.CGColor;
//            _followBtn.contentImage.image = [UIImage imageNamed:@"ic_check_gray"];
//            _followBtn.backgroundColor = [UIColor clearColor];
        }else{
            
            [_followBtn setTitle:@"关注" forState:UIControlStateNormal];
            [_followBtn setTitleColor:AppOrange forState:UIControlStateNormal];
//            _followBtn.contentImage.image = nil;
//            _followBtn.titleLbl.text = @"关注";
//            _followBtn.backgroundColor = AppOrange;
//            _followBtn.contentImage.image = [[UIImage imageNamed:@"ic_chat_add"] imageToColor:[UIColor whiteColor]];
//            _followBtn.titleLbl.textColor = [UIColor whiteColor];
//            _followBtn.layer.borderColor = AppOrange.CGColor;
        }

    }];
    

}

-(UIButton *)followBtn{
    if (!_followBtn) {
        _followBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _followBtn.titleLabel.font = SystemFont(16);
        
//        [_followBtn setFixImage:CGSizeMake(15, 15)];
//        _followBtn.titleLbl.font = SystemFont(14);
//        _followBtn.layer.cornerRadius = 3.0;
//        _followBtn.titleLbl.textColor = AppOrange;
//        _followBtn.layer.borderColor = AppOrange.CGColor;
//        _followBtn.layer.borderWidth = 1.0;
//        _followBtn.layer.masksToBounds = YES;
        [_followBtn addTarget:self action:@selector(followBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self setFollowStatus];
    }
    
    return _followBtn;
}

-(void)followBtnClick:(id)sender{
    __block NSInteger state = 1;
    if (self.userModel.is_follow){
        [[SRActionSheet sr_actionSheetViewWithTitle:@"确定不再关注此人？" cancelTitle:@"取消" destructiveTitle:nil otherTitles:@[@"确定"] otherImages:nil selectSheetBlock:^(SRActionSheet *actionSheet, NSInteger index) {
            state = index;
        }] show];
        
        while (state == 1) {
            [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }
    }
    
    if (state < 0) return;
    
    RelationApi *api = [[RelationApi alloc]initWitObject:@{@"member_id":self.userModel.member_id,@"state":@(state)}];
    api.method = YTKRequestMethodPOST;
    
    WEAK_SELF;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            
            weakSelf.userModel.is_follow = !weakSelf.userModel.is_follow;
            [[RCUserCacheManager sharedManager] cacheUser:weakSelf.userModel.member_id].is_follow =weakSelf.userModel.is_follow;
//            [weakSelf setFollowStatus];
        }
    }];
}

-(void)giftView:(GiftView *)view didSelectObj:(GiftModel *)obj{
    
//    GiftPostModel *postModel = [[GiftPostModel alloc]init];
//    postModel.key_id = self.userModel.member_id;
//    postModel.member_id = self.userModel.member_id;
//    postModel.type = GiftType_ForHomePage;
//    postModel.gift_id = obj.gift_id;
    
    GiftPostModel *postModel = [[GiftPostModel alloc]init];
    postModel.key_id = self.userModel.member_id;
    postModel.member_id = self.userModel.member_id;
    postModel.type = GiftType_ForHomePage;
    
    
    [GiftView showWithDelegate:self postModel:postModel];
    
    
}


-(void)giftBtnClick:(id)sender{
    
    GiftPostModel *postModel = [[GiftPostModel alloc]init];
    postModel.key_id = self.userModel.member_id;
    postModel.member_id = self.userModel.member_id;
    postModel.type = GiftType_ForHomePage;
    
    [GiftView showWithDelegate:self postModel:postModel];
}

-(void)chatBtnClick:(id)sender{
    [[RouteController sharedManager] chatWithContact:self.userModel.huanXinID userName:self.userModel.username];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
