//
//  MsgModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/20.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface MsgModel : BaseModel

@property(nonatomic,strong) NSString *msg;
@property(nonatomic) BOOL state;
@property(nonatomic) ErrorCodeType code;


@property(nonatomic,strong) NSString *message;
@property(nonatomic) ErrorCodeType error;

@end
