//
//  NetImagePreviewVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/29.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NetImagePreviewVC.h"
#import "SRActionSheet.h"

@interface PreviewCell()<UIGestureRecognizerDelegate,UIScrollViewDelegate,UIActionSheetDelegate>



@end

@implementation PreviewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

-(void)createView{
    
    
    self.scView = [[UIScrollView alloc]init];
    self.scView.backgroundColor = [UIColor clearColor];
    self.scView.userInteractionEnabled = YES;
    
    
    self.scView.delegate = self;
    self.scView.showsHorizontalScrollIndicator = NO;
    self.scView.showsVerticalScrollIndicator = NO;
    self.scView.decelerationRate = UIScrollViewDecelerationRateFast;
    self.scView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.scView.maximumZoomScale = 3.0;
    self.scView.minimumZoomScale = 1.0;
    
    [self.contentView addSubview:self.scView];
    
    self.imageView = [[UIImageView alloc]init];
    self.imageView.backgroundColor = [UIColor  clearColor];
    self.imageView.userInteractionEnabled = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.scView addSubview:self.imageView];
    
    

    [self.scView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.scView.center);
        make.size.mas_equalTo(CGSizeMake(APP_WIDTH, APP_HEIGHT));
    }];
    
    
    UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPress:)];
    recognizer.minimumPressDuration = 0.5;
    
    [self.imageView addGestureRecognizer:recognizer];
    
}



- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    
    
    if(!error){
        [LLUtils showActionSuccessHUD:@"保存成功"];
    }else{
        [LLUtils showCenterTextHUD:@"保存失败"];
    }
}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        UIImageWriteToSavedPhotosAlbum(self.imageView.image, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    }
}

-(void)longPress:(UITapGestureRecognizer *)press{
    
    if (press.state == UIGestureRecognizerStateBegan) {
        UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"保存图片", nil];
        [sheet showInView:self];
    }
    

}


-(void)handleTap:(UITapGestureRecognizer *)tap{
    
}

-(void)handlePan:(UIPanGestureRecognizer *)recognizer{
 
}

-(void)handlePinch:(UIPinchGestureRecognizer *)recognizer{
    
    UIView *view = recognizer.view;
    view.transform = CGAffineTransformMakeScale(recognizer.scale,recognizer.scale);
    
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return self.imageView;
}

@end


#pragma mark -

@interface NetImagePreviewVC ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) UIImageView *cacheView;
@property(nonatomic,strong) UILabel *countLbl;

@end

@implementation NetImagePreviewVC


- (void)show
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [window addSubview:self.view];
    [window.rootViewController addChildViewController:self];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:[self layout]];
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.pagingEnabled = YES;
    
    
    
    
    
    [self.collectionView registerClass:[PreviewCell class] forCellWithReuseIdentifier:@"Cell"];
    [self.view addSubview:self.collectionView];
    
    self.countLbl =  [ControllerHelper autoFitLabel];
    self.countLbl.textColor = [UIColor whiteColor];
    self.countLbl.text = @"";
    self.countLbl.font = SystemFont(14);
    self.countLbl.backgroundColor = RGBA(0, 0, 0, 0.7);
    self.countLbl.layer.cornerRadius = 10;
    self.countLbl.layer.masksToBounds = YES;
    self.countLbl.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.countLbl];
    
    [self.countLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(20);
        make.centerX.equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(50, 20));
    }];
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss:)];
    [self.collectionView addGestureRecognizer:tap];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    
    self.cacheView = [[UIImageView alloc]init];
    self.cacheView.clipsToBounds = YES;
    self.cacheView.backgroundColor = [UIColor clearColor];
    self.cacheView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.cacheView];

    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.cacheView.hidden = NO;
    self.collectionView.hidden = YES;
        
    
    UIImageView *imageView = [self.delegate previewImage:nil atIndex:self.showIndex];
    self.cacheView.image = imageView.image;
    self.cacheView.frame = [imageView convertRect:imageView.bounds toView:nil];
    
    self.cacheView.contentMode = UIViewContentModeScaleAspectFill;
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.35 animations:^{
        weakSelf.view.backgroundColor = [UIColor blackColor];
        weakSelf.cacheView.contentMode = UIViewContentModeScaleAspectFit;
        weakSelf.cacheView.frame = CGRectMake(0, 0, APP_WIDTH, APP_HEIGHT);
        
    } completion:^(BOOL finished) {
        
        
        [weakSelf showAtIndex:weakSelf.showIndex];
        
        weakSelf.cacheView.image = nil;
        weakSelf.cacheView.hidden = YES;
        weakSelf.collectionView.hidden = NO;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismiss:(id)sender{
    UIImageView *imageView =  nil;
    __block CGRect toFrame = CGRectZero;
    
    self.cacheView.hidden = NO;
    self.collectionView.hidden = YES;
    
    
    if ([self.delegate respondsToSelector:@selector(previewImage:atIndex:)]){
        NSIndexPath *indexPath = [[self.collectionView indexPathsForVisibleItems] firstObject];
        
        PreviewCell *cell = [[self.collectionView visibleCells] firstObject];
        self.cacheView.image = cell.imageView.image;
        imageView  = [self.delegate previewImage:nil atIndex:indexPath.item];
        if (imageView.image){
            CGSize size = CGSizeMake(APP_WIDTH, APP_WIDTH * imageView.image.size.height / imageView.image.size.width);
            self.cacheView.frame = CGRectMake(0, (APP_HEIGHT - size.height) / 2, size.width, size.height);
            toFrame = [imageView convertRect:imageView.bounds toView:nil];
        }
        
    }
    
    WEAK_SELF;
    
    
    [UIView animateWithDuration:0.35 animations:^{
        weakSelf.view.backgroundColor = [UIColor clearColor];
        weakSelf.cacheView.contentMode = UIViewContentModeScaleAspectFill;
        weakSelf.cacheView.frame = toFrame;
        
        
    } completion:^(BOOL finished) {
        [weakSelf.view removeFromSuperview];
        [weakSelf removeFromParentViewController];
    }];

}

-(void)showAtIndex:(NSInteger)index{
    NSInteger maxCount = [self.delegate numOfPreviewer:nil];
    
    self.countLbl.text = [NSString stringWithFormat:@"%ld/%ld",index + 1,maxCount];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
//    
//    self.pageControl.numberOfPages = maxCount;
//    self.pageControl.currentPage = index ;
}


-(UICollectionViewFlowLayout *)layout{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(APP_WIDTH, APP_HEIGHT);
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    layout.scrollDirection =  UICollectionViewScrollDirectionHorizontal;
    
    return layout;
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSInteger page = roundf(scrollView.contentOffset.x / scrollView.frame.size.width) + 1;
    
    self.countLbl.text = [NSString stringWithFormat:@"%ld/%ld",page,[self.delegate numOfPreviewer:nil]];
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [self.delegate numOfPreviewer:nil];
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PreviewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    [cell.imageView setImageName:[self.delegate originalImage:nil atIndex:indexPath.item]
                     placeholder:[self.delegate previewImage:nil atIndex:indexPath.item].image];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
