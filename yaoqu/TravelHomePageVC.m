//
//  TravelHomePageVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelHomePageVC.h"
#import "TravelHomePageHeader.h"
#import "TravelScheduleListApi.h"
#import "TravelBannerModel.h"
#import "TravelScheduleModel.h"
#import "TravelScheduleListCell.h"
#import "NavTitleView.h"
#import "TravelScheduleDetailVC.h"
#import "TravelApi.h"
#import "CommonApi.h"
#import "RCUserCacheManager.h"
#import "LeftBarButtonItem.h"
#import "AddressProvinceVC.h"
#import "BaseNavigationController.h"


@interface TravelHomePageVC ()<UITableViewDelegate,UITableViewDataSource>{
    UIImage *locationImage;
}

@property(nonatomic,strong) TravelHomePageHeader *headerView;
@property(nonatomic,strong) NavTitleView *titleView;
@property(nonatomic,strong) UIWebView *webView;


@end

@implementation TravelHomePageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.page = 1;
    [self wr_setNavBarBackgroundAlpha:0];
    
    locationImage = [[UIImage imageNamed:@"ic_location_black"] imageToColor:[UIColor whiteColor]];
    
    self.navigationItem.title = @"";
    
    
    [self setLeftItemWithIcon:locationImage title:[RCUserCacheManager sharedManager].currentCity.city_name selector:@selector(addressBtnClick:)];
    [(LeftBarButtonItem *)self.navigationItem.leftBarButtonItem setTitleColor:[UIColor whiteColor]];
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(-64);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    
    [self.tableView registerClass:[TravelScheduleListCell class] forCellReuseIdentifier:@"Cell"];
    self.headerView = [[TravelHomePageHeader alloc]init];
    self.headerView.frame = CGRectMake(0, 0, APP_WIDTH, 180);
//    self.tableView.tableHeaderView = self.headerView;
    
    self.titleView = [[NavTitleView alloc]init];
    self.titleView.frame = CGRectMake(0, 0, 200, 44);
    self.titleView.titles = @[@"最新",@"人气",@"热门",@"时间"];
    
    
    
//    [self startRefresh];
    if (![RCUserCacheManager sharedManager].currentCity)
        [[RCUserCacheManager sharedManager] startLocation];
    [RACObserve([RCUserCacheManager sharedManager], currentCity) subscribeNext:^(id x) {
        [self startRefresh];
    }];
    
    
    
    self.webView = [[UIWebView alloc]init];
    
    NSString *oldAgent = [self.webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    
    
    NSString *customUserAgent = nil;
    
    
    if ([oldAgent rangeOfString:@"gottago"].location == NSNotFound){
        customUserAgent = [NSString stringWithFormat:@"%@( gottago )",oldAgent];
    }else{
        customUserAgent = oldAgent;
    }
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"UserAgent":customUserAgent}];

    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://gottago917.com"]]];
    // Do any additional setup after loading the view.
}

-(void)addressBtnClick:(id)sender{
    [self pushViewControllerWithName:@"AddressProvinceVC"];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y < 100) {
        [self wr_setNavBarBackgroundAlpha:scrollView.contentOffset.y / 100.0];
        [self wr_setStatusBarStyle:UIStatusBarStyleDefault];
        [(LeftBarButtonItem *)self.navigationItem.leftBarButtonItem setTitleColor:[UIColor whiteColor]];
        [(LeftBarButtonItem *)self.navigationItem.leftBarButtonItem setImageColor:[UIColor whiteColor]];
    }else{
        
        [self wr_setNavBarBackgroundAlpha:1];
        [self wr_setNavBarTintColor:[UIColor whiteColor]];
        [self wr_setNavBarTitleColor:[UIColor whiteColor]];
        [self wr_setStatusBarStyle:UIStatusBarStyleLightContent];
        
        [(LeftBarButtonItem *)self.navigationItem.leftBarButtonItem setTitleColor:AppGray];
        [(LeftBarButtonItem *)self.navigationItem.leftBarButtonItem setImageColor:AppGray];
    }
    
    if (scrollView.contentOffset.y > self.headerView.frame.size.height) {
        self.navigationItem.titleView = self.titleView;
    }else{
        self.navigationItem.titleView = nil;
    }
}

-(void)startRefresh{
    WEAK_SELF;
    
    if (![RCUserCacheManager sharedManager].currentCity) return;
    
    self.page = 1;
    [self setLeftItemWithIcon:locationImage title:[RCUserCacheManager sharedManager].currentCity.city_name selector:@selector(addressBtnClick:)];
    [self scrollViewDidScroll:self.tableView];
    
    NSDictionary *dict = @{@"city_id":[RCUserCacheManager sharedManager].currentCity.city_id,
                           @"page":@(self.page),
                           @"size":@(self.size)
                           };
    
    
    [[[TravelApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        weakSelf.headerView.datas = [TravelBannerModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"banner"]];
        weakSelf.tableView.tableHeaderView = weakSelf.headerView;
        
        NSArray *data = [TravelScheduleModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"travel_schedule"]];
        [weakSelf refreshComplete:info response:data];
    }];
}

-(void)startloadMore{
    WEAK_SELF;
    
    NSDictionary *dict = @{@"city_id":[RCUserCacheManager sharedManager].currentCity.city_id,
                           @"page":@(++self.page),
                           @"size":@(self.size)
                           };
    
    [[[TravelApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        
        NSArray *data = [TravelScheduleModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"travel_schedule"]];
        [weakSelf loadMoreComplete:info response:data];
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    return 150;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TravelScheduleDetailVC *vc = [[TravelScheduleDetailVC alloc]init];
    
    vc.model = self.datas[indexPath.section];
    
    BaseNavigationController *nav = [[BaseNavigationController alloc]initWithRootViewController:vc];
    
    [self.navigationController presentViewController:nav animated:YES completion:NULL];
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section == 0 ? 80 : 10;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section !=  0) return nil;
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 80)];
    
    UIView *bgView =[[UIView alloc]initWithFrame:CGRectMake(0, 10, APP_WIDTH , 70)];
    bgView.backgroundColor = [UIColor whiteColor];
    [view addSubview:bgView];
    

    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"旅游套餐";
    label.font = SystemFont(20);
    label.textColor = RGB16(0xf40);
    label.frame = CGRectMake(10, 20, APP_WIDTH - 10, 50);
    [bgView addSubview:label];
    
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [tableView cellHeightForIndexPath:indexPath model:self.datas[indexPath.section] keyPath:@"model" cellClass:[TravelScheduleListCell class] contentViewWidth:APP_WIDTH];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.datas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TravelScheduleListCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.model = self.datas[indexPath.section];
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
