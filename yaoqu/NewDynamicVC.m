//
//  NewDynamicVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/20.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NewDynamicVC.h"
#import "NewDynamicHeader.h"
#import "DynamicUploadManager.h"
#import "LLUtils.h"
#import "LLGaoDeLocationViewController.h"
#import "LLNavigationController.h"
#import "UIView+Extension.h"
#import "UploadObject.h"
#import "GaoDeLocationVC.h"
#import "BaseNavigationController.h"
#import "RCUserCacheManager.h"

@interface NewDynamicVC ()<LLLocationViewDelegate,GaoDeLocationVCDelegate>

@property(nonatomic,strong) NewDynamicHeader *header;
@property(nonatomic,strong) UIView *locationView;
@property(nonatomic,strong) UILabel *localtionLbl;
@property(nonatomic,strong) UIButton *locationBtn;
@property(nonatomic,strong) CityModel *selectPOI;
@property(nonatomic) BOOL isShowKeyboard;
@property(nonatomic,strong) UINavigationBar *navBar;
@property(nonatomic,strong) UINavigationItem *navItem;

@end

@implementation NewDynamicVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self commonInit];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}


-(void)viewFirstAppear{
    [self wr_setNavBarBarTintColor:[UIColor whiteColor]];
    [self wr_setNavBarShadowImageHidden:YES];
}

-(void)commonInit{
    
//    [self wr_setNavBarBackgroundAlpha:0.0];
//    
//    self.navBar = [[UINavigationBar alloc]init];
//    self.navBar.backgroundColor = [UIColor whiteColor];
//    
//    [self.navBar setBackgroundImage:[UIImage imageWithColor:RGBA(0, 0, 0, 0) size:CGSizeMake(APP_WIDTH, 64)]
//                     forBarPosition:UIBarPositionAny
//                         barMetrics:UIBarMetricsDefault];
//    [self.navBar setShadowImage:[UIImage new]];
//    [self.navBar setTitleTextAttributes:@{NSForegroundColorAttributeName:AppTitleColor}];
//    
//    [self.view addSubview:self.navBar];
//    
//    [self.navBar mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.view.mas_left);
//        make.top.equalTo(self.view.mas_top);
//        make.right.equalTo(self.view.mas_right);
//        make.height.mas_equalTo(64);
//    }];
//    
//    self.navItem = [[UINavigationItem alloc]init];
//    
//    self.navBar.items = @[self.navItem];
//    
//    self.navItem.leftBarButtonItem = [self ittemLeftItemWithIcon:[UIImage imageNamed:@"ic_back"] title:nil selector:@selector(cancelBtnClick:)];
//    self.navItem.title = @"发布内容";
//    self.navItem.rightBarButtonItem = [self ittemRightItemWithTitle:@"发布" selector:@selector(publishBtnClick:)];


    
    
    self.title = @"发布内容";
    [self setRightItemWithTitle:LS(@"发布") selector:@selector(publishBtnClick:)];
    [self setLeftItemWithIcon:[UIImage imageNamed:@"ic_back"] title:nil selector:@selector(cancelBtnClick:)];
    
    self.header = [[NewDynamicHeader alloc]init];
    self.header.vc = self;
    [self.header computeHeight];
    self.header.frame = CGRectMake(0, 64, APP_WIDTH, [self.header.height floatValue]);
    [self.view addSubview:self.header];
    
    
    [self.locationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.header.mas_bottom);
        make.height.mas_equalTo(50);
    }];
    
    WEAK_SELF;
    [RACObserve(self.header, height) subscribeNext:^(NSString *height) {
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.header.frame = CGRectMake(0, 64, APP_WIDTH, [height floatValue]);
        });
    }];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!_isShowKeyboard) {
        _isShowKeyboard = YES;
        [self.header.textView becomeFirstResponder];
    }
    
}

-(UIView *)locationView{
    if (!_locationView) {
        _locationView = [[UIView alloc]init];
        _locationView.backgroundColor = [UIColor whiteColor];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(locationBtnClick:)];
        [_locationView addGestureRecognizer:tap];
        
        UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_location"]];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [_locationView addSubview:imageView];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_locationView).offset(10);
            make.centerY.equalTo(_locationView);
            make.size.mas_equalTo(CGSizeMake(25, 25));
        }];
        
        
        
        self.localtionLbl = [ControllerHelper autoFitLabel];
        self.localtionLbl.text = @"显示位置";
        self.localtionLbl.font = SystemFont(14);
        [_locationView addSubview:_localtionLbl];
        
        [self.localtionLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(imageView.mas_right);
            make.right.equalTo(_locationView.mas_right).offset(-10);
            make.centerY.equalTo(_locationView);
        }];
        
        
        [RACObserve(self, selectPOI) subscribeNext:^(id x) {
            if (x) {
                imageView.image = [[UIImage imageNamed:@"ic_location_black"] imageToColor:AppOrange];
                self.localtionLbl.text = [NSString stringWithFormat:@"%@",self.selectPOI.city_name];
                self.localtionLbl.textColor = AppOrange;
            }else{
                imageView.image = [UIImage imageNamed:@"ic_location_black"];
                self.localtionLbl.text = @"显示位置";
                self.localtionLbl.textColor = [UIColor blackColor];
            }
        }];
        
        
        
        [self.view addSubview:_locationView];
    }
    return _locationView;
}




-(UIButton *)locationBtn{
    if (!_locationBtn) {
        _locationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_locationBtn setTitle:@"location" forState:UIControlStateNormal];
        [_locationBtn addTarget:self action:@selector(locationBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_locationBtn];
    }
    return _locationBtn;
}

-(void)publishBtnClick:(id)sender{
    
    [self.header.textView resignFirstResponder];
    
    DynamicUploadModel *upModel = [[DynamicUploadModel alloc]init];
    upModel.desc = self.header.textView.text;
    upModel.imageDatas = self.header.images;
    if (self.selectPOI) {
        upModel.address = self.selectPOI.address;
        upModel.longitude = self.selectPOI.longitude;
        upModel.latitude = self.selectPOI.latitude;
        upModel.is_location = 1;
    }
    
    if (self.header.images.count > 0){
        UploadObject *obj = self.header.images[0];
        upModel.is_video = obj.videoUrl != nil;
    }
    
    WEAK_SELF;
    [[DynamicUploadManager sharedManager] putDynamicModel:upModel block:^(id responseObj) {
        PostNotification(AddDynamciNotification, [DynamicModel objectWithKeyValues:[responseObj objectForKey:@"data"]]);
        [weakSelf dismissViewControllerAnimated:YES completion:NULL];
    }];
}

-(void)cancelBtnClick:(id)sender{
    [self dismissViewControllerAnimated:YES completion:NULL];
}




-(void)locationBtnClick:(id)sender{
    
    if (self.selectPOI){
        self.selectPOI = nil;
    }else{
        [RACObserve([RCUserCacheManager sharedManager], addressPoi) subscribeNext:^(CityModel *x) {
//            AMapPOI *poi = [[AMapPOI alloc]init];
//            poi.city = x.city_name;
            self.selectPOI = x;
        }];
        if (![RCUserCacheManager sharedManager].addressPoi)
            [[RCUserCacheManager sharedManager] getAddress];

    }
    
    
    
//    GaoDeLocationVC *vc = [[GaoDeLocationVC alloc]init];
//    vc.delegate = self;
//    UINavigationController *navigationVC = [[UINavigationController alloc] initWithRootViewController:vc];
//    [self presentViewController:navigationVC animated:YES completion:NULL];
}


#pragma mark -


-(void)didSelectAddress:(AMapPOI *)poi city:(NSString *)city{
//    self.selectPOI = poi;
    if (poi)
        self.localtionLbl.text = [NSString stringWithFormat:@"%@",poi.city];
    else
        self.localtionLbl.text = @"显示位置";
}

- (void)didCancelLocationViewController:(LLGaoDeLocationViewController *)locationViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)asyncTakeCenterSnapshotDidComplete:(UIImage *)resultImage forMessageModel:(LLMessageModel *)messageModel {
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
