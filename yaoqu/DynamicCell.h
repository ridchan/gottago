//
//  DynamicCell.h
//  yaoqu
//
//  Created by ridchan on 2017/6/28.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DynamicModel.h"
@interface DynamicCell : UITableViewCell


@property(nonatomic,strong) DynamicModel *model;

@end
