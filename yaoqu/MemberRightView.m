//
//  MemberRightView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/11.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MemberRightView.h"
#import "MemberRightModel.h"
@interface MemberRightView()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) UIView *titleView;

@end

@implementation MemberRightView


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left);
            make.right.equalTo(self.mas_right);
            make.top.equalTo(self.mas_top);
            make.height.mas_equalTo(40);
        }];
        
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left);
            make.right.equalTo(self.mas_right);
            make.top.equalTo(self.titleView.mas_bottom);
            make.bottom.equalTo(self.mas_bottom);
            make.height.mas_equalTo(0);
        }];
    }
    return self;
}

-(UIView *)titleView{
    if (!_titleView) {
        _titleView = [[UIView alloc]init];
        _titleView.backgroundColor = [UIColor whiteColor];
        UILabel *label = [ControllerHelper autoFitLabel];
        label.text = @"特权说明";
        label.font = SystemFont(16);
        [_titleView addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_titleView.mas_left).offset(10);
            make.top.equalTo(_titleView.mas_top);
            make.bottom.equalTo(_titleView.mas_bottom);
        }];
        
        UIView *line = [[UIView alloc]init];
        line.backgroundColor = AppLineColor;
        [_titleView addSubview:line];
        
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_titleView.mas_left);
            make.right.equalTo(_titleView.mas_right);
            make.bottom.equalTo(_titleView.mas_bottom);
            make.height.mas_equalTo(0.5);
        }];
        
        [self addSubview:_titleView];
    }
    return _titleView;
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addSubview:_tableView];
    }
    return _tableView;
}


-(void)setDatas:(NSArray *)datas{
    _datas = datas;
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.titleView.mas_bottom);
        make.bottom.equalTo(self.mas_bottom);
        make.height.mas_equalTo(40.0 * _datas.count);
    }];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.textLabel.font = SystemFont(12);
        cell.textLabel.textColor = AppGray;
    }
    MemberRightModel *model = self.datas[indexPath.row];
    cell.textLabel.text = model.desc;
    return cell;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
