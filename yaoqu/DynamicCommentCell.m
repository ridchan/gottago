//
//  DynamicCommentCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/29.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicCommentCell.h"
#import "SexView.h"
#import "DynamicCommentLikeApi.h"
#import "CommentApi.h"

@interface DynamicCommentCell()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UIImageView *userImage;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) SexView *sexImage;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UILabel *dateLbl;
@property(nonatomic,strong) ImageButton *likeBtn;
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) UIView *bottomLine;


@end

@implementation DynamicCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        
        [self commonInit2];
        [self rac_prepareForReuseSignal];
        
        [RACObserve(self, model) subscribeNext:^(DynamicCommentModel *x) {
            [self.userImage setImageName:x.image placeholder:nil];
            self.nameLbl.text = x.username;
            self.descLbl.text = x.content;
            self.dateLbl.text = [NSString compareCurrentTime:x.time];
//            self.likeBtn.titleLbl.text = x.like;
            self.sexImage.sexType = [x.sex integerValue];
        }];
        
        
        [RACObserve(self, model.is_like) subscribeNext:^(id x) {
            self.likeBtn.contentImage.image = [x boolValue] ? [UIImage imageNamed:@"ic_dynamic_like_on"] : [[UIImage imageNamed:@"ic_dynamic_like"] imageToColor:AppBlack];
        }];
        
        
        
        RAC(self.likeBtn.titleLbl,text) =  RACObserve(self, model.like);
        
        [RACObserve(self.model, comment_data) subscribeNext:^(id x) {
            [self.tableView reloadData];
        }];
        
    }
    return self;
}


-(void)commonInit2{
    self.userImage.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .topSpaceToView(self.contentView, 10)
    .heightIs(40)
    .widthIs(40);
    self.userImage.layer.cornerRadius = 20;
    self.userImage.layer.masksToBounds = YES;
    
    self.nameLbl.sd_layout
    .autoWidthRatio(0)
    .heightIs(30)
    .leftSpaceToView(self.userImage, 5)
    .centerYEqualToView(self.userImage);
    [self.nameLbl setSingleLineAutoResizeWithMaxWidth:200];
    
    self.sexImage.sd_layout
    .leftSpaceToView(self.nameLbl, 5)
    .centerYEqualToView(self.nameLbl)
    .heightIs(15)
    .widthIs(15);
    
    self.descLbl.sd_layout
    .leftEqualToView(self.nameLbl)
    .topSpaceToView(self.userImage, 10)
    .rightSpaceToView(self.contentView, 10)
    .autoHeightRatio(0);
    
    self.dateLbl.sd_layout
    .leftEqualToView(self.nameLbl)
    .topSpaceToView(self.descLbl, 10)
    .heightIs(20)
    .autoWidthRatio(0);
    [self.dateLbl setSingleLineAutoResizeWithMaxWidth:200];
    
    self.likeBtn.sd_layout
    .centerYEqualToView(self.dateLbl)
    .rightSpaceToView(self.contentView,10)
    .widthIs(50)
    .heightIs(25);
    
    
    self.tableView.sd_layout
    .topSpaceToView(self.dateLbl, 10)
    .leftEqualToView(self.nameLbl)
    .rightSpaceToView(self.contentView, 10)
    .heightIs(0);
    
    self.bottomLine.sd_layout
    .leftEqualToView(self.contentView)
    .rightEqualToView(self.contentView)
    .topSpaceToView(self.tableView, 10)
    .heightIs(0.5);
    
    [self setupAutoHeightWithBottomView:self.bottomLine bottomMargin:0];
    
    
}


-(void)commonInit{
    [self.userImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.equalTo(self.contentView).offset(10);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    self.userImage.layer.cornerRadius = 20;
    self.userImage.layer.masksToBounds = YES;
    
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.userImage.mas_centerY);
        make.left.equalTo(self.userImage.mas_right).offset(5);
    }];
    
    
    
    [self.sexImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_right).offset(5);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.userImage.mas_bottom).offset(20);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
    }];
    
    [self.dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.descLbl.mas_bottom).offset(10);
        make.bottom.equalTo(self.tableView.mas_top).offset(-10);//???
    }];
    
    [self.likeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.centerY.equalTo(self.dateLbl.mas_centerY);
        make.height.mas_equalTo(25);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.dateLbl.mas_bottom).offset(10);///???
        make.left.equalTo(self.nameLbl.mas_left);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-10);
    }];
    
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.right.equalTo(self.contentView.mas_right);
        make.height.mas_equalTo(0.5);
    }];
}

-(void)setModel:(DynamicCommentModel *)model{
    _model = model;
    if (_noDetailComment) return;
    CGFloat height =  30.0 * MIN(self.model.comment_data.count,3);
    
    self.tableView.sd_layout
    .topSpaceToView(self.dateLbl, 10)
    .leftEqualToView(self.nameLbl)
    .rightSpaceToView(self.contentView, 10)
    .heightIs(height);
    
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark lazy layout

-(UIImageView *)userImage{
    if (!_userImage) {
        _userImage = [[UIImageView alloc]init];
        _userImage.contentMode = UIViewContentModeScaleAspectFill;
        _userImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.contentView addSubview:_userImage];
    }
    return _userImage;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        _nameLbl.font = DynamicCommentNameFont;
        _nameLbl.textColor = DynamicCommentNameColor;
        [self.contentView addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(SexView *)sexImage{
    if (!_sexImage) {
        _sexImage = [[SexView alloc]init];
        [self.contentView addSubview:_sexImage];
    }
    return _sexImage;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.numberOfLines = 0;
        _descLbl.font = DynamicCommentDescFont;
        _descLbl.textColor = DynamicCommentDescColor;
        [self.contentView addSubview:_descLbl];
    }
    return _descLbl;
}

-(UILabel *)dateLbl{
    if (!_dateLbl) {
        _dateLbl = [ControllerHelper autoFitLabel];
        _dateLbl.font = DynamicCommentTimeFont;
        _dateLbl.textColor = DynamicCommentTimeColor;
        [self.contentView addSubview:_dateLbl];
    }
    return _dateLbl;
}

-(ImageButton *)likeBtn{
    if (!_likeBtn) {
        _likeBtn = [[ImageButton alloc]init];
        _likeBtn.contentImage.image = [UIImage imageNamed:@"ic_dynamic_like"];
        [_likeBtn addTarget:self action:@selector(likeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_likeBtn];
    }
    return _likeBtn;
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.scrollEnabled = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.contentView addSubview:_tableView];
    }
    return _tableView;
}

-(UIView *)bottomLine{
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc]init];
        _bottomLine.backgroundColor = AppLineColor;
        [self.contentView addSubview:_bottomLine];
    }
    return _bottomLine;
}


-(void)likeBtnClick:(id)sender{
//    [self.likeBtn.contentImage.layer addAnimation:[self.likeBtn keyAnimation] forKey:nil];
//    if (self.model.is_like) {
//        
//    }
    [self.likeBtn.contentImage.layer addAnimation:[self.likeBtn keyAnimation] forKey:nil];
    
    WEAK_SELF;
    
    NSDictionary *dict = @{@"comment_id":self.model.comment_id,
                           @"state":@(!self.model.is_like)
                           };
    CommentApi *api = [[CommentApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodOPTION;
    
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            weakSelf.model.is_like = !weakSelf.model.is_like;
            weakSelf.model.like = weakSelf.model.is_like ?
            [weakSelf.model.like autoIncrice] :
            [weakSelf.model.like autoReduce];
        }else{
            
        }
    }];
    
}

#pragma mark -
#pragma mark tabelview 

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0001;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  MIN(self.model.comment_data.count,3);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    DynamicCommentModel *model = self.model.comment_data[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@:%@",model.username,model.content];
    return cell;
}




@end
