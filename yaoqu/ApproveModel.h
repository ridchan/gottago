//
//  ApproveModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/10.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"



@interface ApproveUploadModel : BaseModel

@property(nonatomic,strong) NSString *fullname;
@property(nonatomic,strong) NSString *idcard;
@property(nonatomic,strong) NSString *positive;
@property(nonatomic,strong) NSString *obverse;

@property(nonatomic,strong) UIImage *positiveImage;
@property(nonatomic,strong) UIImage *obverseImage;



@end


@interface ApproveModel : ApproveUploadModel
//
@property(nonatomic,strong) NSString *member_id;
@property(nonatomic,strong) NSString *approve_id;
@property(nonatomic) long long time;
@property(nonatomic,strong) NSString *bank_card;
@property(nonatomic,strong) NSString *receipt_time;
@property(nonatomic,strong) NSString *receipt_desc;
@property(nonatomic,strong) NSString *admin_id;



@end
