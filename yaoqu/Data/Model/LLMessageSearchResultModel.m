//
//  LLMessageSearchResultModel.m
//  LLWeChat
//
//  Created by GYJZH on 06/10/2016.
//  Copyright © 2016 GYJZH. All rights reserved.
//

#import "LLMessageSearchResultModel.h"
#import "LLUtils.h"
#import "RCUserCacheManager.h"
#import "EMTextMessageBody.h"
@implementation LLMessageSearchResultModel

- (instancetype)initWithMessage:(EMMessage *)message {
    self = [super init];
    if (self) {
        _sdk_message = message;
        _nickName = [message.conversationId copy];
        _timestamp = adjustTimestampFromServer(message.timestamp);
        
        if (message.body.type == EMMessageBodyTypeText) {
            EMTextMessageBody *body = (EMTextMessageBody *)message.body;
            self.text = body.text;
        }
        
        WEAK_SELF;
        [[RCUserCacheManager sharedManager] getUserWithID:message.conversationId block:^(id responseObj) {
            NSLog(@"会话搜索 返回用户信息 %@",[responseObj keyValues]);
            weakSelf.userModel = responseObj;
        }];
        
    }
    
    return self;
}

@end
