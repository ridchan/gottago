//
//  DynamicUploadManager.h
//  yaoqu
//
//  Created by ridchan on 2017/6/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewDynamicApi.h"
#import "ApproveModel.h"

@interface DynamicUploadManager : NSObject

+(instancetype)sharedManager;


//上传
-(void)putDynamicModel:(DynamicUploadModel *)upModel block:(BaseBlock)block;

-(void)putApprove:(ApproveUploadModel *)upModel block:(BaseBlock)block;;

//
@property(nonatomic,strong) NSMutableArray *myDynamicList;
@property(nonatomic,strong) NSMutableArray *allDynamicList;

-(void)getMyDynamicLis:(NSString *)member_id page:(NSInteger)page block:(BaseBlock)block;
-(void)getAllDynamicList:(NSInteger)page block:(BaseBlock)block;

@property(nonatomic,strong) NSMutableArray *dynamicImages;




-(void)putDynamicVedio:(id)asset;
-(void)putImage:(id)asset block:(BaseBlock)block;


@end
