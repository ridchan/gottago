//
//  NetImagePreviewVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/29.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseViewController.h"


@interface PreviewCell : UICollectionViewCell<UIGestureRecognizerDelegate,UIScrollViewDelegate>

@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UIScrollView *scView;


@end


@protocol NetImagePreviewVCDelegate <NSObject>

@optional

-(NSUInteger)numOfPreviewer:(id)obj;
-(UIImageView *)previewImage:(id)obj atIndex:(NSInteger)index;
-(NSString *)originalImage:(id)obj atIndex:(NSInteger)index;

@end

@interface NetImagePreviewVC : BaseViewController

@property(nonatomic,weak) id<NetImagePreviewVCDelegate> delegate;


@property(nonatomic) NSInteger showIndex;
-(void)show;

@end
