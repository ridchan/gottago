//
//  TencentServices.m
//  shiyi
//
//  Created by 陳景雲 on 16/5/3.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import "TencentServices.h"




@implementation TencentServices


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    
    [WXApi registerApp:WX_APP_ID];
    self.tencentOAuth = [[TencentOAuth alloc]initWithAppId:QQ_APP_ID andDelegate:self];
    self.tencentOAuth.redirectURI = @"www.qq.com";

    return YES;
}


-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    if ([url.absoluteString hasPrefix:@"tencent"]){
        if ([url.absoluteString rangeOfString:@"response_from_qq"].location != NSNotFound){
            if ([url.absoluteString rangeOfString:@"error=0"].location != NSNotFound){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"WXShareNotification" object:[NSNumber numberWithInt:0]];
            }
        }
        return [TencentOAuth HandleOpenURL:url];
    }else
        return [WXApi handleOpenURL:url delegate:self];
}


-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    if ([url.absoluteString hasPrefix:@"tencent"]){
        return [TencentOAuth HandleOpenURL:url];
    }else
        return [WXApi handleOpenURL:url delegate:self];
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    if ([url.absoluteString hasPrefix:@"tencent"]){
        return [TencentOAuth HandleOpenURL:url];
    }else
        return [WXApi handleOpenURL:url delegate:self];
    
    
    
}



//

-(void)onResp:(BaseResp *)resp{
    if ([resp isKindOfClass:[SendAuthResp class]]) {
        SendAuthResp *sendResp = (SendAuthResp *)resp;

        
        if (!sendResp.code) {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"WXLoginResult" object:@"fail"];
            return;
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"WXLoginStart" object:nil];
        
//        NSString *link = [NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=%@&secret=%@&code=%@&grant_type=authorization_code",WXAppID,WXAppSecret,sendResp.code];
        
//        [NetWorkObj get:link block:^(id Obj) {
//            NSDictionary *info = (NSDictionary *)Obj;
//            NSString *link2 = [NSString stringWithFormat:@"https://api.weixin.qq.com/sns/userinfo?access_token=%@&openid=%@",[info strForKey:@"access_token"],[info strForKey:@"openid"]];
//            [NetWorkObj get:link2 block:^(id Obj) {
//                NSString *identifierForVendor = [[UIDevice currentDevice].identifierForVendor UUIDString];
//                NSDictionary *params = @{@"type":@"0",
//                                         @"only_id":[Obj strForKey:@"unionid"],
//                                         @"name":[Obj strForKey:@"nickname"],
//                                         @"img":[Obj strForKey:@"headimgurl"],
//                                         @"sex":[Obj strForKey:@"sex"],
//                                         @"imei":identifierForVendor,
//                                         };
//                [NetWorkObj query:nil info:params block:^(id Obj) {
//                    if ([Obj boolForKey:@"state"]) {
//                        NSDictionary *userInfo = [[Obj objectForKey:@"data"] objectForKey:@"user"];
//                        
//                        UserModel *userModel = [GenericModel getObjectByDictionary:userInfo clazz:[UserModel class]];
//                        
//                        [NetWorkObj setUserInfo:userInfo];
//                        
//                        [[NetWorkObj DB] setObject:userModel.token forKey:@"UserToken"];
//                        [[NetWorkObj DB] setObject:userInfo forKey:@"UserInfo"];
//
////                        [RCNetworkObj setUserInfo:userInfo];
////                        [RCNetworkObj setUserToken:[userInfo strForKey:@"token"]];
////                        [RCDataObj saveUserInfo:userInfo];
//                        [[NSNotificationCenter defaultCenter]postNotificationName:@"WXLoginResult" object:@"success"];
//                    }else{
//                        [[NSNotificationCenter defaultCenter]postNotificationName:@"WXLoginResult" object:@"fail"];
//                    }
//                }];
//            }];
//            
//        }];
        
    }else if ([resp isKindOfClass:[SendMessageToWXResp class]]){
        SendMessageToWXResp *wxResp = (SendMessageToWXResp *)resp;
        if (wxResp.errCode == 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"WXShareNotification" object:[NSNumber numberWithInt:wxResp.errCode]];
        }
    }else if ([resp isKindOfClass:[PayResp class]]){
        PayResp *payResp = (PayResp *)resp;
        /*
         payResp.errCode == 0 ：支付成功
         payResp.errCode == -2 ：支付失败
         */
        NSString *errorCode = payResp.errCode == 0 ? @"9000" : @"9001";
        
        [[NSNotificationCenter defaultCenter] postNotificationName:WeChatPayNotification object:@{@"resultStatus":errorCode,@"memo":payResp.returnKey}];
        
    }
}


//QQ登陆

-(void)tencentDidLogin{
    [_tencentOAuth getUserInfo];
}

-(void)tencentDidNotLogin:(BOOL)cancelled{
    
}

-(void)tencentDidNotNetWork{
    
}




-(NSArray *)getAuthorizedPermissions:(NSArray *)permissions withExtraParams:(NSDictionary *)extraParams{
    return @[kOPEN_PERMISSION_GET_USER_INFO];
}



@end
