//
//  UserInfoGetApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface UserInfoGetApi : BaseApi

-(id)initWithMemberID:(NSString *)member_id;

@end
