//
//  TravelEnrollTitleView.m
//  yaoqu
//
//  Created by ridchan on 2017/7/16.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelEnrollTitleView.h"

@interface TravelEnrollTitleView()

@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UILabel *dateLbl;
@property(nonatomic,strong) UIImageView *dateImg;

@end

@implementation TravelEnrollTitleView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

-(void)commonInit{
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.mas_top).offset(10);
        make.bottom.equalTo(self.mas_bottom).offset(-10);
        make.height.mas_equalTo(120);
        make.width.mas_equalTo(150);
    }];
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.imageView.mas_right).offset(10);
        make.right.equalTo(self.mas_right).offset(-10);
        make.top.equalTo(self.imageView.mas_top);
    }];
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl.mas_left);
        make.top.equalTo(self.titleLbl.mas_bottom).offset(5);
    }];
    
    [self.dateImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl.mas_left);
        make.bottom.equalTo(self.imageView.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    [self.dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.dateImg.mas_right).offset(5);
        make.centerY.equalTo(self.dateImg.mas_centerY);
    }];
    
}

#pragma mark -

-(void)setScheduleModel:(TravelScheduleModel *)scheduleModel{
    _scheduleModel = scheduleModel;
    [self.imageView setImageName:scheduleModel.image placeholder:nil];
    self.titleLbl.text = scheduleModel.title;
    
    self.descLbl.text = [NSString stringWithFormat:@"%@%@/人 剩余%ld/%@",MoneySign,scheduleModel.price,[scheduleModel.room_use integerValue],scheduleModel.room_qty];
    self.dateLbl.text = [NSString stringFromTimeMark:scheduleModel.start_time format:KDateFormatTypeYMDHM];
    
    
}

#pragma mark -
#pragma mark lazy layout

-(UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc]init];
        [self addSubview:_imageView];
    }
    return _imageView;
}


-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        _titleLbl.numberOfLines = 2;
        [self addSubview:_titleLbl];
    }
    return _titleLbl;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        [self addSubview:_descLbl];
    }
    return _descLbl;
}

-(UIImageView *)dateImg{
    if (!_dateImg) {
        _dateImg = [[UIImageView alloc]init];
        _dateImg.image = [UIImage imageNamed:@"ic_paper_plain"];
        _dateImg.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_dateImg];
    }
    return _dateImg;
}

-(UILabel *)dateLbl{
    if (!_dateLbl) {
        _dateLbl = [ControllerHelper autoFitLabel];
        _dateLbl.font = SystemFont(12);
        _dateLbl.textColor = [UIColor orangeColor];
        [self addSubview:_dateLbl];
    }
    return _dateLbl;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
