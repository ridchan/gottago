//
//  LoginVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/15.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "LoginVC.h"
#import "LoginViewModel.h"
#import "RegisterVC.h"

@interface LoginVC ()

@property(nonatomic,strong) LoginViewModel *viewModel;
@property(nonatomic,strong) UIImageView *backgroundImage;
@property(nonatomic,strong) ImageButton *wxBtn;
@property(nonatomic,strong) ImageButton *qqBtn;


@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.viewModel = [[LoginViewModel alloc]init];
    self.viewModel.presentVC = self;
    
    [self.backgroundImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.bottom.equalTo(self.view.mas_bottom);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top).offset(-64);
    }];
    
//    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.view.mas_top).offset(20);
//        make.left.equalTo(self.view.mas_left);
//        make.right.equalTo(self.view.mas_right);
//        make.height.mas_equalTo(40);
//    }];
    CGFloat width = APP_WIDTH / 2 - 20;
    [[self wxView] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(width, 40));
        make.centerX.equalTo(self.view.mas_centerX).multipliedBy(0.5);
        make.bottom.equalTo(self.view.mas_bottom).offset(-50);
    }];
    
    [[self qqView] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(width, 40));
        make.centerX.equalTo(self.view.mas_centerX).multipliedBy(1.5);
        make.bottom.equalTo(self.view.mas_bottom).offset(-50);
    }];
    
    [self.wxBtn addTarget:self.viewModel action:@selector(loginBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.qqBtn addTarget:self.viewModel action:@selector(loginBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
//    [[self.viewModel succSubject] subscribeNext:^(UserModel *x) {
//        [[RouteController sharedManager] openMainController];
//    }];
    
    [[self.viewModel failSubject]subscribeNext:^(LoginPostModel *x) {
        RegisterVC *vc = [[RegisterVC alloc]init];
        vc.viewModel.model = x;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
    
    [self wr_setNavBarBackgroundAlpha:0.0];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


//加载


//-(UIButton *)loginBtn{
//    if (!_loginBtn) {
//        _loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [self.view addSubview:_loginBtn];
//        
//        UILabel *label = [[UILabel alloc]init];
//        label.text = LS(@"Login");
//        label.textAlignment = NSTextAlignmentCenter;
//        [label setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
//        label.minimumScaleFactor = 0.5;
//        label.textColor = [UIColor whiteColor];
//        [_loginBtn addSubview:label];
//        [label mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerX.equalTo(_loginBtn.mas_centerX);
//            make.top.equalTo(_loginBtn.mas_top);
//            make.bottom.equalTo(_loginBtn.mas_bottom);
//        }];
//        
//        UIView *leftLine = [[UIView alloc]init];
//        leftLine.backgroundColor = [UIColor whiteColor];
//        [_loginBtn addSubview:leftLine];
//        [leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(_loginBtn.mas_left).offset(20);
//            make.right.equalTo(label.mas_left).offset(-20);
//            make.centerY.equalTo(_loginBtn.mas_centerY);
//            make.height.mas_equalTo(5);
//        }];
//        
//        UIView *rightLine = [[UIView alloc]init];
//        rightLine.backgroundColor = [UIColor whiteColor];
//        [_loginBtn addSubview:rightLine];
//        [rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(label.mas_right).offset(20);
//            make.right.equalTo(_loginBtn.mas_right).offset(-20);
//            make.centerY.equalTo(_loginBtn.mas_centerY);
//            make.height.mas_equalTo(5);
//        }];
//        
//        
//    }
//    return _loginBtn;
//}


-(UIImageView *)backgroundImage{
    if (!_backgroundImage) {
        _backgroundImage = [[UIImageView alloc]init];
        _backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
        _backgroundImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _backgroundImage.image = [UIImage imageNamed:@"login_backgroundImage"];
        [self.view addSubview:_backgroundImage];
    }
    return _backgroundImage;
}

-(UIView *)wxView{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = RGB16withAlpha(0xff8029, 0.8);
//    view.alpha = 0.5;
    view.layer.cornerRadius = 3;
    view.layer.masksToBounds = YES;
    
    [view addSubview:self.wxBtn];
    [self.wxBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 30));
        make.centerX.equalTo(view.mas_centerX);
        make.centerY.equalTo(view.mas_centerY);
    }];
    [self.view addSubview:view];

    
    return view;
}


-(UIView *)qqView{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = RGB16withAlpha(0xff8029, 0.8);
//    view.alpha = 0.5;
    view.layer.cornerRadius = 3;
    view.layer.masksToBounds = YES;
    
    [view addSubview:self.qqBtn];
    
    [self.qqBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 30));
        make.centerX.equalTo(view.mas_centerX);
        make.centerY.equalTo(view.mas_centerY);
    }];
    [self.view addSubview:view];
    return view;
}

-(ImageButton *)wxBtn{
    if (!_wxBtn) {
        _wxBtn = [[ImageButton alloc]init];
        _wxBtn.contentImage.image = [[UIImage imageNamed:@"ic_wechat_gray"] imageToColor:[UIColor whiteColor]];
        
        _wxBtn.titleLbl.font = SystemFont(14);
        _wxBtn.titleLbl.text = @"微信登陆";
        _wxBtn.titleLbl.textColor = [UIColor whiteColor];
        _wxBtn.tag = LoginType_Wechat;

        
    }
    return _wxBtn;
}

-(ImageButton *)qqBtn{
    if (!_qqBtn) {
        _qqBtn = [[ImageButton alloc]init];
        _qqBtn.contentImage.image = [[UIImage imageNamed:@"ic_qq_gray"] imageToColor:[UIColor whiteColor]];
        _qqBtn.titleLbl.font = SystemFont(14);
        _qqBtn.titleLbl.text = @"QQ登陆";
        _qqBtn.titleLbl.textColor = [UIColor whiteColor];
        _qqBtn.tag = LoginType_QQ;
    
        
    }
    return _qqBtn;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
