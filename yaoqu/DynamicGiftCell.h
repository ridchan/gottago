//
//  DynamicGiftCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/29.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DynamicModel.h"

@interface DynamicGiftCell : UITableViewCell

@property(nonatomic,strong) DynamicModel *model;

@end
