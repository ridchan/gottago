//
//  UserEditVC.m
//  yaoqu
//
//  Created by ridchan on 2017/6/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserEditVC.h"
#import "UserImageEditor.h"
#import "NormalTableViewCell.h"
#import "UserInfoGetApi.h"
#import "RCUserCacheManager.h"
#import "UserEditApi.h"
#import "UserLocationEditVC.h"
#import "MemberApi.h"

@interface UserEditVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) UserImageEditor *imageEditor;
@property(nonatomic,strong) NSArray *datas;

@property(nonatomic,strong) UserModel *editModel;


@end

@implementation UserEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [[[UserInfoGetApi alloc]init] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
//        
//    }];
    
    self.editModel = [[RCUserCacheManager sharedManager].currentUser deepCopy];
    [self commonInit];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [self wr_setStatusBarStyle:UIStatusBarStyleDefault];
    [super viewWillAppear:animated];
    
}

-(void)commonInit{
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    self.title = LS(@"个人资料");
    [self setRightItemWithTitle:LS(@"保存") selector:@selector(saveBtnClick:)];
    self.datas = @[@[LS(@"昵称")],
                   @[LS(@"性别"),LS(@"年龄"),LS(@"情感状态"),LS(@"交友意向")],
                   @[LS(@"位置"),LS(@"家乡")],
                   @[LS(@"简介")]
                   ];
    
    
    self.imageEditor.datas = [NSMutableArray arrayWithArray:[self.editModel.images JSONObject]];
    
    [RACObserve(self.imageEditor, dataCount) subscribeNext:^(id x) {
        [self.tableView reloadData];
    }];
    
    
}


-(void)saveBtnClick:(id)sender{
    self.editModel.images = (NSArray *)[self.imageEditor.datas JSONString];
    WEAK_SELF;
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:[self.imageEditor.datas JSONString] forKey:@"images"];
    [dict setValue:self.editModel.username forKey:@"username"];
    [dict setValue:self.editModel.sex forKey:@"sex"];
    [dict setValue:self.editModel.desc forKey:@"desc"];
    [dict setValue:self.editModel.age forKey:@"age"];
    [dict setValue:self.editModel.birthday forKey:@"birthday"];
    [dict setValue:self.editModel.constellation forKey:@"constellation"];
    [dict setValue:self.editModel.sex forKey:@"sex"];
    [dict setValue:@(self.editModel.is_show_from) forKey:@"is_show_from"];
    [dict setValue:@(self.editModel.is_show_location) forKey:@"is_show_location"];
    [dict setValue:self.editModel.pro_id forKey:@"pro_id"];
    [dict setValue:self.editModel.pro_name forKey:@"pro_name"];
    [dict setValue:self.editModel.city_id forKey:@"city_id"];
    [dict setValue:self.editModel.city_name forKey:@"city_name"];
    [dict setValue:self.editModel.home_pro_id forKey:@"home_pro_id"];
    [dict setValue:self.editModel.home_pro_name forKey:@"home_pro_name"];
    [dict setValue:self.editModel.home_city_id forKey:@"home_city_id"];
    [dict setValue:self.editModel.home_city_name forKey:@"home_city_name"];
    [dict setValue:self.editModel.sex_orientation forKey:@"sex_orientation"];
    [dict setValue:self.editModel.make_friends forKey:@"make_friends"];
    
    
    __block MBProgressHUD *hud = [LLUtils showActivityIndicatiorHUDWithTitle:@"更新中..."];
    
    MemberApi *api = [[MemberApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [hud hideAnimated:NO];
        if (info.error == ErrorCodeType_None) {
//            [RCUserCacheManager sharedManager].currentUser = [UserModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
            [[RCUserCacheManager sharedManager] renewAccount];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else{
            
            [LLUtils showCenterTextHUD:info.message];
        }
        
    }];
    
//    [[[UserEditApi alloc]initWithUserModel:self.editModel] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
//        if (info.state) {
//            [MBProgressHUD hideHUD];
//        }else{
//            [MBProgressHUD showInfoTip:info.msg];
//        }
//        [RCUserCacheManager sharedManager].currentUser = [UserModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
//        
//        [weakSelf.navigationController popViewControllerAnimated:YES];
//    }];
}

-(UserImageEditor *)imageEditor{
    if (!_imageEditor) {
        _imageEditor = [[UserImageEditor alloc]init];
        _imageEditor.frame = CGRectMake(0, 0, APP_WIDTH, APP_WIDTH / 4.0);
        
    }
    return _imageEditor;
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = self.imageEditor;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.datas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[self.datas objectAtIndex:section] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NormalTableViewCell *cell = [NormalTableViewCell cellWithType:NormalCellType_Access_Detail identifier:@"Cell" tableView:tableView];
    
    cell.type = indexPath.item == 0 ? NormalCellType_Either : NormalCellType_BottomLine;
    NSArray *arr = self.datas[indexPath.section];
    cell.textLabel.text = arr[indexPath.row];
    [self bindTableView:cell  indexPath:indexPath];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section == 0 ? 30 : 10;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section != 0) return  nil;
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 20)];
    UILabel *label = [[UILabel alloc]init];
    label.textColor = FontGray;
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 2;
    label.text = LS(@"拖动移动图片，最多8张");
    label.frame = CGRectMake(10, 10, APP_WIDTH - 20, 20);
    
    [view addSubview:label];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0001;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        [self pushViewControllerWithName:@"UserNameEditVC" params:self.editModel];
    }else if (indexPath.section == 99){
        [self pushViewControllerWithName:@"UserSafeEditVC" params:self.editModel];
    }else if (indexPath.section == 1){
        if (indexPath.row == 0){
            [self pushViewControllerWithName:@"UserSexEditVC" params:self.editModel];
        }else if (indexPath.row == 1){
            [self pushViewControllerWithName:@"UserAgeEditVC" params:self.editModel];
        }else if (indexPath.row == 2){
            [self pushViewControllerWithName:@"UserEmotionalVC" params:self.editModel];
        }else{
            [self pushViewControllerWithName:@"UserIntentionVC" params:self.editModel];
        }
    }else if (indexPath.section == 3){
        [self pushViewControllerWithName:@"UserDescEditVC" params:self.editModel];
    }else if (indexPath.section == 2){
        UserLocationEditVC *vc = [[UserLocationEditVC alloc]init];
        vc.paramObj = self.editModel;
        vc.addressType = indexPath.row;
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    
}

#pragma mark -
#pragma makr 绑定

-(void)bindTableView:(NormalTableViewCell *)cell indexPath:(NSIndexPath *)indexPath{
    
    
    
    
    [cell rac_prepareForReuseSignal];
    
    
    
    if (indexPath.section == 0) {
        [RACObserve(self.editModel, username) subscribeNext:^(id x) {
            cell.detailLbl.text = x;
        }];

    }else if (indexPath.section == 1){
        if (indexPath.row == 0) {
            [RACObserve(self.editModel, sex) subscribeNext:^(id x) {
                cell.detailLbl.text = [x isEqualToString:@"0"] ? @"女" : @"男";
            }];
        }else if (indexPath.row == 1){
            
            [RACObserve(self.editModel, age) subscribeNext:^(id x) {
                cell.detailLbl.text = x;
            }];
        }else if (indexPath.row == 2){
            [RACObserve(self.editModel, sex_orientation) subscribeNext:^(id x) {
                cell.detailLbl.text = x;
            }];
        }else if (indexPath.row == 3){
            [RACObserve(self.editModel, make_friends) subscribeNext:^(id x) {
                cell.detailLbl.text = x;
            }];
        }
    }else if (indexPath.section == 2) {
        
        if (indexPath.row == 0){
            RACSignal *signA = RACObserve(self.editModel,pro_name);
            RACSignal *signB = RACObserve(self.editModel,city_name);
            
            RACSignal *reduceSignal = [RACSignal combineLatest:@[signA,signB] reduce:^id(NSString *pro ,NSString *city){
                return [NSString stringWithFormat:@"%@ %@",pro,city];
            }];
            
            [reduceSignal subscribeNext:^(id x) {
                cell.detailLbl.text = x;
            }];
        }else{
            RACSignal *signA = RACObserve(self.editModel,home_pro_name);
            RACSignal *signB = RACObserve(self.editModel,home_city_name);
            
            RACSignal *reduceSignal = [RACSignal combineLatest:@[signA,signB] reduce:^id(NSString *pro ,NSString *city){
                return [NSString stringWithFormat:@"%@ %@",pro,city];
            }];
            
            [reduceSignal subscribeNext:^(id x) {
                cell.detailLbl.text = x;
            }];
        }


        
    }else if (indexPath.section == 3){
        //RAC(cell,detailLbl.text) =
        [RACObserve(self.editModel, desc) subscribeNext:^(id x) {
            cell.detailLbl.text = x;
        }];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
