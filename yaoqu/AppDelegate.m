//
//  AppDelegate.m
//  yaoqu
//
//  Created by ridchan on 2017/6/20.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "AppDelegate.h"
#import "SOAComponentAppDelegate.h"
#import "RouteController.h"
#import "ConversationListVC.h"
#import "DiscoverVC.h"
#import "MineVC.h"
#import <JPush/JPUSHService.h>
#import <AlipaySDK/AlipaySDK.h>
#import "LLUtils.h"
#import <UMSocialCore/UMSocialCore.h>
#import "Reachability.h"
#import "RCUserCacheManager.h"
#import <JPUSHService.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [JPUSHService setupWithOption:launchOptions appKey:@"9080d2ddbe4bb58b88b9b55a" channel:@"IOS" apsForProduction:NO];
    
    [JPUSHService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                   UIUserNotificationTypeSound |
                                                   UIUserNotificationTypeAlert)
                                       categories:nil];



    
    id<UIApplicationDelegate> service;
    for(service in [[SOAComponentAppDelegate instance] services]){
        if ([service respondsToSelector:@selector(application:didFinishLaunchingWithOptions:)]){
            [service application:application didFinishLaunchingWithOptions:launchOptions];
        }
    }
    
    
    

    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    [self setNavBarAppearence];
    
    self.window.rootViewController =  [[RouteController sharedManager] rootViewController];

    [self.window makeKeyAndVisible];
    
    return YES;
}


- (void)setNavBarAppearence
{
    
//    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor] size:CGSizeMake(APP_WIDTH, 64)] forBarMetrics:UIBarMetricsDefault];
//    [[UINavigationBar appearance] setShadowImage:[UIImage imageWithColor:RGB16(0xe8e8e8) size:CGSizeMake(APP_WIDTH, 0.5)]];
    
    
    
    // 设置导航栏默认的背景颜色
    [UIColor wr_setDefaultNavBarBarTintColor:[UIColor colorWithWhite:1 alpha:1.0]];
    // 设置导航栏所有按钮的默认颜色
    [UIColor wr_setDefaultNavBarTintColor:AppTitleColor];
    // 设置导航栏标题默认颜色
    [UIColor wr_setDefaultNavBarTitleColor:AppTitleColor];
    // 统一设置状态栏样式
    [UIColor wr_setDefaultStatusBarStyle:UIStatusBarStyleDefault];
    // 如果需要设置导航栏底部分割线隐藏，可以在这里统一设置
     [UIColor wr_setDefaultNavBarShadowImageHidden:YES];
    
    
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    id<UIApplicationDelegate> service;
    if ([url.host isEqualToString:@"qzapp"] || [url.host isEqualToString:@"oauth"]){
        return [[UMSocialManager defaultManager] handleOpenURL:url options:options];
    }
    for(service in [[SOAComponentAppDelegate instance] services]){
        if ([service respondsToSelector:@selector(application:openURL:options:)]){
            if ([service application:app openURL:url options:options])
                return YES;
        }
    }
    
    return YES;
}


-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    
    
    id<UIApplicationDelegate> service;
    for(service in [[SOAComponentAppDelegate instance] services]){
        if ([service respondsToSelector:@selector(application:openURL:sourceApplication:annotation:)]){
            return  [service application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
        }
    }
    return [[UMSocialManager defaultManager]handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    return YES;
}




#pragma mark - APNs

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Jpush
    NSLog(@"device token %@",[[NSString alloc]initWithData:deviceToken encoding:NSUTF8StringEncoding]);
    [JPUSHService registerDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // jpush
    NSLog(@"收到远程通知%@",userInfo);
    [JPUSHService handleRemoteNotification:userInfo];
    [[RCUserCacheManager sharedManager] renewAccount];
    
    
    if (userInfo && application.applicationState == UIApplicationStateInactive) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
}


@end
