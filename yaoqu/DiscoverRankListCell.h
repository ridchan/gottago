//
//  DiscoverRankListCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DynamicRankListModel.h"
@interface DiscoverRankListCell : UITableViewCell

@property(nonatomic) DiscoverRankType rankType;

@property(nonatomic,strong) DynamicRankListModel *model;

@property(nonatomic) NSInteger level;

@end
