//
//  ChatManagerView.h
//  shiyi
//
//  Created by 陳景雲 on 16/6/24.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatInputView.h"
#import "ChatEmjoyView.h"
#import "ChatToolView.h"

typedef NS_ENUM(int,ChatViewType) {
    ChatViewType_Chat,
    ChatViewType_Publish
};

typedef NS_ENUM(int,ChatInputType) {
    ChatInputType_Text,
    ChatInputType_Emjoi,
    ChatInputType_Tool
};

@protocol ChatManagerViewDelegate <NSObject>

@optional
-(void)chatViewFrameChange:(CGFloat)height;
-(void)sendWithInfo:(NSString *)message;
-(void)dismissWithInfo:(id)obj;

@end

@interface ChatManagerView : UIView<ChatEmjoyViewDelegate,ChatInputViewDelegate,UITextFieldDelegate>{
    ChatViewType _viewType;
    BOOL bShow;
}

@property(nonatomic,strong) ChatInputView *inputView;
@property(nonatomic,strong) ChatEmjoyView *emjoyView;
@property(nonatomic,strong) ChatToolView *toolView;
@property(nonatomic,strong) UIView *coverView;
@property(nonatomic,strong) NSLayoutConstraint *constraint;

@property(nonatomic) ChatInputType inputType;

@property(nonatomic,weak) id<ChatManagerViewDelegate> delegate;

-(instancetype)initWithType:(ChatViewType)type;
-(instancetype)initWithHidden;


@property(nonatomic) CGFloat bottomHeight;

@property(nonatomic) CGFloat offset;
@property(nonatomic) BOOL isHidden;
@property(nonatomic) CGFloat moveOffset;

@end
