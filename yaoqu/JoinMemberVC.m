//
//  JoinMemberVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/10.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "JoinMemberVC.h"
#import "CommonConfigManager.h"
#import "OpenMemberView.h"
#import "MemberRightView.h"

@interface JoinMemberVC ()

@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) UIView *titleView;
@property(nonatomic,strong) OpenMemberView *memberView;
@property(nonatomic,strong) MemberRightView *rightView;

@end

@implementation JoinMemberVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.title = LS(@"开通会员");
    
    
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView.mas_left);
        make.top.equalTo(self.scrollView.mas_top);
        make.width.mas_equalTo(APP_WIDTH);
    }];
    
    [self.memberView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleView.mas_bottom).offset(10);
        make.left.equalTo(self.scrollView.mas_left).offset(10);
        make.width.mas_equalTo(APP_WIDTH - 20);
    }];
    
    [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.memberView.mas_bottom).offset(10);
        make.left.equalTo(self.scrollView.mas_left).offset(10);
        make.width.mas_equalTo(APP_WIDTH - 20);
        make.bottom.equalTo(self.scrollView.mas_bottom);
    }];
    
    self.rightView.datas = self.paramObj;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIView *)titleView{
    if (!_titleView) {
        _titleView = [[UIView alloc]init];
        _titleView.backgroundColor = [UIColor whiteColor];
        
        
        UILabel *label = [ControllerHelper autoFitLabel];
        label.text = @"未开通";
        label.font = SystemFont(11);
        label.textColor = [UIColor orangeColor];
        label.textAlignment = NSTextAlignmentCenter;
        [_titleView addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_titleView.mas_centerX);
            make.top.equalTo(_titleView.mas_top);
        }];
        
        UIImage *image = [UIImage imageNamed:@"bg_vip"];
        CGFloat height = APP_WIDTH * image.size.height /  image.size.width;
        UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
        [_titleView addSubview:imageView];
        
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_titleView.mas_left);
            make.right.equalTo(_titleView.mas_right);
            make.top.equalTo(_titleView.mas_top);
            make.height.mas_equalTo(height);
            make.bottom.equalTo(_titleView.mas_bottom);
        }];
        
        
        [self.scrollView addSubview:_titleView];
    }
    return _titleView;
}

-(OpenMemberView *)memberView{
    if (!_memberView) {
        _memberView = [[OpenMemberView alloc]init];
        [self.scrollView addSubview:_memberView];
    }
    return _memberView;
}

-(MemberRightView *)rightView{
    if (!_rightView) {
        _rightView = [[MemberRightView alloc]init];
        [self.scrollView addSubview:_rightView];
    }
    return _rightView;
}

-(UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]init];
        [self.view addSubview:_scrollView];
    }
    return _scrollView;
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
