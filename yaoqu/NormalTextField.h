//
//  NormalTextField.h
//  QBH
//
//  Created by 陳景雲 on 2016/12/25.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>

@interface NormalTextField : UITextField

@property(nonatomic,strong) NSString *title;
//@property(nonatomic,strong) NSString *placeholder;
//@property(nonatomic,strong) NSString *text;

@property(nonatomic,strong) UIView *centerLine;
@property(nonatomic,strong) UIView *bottomLine;
@property(nonatomic,strong) UILabel *titleLabel;
//@property(nonatomic,strong) UITextField *textField;

@end
