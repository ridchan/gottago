//
//  BubbleModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/11.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface BubbleModel : BaseModel

@property(nonatomic,strong) NSString *member_id;
@property(nonatomic,strong) NSString *message_system;
@property(nonatomic,strong) NSString *message_like;
@property(nonatomic,strong) NSString *message_comment;
@property(nonatomic,strong) NSString *preview;
@property(nonatomic,strong) NSString *gift_send;

@end
