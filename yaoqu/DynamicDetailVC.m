//
//  DynamicDetailVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/28.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicDetailVC.h"
#import "DynamicDetailHeader.h"
#import "DynamicDetailApi.h"
#import "DynamicCommentModel.h"
#import "DynamicCommentCell.h"
#import "DynamicGiftCell.h"
#import "QZTopTextView.h"
#import "DynamicDetailBottomBar.h"
#import "GiftView.h"
#import "DynamicCommentApi.h"
#import "RCUserCacheManager.h"
#import "DynamicCommentListVC.h"
#import "CommentApi.h"
#import "CommonConfigManager.h"
#import "GiftPostModel.h"
#import "GiftApi.h"
#import "DynamicApi.h"
#import "SRActionSheet.h"
#import "CollectApi.h"
#import "HomePageApi.h"
#import "ShareView.h"
#import "ChatManagerView.h"


@interface DynamicDetailVC ()<UITableViewDelegate,QZTopTextViewDelegate,DynaicDetailBottomBarDelegate,GiftViewDelegate,ChatManagerViewDelegate>

@property(nonatomic,strong) DynamicDetailHeader *dynamicView;
@property(nonatomic,strong) NSMutableArray *commentArr;
@property(nonatomic,strong) DynamicModel *detailModel;
@property(nonatomic,strong) QZTopTextView *textView;
@property(nonatomic,strong) ChatManagerView *chatView;
@property(nonatomic,strong) DynamicDetailBottomBar *bottomBar;

@end

@implementation DynamicDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"动态详情");
    
    [self wr_setNavBarShadowImageHidden:NO];
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.model = self.paramObj;
    self.dynamicView = [[DynamicDetailHeader alloc]init];
    self.dynamicView.model = self.model;
    self.tableView.tableHeaderView = self.dynamicView;
    
    [self.tableView registerClass:[DynamicCommentCell class] forCellReuseIdentifier:@"CommentCell"];
    [self.tableView registerClass:[DynamicGiftCell class] forCellReuseIdentifier:@"GiftCell"];
    
    _textView =[QZTopTextView topTextView];
    _textView.delegate = self;
    [self.view addSubview:_textView];
    
    [self.view addSubview:self.chatView];
    self.chatView.frame = CGRectMake(0, APP_HEIGHT - 44, APP_WIDTH, APP_HEIGHT);
    
    
    self.bottomBar = [[DynamicDetailBottomBar alloc]init];
    self.bottomBar.delegate = self;
    [self.view addSubview:self.bottomBar];
    
    [self.bottomBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(50);
    }];
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top);
        make.bottom.equalTo(self.bottomBar.mas_top);
    }];
    
    
    [self setRightItemWithIcon:[UIImage imageNamed:@"ic_more"] selector:@selector(moreBtnClick:)];
    
    [self getAllComment];
    
    // Do any additional setup after loading the view.
    
}


-(void)moreBtnClick:(id)sender{
    
    WEAK_SELF;
    
    if ([self.model.member_id isEqualToString:[RCUserCacheManager sharedManager].currentUser.member_id]) {
        [[SRActionSheet sr_actionSheetViewWithTitle:nil cancelTitle:@"取消" destructiveTitle:nil otherTitles:@[@"删除"] otherImages:nil selectSheetBlock:^(SRActionSheet *actionSheet, NSInteger index) {
            if (index == 0) {
                DynamicApi *api = [[DynamicApi alloc]initWitObject:@{@"dynamic_id":weakSelf.model.dynamic_id}];
                api.method = YTKRequestMethodDELETE;
                [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                    if (info.error == ErrorCodeType_None) {
                        PostNotification(RemoveDynamciNotification, weakSelf.model);
                        [weakSelf.navigationController popViewControllerAnimated:YES];
                    }
                    [LLUtils showActionSuccessHUD:info.message];
                }];

            }
        }] show];
    }else{
        [[SRActionSheet sr_actionSheetViewWithTitle:nil cancelTitle:@"取消" destructiveTitle:nil otherTitles:@[@"收藏",@"举报"] otherImages:nil selectSheetBlock:^(SRActionSheet *actionSheet, NSInteger index) {
            
            if (index == 0) {
                NSDictionary *dict = @{@"type":@(CollectType_Dynamic),
                                       @"key_id":weakSelf.model.dynamic_id,
                                       @"state":@"1"
                                       };
                CollectApi *api = [[CollectApi alloc]initWitObject:dict];
                api.method = YTKRequestMethodPOST;
                [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                    if (info.error == ErrorCodeType_None) {
                        
                    }
                    [LLUtils showActionSuccessHUD:info.message];
                }];
            }else if (index == 1){
                
                NSArray *array = [CommonConfigManager sharedManager].hostModel.report_desc;
                if ([array count] == 0) return ;
                [[SRActionSheet sr_actionSheetViewWithTitle:nil cancelTitle:@"取消" destructiveTitle:nil otherTitles:array  otherImages:nil selectSheetBlock:^(SRActionSheet *actionSheet, NSInteger index) {
                    if (index >= 0){
                        HomePageApi *api = [[HomePageApi alloc]initWitObject:@{@"member_id":weakSelf.model.member_id,
                                                                               @"desc":array[index]
                                                                               }];
                        api.method = YTKRequestMethodPUT;
                        [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                            [LLUtils showActionSuccessHUD:info.message];
                        }];
                    }
                }] show];
                
            }
        }] show];
    }
}


-(void)getAllComment{
    WEAK_SELF;
    
    [[[DynamicApi alloc]initWitObject:@{@"dynamic_id":self.model.dynamic_id}] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        self.isloading = NO;
        
        if (info.error == ErrorCodeType_None) {
            
            
            weakSelf.commentArr = [DynamicCommentModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"comment"]];
            weakSelf.detailModel = [DynamicModel objectWithKeyValues:[[responseObj objectForKey:@"data"] objectForKey:@"detail"]];
            
//            weakSelf.dynamicView.model = weakSelf.detailModel;
//            weakSelf.tableView.tableHeaderView = weakSelf.dynamicView;
            
            weakSelf.bottomBar.model = weakSelf.detailModel;

            
        }else{
            
        }
        
        [weakSelf.tableView reloadData];

    }];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (_isShowComment){
        _isShowComment = NO;
        [self.chatView.inputView.textField becomeFirstResponder];
    }

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)giftView:(GiftView *)view didSelectObj:(GiftModel *)obj{
    
//    GiftPostModel *postModel = [[GiftPostModel alloc]init];
//    postModel.key_id = self.model.dynamic_id;
//    postModel.member_id = self.model.member_id;
//    postModel.type = GiftType_ForDynamic;
//    postModel.gift_id = obj.gift_id;
//    postModel.qty = 1;
    
    
    
//    MBProgressHUD *hud = [LLUtils showActivityIndicatiorHUDWithTitle:nil];
//    
//    GiftApi *api = [[GiftApi alloc]initWitObject:postModel];
//    
//    
//    
//    api.method = YTKRequestMethodPOST;
//    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
//        [hud hideAnimated:NO];
//        [LLUtils showActionSuccessHUD:info.message];
//        [view dismiss];
//    }];
    
    
}


#pragma mark - chat view

-(ChatManagerView *)chatView{
    if (!_chatView) {
        _chatView = [[ChatManagerView alloc] initWithType:ChatViewType_Publish];
        _chatView.inputView.textField.placeholder = @"添加评论";
        _chatView.delegate = self;
    }
    return _chatView;
}

-(void)dismissWithInfo:(id)obj{
    _chatView.inputView.textField.placeholder = @"添加评论";
}

-(void)sendWithInfo:(NSString *)message{
    
    self.chatView.inputView.textField.text = @"";
    
    WEAK_SELF;
    
    NSString *text = message;

    
    
    NSDictionary *dict = @{@"dynamic_id":self.model.dynamic_id,
                           @"desc":text
                           };
    CommentApi *api = [[CommentApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            [weakSelf getAllComment];
        }else{
            [LLUtils showCenterTextHUD:info.message];
        }
    }];

}


#pragma mark -
#pragma mark tableview

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 1){
        id model = self.commentArr[indexPath.row];
        return [tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[DynamicCommentCell class] contentViewWidth:APP_WIDTH];
        
    }else{

        return [self.detailModel.gift_qty integerValue] > 0 ? 120 : 200;

    }
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0){
        return self.detailModel ? 1 : 0;
    }else{
        return [self.commentArr count];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section != 1) return nil;
    if (self.commentArr.count == 0) return nil;
    UIView *view = [[UIView alloc]init];
    view.frame = CGRectMake(0, 5, APP_WIDTH, 40);
    view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_dynamic_comment"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:imageView];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = LS(@"评价");
    [view addSubview:label];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.equalTo(view).offset(10);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(view.mas_top);
        make.left.equalTo(imageView.mas_right).offset(10);
        make.centerY.equalTo(imageView.mas_centerY);
    }];

    
    return view;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView{
    return YES;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section == 0 ? 5 : 45 ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1){
        DynamicCommentCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"CommentCell" forIndexPath:indexPath];
        
        cell.model = self.commentArr[indexPath.row];
        return cell;
    }else{
        DynamicGiftCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"GiftCell" forIndexPath:indexPath];
        
        cell.model = self.detailModel;
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        DynamicCommentListVC *vc = [[DynamicCommentListVC alloc]init];
        vc.dynamic_id = self.model.dynamic_id;
        vc.model = self.commentArr[indexPath.row];
        [self.navigationController pushViewController:vc animated:YES];
    }
}


#pragma mark -
#pragma mark delegate


-(void)bottomBarCommentClick:(id)obj{
    [self.chatView.inputView.textField becomeFirstResponder];
}

-(void)didSendGiftSuccess:(id)obj{
    [self getAllComment];
}


-(void)bottomBarGiftClick:(id)obj{
    
    GiftPostModel *postModel = [[GiftPostModel alloc]init];
    postModel.key_id = self.model.dynamic_id;
    postModel.member_id = self.model.member_id;
    postModel.type = GiftType_ForDynamic;

    
    [GiftView showWithDelegate:self postModel:postModel];
}

-(void)bottomBarTextClick:(id)obj{
    [self.chatView.inputView.textField becomeFirstResponder];

}

-(void)sendComment{
    WEAK_SELF;
    
    NSString *text = self.textView.countNumTextView.text;
    self.textView.countNumTextView.text = @"";
    [self.textView.countNumTextView resignFirstResponder];

    
    NSDictionary *dict = @{@"dynamic_id":self.model.dynamic_id,
                           @"desc":text
                           };
    CommentApi *api = [[CommentApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            [weakSelf getAllComment];
        }else{
            [LLUtils showCenterTextHUD:info.message];
        }
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
