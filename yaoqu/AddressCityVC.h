//
//  AddressCityVC.h
//  yaoqu
//
//  Created by ridchan on 2017/7/5.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NormalTableViewController.h"
#import "UserModel.h"
#import "ProModel.h"
@interface AddressCityVC : NormalTableViewController

@property(nonatomic,strong) ProModel *proModel;
@property(nonatomic,strong) UserModel *model;

@end
