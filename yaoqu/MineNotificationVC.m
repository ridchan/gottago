//
//  MineNotificationVC.m
//  yaoqu
//
//  Created by ridchan on 2017/7/23.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineNotificationVC.h"
#import "UserNotificationCell.h"
#import "UserNotificationApi.h"
#import "MessageNotificationModel.h"
#import "MessageApi.h"

@interface MineNotificationVC ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation MineNotificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"系统通知");
    [self.tableView registerClass:[UserNotificationCell class] forCellReuseIdentifier:@"Cell"];
    [self startRefresh];
    // Do any additional setup after loading the view.
}

-(void)startRefresh{
    WEAK_SELF;
    self.page = 1;
    
    NSDictionary *dict = @{@"page":@(self.page),
                           @"size":@(self.size),
                           @"type":@(MessageCenterType_System)
                           };
    
    [[[MessageApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [weakSelf refreshComplete:info response:[MessageNotificationModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]]];
    }];
    
}

-(void)startloadMore{
    WEAK_SELF;
    NSDictionary *dict = @{@"page":@(++self.page),
                           @"size":@(self.size),
                           @"type":@(MessageCenterType_System)
                           };
    [[[MessageApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [weakSelf loadMoreComplete:info response:[MessageNotificationModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]]];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    id model = self.datas[indexPath.row];
    return [tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[UserNotificationCell class] contentViewWidth:APP_WIDTH];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserNotificationCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.model = self.datas[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        MessageNotificationModel *model = self.datas[indexPath.row];
        MessageApi *delApi = [[MessageApi alloc]initWitObject:@{@"type":@"0",@"id":model.message_system_id}];
        delApi.method = YTKRequestMethodDELETE;
        
        [delApi startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            
        }];
        [self.datas removeObjectAtIndex:indexPath.row];
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        [self.tableView endUpdates];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
