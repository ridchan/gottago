//
//  SexChooseView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/8.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "SexChooseView.h"

@interface SexChooseView()

@property(nonatomic,strong) UIButton *maleBtn;
@property(nonatomic,strong) UIButton *femaleBtn;

@property(nonatomic,strong) UIImageView *maleImage;
@property(nonatomic,strong) UIImageView *femaleImage;
@property(nonatomic,strong) UILabel *maleLbl;
@property(nonatomic,strong) UILabel *femaleLbl;



@end

@implementation SexChooseView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    [self.maleImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.centerY.equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    [self.maleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.maleImage.mas_right).offset(10);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    [self.femaleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    [self.femaleImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.femaleLbl.mas_left).offset(-10);
        make.centerY.equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    [self.maleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.maleImage.mas_left);
        make.right.equalTo(self.maleLbl.mas_right);
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
    }];
    
    [self.femaleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.femaleImage.mas_left);
        make.right.equalTo(self.femaleLbl.mas_right);
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
    }];
    
    [[self lineInColor:RGB16(0xfffeff)] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];
    
    self.sex = 0;
}

-(void)setSex:(NSInteger)sex{
    _sex = sex;
    if(_sex == 0){
        self.maleLbl.textColor = [UIColor whiteColor];
        self.maleImage.image = [[UIImage imageNamed:@"ic_not_checked"] imageToColor:[UIColor whiteColor]];
        
        self.femaleLbl.textColor = AppOrange;
        self.femaleImage.image = [UIImage imageNamed:@"ic_checked_yellow"];
    }else{
        self.maleLbl.textColor = AppOrange;
        self.maleImage.image = [UIImage imageNamed:@"ic_checked_yellow"];
        
        self.femaleLbl.textColor = [UIColor whiteColor];
        self.femaleImage.image = [[UIImage imageNamed:@"ic_not_checked"] imageToColor:[UIColor whiteColor]];
    }
}

-(void)btnClick:(id)sender{
    if (sender == self.maleBtn) {
        self.sex = 1;
    }else{
        self.sex = 0;
    }
}

-(UIButton *)maleBtn{
    if (!_maleBtn) {
        _maleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_maleBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_maleBtn];
    }
    return _maleBtn;
}

-(UIButton *)femaleBtn{
    if (!_femaleBtn) {
        _femaleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_femaleBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_femaleBtn];
    }
    return _femaleBtn;
}

-(UIImageView *)maleImage{
    if (!_maleImage) {
        _maleImage = [[UIImageView alloc]init];
        _maleImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_maleImage];
    }
    return _maleImage;
}

-(UIImageView *)femaleImage{
    if (!_femaleImage) {
        _femaleImage = [[UIImageView alloc]init];
        _femaleImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_femaleImage];
    }
    return _femaleImage;
}

-(UILabel *)femaleLbl{
    if (!_femaleLbl) {
        _femaleLbl = [ControllerHelper autoFitLabel];
        _femaleLbl.text = @"女";
        [self addSubview:_femaleLbl];
    }
    return _femaleLbl;
}

-(UILabel *)maleLbl{
    if (!_maleLbl) {
        _maleLbl = [ControllerHelper autoFitLabel];
        _maleLbl.text = @"男";
        [self addSubview:_maleLbl];
    }
    return _maleLbl;
}




@end
