//
//  RCUserCacheManager.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CurrentUserModel.h"
#import "AccessTokenModel.h"
#import "LoginInfoModel.h"
#import "ProModel.h"
#import "BubbleModel.h"


@interface RCUserCacheManager : NSObject

+ (instancetype)sharedManager;

@property(nonatomic,strong) UserModel *currentUser;
@property(nonatomic,strong) AccessTokenModel *currentToken;
@property(nonatomic,strong) LoginInfoModel *currentLoginInfo;
@property(nonatomic,strong) CityModel *currentCity;
@property(nonatomic,strong) BubbleModel *currentBubble;
@property(nonatomic,strong) BubbleModel *lastBubble;
@property(nonatomic,strong) id addressPoi;
@property(nonatomic,strong) UIWebView *webView;

@property(nonatomic,strong) NSMutableArray *dynamicList;


-(void)getUserWithID:(NSString *)member_id block:(BaseBlock)block;
-(UserModel *)cacheUser:(NSString *)member_id;

-(void)getAddress;
-(void)startLocation;
-(void)renewAccessToken;
-(void)renewAccount;
-(void)fetchBubble;
-(void)huanXinAutoLogin;
-(void)huxnXinAoutLogout;

-(void)openLink:(NSString *)link;

@end
