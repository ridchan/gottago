//
//  AliPayServices.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/9.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AliPayServices : NSObject<UIApplicationDelegate>

-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options;

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

@end
