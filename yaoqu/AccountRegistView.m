//
//  AccountRegistView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/8.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "AccountRegistView.h"
#import "ImageTextField.h"
#import "CommonApi.h"
#import "LLUtils.h"
@interface AccountRegistView(){
    NSTimer *timer;
    NSInteger second;
}

@property(nonatomic,strong) ImageTextField *accountFld;
@property(nonatomic,strong) ImageTextField *codeFld;
@property(nonatomic,strong) ImageTextField *passwordFld1;
@property(nonatomic,strong) ImageTextField *passwordFld2;

@property(nonatomic,strong) UIButton *codeBtn;




@end

@implementation AccountRegistView


- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}


-(void)commonInit{
    [self.accountFld mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.right.equalTo(self.mas_right).offset(-20);
        make.top.equalTo(self.mas_top).offset(20);
        make.height.mas_equalTo(50);
    }];
    
    [self.codeFld mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.right.equalTo(self.mas_right).offset(-20);
        make.top.equalTo(self.accountFld.mas_bottom).offset(20);
        make.height.mas_equalTo(50);
    }];
    
    [self.passwordFld1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.right.equalTo(self.mas_right).offset(-20);
        make.top.equalTo(self.codeFld.mas_bottom).offset(20);
        make.height.mas_equalTo(50);
    }];
    
    [self.passwordFld2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.right.equalTo(self.mas_right).offset(-20);
        make.top.equalTo(self.passwordFld1.mas_bottom).offset(20);
        make.height.mas_equalTo(50);
    }];
    
    [self.nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.right.equalTo(self.mas_right).offset(-20);
        make.top.equalTo(self.passwordFld2.mas_bottom).offset(50);
        make.height.mas_equalTo(50);
        make.bottom.equalTo(self.mas_bottom).offset(-50);
    }];
    
    RACSignal *signal1 = [self.accountFld.rac_textSignal distinctUntilChanged];
    RACSignal *signal2 = [self.codeFld.rac_textSignal distinctUntilChanged];
    RACSignal *signal3 = [self.passwordFld1.rac_textSignal distinctUntilChanged];
    RACSignal *signal4 = [self.passwordFld2.rac_textSignal distinctUntilChanged];
    
    
    RAC(self,postModel.phone) = signal1;
    RAC(self,postModel.code) = signal2;
    RAC(self,postModel.password) = signal3;
    RAC(self,postModel.password2) = signal4;
    
    RAC(self.codeBtn,enabled) = [RACSignal combineLatest:@[signal1] reduce:^id(NSString *text){
        return @([text length] > 0);
    }];
    
    RAC(self.nextBtn,enabled) = [RACSignal combineLatest:@[signal1,signal2,signal3,signal4] reduce:^id(NSString *text1,NSString *text2,NSString *text3,NSString *text4){
       return @([text1 length] > 0 && [text2 length] > 0 && [text3 length] > 0 && [text4 length] > 0);
    }];
    
    

}

-(void)codeBtnClick:(id)sender{
    
    WEAK_SELF;
    __block MBProgressHUD *hud = [LLUtils showActivityIndicatiorHUDWithTitle:nil];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSString *link = nil;
    
    if ([self.accountFld.text rangeOfString:@"@"].location == NSNotFound){
        link = @"common/code";
        [dict setValue:@"join" forKey:@"type"];
        [dict setValue:self.accountFld.text forKey:@"phone"];
        self.postModel.phone = self.accountFld.text;
        
        self.postModel.email = nil;
    }else{
        link = @"common/email";
        [dict setValue:@"reg" forKey:@"type"];
        [dict setValue:self.accountFld.text forKey:@"email"];
        self.postModel.phone = nil;
        self.postModel.email = self.accountFld.text;
    }
    if (self.isFindPassword){
        [dict setValue:@"forgetpassword" forKey:@"type"];
        
    }
    
    
    [[[CommonApi alloc]initWithModel:link object:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [hud hideAnimated:NO];
        if (info.error == ErrorCodeType_None) {
            second = 60;
            weakSelf.codeBtn.enabled = NO;
            weakSelf.codeBtn.backgroundColor = [UIColor lightGrayColor];
            
            timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self  selector:@selector(countTime) userInfo:nil repeats:YES];
//            [timer fire];
            [LLUtils showActionSuccessHUD:@"发送成功"];
        }else{
            
            [LLUtils showCenterTextHUD:info.message];
        }
    }];

}

-(void)countTime{
    if (second > 0) {
        [_codeBtn setTitle:[NSString stringWithFormat:@"剩余%ld秒",second] forState:UIControlStateNormal];
    }else{
        self.codeBtn.enabled = YES;
        self.codeBtn.backgroundColor = AppOrange;
        [_codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [timer invalidate];
        timer = nil;
    }
    second --;

}

-(ImageTextField *)accountFld{
    if (!_accountFld) {
        _accountFld = [ImageTextField imageText:@"ic_login_user" placeHolder:@"输入手机或邮箱"];
        [self addSubview:_accountFld];
    }
    return _accountFld;
}

-(ImageTextField *)codeFld{
    if (!_codeFld) {
        _codeFld = [ImageTextField imageText:@"ic_login_code" placeHolder:@"输入验证码"];
        _codeFld.rightViewMode = UITextFieldViewModeAlways;
        _codeFld.rightView = self.codeBtn;
        [self addSubview:_codeFld];
    }
    return _codeFld;
}

-(UIButton *)codeBtn{
    if (!_codeBtn) {
        _codeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _codeBtn.backgroundColor = AppOrange;
        [_codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        _codeBtn.titleLabel.font = SystemFont(12);
        _codeBtn.frame = CGRectMake(0, 0, 70, 35);
        [_codeBtn addTarget:self action:@selector(codeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _codeBtn;
}

-(ImageTextField *)passwordFld1{
    if (!_passwordFld1) {
        _passwordFld1 = [ImageTextField imageText:@"ic_login_password" placeHolder:@"输入帐号密码"];
        _passwordFld1.secureTextEntry = YES;
        [self addSubview:_passwordFld1];
    }
    return _passwordFld1;
}

-(ImageTextField *)passwordFld2{
    if (!_passwordFld2) {
        _passwordFld2 = [ImageTextField imageText:@"ic_login_password" placeHolder:@"再次输入帐号密码"];
        _passwordFld2.secureTextEntry = YES;
        [self addSubview:_passwordFld2];
    }
    return _passwordFld2;
}

-(NormalButton *)nextBtn{
    if (!_nextBtn) {
        _nextBtn = [NormalButton normalButton:@"下一步"];
        [self addSubview:_nextBtn];
    }
    return _nextBtn;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
