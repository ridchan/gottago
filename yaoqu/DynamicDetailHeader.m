//
//  DynamicDetailHeader.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/28.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicDetailHeader.h"
#import "UserImageView.h"
#import "SexAgeView.h"
#import "LevelView.h"
#import "ImageCollectionViewCell.h"
#import "NetImagePreviewVC.h"
#import "UploadCollectionCell.h"
#import "NetVideoPreviewVC.h"
#import "CommonConfigManager.h"
#import "DynamicApi.h"

@interface DynamicDetailHeader()<UICollectionViewDelegate,UICollectionViewDataSource,NetImagePreviewVCDelegate>

@property(nonatomic,strong) UserImageView *userImage;
@property(nonatomic,strong) SexAgeView *sexView;
@property(nonatomic,strong) UIImageView *goldView;
@property(nonatomic,strong) LevelView *levelView;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) UILabel *dateLbl;
@property(nonatomic,strong) UILabel *locationLbl;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) NSArray *imgDatas;


@property(nonatomic,strong) UIImageView *likeImage;
@property(nonatomic,strong) UILabel *likeLbl;

@end

@implementation DynamicDetailHeader



- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        [self commonInit];
        
        [RACObserve(self, model) subscribeNext:^(DynamicModel *model) {
            
            
            self.nameLbl.text = model.username;
            self.descLbl.text = model.content;
            self.levelView.level = model.level;
            [self.userImage.userImg setImageName:model.image placeholder:nil];
            self.sexView.sex = [model.sex integerValue];
            self.sexView.age = [model.age integerValue];
            self.sexView.content = nil;
//            self.dateLbl.text = [NSString compareCurrentTime:model.time];
//            self.locationLbl.text = model.address;
            
            if ([model.dynamic_city_name length] > 0){
                self.dateLbl.text = [NSString stringWithFormat:@"%@.%@%@",[NSString compareCurrentTime:model.time],NoNilString(model.dynamic_city_name),NoNilString(model.address)];
            }else{
                self.dateLbl.text = [NSString compareCurrentTime:model.time];
            }

            
//            self.imgDatas = [model.content_images JSONObject];
            if ([model.video_image length] > 0){
                self.imgDatas = @[model.video_image];
            }else{
                self.imgDatas = [model.content_images JSONObject];
            }
            
            CGFloat row = ceilf(self.imgDatas.count / 3.0);
            CGFloat rowHeight = (APP_WIDTH - 30) / 3 + 10;
            CGFloat height = self.imgDatas.count > 0  ?
            row * rowHeight :
            0;

            
            self.collectionView.sd_resetLayout
            .leftEqualToView(self)
            .rightEqualToView(self)
            .topSpaceToView(self.descLbl, 10)
            .heightIs(height);
            
            self.likeLbl.text = [NSString stringWithFormat:@"已有%@人点赞",model.like];
            [self.collectionView reloadData];
        }];
        
        [RACObserve(self, model.like) subscribeNext:^(id x) {
            self.likeImage.image = self.model.is_like ? [UIImage imageNamed:@"ic_dynamic_like_on"] : [UIImage imageNamed:@"ic_dynamic_like"];
            self.likeLbl.text = [NSString stringWithFormat:@"已有%@人点赞",self.model.like];
        }];
        
        
    }
    return self;
}



-(void)commonInit{
    
    self.backgroundColor = [UIColor whiteColor];
    
    self.userImage.sd_layout
    .leftSpaceToView(self, 10)
    .topSpaceToView(self, 20)
    .heightIs(50)
    .widthIs(50);
    
    self.nameLbl.sd_layout
    .topSpaceToView(self, 20)
    .leftSpaceToView(self.userImage, 5)
    .heightIs(25);
    [self.nameLbl setSingleLineAutoResizeWithMaxWidth:200];
    
    self.sexView.sd_layout
    .leftSpaceToView(self.nameLbl, 5)
    .centerYEqualToView(self.nameLbl)
    .widthIs(35)
    .heightIs(15);
    

    
    
    self.levelView.sd_layout
    .leftSpaceToView(self.sexView, 5)
    .centerYEqualToView(self.nameLbl)
    .heightIs(45)
    .widthIs(45);
    
    self.dateLbl.sd_layout
    .leftSpaceToView(self.userImage, 5)
    .topSpaceToView(self.nameLbl, 1)
    .heightIs(22)
    .widthIs(0);
    
    
    [self.dateLbl setSingleLineAutoResizeWithMaxWidth:APP_WIDTH - 100];
    
    
    
    
    self.descLbl.sd_layout
    .leftSpaceToView(self, 10)
    .rightSpaceToView(self, 10)
    .topSpaceToView(self.userImage, 20)
    .autoHeightRatio(0);
    
    self.collectionView.sd_layout
    .leftEqualToView(self)
    .rightEqualToView(self)
    .topSpaceToView(self.descLbl, 10)
    .autoHeightRatio(0);
    
    self.likeImage.sd_layout
    .centerXEqualToView(self)
    .topSpaceToView(self.collectionView, 20)
    .heightIs(20)
    .widthIs(20);
    
    self.likeLbl.sd_layout
    .topSpaceToView(self.likeImage, 5)
    .leftEqualToView(self)
    .rightEqualToView(self)
    .heightIs(30);
    
    
    [self setupAutoHeightWithBottomView:self.likeLbl bottomMargin:15];
    [self layoutSubviews];
    
}


#pragma mark -
#pragma mark collection view

-(UICollectionViewFlowLayout *)layout{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.minimumLineSpacing = 5;
    layout.minimumInteritemSpacing = 5;
    layout.sectionInset = UIEdgeInsetsMake(5, 10, 5, 10);
    CGFloat width = (APP_WIDTH - 30) / 3;
    layout.itemSize = CGSizeMake(width, width);
    
    //    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    
    return layout;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.imgDatas count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UploadCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    [cell.imageView setImageName:[self.imgDatas[indexPath.item] fixImageString:DynamicImageFix] placeholder:nil];
    
    cell.playImage.hidden = self.model.video_image ? NO : YES;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.model.video_image length]  == 0){
        
        NetImagePreviewVC *vc = [[NetImagePreviewVC alloc]init];
        vc.delegate = self;
        vc.showIndex = indexPath.item;
        [vc show];
        
    }else{
        
        NetVideoPreviewVC *vc = [[NetVideoPreviewVC alloc]init];
        vc.link = [[CommonConfigManager sharedManager].video_host stringByAppendingPathComponent:self.model.video];
        [[self ex_viewController] presentViewController:vc animated:YES completion:NULL];
    }
}


-(NSUInteger)numOfPreviewer:(id)obj{
    return self.imgDatas.count;
}

-(UIImageView *)previewImage:(id)obj atIndex:(NSInteger)index{
    ImageCollectionViewCell *cell = (ImageCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
    return cell.imageView;
}

-(NSString *)originalImage:(id)obj atIndex:(NSInteger)index{
    return self.imgDatas[index];
}


#pragma mark -
#pragma mark lazy layout


-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        _nameLbl.font = DynamicNameFont;
        _nameLbl.textColor = DynamicNameColor;
        [self addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(SexAgeView *)sexView{
    if (!_sexView) {
        _sexView = [[SexAgeView alloc]init];
        [self addSubview:_sexView];
    }
    return _sexView;
}

-(UserImageView *)userImage{
    if (!_userImage) {
        _userImage = [[UserImageView alloc]init];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userImageTap:)];
        [_userImage addGestureRecognizer:tap];
        
        [self addSubview:_userImage];
    }
    return _userImage;
}

-(UIImageView *)goldView{
    if (!_goldView) {
        _goldView = [[UIImageView alloc]init];
        _goldView.contentMode = UIViewContentModeScaleAspectFit;
        _goldView.image = [UIImage imageNamed:@"ic_user_level_1"];
        [self addSubview:_goldView];
    }
    return _goldView;
}

-(LevelView *)levelView{
    if (!_levelView) {
        _levelView = [[LevelView alloc]init];
        [self addSubview:_levelView];
    }
    return _levelView;
}

-(UILabel *)dateLbl{
    if (!_dateLbl) {
        _dateLbl = [ControllerHelper autoFitLabel];
        _dateLbl.textAlignment = NSTextAlignmentLeft;
        _dateLbl.font = DynamicTimeFont;
        _dateLbl.textColor = DynamicTimeColor;
        [self addSubview:_dateLbl];
    }
    return _dateLbl;
}

-(UILabel *)locationLbl{
    if (!_locationLbl) {
        _locationLbl = [ControllerHelper autoFitLabel];
        _locationLbl.font = DynamicTimeFont;
        _locationLbl.textColor = DynamicTimeColor;
        _locationLbl.textAlignment = NSTextAlignmentRight;
        [self addSubview:_locationLbl];
    }
    return _locationLbl;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.numberOfLines = 0;
        _descLbl.font = DynamicDescFont;
        _descLbl.textColor = DynamicDescColor;
        [self addSubview:_descLbl];
    }
    return _descLbl;
}

-(UIImageView *)likeImage{
    if (!_likeImage) {
        _likeImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_dynamic_like"]];
        _likeImage.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(likeBtnClick:)];
        [_likeImage addGestureRecognizer:tap];
        [self addSubview:_likeImage];
    }
    return _likeImage;
}

-(UILabel *)likeLbl{
    if (!_likeLbl) {
        _likeLbl = [ControllerHelper autoFitLabel];
        _likeLbl.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_likeLbl];
    }
    
    return _likeLbl;
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:[self layout]];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.scrollEnabled = NO;
        [_collectionView registerClass:[UploadCollectionCell class] forCellWithReuseIdentifier:@"Cell"];
        [self addSubview:_collectionView];
    }
    return _collectionView;
}


-(void)userImageTap:(id)sender{
    [[RouteController sharedManager] openHomePageVC:[self.model objectWithClass:@"UserModel"]];
}

-(CAKeyframeAnimation *)keyAnimation{
    CAKeyframeAnimation *k = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    k.values = @[@(0.1),@(1.0),@(1.5)];
    k.keyTimes = @[@(0.0),@(0.5),@(0.8),@(1.0)];
    k.calculationMode = kCAAnimationLinear;
    return k;
}

-(void)likeBtnClick:(id)sender{
    
//    if (self.model.is_like) return;
    
    
    
    [self.likeImage.layer addAnimation:[self keyAnimation] forKey:nil];
    
    NSDictionary *dict = @{@"dynamic_id":self.model.dynamic_id,
                           @"state":@(!self.model.is_like)
                           };
    
    DynamicApi *api = [[DynamicApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodOPTION;
    WEAK_SELF;
    
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            weakSelf.model.is_like = !weakSelf.model.is_like;
            if (weakSelf.model.is_like)
                weakSelf.model.like = [weakSelf.model.like autoIncrice];
            else
                weakSelf.model.like =[weakSelf.model.like autoReduce];
        }
        
    }];

}

@end
