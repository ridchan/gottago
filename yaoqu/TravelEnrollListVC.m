//
//  TravelEnrollListVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/17.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelEnrollListVC.h"
#import "TravelEnrollListApi.h"
#import "TravelModel.h"
#import "TravelEnrollListCell.h"
#import "TravelApi.h"
#import "SignUpApi.h"
#import "NavTitleView.h"

@interface TravelEnrollListVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) NavTitleView *titleView;
@property(nonatomic,strong) NSString *selectType;

@end

@implementation TravelEnrollListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = LS(@"我的行程");
    
    self.selectType = @"0";
    [self titleView];
    
    
    
//    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.view.mas_left);
//        make.right.equalTo(self.view.mas_right);
//        make.top.equalTo(self.titleView.mas_bottom);
//        make.bottom.equalTo(self.view.mas_bottom);
//    }];
    
    self.tableView.contentInset = UIEdgeInsetsMake(40, 0, 0, 0);
    

    [self startRefresh];
    // Do any additional setup after loading the view.
}


-(NavTitleView *)titleView{
    if (!_titleView) {
        WEAK_SELF;
        _titleView = [[NavTitleView alloc]initWithFrame:CGRectMake(0, 64, APP_WIDTH, 40)];
        _titleView.backgroundColor = [UIColor whiteColor];
        _titleView.selectBlock = ^(id responseObj) {
            weakSelf.selectType = [responseObj stringValue];
            [weakSelf startRefresh];
        };
        _titleView.titles = @[@"所有",@"未开始",@"已结束"];
        [self.view addSubview:_titleView];
    }
    return _titleView;
}

-(void)startRefresh{
    self.page = 1;
    NSDictionary *dict = @{@"page":@(self.page),
                           @"size":@(self.size),
                           @"type":self.selectType
                           };
    WEAK_SELF;
    [[[SignUpApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        NSArray *datas = [TravelModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        [weakSelf refreshComplete:info response:datas];
    }];
}

-(void)startloadMore{
    NSDictionary *dict = @{@"page":@(++self.page),
                           @"size":@(self.size),
                           @"type":self.selectType
                           };
    WEAK_SELF;
    [[[SignUpApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        NSArray *datas = [TravelModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        [weakSelf loadMoreComplete:info response:datas];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self pushViewControllerWithName:@"TravelEnrollDetailVC" params:self.datas[indexPath.section]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 260;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.datas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TravelEnrollListCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[TravelEnrollListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.model = self.datas[indexPath.section];
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
