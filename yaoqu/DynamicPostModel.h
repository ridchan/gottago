//
//  DynamicPostModel.h
//  yaoqu
//
//  Created by ridchan on 2017/8/2.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface DynamicPostModel : BaseModel

@property(nonatomic,strong) NSString *desc;//	文字	false	int
@property(nonatomic,strong) NSString *images;//	图片	false	json
@property(nonatomic,strong) NSString *video;//	视频	false	string
@property(nonatomic,strong) NSString *video_image;//	视频封面	false	string
@property(nonatomic,strong) NSString *is_location;//	显示位置	false	int
@property(nonatomic,strong) NSString *latitude;//	经度	false	string
@property(nonatomic,strong) NSString *longitude;//	纬度	false	string
@property(nonatomic,strong) NSString *address;//	地址 广州.威斯商务中心	false	string

@end
