//
//  MineWalletVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/31.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineWalletVC.h"
#import "MineWalletHeader.h"
#import "RCUserCacheManager.h"
#import "MineHomeCell.h"
#import "YQWebViewVC.h"

@interface MineWalletVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) MineWalletHeader *header;

@end

@implementation MineWalletVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"我的钱包");
    self.header = [[MineWalletHeader alloc]init];
    self.header.frame = CGRectMake(0, 0, APP_WIDTH, 140);
    
    self.tableView.tableHeaderView = self.header;
    
    
    [self setRightItemWithIcon:[UIImage imageNamed:@"ic_question"] selector:@selector(openHelp:)];
    
    
    UserModel *model = [RCUserCacheManager sharedManager].currentUser;
    
    
    self.datas = [@[@{@"name":LS(@"积分"),@"img":@"ic_score",@"desc":model.integral,@"vc":@"MineDepositVC"},
                   @{@"name":LS(@"旅游币"),@"img":@"ic_coin",@"desc":model.currency,@"vc":@"MineTripCoinVC"},
                   @{@"name":LS(@"礼物券"),@"img":@"ic_gift_roll",@"desc":model.giftroll,@"vc":@"MineGiftRollVC"},
                    ] mutableCopy];
    
    
    [RACObserve([RCUserCacheManager sharedManager], currentUser) subscribeNext:^(UserModel *model) {
        MineHomeCell *cell = [[self.tableView visibleCells] firstObject];
        MineCollectionCell *cell1 = [cell detailCellAtIndex:0];
        MineCollectionCell *cell2 = [cell detailCellAtIndex:1];
        MineCollectionCell *cell3 = [cell detailCellAtIndex:2];
        
        cell1.detailLbl.text = model.integral;
        cell2.detailLbl.text = model.currency;
        cell3.detailLbl.text = model.giftroll;
    }];
    
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"提 现" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor whiteColor];
    [button addTarget:self action:@selector(cashBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];

    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(50);
    }];
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom).offset(-50);
    }];
    

    // Do any additional setup after loading the view.
}


-(void)cashBtnClick:(id)sender{
    switch ([RCUserCacheManager sharedManager].currentUser.is_approve) {
        case ApproveType_Success:
            [self pushViewControllerWithName:@"MineToCashVC"];
            break;
        default:
            [self pushViewControllerWithName:@"ApproveVC"];
            break;
    }
    
}



-(void)openHelp:(id)sender{
    [[RouteController sharedManager] openClassVC:@"MineHelpVC"];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self wr_setNavBarShadowImageHidden:NO];
    [[RCUserCacheManager sharedManager] renewAccount];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self wr_setNavBarShadowImageHidden:YES];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dict = self.datas[indexPath.row];
    [self pushViewControllerWithName:dict[@"vc"]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 30)];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = LS(@"其他服务");
    label.font = DetailFont;
    label.textColor = AppGray;
    
    label.frame = CGRectMake(10, 0, APP_WIDTH, 30);
    [view addSubview:label];
    
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return APP_HEIGHT / 3.0 - 1.0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
//  NormalTableViewCell *cell = [NormalTableViewCell cellWithType:NormalCellType_Access identifier:@"Cell" tableView:tableView];
    
    MineHomeCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[MineHomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    cell.datas = self.datas;
    
    return cell;
}


-(UIView *)currencyView{
    ImageButton *priceView = [[ImageButton alloc]init];
    priceView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    priceView.layer.cornerRadius = 3;
    priceView.layer.masksToBounds = YES;
    priceView.contentImage.image = [UIImage imageNamed:@"ic_travel_icon"];
    priceView.titleLbl.textColor = AppRed;
    RAC(priceView.titleLbl,text) = RACObserve([RCUserCacheManager sharedManager], currentUser.currency);
    return priceView;
}

-(UIView *)integralView{
    UIView *bgView = [[UIView alloc]init];
    
    bgView.layer.cornerRadius = 3;
    bgView.layer.masksToBounds = YES;
    bgView.backgroundColor = [UIColor orangeColor];
    
    UILabel *label = [ControllerHelper autoFitLabel];
    label.textColor = [UIColor whiteColor];
    label.font = SystemFont(11);
    [bgView addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView.mas_left).offset(5);
        make.right.equalTo(bgView.mas_right).offset(-5);
        make.top.equalTo(bgView.mas_top);
        make.bottom.equalTo(bgView.mas_bottom);
    }];
    
    
    RAC(label,text) = RACObserve([RCUserCacheManager sharedManager], currentUser.integral);
    
    return bgView;
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
