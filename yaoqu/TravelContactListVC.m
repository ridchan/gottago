//
//  TravelContactListVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/12.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelContactListVC.h"
#import "TravelContactCell.h"
#import "TravelContactListApi.h"
#import "ContactAddVC.h"
#import "ContactApi.h"

@interface TravelContactListVC ()<UITableViewDelegate,UITableViewDataSource>



@end

@implementation TravelContactListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"联系人列表");
    
    [self setRightItemWithTitle:LS(@"添加") selector:@selector(addBtnClick:)];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
//    [self startRefresh];
    // Do any additional setup after loading the view.
}


-(void)addBtnClick:(id)sender{
    [self pushViewControllerWithName:@"ContactAddVC"];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self startRefresh];
}

-(void)startRefresh{
    [super startRefresh];
    WEAK_SELF;
    self.page = 1;
    NSDictionary *dict = @{@"page":@(self.page),
                           @"size":@(self.size)
                           };
    
    
    [[[ContactApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
    
        [weakSelf refreshComplete:info response:[TravelContactModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]]];
    }];
}

-(void)startloadMore{
    WEAK_SELF;
    NSDictionary *dict = @{@"page":@(++self.page),
                           @"size":@(self.size)
                           };
    [[[ContactApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        
        [weakSelf loadMoreComplete:info response:[TravelContactModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]]];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        WEAK_SELF;
        TravelContactModel *model = self.datas[indexPath.row];
        ContactApi *api = [[ContactApi alloc]initWitObject:@{@"contact_id":model.contact_id}];
        api.method = YTKRequestMethodDELETE;
        [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            if (info.error == ErrorCodeType_None) {
                [weakSelf.datas removeObjectAtIndex:indexPath.item];
                [weakSelf.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
            }
        }];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self.delegate respondsToSelector:@selector(didSelectContact:)]) {
        [self.delegate didSelectContact:self.datas[indexPath.row]];
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self pushViewControllerWithName:@"ContactAddVC" params:self.datas[indexPath.row]];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TravelContactCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[TravelContactCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    UIView *editView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    editView.backgroundColor = [UIColor yellowColor];
    cell.editingAccessoryView = editView;
    cell.model = self.datas[indexPath.row];
    
    return cell ;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}



//-(NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return nil;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
