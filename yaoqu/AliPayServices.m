//
//  AliPayServices.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/9.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "AliPayServices.h"
#import <AlipaySDK/AlipaySDK.h>
#import "LLUtils.h"

@implementation AliPayServices

-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    if ([url.host isEqualToString:@"safepay"]) {
        //跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
//            [LLUtils showTextHUD:[resultDic objectForKey:@"memo"]];
            PostNotification(AliPayNotification, resultDic);
        }];
        return YES;
    }

    return NO;
}


-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    if ([url.host isEqualToString:@"safepay"]) {
        //跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
//            [LLUtils showTextHUD:[resultDic objectForKey:@"memo"]];
            PostNotification(AliPayNotification, resultDic);
        }];
        return YES;
    }

    return NO;
}

@end
