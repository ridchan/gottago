//
//  SimpleUserModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface SimpleUserModel : BaseModel

@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *sex;
@property(nonatomic,strong) NSString *image;

@end
