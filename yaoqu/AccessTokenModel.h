//
//  AccessTokenModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/1.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

typedef NS_ENUM(NSInteger,AccessTokenStatu) {
    AccessTokenStatu_Using,
    AccessTokenStatu_Refreshing,
    AccessTokenStatu_OutDating
};

@interface AccessTokenModel : BaseModel

@property(nonatomic,strong) NSString *access_token;
@property(nonatomic) NSInteger expires;
@property(nonatomic,strong,readonly) NSDate *exprieDate;
@property(nonatomic) AccessTokenStatu statu;

@end
