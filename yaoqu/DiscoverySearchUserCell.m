//
//  DiscoverySearchUserCell.m
//  yaoqu
//
//  Created by ridchan on 2017/9/4.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DiscoverySearchUserCell.h"
#import "NormalCellLayout.h"
#import "UserModel.h"

@interface SearchUserCell()

@property(nonatomic,strong) UILabel *coverLbl;

@end

@implementation SearchUserCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(10);
            make.top.equalTo(self.mas_top);
            make.right.equalTo(self.mas_right).offset(-10);
            make.height.mas_equalTo(self.imageView.mas_width);
        }];
        
        [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left);
            make.right.equalTo(self.mas_right);
            make.top.equalTo(self.imageView.mas_bottom).offset(5);
        }];
        
        [self.coverLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(10);
            make.top.equalTo(self.mas_top);
            make.right.equalTo(self.mas_right).offset(-10);
            make.height.mas_equalTo(self.imageView.mas_width);
        }];
    }
    return self;
}

-(UILabel *)coverLbl{
    if (!_coverLbl) {
        _coverLbl = [[UILabel alloc]init];
        _coverLbl.layer.masksToBounds = YES;
        _coverLbl.backgroundColor = RGBA(0, 0, 0, 0.8);
        _coverLbl.text = @"更多";
        _coverLbl.textColor = [UIColor whiteColor];
        _coverLbl.textAlignment = NSTextAlignmentCenter;
        _coverLbl.hidden = YES;
        [self addSubview:_coverLbl];
    }
    return _coverLbl;
}

-(UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc]init];
        _imageView.layer.masksToBounds = YES;
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:_imageView];
    }
    return _imageView;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        _nameLbl.font = SystemFont(12);
        _nameLbl.textColor = AppBlack;
        _nameLbl.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(void)setIsMoreCell:(BOOL)isMoreCell{
    _isMoreCell = isMoreCell;
    _coverLbl.hidden = !isMoreCell;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    self.imageView.layer.cornerRadius = self.imageView.frame.size.width / 2;
    self.coverLbl.layer.cornerRadius = self.coverLbl.frame.size.width / 2;
}

@end

@interface DiscoverySearchUserCell ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UICollectionView *collectionView;

@end

@implementation DiscoverySearchUserCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self commonInit];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)commonInit{
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        NormalCellLayout *layout = [[NormalCellLayout alloc]init];
        layout.numOfRow = 1;
        layout.numOfColumn = 4;
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        layout.minimumLineSpacing = 30;
        layout.minimumInteritemSpacing = 30;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerClass:[SearchUserCell class] forCellWithReuseIdentifier:@"Cell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [self addSubview:_collectionView];
    }
    return _collectionView;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.item >= 3){
        if ([self.delegate respondsToSelector:@selector(discoverySearchUserCell:moreBtnClick:)]) {
            [self.delegate discoverySearchUserCell:nil moreBtnClick:nil];
        }
    }else{
        UserModel *model = self.members[indexPath.item];
        [[RouteController sharedManager]openHomePageVC:model];
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return MIN(self.members.count, 4);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SearchUserCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    UserModel *model = self.members[indexPath.item];
    [cell.imageView setImageName:model.image placeholder:nil];
    cell.nameLbl.text = model.username;
    cell.isMoreCell = indexPath.item >= 3;
    return cell;
}

-(void)setMembers:(NSArray *)members{
    _members = members;
    [self.collectionView reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
