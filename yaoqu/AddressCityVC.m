//
//  AddressCityVC.m
//  yaoqu
//
//  Created by ridchan on 2017/7/5.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "AddressCityVC.h"
#import "RCUserCacheManager.h"

@interface AddressCityVC ()

@end

@implementation AddressCityVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"市");
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.proModel.city_list.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    CityModel *city = [self.proModel.city_list objectAtIndex:indexPath.row];
    cell.textLabel.text = city.city;
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CityModel *city = [self.proModel.city_list objectAtIndex:indexPath.row];
    
    [RCUserCacheManager sharedManager].currentCity = city;
    
    [self.navigationController popToRootViewControllerAnimated:YES];
//    self.model.pro_id = self.proModel.pro_id;
//    self.model.pro_name = self.proModel.pro;
//    self.model.city_id = city.city_id;
//    self.model.city_name = city.city;
    
//    [self returnViewControllerWithName:@"UserLocationEditVC"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
