//
//  CommonConfigManager.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "CommonConfigManager.h"
#import "CommonConfigApi.h"
#import <YTKNetwork/YTKNetwork.h>
#import "AddressGetApi.h"
#import "CommonApi.h"
#import "Reachability.h"
#import "RCUserCacheManager.h"
@interface CommonConfigManager(){
    CommonApi *addresApi;
    CommonApi *commonApi;
}


@property(nonatomic,strong) NSDictionary *addressDict;
@property(nonatomic,strong) Reachability *hostReach;
@property(nonatomic,strong) NSString *isNetWork;

@end

@implementation CommonConfigManager

CREATE_SHARED_MANAGER(CommonConfigManager)

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.hostModel = [HostModel searchSingleWithWhere:@"1 = 1" orderBy:@"private_id"];
        addresApi = [[CommonApi alloc]initWithModel:@"common/address/get" object:nil] ;
        commonApi = [[CommonApi alloc]initWithModel:@"common/config" object:nil];
        
        
        [RACObserve(self , api_host) subscribeNext:^(id x) {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
            
            self.hostReach = [Reachability reachabilityWithHostName:self.api_host] ;
            
            //开始监听，会启动一个run loop
            
            [self.hostReach startNotifier];
        }];
        
 
    }
    return self;
}


-(void)reachabilityChanged:(NSNotification *)note{
    
    Reachability *currReach = [note object];
    
    NSParameterAssert([currReach isKindOfClass:[Reachability class]]);
    
    //对连接改变做出响应处理动作
    
    NetworkStatus status = [currReach currentReachabilityStatus];
    
    //如果没有连接到网络就弹出提醒实况
    
//    self.isReachable = YES;
    
    if(status == NotReachable)
        
    {
        
        self.isNetWork = @"0";
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"网络连接异常" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//        
//        [alert show];
        return;
        
    }
    
    if (status== ReachableViaWiFi||status== ReachableViaWWAN) {
        
        self.isNetWork = @"1";
        
    }
    
    
    
}

-(void)startConfig{
    [self getCommonConfig];
    [self getAddresss];

}

-(void)getCommonConfig{
    WEAK_SELF;
    
    [commonApi startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None ) {
            weakSelf.hostModel = [HostModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
            //            [[YTKNetworkConfig sharedConfig] setBaseUrl:_hostModel.api_host];
        }else{
            [RACObserve(weakSelf, isNetWork) subscribeNext:^(id x) {
                if ([x boolValue]) {
                    [weakSelf performSelectorOnMainThread:@selector(getCommonConfig) withObject:nil waitUntilDone:NO];
                }
            }];
            
        }
    }];
}


-(void)getAddresss{
    WEAK_SELF;
    
    
    [addresApi startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            weakSelf.addressDict = responseObj;
        }else{
            [RACObserve(weakSelf, isNetWork) subscribeNext:^(id x) {
                if ([x boolValue]) {
                    [weakSelf performSelectorOnMainThread:@selector(getAddresss) withObject:nil waitUntilDone:NO];
                }
            }];
            
        }
    }];
}

-(void)setHostModel:(HostModel *)hostModel{
    [HostModel deleteWithWhere:@"1 = 1"];
    _hostModel = hostModel;
    
    _image_host = hostModel.image_host;
    _video_host = hostModel.video_host;
    _api_host = hostModel.api_host;
    _tpl_host = hostModel.tpl_host;
    [[RCUserCacheManager sharedManager] openLink:_api_host];
    
    [_hostModel updateToDB];
//    [[YTKNetworkConfig sharedConfig] setBaseUrl:_hostModel.api_host];
    
    
}

-(NSString *)video_host{
    if (!_video_host) {
        _video_host = MainDomain;
    }
    return _video_host;
}

-(NSString *)api_host{
    if (!_api_host) {
        _api_host = MainDomain;
    }
    return _api_host;
}

-(NSString *)tpl_host{
    if (!_tpl_host) {
        _tpl_host = MainDomain;
    }
    return _tpl_host;
}


-(NSArray<ProModel *> *)addressArray{
    if (!_addressArray) {
        _addressArray = [ProModel objectArrayWithKeyValuesArray:[[self.addressDict objectForKey:@"data"] objectForKey:@"pro_list"]];
    }
    return _addressArray; 
}


-(NSMutableArray *)explainAddress{
    NSMutableArray *array = [NSMutableArray array];
    NSArray *proArr = [ProModel objectArrayWithKeyValuesArray:[self.addressDict objectForKey:@"pro"]];
    NSArray *cityArr = [CityModel objectArrayWithKeyValuesArray:[self.addressDict objectForKey:@"city"]];
    
    for (ProModel*dic in proArr){
        NSString *proKey = [dic.pro_id substringToIndex:3];
        NSString *fliter = [NSString stringWithFormat:@"city_id BEGINSWITH '%@'",proKey];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:fliter];
        NSArray *citys = [cityArr filteredArrayUsingPredicate:predicate];
        
//        dic.citys = [NSMutableArray arrayWithArray:citys];
        [array addObject:dic];
    }
    
    return array;
}


-(NSString *)image_host{
    return _image_host;
}


@end
