//
//  DebugConstants.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/12.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#define DEBUG_HTTP NO
#define DEBUG_ANALYTICS NO


#ifdef DEBUG
#define debugLog(...) NSLog(__VA_ARGS__)
#define debugMethod() NSLog(@"%s", __func__)
#define NSLog(...) NSLog(__VA_ARGS__)
#else
#define debugLog(...)
#define debugMethod()
#define NSLog(...)
#endif

/* DebugConstants_h */
