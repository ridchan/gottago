//
//  EaseMobManager.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "EaseMobManager.h"
#import "EMClient.h"
#import "MBProgressHUD+XIE.h"
#import "UserModel.h"

@implementation EaseMobManager

CREATE_SHARED_MANAGER(EaseMobManager)

-(void)login:(UserModel *)userModel{
    [[EMClient sharedClient] asyncLoginWithUsername:userModel.member_id  password:@"@&1234567" success:^{
        
    } failure:^(EMError *aError) {
        NSLog(@"error %@",aError);
    }];
}

-(void)logout{
    [[EMClient sharedClient] asyncLogout:YES success:^{
        
    } failure:^(EMError *aError) {
        
    }];
}

@end
