//
//  UserFollowApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface UserFollowListApi : BaseApi

-(id)initWithMember:(NSString *)member_id page:(NSInteger)page size:(NSInteger)size;

@end
