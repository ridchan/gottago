//
//  NewDynamicApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/20.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NewDynamicApi.h"


@interface NewDynamicApi(){
    
    NSString *_desc;
    NSString *_images;
    NSString *_video;
    BOOL _is_location;
    NSString *_latitude;
    NSString *_longitude;
    NSString *_address;
    
    DynamicUploadModel *uploadModel;
}

//@property(nonatomic,strong) DynamicUploadModel *model;


@end

@implementation NewDynamicApi

-(instancetype)initWithDesc:(NSString *)desc images:(NSString *)images video:(NSString *)video is_location:(BOOL)is_location latitude:(NSString *)latitude longitude:(NSString *)longitude address:(NSString *)address{
    if (self = [super init]) {
        _desc = desc;
        _images = images;
        _is_location = is_location;
        _latitude = latitude;
        _longitude = longitude;
        _address = address;
        _video = video;
    }
    return self;
}

-(instancetype)initWithDynaimcModel:(DynamicUploadModel *)model{
    if (self = [super init]) {
        uploadModel = model;
    }
    return self;
}




-(NSString *)requestUrl{
    return DynamicAddUrl;
}

-(id)requestArgument{
    NSMutableDictionary *dict = [self baseConfig];
//    [dict addEntriesFromDictionary:[self.model keyValues]];
    
    [dict setValue:uploadModel.desc forKey:@"desc"];
    [dict setValue:uploadModel.images forKey:@"images"];
    [dict setValue:@(uploadModel.is_location) forKey:@"is_location"];
    [dict setValue:uploadModel.latitude forKey:@"latitude"];
    [dict setValue:uploadModel.longitude forKey:@"longitude"];
    [dict setValue:uploadModel.address forKey:@"address"];
    [dict setValue:uploadModel.video forKey:@"video"];
    [dict setValue:uploadModel.video_image forKey:@"video_image"];
    
    return dict;
}

@end
