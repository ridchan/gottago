//
//  TravelScheduleDetailVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/24.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelScheduleDetailVC.h"
#import "CommonConfigManager.h"
#import "TravelEnrollVC.h"
#import "YQWebViewVC.h"
#import "RCUserCacheManager.h"
#import "ShareView.h"
#import "CollectApi.h"
#import <WebKit/WebKit.h>

@interface TravelScheduleDetailVC ()<WKUIDelegate,WKNavigationDelegate,WKScriptMessageHandler,UIScrollViewDelegate>

@property(nonatomic,strong) WKWebView *webView;
@property(nonatomic,strong) UIView *bottomView;
@property(nonatomic,strong) UIActivityIndicatorView *indicatorView;



@end

@implementation TravelScheduleDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    
    
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top).offset(64);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
//    [self wr_setNavBarBackgroundAlpha:0];

    
    NSString *link = [[CommonConfigManager sharedManager].hostModel.api_host stringByAppendingPathComponent:@"travel-detail?id="];
    link = [NSString stringWithFormat:@"%@%@&access_token=%@",link,_model.travel_schedule_id,
            [RCUserCacheManager sharedManager].currentToken.access_token];
    
    
    [self deleteCookieInLike:link];
    [self setCookieForLink:link];
    

    
    
    
    NSURL *url = [NSURL URLWithString:link];
    
//    NSString *oldAgent = [self.webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
//    
//    
//    NSString *customUserAgent = nil;
//    
//    
//    if ([oldAgent rangeOfString:@"gottago"].location == NSNotFound){
//        customUserAgent = [NSString stringWithFormat:@"%@( gottago )",oldAgent];
//    }else{
//        customUserAgent = oldAgent;
//    }
//
//    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"UserAgent":customUserAgent}];
    
    
    
//    NSURL *url = [NSURL URLWithString:@"http://m.xiaoniubang.com/demo/test/agent.html"];
    
    NSURLRequest *request2 = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.f];
    
    self.webView.hidden = YES;
    self.indicatorView.hidden = NO;
    [self.indicatorView startAnimating];
    [self.webView loadRequest:request2];
    
    
//    [self setLeftItemWithIcon:[UIImage imageNamed:@"ic_back"] title:nil selector:@selector(closeMe)];
//    [self setIsCollect];
//    [self setRightItemWithIcon:[UIImage imageNamed:@"ic_close"] selector:@selector(closeMe)];
    
    // Do any additional setup after loading the view.
}

-(void)setIsCollect{
    if (self.model.is_collect)
        [self setLeftItemWithIcon:[UIImage imageNamed:@"ic_heart_select"] title:nil selector:@selector(collectBtnClick:)];
    else
        [self setLeftItemWithIcon:[UIImage imageNamed:@"ic_heart"] title:nil selector:@selector(collectBtnClick:)];
}


-(void)collectBtnClick:(id)sender{
    NSDictionary *dict = @{@"type":@(CollectType_Travel),
                           @"key_id":self.model.travel_schedule_id,
                           @"state":@(!self.model.is_collect)
                           };
    WEAK_SELF;
    CollectApi *api = [[CollectApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            weakSelf.model.is_collect = !weakSelf.model.is_collect;
            [weakSelf setIsCollect];
        }
        [LLUtils showActionSuccessHUD:info.message];
    }];
}

-(void)closeMe{
    if (self.navigationController) {
        if ([self.navigationController.viewControllers firstObject] == self) {
            [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }else{
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIActivityIndicatorView *)indicatorView{
    if (!_indicatorView) {
        _indicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

        _indicatorView.hidesWhenStopped = YES;
        [self.view addSubview:_indicatorView];
        [_indicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view.mas_centerX);
            make.centerY.equalTo(self.view.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(20, 20));
        }];
    }
    return _indicatorView;
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.y <= 0) {
        scrollView.contentOffset = CGPointMake(0, 0);
    }else if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height){
        scrollView.contentOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.frame.size.height);
    }
    

    
//    if (scrollView.contentOffset.y < 64) {
//        if (!self.model.is_collect)
//            [(LeftBarButtonItem *)self.navigationItem.leftBarButtonItem setImageColor:[UIColor whiteColor]];
//        [(RightBarButtonItem *)self.navigationItem.rightBarButtonItem setImageColor:[UIColor whiteColor]];
//        [self wr_setNavBarBackgroundAlpha:scrollView.contentOffset.y / 64.0];
//        [self wr_setStatusBarStyle:UIStatusBarStyleDefault];
//    }else{
//        if (!self.model.is_collect)
//            [(LeftBarButtonItem *)self.navigationItem.leftBarButtonItem setImageColor:AppGray];
//        [(RightBarButtonItem *)self.navigationItem.rightBarButtonItem setImageColor:AppGray];
//
//        [self wr_setNavBarBackgroundAlpha:1];
//        [self wr_setNavBarTintColor:[UIColor whiteColor]];
//        [self wr_setNavBarTitleColor:[UIColor whiteColor]];
//        [self wr_setStatusBarStyle:UIStatusBarStyleLightContent];
//    }

}

-(WKWebView *)webView{
    if (!_webView) {
        _webView = [[WKWebView alloc]init];
//        _webView.delegate = self;
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 9.0)
            _webView.customUserAgent = @" gottago ";
        _webView.navigationDelegate = self;
        _webView.UIDelegate = self;
        _webView.scrollView.delegate = self;
        [self.view addSubview:_webView];
    }
    return _webView;
}


-(void)dealloc{
    _webView.navigationDelegate = nil;
    _webView.UIDelegate = nil;
    _webView.scrollView.delegate = nil;
}

#pragma mark -

// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    
}
// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation{
    
}
// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    self.webView.hidden = NO;
    [self.indicatorView stopAnimating];
    WEAK_SELF;
    [webView evaluateJavaScript:@"document.title" completionHandler:^(id _Nullable title, NSError * _Nullable error) {
        weakSelf.title = title;
    }];
    
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation{
    self.webView.hidden = NO;
    [self.indicatorView stopAnimating];
}


- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    //下面这句话一定不能少一少就报错
    decisionHandler(WKNavigationActionPolicyAllow);
    
    NSLog(@"run hear %@",navigationAction.request.URL.absoluteString);
    
    NSString *link = navigationAction.request.URL.absoluteString;
    
    if ([link hasPrefix:@"app://open/link/"]){
        YQWebViewVC *vc = [[YQWebViewVC alloc]init];
        vc.paramObj = self.model;
        vc.link = [link substringFromString:@"open/link/"];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([link hasPrefix:@"app://open/returnShare"]){
//        NSString *stringValue = [self.webView stringByEvaluatingJavaScriptFromString:@"returnShare()"];
        [self.webView evaluateJavaScript:@"returnShare()" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        
            if (result){
                ShareView *shareView = [[ShareView alloc]init];
                shareView.shareInfo = result;
                shareView.handleBlock = ^(id Obj){
                    if ([Obj integerValue] == ShareType_Copy){
                        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                        pasteboard.string = [result objectForKey:@"link"];
                        [LLUtils showActionSuccessHUD:@"已复制"];
                    }else{
                        
                    }
                    
                };
                [shareView show];
            }

        }];
    }
}


- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler{
    //下面这句话一定不能少一少就报错
    decisionHandler(WKNavigationResponsePolicyAllow);
    NSLog(@"run there");
}

// 接收到服务器跳转请求之后调用
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation{
    NSLog(@"%@",navigation.description);
}



-(void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    NSLog(@"cmd %@",NSStringFromSelector(_cmd));
    NSLog(@"message body %@",message.body);
}


#pragma mark -

-(UIView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = [UIColor whiteColor];
        
        UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn1 setTitle:LS(@"马上参加") forState:UIControlStateNormal];
        [_bottomView addSubview:btn1];
        
        UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
        btn2.backgroundColor = [UIColor orangeColor];
        [btn2 setImage:[UIImage imageNamed:@"ic_gift"] forState:UIControlStateNormal];
        [_bottomView addSubview:btn2];
        
        [btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bottomView.mas_left);
            make.top.equalTo(_bottomView.mas_top);
            make.bottom.equalTo(_bottomView.mas_bottom);
            make.right.equalTo(btn2.mas_left);
        }];
        
        [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bottomView.mas_right);
            make.top.equalTo(_bottomView.mas_top);
            make.bottom.equalTo(_bottomView.mas_bottom);
            make.width.equalTo(btn2.mas_height);
        }];

        [self.view addSubview:_bottomView];
        
    }
    return _bottomView;
}


-(void)setCookieForLink:(NSString *)link{
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:[RCUserCacheManager sharedManager].currentToken.access_token forKey:NSHTTPCookieValue];
    [dict setValue:@"access_token" forKey:NSHTTPCookieName];
    [dict setValue:@".gottago917.com" forKey:NSHTTPCookieDomain];
    [dict setValue:@"/" forKey:NSHTTPCookiePath];
    [dict setValue:[NSDate dateWithTimeIntervalSinceNow:3600 * 24] forKey:NSHTTPCookieExpires];
    [dict setValue:[NSURL URLWithString:link] forKey:NSHTTPCookieOriginURL];
//    [dict setValue:[NSURL URLWithString:link] forKey:NSHTTPCookieCommentURL];
    

    
    
    NSArray *cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:
                           [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"access_token=%@",[RCUserCacheManager sharedManager].currentToken.access_token]
                                                       forKey:@"Set-Cookie"]
                                                                 forURL:[NSURL URLWithString:link]];
    

    NSLog(@"%@========",[NSHTTPCookie requestHeaderFieldsWithCookies:cookies]);
    
    NSArray *array = @[[cookies firstObject]];
    
    
    
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookies:array
                                                       forURL:[NSURL URLWithString:link]
                                              mainDocumentURL:nil];
    
    
    
}


- (void)deleteCookieInLike:(NSString *)link{
    NSHTTPCookie *cookie;
    
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    NSArray *cookieAry = [cookieJar cookiesForURL: [NSURL URLWithString:link]];
    
    for (cookie in cookieAry) {
        NSLog(@"delete %@ = %@",cookie.name,cookie.value);
        [cookieJar deleteCookie: cookie];
        
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
