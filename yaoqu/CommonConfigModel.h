//
//  CommonConfigModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"
#import "UserModel.h"

@interface CommonConfigModel : BaseModel

@end


@interface CardTypeModel : BaseModel

@property(nonatomic,strong) NSString *card_type_id;
@property(nonatomic,strong) NSString *card_type_name;

@end

@interface GiftModel : UserModel

@property(nonatomic,strong) NSString *gift_id;
@property(nonatomic,strong) NSString *gift_name;
@property(nonatomic,strong) NSString *gift_image;
@property(nonatomic,strong) NSString *gift_desc;
//@property(nonatomic,strong) NSString *currency;
@property(nonatomic,strong) NSString *qty;
@property(nonatomic,strong) NSString *message;


@property(nonatomic,strong) NSString *isGiftSelected;

@end

@interface LinkDetailModel : BaseModel

@property(nonatomic,strong) NSString *file;
@property(nonatomic,strong) NSString *fiel;
@property(nonatomic,strong) NSString *ver;

@end

@interface LinkModel : BaseModel

@property(nonatomic,strong) LinkDetailModel *discovery_hot;
@property(nonatomic,strong) LinkDetailModel *discovery_ranking;
@property(nonatomic,strong) LinkDetailModel *travel_home;

@end



@interface CurrencyValueModel : BaseModel

@property(nonatomic,strong) NSString *giftroll;
@property(nonatomic,strong) NSString *currency;
@property(nonatomic,strong) NSString *price;
@property(nonatomic,strong) NSString *expand;
@property(nonatomic,strong) NSString *give;

@end


@interface MemberValueModel : BaseModel

@property(nonatomic,strong) NSString *desc;
@property(nonatomic,strong) NSString *val;
@property(nonatomic,strong) NSString *price;
@property(nonatomic,strong) NSString *pay_total;
@property(nonatomic,strong) NSString *is_default;

@end


@interface HostModel : BaseModel


@property(nonatomic,strong) NSString *image_host;
@property(nonatomic,strong) NSString *api_host;
@property(nonatomic,strong) NSString *video_host;
@property(nonatomic,strong) NSString *tpl_host;
@property(nonatomic,strong) NSString *version;
@property(nonatomic,strong) NSString *scale;

@property(nonatomic,strong) NSArray *report_desc;
@property(nonatomic,strong) NSArray *make_friends;
@property(nonatomic,strong) NSArray *sex_orientation;
@property(nonatomic,strong) NSArray <CardTypeModel *> *card_type;
@property(nonatomic,strong) NSArray <GiftModel *>*gift;
@property(nonatomic,strong) NSArray <GiftModel *>*gift_depot;
@property(nonatomic,strong) LinkModel *link;
@property(nonatomic,strong) NSArray <CurrencyValueModel *> *currency_val;
@property(nonatomic,strong) NSArray <CurrencyValueModel *> *giftroll_val;
@property(nonatomic,strong) NSArray <MemberValueModel *> *member_val;

@end


