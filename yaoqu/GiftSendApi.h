//
//  GiftSendApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/12.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface GiftSendApi : BaseApi

-(id)initWithID:(NSString *)gift_id type:(GiftType)type keyID:(NSString *)key_id desc:(NSString *)desc memberID:(NSString *)member_id;

@end
