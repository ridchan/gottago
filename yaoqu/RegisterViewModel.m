//
//  RegisterViewModel.m
//  QBH
//
//  Created by 陳景雲 on 2016/12/25.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import "RegisterViewModel.h"
#import "PhoneCodeApi.h"
#import "RegistApi.h"
#import "QiniuSDK.h"
#import "NSString+Extend.h"
#import "QinniuCodeApi.h"
#import "RCUserCacheManager.h"
#import "LoginApi.h"
#import "CommonApi.h"
#import "LLUtils.h"
#import "MemberApi.h"

@implementation RegisterViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.model = [[LoginPostModel alloc]init];
        self.succSubject = [RACSubject subject];
        self.failSubject = [RACSubject subject];
        self.registSubject = [RACSubject subject];
        RACSignal *textSignal = RACObserve(self , phoneNum);
        self.btnEnableSignal = [textSignal map:^id(id value) {
            return @([self isMobileNumber:value]);
        }];
    }
    return self;
}

- (BOOL)isMobileNumber:(NSString *)mobileNum
{
    //正则表达式
    NSString *mobile = @"^1\\d{10}$";
    NSPredicate *regextestMobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobile];
    if ([regextestMobile evaluateWithObject:mobileNum] == YES) {
        return YES;
    }else {
        return NO;
    }
}

-(void)setSendCodeSignal:(RACSignal *)sendCodeSignal{
    _sendCodeSignal = sendCodeSignal;
    
    WEAK_SELF;
    [_sendCodeSignal subscribeNext:^(id x) {
        [weakSelf sendCode];
    }];
}

-(void)setRegistSignal:(RACSignal *)registSignal{
    _registSignal = registSignal;
    
    WEAK_SELF;
    [_registSignal subscribeNext:^(id x) {
        [weakSelf regist];
    }];
}


-(void)sendCode{
    
    
//    [[CommonApi alloc]initWithModel:@"common/code" object:@{@"phone":self.phoneNum}]
    __block MBProgressHUD *hud = [LLUtils showActivityIndicatiorHUDWithTitle:nil];
    [[[CommonApi alloc]initWithModel:@"common/code" object:@{@"phone":self.phoneNum,@"type":@"reg"}] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [hud hideAnimated:NO];
        if (info.error == ErrorCodeType_None) {
            [self.succSubject sendNext:nil];
            [LLUtils showActionSuccessHUD:@"发送成功"];
        }else{
            [self.failSubject sendNext:nil];
            [LLUtils showCenterTextHUD:info.message];
        }
    }];
}

-(void)regist{
    __block MBProgressHUD *hud =  [LLUtils showActivityIndicatiorHUDWithTitle:nil];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        __block NSString *token = nil;
        
        [[[CommonApi alloc]initWithModel:@"common/uptoken" object:nil]startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            if (info.error == ErrorCodeType_None){
                token = [responseObj objectForKey:@"data"];
            }else{
                token = @"";
            }
        }];
        
        while (token == nil) {
            [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }
        
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:_model.imagelink]];
        NSString *key = [NSString randomString];
        __block NSString *imageName = nil;
        [[[QNUploadManager alloc]init] putData:data key:key token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
            if (!info.error) {
                imageName = key;
            }else{
                imageName = @"";
            }
            NSLog(@"info %@",info.error.description);
        } option:nil];
        
        while (imageName == nil) {
            [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }
        
//        WEAK_SELF;
        self.model.image = imageName;
        self.model.password = @"123456";
        self.model.password2 = @"123456";
        LoginApi *loginApi = [[LoginApi alloc]initWitObject:self.model];
        loginApi.method = YTKRequestMethodPOST;
        
        
        [loginApi startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            [hud hideAnimated:NO];
            
            if (info.error != ErrorCodeType_None){
                [hud hideAnimated:NO];
                [LLUtils showCenterTextHUD:info.message];
                return ;
            }
            
            LoginInfoModel *model = [LoginInfoModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
            
            [[RCUserCacheManager sharedManager]setCurrentLoginInfo:model];
            

            
            [[[CommonApi alloc]initWithModel:@"accessToken" object:model] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                
                if (info.error != ErrorCodeType_None){
                    [hud hideAnimated:NO];
                    [LLUtils showCenterTextHUD:info.message];
                    return ;
                }

                AccessTokenModel *model = [AccessTokenModel objectWithKeyValues:responseObj];
                [[RCUserCacheManager sharedManager] setCurrentToken:model];
                
                [[[MemberApi alloc]initWitObject:responseObj] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                    
                    if (info.error != ErrorCodeType_None){
                        [hud hideAnimated:NO];
                        [LLUtils showCenterTextHUD:info.message];
                        return ;
                    }

                    UserModel *userModel = [UserModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
                    [[RCUserCacheManager sharedManager] setCurrentUser:userModel];
                    [[RouteController sharedManager] openMainController];
                    
                }];
                
            }];
            
        }];
        

    });
    
}


@end
