//
//  OtherHomePageTripView.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/24.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherHomePageTripView : UIView

@property(nonatomic,strong) NSArray *trips;

@end
