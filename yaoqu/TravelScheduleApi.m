//
//  TravelScheduleApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/18.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelScheduleApi.h"

@implementation TravelScheduleApi

-(id)initWithSchedule:(NSString *)schedule_id{
    if (self = [super init]) {
        [self.params setValue:schedule_id forKey:@"travel_schedule_id"];
    }
    return self;
}

-(NSString *)requestUrl{
    return TravelScheduleUrl;
}

@end
