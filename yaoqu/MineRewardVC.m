//
//  MineRewardVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineRewardVC.h"
#import "MineRewardHeader.h"
#import "RewardApi.h"
#import "MineRewardCell.h"
#import "MineRewardModel.h"
@interface MineRewardVC ()

@property(nonatomic,strong) MineRewardHeader *headerView;
@property(nonatomic,strong) NSString *YearAndMonth;
@end

@implementation MineRewardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"我的奖励");
    
    [self setRightItemWithIcon:[UIImage imageNamed:@"ic_question"] selector:nil];
    
    [self wr_setNavBarShadowImageHidden:NO];
    
    self.headerView = [[MineRewardHeader alloc]init];
    self.headerView.frame = CGRectMake(0, 0, APP_WIDTH, 150);
    self.tableView.tableHeaderView = self.headerView;
    [self.tableView registerClass:[MineRewardCell class] forCellReuseIdentifier:@"Cell"];
    [self startRefresh];
    
    
    [RACObserve(self.headerView, YearAndMonth) subscribeNext:^(id x) {
        if ([x length] > 0) {
            self.YearAndMonth = x;
            [self startRefresh];
      
        }
    }];
    // Do any additional setup after loading the view.
}

-(void)startRefresh{
    WEAK_SELF;
    self.page = 1;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:@(self.page) forKey:@"page"];
    [dict setValue:@(self.size) forKey:@"size"];
    [dict setValue:self.YearAndMonth forKey:@"date"];
    [[[RewardApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            weakSelf.headerView.dict = responseObj[@"data"][@"detail"];
        }
        NSArray *detail_list = [MineRewardModel objectArrayWithKeyValuesArray:responseObj[@"data"][@"detail_list"]];
        [weakSelf refreshComplete:info response:detail_list];
    }];
}

-(void)startloadMore{
    WEAK_SELF;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:@(++self.page) forKey:@"page"];
    [dict setValue:@(self.size) forKey:@"size"];
    [dict setValue:self.YearAndMonth forKey:@"date"];
    
    [[[RewardApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            weakSelf.headerView.dict = responseObj[@"data"][@"detail"];
        }
        NSArray *detail_list = [MineRewardModel objectArrayWithKeyValuesArray:responseObj[@"data"][@"detail_list"]];
        [weakSelf loadMoreComplete:info response:detail_list];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (self.datas.count == 0) return nil;
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 50)];
    
    UIView *bgView =[[UIView alloc]initWithFrame:CGRectMake(0, 10, APP_WIDTH , 40)];
    bgView.backgroundColor = [UIColor whiteColor];
    [view addSubview:bgView];
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 39.5, APP_WIDTH, 0.5)];
    line.backgroundColor = AppLineColor;
    [bgView addSubview:line];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"奖励";
    label.font = SystemFont(14);
    label.frame = CGRectMake(10, 0, APP_WIDTH - 10, 40);
    [bgView addSubview:label];
    
    return view;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MineRewardCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.model = self.datas[indexPath.row];
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
