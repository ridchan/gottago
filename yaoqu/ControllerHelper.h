//
//  ControllerHelper.h
//  QBH
//
//  Created by 陳景雲 on 2017/1/4.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ControllerHelper : NSObject


+(UIButton *)buttonWithTitle:(NSString *)title Color:(UIColor *)color;

+(UILabel *)autoFitLabel;

+(UIView *)doubleLine;

@end



@interface ImageButton : UIButton

@property(nonatomic,strong) UIImageView *contentImage;
@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic) NSTextAlignment alignment;

-(void)setAutoFit;
-(void)setFix;
-(void)setFixImage:(CGSize)size;
-(CAKeyframeAnimation *)keyAnimation;

@end

@interface NormalButton : UIButton

@property(nonatomic,strong) UIColor *normalBackgroundColor;
@property(nonatomic,strong) UIColor *disableBackGroundColor;
@property(nonatomic,strong) UIColor *normalShadowColor;
@property(nonatomic,strong) UIColor *disableShadowColor;

@property(nonatomic,strong) UIColor *normalTitleColor;
@property(nonatomic,strong) UIColor *normalTitleShadowColor;
@property(nonatomic,strong) UIColor *disableTitleShadowColor;

+(NormalButton *)normalButton:(NSString *)title;
+(NormalButton *)whiteButtton:(NSString *)title;

@end

@interface UIView (RCExtend)

-(UIView *)lineInColor:(UIColor *)color;
-(void)setBubble:(NSString *)bubble offset:(CGSize)size;

@end


@interface UILabel (RCExtend)

-(void)addTarget:(id)target action:(SEL)action;

@end
