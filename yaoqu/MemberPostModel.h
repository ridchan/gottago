//
//  MemberPostModel.h
//  yaoqu
//
//  Created by ridchan on 2017/8/2.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface MemberPostModel : BaseModel

@property(nonatomic,strong) NSString *image;//	用户头像	false	json
@property(nonatomic,strong) NSString *username;//	昵称	false	string
@property(nonatomic,strong) NSString *sex;//	性别	false	int		0-2
@property(nonatomic,strong) NSString *desc;//	个性签名	false	string
@property(nonatomic,strong) NSString *age;//	年龄	false	int		自动跟随生日
@property(nonatomic,strong) NSString *birthday;//	生日	false	string
@property(nonatomic,strong) NSString *constellation;//	星座	false	json
@property(nonatomic,strong) NSString *is_show_from;//	来自	false	int
@property(nonatomic,strong) NSString *is_show_location;//	当前所在地	false	int
@property(nonatomic,strong) NSString *pro_id;//	省份ID	false	int
@property(nonatomic,strong) NSString *pro_name;//	身份名称	false	string
@property(nonatomic,strong) NSString *city_id;//	城市ID	false	int
@property(nonatomic,strong) NSString *city_name;//	城市名称	false	json
@property(nonatomic,strong) NSString *images;//	用户头像	true	json


@end
