//
//  TravelEnrollModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface TravelEnrollModel : BaseModel

@property(nonatomic,strong) NSString *integral;
@property(nonatomic,strong) NSString *currency;
@property(nonatomic) PayType pay_type;
@property(nonatomic,strong) NSString *is_return_total;
@property(nonatomic,strong) NSString *travel_schedule_id;
@property(nonatomic,strong) NSString *title;

@property(nonatomic,strong) NSString *price;
@property(nonatomic,strong) NSString *pay_total;
@property(nonatomic,strong) NSString *total;


@end
