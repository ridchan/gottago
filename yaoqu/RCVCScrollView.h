//
//  RCVCScrollView.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/24.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCVCScrollView : UIView


@property(nonatomic,strong) NSArray *vcNames;
@property(nonatomic,strong) NSArray *vcParams;
@property(nonatomic) NSInteger selectIndex;

@property(nonatomic) CGFloat contentOffsetX;

@property(nonatomic,copy) BaseBlock selectBlock;

@end
