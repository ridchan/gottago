//
//  CoinChargeView.m
//  yaoqu
//
//  Created by ridchan on 2017/7/10.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "CoinChargeView.h"




@implementation CoinChargeCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self commonInit];
    }
    return self;
}

-(void)setModel:(CurrencyValueModel *)model{
    _model = model;
    
    self.descLbl.attributedText = [self descAttDesc:model.currency other:model.expand];
//    self.priceLbl.text = [NSString stringWithFormat:@"%@%@",MoneySign,model.price];
    
    
}

-(NSMutableAttributedString *)descAttDesc:(NSString *)str1 other:(NSString *)str2{
    NSString *str = [NSString stringWithFormat:@"%@ %@",str1,str2];
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc]initWithString:str];
    
    [att addAttribute:NSForegroundColorAttributeName value:AppOrange range:[str rangeOfString:str2]];
    [att addAttribute:NSFontAttributeName value:SystemFont(10) range:[str rangeOfString:str2]];
    
    return att;
}

-(void)commonInit{
    [self.coinImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.centerY.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.coinImage.mas_right).offset(5);
        make.centerY.equalTo(self.contentView);
    }];
    
    [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.centerY.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(80, 35));
    }];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];
}

-(UIImageView *)coinImage{
    if (!_coinImage) {
        _coinImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_travel_icon"]];
        _coinImage.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_coinImage];
    }
    
    return _coinImage;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.text = @"1234";
        _descLbl.textColor = AppOrange;
        [self.contentView addSubview:_descLbl];
    }
    return _descLbl;
}

-(UILabel *)priceLbl{
    if (!_priceLbl) {
        _priceLbl = [ControllerHelper autoFitLabel];
        _priceLbl.text = @"充值";
        _priceLbl.textAlignment = NSTextAlignmentCenter;
        _priceLbl.textColor = AppOrange;
        _priceLbl.layer.borderColor = AppOrange.CGColor;
        _priceLbl.layer.borderWidth = 1.0;
        _priceLbl.layer.cornerRadius = 3.0;
        _priceLbl.layer.masksToBounds = YES;
        [self.contentView addSubview:_priceLbl];
    }
    return _priceLbl;
}

-(UIView *)line{
    if (!_line) {
        _line = [[UIView alloc]init];
        _line.backgroundColor = AppLineColor;
        [self.contentView addSubview:_line];
    }
    return _line;
}

@end

#pragma mark -

@interface CoinChargeView()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) UIView *bgView;

@property(nonatomic,strong) UIView *topView;
@property(nonatomic,strong) UIView *bottomView;

@property(nonatomic,strong) UITextField *textField;


@end

@implementation CoinChargeView


+(void)show:(NSArray *)datas{
    CoinChargeView *view = [[CoinChargeView alloc]init];
    view.datas = datas;
    view.frame = CGRectMake(0, APP_HEIGHT, APP_WIDTH, view.autoHeight);
    UIWindow *windown = [UIApplication sharedApplication].keyWindow;
    
    [windown addSubview:view.bgView];
    [windown addSubview:view];
    
    
    [UIView animateWithDuration:0.35 animations:^{
        view.transform = CGAffineTransformMakeTranslation(0, - view.autoHeight);
    }];
}

+(void)show:(NSArray *)datas inView:(UIView *)view delegate:(id)delegate{
    CoinChargeView *cv = [[CoinChargeView alloc]init];
    cv.delegate = delegate;
    
    cv.datas = [CommonConfigManager sharedManager].hostModel.currency_val;
    
    
    cv.frame = CGRectMake(0, view.height, view.width, cv.autoHeight);
    
    [view addSubview:cv.bgView];
    [view addSubview:cv];
    
    [UIView animateWithDuration:0.35 animations:^{
        cv.transform = CGAffineTransformMakeTranslation(0, - cv.autoHeight);
    }];

}

+(void)show:(NSArray *)datas inView:(UIView *)view{
    CoinChargeView *cv = [[CoinChargeView alloc]init];
    
    
    cv.datas = [CommonConfigManager sharedManager].hostModel.currency_val;
    
    
    cv.frame = CGRectMake(0, view.height, view.width, cv.autoHeight);
    
    [view addSubview:cv.bgView];
    [view addSubview:cv];
    
    [UIView animateWithDuration:0.35 animations:^{
        cv.transform = CGAffineTransformMakeTranslation(0, - cv.autoHeight);
    }];
}


-(void)dismiss{
    
    WEAK_SELF;
    [self.bgView removeFromSuperview];
    [UIView animateWithDuration:0.35 animations:^{
        weakSelf.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        [weakSelf removeFromSuperview];
    }];
}


-(UIView *)bgView{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, APP_WIDTH, APP_HEIGHT)];
        _bgView.backgroundColor = RGB16(0x000000);
        _bgView.alpha = 0.5;
        
        UITapGestureRecognizer * _tap = [[UITapGestureRecognizer alloc] init];
        [_tap addTarget:self action:@selector(dismiss)];
        [_bgView addGestureRecognizer:_tap];
        
    }
    return _bgView;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self bgView];
        [self commonInit];
        
    }
    return self;
}

-(void)setDatas:(NSArray *)datas{
    _datas = datas;
    
    self.tableView.sd_resetLayout
    .leftEqualToView(self)
    .rightEqualToView(self)
    .topSpaceToView(self.topView, 5)
    .heightIs(50.0 * datas.count);
    
    self.autoHeight = 140 +  50 * datas.count;
}


-(void)commonInit{
    
    self.backgroundColor = [UIColor whiteColor];
    
    self.topView.sd_layout
    .leftEqualToView(self)
    .rightEqualToView(self)
    .topEqualToView(self)
    .heightIs(40);
    
    self.tableView.sd_layout
    .leftEqualToView(self)
    .rightEqualToView(self)
    .topSpaceToView(self.topView, 5)
    .heightIs(50);
    
    self.bottomView.sd_layout
    .leftEqualToView(self)
    .rightEqualToView(self)
    .topSpaceToView(self.tableView, 5)
    .heightIs(100);
    
    self.autoHeight = 190;
    
    [self setupAutoHeightWithBottomView:self.bottomView bottomMargin:0];
    
}


-(UIView *)topView{
    if (!_topView) {
        _topView = [[UIView alloc]init];
        _topView.backgroundColor = [UIColor whiteColor];
        UILabel *label = [ControllerHelper autoFitLabel];
        label.text = @"请选择充值面额";
        [_topView addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_topView).insets(UIEdgeInsetsMake(0, 10, 0, 0));
        }];
        [self addSubview:_topView];
    }
    return _topView;
}

-(UIView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_bottomView];
        
        UILabel *tipLbl  = [ControllerHelper autoFitLabel];
        tipLbl.text = @"输入其他金额";
        tipLbl.textColor = AppGray;
        tipLbl.font = DetailFont;
        [_bottomView addSubview:tipLbl];
        
        [tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bottomView.mas_left).offset(10);
            make.top.equalTo(_bottomView.mas_top).offset(5);
        }];
        
        self.textField = [[UITextField alloc]init];
        self.textField.leftViewMode = UITextFieldViewModeAlways;
        self.textField.rightViewMode = UITextFieldViewModeAlways;
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 20, 30)];
        label.text = MoneySign;
        self.textField.leftView = label;
        
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:@"充值" forState:UIControlStateNormal];
        button.backgroundColor = AppBlue;
        button.layer.cornerRadius = 3.0;
        button.layer.masksToBounds = YES;
        button.frame = CGRectMake(0, 0, 80, 30);
        [button addTarget:self action:@selector(chargeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        self.textField.rightView = button;
        
        [_bottomView addSubview:self.textField];
        
        
        [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bottomView.mas_left).offset(10);
            make.right.equalTo(_bottomView.mas_right).offset(-10);
            make.top.equalTo(tipLbl.mas_top).offset(5);
            make.height.mas_equalTo(50);
        }];
        
        UIView *bottomLine = [[UIView alloc]init];
        bottomLine.backgroundColor = AppLineColor;
        [_bottomView addSubview:bottomLine];
        
        [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bottomView.mas_left).offset(10);
            make.right.equalTo(_bottomView.mas_right).offset(-10);
            make.top.equalTo(self.textField.mas_bottom);
            make.height.mas_equalTo(0.5);
        }];
        
        
        UILabel *showLbl = [ControllerHelper autoFitLabel];
        showLbl.text = @"1旅游币：1RMB";
        showLbl.textColor = AppGray;
        showLbl.font = DetailFont;
        
        [_bottomView addSubview:showLbl];
        
        [showLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bottomView.mas_left).offset(10);
            make.right.equalTo(_bottomView.mas_right).offset(-10);
            make.top.equalTo(self.textField.mas_bottom).offset(5);
            make.height.mas_equalTo(20);
        }];
        
        //
    }
    return _bottomView;
}


-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.scrollEnabled = NO;
        [self addSubview:_tableView];
    }
    return _tableView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CoinChargeCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[CoinChargeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.model = self.datas[indexPath.item];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.delegate respondsToSelector:@selector(didSelectCoin:)]) {
        [self.delegate didSelectCoin:self.datas[indexPath.item]];
    }
    [self dismiss];
}

-(void)chargeBtnClick:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(didSelectCoin:)]) {
        CurrencyValueModel *model = [[CurrencyValueModel alloc]init];
        model.price = self.textField.text;
        model.currency = self.textField.text;
        [self.delegate didSelectCoin:model];
    }
    [self dismiss];

}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
