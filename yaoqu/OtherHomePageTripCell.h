//
//  OtherHomePageTripCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/27.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherHomePageTripCell : UITableViewCell

@property(nonatomic,strong) NSArray *trips;

@end
