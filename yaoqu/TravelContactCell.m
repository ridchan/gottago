//
//  TravelContactCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/12.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelContactCell.h"

@interface TravelContactCell()

@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) UIImageView *sexImage;
@property(nonatomic,strong) UILabel *ageLbl;
@property(nonatomic,strong) UIImageView *phoneImage;
@property(nonatomic,strong) UILabel *phoneLbl;
@property(nonatomic,strong) UIImageView *numImage;
@property(nonatomic,strong) UILabel *numLbl;

@end

@implementation TravelContactCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self commonInit];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self rac_prepareForReuseSignal];
        [RACObserve(self, model) subscribeNext:^(TravelContactModel *x) {
            self.nameLbl.text = x.fullname;
            self.ageLbl.text = x.age;
            self.phoneLbl.text=  x.phone;
            self.numLbl.text = x.card_num;
            self.sexImage.image = [x.sex integerValue] == Sex_Male ?
            [[UIImage imageNamed:@"ic_aboutme_male"] imageToColor:AppBlue] :
            [[UIImage imageNamed:@"ic_aboutme_female"] imageToColor:AppBlue];
        }];
    }
    return self;
}


-(void)commonInit{
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.top.equalTo(self.contentView.mas_top).offset(5);
    }];
    
    [self.sexImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_right).offset(10);
        make.centerY.equalTo(self.nameLbl);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    [self.ageLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.sexImage.mas_right).offset(10);
        make.centerY.equalTo(self.nameLbl);
    }];
    
    
    
    [self.phoneImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.top.equalTo(self.nameLbl.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    [self.phoneLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.phoneImage.mas_right).offset(10);
        make.centerY.equalTo(self.phoneImage);
    }];
    
    
    [self.numImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.top.equalTo(self.phoneImage.mas_bottom).offset(5);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    [self.numLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.numImage.mas_right).offset(10);
        make.centerY.equalTo(self.numImage);
    }];
}

#pragma mark -
#pragma mark lazy layout


-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        _nameLbl.font = UserNameFont;
        [self.contentView addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(UILabel *)ageLbl{
    if (!_ageLbl) {
        _ageLbl = [ControllerHelper autoFitLabel];
        _ageLbl.font = DetailFont;
        [self.contentView addSubview:_ageLbl];
    }
    return _ageLbl;
}

-(UILabel *)phoneLbl{
    if (!_phoneLbl) {
        _phoneLbl = [ControllerHelper autoFitLabel];
        _phoneLbl.font = DetailFont;
        [self.contentView addSubview:_phoneLbl];
    }
    return _phoneLbl;
}

-(UILabel *)numLbl{
    if (!_numLbl) {
        _numLbl = [ControllerHelper autoFitLabel];
        _numLbl.font = DetailFont;
        [self.contentView addSubview:_numLbl];
    }
    return _numLbl;
}


-(UIImageView *)sexImage{
    if (!_sexImage) {
        _sexImage = [[UIImageView alloc]init];
        _sexImage.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_sexImage];
    }
    return _sexImage;
}

-(UIImageView *)phoneImage{
    if (!_phoneImage) {
        _phoneImage = [[UIImageView alloc]init];
        _phoneImage.contentMode = UIViewContentModeScaleAspectFit;
        _phoneImage.image = [UIImage imageNamed:@"ic_user_reward"];
        [self.contentView addSubview:_phoneImage];
    }
    return _phoneImage;
}


-(UIImageView *)numImage{
    if (!_numImage) {
        _numImage = [[UIImageView alloc]init];
        _numImage.contentMode = UIViewContentModeScaleAspectFit;
        _numImage.image = [UIImage imageNamed:@"ic_user_reward"];
        [self.contentView addSubview:_numImage];
    }
    return _numImage;
}




@end
