//
//  StrangerConversationListVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/9.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseViewController.h"

@interface StrangerConversationListVC : BaseViewController

@property(nonatomic,strong) NSMutableArray *conversationModels;

@end
