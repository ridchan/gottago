//
//  DiscoverDynamicHeader.m
//  yaoqu
//
//  Created by ridchan on 2017/8/16.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DiscoverDynamicHeader.h"
#import "TravelBannerModel.h"
#import "CommonConfigManager.h"

@interface DiscoverDynamicHeader()<SDCycleScrollViewDelegate>

@property(nonatomic,strong) SDCycleScrollView *bannerView;

@end

@implementation DiscoverDynamicHeader

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self commonInit];
    }
    return self;
}



-(void)commonInit{
    self.bannerView.sd_layout
    .leftEqualToView(self)
    .rightEqualToView(self)
    .topEqualToView(self)
    .bottomEqualToView(self);
    

    //    self.titleView.sd_layout
    //    .rightSpaceToView(self, 10)
    //    .topSpaceToView(self.bannerView, 0);
    
    //    [self setupAutoHeightWithBottomView:self.titleLbl bottomMargin:0];
}


-(void)setDatas:(NSMutableArray *)datas{
    _datas = datas;
    
    NSString *baseUrl = [CommonConfigManager sharedManager].image_host;
    if (baseUrl == nil) return;
    NSMutableArray *imageGroup = [NSMutableArray array];
    
    [_datas sortUsingComparator:^NSComparisonResult(TravelBannerModel *obj1, TravelBannerModel *obj2) {
        if (obj1.sort > obj2.sort) {
            return NSOrderedDescending;
        }else{
            return NSOrderedAscending;
        }
    }];
    for (TravelBannerModel *model in _datas){
        [imageGroup addObject:[baseUrl stringByAppendingPathComponent:model.image]];
    }
    self.bannerView.imageURLStringsGroup =  imageGroup;
}

#pragma mark -
#pragma makr delegate


#pragma mark -
#pragma mark lazy layout

-(SDCycleScrollView *)bannerView{
    if (!_bannerView) {
        _bannerView = [[SDCycleScrollView alloc]init];
        _bannerView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
        _bannerView.delegate = self;
        [self addSubview:_bannerView];
    }
    return _bannerView;
}

@end
