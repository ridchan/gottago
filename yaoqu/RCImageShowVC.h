//
//  RCImageShowVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/26.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseViewController.h"
#import "LLAssetModel.h"
#import "LLAssetsGroupModel.h"

@interface RCImageShowVC : UIViewController

@property (nonatomic) LLAssetsGroupModel *assetGroupModel;
@property (nonatomic) NSArray<LLAssetModel *> *allAssets;
@property (nonatomic) NSMutableArray<LLAssetModel *> *allSelectdAssets;
@property (nonatomic) LLAssetModel *curShowAsset;


@end
