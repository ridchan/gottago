//
//  TravelPayTypeView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/17.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelPayTypeView.h"
#import "NormalTableViewCell.h"

@interface TravelPayTypeView()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,copy) BaseBlock completeBlock;

@property(nonatomic,strong) NSArray *datas;

@property(nonatomic,strong) UIView *bgView;

@property(nonatomic,strong) UIImageView *backImage;
@property(nonatomic,strong) UILabel *titleLbl;

@end

@implementation TravelPayTypeView


+(void)showInView:(UIView *)view compelteBlock:(BaseBlock)block{
    TravelPayTypeView *paytypeView = [[TravelPayTypeView alloc]init];
    paytypeView.frame = CGRectMake(0, view.height,view.width , 180);
    paytypeView.completeBlock = block;
    
    [view addSubview:paytypeView.bgView];
    [view addSubview:paytypeView];
    
    [UIView animateWithDuration:0.35 animations:^{
        paytypeView.transform = CGAffineTransformMakeTranslation(0, - 180);
    }];

}

-(void)dismiss{
    WEAK_SELF;
    [self.bgView removeFromSuperview];
    [UIView animateWithDuration:0.35 animations:^{
        weakSelf.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        [weakSelf removeFromSuperview];
    }];

}


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.datas = @[@{@"name":LS(@"支付宝"),@"image":@"ic_orderpay_zhifubao"},
                       @{@"name":LS(@"微信"),@"image":@"ic_orderpay_wechat"}
                       ];
        [self commonInit];
        self.backgroundColor =  [UIColor whiteColor];
    }
    return self;
}

-(void)commonInit{
    [self.backImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.mas_top);
        make.height.mas_equalTo(60);
        make.width.mas_equalTo(20);
    }];
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.height.mas_equalTo(60);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.titleLbl.mas_bottom);
        make.bottom.equalTo(self.mas_bottom);
    }];
    
}

#pragma mark -
#pragma mark lazy layout


-(UIView *)bgView{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, APP_WIDTH, APP_HEIGHT)];
        _bgView.backgroundColor = RGB16(0x000000);
        _bgView.alpha = 0.5;
        
        UITapGestureRecognizer * _tap = [[UITapGestureRecognizer alloc] init];
        [_tap addTarget:self action:@selector(dismiss)];
        [_bgView addGestureRecognizer:_tap];
        
    }
    return _bgView;
}


-(UIImageView *)backImage{
    if (!_backImage) {
        _backImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_back"]];
        _backImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_backImage];
    }
    return _backImage;
}

-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        _titleLbl.textAlignment = NSTextAlignmentCenter;
        _titleLbl.text = LS(@"选择付款方式");
        [self addSubview:_titleLbl];
    }
    
    return _titleLbl;
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.scrollEnabled = NO;
        [self addSubview:_tableView];
    }
    return _tableView;
}


#pragma mark -
#pragma mark tableview 


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.completeBlock) {
        self.completeBlock(@(indexPath.row));
    }
    [self dismiss];
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NormalTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[NormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.type = NormalCellType_TopLine;
        
    }
    NSDictionary *dic = self.datas[indexPath.row];
    cell.contentImage.image = [UIImage imageNamed:dic[@"image"]];
    cell.textLbl.text = dic[@"name"];
    return cell;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
