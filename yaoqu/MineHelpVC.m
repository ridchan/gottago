//
//  MineHelpVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/18.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineHelpVC.h"
#import "MineHelpCell.h"

@interface MineHelpVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic) NSInteger expandIndex;

@end

@implementation MineHelpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"帮助";
    
    NSArray *array= @[@{@"title":@"1.我的支付是否安全？",@"desc":@"成双成对支持支付宝与微信支付，全方位加密，竭力保障您 的支付与信息安全"},
                      @{@"title":@"2.如何使用我的积分？",@"desc":@"成双成对支持支付宝与微信\n支付，全方位加密，竭力保障您 的支付与信息安全"},
                      @{@"title":@"3.如何使用我的旅游币？",@"desc":@"成双成对支持支\n付宝与微信支付，全方位加密，\n竭力保障您 的支付与信息安全"},
                      @{@"title":@"4.如何申请退款？",@"desc":@"成双成对支持支付宝与微信支付\n，全方位加密\n，竭力保障您\n的支付与信息安全"},
                      @{@"title":@"5.如果我需要更多帮助，该怎么办？",@"desc":@"成双成对\n支持支付宝与\n微信支付，全\n方位加密，竭\n力保障您 的支付与信息安全"},
                    ] ;
    
    self.expandIndex = -1;
    self.datas = [QuestionHelpModel objectArrayWithKeyValuesArray:array];
    
    [self.tableView registerClass:[MineHelpCell class] forCellReuseIdentifier:@"Cell"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 30)];
    view.backgroundColor = RGB16(0xedeeef);
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 200, 30)];
    label.font = SystemFont(12);
    label.text = @"常见问题";
    [view addSubview:label];
    
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [tableView cellHeightForIndexPath:indexPath model:self.datas[indexPath.row] keyPath:@"model" cellClass:[MineHelpCell class] contentViewWidth:APP_WIDTH];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MineHelpCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.model = self.datas[indexPath.row];
    WEAK_SELF;
    cell.actionBlock = ^(id responseObj) {
        NSMutableArray *array = [NSMutableArray arrayWithObject:indexPath];
        if (weakSelf.expandIndex > -1){
            QuestionHelpModel *model = weakSelf.datas[weakSelf.expandIndex];
            model.isExpand = NO;
            NSIndexPath *lastIndexPath = [NSIndexPath indexPathForRow:weakSelf.expandIndex inSection:0];
            [array addObject:lastIndexPath];
        }
        weakSelf.expandIndex = weakSelf.expandIndex == indexPath.row ? -1 : indexPath.row;
        [weakSelf.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationFade];
    };
    return cell;
}


@end
