//
//  AliPayObj.m
//  angelslike
//
//  Created by angelslike on 15/9/18.
//  Copyright (c) 2015年 angelslike. All rights reserved.
//

#import "AliPayObj.h"
#import "Order.h"
#import "APAuthV2Info.h"
#import "RSADataSigner.h"
#import <AlipaySDK/AlipaySDK.h>

#define AliPID @"2017012305376878"
#define AliSID @"weilichengbang@126.com"


@implementation AliPayObj

+(void)payWithInfo:(NSDictionary *)info successBlock:(AlipayBlock)sblock failBlock:(AlipayBlock)fblock{
    
    
    NSString *appID = @"2017012305376878";
    
    NSMutableString *rsa2PrivateKey = [NSMutableString string];
    
    
    NSString *rsaPrivateKey = @"MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCClR+tJeS5WFELDhwGRoWYZlBP6PxojIgJ5zSs2pEhg5wMHRuelj4MG0AiBy1EREkjTpCYCwWJJo3zTgGuokeyoK4hP938ygXxI8G6Ijk0dvZzHlGH/iBjhJc1PG+wBhEuDIdWIYIRar0STFpx5I1TiAqZIQCkumyBYkdlsqYpfVqBMfCLXoGpC/Z/ie4aETXGE/UuE2tk33OL+qfgJjKuZ0yWGaxmV9H0W5DBYt8GIYIhm9uUCqIH5I7invAu/1JG9lwRBF2hJgIB6qM8tf+M8hNI1OcG58fuhS1RLII7Xu3jyCvsOuOF0/C88HwY3HSups6R1eJQQWJ+M4QxlvMFAgMBAAECggEAd0uFBMn5itoEL/VT5OfIZNITzO0sqta38VQn60JCFDwH84kkEl1vkd/TV0L3N8aqinbb4gsvo3x1cjFxl/jpR2dDrVyLkMV/aC0ibsmVOTj1LXYlRfgD3ubivN/ZqRXh3A/WE/nIueCpmm3msuX5trYx+HMOA2/McOc4EtCCvKNaPcJZVzasnZmWOr+TbiHO9AvvPGnQFLCCVNyyGtXGjb64Ip6bicXgz0J9tEguO7/55MLLZ4gUHIPuBSrUwaUKGzLxxmEW7ZuUOGVp47JuOfK/SJbv7lZDWt3djU/n/CjYhvMokXTb1vuInnOFrA8yALZIU6AwOFKO0Axg2B6MAQKBgQC7kq6BUKP9N/35T5TZu+vbKRZr+dGPECStrINzlabSQtwYpJ6P0S54CRW/a7ZQt1AIX+rLwSaFhf94hdrwAWVFyeCEEiOv4V9XYmQwyewuKsluPyQedyjM00IF/Fn2Cim7JahjwApz/IfnFM9qd7t1qREnzREW75OvQbRrEFnoLQKBgQCyOCPU6wcfg8XvJOsH26khDZ6QsZ5wG87Jrll7v/8qW+xS3Cz+COjcfyqrv0coWzVm/cWnPsNKvBRiQu+shkqAyf5kIhkP7UJ8SnJ194ZJU4OpkrHF8HR46ZbsT9bVwKpedY6LRAH+vd/yjxtN+JuZkPlbvvxUPCJhrWXZn6rlOQKBgE2AqxgFgOzU4NzNlBxvOGIC1R/iRkNNkUoteNceY6KHO7f29QlJjmDMqC7Cy78IL0p2j2aHNWqAVnGvJSp7+SNf6aPIdvRqM+HAZtN961ecGES0VDE+XR5cv/AHOAigZGPOiIhGxkrsch76C0XbmRphtBP0+HW4ohYNGQYP6IY1AoGAS6k31RnekUuRSaDDwzdHS0gL7GPr3q5/kPHa/YWaX7Jc1ySwWT0RFyNJy1ZOcFyghFPyiLlFkcHdaYX/WKA+mwSmYn5zY2aX1kqI43otYY36FJC5tk4KNJb+wt52V5MPAUEpPgimTC7yF3kX33dPH9gDdSbU2zIYap2B4u09UUkCgYEAqARwWAtqzBDCNDcC83pIKw4DvTyoIqjp+fLPvvc4lvpr/qIRK31FnM3o9cTtzxao2tTHtQvO9whz0aAxHSPiGRQVD9FEjfAKjfCH8ojnUAacjQW97ezXNP83+o2rLikGTJ5+5+J2Yz5myBWHZMaJ9KM0fFyXVSBoJV1fu8S26iQ=";
    /*============================================================================*/
    /*============================================================================*/
    /*============================================================================*/
    
    //partner和seller获取失败,提示
    if ([appID length] == 0 ||
        ([rsa2PrivateKey length] == 0 && [rsaPrivateKey length] == 0))
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                        message:@"缺少appId或者私钥。"
                                                       delegate:self
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    /*
     *生成订单信息及签名
     */
    //将商品信息赋予AlixPayOrder的成员变量
    Order* order = [Order new];
    
    // NOTE: app_id设置
    order.app_id = appID;
    
    // NOTE: 支付接口名称
    order.method = @"alipay.trade.app.pay";
    
    // NOTE: 参数编码格式
    order.charset = @"utf-8";
    
    // NOTE: 当前时间点
    NSDateFormatter* formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    order.timestamp = [formatter stringFromDate:[NSDate date]];
    
    // NOTE: 支付版本
    order.version = @"1.0";
    
    // NOTE: sign_type 根据商户设置的私钥来决定
    order.sign_type = (rsa2PrivateKey.length > 1)?@"RSA2":@"RSA";
    
    // NOTE: 商品数据
    order.biz_content = [BizContent new];
    order.biz_content.body = [info objectForKey:@"body"];
    order.biz_content.subject = @"1";
    order.biz_content.out_trade_no = [info objectForKey:@"order_sn"]; //订单ID（由商家自行制定）
    order.biz_content.timeout_express = @"30m"; //超时时间设置
    order.biz_content.total_amount = [NSString stringWithFormat:@"%.2f", [[info objectForKey:@"paytotal"] floatValue]]; //商品价格
    
    //将商品信息拼接成字符串
    NSString *orderInfo = [order orderInfoEncoded:NO];
    NSString *orderInfoEncoded = [order orderInfoEncoded:YES];
    NSLog(@"orderSpec = %@",orderInfo);
    
    // NOTE: 获取私钥并将商户信息签名，外部商户的加签过程请务必放在服务端，防止公私钥数据泄露；
    //       需要遵循RSA签名规范，并将签名字符串base64编码和UrlEncode
    NSString *signedString = nil;
    RSADataSigner* signer = [[RSADataSigner alloc] initWithPrivateKey:((rsa2PrivateKey.length > 1)?rsa2PrivateKey:rsaPrivateKey)];
    if ((rsa2PrivateKey.length > 1)) {
        signedString = [signer signString:orderInfo withRSA2:YES];
    } else {
        signedString = [signer signString:orderInfo withRSA2:NO];
    }
    
    // NOTE: 如果加签成功，则继续执行支付
    if (signedString != nil) {
        //应用注册scheme,在AliSDKDemo-Info.plist定义URL types
        NSString *appScheme = @"qbh";
        
        // NOTE: 将签名成功字符串格式化为订单字符串,请严格按照该格式
        NSString *orderString = [NSString stringWithFormat:@"%@&sign=%@",
                                 orderInfoEncoded, signedString];
        
        // NOTE: 调用支付结果开始支付
//        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
//            NSLog(@"reslut = %@",resultDic);
//        }];
        
        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            if ([[resultDic objectForKey:@"resultStatus"] integerValue] == 9000) {
                if (sblock) sblock(resultDic);
            }else{
                if (fblock) fblock(resultDic);
                //
            }
        }];

    }


}

+ (void)doAlipayPay
{
    
    
    NSString *appID = @"2017012305376878";
    NSMutableString *rsa2PrivateKey = [NSMutableString string];
    
    
    NSString *rsaPrivateKey = @"MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCClR+tJeS5WFELDhwGRoWYZlBP6PxojIgJ5zSs2pEhg5wMHRuelj4MG0AiBy1EREkjTpCYCwWJJo3zTgGuokeyoK4hP938ygXxI8G6Ijk0dvZzHlGH/iBjhJc1PG+wBhEuDIdWIYIRar0STFpx5I1TiAqZIQCkumyBYkdlsqYpfVqBMfCLXoGpC/Z/ie4aETXGE/UuE2tk33OL+qfgJjKuZ0yWGaxmV9H0W5DBYt8GIYIhm9uUCqIH5I7invAu/1JG9lwRBF2hJgIB6qM8tf+M8hNI1OcG58fuhS1RLII7Xu3jyCvsOuOF0/C88HwY3HSups6R1eJQQWJ+M4QxlvMFAgMBAAECggEAd0uFBMn5itoEL/VT5OfIZNITzO0sqta38VQn60JCFDwH84kkEl1vkd/TV0L3N8aqinbb4gsvo3x1cjFxl/jpR2dDrVyLkMV/aC0ibsmVOTj1LXYlRfgD3ubivN/ZqRXh3A/WE/nIueCpmm3msuX5trYx+HMOA2/McOc4EtCCvKNaPcJZVzasnZmWOr+TbiHO9AvvPGnQFLCCVNyyGtXGjb64Ip6bicXgz0J9tEguO7/55MLLZ4gUHIPuBSrUwaUKGzLxxmEW7ZuUOGVp47JuOfK/SJbv7lZDWt3djU/n/CjYhvMokXTb1vuInnOFrA8yALZIU6AwOFKO0Axg2B6MAQKBgQC7kq6BUKP9N/35T5TZu+vbKRZr+dGPECStrINzlabSQtwYpJ6P0S54CRW/a7ZQt1AIX+rLwSaFhf94hdrwAWVFyeCEEiOv4V9XYmQwyewuKsluPyQedyjM00IF/Fn2Cim7JahjwApz/IfnFM9qd7t1qREnzREW75OvQbRrEFnoLQKBgQCyOCPU6wcfg8XvJOsH26khDZ6QsZ5wG87Jrll7v/8qW+xS3Cz+COjcfyqrv0coWzVm/cWnPsNKvBRiQu+shkqAyf5kIhkP7UJ8SnJ194ZJU4OpkrHF8HR46ZbsT9bVwKpedY6LRAH+vd/yjxtN+JuZkPlbvvxUPCJhrWXZn6rlOQKBgE2AqxgFgOzU4NzNlBxvOGIC1R/iRkNNkUoteNceY6KHO7f29QlJjmDMqC7Cy78IL0p2j2aHNWqAVnGvJSp7+SNf6aPIdvRqM+HAZtN961ecGES0VDE+XR5cv/AHOAigZGPOiIhGxkrsch76C0XbmRphtBP0+HW4ohYNGQYP6IY1AoGAS6k31RnekUuRSaDDwzdHS0gL7GPr3q5/kPHa/YWaX7Jc1ySwWT0RFyNJy1ZOcFyghFPyiLlFkcHdaYX/WKA+mwSmYn5zY2aX1kqI43otYY36FJC5tk4KNJb+wt52V5MPAUEpPgimTC7yF3kX33dPH9gDdSbU2zIYap2B4u09UUkCgYEAqARwWAtqzBDCNDcC83pIKw4DvTyoIqjp+fLPvvc4lvpr/qIRK31FnM3o9cTtzxao2tTHtQvO9whz0aAxHSPiGRQVD9FEjfAKjfCH8ojnUAacjQW97ezXNP83+o2rLikGTJ5+5+J2Yz5myBWHZMaJ9KM0fFyXVSBoJV1fu8S26iQ=";
    /*============================================================================*/
    /*============================================================================*/
    /*============================================================================*/
    
    //partner和seller获取失败,提示
    if ([appID length] == 0 ||
        ([rsa2PrivateKey length] == 0 && [rsaPrivateKey length] == 0))
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                        message:@"缺少appId或者私钥。"
                                                       delegate:self
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    /*
     *生成订单信息及签名
     */
    //将商品信息赋予AlixPayOrder的成员变量
    Order* order = [Order new];
    
    // NOTE: app_id设置
    order.app_id = appID;
    
    // NOTE: 支付接口名称
    order.method = @"alipay.trade.app.pay";
    
    // NOTE: 参数编码格式
    order.charset = @"utf-8";
    
    // NOTE: 当前时间点
    NSDateFormatter* formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    order.timestamp = [formatter stringFromDate:[NSDate date]];
    
    // NOTE: 支付版本
    order.version = @"1.0";
    
    // NOTE: sign_type 根据商户设置的私钥来决定
    order.sign_type = (rsa2PrivateKey.length > 1)?@"RSA2":@"RSA";
    
    // NOTE: 商品数据
    order.biz_content = [BizContent new];
    order.biz_content.body = @"我是测试数据";
    order.biz_content.subject = @"1";
    order.biz_content.out_trade_no = @"123456789000"; //订单ID（由商家自行制定）
    order.biz_content.timeout_express = @"30m"; //超时时间设置
    order.biz_content.total_amount = [NSString stringWithFormat:@"%.2f", 0.01]; //商品价格
    
    //将商品信息拼接成字符串
    NSString *orderInfo = [order orderInfoEncoded:NO];
    NSString *orderInfoEncoded = [order orderInfoEncoded:YES];
    NSLog(@"orderSpec = %@",orderInfo);
    
    // NOTE: 获取私钥并将商户信息签名，外部商户的加签过程请务必放在服务端，防止公私钥数据泄露；
    //       需要遵循RSA签名规范，并将签名字符串base64编码和UrlEncode
    NSString *signedString = nil;
    RSADataSigner* signer = [[RSADataSigner alloc] initWithPrivateKey:((rsa2PrivateKey.length > 1)?rsa2PrivateKey:rsaPrivateKey)];
    if ((rsa2PrivateKey.length > 1)) {
        signedString = [signer signString:orderInfo withRSA2:YES];
    } else {
        signedString = [signer signString:orderInfo withRSA2:NO];
    }
    
    // NOTE: 如果加签成功，则继续执行支付
    if (signedString != nil) {
        //应用注册scheme,在AliSDKDemo-Info.plist定义URL types
        NSString *appScheme = @"alisdkdemo";
        
        // NOTE: 将签名成功字符串格式化为订单字符串,请严格按照该格式
        NSString *orderString = [NSString stringWithFormat:@"%@&sign=%@",
                                 orderInfoEncoded, signedString];
        
        // NOTE: 调用支付结果开始支付
        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            NSLog(@"reslut = %@",resultDic);
        }];
    }
}







@end
