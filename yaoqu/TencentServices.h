//
//  TencentServices.h
//  shiyi
//
//  Created by 陳景雲 on 16/5/3.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "WXApi.h"
#import "TencentOpenAPI.framework/Headers/TencentOAuth.h"


@interface TencentServices : NSObject<UIApplicationDelegate,WXApiDelegate,TencentSessionDelegate,TencentLoginDelegate>

@property (strong, nonatomic) TencentOAuth *tencentOAuth;

-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url;
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;
-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

@end
