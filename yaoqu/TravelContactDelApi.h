//
//  TravelContactDelApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface TravelContactDelApi : BaseApi

-(id)initWithContactID:(NSString *)contact_id;

@end
