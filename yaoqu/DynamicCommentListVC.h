//
//  DynamicCommentListVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/30.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NormalTableViewController.h"
#import "DynamicCommentModel.h"

@interface DynamicCommentListVC : NormalTableViewController

@property(nonatomic,strong) NSString *dynamic_id;
@property(nonatomic,strong) DynamicCommentModel *model;

@end
