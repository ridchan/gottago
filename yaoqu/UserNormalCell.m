//
//  UserNormalCell.m
//  yaoqu
//
//  Created by ridchan on 2017/7/17.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserNormalCell.h"



@interface UserNormalCell()

@end

@implementation UserNormalCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


+(UserNormalCell *)normalCellType:(UserCellType)cellType identify:(NSString *)identify{
    UserNormalCell *cell = [[UserNormalCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    [cell commonInit];
    cell.cellType = cellType;
    
    return cell;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)setCellType:(UserCellType)cellType{
    _cellType = cellType;
    
    if (_cellType == UserCellType_Normal) {
        [self.nameLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.userImage.mas_right).offset(5);
            make.centerY.equalTo(self.userImage.mas_centerY);
        }];
        
    }else{
        [self.nameLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.userImage.mas_right).offset(5);
            make.bottom.equalTo(self.userImage.mas_centerY).offset(-3);
        }];
        
        [self.dateLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.nameLbl.mas_left);
            make.bottom.equalTo(self.userImage.mas_bottom);
        }];
        
        if (_cellType == UserCellType_Date_Desc){
            self.contentImage.hidden = YES;
            self.descLbl.hidden = NO;
            [self.descLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self.mas_right).offset(-10);
                make.top.equalTo(self.mas_top).offset(10);
                make.bottom.equalTo(self.mas_bottom).offset(-10);
                make.width.equalTo(self.descLbl.mas_height).multipliedBy(2.0);
            }];
        }
        
        if (_cellType == UserCellType_Date_Image) {
            self.contentImage.hidden = NO;
            self.descLbl.hidden = YES;
            [self.contentImage mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self.mas_right).offset(-10);
                make.top.equalTo(self.mas_top).offset(10);
                make.bottom.equalTo(self.mas_bottom).offset(-10);
                make.width.equalTo(self.contentImage.mas_height);
            }];
        }
        
    }

}



-(void)commonInit{
    [self.userImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.mas_top).offset(10);
        make.bottom.equalTo(self.mas_bottom).offset(-10);
        make.width.equalTo(self.userImage.mas_height);
    }];
    

    
    [self.sexView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_right).offset(5);
        make.height.mas_equalTo(15);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
    }];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];

}

-(UserImageView *)userImage{
    if (!_userImage) {
        _userImage = [[UserImageView alloc]init];
        [self.contentView addSubview:_userImage];
    }
    return _userImage;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        _nameLbl.font = SystemFont(14);
        [self.contentView addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(SexAgeView *)sexView{
    if (!_sexView) {
        _sexView = [[SexAgeView alloc]init];
        [self.contentView addSubview:_sexView];
    }
    return _sexView;
}

-(UILabel *)dateLbl{
    if (!_dateLbl) {
        _dateLbl = [ControllerHelper autoFitLabel];
        _dateLbl.font = SystemFont(12);
        _dateLbl.textColor = AppGray;
        [self.contentView addSubview:_dateLbl];
    }
    return _dateLbl;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.font = SystemFont(11);
        _descLbl.numberOfLines = 0;
        _descLbl.textAlignment = NSTextAlignmentRight;
        _descLbl.textColor = [UIColor lightGrayColor];
        [self.contentView addSubview:_descLbl];
    }
    return _descLbl;
}

-(UIImageView *)contentImage{
    if (!_contentImage) {
        _contentImage = [[UIImageView alloc]init];
        _contentImage.clipsToBounds = YES;
        _contentImage.contentMode = UIViewContentModeScaleAspectFill;
        _contentImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.contentView addSubview:_contentImage];
    }
    return _contentImage;
}

-(UIView *)line{
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = AppLineColor;
        [self.contentView addSubview:_line];
    }
    return _line;
}

@end
