//
//  Responese.h
//  QBH
//
//  Created by 陳景雲 on 2016/12/28.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
@interface Responese : NSObject

@property(strong, nonatomic) AFHTTPSessionManager* afom;
@property(assign, nonatomic) NSInteger secret;
@property(assign, nonatomic) long long time;
@property(strong, nonatomic) NSString *code;
@property(copy, nonatomic) NSString *msg;
@property(strong, nonatomic) NSDictionary *data;
@property(strong, nonatomic) NSError *error;
@property (nonatomic,copy)NSString *createTime;


@end
