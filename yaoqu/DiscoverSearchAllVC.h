//
//  DiscoverSearchAllVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/6.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NormalTableViewController.h"
@class DiscoverySearchResultVC;

@interface DiscoverSearchAllVC : NormalTableViewController

@property(nonatomic,strong) DiscoverySearchResultVC *resultVC;

@end
