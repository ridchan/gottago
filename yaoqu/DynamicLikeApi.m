//
//  DynamicLikeApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/29.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicLikeApi.h"


@interface DynamicLikeApi(){
    BOOL _is_like;
}
@end

@implementation DynamicLikeApi

-(id)initWithDynamic_id:(NSString *)dynamic_id is_like:(BOOL)is_like{
    if (self = [super init]) {
        [self.params setValue:dynamic_id forKey:@"dynamic_id"];
        _is_like = is_like;
    }
    return self;
}

-(NSString *)requestUrl{
    return _is_like ? DynamicLikeUrl : DynamicUnLikeUrl;
}


@end
