//
//  MineRelationVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/10.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineRelationVC.h"
#import "UserNormalCell.h"
#import "UserModel.h"
#import "UserFollowListApi.h"
#import "UserFollowApi.h"
#import "RelationApi.h"
#import "RCUserCacheManager.h"

@interface MineRelationVC ()


@property(nonatomic,strong) NSString *relationString;
@property(nonatomic,strong) NSString *member_id;

@end

@implementation MineRelationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.relationType = [[self.paramObj objectForKey:@"param"] integerValue];
    self.member_id = [self.paramObj objectForKey:@"member_id"];
    BOOL isSelf = self.member_id == nil || [self.member_id isEqualToString:[RCUserCacheManager sharedManager].currentUser.member_id];
    
    switch (self.relationType) {
        case RelationType_Fans:
            self.relationString = @"fans";
            self.title = isSelf ? LS(@"我的粉丝") : LS(@"他的粉丝");
            break;
        case RelationType_Follow:
            self.relationString = @"follow";
            self.title = isSelf ? LS(@"我的关注") : LS(@"他的关注");
            break;
        case RelationType_Friend:
            self.relationString = @"partner";
            self.title = isSelf ? LS(@"我的好友") : LS(@"他的好友");
        default:
            break;
    }
    
    [self startRefresh];
    // Do any additional setup after loading the view.
}

-(void)startRefresh{
    [super startRefresh];
    WEAK_SELF;
    self.page = 1;
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:@(self.page) forKey:@"page"];
    [dict setValue:@(self.size) forKey:@"size"];
    [dict setValue:self.relationString forKey:@"type"];
    [dict setValue:self.member_id forKey:@"member_id"];
    
    
    
    [[[RelationApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [weakSelf refreshComplete:info response:[UserModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]]];
    }];
    
    
    
}

-(void)startloadMore{
    WEAK_SELF;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:@(++self.page) forKey:@"page"];
    [dict setValue:@(self.size) forKey:@"size"];
    [dict setValue:self.relationString forKey:@"type"];
    [dict setValue:self.member_id forKey:@"member_id"];
    
    [[[RelationApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        NSArray *array = [UserModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        [weakSelf loadMoreComplete:info response:array];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -



-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (self.relationType) {
        case RelationType_Fans:
            return @"关注";
        case RelationType_Friend:
        case RelationType_Follow:
            return @"取消关注";
        default:
            break;
    }
    return @"取消关注";

}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        UserModel *model = [self.datas objectAtIndex:indexPath.row];
        
        if (self.relationType == RelationType_Fans){
            NSDictionary *dict =  @{@"member_id":model.member_id,
                                    @"type":self.relationString,
                                    @"state":@"1"
                                    };
            
            RelationApi *api = [[RelationApi alloc]initWitObject:dict];
            api.method = YTKRequestMethodPOST;
            [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                [LLUtils showActionSuccessHUD:info.message];
                if (info.error == ErrorCodeType_None){
                    [[RCUserCacheManager sharedManager] cacheUser:model.member_id].is_follow = YES;
                }
            }];
        }else{
            NSDictionary *dict =  @{@"member_id":model.member_id,
                                    @"type":self.relationString,
                                    @"state":@"0"
                                    };
            RelationApi *api = [[RelationApi alloc]initWitObject:dict];
            api.method = YTKRequestMethodPOST;
            [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                [[RCUserCacheManager sharedManager] cacheUser:model.member_id].is_follow = NO;
                [LLUtils showActionSuccessHUD:info.message];
            }];
            
            
            [self.datas removeObjectAtIndex:indexPath.row];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
        }
        
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserNormalCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ImageCell"];
    if (cell == nil) {
        cell = [UserNormalCell normalCellType:UserCellType_Normal identify:@"ImageCell"];
    }
    
    UserModel *model = [self.datas objectAtIndex:indexPath.row];
    
    cell.nameLbl.text = model.username;
    
    cell.sexView.sex = [model.sex integerValue];
    cell.sexView.age = [model.age integerValue];
    cell.sexView.content = nil;
    cell.userImage.userModel = model;
    
    
    return cell;
}

@end
