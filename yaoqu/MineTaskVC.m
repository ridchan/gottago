//
//  MineTaskVC.m
//  yaoqu
//
//  Created by ridchan on 2017/8/12.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineTaskVC.h"
#import "MineTaskCell.h"
#import "TaskApi.h"

@interface MineTaskVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UIView *headerView;

@property(nonatomic,strong) NSArray *initialDatas;
@property(nonatomic,strong) NSArray *dailyDatas;
@property(nonatomic,strong) NSArray *weekDatas;
@property(nonatomic,strong) NSArray *monthDatas;

@end

@implementation MineTaskVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"任务中心");
    
    
    
    self.tableView.tableHeaderView = self.headerView;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    WEAK_SELF;
    
    [[[TaskApi alloc]init] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        NSDictionary *dict = [responseObj objectForKey:@"data"];
        weakSelf.initialDatas = [TaskModel objectArrayWithKeyValuesArray:[dict objectForKey:@"init"]];
        weakSelf.dailyDatas = [TaskModel objectArrayWithKeyValuesArray:[dict objectForKey:@"daily"]];
        weakSelf.weekDatas = [TaskModel objectArrayWithKeyValuesArray:[dict objectForKey:@"week"]];
        weakSelf.monthDatas = [TaskModel objectArrayWithKeyValuesArray:[dict objectForKey:@"month"]];
        weakSelf.datas = [@[weakSelf.initialDatas,
                           weakSelf.dailyDatas,
                           weakSelf.weekDatas,
                           weakSelf.monthDatas
                           ] mutableCopy];
        [weakSelf.tableView reloadData];
    }];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *)headerView{
    if (!_headerView) {
        _headerView = [[UIView alloc]init];
        _headerView.backgroundColor = [UIColor orangeColor];
        
        UIImage *image = [UIImage imageNamed:@"bg_yellow_buble"];
        UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
        [_headerView addSubview:imageView];
        
        CGFloat height = APP_WIDTH  * image.size.height / image.size.width;
        _headerView.frame = CGRectMake(0, 0, APP_WIDTH, height);
        imageView.frame = CGRectMake(0, 0, APP_WIDTH, height);
        
        
    }
    return _headerView;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 40)];
    UILabel *label = [[UILabel alloc]init];
    label.textColor = FontGray;
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentLeft;
    label.numberOfLines = 2;
    label.text = @[@"新手任务",@"日常任务",@"每周任务",@"每月任务"][section];
    label.frame = CGRectMake(10, 0, APP_WIDTH - 20, 40);
    
    [view addSubview:label];
    
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.datas[section] count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MineTaskCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[MineTaskCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.model = self.datas[indexPath.section][indexPath.row];
    return cell;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
