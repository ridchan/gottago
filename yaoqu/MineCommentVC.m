//
//  MineCommentVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineCommentVC.h"
#import "UserMessageCommentApi.h"
#import "UserCommentCell.h"
#import "MessageCommentModel.h"
#import "MessageApi.h"
#import "DynamicModel.h"
@interface MineCommentVC ()



@end

@implementation MineCommentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = LS(@"评论与回复");
    
    [self.tableView registerClass:[UserCommentCell class] forCellReuseIdentifier:@"Cell"];
    
    [self startRefresh];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)startRefresh{
    WEAK_SELF;
    self.page = 1;
    
    NSDictionary *dict = @{@"page":@(self.page),
                           @"size":@(self.size),
                           @"type":@(MessageCenterType_Comment)
                           };
    
    [[[MessageApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [weakSelf refreshComplete:info response:[MessageCommentModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]]];
    }];

}

-(void)startloadMore{
    
    WEAK_SELF;
    
    NSDictionary *dict = @{@"page":@(++self.page),
                           @"size":@(self.size),
                           @"type":@(MessageCenterType_Comment)
                           };

    [[[MessageApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        
        [weakSelf loadMoreComplete:info response:[MessageCommentModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]]];
        
    }];

}




#pragma mark -

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    MessageCommentModel *model = self.datas[indexPath.row];
//    DynamicModel *newModel = [[DynamicModel alloc]init];
//    newModel.dynamic_id = model.key_id;
//    [[RouteController sharedManager]openClassVC:@"DynamicDetailVC" withObj:newModel];
}



-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        MessageCommentModel *model = self.datas[indexPath.row];
        MessageApi *delApi = [[MessageApi alloc]initWitObject:@{@"type":@"2",@"id":model.message_comment_id}];
        delApi.method = YTKRequestMethodDELETE;
        
        [delApi startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            
        }];
        [self.datas removeObjectAtIndex:indexPath.row];
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        [self.tableView endUpdates];
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    MessageCommentModel *model = [self.datas objectAtIndex:indexPath.row];
    return [tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[UserCommentCell class] contentViewWidth:APP_WIDTH];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserCommentCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    MessageCommentModel *model = [self.datas objectAtIndex:indexPath.row];
    cell.model = model;
    
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
