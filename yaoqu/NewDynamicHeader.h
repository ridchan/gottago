//
//  NewDynamicHeader.h
//  QBH
//
//  Created by 陳景雲 on 2017/3/2.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseViewController.h"
#import "RCTextView.h"


@interface NewDynamicHeader : UIView

@property(nonatomic,strong) RCTextView *textView;
@property(nonatomic,strong) NSMutableArray *images;
@property(nonatomic,strong) UIViewController *vc;
@property(nonatomic,strong) NSString *height;

-(void)computeHeight;

@end
