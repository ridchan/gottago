//
//  UserEditApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"
#import "UserModel.h"

@interface UserEditApi : BaseApi

-(id)initWithUserModel:(UserModel *)userModel;

@end
