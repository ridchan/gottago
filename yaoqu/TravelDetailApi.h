//
//  TravelDetailApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface TravelDetailApi : BaseApi

-(id)initWithTravel:(NSString *)travel_schedule_id;

@end
