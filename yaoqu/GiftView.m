//
//  GiftView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/30.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "GiftView.h"
#import "GiftCell.h"
#import "NormalCellLayout.h"
#import "GiftViewCell.h"
#import "CommonConfigManager.h"
#import "RCUserCacheManager.h"
#import "GiftPostModel.h"
#import "GiftApi.h"
#import "LLUtils.h"

@implementation GiftIconButton


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    [self.iconImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(3);
        make.top.equalTo(self.mas_top).offset(3);
        make.bottom.equalTo(self.mas_bottom).offset(-3);
        make.width.equalTo(self.mas_height);
    }];
    
    
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.iconImage.mas_right).offset(3);
//        make.right.equalTo(self.accessImage.mas_left).offset(-3);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    [self.accessImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.label.mas_right).offset(8);
        make.right.equalTo(self.mas_right).offset(-3);
        make.top.equalTo(self.mas_top).offset(3);
        make.bottom.equalTo(self.mas_bottom).offset(-3);
        make.width.mas_equalTo(12);
    }];

}


-(UIImageView *)iconImage{
    if (!_iconImage) {
        _iconImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_gift_roll"]];
        _iconImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_iconImage];
    }
    return _iconImage;
}

-(UIImageView *)accessImage{
    if (!_accessImage) {
        _accessImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_forward"]];
        _accessImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_accessImage];
    }
    return _accessImage;

}

-(UILabel *)label{
    if (!_label) {
        _label =  [ControllerHelper autoFitLabel];
        [self addSubview:_label];
    }
    return _label;
}

@end


#pragma mark -

@interface GiftView()<UICollectionViewDelegate,UICollectionViewDataSource,GiftViewDelegate>

@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) UIView *topView;
@property(nonatomic,strong) UIView *bottomView;
@property(nonatomic,strong) NSArray *datas;
@property(nonatomic,strong) NSArray *storeDatas;
@property(nonatomic,strong) NSArray *myDatas;

//top view
@property(nonatomic,strong) GiftIconButton *leftBtn;
@property(nonatomic,strong) UIButton *storeGiftBtn;
@property(nonatomic,strong) UIButton *myGiftBtn;

//bottom view
@property(nonatomic,strong) UIButton *sendBtn;
@property(nonatomic,strong) UIPageControl *pageController;




//bgView

@property(nonatomic,strong) UIView *bgView;

@property(nonatomic,weak) id<GiftViewDelegate>delegate;

@property(nonatomic,strong) GiftModel *selectGift;
@property(nonatomic,strong) GiftPostModel *postModel;

@property(nonatomic,strong) MBProgressHUD *hud;

@end

@implementation GiftView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+(void)showWithDelegate:(id)delegate postModel:(GiftPostModel *)postModel{
    GiftView *view = [[GiftView alloc]init];
    view.delegate = delegate;
    view.postModel = postModel;
    view.frame = CGRectMake(0, APP_HEIGHT, APP_WIDTH, 300);
    UIWindow *windown = [UIApplication sharedApplication].keyWindow;
    
    [windown addSubview:view.bgView];
    [windown addSubview:view];
    
    [UIView animateWithDuration:0.35 animations:^{
        view.transform = CGAffineTransformMakeTranslation(0, -300);
    }];
    
    [[CommonConfigManager sharedManager] getCommonConfig];
}

+(void)showWithDelegate:(id)delegate{
    GiftView *view = [[GiftView alloc]init];
    view.delegate = delegate;
//    CGFloat height = (APP_WIDTH / 4.0 - 50) * 2 + 60;
    view.frame = CGRectMake(0, APP_HEIGHT, APP_WIDTH, 300);
    UIWindow *windown = [UIApplication sharedApplication].keyWindow;
    
    [windown addSubview:view.bgView];
    [windown addSubview:view];
    
    [UIView animateWithDuration:0.35 animations:^{
        view.transform = CGAffineTransformMakeTranslation(0, -300);
    }];
    
    [[CommonConfigManager sharedManager] getCommonConfig];
}

+(void)show{
    GiftView *view = [[GiftView alloc]init];
    view.frame = CGRectMake(0, APP_HEIGHT, APP_WIDTH, 340);
    UIWindow *windown = [UIApplication sharedApplication].keyWindow;
    
    [windown addSubview:view.bgView];
    [windown addSubview:view];
    
    [UIView animateWithDuration:0.35 animations:^{
        view.transform = CGAffineTransformMakeTranslation(0, -340.0);
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
    }];
}


-(void)dismiss{
    
    WEAK_SELF;
    [self.bgView removeFromSuperview];
    [UIView animateWithDuration:0.35 animations:^{
        weakSelf.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        [weakSelf removeFromSuperview];
    }];
}

-(void)giftView:(id)view didSelectObj:(GiftModel *)obj{
    if (self.selectGift){
        self.selectGift.isGiftSelected = @"0";
    }
    self.selectGift = obj;
    self.selectGift.isGiftSelected = @"1";
    [self.collectionView reloadData];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self commonInit];
        self.myGiftBtn.selected = YES;
        [RACObserve([CommonConfigManager sharedManager], hostModel) subscribeNext:^(HostModel *x) {
            self.myDatas = [self explandGiftDatas:x.gift_depot];
            self.storeDatas = [self explandGiftDatas:x.gift];
            
            self.datas = self.myDatas;
            [self reload];
        }];
        
        
    }
    return self;
}

-(NSMutableArray *)explandGiftDatas:(NSArray *)gifts{
    NSInteger column = ceilf(gifts.count / 8.0);
    
    NSMutableArray *totalGifts = [NSMutableArray array];
    for (int i = 0 ; i < column ; i ++){
        CGFloat offsetJ = i * 8;
        CGFloat toJ = MIN(offsetJ + 8, gifts.count);
        NSMutableArray *subGifts = [NSMutableArray array];
        for (int j  = offsetJ ; j < toJ ; j ++){
            [subGifts addObject:gifts[j]];
        }
        [totalGifts addObject:subGifts];
    }
    return totalGifts;
}


-(void)reload{
    self.pageController.numberOfPages = self.datas.count;
    self.pageController.currentPage = 1;
    [self.collectionView reloadData];
}

-(UIView *)bgView{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, APP_WIDTH, APP_HEIGHT)];
        _bgView.backgroundColor = RGB16(0x000000);
        _bgView.alpha = 0.5;
        
        UITapGestureRecognizer * _tap = [[UITapGestureRecognizer alloc] init];
        [_tap addTarget:self action:@selector(dismiss)];
        [_bgView addGestureRecognizer:_tap];

    }
    return _bgView;
}

-(void)commonInit{
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.top.equalTo(self.mas_top).offset(10);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(30);
    }];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.top.equalTo(self.topView.mas_bottom);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.bottom.equalTo(self.mas_bottom).offset(-10);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(30);
    }];
    
    
}

#pragma mark -
#pragma mark top view
-(UIView *)topView{
    if (!_topView) {
        _topView = [[UIView alloc] init];
        [self addSubview:_topView];
        
        [self.leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_topView);
            make.left.equalTo(_topView.mas_left).offset(5);
            make.height.mas_equalTo(20);
//            make.width.mas_equalTo(80);
        }];
        
        [self.storeGiftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_topView.mas_top);
            make.bottom.equalTo(_topView.mas_bottom);
            make.right.equalTo(_topView.mas_right);
            make.width.mas_equalTo(80);
        }];
        
        [self.myGiftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_topView.mas_top);
            make.bottom.equalTo(_topView.mas_bottom);
            make.right.equalTo(self.storeGiftBtn.mas_left);
            make.width.mas_equalTo(80);
        }];
    }
    return _topView;
}

-(GiftIconButton *)leftBtn{
    if (!_leftBtn) {
        _leftBtn = [[GiftIconButton alloc]init];
        _leftBtn.label.text = @"123";
        _leftBtn.label.font = SystemFont(12);
        RAC(_leftBtn.label,text) = RACObserve([RCUserCacheManager sharedManager],currentUser.giftroll);
        [_leftBtn addTarget:self action:@selector(openGiftRollVC:) forControlEvents:UIControlEventTouchUpInside];
        [_topView addSubview:_leftBtn];
    }
    return _leftBtn;
}

-(UIButton *)storeGiftBtn{
    if (!_storeGiftBtn) {
        _storeGiftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_storeGiftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_storeGiftBtn setTitleColor:AppOrange forState:UIControlStateSelected];
        [_storeGiftBtn setTitle:LS(@"商城礼物") forState:UIControlStateNormal];
        _storeGiftBtn.titleLabel.font = SystemFont(12);
        [_storeGiftBtn addTarget:self action:@selector(giftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_topView addSubview:_storeGiftBtn];
    }
    return _storeGiftBtn;
}

-(UIButton *)myGiftBtn{
    if (!_myGiftBtn) {
        _myGiftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_myGiftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_myGiftBtn setTitleColor:AppOrange forState:UIControlStateSelected];
        [_myGiftBtn setTitle:LS(@"我的礼物") forState:UIControlStateNormal];
        _myGiftBtn.titleLabel.font = SystemFont(12);
        [_myGiftBtn addTarget:self action:@selector(giftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_topView addSubview:_myGiftBtn];
    }
    return _myGiftBtn;
}


-(void)giftBtnClick:(UIButton *)sender{
    if (sender == self.storeGiftBtn) {
        self.storeGiftBtn.selected = YES;
        self.myGiftBtn.selected = NO;
        self.datas = self.storeDatas;
        [self reload];
    }else{
        self.myGiftBtn.selected = YES;
        self.storeGiftBtn.selected = NO;
        self.datas = self.myDatas;
        [self reload];
    }
}

-(void)openGiftRollVC:(id)sender{
    [self dismiss];
    [[RouteController sharedManager] openClassVC:@"MineGiftRollVC"];
}

#pragma mark -
#pragma mark bottom view



-(UIView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        [self.pageController mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_bottomView);
        }];
        
        [self.sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_bottomView.mas_right).offset(-10);
            make.top.equalTo(_bottomView.mas_top);
            make.bottom.equalTo(_bottomView.mas_bottom);
            make.width.mas_equalTo(80);
        }];
        self.sendBtn.layer.masksToBounds = YES;
        self.sendBtn.layer.cornerRadius = 3;
        [self addSubview:_bottomView];
    }
    return _bottomView;
}

-(UIPageControl *)pageController{
    if (!_pageController) {
        _pageController = [[UIPageControl alloc]init];
        _pageController.numberOfPages = 3;
        _pageController.currentPage = 1;
        _pageController.pageIndicatorTintColor = [UIColor blackColor];
        _pageController.currentPageIndicatorTintColor = RGB16(0x1493db);
        [_bottomView addSubview:_pageController];
    }
    return _pageController;
}

-(UIButton *)sendBtn{
    if (!_sendBtn) {
        _sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _sendBtn.backgroundColor = AppOrange;// RGB16(0x1493db);
        [_sendBtn setTitle:LS(@"赠送") forState:UIControlStateNormal];
        [_sendBtn addTarget:self action:@selector(sendBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_bottomView addSubview:_sendBtn];
    }
    return _sendBtn;
}

-(void)sendBtnClick:(id)sender{
    
    self.postModel.gift_id = self.selectGift.gift_id;
    self.postModel.qty = 1;
    

    self.hud = [LLUtils showActivityIndicatiorHUDWithTitle:nil];
    
    [self sendGift];
    

}

-(void)sendGift{
    
    WEAK_SELF;
    GiftApi *api = [[GiftApi alloc]initWitObject:self.postModel];
    
    api.method = YTKRequestMethodPUT;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_StockNotEnough){
            [weakSelf buyGift];
        }else{
            
            UIImage *image = [UIImage imageNamed:@"operationbox_successful"];
            image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            weakSelf.hud.mode = MBProgressHUDModeCustomView;
            weakSelf.hud.customView = [[UIImageView alloc] initWithImage:image];
            weakSelf.hud.label.text = info.message;
            
            [weakSelf.hud hideAnimated:YES afterDelay:1.0];
            
            [[RCUserCacheManager sharedManager] renewAccount];
            
            if ([weakSelf.delegate respondsToSelector:@selector(didSendGiftSuccess:)]) {
                [weakSelf.delegate didSendGiftSuccess:weakSelf.selectGift];
            }
            [weakSelf dismiss];
        }
    }];

}

-(void)buyGift{
    WEAK_SELF;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:self.selectGift.gift_id forKey:@"gift_id"];
    [dict setValue:@"1" forKey:@"qty"];
    GiftApi *api = [[GiftApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            [weakSelf sendGift];
        }else if (info.error == ErrorCodeType_GiftRollNotEnough){
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = @"礼物券不足";
            [weakSelf.hud hideAnimated:YES afterDelay:1.0];
            [[RouteController sharedManager] openClassVC:@"MineTicketVC"];
            [weakSelf dismiss];
        }else{
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = info.message;
            [weakSelf.hud hideAnimated:YES afterDelay:1.0];
        }
    }];

}

#pragma mark -
#pragma mark collection view


-(UICollectionView *)collectionView{
    if (!_collectionView) {
        NormalCellLayout *layout = [[NormalCellLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.pagingEnabled = YES;
        [_collectionView registerClass:[GiftViewCell class] forCellWithReuseIdentifier:@"Cell"];
        _collectionView.showsHorizontalScrollIndicator = NO;
        [self addSubview:_collectionView];
    }
    return _collectionView;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GiftViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.datas = [self.datas objectAtIndex:indexPath.item];
    cell.delegate = self;
    return cell;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return  self.datas.count;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    self.pageController.currentPage = roundf(scrollView.contentOffset.x / scrollView.frame.size.width);
}

@end


#pragma mark -
#pragma mark Layout
