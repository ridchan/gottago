//
//  RegistApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/15.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseApi.h"

@interface RegistApi : BaseApi

-(id)initWithPhone:(NSString *)phone
              code:(NSString *)code
              name:(NSString *)name
               sex:(NSString *)sex
            openid:(NSString *)openid
           unionid:(NSString *)unionid
             image:(NSString *)image;


@end
