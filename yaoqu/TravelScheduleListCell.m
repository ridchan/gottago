//
//  TravelScheduleListCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelScheduleListCell.h"
#import "TravelScheduleModel.h"

@interface TravelScheduleListCell()

@property(nonatomic,strong) UIImageView *contentImage;
@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) UIView *blackView;
@property(nonatomic,strong) UIView *discountView;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) ImageButton *addressBtn;
@property(nonatomic,strong) UILabel *priceLbl;
@property(nonatomic,strong) UILabel *discountLbl;
@property(nonatomic,strong) UIImageView *accessImage;
@property(nonatomic,strong) UIImageView *collectImage;

@end

@implementation TravelScheduleListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
//        self.contentView.backgroundColor = RGB16(0x5d5b5b);
        [self commonInit];
        [RACObserve(self, model) subscribeNext:^(TravelScheduleModel *model) {
            self.titleLbl.text = model.title;
            self.nameLbl.text = model.desc;
            self.discountLbl.text = model.integral;
            self.addressBtn.titleLbl.text  = model.address;
            self.priceLbl.text = [NSString stringWithFormat:@"%@%@",MoneySign,model.price];
            [self.contentImage setImageName:[model.image fixImageString:TravelImageFix] placeholder:nil];
            self.collectImage.image = model.is_collect ? [UIImage imageNamed:@"ic_heart_select"] : [UIImage imageNamed:@"ic_heart"];
        }];
        
        [RACObserve(self, model.is_collect) subscribeNext:^(id x) {
            self.collectImage.image = [x boolValue] ? [UIImage imageNamed:@"ic_heart_select"] : [UIImage imageNamed:@"ic_heart"];
        }];
    }
    return self;
}


-(void)commonInit{
    self.contentImage.sd_layout
    .leftEqualToView(self.contentView)
    .rightEqualToView(self.contentView)
    .topEqualToView(self.contentView)
    .heightIs(200);
//    
//    self.blackView.sd_layout
//    .leftSpaceToView(self.contentView, 0)
//    .bottomSpaceToView(self.contentView, 0)
//    .heightIs(40)
//    .rightEqualToView(self.contentView);

    
    self.titleLbl.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .topSpaceToView(self.contentImage, 10)
    .autoHeightRatio(0)
    .rightEqualToView(self.contentView);
    
    self.accessImage.sd_layout
    .rightSpaceToView(self.contentView, 10)
    .centerYEqualToView(self.titleLbl)
    .heightRatioToView(self.titleLbl, 1.0)
    .widthIs(15);
    
    self.priceLbl.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .topSpaceToView(self.titleLbl, 10)
    .heightIs(20)
    .rightEqualToView(self.contentView);
    
    self.nameLbl.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .topSpaceToView(self.priceLbl, 10)
    .heightIs(20)
    .rightEqualToView(self.contentView);
    
    
    
    self.addressBtn.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .topSpaceToView(self.nameLbl, 0)
    .rightSpaceToView(self.contentView, 10)
    .heightIs(20);
    
    self.discountView.sd_layout
    .rightSpaceToView(self.contentView, 10)
    .bottomSpaceToView(self.nameLbl,100)
    .heightIs(88)
    .widthIs(56);
    
    
    [self setupAutoHeightWithBottomView:self.addressBtn bottomMargin:10];
}


-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        _titleLbl.textColor = [UIColor blackColor];
        [self.contentView addSubview:_titleLbl];
    }
    return _titleLbl;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        _nameLbl.textColor = AppGray;
        _nameLbl.font = SystemFont(14);
        [self.contentView addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(UILabel *)priceLbl{
    if (!_priceLbl) {
        _priceLbl =[ControllerHelper autoFitLabel];
        _priceLbl.textColor = [UIColor redColor];
        [self.contentView addSubview:_priceLbl];
    }
    return _priceLbl;
}

-(ImageButton *)addressBtn{
    if (!_addressBtn) {
        _addressBtn = [[ImageButton alloc]init];
        _addressBtn.contentImage.image = [UIImage imageNamed:@"ic_discover_location"];
        [_addressBtn setAutoFit];
        [self.contentView addSubview:_addressBtn];
    }
    return _addressBtn;
}

-(UIImageView *)contentImage{
    if (!_contentImage) {
        _contentImage = [[UIImageView alloc]init];
        _contentImage.clipsToBounds = YES;
        _contentImage.contentMode = UIViewContentModeScaleAspectFill;
        _contentImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.contentView addSubview:_contentImage];
    }
    return _contentImage;
}

-(UIImageView *)accessImage{
    if (!_accessImage) {
        _accessImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_forward"]];
        _accessImage.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_accessImage];
    }
    return _accessImage;
}

-(UIView *)discountView{
    if (!_discountView) {
        _discountView = [[UIView alloc]init];
        _discountView.layer.cornerRadius = 3;
        _discountView.layer.masksToBounds = YES;
        _discountView.backgroundColor = RGBA(0, 0, 0, 0.3);
        
        
        UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_heart"]];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
//        imageView.backgroundColor = [UIColor blueColor];
        [_discountView addSubview:imageView];
        self.collectImage = imageView;
        
        UIView *whiteView = [[UIView alloc]init];
        whiteView.layer.cornerRadius = 3;
        whiteView.layer.masksToBounds = YES;
        whiteView.backgroundColor = [UIColor whiteColor];
        [_discountView addSubview:whiteView];
        
        UILabel *titleLbl = [ControllerHelper autoFitLabel];
        titleLbl.font = SystemFont(11);
        titleLbl.text = @"抵扣积分";
        [_discountView addSubview:titleLbl];
        
        self.discountLbl = [ControllerHelper autoFitLabel];
        self.discountLbl.textColor = [UIColor orangeColor];
        self.discountLbl.font = SystemFont(14);
        [_discountView addSubview:_discountLbl];
        
        
        [whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_discountView.mas_left).offset(5);
            make.right.equalTo(_discountView.mas_right).offset(-5);
            make.bottom.equalTo(_discountView.mas_bottom).offset(-5);
            make.height.equalTo(whiteView.mas_width);
//            make.height.mas_equalTo(50);
        }];
        
        [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(whiteView.mas_centerX);
            make.bottom.equalTo(whiteView.mas_centerY).offset(-2);
        }];
        
        [self.discountLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(whiteView.mas_centerX);
            make.top.equalTo(whiteView.mas_centerY).offset(2);
        }];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(whiteView.mas_centerX);
            make.top.equalTo(_discountView.mas_top).offset(10);
            make.bottom.equalTo(whiteView.mas_top).offset(-5);
            make.width.equalTo(whiteView.mas_width);
//            make.height.mas_equalTo(30);
        }];
        
        
        [self.contentView addSubview:_discountView];
    }
    
    return _discountView;
}

-(UIView *)blackView{
    if (!_blackView) {
        _blackView = [[UIView alloc]init];
        _blackView.backgroundColor = RGBA(0, 0, 0, 0.3);
        [self.contentView addSubview:_blackView];
    }
    return _blackView;
}

@end
