//
//  LoginViewModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/15.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseViewModel.h"
#import "UserModel.h"
#import "LoginPostModel.h"

@interface LoginViewModel : BaseViewModel

@property(nonatomic,strong) RACSignal *btnSignal;
@property(nonatomic,strong) RACCommand *btnCommand;

@property(nonatomic,weak) UIViewController *presentVC;
-(void)loginBtnClick:(UIButton *)button;

@end
