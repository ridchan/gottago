//
//  ApiConstants.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/12.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#ifndef ApiConstants_h
#define ApiConstants_h

#define MainDomain @"https://api.gottago917.com/v1/" //@"http://Interesting.1990design.com/"


//微信、QQ 登陆认证
#define AuthUrl @"member/oauth"
//用户注册
#define RegistUrl @"member/register"
//注册验证码
#define RegistCodeUrl @"member/reg/code/get"

//七牛token

#define QiuniuCodeUrl @"common/uptoken"

//APP 通用配置
#define CommonConfigUrl @"common/config"

//用户资料编辑

#define MemberEditUrl @"member/home/edit"

//用户配置

#define MemberConfigSetUrl @"member/config/message/put"

//动态添加

#define DynamicAddUrl @"dynamic/put"

//动态主页

#define DynamicHomeUrl @"dynamic/index"

//动态详情

#define DynamicDetailUrl @"dynamic/detail"

//动态点赞

#define DynamicLikeUrl @"dynamic/like/add"
#define DynamicUnLikeUrl @"dynamic/like/cancel"

//动态评论点赞

#define DynamicCommentLikeUrl @"dynamic/comment/like/add"
#define DynamicCommentUnLikeUrl @"dynamic/comment/like/cancel"

//动态评论

#define DynamicCommentUrl @"dynamic/comment/put"

//动态举报

#define DynamicReportUrl @"dynamic/report"

//动态删除评论

#define DynamicCommentDelUrl @"dynamic/comment/del"

//用户信息获取

#define UserInfoGetUrl @"member/info/get"

//聊天用户信息获取

#define ChatUserInfoGetUrl @"chat/member/get"

//用户信息编辑

#define UserInfoEditUrl @"member/home/edit"

//用户关注列表

#define UserFollowListUrl @"relation/follow/list"

//关注用户

#define UserFollowAddUrl @"relation/follow/add"

//取消关注

#define UserFollowCancelUrl  @"relation/follow/cancel"

//用户好友列表

#define UserFriendsListUrl @"relation/partner/list"

//充值记录
#define UserChargeListUrl @"record/recharge/list"

//发起充值

#define UserChargeAddUrl @"currency/recharge/add"

//消费记录

#define UserCurrencyListUrl  @"record/currency/list"

//旅游币兑换

#define ExchangeUrl @"currency/integral/cash"

//积分记录

#define UserIntegralUrl @"record/integral/list"

//粉丝列表

#define UserFansListUrl @"relation/fans/list"

//商城购买礼物

#define GiftBuyUrl @"gift/buy"

//快速送礼物

#define GiftSendUrl @"gift/give"

//用户信息获取

#define MemberGetUrl @"chat/member/get"

//获取地区

#define AddressGetUrl @"common/address/get"

//旅行添加联系人

#define TravelContactAddUrl @"contact/put"

//旅行联系人列表

#define TravelContactListUrl @"contact/index"

//旅行联系人删除

#define TravelContactDelUrl @"contact/del"

//旅游参加列表

#define TravelEnrollListUrl @"mytravel/index"

//添加旅行

#define TraveEnrollUrl @"signup/put"

//旅行详情

#define TravelScheduleUrl @"travel/schedule/get"

//旅行列表

#define TravelScheduleListUrl @"travel/schedule/list"


//个人主页

#define UserHomePageUrl @"homepage/get"

//发现，排行
#define DiscoverWeekLikeUrl @"ranking/week_like_hot"
#define DiscoverWeekFollowUrl @"ranking/week_follow_hot"
#define DiscoverWeekGiftUrl @"ranking/week_gift_hot"
#define DiscoverWeekCurrencyUrl @"ranking/week_currency_hot"

//消息中心

#define MessageCenterNotificationListUrl @"message/system/list"
#define MessageCenterNotificationClearUrl @"message/system/all/del"
#define MessageCenterNotificationDelUrl @"message/system/del"



#define MessageCenterLikeListUrl @"message/like/list"
#define MessageCenterLikeDelUrl @"message/like/del"
#define MessageCenterLikeClearUrl @"message/like/all/del"


#define MessageCenterCommentListUrl @"message/comment/list"

#endif /* ApiConstants_h */
