//
//  NewDynamicApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/20.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"
#import "DynamicModel.h"

@interface NewDynamicApi : BaseApi

-(instancetype)initWithDynaimcModel:(DynamicUploadModel *)model;

-(instancetype)initWithDesc:(NSString *)desc
                     images:(NSString *)images
                      video:(NSString *)video
                is_location:(BOOL )is_location
                   latitude:(NSString *)latitude
                  longitude:(NSString *)longitude
                    address:(NSString *)address;


@end
