//
//  TravelContactEditCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelContactEditCell.h"


@interface TravelContactEditCell ()


@property(nonatomic,strong) ImageButton *maleBtn;
@property(nonatomic,strong) ImageButton *femaleBtn;

@end

@implementation TravelContactEditCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.sex = @"1";
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.centerY.equalTo(self.contentView);
        make.width.mas_equalTo(100);
    }];
    
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.left.equalTo(self.titleLbl.mas_right).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
    
    [self.maleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl.mas_right).offset(10);
        make.centerY.equalTo(self.contentView);
        make.height.mas_equalTo(30);
    }];
    
    [self.femaleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.maleBtn.mas_right).offset(10);
        make.centerY.equalTo(self.contentView);
        make.height.mas_equalTo(30);
    }];
}

-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        _titleLbl.font = SystemFont(16);
        [self.contentView addSubview:_titleLbl];
    }
    return _titleLbl;
}

-(UITextField *)textField{
    if (!_textField) {
        _textField = [[UITextField alloc]init];
        _textField.font = SystemFont(16);
        [self.contentView addSubview:_textField];
    }
    return _textField;
}

-(ImageButton *)maleBtn{
    if (!_maleBtn) {
        _maleBtn = [[ImageButton alloc]init];
        _maleBtn.titleLbl.text  = LS(@"男");
        _maleBtn.contentImage.image = [UIImage imageNamed:@"ic_id_select"];
        [_maleBtn addTarget:self action:@selector(sexBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_maleBtn];
    }
    return _maleBtn;
}

-(ImageButton *)femaleBtn{
    if (!_femaleBtn) {
        _femaleBtn = [[ImageButton alloc]init];
        _femaleBtn.titleLbl.text  = LS(@"女");
        _femaleBtn.contentImage.image = [UIImage imageNamed:@"ic_id_select_no"];
        [_femaleBtn addTarget:self action:@selector(sexBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_femaleBtn];
    }
    return _femaleBtn;
}

-(void)setCellType:(EditCellType)cellType{
    _cellType = cellType;
    
    
    switch (cellType) {
        case EditCellType_Sex:
            self.textField.hidden = YES;
            self.femaleBtn.hidden = NO;
            self.maleBtn.hidden = NO;
            break;
        case EditCellType_TextField:
            self.textField.hidden = NO;
            self.textField.enabled = YES;
            self.femaleBtn.hidden = YES;
            self.maleBtn.hidden = YES;
            break;
        case EditCellType_Birthday:
        case EditCellType_IdCard:
            self.textField.hidden = NO;
            self.textField.enabled = NO;
            self.femaleBtn.hidden = YES;
            self.maleBtn.hidden = YES;
            break;
        default:
            break;
    }
}

-(void)setModel:(TravelContactModel *)model{
    _model = model;
}

-(void)setSex:(NSString *)sex{
    _sex = sex;
    if ([sex integerValue] == 0) {
        
        _maleBtn.contentImage.image = [UIImage imageNamed:@"ic_id_select_no"];
        _femaleBtn.contentImage.image = [UIImage imageNamed:@"ic_id_select"];
    }else{
        
        _maleBtn.contentImage.image = [UIImage imageNamed:@"ic_id_select"];
        _femaleBtn.contentImage.image = [UIImage imageNamed:@"ic_id_select_no"];
    }
}

-(void)sexBtnClick:(id)sender{
    if (sender == _maleBtn) {
        self.sex = @"1";
        
    }else{
        self.sex = @"0";
        
    }
}

@end
