//
//  TravelModel.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/18.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelModel.h"

@implementation TravelModel

+(NSDictionary *)objectClassInArray{
    return @{@"contact_data":@"TravelContactModel"};
}

@end
