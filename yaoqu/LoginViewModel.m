//
//  LoginViewModel.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/15.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "LoginViewModel.h"
#import "RegistApi.h"
#import "OuthApi.h"
#import <UMSocialCore/UMSocialCore.h>
#import "RCUserCacheManager.h"
#import "RouteController.h"
#import "MemberApi.h"
#import "LoginApi.h"
#import "CommonApi.h"
#import "LoginInfoModel.h"
#import "AccessTokenModel.h"
#import "LLUtils.h"

@interface LoginViewModel ()


@end

@implementation LoginViewModel{
    
}

-(void)initialize{
    self.succSubject = [RACSubject subject];
    self.failSubject = [RACSubject subject];
}


-(void)loginBtnClick:(UIButton *)button{
    [self userAouth:nil];

    return;
    WEAK_SELF;
    UMSocialPlatformType type;
    type = button.tag == LoginType_QQ ? UMSocialPlatformType_QQ : UMSocialPlatformType_WechatSession;
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:type currentViewController:self.presentVC completion:^(id result, NSError *error) {
        if (!error) {
            [weakSelf userAouth:result];
        }else{
            
//            [MBProgressHUD showErrorTip:error.description];
            [LLUtils showCenterTextHUD:error.description];
        }
        
    }];

    
  
    
}


-(LoginPostModel *)changeToPostModel:(UMSocialUserInfoResponse *)response{
    LoginPostModel *model = [[LoginPostModel alloc]init];
    model.username = response.name;
    model.sex = [response.gender isEqualToString:@"男"] ? @"1" : @"0";
    model.unionid = response.uid;
    
    if (response.platformType == UMSocialPlatformType_QQ){
        model.qq_openid = response.openid;
    }else{
        model.openid = response.openid;
    }
    
    model.imagelink = response.iconurl;
    
    return model;
}


-(void)userAouth:(UMSocialUserInfoResponse *)response{
    
//    [MBProgressHUD showProgressWithText:@"验证中..."];
    
    WEAK_SELF;
    NSDictionary *dic = nil;// @{@"type":@"wechat",@"openid":@"1B6D68D1E8E5277585392BD7559CC9DA"};
    dic = @{@"type":@"wechat",@"openid":@"ouc6O1PYA9X4WLrSTYXlsJv_d6z4",@"unionid":@"oBcJR1HelD7ofxxRP4BSDcAyvcAI"};
    
//    if (response.platformType == UMSocialPlatformType_QQ) {
//        dic = @{@"type":@"qq",@"qq_openid":response.openid,@"unionid":response.uid};
//    }else{
//        dic = @{@"type":@"wechat",@"openid":response.openid,@"unionid":response.uid};
//    }
    
    

    
    
    [[[LoginApi alloc]initWitObject:dic] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        
        
        if (info.error != ErrorCodeType_None){
            [weakSelf.failSubject sendNext:[weakSelf changeToPostModel:response]];
            return ;
        }
        
        LoginInfoModel *model = [LoginInfoModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
        
        [[RCUserCacheManager sharedManager]setCurrentLoginInfo:model];
        
//        NSDictionary *dict = @{@"client_id":model.client_id,@"secret":model.client_secret};
        [[[CommonApi alloc]initWithModel:@"accessToken" object:model] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            
            AccessTokenModel *model = [AccessTokenModel objectWithKeyValues:responseObj];
            [[RCUserCacheManager sharedManager] setCurrentToken:model];
            
            [[[MemberApi alloc]initWitObject:responseObj] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                
                UserModel *userModel = [UserModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
                [[RCUserCacheManager sharedManager] setCurrentUser:userModel];
                [[RouteController sharedManager] openMainController];
                
            }];
            
        }];
        
    }];
    
//    OuthApi *outhApi = [[OuthApi alloc]initWithOpenID:@"1B6D68D1E8E5277585392BD7559CC9DA" unionidID:@"1B6D68D1E8E5277585392BD7559CC9DA"];
    
//    WEAK_SELF;
//    [outhApi startWithCompleteBlock:^(MsgModel *info, id responseObj) {
//        if (info.state) {
//            UserModel *userModel = [UserModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
//            [[RCUserCacheManager sharedManager] setCurrentUser:userModel];
//            [[RouteController sharedManager] openMainController];
//        }else{
//            [weakSelf.failSubject sendNext:[weakSelf changeToUserModel:response]];
//        }
//    }];
    
}

@end
