//
//  MemberCenterVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/10.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MemberCenterVC.h"
#import "MemberApi.h"
#import "MemberRightModel.h"
#import "RCUserCacheManager.h"

@interface MemberCenterVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UIView *headerView;

@end

@implementation MemberCenterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"会员中心");
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(10);
        make.right.equalTo(self.view.mas_right).offset(-10);
        make.bottom.equalTo(self.view.mas_bottom);
        make.top.equalTo(self.view.mas_top);
    }];
    
    self.tableView.contentInset = UIEdgeInsetsMake(10, 0, 0, 0);
    self.tableView.tableHeaderView = self.headerView;
    
    
    self.isloading = NO;
    WEAK_SELF;
    MemberApi *api = [[MemberApi alloc]initWitObject:@{@"type":@"member"}];
    api.method = YTKRequestMethodOPTION;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        weakSelf.datas = [MemberRightModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        [weakSelf.tableView reloadData];
    }];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIView *)headerView{
    if (!_headerView) {
        
        
        
        _headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH - 20, 100)];
        _headerView.backgroundColor = [UIColor whiteColor];
        
        UIImageView *imageView = [[UIImageView alloc]init];
        imageView.layer.cornerRadius = 3;
        imageView.layer.masksToBounds = YES;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [_headerView addSubview:imageView];
        
        UILabel *label1 = [ControllerHelper autoFitLabel];
        label1.font = SystemFont(14);
        [_headerView addSubview:label1];
        
        UILabel *label2 = [ControllerHelper autoFitLabel];
        label2.font = SystemFont(12);
        label2.textColor = AppGray;
        [_headerView addSubview:label2];
        
        ImageButton *btn = [[ImageButton alloc]init];
        btn.layer.cornerRadius = 3;
        btn.layer.masksToBounds = YES;
        btn.backgroundColor = [UIColor orangeColor];
        btn.contentImage.image = [[UIImage imageNamed:@"ic_user_level_1"] imageToColor:[UIColor whiteColor]];
        btn.titleLbl.textColor = [UIColor whiteColor];
        btn.titleLbl.text = LS(@"开通会员");
        [btn addTarget:self action:@selector(openBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_headerView addSubview:btn];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headerView.mas_left).offset(10);
            make.top.equalTo(_headerView.mas_top).offset(20);
            make.bottom.equalTo(_headerView.mas_bottom).offset(-20);
            make.width.equalTo(imageView.mas_height);
        }];
        
        [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(imageView.mas_right).offset(10);
            make.top.equalTo(imageView.mas_top);
        }];
        
        [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(imageView.mas_right).offset(10);
            make.bottom.equalTo(imageView.mas_bottom);
        }];
        
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_headerView.mas_right).offset(-10);
            make.centerY.equalTo(_headerView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(100, 30));
        }];
        
        [RACObserve([RCUserCacheManager sharedManager], currentUser) subscribeNext:^(UserModel *x) {
            label1.text = x.is_member ? @"已开通会员" : @"未开通服务";
            btn.hidden = x.is_member;
            [imageView setImageName:x.image placeholder:nil];
        }];
        
        [RACObserve(self, datas) subscribeNext:^(id x) {
            label2.text = [NSString stringWithFormat:@"会员立享%ld项特权",[x count]];
        }];
        
    }
    return _headerView;
}

-(UIView *)sectionTipView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH - 20, 60)];
    
    UIView *bgView =[[UIView alloc]initWithFrame:CGRectMake(0, 10, APP_WIDTH - 20, 50)];
    bgView.backgroundColor = [UIColor whiteColor];
    [view addSubview:bgView];
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 59.5, APP_WIDTH, 0.5)];
    line.backgroundColor = AppLineColor;
    [view addSubview:line];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"特权介绍";
    label.font = SystemFont(18);
    label.frame = CGRectMake(10, 10, APP_WIDTH - 10, 50);
    [view addSubview:label];
    
    return view;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [self sectionTipView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.textLabel.font = SystemFont(14);
        cell.textLabel.textColor = AppGray;
    }
    MemberRightModel *model = self.datas[indexPath.row];
    cell.textLabel.text = model.desc;
    return cell;
}


-(void)openBtnClick:(id)sender{
    [self pushViewControllerWithName:@"JoinMemberVC" params:self.datas];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
