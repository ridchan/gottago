//
//  TravelPayTypeView.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/17.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TravelPayTypeView : UIView

+(void)showInView:(UIView *)view compelteBlock:(BaseBlock)block;

@end
