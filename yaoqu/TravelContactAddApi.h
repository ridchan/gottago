//
//  TravelContactAddApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"
#import "TravelContactModel.h"
@interface TravelContactAddApi : BaseApi

-(id)initWithModel:(TravelContactModel *)model;

@end
