//
//  UserHeaderTipBar.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/16.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UserHeaderTipBarLayout : UICollectionViewFlowLayout

@end

@interface UserHeaderTipBarCell : UICollectionViewCell

@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) UILabel *countLbl;

@end


@interface UserHeaderTipBar : UIView

@property(nonatomic,strong) NSArray *dataSource;
@property(nonatomic,strong) UIFont *titleFont;
@property(nonatomic,strong) UIFont *countFont;

@end
