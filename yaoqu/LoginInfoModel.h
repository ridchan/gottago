//
//  LoginInfoModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/1.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface LoginInfoModel : BaseModel

@property(nonatomic,strong) NSString *client_id;
@property(nonatomic,strong) NSString *client_secret;
@property(nonatomic,strong,readonly) NSString *secret;



@end
