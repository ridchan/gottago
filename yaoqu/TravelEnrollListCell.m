//
//  TravelEnrollListCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/17.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelEnrollListCell.h"


@interface TravelEnrollListCell()

@property(nonatomic,strong) UIImageView *contentImage;
@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) ImageButton *localLbl;
@property(nonatomic,strong) ImageButton *dateLbl;
@property(nonatomic,strong) UILabel *statuLbl;
@property(nonatomic,strong) UILabel *totalLbl;
@property(nonatomic,strong) ImageButton *marchBtn;

@property(nonatomic,strong) UIView *line;
@property(nonatomic,strong) ImageButton *depositLbl;
@property(nonatomic,strong) ImageButton *coinLbl;
@property(nonatomic,strong) UILabel *payLbl;

@property(nonatomic,strong) UIImageView *accessImage;
@property(nonatomic,strong) UILabel *accessLbl;

@end

@implementation TravelEnrollListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self commonInit];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)commonInit{
    [self.contentImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left);
        make.top.equalTo(self.contentView.mas_top);
        make.right.equalTo(self.contentView.mas_right);
        make.height.mas_equalTo(160);
    }];
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.top.equalTo(self.contentImage.mas_bottom).offset(5);
    }];

    [self.localLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.top.equalTo(self.titleLbl.mas_bottom).offset(15);
        make.height.mas_equalTo(20);
    }];

    [self.dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.localLbl.mas_right).offset(5);
        make.top.equalTo(self.localLbl.mas_top);
        make.height.mas_equalTo(20);
    }];
    

    [self.totalLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl.mas_left);
        make.bottom.equalTo(self.contentImage.mas_bottom);
    }];

    [self.statuLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl.mas_right).offset(10);
        make.centerY.equalTo(self.titleLbl.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50, 20));
    }];

    
    [self.marchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.top.equalTo(self.statuLbl.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(80, 25));
    }];

    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left);
        make.top.equalTo(self.localLbl.mas_bottom).offset(5);
        make.right.equalTo(self.contentView.mas_right);
        make.height.mas_equalTo(0.5);
    }];

    [self.depositLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.top.equalTo(self.line.mas_bottom).offset(5);
        make.height.mas_equalTo(20);
//        make.bottom.equalTo(self.contentView.mas_bottom).offset(-5);
    }];

    [self.coinLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.depositLbl.mas_right).offset(20);
        make.height.mas_equalTo(20);
        make.centerY.equalTo(self.depositLbl.mas_centerY);
    }];

    
    [self.payLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.coinLbl.mas_right).offset(20);
        make.height.mas_equalTo(20);
        make.centerY.equalTo(self.depositLbl.mas_centerY);
    }];
//
    [self.accessImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.centerY.equalTo(self.depositLbl.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(RightAccessWidth, 30));
    }];
    
    [self.accessLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.accessImage.mas_left).offset(-5);
        make.centerY.equalTo(self.depositLbl.mas_centerY);
    }];
    
}

#pragma mark -
#pragma mark lazy layout

-(UIImageView *)contentImage{
    if (!_contentImage) {
        _contentImage = [[UIImageView alloc]init];
        _contentImage.clipsToBounds = YES;
        _contentImage.contentMode = UIViewContentModeScaleAspectFill;
        _contentImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self.contentView addSubview:_contentImage];
    }
    return _contentImage;
}

-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        [self.contentView addSubview:_titleLbl];
    }
    return _titleLbl;
}

-(ImageButton *)localLbl{
    if (!_localLbl) {
        _localLbl = [[ImageButton alloc]init];
        _localLbl.contentImage.image = [UIImage imageNamed:@"ic_location_gray"];
        _localLbl.titleLbl.text = @"位置";
        [self.contentView addSubview:_localLbl];
    }
    return _localLbl;
}

-(ImageButton *)dateLbl{
    if (!_dateLbl) {
        _dateLbl = [[ImageButton alloc]init];
        _dateLbl.contentImage.image = [UIImage imageNamed:@"ic_travel_icon"];
        _dateLbl.titleLbl.text = @"日期";
        [self.contentView addSubview:_dateLbl];
    }
    return _dateLbl;
}

-(UILabel *)statuLbl{
    if (!_statuLbl) {
        _statuLbl = [ControllerHelper autoFitLabel];
        _statuLbl.textColor = [UIColor orangeColor];
        _statuLbl.text = LS(@"未开始");
        _statuLbl.font = SystemFont(11);
        _statuLbl.textAlignment = NSTextAlignmentCenter;
        _statuLbl.layer.borderColor = [UIColor orangeColor].CGColor;
        _statuLbl.layer.borderWidth = 1.0;
        _statuLbl.layer.masksToBounds = YES;
        _statuLbl.layer.cornerRadius = 3;
        [self.contentView addSubview:_statuLbl];
    }
    return _statuLbl;
}

-(UILabel *)totalLbl{
    if (!_totalLbl) {
        _totalLbl = [ControllerHelper autoFitLabel];
        _totalLbl.textColor = [UIColor redColor];
        _totalLbl.text = @"abcde";
        [self.contentView addSubview:_totalLbl];
    }
    return _totalLbl;
}

-(ImageButton *)marchBtn{
    if (!_marchBtn) {
        _marchBtn = [[ImageButton alloc]init];
        _marchBtn.contentImage.image = [[UIImage imageNamed:@"ic_dynamic_comment"] imageToColor:[UIColor whiteColor]];
        _marchBtn.titleLbl.text = LS(@"行程主页");
        _marchBtn.titleLbl.textColor = [UIColor whiteColor];
        _marchBtn.backgroundColor = [UIColor orangeColor];
        _marchBtn.layer.cornerRadius = 3;
        _marchBtn.layer.masksToBounds = YES;
        [self.contentView addSubview:_marchBtn];
    }
    return _marchBtn;
}


-(UIView *)line{
    if (!_line) {
        _line = [[UIView alloc]init];
        _line.backgroundColor = AppLineColor;
        [self.contentView addSubview:_line];
    }
    return _line;
}

-(ImageButton *)depositLbl{
    if (!_depositLbl) {
        _depositLbl = [[ImageButton alloc]init];
        _depositLbl.contentImage.image = [UIImage imageNamed:@"ic_score"];
        _depositLbl.titleLbl.text = @"150 积分";
        [self.contentView addSubview:_depositLbl];
    }
    return _depositLbl;
}

-(ImageButton *)coinLbl{
    if (!_coinLbl) {
        _coinLbl = [[ImageButton alloc]init];
        _coinLbl.contentImage.image = [UIImage imageNamed:@"ic_coin"];
        _coinLbl.titleLbl.text = @"150 旅游币";
        [self.contentView addSubview:_coinLbl];
    }
    return _coinLbl;
}


-(UILabel *)payLbl{
    if (!_payLbl) {
        _payLbl = [ControllerHelper autoFitLabel];
        _payLbl.font = SystemFont(11);
        _payLbl.text = @"实付:1923";
        _payLbl.textColor = [UIColor orangeColor];
        [self.contentView addSubview:_payLbl];
    }
    return _payLbl;
}

-(UILabel *)accessLbl{
    if (!_accessLbl) {
        _accessLbl = [ControllerHelper autoFitLabel];
        _accessLbl.font = SystemFont(14);
        _accessLbl.text = @"详情";
        [self.contentView addSubview:_accessLbl];
    }
    return _accessLbl;
}

-(UIImageView *)accessImage{
    if (!_accessImage) {
        _accessImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_forward"]];
        _accessImage.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_accessImage];
    }
    return _accessImage;
}

-(void)setModel:(TravelModel *)model{
    _model = model;
    [self.contentImage setImageName:model.image placeholder:nil];
    self.titleLbl.text = model.title;
    self.localLbl.titleLbl.text = model.address;
    self.dateLbl.titleLbl.text = model.date;
    self.totalLbl.text = nil;// model.pay_total;
    self.depositLbl.titleLbl.text = [NSString stringWithFormat:@"%@积分",model.integral];
    self.coinLbl.titleLbl.text = [NSString stringWithFormat:@"%@旅游币",model.currency];
    self.accessLbl.text = [NSString stringWithFormat:@"%@%@",MoneySign,model.total];
    self.payLbl.text = [NSString stringWithFormat:@"实付:%@%@",MoneySign,model.pay_total];
    
}

@end
