//
//  DiscoverVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DiscoverVC.h"
#import "NavTitleView.h"
#import "DiscoverDynamicVC.h"
#import "DiscoverHotVC.h"
#import "DiscoverrRankVC.h"
#import "RCVCScrollView.h"
@interface DiscoverVC ()<UIScrollViewDelegate>

@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) NavTitleView *titleView;


//

@property(nonatomic,strong) DiscoverrRankVC *rankVC;
@property(nonatomic,strong) DiscoverDynamicVC *dynamicVC;
@property(nonatomic,strong) DiscoverHotVC *hotVC;

@end

@implementation DiscoverVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = nil;
    self.dynamicVC  =  [[DiscoverDynamicVC alloc]init];
    self.hotVC = [[DiscoverHotVC alloc]init];
    self.rankVC = [[DiscoverrRankVC alloc]init];
    [self commonInit];
    
    [self setRightItemWithIcon:[UIImage imageNamed:@"ic_search"] selector:@selector(showSearch:)];
    // Do any additional setup after loading the view.
}


-(void)showSearch:(id)sender{
    [self pushViewControllerWithName:@"DiscoverSearchVC"];
//    [[RouteController sharedManager] presentNavVC:@"DiscoverSearchVC"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)commonInit{
    
    

    
    
    self.navigationItem.titleView = self.titleView;
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
        make.top.equalTo(self.view.mas_top);
    }];
    
    
    
    [self addChildViewController:self.dynamicVC];
    [self.scrollView addSubview:self.dynamicVC.view];
    
    [self addChildViewController:self.hotVC];
    [self.scrollView addSubview:self.hotVC.view];
    
    
    [self addChildViewController:self.rankVC];
    [self.scrollView addSubview:self.rankVC.view];
    
    [self.rankVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView.mas_left);
        make.top.equalTo(self.scrollView.mas_top);
        make.width.mas_equalTo(APP_WIDTH);
        make.height.mas_equalTo(APP_HEIGHT - 64 - 50);
    }];

    
    [self.hotVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView.mas_left).offset(APP_WIDTH);
        make.top.equalTo(self.scrollView.mas_top);
        make.width.mas_equalTo(APP_WIDTH);
        make.height.mas_equalTo(APP_HEIGHT - 64 - 50);
    }];
    
    [self.dynamicVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView.mas_left).offset(APP_WIDTH * 2);
        make.top.equalTo(self.scrollView.mas_top);
        make.width.mas_equalTo(APP_WIDTH);
        make.height.mas_equalTo(APP_HEIGHT - 64 - 50);
    }];
    
    
    
    
}

-(UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]init];
        _scrollView.pagingEnabled = YES;
        _scrollView.contentSize = CGSizeMake(APP_WIDTH * 3, 1);
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.bounces = NO;
        _scrollView.delegate = self;
        [self.view addSubview:_scrollView];
    }
    
    return _scrollView;
}



-(NavTitleView *)titleView{
    if (!_titleView) {
        _titleView = [[NavTitleView alloc]initWithFrame:CGRectMake(0, 0, 200, 35)];
        _titleView.titles = @[LS(@"排行"),LS(@"热门"),LS(@"动态")];
        WEAK_SELF;
        _titleView.selectBlock = ^(id responseObj) {
            CGPoint point = weakSelf.scrollView.contentOffset;
            weakSelf.scrollView.contentOffset = CGPointMake(APP_WIDTH * [responseObj integerValue],point.y);
        };
    }
    return _titleView;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    self.titleView.contentOffset = scrollView.contentOffset.x / scrollView.frame.size.width;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
