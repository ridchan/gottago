//
//  NSString+Extend.h
//  ToWest
//
//  Created by 陳景雲 on 15/11/3.
//  Copyright © 2015年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSUInteger, DateFormatType) {
    kDateFormatTypeYYYYMMDDHHMM,
    kDateFormatTypeYYYYMM,
    kDateFormatTypeYYYYMMDD,
    kDateFormatTypeYYYYMMDDHHMMSS,
    kDateFormatTypeMMDD,
    kDateFormatTypeDDMM_CN,
    kDateFormatTypeHH,
    kDateFormatTypeHHMMSS,
    kDateFormatTypeTomorrow,
    KDateFormatTypeDDMM,
    KDateFormatTypeYMDHM,
    KDateFormatTypeYMD,
};


@interface NSString (Extend)


+(NSString *)md5:(NSString *)str;
+ (NSString *)documentPath;

+ (NSString *)tempPath;
+ (NSString *)cachePath;

+ (NSString *)strValue:(NSInteger)integer;
+(NSString *)floatValue:(float)num;
+(NSString *)intValue:(NSInteger)integer;
+(NSString *)boolValue:(BOOL)value;

+(NSString *)firstWord:(NSString *)name;
+(NSString *)randomString;
+(NSString *)randomTime;



- (BOOL)isMobileNumber;

-(NSString *)fixLink;
-(NSString *)fixImageString:(NSString *)fix;
-(NSString *)lastString:(NSInteger)num;
-(NSString *)numberString;
-(NSString *)numberValue;
-(NSString *)substringFromString:(NSString *)str;
-(NSString *)substringToString:(NSString *)str;
-(NSString *)subStringWithString:(NSString *)str;
-(NSString *)autoIncrice;
-(NSString *)autoReduce;

-(CGFloat)heightWithFont:(UIFont *)font inWidth:(CGFloat)width;
-(CGFloat)widthtWithFont:(UIFont *)font inHeigth:(CGFloat)height;
-( unichar)firstChar;
-(float) toVersion;
-(BOOL)isStringEmpty;


//

+ (NSString *)stringFromDate:(NSDate *)date format:(DateFormatType)dateType;
+ (NSString *)stringFromTimeMark:(long long)time format:(DateFormatType) dateType;
+ (NSString *) compareCurrentTime:(long long) time;


@end
