//
//  UserCommentCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserCommentCell.h"
#import "SexAgeView.h"
#import "UserImageView.h"

@interface UserCommentCell()

@property(nonatomic,strong) UserImageView *userImage;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) SexAgeView *sexView;
@property(nonatomic,strong) UILabel *dateLbl;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UIImageView *contentImage;
@property(nonatomic,strong) UIView *line;
@property(nonatomic,strong) UILabel *commentLbl;
@property(nonatomic,strong) UIView *bottomLine;


@end

@implementation UserCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    self.userImage.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .topSpaceToView(self.contentView, 10)
    .heightIs(50)
    .widthIs(50);
    
    self.nameLbl.sd_layout
    .leftSpaceToView(self.userImage, 10)
    .topSpaceToView(self.contentView, 10)
    .heightIs(25)
    .autoWidthRatio(0);
    [self.nameLbl setSingleLineAutoResizeWithMaxWidth:200];
    
    
    self.sexView.sd_layout
    .leftSpaceToView(self.nameLbl, 5)
    .centerYEqualToView(self.nameLbl)
    .heightIs(15)
    .widthIs(35);
    
    self.descLbl.sd_layout
    .leftEqualToView(self.nameLbl)
    .topSpaceToView(self.userImage, 0)
    .rightSpaceToView(self.contentView, 10)
    .autoHeightRatio(0);
    
    self.line.sd_layout
    .leftEqualToView(self.nameLbl)
    .rightSpaceToView(self.contentView, 10)
    .topSpaceToView(self.descLbl, 10)
    .heightIs(0.5);
    
    self.commentLbl.sd_layout
    .leftEqualToView(self.nameLbl)
    .topSpaceToView(self.line, 10)
    .rightSpaceToView(self.contentView, 10)
    .autoHeightRatio(0);
    
    self.dateLbl.sd_layout
    .rightSpaceToView(self.contentView, 10)
    .topSpaceToView(self.contentView, 10)
    .heightIs(15)
    .widthIs(100);
    
    self.bottomLine.sd_layout
    .leftEqualToView(self.contentView)
    .rightEqualToView(self.contentView)
    .bottomEqualToView(self.contentView)
    .heightIs(1.0);
    
    [self setupAutoHeightWithBottomView:self.commentLbl bottomMargin:10];
    
    
}


-(UserImageView *)userImage{
    if (!_userImage) {
        _userImage = [[UserImageView alloc]init];
        [self.contentView addSubview:_userImage];
    }
    return _userImage;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        _nameLbl.font = SystemFont(14);
        [self.contentView addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(SexAgeView *)sexView{
    if (!_sexView) {
        _sexView = [[SexAgeView alloc]init];
        [self.contentView addSubview:_sexView];
    }
    return _sexView;
}

-(UILabel *)dateLbl{
    if (!_dateLbl) {
        _dateLbl = [ControllerHelper autoFitLabel];
        _dateLbl.font = SystemFont(10);
        _dateLbl.textColor = AppGray;
        _dateLbl.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_dateLbl];
    }
    return _dateLbl;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.font = SystemFont(12);
        _descLbl.numberOfLines = 0;
        _descLbl.textColor = AppGray;
        [self.contentView addSubview:_descLbl];
    }
    return _descLbl;
}

-(UILabel *)commentLbl{
    if (!_commentLbl) {
        _commentLbl = [ControllerHelper autoFitLabel];
        _commentLbl.font = SystemFont(12);
        _commentLbl.numberOfLines = 0;
        _commentLbl.textColor = AppGray;
        [self.contentView addSubview:_commentLbl];
    }
    return _commentLbl;
}


-(UIImageView *)contentImage{
    if (!_contentImage) {
        _contentImage = [[UIImageView alloc]init];
        _contentImage.clipsToBounds = YES;
        _contentImage.contentMode = UIViewContentModeScaleAspectFill;
        _contentImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.contentView addSubview:_contentImage];
    }
    return _contentImage;
}

-(UIView *)line{
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = AppLineColor;
        [self.contentView addSubview:_line];
    }
    return _line;
}


-(UIView *)bottomLine{
    if (!_bottomLine) {
        _bottomLine = [ControllerHelper doubleLine];
        [self.contentView addSubview:_bottomLine];
    }
    return _bottomLine;
}

-(void)setModel:(MessageCommentModel *)model{
    _model = model;
    self.userImage.userModel = [model objectWithClass:@"UserModel"];
    self.nameLbl.text = model.username;
    self.sexView.sex = [model.sex integerValue];
    self.sexView.age = [model.age integerValue];
    self.dateLbl.text = model.show_time;
    self.descLbl.text = model.message_desc;
    self.commentLbl.attributedText = [self attFromString:model.message_content];
}


-(NSMutableAttributedString *)attFromString:(NSString *)str{
    
    NSString *str1 = LS(@"回复我的评论:");
    NSString *attString = [NSString stringWithFormat:@"%@%@",str1,str];
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc]initWithString:attString];
    [att addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, [str1 length])];
    return att;
}


@end
