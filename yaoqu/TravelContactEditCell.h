//
//  TravelContactEditCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TravelContactModel.h"

typedef NS_ENUM(NSInteger,EditCellType) {
    EditCellType_TextField,
    EditCellType_Sex,
    EditCellType_IdCard,
    EditCellType_Birthday
};

@interface TravelContactEditCell : UITableViewCell


@property(nonatomic) EditCellType cellType;

@property(nonatomic,strong) NSString *sex;
@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) UITextField *textField;

@property(nonatomic,strong) TravelContactModel *model;

@end
