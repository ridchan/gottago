//
//  ControllerHelper.m
//  QBH
//
//  Created by 陳景雲 on 2017/1/4.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "ControllerHelper.h"

@implementation ControllerHelper

+(UIButton *)buttonWithTitle:(NSString *)title Color:(UIColor *)color{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.layer.cornerRadius = 8.0;
    btn.backgroundColor = [UIColor orangeColor];
    btn.titleLabel.textColor = [UIColor whiteColor];
    btn.layer.shadowOpacity = 0.6;
    btn.layer.shadowColor = [UIColor orangeColor].CGColor;
    btn.layer.shadowOffset = CGSizeMake(2, 2);
    btn.titleLabel.layer.shadowOpacity = 0.6;
    btn.titleLabel.layer.shadowOffset = CGSizeMake(2, 1);
    btn.titleLabel.layer.shadowColor = RGB(218,133,59).CGColor;
    [btn setTitle:title forState:UIControlStateNormal];
    return btn;
}

+(UILabel *)autoFitLabel{
    UILabel *label = [[UILabel alloc]init];
    label.text = @"没有内容";
    [label setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal | UILayoutConstraintAxisVertical];
    return label;
}

+(UIView *)doubleLine{
    UIView *doubleLine = [[UIView alloc]init];
    doubleLine.backgroundColor = [UIColor clearColor];

    
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = RGB16(0xf5f5f5);
    [doubleLine addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(doubleLine.mas_left);
        make.right.equalTo(doubleLine.mas_right);
        make.bottom.equalTo(doubleLine.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];
    
    UIView *line2 = [[UIView alloc]init];
    line2.backgroundColor = RGB16(0xe9e9e9);
    [doubleLine addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(doubleLine.mas_left);
        make.right.equalTo(doubleLine.mas_right);
        make.top.equalTo(doubleLine.mas_top);
        make.height.mas_equalTo(0.5);
    }];
    
    return doubleLine;

}


@end


@implementation ImageButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self createViews];
    }
    return self;
}

-(void)createViews{
    
    self.titleLbl = [ControllerHelper autoFitLabel];
    self.titleLbl.textColor = [UIColor lightGrayColor];
    self.titleLbl.font = [UIFont systemFontOfSize:11];
    [self addSubview:self.titleLbl];
    
    self.contentImage = [[UIImageView alloc]init];
    self.contentImage.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:self.contentImage];
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentImage.mas_right).offset(5);
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
        make.right.equalTo(self.mas_right).offset(-5);
    }];
    
    
    [self.contentImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.top.equalTo(self.mas_top).offset(5);
        make.bottom.equalTo(self.mas_bottom).offset(-5);
        make.width.equalTo(self.mas_height);
    }];
}

-(void)setAutoFit{
    [self.titleLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentImage.mas_right).offset(2);
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
        make.right.equalTo(self.mas_right);
    }];
    
    
    [self.contentImage mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
        make.width.equalTo(self.mas_height);
    }];

}

-(void)setFixImage:(CGSize)size{
    [self.contentImage mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(size);
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.titleLbl.mas_left).offset(-2);
    }];
    
    [self.titleLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
        make.centerX.equalTo(self.mas_centerX).offset(size.width / 2.0);
    }];

}

-(void)setFix{
    
    
    [self.contentImage mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(5);
        make.bottom.equalTo(self.mas_bottom).offset(-5);
        make.right.equalTo(self.titleLbl.mas_left);
        make.width.equalTo(self.mas_height);
    }];
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
        make.centerX.equalTo(self.mas_centerX).offset(10);
    }];
}

-(void)setAlignment:(NSTextAlignment )alignment{
    _alignment = alignment;
    
    if (alignment == NSTextAlignmentCenter) {
        
    }
}

-(CAKeyframeAnimation *)keyAnimation{
    CAKeyframeAnimation *k = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    k.values = @[@(0.1),@(1.0),@(1.5)];
    k.keyTimes = @[@(0.0),@(0.5),@(0.8),@(1.0)];
    k.calculationMode = kCAAnimationLinear;
    return k;
}

@end

@implementation NormalButton

+(NormalButton *)normalButton:(NSString *)title{
    NormalButton *btn = [NormalButton buttonWithType:UIButtonTypeCustom];
    btn.layer.cornerRadius = 8.0;
    
    
    btn.normalBackgroundColor = [UIColor orangeColor];
    btn.disableBackGroundColor = [UIColor lightGrayColor];
    btn.normalShadowColor = [UIColor orangeColor];
    btn.disableShadowColor = [UIColor lightGrayColor];
    
    btn.normalTitleColor = [UIColor whiteColor];
    btn.normalTitleShadowColor = RGB(218,133,59);
    btn.disableTitleShadowColor = [UIColor clearColor];
    
    
    
    btn.backgroundColor = btn.normalBackgroundColor;
    btn.titleLabel.textColor = btn.normalTitleColor;
    btn.layer.shadowOpacity = 0.3;
    btn.layer.shadowColor = btn.normalShadowColor.CGColor;
    btn.layer.shadowOffset = CGSizeMake(2, 2);
    btn.titleLabel.layer.shadowOpacity = 0.6;
    btn.titleLabel.layer.shadowOffset = CGSizeMake(2, 1);
    btn.titleLabel.layer.shadowColor = btn.normalTitleShadowColor.CGColor;
    [btn setTitle:title forState:UIControlStateNormal];

    
    
    return btn;
}

+(NormalButton *)whiteButtton:(NSString *)title{

    
    
    NormalButton *btn = [NormalButton buttonWithType:UIButtonTypeCustom];
    btn.layer.cornerRadius = 8.0;
    
    btn.backgroundColor =[UIColor whiteColor];
    btn.layer.cornerRadius = 8.0;
    btn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    btn.layer.borderWidth = 0.5;
    
    
    
    btn.normalBackgroundColor = [UIColor whiteColor];
    btn.disableBackGroundColor = [UIColor lightGrayColor];
    btn.normalShadowColor = [UIColor groupTableViewBackgroundColor];
    btn.disableShadowColor = [UIColor lightGrayColor];
    
    btn.normalTitleColor = [UIColor blackColor];
    btn.normalTitleShadowColor = [UIColor clearColor];
    btn.disableTitleShadowColor = [UIColor clearColor];
    
    
    
    btn.backgroundColor = btn.normalBackgroundColor;
    [btn setTitleColor:btn.normalTitleColor forState:UIControlStateNormal];
    btn.layer.shadowOpacity = 0.5;
    btn.layer.shadowColor = btn.normalShadowColor.CGColor;
    btn.layer.shadowOffset = CGSizeMake(2, 2);
    btn.titleLabel.layer.shadowOpacity = 0.6;
    btn.titleLabel.layer.shadowOffset = CGSizeMake(2, 1);
    btn.titleLabel.layer.shadowColor = btn.normalTitleShadowColor.CGColor;
    [btn setTitle:title forState:UIControlStateNormal];
    
    
    
    return btn;
}

-(void)setHighlighted:(BOOL)highlighted{
    [super setHighlighted:highlighted];
    if (highlighted) {
        self.backgroundColor = self.normalTitleShadowColor;
    }else{
        self.backgroundColor = self.normalBackgroundColor;
    }
}

-(void)setEnabled:(BOOL)enabled{
    [super setEnabled:enabled];
    self.backgroundColor = enabled ? self.normalBackgroundColor : self.disableBackGroundColor;
    self.layer.shadowColor = enabled ? self.normalShadowColor.CGColor : self.disableShadowColor.CGColor;
    self.titleLabel.layer.shadowColor = enabled ? self.normalTitleShadowColor.CGColor : self.disableTitleShadowColor.CGColor;
}

@end


@implementation UIView (RCExtend)

-(UIView *)lineInColor:(UIColor *)color{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = color;
    [self addSubview:view];
    return view;
}

-(void)setBubble:(NSString *)bubble offset:(CGSize)size{
    UIView *view = self.superview ? [self.superview viewWithTag:9999] : [self viewWithTag:9999];
    if (view){
        
    }else{
        view = [[UIView alloc]init];
        view.layer.masksToBounds = YES;
        view.layer.cornerRadius = 4.0;
        view.backgroundColor = AppRedPoint;
        view.tag = 9999;
        
        if (!self.superview){
            [self addSubview:view];
            [view mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self.mas_right).offset(-size.width);
                make.top.equalTo(self.mas_top).offset(-size.height);
                make.size.mas_equalTo(CGSizeMake(8, 8));
            }];
        }else{
            [self.superview addSubview:view];
            [view mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.mas_right);
                make.top.equalTo(self.mas_top);
                make.size.mas_equalTo(CGSizeMake(8, 8));
            }];
        }

    }
    view.hidden = [bubble integerValue] == 0;
}

@end



@implementation UILabel (RCExtend)

-(void)addTarget:(id)target action:(SEL)action{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:target action:action];
    self.userInteractionEnabled = YES;
    [self addGestureRecognizer:tap];
}

@end


