//
//  MineNotificationCenterVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/31.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineNotificationCenterVC.h"
#import "RCUserCacheManager.h"
#import "MemberApi.h"

@interface MineNotificationCenterVC()

@property(nonatomic,strong) UserModel *userModel;

@end

@implementation MineNotificationCenterVC


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = LS(@"消息设置");
    self.datas = [@[
                    @[@{@"name":LS(@"接收推送"),@"vc":@"MineNotificationVC"},
                      @{@"name":LS(@"聊天消息提示"),@"vc":@"MinePraiseVC"},
                      @{@"name":LS(@"动态消息提示"),@"vc":@"MineCommentVC"},
                      @{@"name":LS(@"系统消息"),@"vc":@"MineLookAtMeVC"},
                      @{@"name":LS(@"邀请消息提示"),@"vc":@"MineGiftReceiveVC"},
                      @{@"name":LS(@"陌生人消息提示"),@"vc":@""}
                      ],
                    @[@{@"name":LS(@"黑名单"),@"vc":@""}
                      ]
                    ] mutableCopy];
    
    self.userModel = [RCUserCacheManager sharedManager].currentUser;
//    NSLog(@"%@",[[RCUserCacheManager sharedManager].currentUser keyValues]);
    //    [self commonInit];
    // Do any additional setup after loading the view.
}

-(void)btnClick:(id)sender{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1){
        [self pushViewControllerWithName:@"MineBlacklistVC"];
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.datas[section] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NormalTableViewCell *cell = nil;
    if (indexPath.section == 0){
        cell = [NormalTableViewCell cellWithType:NormalCellType_Switch identifier:@"Cell" tableView:tableView];
        [self setCell:cell index:indexPath.row];
        [self changeCell:cell index:indexPath.row];
    }else{
        cell = [NormalTableViewCell cellWithType:NormalCellType_Access identifier:@"Cell" tableView:tableView];
    }

    NSDictionary *dict = self.datas[indexPath.section][indexPath.row];
    cell.textLabel.text = dict[@"name"];
    cell.type = indexPath.row == 0 ? NormalCellType_None : NormalCellType_TopLine;
    
    return cell;
}


-(void)changeCell:(NormalTableViewCell *)cell index:(NSInteger )index{
    NSArray *array = @[@"is_push",@"is_push_chat",@"is_push_dynamic",@"is_push_system",@"is_push_invite",@"is_push_stranger"];
    [cell rac_prepareForReuseSignal];
    WEAK_SELF;
    [RACObserve(cell, isSwitch) subscribeNext:^(id x) {
        MemberApi *api  = [[MemberApi alloc]initWitObject:@{array[index]:x}];
        api.method = YTKRequestMethodPOST;
        [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            if (info.error == ErrorCodeType_None){
                weakSelf.userModel = [UserModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
                [RCUserCacheManager sharedManager].currentUser.is_push = weakSelf.userModel.is_push;
                [RCUserCacheManager sharedManager].currentUser.is_push_stranger = weakSelf.userModel.is_push_stranger;
                [RCUserCacheManager sharedManager].currentUser.is_push_invite = weakSelf.userModel.is_push_invite;
                [RCUserCacheManager sharedManager].currentUser.is_push_system = weakSelf.userModel.is_push_system;
                [RCUserCacheManager sharedManager].currentUser.is_push_chat = weakSelf.userModel.is_push_chat;
                [RCUserCacheManager sharedManager].currentUser.is_push_dynamic = weakSelf.userModel.is_push_dynamic;
            }
        }];
    }];

}

-(void)setCell:(NormalTableViewCell *)cell index:(NSInteger )index{
    [RACObserve([RCUserCacheManager sharedManager], currentUser) subscribeNext:^(UserModel *x) {
        switch (index) {
            case 0:
                cell.isSwitch = [NSString boolValue:x.is_push];
                break;
            case 1:
                cell.isSwitch = [NSString boolValue:x.is_push_chat];
                break;
            case 2:
                cell.isSwitch = [NSString boolValue:x.is_push_dynamic];
                break;
            case 3:
                cell.isSwitch = [NSString boolValue:x.is_push_system];
                break;
            case 4:
                cell.isSwitch = [NSString boolValue:x.is_push_invite];
                break;
            case 5:
                cell.isSwitch = [NSString boolValue:x.is_push_stranger];
                break;
            default:
                break;
        }
    }];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}


@end
