//
//  UserNotificationCell.h
//  yaoqu
//
//  Created by ridchan on 2017/7/23.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MessageNotificationModel;
@interface UserNotificationCell : UITableViewCell

@property(nonatomic,strong) MessageNotificationModel *model;

@end
