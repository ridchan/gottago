//
//  DiscoverListApi.h
//  yaoqu
//
//  Created by ridchan on 2017/7/23.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface DiscoverListApi : BaseApi


-(id)initWithType:(DiscoverRankType)type page:(NSInteger)page size:(NSInteger)size;

@end
