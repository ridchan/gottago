//
//  TravelEnrollDetailHeader.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/15.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TravelModel.h"
@interface TravelEnrollDetailHeader : UIView

@property(nonatomic,strong) TravelModel *model;

@end
