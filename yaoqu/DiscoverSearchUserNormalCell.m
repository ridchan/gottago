//
//  DiscoverSearchUserNormalCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/6.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DiscoverSearchUserNormalCell.h"
#import "UserImageView.h"
#import "SexAgeView.h"
#import "SRActionSheet.h"
#import "RelationApi.h"
#import "RCUserCacheManager.h"

@interface DiscoverSearchUserNormalCell()

@property(nonatomic,strong) UserImageView *userImage;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) SexAgeView *sexView;
@property(nonatomic,strong) ImageButton *followBtn;

@end

@implementation DiscoverSearchUserNormalCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self commonInit];
    }
    return self;
}


-(void)setUserModel:(UserModel *)userModel{
    _userModel = userModel;
    self.userImage.userModel = userModel;
    self.nameLbl.text = userModel.username;
    self.sexView.sex = [userModel.sex integerValue];
    self.sexView.age = [userModel.age integerValue];
    [self setFollowStatus];
}


-(void)commonInit{
    [self.userImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.mas_top).offset(10);
        make.bottom.equalTo(self.mas_bottom).offset(-10);
        make.width.equalTo(self.userImage.mas_height);
    }];
    
    [self.nameLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userImage.mas_right).offset(5);
        make.centerY.equalTo(self.userImage.mas_centerY);
    }];
    
    
    [self.sexView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_right).offset(5);
        make.height.mas_equalTo(15);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
    }];
    
    [self.followBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.centerY.equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(70, 25));
    }];
}


-(UserImageView *)userImage{
    if (!_userImage) {
        _userImage = [[UserImageView alloc]init];
        [self.contentView addSubview:_userImage];
    }
    return _userImage;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        _nameLbl.font = SystemFont(14);
        [self.contentView addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(SexAgeView *)sexView{
    if (!_sexView) {
        _sexView = [[SexAgeView alloc]init];
        [self.contentView addSubview:_sexView];
    }
    return _sexView;
}

-(ImageButton *)followBtn{
    if (!_followBtn) {
        _followBtn = [[ImageButton alloc]init];
        _followBtn.titleLbl.text = @"关注";
        [_followBtn setFixImage:CGSizeMake(15, 15)];
        _followBtn.titleLbl.font = SystemFont(12);
        _followBtn.layer.cornerRadius = 3.0;
        _followBtn.titleLbl.textColor = AppOrange;
        _followBtn.layer.borderColor = AppOrange.CGColor;
        _followBtn.layer.borderWidth = 1.0;
        _followBtn.layer.masksToBounds = YES;
        [_followBtn addTarget:self action:@selector(followBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_followBtn];
    }
    return _followBtn;
}

-(void)setFollowStatus{
    _followBtn.hidden = [self isSelf];
    if (self.userModel.is_follow){
        _followBtn.titleLbl.text = @"已关注";
        _followBtn.titleLbl.textColor = AppGray;
        _followBtn.layer.borderColor = AppGray.CGColor;
        _followBtn.contentImage.image = [UIImage imageNamed:@"ic_check_gray"];
        _followBtn.backgroundColor = [UIColor clearColor];
    }else{
        _followBtn.contentImage.image = nil;
        _followBtn.titleLbl.text = @"关注";
        _followBtn.backgroundColor = AppOrange;
        _followBtn.contentImage.image = [[UIImage imageNamed:@"ic_chat_add"] imageToColor:[UIColor whiteColor]];
        _followBtn.titleLbl.textColor = [UIColor whiteColor];
        _followBtn.layer.borderColor = AppOrange.CGColor;
    }
}


-(BOOL)isSelf{
    NSString *member_id = [RCUserCacheManager sharedManager].currentUser.member_id;
    return  [_userModel.member_id isEqualToString:member_id];
}


-(void)followBtnClick:(id)sender{
    __block NSInteger state = 1;
    if (self.userModel.is_follow){
        [[SRActionSheet sr_actionSheetViewWithTitle:@"确定不再关注此人？" cancelTitle:@"取消" destructiveTitle:nil otherTitles:@[@"确定"] otherImages:nil selectSheetBlock:^(SRActionSheet *actionSheet, NSInteger index) {
            state = index;
        }] show];
        
        while (state == 1) {
            [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }
    }
    
    if (state < 0) return;
    
    RelationApi *api = [[RelationApi alloc]initWitObject:@{@"member_id":self.userModel.member_id,@"state":@(state)}];
    api.method = YTKRequestMethodPOST;
    
    WEAK_SELF;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            
            weakSelf.userModel.is_follow = !weakSelf.userModel.is_follow;
            [[RCUserCacheManager sharedManager] cacheUser:weakSelf.userModel.member_id].is_follow = weakSelf.userModel.is_follow;
            [weakSelf setFollowStatus];
        }
    }];
}



@end
