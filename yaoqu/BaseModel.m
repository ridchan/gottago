//
//  BaseModel.m
//  yaoqu
//
//  Created by ridchan on 2017/6/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel

//+(BOOL) isContainParent{
//    return YES;
//}
///**
// *  @author wjy, 15-01-26 14:01:01
// *
// *  @brief  设定表名
// *  @return 返回表名
// */
//+(NSString *)getTableName
//{
//    return NSStringFromClass([self class]);
//}
///**
// *  @author wjy, 15-01-26 14:01:22
// *
// *  @brief  设定表的单个主键
// *  @return 返回主键表
// */
//+(NSString *)getPrimaryKey
//{
//    return @"private_id";
//}


-(id)deepCopy{
    return [[self class] objectWithKeyValues:[self keyValues]];
}


-(id)objectWithClass:(NSString *)className{
    NSDictionary *dic = [self keyValues];
    return [NSClassFromString(className) objectWithKeyValues:dic];
}

@end
