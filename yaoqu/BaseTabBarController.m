//
//  BaseTabBarController.m
//  黄轩博客 blog.libuqing.com
//
//  Created by 黄轩 on 16/1/14.
//  Copyright © 2016年 IT小子. All rights reserved.
//

#import "BaseTabBarController.h"
#import "BaseNavigationController.h"
#import "BaseTableViewController.h"

#import "UITabBar+Badge.h"
#import "BaseViewController.h"

@interface BaseTabBarController ()

@property(nonatomic,strong) NSArray *dataAry;
@property(nonatomic,strong) UITabBarItem *lastItem;
@property(nonatomic,strong) UIButton *dynamicBtn;

@end

@implementation BaseTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate = self;
    [self setSVProgressHUD];
//    [self removeTabarTopLine];
    
    self.dataAry = @[@{@"image":@"tab_trip_gray",@"selectImage":@"tab_trip_blue"},
                     @{@"image":@"tab_discovery_gray",@"selectImage":@"tab_discovery_blue"},
                     @{@"image":@"",@"selectImage":@""},
                     @{@"image":@"tab_chat_gray",@"selectImage":@"tab_chat_blue"},
                     @{@"image":@"tab_mine_gray",@"selectImage":@"tab_mine_blue"}
                    ];
    
    
}

- (void)viewWillLayoutSubviews{
    float height = 50;
    CGRect tabFrame = self.tabBar.frame; //self.TabBar is IBOutlet of your TabBar
    tabFrame.size.height = height;
    tabFrame.origin.y = self.view.frame.size.height - height;
    self.tabBar.frame = tabFrame;
    
    self.tabBar.clipsToBounds = NO;
    [self.tabBar addSubview:self.dynamicBtn];
    
    
    [self.tabBar setShadowImage:[UIImage new]];
//    [self.tabBar setBackgroundImage:[[UIImage alloc]init]];
    
    self.lastItem = [self.tabBar.items objectAtIndex:self.selectedIndex];
    [self setSelectItem:self.lastItem];
    
    self.dynamicBtn.frame = CGRectMake(tabFrame.size.width / 2 - 25, -15, 50, 50);
    [self.tabBar addSubview:self.dynamicBtn];
    
}

-(void)dynamicBtnClick:(id)sender{
    [[RouteController sharedManager] openNewDynamicController];
}


-(UIButton *)dynamicBtn{
    if (!_dynamicBtn) {
        _dynamicBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_dynamicBtn setImage:[[UIImage imageNamed:@"ic_chat_add"] imageToColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        _dynamicBtn.backgroundColor = [UIColor orangeColor];
        _dynamicBtn.layer.masksToBounds = YES;
        _dynamicBtn.frame = CGRectMake((self.view.frame.size.width-50)/2, -20, 50, 50);
        _dynamicBtn.layer.cornerRadius = 25;
        [_dynamicBtn addTarget:self action:@selector(dynamicBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _dynamicBtn;
}

- (void)setSVProgressHUD {
    //SVProgressHUDMaskType 设置显示的样式
 
}


-(void)setSelectItem:(UITabBarItem *)item{
    NSInteger selectIndex = [self.tabBar.items indexOfObject:item];
    NSDictionary *selectDict = self.dataAry[selectIndex];
    item.image = [[UIImage imageNamed:selectDict[@"selectImage"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item.selectedImage = [[UIImage imageNamed:selectDict[@"selectImage"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.lastItem = item;
}


- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    if (self.lastItem){
        NSInteger lastIndex = [self.tabBar.items indexOfObject:self.lastItem];
        NSDictionary *lastDict = self.dataAry[lastIndex];
        self.lastItem.image = [[UIImage imageNamed:lastDict[@"image"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        self.lastItem.selectedImage = [[UIImage imageNamed:lastDict[@"image"]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }

    [self setSelectItem:item];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

}

-(BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{

    
    if ([tabBarController.viewControllers indexOfObject:viewController] == 2){
        return NO;
    }
    

    CATransition *animation = [CATransition animation];
    [animation setType:kCATransitionFade];
    [animation setDuration:0.25];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:
                                  kCAMediaTimingFunctionEaseIn]];
    [self.view.window.layer addAnimation:animation forKey:@"fadeTransition"];

    return YES;
}

#pragma 设置小红点数值
//设置指定tabar 小红点的值
- (void)setBadgeValue:(NSString *)badgeValue index:(NSInteger)index {
    if (index + 1 > self.viewControllers.count || index < 0) {
        //越界或者数据异常直接返回
        return;
    }
    BaseNavigationController *base = self.viewControllers[index];
    if (base.viewControllers.count == 0) {
        return;
    }
    UIViewController *vc = base.viewControllers[0];
    vc.tabBarItem.badgeValue = badgeValue.intValue > 0 ? badgeValue : nil;
}

#pragma 设置小红点显示或者隐藏

//显示小红点 没有数值
- (void)showBadgeWithIndex:(int)index {
    [self.tabBar showBadgeOnItemIndex:index];
}

//隐藏小红点 没有数值
- (void)hideBadgeWithIndex:(int)index {
    [self.tabBar hideBadgeOnItemIndex:index];
}

#pragma mark - 去掉tabBar顶部线条

//去掉tabBar顶部线条
- (void)removeTabarTopLine {
    CGRect rect = CGRectMake(0, 0, APP_WIDTH, APP_HEIGHT);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.tabBar setBackgroundImage:img];
    [self.tabBar setShadowImage:img];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
