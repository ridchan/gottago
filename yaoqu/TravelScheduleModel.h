//
//  TravelScheduleModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/18.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface TravelScheduleModel : BaseModel

@property(nonatomic,strong) NSString *travel_schedule_id;
@property(nonatomic,strong) NSString *travel_id;
@property(nonatomic,strong) NSString *group_id;
@property(nonatomic,strong) NSString *country_id;
@property(nonatomic,strong) NSString *pro_id;
@property(nonatomic,strong) NSString *city_id;
@property(nonatomic,strong) NSString *dis_id;
@property(nonatomic,strong) NSString *address;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *desc;
@property(nonatomic,strong) NSString *video;
@property(nonatomic,strong) NSString *video_image;
@property(nonatomic,strong) NSString *is_video_auto;
@property(nonatomic,strong) NSString *image;
@property(nonatomic,strong) NSString *share_image;
@property(nonatomic,strong) NSString *images;
@property(nonatomic,strong) NSString *detail;
@property(nonatomic,strong) NSString *room_qty;
@property(nonatomic,strong) NSString *room_surplus_show;
@property(nonatomic,strong) NSString *room_price;
@property(nonatomic,strong) NSString *room_is_child;
@property(nonatomic,strong) NSString *room_adult_qty;
@property(nonatomic,strong) NSString *room_child_qty;

@property(nonatomic,strong) NSString *room_child_age_free;
@property(nonatomic,strong) NSString *room_child_age_discount;
@property(nonatomic,strong) NSString *room_child_inner_discount;

@property(nonatomic,strong) NSString *wineshop_id;
@property(nonatomic,strong) NSString *day;
@property(nonatomic,strong) NSString *price;
@property(nonatomic,strong) NSString *total;
@property(nonatomic,strong) NSString *integral;
@property(nonatomic,strong) NSString *back_1_integral;
@property(nonatomic,strong) NSString *back_1_currency;
@property(nonatomic,strong) NSString *back_2_integral;
@property(nonatomic,strong) NSString *back_2_currency;
@property(nonatomic,strong) NSString *back_common_integral;
@property(nonatomic,strong) NSString *back_common_currency;
@property(nonatomic) long long time;
@property(nonatomic) long long update_time;
@property(nonatomic) long long cutoff_time;
@property(nonatomic) long long start_time;
@property(nonatomic) long long end_time;
@property(nonatomic,strong) NSString *surplus_adult;
@property(nonatomic,strong) NSString *surplus_room;
@property(nonatomic,strong) NSString *chatroom_id;
@property(nonatomic,strong) NSString *room_use;

@property(nonatomic) BOOL is_collect;
@property(nonatomic) NSInteger state;


@end
