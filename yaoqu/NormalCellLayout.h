//
//  NormalCellLayout.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NormalCellLayout : UICollectionViewFlowLayout

@property(nonatomic) NSInteger numOfRow;
@property(nonatomic) NSInteger numOfColumn;
@property(nonatomic) CGFloat cellHeight;

@end
