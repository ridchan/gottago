//
//  MemberRightModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface MemberRightModel : BaseModel

@property(nonatomic,strong) NSString *desc;

@end
