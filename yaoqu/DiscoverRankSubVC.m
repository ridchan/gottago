//
//  DiscoverRankSubVC.m
//  yaoqu
//
//  Created by ridchan on 2017/7/23.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DiscoverRankSubVC.h"
#import "DiscoverRankListCell.h"
#import "DiscoverListApi.h"
#import "DynamicRankListModel.h"
#import "RankingApi.h"

@interface DiscoverRankSubVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) NSArray *listType;

@end

@implementation DiscoverRankSubVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.page = 1;
    
    self.listType = @[@"week_like_hot",@"week_follow_hot",@"week_gift_hot",@"week_currency_hot"];
    [self.tableView registerClass:[DiscoverRankListCell class] forCellReuseIdentifier:@"Cell"];
    // Do any additional setup after loading the view.
    [self startRefresh];
    
    WEAK_SELF;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf startRefresh];
    }];
    
}

//-(NSString *)rankStirng{
//    NSArray *array = @[@"0",@"1",@"2",@"3"];
//    return array[self.rankType];
//}

-(void)startRefresh{
    WEAK_SELF;
    
    self.page = 1;
    NSDictionary *dict = @{@"type":@(self.rankType),
                           @"page":@(self.page),
                           @"size":@(self.size)
                           };
    
    
    
    
    [[[RankingApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        NSMutableArray *array = nil;
        if ([responseObj isKindOfClass:[NSDictionary class]]){
            array = [DynamicRankListModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:weakSelf.listType[weakSelf.rankType]]];
        }
 
        [weakSelf refreshComplete:info response:array];
    }];
}


-(void)startloadMore{
    WEAK_SELF;
    NSDictionary *dict = @{@"type":@(self.rankType),
                           @"page":@(++self.page),
                           @"size":@(self.size)
                           };

    [[[RankingApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        NSMutableArray *array = nil;
        if ([responseObj isKindOfClass:[NSDictionary class]]){
            array = [DynamicRankListModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:weakSelf.listType[weakSelf.rankType]]];
        }
        [weakSelf loadMoreComplete:info response:array];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    id model = self.datas[indexPath.section];
    return [tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[DiscoverRankListCell class] contentViewWidth:APP_WIDTH];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.datas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DiscoverRankListCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.rankType = self.rankType;
    cell.model = self.datas[indexPath.section];
    cell.level = indexPath.section;
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
