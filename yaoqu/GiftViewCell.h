//
//  GiftViewCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GiftView.h"

@interface GiftViewCell : UICollectionViewCell

@property(nonatomic,strong) NSArray *datas;

@property(nonatomic,assign) id<GiftViewDelegate>delegate;

@end
