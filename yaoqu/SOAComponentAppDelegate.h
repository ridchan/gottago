//
//  SOAComponentAppDelegate.h
//  shiyi
//
//  Created by 陳景雲 on 16/5/2.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SOAComponentAppDelegate : NSObject

+ (instancetype)instance ;

-(NSMutableArray*) services;

@end
