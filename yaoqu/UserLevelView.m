//
//  UserLevelView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/7.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserLevelView.h"
#import "UserImageView.h"
#import "SexAgeView.h"
#import "LevelView.h"
#import "RCUserCacheManager.h"

@interface UserLevelView()



@property(nonatomic,strong) UserImageView *userImage;
@property(nonatomic,strong) SexAgeView *sexView;
@property(nonatomic,strong) UIImageView *goldView;
@property(nonatomic,strong) LevelView *levelView;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) UIView *progressBg;
@property(nonatomic,strong) UILabel *descLbl;

@property(nonatomic,strong) UIView *line;
@property(nonatomic,strong) UIProgressView *progress;

@end

@implementation UserLevelView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
        [self bindUserInfo];
    }
    return self;
}

-(void)bindUserInfo{
    WEAK_SELF;
    
    [RACObserve([RCUserCacheManager sharedManager], currentUser) subscribeNext:^(UserModel *x) {
        weakSelf.nameLbl.text = x.username;
        weakSelf.levelView.level = x.level;
        weakSelf.sexView.sex = [x.sex integerValue];
        weakSelf.sexView.age = [x.age integerValue];
        weakSelf.userImage.userModel = x;
    }];
    
}


-(void)commonInit{
    
    self.backgroundColor = [UIColor whiteColor];
    
    
    [self.userImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15);
        make.top.equalTo(self.mas_top).offset(15);
        make.size.mas_equalTo(CGSizeMake(80, 80));
    }];
    
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userImage.mas_right).offset(10);
        make.top.equalTo(self.mas_top).offset(15);
    }];
    
    
    [self.sexView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.nameLbl.mas_bottom).offset(5);
        make.height.mas_equalTo(15);
        make.width.mas_equalTo(30);
    }];
    
    
    [self.levelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.sexView.mas_right).offset(5);
        make.centerY.equalTo(self.sexView.mas_centerY);
        make.height.mas_equalTo(45);
        make.width.mas_equalTo(45);
    }];
    
    
    [self.goldView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_right).offset(5);
        make.top.equalTo(self.nameLbl.mas_top);
        make.bottom.equalTo(self.nameLbl.mas_bottom);
        make.width.equalTo(self.nameLbl.mas_height);
    }];
    
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.userImage.mas_bottom).offset(15);
        make.height.mas_equalTo(0.5);
    }];
    

    
    [self.progressBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.right.equalTo(self.mas_right).offset(-20);
        make.top.equalTo(self.line.mas_bottom).offset(20);
        make.height.mas_equalTo(10);
    }];
    
    [self.progressBg addSubview:self.progress];
    
    [self.progress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.progressBg).insets(UIEdgeInsetsMake(2, 2, 2, 2));
    }];
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.progressBg.mas_left);
        make.right.equalTo(self.progressBg.mas_right);
        make.top.equalTo(self.progressBg.mas_bottom).offset(10);
    }];
    
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */


-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc]init];
        [_nameLbl setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        _nameLbl.font = [UIFont systemFontOfSize:14];
        _nameLbl.text = @"我的帐号";
        _nameLbl.textColor = [UIColor blackColor];
        [self addSubview:_nameLbl];
    }
    return _nameLbl;
}


-(UserImageView *)userImage{
    if (!_userImage) {
        _userImage = [[UserImageView alloc]init];
        [self addSubview:_userImage];
    }
    return _userImage;
}

-(SexAgeView *)sexView{
    if (!_sexView) {
        _sexView = [[SexAgeView alloc]init];
        [self addSubview:_sexView];
    }
    return _sexView;
}

-(UIImageView *)goldView{
    if (!_goldView) {
        _goldView = [[UIImageView alloc]init];
        _goldView.contentMode = UIViewContentModeScaleAspectFit;
        _goldView.image = [UIImage imageNamed:@"ic_user_level_1"];
        [self addSubview:_goldView];
    }
    return _goldView;
}

-(LevelView *)levelView{
    if (!_levelView) {
        _levelView = [[LevelView alloc]init];
        [self addSubview:_levelView];
    }
    return _levelView;
}

-(UIView *)line{
    if (!_line) {
        _line = [[UIView alloc]init];
        _line.backgroundColor = AppLineColor;
        [self addSubview:_line];
    }
    return _line;
}

-(UIProgressView *)progress{
    if (!_progress) {
        _progress = [[UIProgressView alloc]init];
        _progress.trackTintColor = [UIColor whiteColor];
        _progress.progressTintColor = AppBlue;
        _progress.layer.cornerRadius = 5.0;
        _progress.layer.masksToBounds = YES;
        [_progress setProgress:0.7];
//        [self addSubview:_progress];
    }
    return _progress;
}

-(UIView *)progressBg{
    if (!_progressBg) {
        _progressBg = [[UIView alloc]init];
        _progressBg.layer.borderColor = AppBlue.CGColor;
        _progressBg.layer.borderWidth = 1.0;
        _progressBg.layer.cornerRadius = 5;
        _progressBg.layer.masksToBounds = YES;
        [self addSubview:_progressBg];
    }
    return _progressBg;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.font = DetailFont;
        _descLbl.textColor = AppGray;
        _descLbl.text = @"距升级还差100经验";
        [self addSubview:_descLbl];
    }
    return _descLbl;
}

@end
