//
//  GaoDeLocationVC.h
//  QBH
//
//  Created by 陳景雲 on 2017/3/2.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseViewController.h"
@class AMapPOI;
@protocol GaoDeLocationVCDelegate <NSObject>

-(void)didSelectAddress:(AMapPOI *)poi city:(NSString *)city;

@end

@interface GaoDeLocationVC : BaseViewController

@property(nonatomic,strong) NSString *selectCity;
@property(nonatomic,strong) AMapPOI *selectPOI;
@property(nonatomic,weak) id<GaoDeLocationVCDelegate>delegate;

@end
