//
//  UserFollowApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserFollowApi.h"

@implementation UserFollowApi{
    NSString *_member_id;
    BOOL _is_follow;
}

-(id)initWithMember:(NSString *)member_id isFollow:(BOOL)is_Follow{
    if (self = [super init]) {
        _member_id = member_id;
        _is_follow = is_Follow;
    }
    return self;
}

-(NSString *)requestUrl{
    return _is_follow ? UserFollowAddUrl : UserFollowCancelUrl;
}

-(id)requestArgument{
    NSMutableDictionary *dict = [self baseConfig];
    [dict setValue:_member_id forKey:@"member_id"];
    return dict;
}

@end
