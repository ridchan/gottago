//
//  UserInfoGetApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserInfoGetApi.h"

@implementation UserInfoGetApi

-(id)initWithMemberID:(NSString *)member_id{
    if (self = [super init]) {
        [self.params setValue:member_id forKey:@"member_id"];
    }
    return self;
}


-(NSString *)requestUrl{
    return UserInfoGetUrl;
}



-(NSInteger)cacheTimeInSeconds{
    return 60 * 5;
}




@end
