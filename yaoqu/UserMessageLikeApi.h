//
//  UserMessageLikeApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface UserMessageLikeApi : BaseApi

-(id)initWithPage:(NSInteger)page sie:(NSInteger)size;
-(id)initWithMessageLikeID:(NSString *)message_like_id;

@end
