//
//  DynamicReportApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicReportApi.h"

@implementation DynamicReportApi{
    NSString *_dynamic_id;
    NSString *_comment_id;
    NSString *_desc;
}

-(id)initWithDynamic:(NSString *)dynamic_id comment:(NSString *)comment_id desc:(NSString *)desc{
    if (self = [super init]) {
        [self.params setValue:_dynamic_id forKey:@"dynamic_id"];
        [self.params setValue:_comment_id forKey:@"comment_id"];
        [self.params setValue:_desc forKey:@"desc"];
    }
    return self;
}

-(NSString *)requestUrl{
    return DynamicReportUrl;
}



@end
