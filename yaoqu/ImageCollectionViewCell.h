//
//  ImageCollectionViewCell.h
//  QBH
//
//  Created by 陳景雲 on 2017/2/18.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ImageCollectionViewCell;

@protocol ImageCollectionViewCellDelegate <NSObject>

@optional
-(void)deleteCell:(ImageCollectionViewCell *)cell;

@end

@interface ImageCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UIButton *delBtn;

@property(nonatomic,weak) id<ImageCollectionViewCellDelegate>delegate;

@end
