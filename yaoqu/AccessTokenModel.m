//
//  AccessTokenModel.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/1.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "AccessTokenModel.h"

@implementation AccessTokenModel

-(void)setExpires:(NSInteger)expires{
    _expires = expires;
    _exprieDate = [NSDate dateWithTimeIntervalSinceNow:_expires];
}


+(BOOL) isContainParent{
    return YES;
}
/**
 *  @author wjy, 15-01-26 14:01:01
 *
 *  @brief  设定表名
 *  @return 返回表名
 */
+(NSString *)getTableName
{
    return @"AccessTokenModel";
}
/**
 *  @author wjy, 15-01-26 14:01:22
 *
 *  @brief  设定表的单个主键
 *  @return 返回主键表
 */
+(NSString *)getPrimaryKey
{
    return @"private_id";
}



@end
