//
//  RCAlbumListVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/26.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "RCAlbumListVC.h"

#import "LLAssetListController.h"
#import "RCAssetListVC.h"
#import "LLAssetManager.h"
#import "LLUtils.h"
#import "RCImagePickerVC.h"


#define TABLE_CELL_HEIGHT 57

@interface RCImagePickerTableViewCell : UITableViewCell

@end

@implementation RCImagePickerTableViewCell

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect imgFrame = self.imageView.frame;
    imgFrame.origin.x = 0;
    self.imageView.frame = imgFrame;
    
    CGRect textFrame = self.textLabel.frame;
    self.textLabel.frame = CGRectMake(CGRectGetMaxX(imgFrame)+7, CGRectGetMinY(textFrame), CGRectGetWidth(textFrame), CGRectGetHeight(textFrame));
}

@end

@interface RCAlbumListVC (){
    CGSize posterImageSize;
}

@end

@implementation RCAlbumListVC



- (instancetype)init
{
    self = [super init];
    if (self) {
        posterImageSize = CGSizeMake(TABLE_CELL_HEIGHT, TABLE_CELL_HEIGHT);
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"照片";
    
    [self setupViews];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}


- (void)setupViews {
//    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(doCancel)];
//    
//    UIBarButtonItem *spaceItem = [[UIBarButtonItem alloc]   initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
//    spaceItem.width = NAVIGATION_BAR_RIGHT_MARGIN;
//    
//    self.navItem.rightBarButtonItems = @[spaceItem, item];
//    
//    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:nil action:nil];
//    self.navItem.backBarButtonItem = backItem;
    
    [self setRightItemWithTitle:LS(@"取消") selector:@selector(doCancel)];
    
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 10, 0, 0);
    self.tableView.rowHeight = TABLE_CELL_HEIGHT;
}

- (void)doCancel {
    [(RCImagePickerVC *)(self.navigationController) didCancelPickingImages];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refresh {
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [LLAssetManager sharedAssetManager].allAssetsGroups.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *ID = @"Cell";
    RCImagePickerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    
    if (cell == nil) {
        cell = [[RCImagePickerTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:ID];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    
    LLAssetsGroupModel *groupModel = [LLAssetManager sharedAssetManager].allAssetsGroups[indexPath.row];
    NSString *countStr = [NSString stringWithFormat:@"(%lu)", (unsigned long)groupModel.numberOfAssets];
    NSString *str = [NSString stringWithFormat:@"%@  %@", groupModel.assetsGroupName, countStr];
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:str];
    
    NSRange range = [str rangeOfString:countStr];
    [attributeString addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range: range];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:17] range: NSMakeRange(0, range.location-2)];
    
    cell.textLabel.attributedText = attributeString;
    
    cell.imageView.image = [groupModel syncFetchPosterImageWithPointSize:posterImageSize];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    LLAssetsGroupModel *model = [LLAssetManager sharedAssetManager].allAssetsGroups[indexPath.row];
    
    RCAssetListVC *assetListVC = [[RCAssetListVC alloc] init];
    assetListVC.groupModel = model;
    
    [self.navigationController pushViewController:assetListVC animated:YES];
    
}



@end
