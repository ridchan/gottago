//
//  UploadCollectionCell.m
//  yaoqu
//
//  Created by ridchan on 2017/7/5.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UploadCollectionCell.h"
#import "UIView+Extension.h"
@implementation UploadCollectionCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createViews];
    }
    return self;
}

-(void)createViews{
    self.imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.imageView.clipsToBounds = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:self.imageView];
    
    self.playImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_play"]];
    self.playImage.frame = CGRectMake(0, 0, 30, 30);
    self.playImage.center = self.imageView.center;
    [self addSubview:self.playImage];

}

-(void)setUploadObj:(UploadObject *)uploadObj{
    _uploadObj = uploadObj;
    self.imageView.image = _uploadObj.image;
    self.playImage.hidden = _uploadObj.videoUrl == nil;
}

@end
