//
//  NormalTableViewController.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/23.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NormalTableViewController.h"
#import "MsgModel.h"

@interface NormalTableViewController ()<UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>


@property(nonatomic) UITableViewStyle tableStyle;


@property(nonatomic) ErrorCodeType errorCodeType;
@property(nonatomic) NSInteger animationIndex;
@property(nonatomic,strong) NSDictionary *emptyImgs;


@end

@implementation NormalTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.page = 1;
    self.size = 20;
    self.animationIndex = -1;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    self.isloading = YES;
    
    self.emptyImgs = @{@"image":@"empty_gift",
                       @"image":@"empty_coin",
                       @"MineCollectionVC":@"empty_collect",
                       @"image":@"empty_footprint",
                       @"MineFollowVC":@"empty_follow",
                       };

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)startRefresh{
    self.isloading = YES;
    if (self.datas.count == 0) [self.tableView reloadData];
    if (!self.tableView.mj_header) {
        WEAK_SELF;
        MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf startRefresh];
        }];
        
        self.tableView.mj_header = header;
    }
}

-(void)refreshComplete:(MsgModel *)info response:(id)obj{
    
    [self.tableView.mj_header endRefreshing];
    self.isloading = NO;
    self.datas = obj;
    self.errorCodeType = info ? info.error : ErrorCodeType_UnkownError;
    [self.tableView reloadData];
    
    WEAK_SELF;
    if (self.datas.count > 0){
        
        if (self.datas.count >= self.size){
        
            self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                [weakSelf startloadMore];
            }];
        }else{
            self.tableView.mj_footer = nil;
        }
    }else{
        self.tableView.mj_footer = nil;
    }
}

-(void)startloadMore{
    self.isloading = YES;
}

-(void)loadMoreComplete:(MsgModel *)info response:(id)obj{
    self.isloading = NO;
    
    if ([obj count] > 0){
        [self.datas addObjectsFromArray:obj];
        [self.tableView reloadData];
        [self.tableView.mj_footer endRefreshing];
    }else{
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }

}

-(UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView{
    if (self.isloading) return nil;
    NSString *className = NSStringFromClass([self class]);
    NSLog(@"class %@",className);
    if ([self.emptyImgs objectForKey:className]) {
        return [UIImage imageNamed:self.emptyImgs[className]];
    }
    return nil;
}

-(NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    
    
    if (self.isloading) return nil;
    
    NSString *className = NSStringFromClass([self class]);
    if ([self.emptyImgs objectForKey:className]) return nil;
    
    NSString *title = @"";
    
    if (self.errorCodeType == ErrorCodeType_NetWorkError){
        title = @"网络出现错误";
    }else if (self.errorCodeType == ErrorCodeType_UnkownError){
        title = @"未知错误";
    }else if(self.errorCodeType == ErrorCodeType_AuthenticationFailed){
        title = @"没有权限或令牌过期";
    }else{
        if (self.datas.count == 0) {
            title = @"没有数据";
        }
    }
    
    NSAttributedString *att = [[NSAttributedString alloc]initWithString:title attributes:@{NSFontAttributeName:SystemFont(14)}];
    
    return att;
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView {
    if (!self.isloading) return nil;
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [activityView startAnimating];
    return activityView;
}


//-(void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button{
//    [self startRefresh];
//}

-(void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view{
    self.page = 1;
    [self startRefresh];
}

-(void)emptyDataSetWillDisappear:(UIScrollView *)scrollView{
//    [self.tableView.layer addAnimation:[self animation] forKey:nil];
}

-(CAAnimationGroup *)animation{
    
    
    //缩放动画
    CABasicAnimation *scaleAnim = [CABasicAnimation animationWithKeyPath:@"transform"];
    scaleAnim.fromValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.95, 0.95, 1.0)];
    scaleAnim.toValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    scaleAnim.removedOnCompletion = YES;
    
    //移动动画
    CABasicAnimation *moveAnim = [CABasicAnimation animationWithKeyPath:@"transform.translation.y"];
    moveAnim.fromValue = [NSNumber numberWithFloat:100.0];
    moveAnim.toValue = [NSNumber numberWithFloat:0];
    moveAnim.removedOnCompletion = YES;

    
    //透明动画
    CABasicAnimation *opacityAnim = [CABasicAnimation animationWithKeyPath:@"alpha"];
    opacityAnim.fromValue = [NSNumber numberWithFloat:0.9];
    opacityAnim.toValue = [NSNumber numberWithFloat:1.0];
    opacityAnim.removedOnCompletion = YES;
    
    //动画组
    CAAnimationGroup *animGroup = [CAAnimationGroup animation];
    animGroup.animations = [NSArray arrayWithObjects:scaleAnim,  opacityAnim, nil];
    animGroup.duration = 0.35;

    return animGroup;
    
}

//-(NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state{
//    NSAttributedString *att = [[NSAttributedString alloc]initWithString:@"点击重新加载"];
//    return att;
//}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.emptyDataSetSource = self;
        _tableView.emptyDataSetDelegate = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_tableView];
        
    }
    return _tableView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 7;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return nil;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    
//    if ([tableView numberOfSections] == 1){
//        if (indexPath.row > self.animationIndex){
//            NSInteger left = indexPath.row % 10;
//            cell.layer.transform = CATransform3DMakeTranslation(0, 30, 0);
//            
//            
//            [UIView animateWithDuration:(0.35 + left * 0.05) animations:^{
//                cell.layer.transform = CATransform3DIdentity;
//            }];
//            self.animationIndex = indexPath.row;
//        }
//        
//    }else if ([tableView numberOfSections]  > 1) {
//        if (indexPath.section > self.animationIndex){
//            
//            NSInteger left = indexPath.section % 10;
//            
//            cell.layer.transform = CATransform3DMakeTranslation(0, 30, 0);
//            
//            [UIView animateWithDuration:(0.35 + left * 0.05) animations:^{
//                cell.layer.transform = CATransform3DIdentity;
//            }];
//             self.animationIndex = indexPath.section;
//        }
//    }
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
