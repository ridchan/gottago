//
//  RedPacketView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/25.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "RedPacketView.h"
#import "RCTextView.h"
@interface RedPacketView()

@property(nonatomic,strong) UIButton *luckyBtn;
@property(nonatomic,strong) UIButton *normalBtn;

@property(nonatomic,strong) UIView *grayline;
@property(nonatomic,strong) UIView *orangeline;

@property(nonatomic,strong) UILabel *amountLbl;
@property(nonatomic,strong) UILabel *numLbl;
@property(nonatomic,strong) UITextField *amountFld;
@property(nonatomic,strong) UITextField *numFld;

@property(nonatomic,strong) RCTextView *descFld;


@end

@implementation RedPacketView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)commonInit{
    [self.luckyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.top.equalTo(self.mas_top);
        make.height.mas_equalTo(40);
        make.right.mas_equalTo(self.mas_centerX);
    }];
    
    [self.normalBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_centerX);
        make.top.equalTo(self.mas_top);
        make.height.mas_equalTo(40);
        make.right.mas_equalTo(self.mas_right);
    }];
    
    [self.grayline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.luckyBtn.mas_bottom);
        make.height.mas_equalTo(1.0);
    }];
    
    [self.orangeline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.width.equalTo(self.mas_width).multipliedBy(0.5);
        make.top.equalTo(self.luckyBtn.mas_bottom);
        make.height.mas_equalTo(1.0);
    }];
    
    
}

-(UIButton *)luckyBtn{
    if (!_luckyBtn) {
        _luckyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_luckyBtn setTitle:@"拼手气红包" forState:UIControlStateNormal];
        [_luckyBtn setTitleColor:AppGray forState:UIControlStateNormal];
        [_luckyBtn setTitleColor:AppOrange forState:UIControlStateNormal];
        [self addSubview:_luckyBtn];
    }
    return _luckyBtn;
}


-(UIButton *)normalBtn{
    if (!_normalBtn) {
        _normalBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_normalBtn setTitle:@"普通红包" forState:UIControlStateNormal];
        [_normalBtn setTitleColor:AppGray forState:UIControlStateNormal];
        [_normalBtn setTitleColor:AppOrange forState:UIControlStateNormal];
        [self addSubview:_luckyBtn];
    }
    return _normalBtn;
}


-(UIView *)grayline{
    if (!_grayline) {
        _grayline = [[UIView alloc]init];
        _grayline.backgroundColor = AppLineColor;
        [self addSubview:_grayline];
    }
    return _grayline;
}

-(UIView *)orangeline{
    if (!_orangeline) {
        _orangeline = [[UIView alloc]init];
        _orangeline.backgroundColor = AppOrange;
        [self addSubview:_orangeline];
    }
    return _orangeline;
}

-(UILabel *)amountLbl{
    if(!_amountLbl){
        _amountLbl = [ControllerHelper autoFitLabel];
        _amountLbl.text = @"红包金额";
        [self addSubview:_amountLbl];
    }
    return _amountLbl;
}

-(UILabel *)numLbl{
    if (!_numLbl) {
        _numLbl = [ControllerHelper autoFitLabel];
        _numLbl.text = @"红包个数";
        [self addSubview:_numLbl];
    }
    return _numLbl;
}

-(UITextField *)amountFld{
    if (!_amountFld) {
        _amountFld = [[UITextField alloc]init];
        _amountFld.placeholder = @"输入金额";
        [self addSubview:_amountFld];
    }
    return _amountFld;
}

-(UITextField *)numFld{
    if (!_numFld) {
        _numFld = [[UITextField alloc]init];
        _numFld.placeholder = @"输入个数";
        [self addSubview:_numFld];
    }
    return _numFld;
}

-(RCTextView *)descFld{
    if (!_descFld) {
        _descFld = [[RCTextView alloc]init];
        _descFld.placeholder = @"恭喜发财";
        [self addSubview:_descFld];
    }
    return _descFld;
}

@end
