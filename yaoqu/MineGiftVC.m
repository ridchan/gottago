//
//  MineGiftVC.m
//  yaoqu
//
//  Created by ridchan on 2017/8/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineGiftVC.h"
#import "GiftCell.h"
#import "GiftApi.h"
#import "GiftPostModel.h"
#import "NormalCellLayout.h"
#import "CommonConfigManager.h"
@interface MineGiftVC ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) NSMutableArray *datas;
@property(nonatomic,strong) UIButton *receiveBtn;
@property(nonatomic,strong) UIButton *sendBtn;

@end

@implementation MineGiftVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self commonInit];
    
    [self setRightItemWithTitle:@"礼物商城" selector:@selector(storeBtnClick:)];
    [(RightBarButtonItem *)self.navigationItem.rightBarButtonItem setTitleColor:AppOrange];
    
    [[CommonConfigManager sharedManager] getCommonConfig];
    [RACObserve([CommonConfigManager sharedManager], hostModel) subscribeNext:^(HostModel *x) {
        self.datas = (NSMutableArray *)x.gift_depot;
        [self.collectionView reloadData];
    }];
    
//    [self btnClick:self.receiveBtn];
    
    // Do any additional setup after loading the view.
}


-(void)storeBtnClick:(id)sender{
    [self pushViewControllerWithName:@"MineGiftStoreVC"];
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        NormalCellLayout *layout = [[NormalCellLayout alloc]init];
        layout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10);
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        layout.numOfColumn = 4;
        layout.cellHeight = 100;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;

        [_collectionView registerClass:[GiftCell class] forCellWithReuseIdentifier:@"Cell"];
        [self.view addSubview:_collectionView];

    }
    return _collectionView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)commonInit{
    self.title = LS(@"我的礼物");
    
//    [self setRightItemWithIcon:[UIImage imageNamed:@"ic_question"] selector:nil];
    

    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top).offset(0);
        make.bottom.equalTo(self.view.mas_bottom).offset(-50);
    }];
    
    
    
    [self.receiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_centerX).offset(-0.5);
        make.bottom.equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(50);
    }];
    
    [self.sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_centerX).offset(0.5);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(50);
    }];
    
    
}

-(void)btnClick:(UIButton *)button{
    if (button.tag == 0){
        
        [[RouteController sharedManager] openClassVC:@"MineGiftReceiveVC" withObj:@(GiftSearchType_Receive)];
//        [self.receiveBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
//        [self.sendBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }else{
        [[RouteController sharedManager] openClassVC:@"MineGiftReceiveVC" withObj:@(GiftSearchType_Send)];
//        [self.receiveBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        [self.sendBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    }
    
//    WEAK_SELF;
//    [[[GiftApi alloc]initWitObject:@{@"type":@(button.tag)}] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
//        if (info.error == ErrorCodeType_None) {
//            weakSelf.datas = [GiftModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
//            [weakSelf.collectionView reloadData];
//        }else{
//            [LLUtils showActionSuccessHUD:info.message];
//        }
//    }];
}

-(UIButton *)receiveBtn{
    if (!_receiveBtn) {
        _receiveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _receiveBtn.tag = 0;
        [_receiveBtn setTitle:@"收礼物明细" forState:UIControlStateNormal];
        [_receiveBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _receiveBtn.backgroundColor = [UIColor whiteColor];
        [_receiveBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_receiveBtn];
    }
    return _receiveBtn;
}

-(UIButton *)sendBtn{
    if (!_sendBtn) {
        _sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _sendBtn.tag = 1;
        [_sendBtn setTitle:@"送出的礼物" forState:UIControlStateNormal];
        [_sendBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        _sendBtn.backgroundColor = [UIColor whiteColor];
        [_sendBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.sendBtn];
    }
    return _sendBtn;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GiftCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    cell.model = self.datas[indexPath.item];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    WEAK_SELF;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"用该礼物" message:@"使用礼物转化为旅游币" preferredStyle:UIAlertControllerStyleAlert];
    [self addCancelActionTarget:alert title:@"取消" color:AppBlack];
    
    [self addActionTarget:alert title:@"确定" color:AppOrange action:^(UIAlertAction *action) {
        GiftModel *model = weakSelf.datas[indexPath.item];
        
        GiftApi *api = [[GiftApi alloc]initWitObject:@{@"gift_id":model.gift_id}];
        api.method = YTKRequestMethodDELETE;
        [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            if (info.error == ErrorCodeType_None) {
                [[CommonConfigManager sharedManager] getCommonConfig];
            }
        }];
        
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


// 取消按钮
-(void)addCancelActionTarget:(UIAlertController*)alertController title:(NSString *)title color:(UIColor *)color
{
    UIAlertAction *action = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }];
    [action setValue:color forKey:@"_titleTextColor"];
    [alertController addAction:action];
}
//添加对应的title    这个方法也可以传进一个数组的titles  我只传一个是为了方便实现每个title的对应的响应事件不同的需求不同的方法
- (void)addActionTarget:(UIAlertController *)alertController title:(NSString *)title color:(UIColor *)color action:(void(^)(UIAlertAction *action))actionTarget
{
    UIAlertAction *action = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        actionTarget(action);
    }];
    [action setValue:color forKey:@"_titleTextColor"];
    [alertController addAction:action];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
