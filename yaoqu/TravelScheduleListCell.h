//
//  TravelScheduleListCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TravelScheduleModel;

@interface TravelScheduleListCell : UITableViewCell

@property(nonatomic,strong) TravelScheduleModel *model;

@end
