//
//  GiftBuyApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/12.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface GiftBuyApi : BaseApi

-(id)initWithGift:(NSString *)gift_id quantify:(NSInteger)qty;

@end
