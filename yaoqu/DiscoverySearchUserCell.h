//
//  DiscoverySearchUserCell.h
//  yaoqu
//
//  Created by ridchan on 2017/9/4.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NormalTableViewController.h"

@interface SearchUserCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic) BOOL isMoreCell;

@end

@protocol DiscoverySearchUserCellDelegate <NSObject>

-(void)discoverySearchUserCell:(id)obj moreBtnClick:(id)obj2;

@end

@interface DiscoverySearchUserCell : UITableViewCell

@property(nonatomic,strong) NSArray *members;
@property(nonatomic,weak) id<DiscoverySearchUserCellDelegate>delegate;

@end
