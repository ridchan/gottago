//
//  AddressProvinceVC.m
//  yaoqu
//
//  Created by ridchan on 2017/7/5.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "AddressProvinceVC.h"
#import "CommonConfigManager.h"
#import "AddressCityVC.h"
#import "RCUserCacheManager.h"
@interface AddressProvinceVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) NSArray *addressArray;

@end

@implementation AddressProvinceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"省");
    
    [[CommonConfigManager sharedManager] getAddresss];
    
    self.addressArray = [CommonConfigManager sharedManager].addressArray;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.addressArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    ProModel *pro = [self.addressArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = pro.pro;
    if (pro.city_list.count > 0) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }

    
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ProModel *pro = [self.addressArray objectAtIndex:indexPath.row];
    
    
    if (pro.city_list.count > 0) {
        AddressCityVC *vc = [[AddressCityVC alloc]init];
        vc.proModel = pro;
        vc.model = self.model;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        CityModel *city = [[CityModel alloc]init];
        city.city_name = pro.pro;
        city.city_id = pro.pro_id;
        
        [RCUserCacheManager sharedManager].currentCity = city;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
