//
//  UserHomePageGetApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/25.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface UserHomePageGetApi : BaseApi

-(id)initWithMember:(NSString *)member_id;

@end
