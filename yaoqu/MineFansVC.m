//
//  MineFansVC.m
//  yaoqu
//
//  Created by ridchan on 2017/7/17.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineFansVC.h"
#import "UserFansListApi.h"
#import "UserNormalCell.h"
#import "RelationApi.h"

@interface MineFansVC ()<UITableViewDelegate,UITableViewDataSource>



@end

@implementation MineFansVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"我的粉丝");
    
    [self startRefresh];
    // Do any additional setup after loading the view.
}

-(void)startRefresh{
    WEAK_SELF;
    self.page = 1;
    
    NSDictionary *dict = @{@"page":@(self.page),
                           @"size":@(self.size),
                           @"type":@"fans"
                           };
    [[[RelationApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [weakSelf refreshComplete:info response:[UserModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]]];
    }];

}

-(void)startloadMore{
    WEAK_SELF;
    NSDictionary *dict = @{@"page":@(++self.page),
                           @"size":@(self.size),
                           @"type":@"fans"
                           };

    [[[RelationApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        NSArray *array = [UserModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        [weakSelf loadMoreComplete:info response:array];
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserNormalCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ImageCell"];
    if (cell == nil) {
        cell = [UserNormalCell normalCellType:UserCellType_Normal identify:@"ImageCell"];
    }
    
    UserModel *model = [self.datas objectAtIndex:indexPath.row];
    
    cell.nameLbl.text = model.username;
    
    cell.sexView.sex = [model.sex integerValue];
    cell.sexView.age = [model.age integerValue];
    cell.sexView.content = nil;
    cell.userImage.userModel = model;
    
    
    return cell;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
