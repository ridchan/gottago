//
//  DynamicCommentModel.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/29.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicCommentModel.h"

@implementation DynamicCommentModel

+(NSDictionary *)objectClassInArray{
    return @{@"comment_data":@"DynamicCommentModel"};
}

@end
