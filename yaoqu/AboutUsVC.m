//
//  AboutUsVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/2.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "AboutUsVC.h"

@interface AboutUsVC ()

@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UILabel *versionLbl;
@property(nonatomic,strong) UILabel *companyLbl;
@property(nonatomic,strong) UILabel *descLbl;
@end

@implementation AboutUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = LS(@"关于我们");
    
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(100);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.height.mas_equalTo(80);
    }];
    
    [self.versionLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.imageView.mas_bottom).offset(10);
    }];
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(20);
        make.right.equalTo(self.view.mas_right).offset(-20);
        make.top.equalTo(self.versionLbl.mas_bottom).offset(10);
    }];
    
    
    [self.companyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom).offset(-10);
    }];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc]init];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        _imageView.image = [UIImage imageNamed:@"icon_gottago"];
        [self.view addSubview:_imageView];
    }
    return _imageView;
}

-(UILabel *)versionLbl{
    if (!_versionLbl) {
        NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        _versionLbl = [ControllerHelper autoFitLabel];
        _versionLbl.textColor = AppGray;
        _versionLbl.font = SystemFont(14);
        _versionLbl.textAlignment = NSTextAlignmentCenter;
        _versionLbl.text = [NSString stringWithFormat:@"版本：v %@",appVersion];
        [self.view addSubview:_versionLbl];
    }
    return _versionLbl;
}

-(UILabel *)companyLbl{
    if (!_companyLbl) {
        _companyLbl = [ControllerHelper autoFitLabel];
        _companyLbl.textColor = AppGray;
        _companyLbl.font = SystemFont(14);
        _companyLbl.numberOfLines = 0;
        _companyLbl.textAlignment = NSTextAlignmentCenter;
        _companyLbl.text = @"Copyright©2017-2030\n广州市要趣旅游有限公司\n";
        [self.view addSubview:_companyLbl];
    }
    return _companyLbl;
}


-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.textColor = AppGray;
        _descLbl.font = SystemFont(14);
        _descLbl.numberOfLines = 0;
        _descLbl.textAlignment = NSTextAlignmentLeft;
        _descLbl.text = @"        GottaGo 成双成对 是一个基于大数据智能推荐、全新互动模式的旅游社交App,成双成对 根据用户的个人资料、位置、兴趣爱好等信息，推送身边与你匹配的人，帮助用户结识互有好感的新朋友,并提供高端策划的短程或国外经常的线路，赋予陌生男女或朋友基友擦出更多的乐趣，成双成对 也是一名资深的旅行爱好者记录自己旅途的工具，并且永久存储,丰富的礼物系统让网络红人提升自身更高的价值。";
        [self.view addSubview:_descLbl];
    }
    return _descLbl;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
