//
//  SexAgeView.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SexAgeView : UIView

@property(nonatomic,strong) UIImageView *sexImg;
@property(nonatomic,strong) UILabel *ageLbl;
@property(nonatomic,strong) UILabel *contentLbl;


@property(nonatomic) SexType sex;
@property(nonatomic) NSInteger age;
@property(nonatomic,strong) NSString *content;

@end
