//
//  TravelHomePageWebVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/10.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelHomePageWebVC.h"
#import <WebKit/WebKit.h>
#import "CommonConfigManager.h"
#import "RCUserCacheManager.h"
#import "YQWebViewVC.h"
#import "TravelScheduleDetailVC.h"
#import "NetVideoPreviewVC.h"
@interface TravelHomePageWebVC ()<WKUIDelegate,WKNavigationDelegate,WKScriptMessageHandler,UIScrollViewDelegate>

@property(nonatomic,strong) WKWebView *webView;
@property(nonatomic,strong) UIActivityIndicatorView *indicatorView;

@end

@implementation TravelHomePageWebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    self.navigationItem.title = @"";
    
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    [self wr_setNavBarBackgroundAlpha:0];
    
    
    NSString *link = [[CommonConfigManager sharedManager].api_host stringByAppendingPathComponent:@"travel.html"];
    link = [NSString stringWithFormat:@"%@&access_token=%@",link,
            [RCUserCacheManager sharedManager].currentToken.access_token];
//    
//    [self setLeftItemWithIcon:[[UIImage imageNamed:@"ic_location_black"] imageToColor:[UIColor whiteColor]] title:[RCUserCacheManager sharedManager].currentCity.city_name selector:@selector(addressBtnClick:)];
//    [(LeftBarButtonItem *)self.navigationItem.leftBarButtonItem setTitleColor:[UIColor whiteColor]];
    
    [self deleteCookieInLike:link];
    [self setCookieForLink:link];
    
    
    
    
    
    NSURL *url = [NSURL URLWithString:link];
    
    //    NSString *oldAgent = [self.webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    //
    //
    //    NSString *customUserAgent = nil;
    //
    //
    //    if ([oldAgent rangeOfString:@"gottago"].location == NSNotFound){
    //        customUserAgent = [NSString stringWithFormat:@"%@( gottago )",oldAgent];
    //    }else{
    //        customUserAgent = oldAgent;
    //    }
    //
    //    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"UserAgent":customUserAgent}];
    
    
    
    //    NSURL *url = [NSURL URLWithString:@"http://m.xiaoniubang.com/demo/test/agent.html"];
    
    NSURLRequest *request2 = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.f];
    
    self.webView.hidden = YES;
    self.indicatorView.hidden = NO;
    [self.indicatorView startAnimating];
    [self.webView loadRequest:request2];
    
    
    
    
    
    // Do any additional setup after loading the view.
}

-(void)addressBtnClick:(id)sender{
    [self pushViewControllerWithName:@"AddressProvinceVC"];
}


-(void)closeMe{
    if (self.navigationController) {
        if ([self.navigationController.viewControllers firstObject] == self) {
            [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }else{
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIActivityIndicatorView *)indicatorView{
    if (!_indicatorView) {
        _indicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        _indicatorView.hidesWhenStopped = YES;
        [self.view addSubview:_indicatorView];
        [_indicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view.mas_centerX);
            make.centerY.equalTo(self.view.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(20, 20));
        }];
    }
    return _indicatorView;
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.y <= 0) {
        scrollView.contentOffset = CGPointMake(0, 0);
    }else if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height){
        scrollView.contentOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.frame.size.height);
    }
    
    
    
//    if (scrollView.contentOffset.y < 64) {
//        
//        [(LeftBarButtonItem *)self.navigationItem.leftBarButtonItem setImageColor:[UIColor whiteColor]];
//        
//        [self wr_setNavBarBackgroundAlpha:scrollView.contentOffset.y / 64.0];
//        [self wr_setStatusBarStyle:UIStatusBarStyleDefault];
//    }else{
//        
//        [(LeftBarButtonItem *)self.navigationItem.leftBarButtonItem setImageColor:AppGray];
//        
//        
//        [self wr_setNavBarBackgroundAlpha:1];
//        [self wr_setNavBarTintColor:[UIColor whiteColor]];
//        [self wr_setNavBarTitleColor:[UIColor whiteColor]];
//        [self wr_setStatusBarStyle:UIStatusBarStyleLightContent];
//    }
    
}

-(WKWebView *)webView{
    if (!_webView) {
        _webView = [[WKWebView alloc]init];
        //        _webView.delegate = self;
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 9.0)
            _webView.customUserAgent = @" gottago ";
        _webView.navigationDelegate = self;
        _webView.UIDelegate = self;
        _webView.scrollView.delegate = self;
        [self.view addSubview:_webView];
    }
    return _webView;
}

#pragma mark -

// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    
}
// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation{
    
}
// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    self.webView.hidden = NO;
    [self.indicatorView stopAnimating];
    
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation{
    self.webView.hidden = NO;
    [self.indicatorView stopAnimating];
}


- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    //下面这句话一定不能少一少就报错
    
    
    NSLog(@"run hear %@",navigationAction.request.URL.absoluteString);
    
    NSString *link = navigationAction.request.URL.absoluteString;
    
    if ([link hasPrefix:@"app://open/link/"]){
//        YQWebViewVC *vc = [[YQWebViewVC alloc]init];
//        vc.link = [link substringFromString:@"open/link/"];
//        [self.navigationController pushViewController:vc animated:YES];
        
        TravelScheduleDetailVC *vc = [[TravelScheduleDetailVC alloc]init];
        TravelScheduleModel *model = [[TravelScheduleModel alloc]init];
        model.travel_schedule_id = [[link substringFromString:@"id="] substringToString:@"&"];
        vc.model = model;
        
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if ([link hasPrefix:@"app://open/returnShare"]){
        
    }else if ([link hasPrefix:@"http"] && [link hasSuffix:@"mp4"]){
        NetVideoPreviewVC *vc = [[NetVideoPreviewVC alloc]init];
        vc.link = link;
        [self presentViewController:vc animated:YES completion:NULL];
        decisionHandler(WKNavigationActionPolicyCancel);
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
}


- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler{
    //下面这句话一定不能少一少就报错
    decisionHandler(WKNavigationResponsePolicyAllow);
    NSLog(@"run there");
}

// 接收到服务器跳转请求之后调用
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation{
    NSLog(@"%@",navigation.description);
}



-(void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    NSLog(@"cmd %@",NSStringFromSelector(_cmd));
    NSLog(@"message body %@",message.body);
}


#pragma mark -


-(void)setCookieForLink:(NSString *)link{
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:[RCUserCacheManager sharedManager].currentToken.access_token forKey:NSHTTPCookieValue];
    [dict setValue:@"access_token" forKey:NSHTTPCookieName];
    [dict setValue:@".gottago917.com" forKey:NSHTTPCookieDomain];
    [dict setValue:@"/" forKey:NSHTTPCookiePath];
    [dict setValue:[NSDate dateWithTimeIntervalSinceNow:3600 * 24] forKey:NSHTTPCookieExpires];
    [dict setValue:[NSURL URLWithString:link] forKey:NSHTTPCookieOriginURL];
    //    [dict setValue:[NSURL URLWithString:link] forKey:NSHTTPCookieCommentURL];
    
    
    
    
    NSArray *cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:
                        [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"access_token=%@",[RCUserCacheManager sharedManager].currentToken.access_token]
                                                    forKey:@"Set-Cookie"]
                                                              forURL:[NSURL URLWithString:link]];
    
    
    NSLog(@"%@========",[NSHTTPCookie requestHeaderFieldsWithCookies:cookies]);
    
    NSArray *array = @[[cookies firstObject]];
    
    
    
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookies:array
                                                       forURL:[NSURL URLWithString:link]
                                              mainDocumentURL:nil];
    
    
    
}


- (void)deleteCookieInLike:(NSString *)link{
    NSHTTPCookie *cookie;
    
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    NSArray *cookieAry = [cookieJar cookiesForURL: [NSURL URLWithString:link]];
    
    for (cookie in cookieAry) {
        NSLog(@"delete %@ = %@",cookie.name,cookie.value);
        [cookieJar deleteCookie: cookie];
        
    }
}/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
