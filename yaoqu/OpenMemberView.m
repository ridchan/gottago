//
//  OpenMemberView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/11.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "OpenMemberView.h"
#import "RCUserCacheManager.h"
#import "CommonConfigManager.h"
#import "RechargeModel.h"
#import "MemberApi.h"
#import "LLUtils.h"
@implementation OpenMemberCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        UIImageView *checkImage = [[UIImageView alloc]init];
        checkImage.contentMode = UIViewContentModeScaleAspectFit;
        checkImage.frame = CGRectMake(0, 0, 25, 40);
        self.accessoryView = checkImage;
    }
    return self;
}


-(UILabel *)detailLbl{
    if (!_detailLbl) {
        _detailLbl = [[UILabel alloc]init];
        [_detailLbl setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        _detailLbl.font = SystemFont(14);
        _detailLbl.textColor = AppGray;
        
        [self.contentView addSubview:_detailLbl];
        
        [_detailLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.equalTo(self.accessoryView.mas_left).offset(-10);
            make.right.equalTo(self.contentView.mas_right).offset(-10);
            make.top.equalTo(self.contentView.mas_top);
            make.bottom.equalTo(self.contentView.mas_bottom);
        }];
        
    }
    return _detailLbl;
}


-(void)setBCheck:(BOOL)bCheck{
    _bCheck = bCheck;
    UIImageView *imageView = (UIImageView *)self.accessoryView;
    imageView.image = !_bCheck ? [UIImage imageNamed:@"ic_not_checked"] : [UIImage imageNamed:@"ic_checked_yellow"];
    self.detailLbl.textColor = _bCheck ? [UIColor orangeColor] : AppGray;
}

@end

@interface OpenMemberView()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) UIButton *comfirnBtn;
@property(nonatomic,strong) NSArray *datas;
@property(nonatomic) NSInteger selectIndex;

@end

@implementation OpenMemberView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self commonInit];
        self.selectIndex = 0;
    }
    return self;
}


-(void)commonInit{
    self.datas = [CommonConfigManager sharedManager].hostModel.member_val;
    self.model = [self.datas firstObject];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.mas_top);
        make.height.mas_equalTo(120);
    }];
    
    [self.comfirnBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.right.equalTo(self.mas_right).offset(-10);
        make.top.equalTo(self.tableView.mas_bottom).offset(10);
        make.bottom.equalTo(self.mas_bottom).offset(-10);
        make.height.mas_equalTo(50);
    }];
}


-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.scrollEnabled = NO;
        [self addSubview:_tableView];
    }
    return _tableView;
}



-(UIButton *)comfirnBtn{
    if (!_comfirnBtn) {
        _comfirnBtn = [NormalButton normalButton:@"确认购买"];
        
        [_comfirnBtn addTarget:self action:@selector(comfirnBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_comfirnBtn];
    }
    return _comfirnBtn;
}


-(void)comfirnBtnClick:(id)sender{
    RechargeModel *model = [[RechargeModel alloc]init];
    model.title_desc = @"开通会员";
    model.title_value = self.model.desc;
    model.pay_total = self.model.pay_total;
    model.val = self.model.val;
    model.type = [NSString intValue:RechargeType_Money];
    model.actionType = [NSString intValue:RechargeType_OpenMember];
    model.bOpenMember = YES;
    
    [[RouteController sharedManager] openClassVC:@"TripCoinPayVC" withObj:model];
    
//    MemberApi *api = [[MemberApi alloc]initWitObject:@{@"val":self.model.val}];
//    api.method = YTKRequestMethodPUT;
//    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
//        if (info.error == ErrorCodeType_NotEnoughMoney) {
//            
//        }
////        [LLUtils showTextHUD:info.message];
//    }];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MemberValueModel *model = self.datas[indexPath.row];
    self.model = model;
    self.selectIndex = indexPath.row;
    [tableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OpenMemberCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[OpenMemberCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    MemberValueModel *model = self.datas[indexPath.row];
    
    cell.textLabel.text = model.desc;
    cell.detailLbl.text = [NSString stringWithFormat:@"%@%@",MoneySign,model.price];
    cell.bCheck = self.selectIndex == indexPath.row;
    
    return cell;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
