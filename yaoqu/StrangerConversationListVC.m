//
//  StrangerConversationListVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/9.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "StrangerConversationListVC.h"
#import "LLTableViewCell.h"
#import "LLTableViewCellData.h"
#import "LLChatManager.h"
#import "ConversationListCell.h"
#import "LLChatViewController.h"
#import "GiftView.h"
#import "LLSearchViewController.h"
#import "LLNavigationController.h"
#import "LLChatSearchController.h"
#import "ConversationStrangerCell.h"


@interface StrangerConversationListVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView *tableView;

@end

@implementation StrangerConversationListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self commonInit];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)commonInit{
    self.title = @"陌生人";
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    
    
    [RACObserve(self, conversationModels) subscribeNext:^(id x) {
            [self.tableView reloadData];
        
    }];
}


-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        [_tableView registerClass:[ConversationListCell class] forCellReuseIdentifier:@"Cell"];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}


#pragma mark -
#pragma mark tableview

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.conversationModels.count ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    ConversationListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.conversationModel = self.conversationModels[indexPath.row];
    return cell;
    
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    ConversationListCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell markAllMessageAsRead];
    [[RouteController sharedManager] chatWithConversationModel:cell.conversationModel];
    
    
}

#pragma mark - 左滑显示删除和已读

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    ConversationListCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:LS(@"删除") handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        BOOL result = [[LLChatManager sharedManager] deleteConversation:cell.conversationModel];
        if (result) {
            [self.conversationModels removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:@[indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            PostNotification(LLMessageRefreshStatusChangedNotification, nil);
        }
        
    }];
    
    //    UITableViewRowAction *setReadAction;
    //    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    //    [dict addEntriesFromDictionary:cell.conversationModel.sdk_conversation.ext];
    //    if ([dict objectForKey:@"isTop"]) {
    //        setReadAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:LS(@"取消置顶") handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
    //            [dict removeObjectForKey:@"isTop"];
    //            [cell.conversationModel.sdk_conversation setExt:dict];
    //        }];
    //
    //    }else{
    //        setReadAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:LS(@"置顶") handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
    //            [dict setObject:@"1" forKey:@"isTop"];
    //            [cell.conversationModel.sdk_conversation setExt:dict];
    //        }];
    //    }
    
    //    if (cell.conversationModel.unreadMessageNumber > 0) {
    //        setReadAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:LS(@"移至顶部") handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
    //            [cell markAllMessageAsRead];
    //            [self setUnreadMessageCount];
    //
    //
    //            [tableView setEditing:NO animated:YES];
    //        }];
    //    }else {
    //        setReadAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:LS(@"移至顶部") handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
    ////            [cell markMessageAsNotRead];
    //            [self setUnreadMessageCount];
    //            [tableView setEditing:NO animated:YES];
    //        }];
    //    }
    //
    
    return @[deleteAction];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
