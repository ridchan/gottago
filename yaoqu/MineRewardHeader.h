//
//  MineRewardHeader.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineRewardHeader : UIView

@property(nonatomic,strong) NSDictionary *dict;

@property(nonatomic,strong) NSString *YearAndMonth;

@end
