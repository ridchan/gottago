//
//  HomePageHeaderView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/27.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "HomePageHeaderView.h"
#import "UserHeaderTipBar.h"
#import "UserImageEditor.h"
#import "SexAgeView.h"
#import "SRActionSheet.h"
#import "DynamicUploadManager.h"
#import "MemberApi.h"
#import "RCUserCacheManager.h"
#import "HomePageApi.h"


@interface HomePageHeaderView()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(nonatomic,strong) UIImageView *backgroundImage;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) UILabel *locationLbl;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) SexAgeView *sexView;
@property(nonatomic,strong) UserHeaderTipBar *tipBar;
@property(nonatomic,strong) UIButton *editBtn;
@property(nonatomic,strong) UILabel *tagLbl;


@property(nonatomic,strong) UserImageEditor *imageEditor;

@property(nonatomic,strong) UIImagePickerController *picker;

@end

@implementation HomePageHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
        
        [RACObserve(self, userModel) subscribeNext:^(UserModel *x) {
            self.nameLbl.text = x.username;
            self.locationLbl.text = x.city_name;
            self.descLbl.text = x.desc;
            self.sexView.sex = [x.sex integerValue];
            self.sexView.age = [x.age integerValue];
            self.sexView.content = x.constellation;
            self.tagLbl.hidden = [x.sex_orientation length] == 0 && [x.make_friends length] == 0;
            self.tagLbl.text = [NSString stringWithFormat:@" %@ %@ ",x.sex_orientation,x.make_friends];
            
            if (x.background){
                [self.backgroundImage setImageName:x.background placeholder:self.backgroundImage.image];
            }else{
                [self.backgroundImage setImageName:x.background placeholder:[UIImage imageNamed:@"ic_user_bg"]];
            }
            NSString *follow = @"0";
            NSString *fans = @"0";
            NSString *dynamic = @"0";
            
            if (x.info){
                follow = x.info.follow;
                fans = x.info.fans;
                dynamic = x.info.dynamic;
            }
            
            
                                       
            self.tipBar.dataSource = @[@{@"title":LS(@"关注"),@"count":follow,@"vc":@"MineRelationVC",@"param":@(RelationType_Follow)},
                                       @{@"title":LS(@"粉丝"),@"count":fans,@"vc":@"MineRelationVC",@"param":@(RelationType_Fans)},
                                       @{@"title":LS(@"动态"),@"count":dynamic,@"vc":@"DynamicListVC"}
                                       ];

            
            self.imageEditor.datas = [x.images mutableCopy];
            
            CGFloat nameHeight = [x.username heightWithFont:[UIFont systemFontOfSize:14] inWidth:(APP_WIDTH - 20)];
            CGFloat descHeight = [x.desc heightWithFont:[UIFont systemFontOfSize:12] inWidth:(APP_WIDTH - 20)];
            CGFloat height = 200 + 10 + nameHeight + 10 + descHeight + 10 + 50 + self.imageEditor.height ;
//            self.height =  height;
            
            [self setFrameSizeHeight:height];
            self.frameHeight = [NSString stringWithFormat:@"%f",height];
        }];
    }
    return self;
}

-(void)commonInit{
    self.backgroundColor = [UIColor whiteColor];
    [self.backgroundImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.top.equalTo(self.mas_top);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(200);
    }];
    
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.backgroundImage.mas_bottom).offset(10);
    }];
    
    [self.sexView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_right).offset(5);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
        make.height.mas_equalTo(15);
    }];
    
    [self.tagLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.sexView.mas_right).offset(2);
        make.centerY.equalTo(self.sexView.mas_centerY);
    }];

    
    [self.locationLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
    }];
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.nameLbl.mas_bottom).offset(10);
        make.right.lessThanOrEqualTo(self.mas_right).offset(-10);
    }];
    
    [self.tipBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(50);
        make.top.equalTo(self.descLbl.mas_bottom).offset(10);
    }];
    
    [self.imageEditor mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom);
        make.top.equalTo(self.tipBar.mas_bottom);
//        make.height.mas_equalTo(APP_WIDTH / 4);
    }];
    
    [self.editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-5);
        make.bottom.equalTo(self.backgroundImage.mas_bottom).offset(-5);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
}

#pragma mark -
#pragma mark lazy controller 


-(UserImageEditor *)imageEditor{
    if (!_imageEditor) {
        _imageEditor = [[UserImageEditor alloc]init];
        _imageEditor.hideAction = YES;
        
        _imageEditor.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self addSubview:_imageEditor];
    }
    
    return _imageEditor;
}


-(UIImageView *)backgroundImage{
    if (!_backgroundImage) {
        _backgroundImage = [[UIImageView alloc]init];
        _backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
        _backgroundImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _backgroundImage.image = [UIImage imageNamed:@"ic_user_bg"];
        _backgroundImage.clipsToBounds = YES;
        [self addSubview:_backgroundImage];
    }
    return _backgroundImage;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc]init];
        _nameLbl.font = UserNameFont;
        [_nameLbl setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal | UILayoutConstraintAxisVertical];
        [self addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(UILabel *)locationLbl{
    if (!_locationLbl) {
        _locationLbl = [[UILabel alloc]init];
        _locationLbl.textColor = AppGray;
        _locationLbl.font = DetailFont;
        [_locationLbl setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal | UILayoutConstraintAxisVertical];
        [self addSubview:_locationLbl];
    }
    
    return _locationLbl;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.font = DetailFont;
        _descLbl.textColor = AppGray;
        _descLbl.numberOfLines = 0;
        [self addSubview:_descLbl];
    }
    return _descLbl;
}

-(SexAgeView *)sexView{
    if (!_sexView) {
        _sexView = [[SexAgeView alloc]init];
        [self addSubview:_sexView];
    }
    return _sexView;
}

-(UserHeaderTipBar *)tipBar{
    if (!_tipBar) {
        _tipBar = [[UserHeaderTipBar alloc]init];
        [self addSubview:_tipBar];
    }
    return _tipBar;
}

-(UIButton *)editBtn{
    if (!_editBtn) {
        _editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_editBtn setImage:[UIImage imageNamed:@"ic_user_add"] forState:UIControlStateNormal];
        [_editBtn addTarget:self action:@selector(editBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_editBtn];
    }
    return _editBtn;
}

-(UILabel *)tagLbl{
    if (!_tagLbl) {
        _tagLbl = [ControllerHelper autoFitLabel];
        _tagLbl.layer.masksToBounds = YES;
        _tagLbl.layer.cornerRadius = 3.0;
        _tagLbl.backgroundColor = RGB16(0xe9eaeb);;
        _tagLbl.textColor = AppOrange;
        _tagLbl.font = SystemFont(10);
        [self addSubview:_tagLbl];
    }
    return _tagLbl;
}


#pragma mark -

-(UIImagePickerController *)picker{
    if (!_picker) {
        _picker = [[UIImagePickerController alloc]init];
        _picker.modalPresentationStyle= UIModalPresentationOverFullScreen;
        //        _picker.modalPresentationCapturesStatusBarAppearance = YES;
        _picker.delegate = self;
    }
    return _picker;
}


-(void)editBtnClick:(id)sender{
    WEAK_SELF;
    
    
    [[SRActionSheet sr_actionSheetViewWithTitle:nil cancelTitle:LS(@"取消") destructiveTitle:nil otherTitles:@[LS(@"相机"),LS(@"相册")] otherImages:nil selectSheetBlock:^(SRActionSheet *actionSheet, NSInteger index) {
        if (index < 0) return ;
        
        weakSelf.picker.sourceType =  index == 0 ? UIImagePickerControllerSourceTypeCamera :UIImagePickerControllerSourceTypePhotoLibrary;
        weakSelf.picker.editing = NO;
        [[[RouteController sharedManager] currentNav] presentViewController:weakSelf.picker animated:YES completion:NULL];
        
    }] show];
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *image =  [info objectForKey:UIImagePickerControllerOriginalImage];
    
    
    


    
    self.backgroundImage.image = image;
    [[DynamicUploadManager sharedManager] putImage:image block:^(id responseObj) {
        
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:responseObj forKey:@"background"];
        HomePageApi *api = [[HomePageApi alloc]initWitObject:dict];
        api.method = YTKRequestMethodPOST;
        
        [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            
            if (info.error == ErrorCodeType_None) {
                
                [[RCUserCacheManager sharedManager] renewAccount];
                
            }else{
                
                
            }
            
        }];
    }];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        

    }];
}


@end
