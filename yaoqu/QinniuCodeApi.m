//
//  QinniuCodeApi.m
//  yaoqu
//
//  Created by ridchan on 2017/6/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "QinniuCodeApi.h"

@implementation QinniuCodeApi{
    NSString *_type;
}

-(id)initWithType:(NSString *)type{
    if (self = [super init]) {
        _type = type;
    }
    return self;
}


-(NSString *)requestUrl{
    return QiuniuCodeUrl;
}

-(id)requestArgument{
    if (_type) {
        return @{@"type":_type};
    }else{
        return nil;
    }
    
}




@end
