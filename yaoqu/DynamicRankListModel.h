//
//  DynamicRankListModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserModel.h"

@interface ImageModel : BaseModel

@property(nonatomic,strong) NSString *image;

@end

@interface DynamicRankListModel : UserModel



@property(nonatomic,strong) NSArray *dynamic_images;
@property(nonatomic,strong) NSArray *recent_image_data;
@property(nonatomic,strong) NSString *like;
@property(nonatomic,strong) NSString *follow;
@property(nonatomic,strong) NSString *qty;



@end
