//
//  DiscoverRankListCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DiscoverRankListCell.h"
#import "UserImageView.h"
#import "ImageCollectionViewCell.h"
#import "UserFollowApi.h"
#import "RCUserCacheManager.h"
#import "RelationApi.h"
#import "SRActionSheet.h"
#import "NetImagePreviewVC.h"

@interface DiscoverRankListCell()<UICollectionViewDelegate,UICollectionViewDataSource,NetImagePreviewVCDelegate>


@property(nonatomic,strong) NSArray *imgDatas;

@property(nonatomic,strong) UIImageView *topImage;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) UILabel *addressLbl;
@property(nonatomic,strong) UserImageView *userImage;
@property(nonatomic,strong) UIImageView *likeImage;
@property(nonatomic,strong) UILabel *likeLbl;
@property(nonatomic,strong) ImageButton *followBtn;
@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) UIImageView *levelImage;

@end

@implementation DiscoverRankListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self commonInit];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}


-(void)commonInit{
    self.topImage.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .topSpaceToView(self.contentView, 10)
    .widthIs(30)
    .heightIs(30);
    

    
    self.nameLbl.sd_layout
    .topSpaceToView(self.topImage, 5)
    .centerXEqualToView(self.contentView)
    .heightIs(30)
    .autoWidthRatio(0);
    
    [self.nameLbl setSingleLineAutoResizeWithMaxWidth:200];
    
    self.followBtn.sd_layout
    .widthIs(70)
    .heightIs(25)
    .centerYEqualToView(self.topImage)
    .rightSpaceToView(self.contentView, 10);
    
    self.addressLbl.sd_layout
    .centerXEqualToView(self.contentView)
    .topSpaceToView(self.nameLbl, 0)
    .widthIs(200)
    .heightIs(15);
    
    self.userImage.sd_layout
    .centerXEqualToView(self.contentView)
    .topSpaceToView(self.addressLbl, 10)
    .widthIs(60)
    .heightIs(60);
    
    self.likeLbl.sd_layout
    .centerXEqualToView(self.contentView).offset(10)
    .topSpaceToView(self.userImage, 0)
    .heightIs(25)
    .autoWidthRatio(0);
    
    [self.likeLbl setSingleLineAutoResizeWithMaxWidth:200];
    
    self.likeImage.sd_layout
    .rightSpaceToView(self.likeLbl, 5)
    .centerYEqualToView(self.likeLbl)
    .widthIs(18)
    .heightIs(18);
    
    
    self.collectionView.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .rightSpaceToView(self.contentView, 10)
    .topSpaceToView(self.likeLbl, 20)
    .heightIs(30);
    
    [self setupAutoHeightWithBottomView:self.collectionView bottomMargin:10];
    
}

-(void)setLevel:(NSInteger)level{
    _level = level;
    if (_level < 3) {
        self.topImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_no_%d",_level + 1]];
    }else{
        self.topImage.image = nil;
    }
}

#pragma mark -
#pragma mark lazy layout

-(UIImageView *)levelImage{
    if (!_levelImage) {
        _levelImage = [[UIImageView alloc]init];
        _levelImage.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_levelImage];
    }
    return _levelImage;
}

-(UIImageView *)topImage{
    if (!_topImage) {
        _topImage = [[UIImageView alloc]init];
        _topImage.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_topImage];
    }
    return _topImage;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        [self.contentView addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(UILabel *)addressLbl{
    if (!_addressLbl) {
        _addressLbl = [ControllerHelper autoFitLabel];
        _addressLbl.font = SystemFont(12);
        _addressLbl.textColor = AppGray;
        _addressLbl.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_addressLbl];
    }
    return _addressLbl;
}

-(UserImageView *)userImage{
    if (!_userImage) {
        _userImage = [[UserImageView alloc]init];
        [self.contentView addSubview:_userImage];
    }
    return _userImage;
}

-(ImageButton *)followBtn{
    if (!_followBtn) {
        _followBtn = [[ImageButton alloc]init];
        _followBtn.titleLbl.text = @"关注";
        [_followBtn setFixImage:CGSizeMake(15, 15)];
        _followBtn.titleLbl.font = SystemFont(12);
        _followBtn.layer.cornerRadius = 3.0;
        _followBtn.titleLbl.textColor = AppOrange;
        _followBtn.layer.borderColor = AppOrange.CGColor;
        _followBtn.layer.borderWidth = 1.0;
        _followBtn.layer.masksToBounds = YES;
        [_followBtn addTarget:self action:@selector(followBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_followBtn];
    }
    return _followBtn;
}

-(UIImageView *)likeImage{
    if (!_likeImage) {
        _likeImage = [[UIImageView alloc]init];
        _likeImage.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_likeImage];
    }
    return _likeImage;
}

-(UILabel *)likeLbl{
    if (!_likeLbl) {
        _likeLbl = [ControllerHelper autoFitLabel];
        _likeLbl.textColor = AppGray;
        _likeLbl.font = SystemFont(14);
        [self.contentView addSubview:_likeLbl];
    }
    return _likeLbl;
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:[self layout]];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.scrollEnabled = NO;
        _collectionView.userInteractionEnabled = YES;
        [_collectionView registerClass:[ImageCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
        [self.contentView addSubview:_collectionView];
    }
    return _collectionView;
}


-(void)setModel:(DynamicRankListModel *)model{
    
    
    _model = model;
    
    self.userImage.userModel = [model objectWithClass:@"UserModel"];
    self.nameLbl.text = _model.username;
    self.addressLbl.text = _model.home_city_name;
    [self setFollowStatus];
    
    switch (_rankType) {
        case DiscoverRankType_Like:
            self.likeImage.image = [UIImage imageNamed:@"ic_rank_like"];
            self.likeLbl.text = [NSString stringWithFormat:@"%0.0f 赞",[_model.qty floatValue]];
            break;
        case DiscoverRankType_Follow:
            self.likeImage.image = [UIImage imageNamed:@"ic_rank_follow"];
            self.likeLbl.text = [NSString stringWithFormat:@"%0.0f 人关注",[_model.qty floatValue]];
            break;
        case DiscoverRankType_Gift:
            self.likeImage.image = [UIImage imageNamed:@"ic_rank_gift"];
            self.likeLbl.text = [NSString stringWithFormat:@"%0.0f 旅游币",[_model.qty floatValue]];
            break;
        case DiscoverRankType_Currency:
            self.likeImage.image = [UIImage imageNamed:@"ic_rank_coin"];
            self.likeLbl.text = [NSString stringWithFormat:@"%0.0f 旅游币",[_model.qty floatValue]];
            break;
        default:
            break;
    }
    
//    if ([model.dynamic_images isKindOfClass:[NSString class]])

    self.imgDatas = nil;
    if ([model.recent_image_data count] == 0){
        NSInteger count = model.images.count;
        if (count > 1){
            self.imgDatas = [model.images subarrayWithRange:NSMakeRange(1, count - 1)];
        }
    }else{
        self.imgDatas = model.recent_image_data;
    }
    
    [self.collectionView reloadData];
    
    CGFloat row = ceilf(self.imgDatas.count / 3.0);
    CGFloat rowHeight = (APP_WIDTH - 30) / 3 + 10;
    CGFloat height = self.imgDatas.count > 0  ?
    row * rowHeight :
    0;
    
    
    self.collectionView.sd_resetLayout
    .leftSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 0)
    .topSpaceToView(self.likeLbl, 0)
    .heightIs(height);
}

-(BOOL)isSelf{
    NSString *member_id = [RCUserCacheManager sharedManager].currentUser.member_id;
    return  [_model.member_id isEqualToString:member_id];
}

-(void)setFollowStatus{
    _followBtn.hidden = [self isSelf];
    if (self.model.is_follow){
        _followBtn.titleLbl.text = @"已关注";
        _followBtn.titleLbl.textColor = AppGray;
        _followBtn.layer.borderColor = AppGray.CGColor;
        _followBtn.contentImage.image = [UIImage imageNamed:@"ic_check_gray"];
        _followBtn.backgroundColor = [UIColor clearColor];
    }else{
        _followBtn.contentImage.image = nil;
        _followBtn.titleLbl.text = @"关注";
        _followBtn.backgroundColor = AppOrange;
        _followBtn.contentImage.image = [[UIImage imageNamed:@"ic_chat_add"] imageToColor:[UIColor whiteColor]];
        _followBtn.titleLbl.textColor = [UIColor whiteColor];
        _followBtn.layer.borderColor = AppOrange.CGColor;
    }
}


-(void)followBtnClick:(id)sender{
    __block NSInteger state = 1;
    if (self.model.is_follow){
        [[SRActionSheet sr_actionSheetViewWithTitle:@"确定不再关注此人？" cancelTitle:@"取消" destructiveTitle:nil otherTitles:@[@"确定"] otherImages:nil selectSheetBlock:^(SRActionSheet *actionSheet, NSInteger index) {
            state = index;
        }] show];
        
        while (state == 1) {
            [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }
    }
    
    if (state < 0) return;
    
    RelationApi *api = [[RelationApi alloc]initWitObject:@{@"member_id":self.model.member_id,@"state":@(state)}];
    api.method = YTKRequestMethodPOST;
    
    WEAK_SELF;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            
            weakSelf.model.is_follow = !weakSelf.model.is_follow;
            [[RCUserCacheManager sharedManager] cacheUser:weakSelf.model.member_id].is_follow = weakSelf.model.is_follow;
            [weakSelf setFollowStatus];
        }
    }];
}



#pragma mark -
#pragma mark collection view


-(UICollectionViewFlowLayout *)layout{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.minimumLineSpacing = 5;
    layout.minimumInteritemSpacing = 5;
    layout.sectionInset = UIEdgeInsetsMake(5, 10, 5, 10);
    CGFloat width = (APP_WIDTH - 30) / 3;
    layout.itemSize = CGSizeMake(width, width);
    
    //    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    
    return layout;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.imgDatas.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    [cell.imageView setImageName:[self.imgDatas[indexPath.item] fixImageString:DynamicImageFix] placeholder:nil];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NetImagePreviewVC *vc = [[NetImagePreviewVC alloc]init];
    vc.delegate = self;
    vc.showIndex = indexPath.item;
    [vc show];
}

#pragma mark -

-(NSUInteger)numOfPreviewer:(id)obj{
    return self.imgDatas.count;
}

-(UIImageView *)previewImage:(id)obj atIndex:(NSInteger)index{
    ImageCollectionViewCell *cell = (ImageCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
    if (cell == nil)
        return nil;
    else{
        return cell.imageView;
    }
    
}

-(NSString *)originalImage:(id)obj atIndex:(NSInteger)index{
    return self.imgDatas[index];
    
}


@end
