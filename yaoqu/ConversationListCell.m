//
//  ConversationListCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "ConversationListCell.h"
#import "UserImageView.h"
#import "SexAgeView.h"
#import "LLChatManager.h"
@interface ConversationListCell()

@property(nonatomic,strong) UserImageView *userImg;
@property(nonatomic,strong) SexAgeView *ageView;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UILabel *dateLbl;

@property(nonatomic,strong) UIView *yellowView;

@end

@implementation ConversationListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self commonInit];
        
        [self rac_prepareForReuseSignal];
        
        WEAK_SELF;
        [RACObserve(self,conversationModel.model.autoid) subscribeNext:^(NSString *x) {
            NSLog(@"auto id %@",x);
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.userImg.userModel = weakSelf.conversationModel.model;
                weakSelf.nameLbl.text = weakSelf.conversationModel.model.username;
                weakSelf.ageView.age  = [weakSelf.conversationModel.model.age integerValue];
                weakSelf.ageView.sex = [weakSelf.conversationModel.model.sex integerValue];
                weakSelf.ageView.contentLbl.text = @"";
            });
            
        }];
    }
    return self;
}


-(void)commonInit{
    [self.userImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.mas_top).offset(10);
        make.bottom.equalTo(self.mas_bottom).offset(-10);
        make.width.equalTo(self.userImg.mas_height);
    }];
    
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userImg.mas_right).offset(5);
        make.bottom.equalTo(self.userImg.mas_centerY).offset(-5);
    }];
    
    [self.ageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_right).offset(5);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
        make.height.mas_equalTo(15);
    }];
    
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userImg.mas_right).offset(5);
        make.top.equalTo(self.userImg.mas_centerY).offset(5);
    }];
    
    [self.dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
    }];
    
    [self.yellowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.centerY.equalTo(self.descLbl.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(6, 6));
    }];
    self.yellowView.layer.masksToBounds = YES;
    self.yellowView.layer.cornerRadius = 3.0;
}

-(UserImageView *)userImg{
    if (!_userImg) {
        _userImg = [[UserImageView alloc]init];
        [self addSubview:_userImg];
    }
    return _userImg;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        _nameLbl.font = UserNameFont;
        [self addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.font = DetailFont;
        _descLbl.textColor = AppGray;
        [self addSubview:_descLbl];
    }
    return _descLbl;
}

-(UILabel *)dateLbl{
    if (!_dateLbl) {
        _dateLbl = [ControllerHelper autoFitLabel];
        _dateLbl.font = [UIFont systemFontOfSize:10];
        _dateLbl.textAlignment = NSTextAlignmentRight;
        _dateLbl.textColor = RGB16(0x979797);
        [self addSubview:_dateLbl];
    }
    return _dateLbl;
}

-(SexAgeView *)ageView{
    if (!_ageView) {
        _ageView = [[SexAgeView alloc]init];
        [self addSubview:_ageView];
    }
    return _ageView;
}

-(UIView *)yellowView{
    if (!_yellowView) {
        _yellowView = [[UIView alloc]init];
        _yellowView.backgroundColor = AppRedPoint;
        _yellowView.hidden = YES;
        [self addSubview:_yellowView];
    }
    return _yellowView;
}

-(void)setConversationModel:(LLConversationModel *)conversationModel{
    _conversationModel = conversationModel;
//    self.nameLbl.text = _conversationModel.nickName ? _conversationModel.nickName :@"...";
    self.dateLbl.text = conversationModel.latestMessageTimeString;
    self.yellowView.hidden = conversationModel.unreadMessageNumber == 0;
    if (conversationModel.draft.length == 0) {
        self.descLbl.text = conversationModel.latestMessage;
    }else {
        NSString *string = [NSString stringWithFormat:@"[草稿] %@", conversationModel.draft];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
        [attributedString setAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:183/255.0 green:24/255.0 blue:24/255.0 alpha:1]} range:NSMakeRange(0, 4)];
        self.descLbl.attributedText = attributedString;
    }

}


- (void)markAllMessageAsRead {
    
    self.yellowView.hidden = YES;
    [[LLChatManager sharedManager] markAllMessagesAsRead:self.conversationModel];
}


@end
