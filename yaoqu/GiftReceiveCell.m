//
//  GiftReceiveCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/30.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "GiftReceiveCell.h"
#import "UserImageView.h"
#import "SexView.h"


@interface GiftReceiveCell()

@property(nonatomic,strong) UIImageView *bgImage;
@property(nonatomic,strong) UserImageView *userImage;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) SexView *sexView;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UILabel *contentLbl;
@property(nonatomic,strong) UIImageView *contentImage;

@end

@implementation GiftReceiveCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self commonInit];
    }
    return self;
}


-(void)commonInit{
    [self.bgImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.top.equalTo(self.contentView.mas_top);
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
    }];
    
    [self.userImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bgImage.mas_left).offset(10);
        make.top.equalTo(self.bgImage.mas_top).offset(10);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userImage.mas_right).offset(10);
        make.centerY.equalTo(self.userImage.mas_centerY);
    }];
    
    [self.sexView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_right).offset(5);
        make.centerY.equalTo(self.userImage.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userImage.mas_right).offset(10);
        make.top.equalTo(self.userImage.mas_bottom).offset(10);
        make.right.equalTo(self.contentImage.mas_right).offset(-10);
    }];
    
    
    
    [self.contentLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userImage.mas_right).offset(10);
        make.top.equalTo(self.contentImage.mas_bottom).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
    }];
    
    [self.contentImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.bgImage.mas_right).offset(-10);
        make.centerY.equalTo(self.bgImage.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(60, 60));
    }];
}

-(void)setModel:(GiftModel *)model{
    
    
    _model = model;
    self.userImage.userModel = [_model objectWithClass:@"UserModel"];
    self.nameLbl.text = model.username;
    self.sexView.sexType = [model.sex integerValue];
    self.descLbl.text = [NSString stringWithFormat:@"%@\n%@\n价值%@礼品券",model.gift_name,model.gift_desc,model.giftroll];
    
    [self.contentImage setImageName:model.gift_image placeholder:nil];
    if (self.type == GiftSearchType_Receive)
        self.contentLbl.text = @"送给你的礼物";
    else
        self.contentLbl.text = @"送出了礼物";
}

-(UIImageView *)bgImage{
    if (!_bgImage) {
        _bgImage = [[UIImageView alloc]init];
        _bgImage.image = [UIImage imageNamed:@"bg_msg_gift.jpg"];
        _bgImage.layer.masksToBounds = YES;
        _bgImage.layer.borderColor = AppOrange.CGColor;
        _bgImage.layer.borderWidth = 1.0;
        _bgImage.layer.cornerRadius = 5.0;
        [self.contentView addSubview:_bgImage];
    }
    return _bgImage;
}

-(UserImageView *)userImage{
    if (!_userImage) {
        _userImage = [[UserImageView alloc]init];
        [self.contentView addSubview:_userImage];
    }
    return _userImage;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        [self.contentView addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(SexView *)sexView{
    if (!_sexView) {
        _sexView = [[SexView alloc]init];
        [self.contentView addSubview:_sexView];
    }
    return _sexView;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.font = SystemFont(14);
        _descLbl.textColor = AppGray;
        [self.contentView addSubview:_descLbl];
    }
    
    return _descLbl;
}

-(UILabel *)contentLbl{
    if (!_contentLbl) {
        _contentLbl = [ControllerHelper autoFitLabel];
        [self.contentView addSubview:_contentLbl];
    }
    return _contentLbl;
}

-(UIImageView *)contentImage{
    if (!_contentImage) {
        _contentImage = [[UIImageView alloc]init];
        [self.contentView addSubview:_contentImage];
    }
    return _contentImage;
}

@end
