//
//  UserImageView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserImageView.h"
#import "RouteController.h"

@interface UserImageView()



@end

@implementation UserImageView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isShowTalent = YES;
        self.isShowMember = YES;
        [self commonInit];
    }
    return self;
}


-(void)commonInit{
//    self.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.layer.masksToBounds = YES;
    [self setupConstranits];
    
    [RACObserve(self, userModel) subscribeNext:^(UserModel *x) {
        [self.userImg setImageName:[x.image fixImageString:MemberLogoMiddleFix] placeholder:nil];
        self.levelLbl.hidden = self.isShowTalent ? !x.is_talent : YES;
        self.levelImg.hidden = self.isShowTalent ? !x.is_talent : YES;
        self.memberImg.hidden = self.isShowMember ? !x.is_member : YES;
    }];
    
}

-(void)setIsShowMember:(BOOL)isShowMember{
    _isShowMember = isShowMember;
    
    
    self.memberImg.hidden = self.isShowMember ? !self.userModel.is_member : YES;
    
}

-(void)setIsShowTalent:(BOOL)isShowTalent{
    _isShowTalent = isShowTalent;
    
    self.levelLbl.hidden = self.isShowTalent ? !self.userModel.is_talent : YES;
    self.levelImg.hidden = self.isShowTalent ? !self.userModel.is_talent : YES;
    
}



-(void)tap:(id)sender{
    [[RouteController sharedManager] openHomePageVC:self.userModel];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    CGFloat width = self.userImg.frame.size.width / 2;
    
    CGFloat a = sqrtf(width * width / 2.0);
    self.userImg.layer.cornerRadius = self.userImg.frame.size.width / 2;
    self.memberImg.layer.cornerRadius = self.memberImg.frame.size.width / 2;
    [self.memberImg mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.userImg.mas_centerX).offset(a);
        make.centerY.equalTo(self.userImg.mas_centerY).offset(0 - a);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
}


-(UIImageView *)userImg{
    if (!_userImg) {
        _userImg = [[UIImageView alloc]init];
        _userImg.userInteractionEnabled = YES;
        _userImg.contentMode = UIViewContentModeScaleAspectFill;
        _userImg.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _userImg.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _userImg.clipsToBounds = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
        [_userImg addGestureRecognizer:tap];
        [self addSubview:_userImg];
    }
    return _userImg;
}

-(UIImageView *)levelImg{
    if (!_levelImg) {
        _levelImg = [[UIImageView alloc]init];
        _levelImg.image = [UIImage imageNamed:@"ic_user_blue_bg"];
        [self addSubview:_levelImg];
    }
    return _levelImg;
}

-(UIView *)memberImg{
    if (!_memberImg) {
        _memberImg = [[UIView alloc]init];
        _memberImg.backgroundColor = [UIColor whiteColor];
        _memberImg.layer.masksToBounds = YES;
        
        UIImageView *imageView = [[UIImageView alloc]init];
        imageView.image = [UIImage imageNamed:@"ic_user_level_1"];
        [_memberImg addSubview:imageView];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_memberImg).insets(UIEdgeInsetsMake(3, 3, 3, 3));
        }];
        
        [self addSubview:_memberImg];
    }
    return _memberImg;
}

-(UILabel *)levelLbl{
    if (!_levelLbl) {
        _levelLbl = [[UILabel alloc]init];
        _levelLbl.backgroundColor = [UIColor clearColor];
        _levelLbl.textColor = [UIColor whiteColor];
        _levelLbl.font = [UIFont systemFontOfSize:7];
        _levelLbl.textAlignment = NSTextAlignmentCenter;
        _levelLbl.text = @"达人";
        [self.levelImg addSubview:_levelLbl];
    }
    return _levelLbl;
}


-(void)setupConstranits{
    
    [self.levelImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.mas_width).multipliedBy(1/3.0);
        make.height.mas_equalTo(12);
        make.bottom.equalTo(self.mas_bottom);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    [self.levelLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.levelImg);
    }];
    
    [self.userImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.levelImg.mas_centerY);
        make.centerX.equalTo(self.mas_centerX);
        make.width.equalTo(self.userImg.mas_height);
    }];
    
    [self.memberImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.userImg.mas_right);
        make.centerY.equalTo(self.userImg.mas_top);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    [self sendSubviewToBack:self.userImg];
    
    
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
