//
//  DynamicLikeApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/29.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface DynamicLikeApi : BaseApi


-(id)initWithDynamic_id:(NSString *)dynamic_id is_like:(BOOL)is_like;

@end
