//
//  TripCoinPayVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseViewController.h"
#import "CommonConfigModel.h"
#import "TravelEnrollModel.h"

@interface TripCoinPayVC : BaseViewController


@property(nonatomic,strong) NSString *titleDesc;

@property(nonatomic,strong) CurrencyValueModel *model;
@property(nonatomic,strong) TravelEnrollModel *enrollModel;

@property(nonatomic,strong) NSMutableDictionary *enrollObj;

@property(nonatomic) CommonPayViewType payViewType;

@end
