//
//  RedPacketSendVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/25.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "RedPacketSendVC.h"

@interface RedPacketSendVC ()

@property(nonatomic,strong) UIView *headerView;
@property(nonatomic,strong) UIButton *sendBtn;
@property(nonatomic,strong) UILabel *totalLbl;

@end

@implementation RedPacketSendVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"发红包";
    [self setRightItemWithTitle:@"红包记录" selector:nil];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIView *)headerView{
    if (!_headerView) {
        _headerView = [[UIView alloc]init];
        _headerView.backgroundColor = [UIColor whiteColor];
        _headerView.layer.cornerRadius = 3;
        _headerView.layer.masksToBounds = YES;
        _headerView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        _headerView.layer.shadowRadius = 3;
        _headerView.layer.shadowOffset = CGSizeMake(3, 3);
        [self.view addSubview:_headerView];
    }
    return _headerView;
}

-(UILabel *)totalLbl{
    if (!_totalLbl) {
        _totalLbl = [ControllerHelper autoFitLabel];
        [self.view addSubview:_totalLbl];
    }
    return _totalLbl;
}

-(UIButton *)sendBtn{
    if (!_sendBtn) {
        _sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [_sendBtn setTitle:@"塞钱进红包" forState:UIControlStateNormal];
        [_sendBtn setTitleColor:AppGray forState:UIControlStateDisabled];
        [_sendBtn setTitleColor:AppBlack forState:UIControlStateNormal];
        
        _sendBtn.layer.masksToBounds = YES;
        _sendBtn.layer.cornerRadius = 3;
        _sendBtn.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        _sendBtn.layer.shadowRadius = 3;
        _sendBtn.layer.shadowOffset = CGSizeMake(3, 3);
        
        [self.view addSubview:_sendBtn];
        
    }
    return _sendBtn;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
