//
//  ChatInputView.h
//  shiyi
//
//  Created by ridchan on 16/6/24.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChatInputViewDelegate <NSObject>

@optional
-(void)chatInputSend:(id)obj;

@end



@interface ChatInputView : UIView


@property(nonatomic,strong) UITextField *textField;
@property(nonatomic,strong) UIButton *toolBtn;
@property(nonatomic,strong) UIButton *emjoyBtn;

@property(nonatomic,strong) UIButton *sendBtn;

@property(nonatomic) BOOL inPublish;

-(instancetype) initWithPublish;
-(instancetype) initWithChat;
-(void)valueChange:(id)sender;

@property(nonatomic,assign) id<ChatInputViewDelegate> delegate;


@end
