//
//  UserEmotionalVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/7.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NormalTableViewController.h"
#import "UserModel.h"

@interface UserEmotionalVC : NormalTableViewController

@property(nonatomic,strong) UserModel *editModel;

@end
