//
//  ConversationListVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "ConversationListVC.h"
#import "FTPopOverMenu.h"

#import "LLClientManager.h"
#import "LLTableViewCell.h"
#import "LLTableViewCellData.h"
#import "LLChatManager.h"
#import "ConversationListCell.h"
#import "LLChatViewController.h"
#import "GiftView.h"
#import "LLSearchViewController.h"
#import "LLNavigationController.h"
#import "LLChatSearchController.h"
#import "ConversationStrangerCell.h"
#import "StrangerConversationListVC.h"

@interface ConversationListVC ()<LLChatManagerConversationListDelegate,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,LLSearchControllerDelegate>

@property (nonatomic) NSMutableArray<LLConversationModel *> *allConversationModels;
@property (nonatomic) NSMutableArray<LLConversationModel *> *frinedConversationModels;
@property (nonatomic) NSMutableArray<LLConversationModel *> *strangerConversationModels;

@property (nonatomic) NSMutableArray<LLConversationModel *> *topConversationModels;
@property (nonatomic) NSMutableArray<LLConversationModel *> *otherConversationModels;


@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) UISearchBar *searchBar;
@property (nonatomic) UIView *tableHeaderView;

@property(nonatomic) StrangerConversationListVC *strangerVC;

@end

@implementation ConversationListVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [self commonInit];
    // Do any additional setup after loading the view.
}

-(void)commonInit{
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    [self setLeftItemWithIcon:[UIImage imageNamed:@"ic_chat_message"] title:nil selector:@selector(messageBtnClick:)];
    [self setRightItemWithIcon:[UIImage imageNamed:@"ic_chat_add"] selector:@selector(addBtnClick:)];
    
    
    FTPopOverMenuConfiguration *configuration = [FTPopOverMenuConfiguration defaultConfiguration];
    configuration.borderColor = [UIColor lightGrayColor];
    configuration.borderWidth = 0.2;
    
    self.strangerVC = [[StrangerConversationListVC alloc]init];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadCompleteHandler:) name:LLMessageUploadStatusChangedNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshRemoveConversation:) name:LLMessageRefreshStatusChangedNotification object:nil];

    [self fetchData];
    
    
}


-(void)refreshRemoveConversation:(NSNotification *)notification{
    if (notification.object){
        [self.allConversationModels removeObject:notification.object];
    }
    [self refreshData];
}


-(void)messageBtnClick:(id)sender{
    [[RouteController sharedManager]openClassVC:@"MessageCenterVC"];
}

-(UIView *)tableHeaderView{
    if (!_tableHeaderView) {
        _tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, APP_WIDTH, 44)];
        _tableHeaderView.backgroundColor = [UIColor clearColor];
        [_tableHeaderView addSubview:self.searchBar];
    }
    return _tableHeaderView;
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        [_tableView registerClass:[ConversationListCell class] forCellReuseIdentifier:@"Cell"];
        [_tableView registerClass:[ConversationStrangerCell class] forCellReuseIdentifier:@"StrangerCell"];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.tableHeaderView = self.tableHeaderView;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

-(UISearchBar *)searchBar{
    if (!_searchBar) {
        _searchBar = [LLSearchBar defaultSearchBarWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
        _searchBar.delegate = self;
        _searchBar.placeholder = @"搜索";
//        _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 40)];
//        _searchBar.delegate = self;
//        _searchBar.barStyle = UISearchBarStyleMinimal;
//        _searchBar.searchBarStyle
    }
    return _searchBar;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)uploadCompleteHandler:(NSNotification *)notification {
    LLMessageModel *messageModel = notification.userInfo[LLChatManagerMessageModelKey];
    if (!messageModel)
        return;
    
    WEAK_SELF;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf refreshTableRowWithConversationId:messageModel.conversationId];
    });
    
}

- (void)refreshTableRowWithConversationModel:(LLConversationModel *)conversationModel {
    for (ConversationListCell *cell in self.tableView.visibleCells) {
        if (![cell isKindOfClass:[ConversationListCell class]]) continue;
        if ([cell.conversationModel.conversationId isEqualToString:conversationModel.conversationId]) {
            cell.conversationModel = conversationModel;
            break;
        }
    }
}

- (void)refreshTableRowWithConversationId:(NSString *)conversationId {
    for (ConversationListCell *cell in self.tableView.visibleCells) {
        if (![cell isKindOfClass:[ConversationListCell class]]) continue;
        if ([cell.conversationModel.conversationId isEqualToString:conversationId]) {
            cell.conversationModel = cell.conversationModel;
            break;
        }
    }
    
    
}
#pragma mark - 获取会话数据

-(void)explainViewConversation:(NSArray *)conversations{
    
}

- (void)refreshData{
    [LLChatManager sharedManager].conversationListDelegate = self;
    [[LLChatManager sharedManager] getAllConversation];
}

- (void)fetchData {
    [LLChatManager sharedManager].conversationListDelegate = self;
    [[LLChatManager sharedManager] getAllConversationFromDB];
}

- (void)conversationListDidChanged:(NSArray<LLConversationModel *> *)conversationList {
    
    WEAK_SELF;
    __block NSMutableArray *friends = [NSMutableArray array];
    __block NSMutableArray *strangers = [NSMutableArray array];
    
    
    self.allConversationModels = [conversationList mutableCopy];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (LLConversationModel *conversationModel in conversationList){
            while ([conversationModel.model.member_id integerValue] == 0) {
                [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
            }
            
            if (conversationModel.model.is_follow){
                [friends addObject:conversationModel];
            }else{
                if ([friends indexOfObject:strangers] == NSNotFound){
                    [friends addObject:strangers];
                }
                [strangers addObject:conversationModel];
            }
        }
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            weakSelf.frinedConversationModels = friends;
            weakSelf.strangerConversationModels = strangers;
            weakSelf.strangerVC.conversationModels = strangers;
            [weakSelf.tableView reloadData];
            [weakSelf setUnreadMessageCount];
        });
    });
    
    
    
//    self.allConversationModels = [conversationList mutableCopy];

}

- (NSMutableArray<LLConversationModel *> *)currentConversationList {
    return [self.allConversationModels mutableCopy];
}


- (void)unreadMessageNumberDidChanged {
    [self setUnreadMessageCount];
}


- (void)setUnreadMessageCount {
    NSInteger count = 0;
    for (LLConversationModel *data in self.allConversationModels) {
        if (![data isKindOfClass:[NSArray class]])
            count += data.unreadMessageNumber;
    }
    
//    [[LLUtils appDelegate].mainViewController setTabbarBadgeValue:count tabbarIndex:kLLMainTabbarIndexChat];
    
    self.tabBarController.tabBar.items[3].badgeValue = count > 0 ? [NSString stringWithFormat:@"%ld", (long)count] : nil;
    
    
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = count;
    
}


#pragma mark -
#pragma mark tableview 

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.frinedConversationModels.count ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    id obj = self.frinedConversationModels[indexPath.row];
    if (![obj isKindOfClass:[NSArray class]]){
        ConversationListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        
        cell.conversationModel = self.frinedConversationModels[indexPath.row];
        return cell;
    }else{
        ConversationStrangerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StrangerCell" forIndexPath:indexPath];
        cell.conversationModels = (NSArray *)self.frinedConversationModels[indexPath.row];
        return cell;
    }
    
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    id obj = self.frinedConversationModels[indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (![obj isKindOfClass:[NSArray class]]){
        ConversationListCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [cell markAllMessageAsRead];
        [[RouteController sharedManager] chatWithConversationModel:cell.conversationModel];
    }else{
        
        [self.navigationController pushViewController:self.strangerVC animated:YES];
    }
    
    

}

#pragma mark - 左滑显示删除和已读

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    ConversationListCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    WEAK_SELF;
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:LS(@"删除") handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        
        id obj = weakSelf.frinedConversationModels[indexPath.row];
        if ([obj isKindOfClass:[NSArray class]]){
            
            
            [weakSelf.frinedConversationModels removeObjectAtIndex:indexPath.row];
            for (LLConversationModel *model in weakSelf.strangerConversationModels){
                [weakSelf.allConversationModels removeObject:model];
                [[LLChatManager sharedManager] deleteConversation:model];
            }
            
            [tableView deleteRowsAtIndexPaths:@[indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [weakSelf setUnreadMessageCount];
        }else{
            BOOL result = [[LLChatManager sharedManager] deleteConversation:cell.conversationModel];
            if (result) {
                [weakSelf.allConversationModels removeObject:obj];
                [weakSelf.frinedConversationModels removeObjectAtIndex:indexPath.row];
                [tableView deleteRowsAtIndexPaths:@[indexPath]
                                 withRowAnimation:UITableViewRowAnimationFade];
                [weakSelf setUnreadMessageCount];
            }
        }
        
    }];
    
//    UITableViewRowAction *setReadAction;
//    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//    [dict addEntriesFromDictionary:cell.conversationModel.sdk_conversation.ext];
//    if ([dict objectForKey:@"isTop"]) {
//        setReadAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:LS(@"取消置顶") handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
//            [dict removeObjectForKey:@"isTop"];
//            [cell.conversationModel.sdk_conversation setExt:dict];
//        }];
//
//    }else{
//        setReadAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:LS(@"置顶") handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
//            [dict setObject:@"1" forKey:@"isTop"];
//            [cell.conversationModel.sdk_conversation setExt:dict];
//        }];
//    }
    
//    if (cell.conversationModel.unreadMessageNumber > 0) {
//        setReadAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:LS(@"移至顶部") handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
//            [cell markAllMessageAsRead];
//            [self setUnreadMessageCount];
//            
//            
//            [tableView setEditing:NO animated:YES];
//        }];
//    }else {
//        setReadAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:LS(@"移至顶部") handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
////            [cell markMessageAsNotRead];
//            [self setUnreadMessageCount];
//            [tableView setEditing:NO animated:YES];
//        }];
//    }
//
    
    return @[deleteAction];
}



#pragma mark -
#pragma mark button action

-(void)addBtnClick:(id)sender{
    
    
    [FTPopOverMenu showFromSenderFrame:CGRectMake(self.view.frame.size.width - 45, 20, 40, 40)
                         withMenuArray:@[LS(@"搜索"),LS(@"扫描")]
                            imageArray:@[@"ic_search",@"扫一扫"]
                             doneBlock:^(NSInteger selectedIndex) {
                                 if (selectedIndex == 1) {
//                                     QRCodeReaderViewController *vc = [[QRCodeReaderViewController alloc]init];
//                                     [self.navigationController pushViewController:vc animated:YES];
//                                     [GiftView show];
                                     [self pushViewControllerWithName:@"ScanerVC"];
                                 }else{
                                     [self pushViewControllerWithName:@"ChatSearchUserVC"];
                                 }
                
                                 
                              } dismissBlock:^{
 
                              }];;


    
}


#pragma mark -

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    LLSearchViewController *vc = [LLSearchViewController sharedInstance];
    LLNavigationController *navigationVC = [[LLNavigationController alloc] initWithRootViewController:vc];
    navigationVC.view.backgroundColor = [UIColor clearColor];
    vc.delegate = self;
    LLChatSearchController *resultController = [[LLUtils mainStoryboard] instantiateViewControllerWithIdentifier:SB_CONVERSATION_SEARCH_VC_ID];
    
    vc.searchResultController = resultController;
    resultController.searchViewController = vc;
    [vc showInViewController:self fromSearchBar:self.searchBar];
    
    return NO;
}

- (void)willPresentSearchController:(LLSearchViewController *)searchController {
    
}

- (void)didPresentSearchController:(LLSearchViewController *)searchController {
//    self.tableView.tableHeaderView = nil;
//    CGRect frame = _tableHeaderView.frame;
//    frame.origin.y = -frame.size.height;
//    _tableHeaderView.frame = frame;
}

- (void)willDismissSearchController:(LLSearchViewController *)searchController {
    
//    [UIView animateWithDuration:HIDE_ANIMATION_DURATION animations:^{
//        _searchBar.hidden = YES;
//        self.tableView.tableHeaderView = _tableHeaderView;
//    } completion:^(BOOL finished) {
//        _searchBar.hidden = NO;
//    }];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
