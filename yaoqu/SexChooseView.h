//
//  SexChooseView.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/8.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SexChooseView : UIView

@property(nonatomic) NSInteger sex;

@end
