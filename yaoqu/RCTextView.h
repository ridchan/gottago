//
//  RCTextView.h
//  QBH
//
//  Created by 陳景雲 on 2017/2/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCTextView : UITextView<UITextViewDelegate>

@property(nonatomic,strong) NSString *placeholder;
@property(nonatomic,strong) UILabel *label;
@property(nonatomic,strong) UILabel *maxLbl;
@property(nonatomic) NSInteger maxNum;


@end
