//
//  GiftBuyApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/12.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "GiftBuyApi.h"

@implementation GiftBuyApi

-(id)initWithGift:(NSString *)gift_id quantify:(NSInteger)qty{
    if (self = [super init]) {
        [self.params setValue:gift_id forKey:@"gift_id"];
        [self.params setValue:@(qty) forKey:@"qty"];
    }
    return self;
}

-(NSString *)requestUrl{
    return GiftBuyUrl;
}


@end
