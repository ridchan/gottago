//
//  TravelEnrollDetailCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/15.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelEnrollDetailCell.h"

@interface TravelEnrollDetailCell()

@property(nonatomic,strong) UILabel *label1;
@property(nonatomic,strong) UILabel *label2;
@property(nonatomic,strong) UILabel *label3;
@property(nonatomic,strong) UILabel *label4;

@end

@implementation TravelEnrollDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self =  [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    [self.label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.height.mas_equalTo(20);
        make.top.equalTo(self.contentView.mas_top).offset(10);
    }];
    
    [self.label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.height.mas_equalTo(20);
        make.top.equalTo(self.label1.mas_bottom).offset(10);
    }];
    
    [self.label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.height.mas_equalTo(20);
        make.top.equalTo(self.label2.mas_bottom).offset(10);
    }];
    
    [self.label4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.height.mas_equalTo(20);
        make.top.equalTo(self.label3.mas_bottom).offset(10);
    }];
    
    
}

-(void)setModel:(TravelContactModel *)model{
    _model = model;
    self.label1.text = [NSString stringWithFormat:@"姓名：%@",model.fullname];
    self.label2.text = [NSString stringWithFormat:@"证件类型：%@",model.card_type];
    self.label3.text = [NSString stringWithFormat:@"证件编号：%@",model.card_num];
    self.label4.text = [NSString stringWithFormat:@"联系电话：%@",model.phone];
}


-(UILabel *)label1{
    if (!_label1) {
        _label1 = [ControllerHelper autoFitLabel];
        _label1.textColor = AppGray;
        _label1.font = SystemFont(14);
        [self.contentView addSubview:_label1];
    }
    return _label1;
}

-(UILabel *)label2{
    if (!_label2) {
        _label2 = [ControllerHelper autoFitLabel];
        _label2.textColor = AppGray;
        _label2.font = SystemFont(14);
        [self.contentView addSubview:_label2];
    }
    return _label2;
}

-(UILabel *)label3{
    if (!_label3) {
        _label3 = [ControllerHelper autoFitLabel];
        _label3.textColor = AppGray;
        _label3.font = SystemFont(14);
        [self.contentView addSubview:_label3];
    }
    return _label3;
}

-(UILabel *)label4{
    if (!_label4) {
        _label4 = [ControllerHelper autoFitLabel];
        _label4.textColor = AppGray;
        _label4.font = SystemFont(14);
        [self.contentView addSubview:_label4];
    }
    return _label4;
}

@end
