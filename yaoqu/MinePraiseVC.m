//
//  MinePraiseVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MinePraiseVC.h"
#import "UserNormalCell.h"
#import "UserMessageLikeApi.h"
#import "MessageLikeModel.h"
#import "MessageApi.h"

@interface MinePraiseVC ()<UITableViewDelegate,UITableViewDataSource>


@end

@implementation MinePraiseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"谁点赞了我");
    
    [self startRefresh];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)startRefresh{
    WEAK_SELF;
    self.page = 1;
    
    NSDictionary *dict = @{@"page":@(self.page),
                           @"size":@(self.size),
                           @"type":@(MessageCenterType_Like)
                           };
    
    
    [[[MessageApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [weakSelf refreshComplete:info response:[MessageLikeModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]]];
    }];
    
}

-(void)startloadMore{
    
    WEAK_SELF;
    
    NSDictionary *dict = @{@"page":@(++self.page),
                           @"size":@(self.size),
                           @"type":@(MessageCenterType_Like)
                           };
    
    [[[MessageApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        
        [weakSelf loadMoreComplete:info response:[MessageLikeModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]]];
        
    }];
    
}



#pragma mark -

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    MessageLikeModel *model = self.datas[indexPath.row];
//    DynamicModel *newModel = [[DynamicModel alloc]init];
//    newModel.dynamic_id = model.key_id;
//    [[RouteController sharedManager]openClassVC:@"DynamicDetailVC" withObj:newModel];
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        MessageLikeModel *model = [self.datas objectAtIndex:indexPath.row];
        
        MessageApi *delApi = [[MessageApi alloc]initWitObject:@{@"type":@"1",@"id":model.message_like_id}];
        delApi.method = YTKRequestMethodDELETE;
        
        [delApi startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            
        }];
        
        [self.datas removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserNormalCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ImageCell"];
    if (cell == nil) {
        cell = [UserNormalCell normalCellType:UserCellType_Date_Desc identify:@"ImageCell"];
    }
    
    MessageLikeModel *model = [self.datas objectAtIndex:indexPath.row];
    
    cell.nameLbl.text = model.username;
    cell.dateLbl.text = model.show_time;
    cell.sexView.sex = [model.sex integerValue];
    cell.sexView.age = [model.age integerValue];
    cell.userImage.userModel = [model objectWithClass:@"UserModel"];
    if ([model.data.images length] > 0 || [model.data.video_image length] > 0) {
        if ([model.data.video length] == 0 && [[model.data.images JSONObject] count] == 0){
            cell.cellType = UserCellType_Date_Desc;
            cell.descLbl.text = model.data.desc;
            return cell;
        }
        cell.cellType = UserCellType_Date_Image;
        NSString *image = nil;
        if ([model.data.video_image length] > 0) {
            image = model.data.video_image;
        }else{
            image = [[model.data.images JSONObject] firstObject];
        }
        [cell.contentImage setImageName:image placeholder:nil];
    }else{
        cell.cellType = UserCellType_Date_Desc;
        cell.descLbl.text = model.data.desc;
    }
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
