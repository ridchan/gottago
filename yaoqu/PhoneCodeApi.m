//
//  PhoneCodeApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/16.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "PhoneCodeApi.h"


@interface PhoneCodeApi()

@end

@implementation PhoneCodeApi

-(id)initWithPhone:(NSString *)phone{
    if (self = [super init]) {
        [self.params setValue:phone forKey:@"phone"];
    }
    return self;
}


-(NSString *)requestUrl{
    return RegistCodeUrl;
}

@end
