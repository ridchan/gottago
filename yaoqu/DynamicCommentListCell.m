//
//  DynamicCommentListCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/30.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicCommentListCell.h"
#import "SexView.h"
#import "DynamicCommentLikeApi.h"
#import "CommentApi.h"

@interface DynamicCommentListCell()

@property(nonatomic,strong) UIImageView *userImage;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) SexView *sexImage;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UILabel *dateLbl;
@property(nonatomic,strong) ImageButton *likeBtn;
@property(nonatomic,strong) UIView *bottomLine;

@end

@implementation DynamicCommentListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self commonInit2];
        [self rac_prepareForReuseSignal];
        
        [RACObserve(self, model) subscribeNext:^(DynamicCommentModel *x) {
            [self.userImage setImageName:x.image placeholder:nil];
            self.nameLbl.text = x.username;
            self.descLbl.text = x.content;
            self.dateLbl.text = [NSString compareCurrentTime:x.time];
            self.likeBtn.titleLbl.text = x.like;
            self.sexImage.sexType = [x.sex integerValue];
            
        }];
        
        [RACObserve(self, model.is_like) subscribeNext:^(id x) {
            self.likeBtn.contentImage.image = [x boolValue] ? [UIImage imageNamed:@"ic_dynamic_like_on"] : [[UIImage imageNamed:@"ic_dynamic_like"] imageToColor:AppBlack];
        }];
        
        RAC(self.likeBtn.titleLbl,text) =  RACObserve(self, model.like);
    }
    return self;
}

-(void)commonInit2{
    
    
    self.userImage.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .topSpaceToView(self.contentView, 10)
    .heightIs(40)
    .widthIs(40);
    self.userImage.layer.cornerRadius = 20;
    self.userImage.layer.masksToBounds = YES;
    
    self.nameLbl.sd_layout
    .autoWidthRatio(0)
    .heightIs(30)
    .leftSpaceToView(self.userImage, 5)
    .centerYEqualToView(self.userImage);
    [self.nameLbl setSingleLineAutoResizeWithMaxWidth:200];
    
    self.sexImage.sd_layout
    .leftSpaceToView(self.nameLbl, 5)
    .centerYEqualToView(self.nameLbl)
    .heightIs(15)
    .widthIs(15);
    
    self.descLbl.sd_layout
    .leftEqualToView(self.nameLbl)
    .topSpaceToView(self.userImage, 10)
    .rightSpaceToView(self.contentView, 10)
    .autoHeightRatio(0);
    
    self.dateLbl.sd_layout
    .leftEqualToView(self.nameLbl)
    .topSpaceToView(self.descLbl, 10)
    .heightIs(20)
    .autoWidthRatio(0);
    [self.dateLbl setSingleLineAutoResizeWithMaxWidth:200];
    
    self.likeBtn.sd_layout
    .centerYEqualToView(self.dateLbl)
    .rightSpaceToView(self.contentView,10)
    .widthIs(50)
    .heightIs(25);
    
    self.bottomLine.sd_layout
    .leftEqualToView(self.contentView)
    .rightEqualToView(self.contentView)
    .topSpaceToView(self.likeBtn, 10)
    .heightIs(0.5);
    
    [self setupAutoHeightWithBottomView:self.bottomLine bottomMargin:0];
    
    [self layoutSubviews];
}

-(void)commonInit{
    [self.userImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.equalTo(self.contentView).offset(10);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    self.userImage.layer.cornerRadius = 25;
    self.userImage.layer.masksToBounds = YES;
    
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.userImage.mas_centerY);
        make.left.equalTo(self.userImage.mas_right).offset(5);
    }];
    
    [self.sexImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_right).offset(5);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.userImage.mas_bottom).offset(20);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
    }];
    

    
    [self.likeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.centerY.equalTo(self.dateLbl.mas_centerY);
        make.height.mas_equalTo(25);
    }];
    
    
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.right.equalTo(self.contentView.mas_right);
        make.height.mas_equalTo(0.5);
    }];
    
    [self.dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.descLbl.mas_bottom).offset(10);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-5);//???
    }];
}



#pragma mark -
#pragma mark lazy layout

-(UIImageView *)userImage{
    if (!_userImage) {
        _userImage = [[UIImageView alloc]init];
        _userImage.contentMode = UIViewContentModeScaleAspectFill;
        _userImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.contentView addSubview:_userImage];
    }
    return _userImage;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        _nameLbl.font = DynamicCommentNameFont;
        _nameLbl.textColor = DynamicCommentNameColor;
        [self.contentView addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(SexView *)sexImage{
    if (!_sexImage) {
        _sexImage = [[SexView alloc]init];
        [self.contentView addSubview:_sexImage];
    }
    return _sexImage;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.numberOfLines = 0;
        _descLbl.font = DynamicCommentDescFont;
        _descLbl.textColor = DynamicCommentDescColor;
        [self.contentView addSubview:_descLbl];
    }
    return _descLbl;
}

-(UILabel *)dateLbl{
    if (!_dateLbl) {
        _dateLbl = [ControllerHelper autoFitLabel];
        _dateLbl.font = DynamicCommentTimeFont;
        _dateLbl.textColor = DynamicCommentTimeColor;
        [self.contentView addSubview:_dateLbl];
    }
    return _dateLbl;
}

-(ImageButton *)likeBtn{
    if (!_likeBtn) {
        _likeBtn = [[ImageButton alloc]init];
        _likeBtn.contentImage.image = [UIImage imageNamed:@"ic_dynamic_like"];
        [_likeBtn addTarget:self action:@selector(likeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_likeBtn];
    }
    return _likeBtn;
}

-(UIView *)bottomLine{
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc]init];
        _bottomLine.backgroundColor = AppLineColor;
        [self.contentView addSubview:_bottomLine];
    }
    return _bottomLine;
}


#pragma mark -
#pragma mark button action

-(void)likeBtnClick:(id)sender{
    [self.likeBtn.contentImage.layer addAnimation:[self.likeBtn keyAnimation] forKey:nil];
    
    WEAK_SELF;
    NSDictionary *dict = @{@"comment_id":self.model.comment_id,
                           @"state":@(!self.model.is_like)
                           };
    CommentApi *api = [[CommentApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodOPTION;
    
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            weakSelf.model.is_like = !weakSelf.model.is_like;
            weakSelf.model.like = weakSelf.model.is_like ?
            [weakSelf.model.like autoIncrice] :
            [weakSelf.model.like autoReduce];
        }else{
            
        }
    }];}

@end
