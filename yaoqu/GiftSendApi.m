//
//  GiftSendApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/12.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "GiftSendApi.h"

@implementation GiftSendApi

-(id)initWithID:(NSString *)gift_id type:(GiftType)type keyID:(NSString *)key_id desc:(NSString *)desc memberID:(NSString *)member_id{
    if (self = [super init]) {
        [self.params setValue:gift_id forKey:@"gift_id"];
        [self.params setValue:@(type) forKey:@"type"];
        [self.params setValue:key_id forKey:@"key_id"];
        [self.params setValue:desc forKey:@"desc"];
        [self.params setValue:member_id forKey:@"member_id"];    }
    
    return self;
}

-(NSString *)requestUrl{
    return GiftSendUrl;
}


@end
