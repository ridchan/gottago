//
//  UserSexEditVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/22.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserSexEditVC.h"
#import "NormalTableViewCell.h"
#import "NSString+Extend.h"
@interface UserSexEditVC ()


@property(nonatomic,strong) NSString *sex;

@end

@implementation UserSexEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.editModel = self.paramObj;
    
    
    self.title = LS(@"性别");
    self.datas = [@[LS(@"女"),LS(@"男")] mutableCopy];
    self.sex = self.editModel.sex;
    [self setRightItemWithTitle:LS(@"保存") selector:@selector(saveBtnClick:)];
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NormalTableViewCell *cell = [NormalTableViewCell cellWithType:NormalCellType_Checked identifier:@"Cell" tableView:tableView];
    cell.type = indexPath.row == 0 ? NormalCellType_None : NormalCellType_TopLine;
    cell.textLabel.text = self.datas[indexPath.item];
    cell.bChecked = [self.sex integerValue] == indexPath.row ;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.sex = [NSString intValue:indexPath.row];
    [self.tableView reloadData];
}

-(void)saveBtnClick:(id)sender{
    self.editModel.sex = self.sex;
    [self.navigationController popViewControllerAnimated:YES];
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
