//
//  NewDynamicHeader.m
//  QBH
//
//  Created by 陳景雲 on 2017/3/2.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NewDynamicHeader.h"
#import "ImageCollectionViewCell.h"
#import "RCTextView.h"
#import "LLImagePickerController.h"
#import "LLAssetManager.h"

//#import "IPickerViewController.h"
//#import "IPAssetManager.h"
#import "LLUtils.h"
#import <AssetsLibrary/AssetsLibrary.h>

#import "DynamicUploadManager.h"
#import "LLImageShowController.h"
#import "RCImagePickerVC.h"
#import "UploadCollectionCell.h"
#import "NetVideoPreviewVC.h"
#import "NetImagePreviewVC.h"

#define MaxCount 8

@interface NewDynamicHeader ()<UICollectionViewDelegate,UICollectionViewDataSource,ImageCollectionViewCellDelegate,RCImagePickerVCDelegate,UINavigationControllerDelegate,NetImagePreviewVCDelegate>

@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) RCImagePickerVC *picker;


@property (nonatomic, strong) NSString * videoPath;
@property (nonatomic, strong) ALAsset * asset;
@property (nonatomic, strong) ALAssetsLibrary * assetslibrary;
@property (nonatomic) NSInteger maxCount;

@property (nonatomic, strong) UILongPressGestureRecognizer *longPress;

@end

@implementation NewDynamicHeader

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self createViews];
        self.maxCount = 9;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self createViews];
    }
    return self;
}


-(void)createViews{
    
    self.backgroundColor = [UIColor whiteColor];
    
    self.images = [NSMutableArray array];
    
    self.textView = [[RCTextView alloc]init];
    self.textView.placeholder = LS(@"描述");
    self.textView.font = [UIFont systemFontOfSize:14];
    [self addSubview:self.textView];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.right.equalTo(self.mas_right).offset(-20);
        make.top.equalTo(self.mas_top).offset(20);
        make.height.mas_equalTo(120);
    }];
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:[self layout]];
    self.collectionView.clipsToBounds = NO;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.collectionView registerClass:[UploadCollectionCell class] forCellWithReuseIdentifier:@"Cell"];
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 20, 0, 20);
    self.collectionView.clipsToBounds = NO;
    
    _longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(lonePressMoving:)];
    [self.collectionView addGestureRecognizer:_longPress];
    
    [self addSubview:self.collectionView];
    
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView.mas_bottom).offset(20);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom).offset(-10);
    }];
    
//    self.assetslibrary = [[ALAssetsLibrary alloc]init];

}


-(void)computeHeight{
    CGFloat row = ceilf((self.images.count + 1) / 4.0);
    
    CGFloat width = (APP_WIDTH - 40  - 30) / 4;
    CGFloat height = 170 + row * (width + 10);
    

    self.height = [NSString stringWithFormat:@"%0.0f",height];

}


-(UICollectionViewFlowLayout *)layout{
    CGFloat width = (APP_WIDTH - 40  - 30) / 4;
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.minimumInteritemSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    layout.itemSize = CGSizeMake(width, width);
    return layout;
}


-(CABasicAnimation *)scaleTo:(CGFloat)scale{
    CABasicAnimation* shake = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    
    //设置抖动幅度
//    shake.fromValue = [NSNumber numberWithFloat:-0.05];
    
    shake.toValue = [NSNumber numberWithFloat:scale];
    
    shake.duration = 0.3;
    
    shake.fillMode = kCAFillModeForwards;
    
    shake.removedOnCompletion = NO;
//    shake.autoreverses = YES; //是否重复
    
    
    
    return shake;
}

#pragma mark -
#pragma mark  image picker

-(void)imagePickerController:(LLImagePickerController *)picker didFinishPickingImages:(NSArray<LLAssetModel *> *)assets withError:(NSError *)error{
    WEAK_SELF;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (LLAssetModel *asset in assets) {
            NSData *data = [[LLAssetManager sharedAssetManager] fetchImageDataFromAssetModel:asset];
            UIImage *image = [[UIImage alloc]initWithData:data];
            UploadObject *obj = [[UploadObject alloc]init];
            obj.image = image;
            [weakSelf.images addObject:obj];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.collectionView reloadData];
        });
        
        [weakSelf computeHeight];
        
    });
    
    [picker dismissViewControllerAnimated:YES completion:NULL];

}

-(void)imagePickerControllerDidCancel:(LLImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)imagePickerController:(RCImagePickerVC *)picker didFinishPickingVideo:(NSString *)videoPath{
    UIImage *image = [LLUtils getVideoThumbnailImage:videoPath];
    self.maxCount = 1;
    UploadObject *obj = [[UploadObject alloc]init];
    obj.image = image;
    obj.videoUrl = videoPath;
    
    [self.images addObject:obj];
    [self.collectionView reloadData];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}



#pragma mark -
#pragma mark image preview  

-(NSUInteger)numOfPreviewer:(id)obj{
    return self.images.count;
}

-(UIImageView *)previewImage:(id)obj atIndex:(NSInteger)index{
    UploadCollectionCell *cell  = (UploadCollectionCell *) [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
    return cell.imageView;
}

-(NSString *)originalImage:(id)obj atIndex:(NSInteger)index{
//    UploadObject *upObj = self.images[index];
//    return upObj.image;
    return nil;
}


#pragma mark -
#pragma mark collection view

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.item == self.images.count) {

        self.picker = [[RCImagePickerVC alloc]init];
        self.picker.pickerDelegate = self;
        self.picker.pickerType = self.images.count > 0 ? RCImagePickerType_Photo : RCImagePickerType_All;
        [self.vc presentViewController:self.picker animated:YES completion:NULL];
    }else{
        if (self.maxCount == 1) {
            UploadObject *uploadObj = [self.images objectAtIndex:indexPath.item];
            NetVideoPreviewVC *vc = [[NetVideoPreviewVC alloc]init];
            vc.link = uploadObj.videoUrl;
            [[self ex_viewController] presentViewController:vc animated:YES completion:NULL];
        }else{
            NetImagePreviewVC *vc = [[NetImagePreviewVC alloc]init];
            vc.delegate = self;
            vc.showIndex = indexPath.item;
            [vc show];
        }
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return MIN(self.images.count + 1 , self.maxCount);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UploadCollectionCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    if (indexPath.item  == self.images.count){
        cell.imageView.image = [UIImage imageNamed:@"ic_user_info_add"];
        cell.playImage.hidden = YES;
    }else{
        cell.uploadObj = [self.images objectAtIndex:indexPath.item];
    }
    
    
    return cell;
}

-(BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

-(void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    
    NSLog(@"交换");
    [self.images exchangeObjectAtIndex:sourceIndexPath.item withObjectAtIndex:destinationIndexPath.item];

}



- (void)lonePressMoving:(UILongPressGestureRecognizer *)longPress
{
    
    switch (_longPress.state) {
        case UIGestureRecognizerStateBegan: {
            {
                
                NSIndexPath *selectIndexPath = [self.collectionView indexPathForItemAtPoint:[_longPress locationInView:self.collectionView]];
                // 找到当前的cell
                ImageCollectionViewCell *cell = (ImageCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:selectIndexPath];
                [cell.layer addAnimation:[self scaleTo:1.2] forKey:@"Scale"];
                [self.collectionView bringSubviewToFront:cell];
                [_collectionView beginInteractiveMovementForItemAtIndexPath:selectIndexPath];
            }
            break;
        }
        case UIGestureRecognizerStateChanged: {
            [self.collectionView updateInteractiveMovementTargetPosition:[longPress locationInView:_longPress.view]];
            break;
        }
        case UIGestureRecognizerStateEnded: {
            
            NSIndexPath *selectIndexPath = [self.collectionView indexPathForItemAtPoint:[_longPress locationInView:self.collectionView]];
            // 找到当前的cell
            ImageCollectionViewCell *cell = (ImageCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:selectIndexPath];
            [cell.layer removeAllAnimations];
            
            [self.collectionView endInteractiveMovement];
            [self.collectionView reloadData];
            break;
        }
        default: [self.collectionView cancelInteractiveMovement];
            break;
    }
}




-(void)deleteCell:(UploadCollectionCell *)cell{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
    if (indexPath){
        [self.images removeObjectAtIndex:indexPath.item ];
        if (self.images.count == MaxCount - 1 )
            [self.collectionView reloadData];
        else
            [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
        
        
        [self computeHeight];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
