//
//  OtherHomePageHeader.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/24.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "OtherHomePageHeader.h"
#import "UserImageEditor.h"
#import "SexAgeView.h"
#import "UserModel.h"
#import "UserHeaderTipBar.h"


@interface OtherHomePageHeader()

@property(nonatomic,strong) UIImageView *backgroundImage;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) SexAgeView *sexView;
@property(nonatomic,strong) UILabel *locationLbl;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UILabel *tagLbl;
@property(nonatomic,strong) UIImageView *userImage;


@property(nonatomic,strong) UserHeaderTipBar *tipBar;


@property(nonatomic,strong) UserImageEditor *imageEditor;

@end

@implementation OtherHomePageHeader

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
        
        [RACObserve(self, userModel) subscribeNext:^(UserModel *x) {
            self.nameLbl.text = x.username;
            self.locationLbl.text = x.city_name;
            self.descLbl.text = x.desc;
            [self.userImage setImageName:x.image placeholder:nil];
            [self.backgroundImage setImageName:x.background placeholder:[UIImage imageNamed:@"ic_user_bg"]];
            
            self.sexView.sex = [x.sex integerValue];
            self.sexView.age = [x.age integerValue];
            self.sexView.content = x.constellation;
            
            self.tagLbl.hidden = [x.sex_orientation length] == 0 && [x.make_friends length] == 0;
            self.tagLbl.text = [NSString stringWithFormat:@" %@ %@ ",x.sex_orientation,x.make_friends];
            
            NSString *follow = @"0";
            NSString *fans = @"0";
            NSString *dynamic = @"0";
            
            
            if (x.info){
                follow = x.info.follow;
                fans = x.info.fans;
                dynamic = x.info.dynamic;
            }
            UserModel *model = [[UserModel alloc]init];
            model.member_id = x.member_id;
            
            NSMutableDictionary *dict1 = [@{@"title":LS(@"关注"),@"count":follow,@"vc":@"MineRelationVC",@"param":@(RelationType_Follow)} mutableCopy];
            NSMutableDictionary *dict2 = [@{@"title":LS(@"粉丝"),@"count":fans,@"vc":@"MineRelationVC",@"param":@(RelationType_Fans)} mutableCopy];
            
            [dict1 setValue:x.member_id forKey:@"member_id"];
            [dict2 setValue:x.member_id forKey:@"member_id"];
            
            self.tipBar.dataSource = @[dict1,
                                       dict2,
                                       @{@"title":LS(@"动态"),@"count":dynamic,@"vc":@"DynamicListVC",@"param":model}
                                       ];
            
            self.imageEditor.datas =  [x.images mutableCopy];//[x.images JSONObject];
            
            CGFloat nameHeight = [x.username heightWithFont:[UIFont systemFontOfSize:14] inWidth:(APP_WIDTH - 20)];
            CGFloat descHeight = [x.desc heightWithFont:[UIFont systemFontOfSize:12] inWidth:(APP_WIDTH - 20)];
            CGFloat height = 200 + 10 + nameHeight + 10 + descHeight + 30 + 65 + self.imageEditor.height ;
            //            self.height =  height;
            
            [self setFrameSizeHeight:height];
            self.frameHeight = [NSString stringWithFormat:@"%f",height];
        }];
    }
    return self;
}

-(void)commonInit{
    self.backgroundColor = [UIColor whiteColor];
    [self.backgroundImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.top.equalTo(self.mas_top);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(200);
    }];
    
    
    
    [self.userImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.centerY.equalTo(self.backgroundImage.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userImage.mas_right).offset(10);
        make.top.equalTo(self.backgroundImage.mas_bottom).offset(5);
    }];
    
//    [self.sexView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.nameLbl.mas_right);
//        make.centerY.equalTo(self.nameLbl.mas_centerY);
//        make.size.height.mas_equalTo(16);
//    }];
    
    [self.sexView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_right).offset(5);
//        make.top.equalTo(self.nameLbl.mas_bottom).offset(5);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
        make.height.mas_equalTo(15);
    }];
    
    [self.tagLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.sexView.mas_right).offset(2);
        make.centerY.equalTo(self.sexView.mas_centerY);
    }];

    
    [self.locationLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
    }];
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.nameLbl.mas_bottom).offset(10);
        make.right.equalTo(self.mas_right).offset(-10);
    }];
    
    [self.tipBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
//        make.height.mas_equalTo(50);
//        make.top.equalTo(self.descLbl.mas_bottom).offset(10);
        make.height.mas_equalTo(65);
        make.top.equalTo(self.descLbl.mas_bottom).offset(30);

    }];
    
    [self.imageEditor mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom);
        make.top.equalTo(self.tipBar.mas_bottom);
        //        make.height.mas_equalTo(APP_WIDTH / 4);
    }];
}

#pragma mark -
#pragma mark lazy controller

-(UILabel *)tagLbl{
    if (!_tagLbl) {
        _tagLbl = [ControllerHelper autoFitLabel];
        _tagLbl.layer.masksToBounds = YES;
        _tagLbl.layer.cornerRadius = 3.0;
        _tagLbl.backgroundColor = RGB16(0xe9eaeb);
        _tagLbl.textColor = AppOrange;
        _tagLbl.font = SystemFont(10);
        [self addSubview:_tagLbl];
    }
    return _tagLbl;
}



-(UserImageEditor *)imageEditor{
    if (!_imageEditor) {
        _imageEditor = [[UserImageEditor alloc]init];
        _imageEditor.hideAction = YES;
        
        _imageEditor.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self addSubview:_imageEditor];
    }
    
    return _imageEditor;
}


-(SexAgeView *)sexView{
    if (!_sexView) {
        _sexView = [[SexAgeView alloc]init];
        [self addSubview:_sexView];
    }
    return _sexView;
}

-(UIImageView *)userImage{
    if (!_userImage) {
        _userImage = [[UIImageView alloc]init];
        _userImage.contentMode = UIViewContentModeScaleAspectFill;
        _userImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _userImage.image = [UIImage imageNamed:@"ic_user_bg"];
        _userImage.clipsToBounds = YES;
        _userImage.layer.masksToBounds = YES;
        _userImage.layer.cornerRadius = 25;
        _userImage.layer.borderColor = [UIColor whiteColor].CGColor;
        _userImage.layer.borderWidth = 1.0;
        [self addSubview:_userImage];

    }
    return _userImage;
}


-(UIImageView *)backgroundImage{
    if (!_backgroundImage) {
        _backgroundImage = [[UIImageView alloc]init];
        _backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
        _backgroundImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _backgroundImage.image = [UIImage imageNamed:@"ic_user_bg"];
        _backgroundImage.clipsToBounds = YES;
        [self addSubview:_backgroundImage];
    }
    return _backgroundImage;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        _nameLbl.font = UserNameFont;
//        [_nameLbl setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal | UILayoutConstraintAxisVertical];
        [self addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(UILabel *)locationLbl{
    if (!_locationLbl) {
        _locationLbl = [[UILabel alloc]init];
        _locationLbl.textColor = AppGray;
        _locationLbl.font = DetailFont;
        [_locationLbl setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal | UILayoutConstraintAxisVertical];
        [self addSubview:_locationLbl];
    }
    
    return _locationLbl;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [[UILabel alloc]init];
        _descLbl.font = DetailFont;
        _descLbl.textColor = AppGray;
        _descLbl.numberOfLines = 0;
        [_descLbl setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical | UILayoutConstraintAxisHorizontal ];
        [self addSubview:_descLbl];
    }
    return _descLbl;
}

-(UserHeaderTipBar *)tipBar{
    if (!_tipBar) {
        _tipBar = [[UserHeaderTipBar alloc]init];
        [self addSubview:_tipBar];
    }
    return _tipBar;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
