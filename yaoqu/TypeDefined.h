//
//  TypeDefined.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#ifndef TypeDefined_h
#define TypeDefined_h
#import <UIKit/UIKit.h>


typedef void(^BaseBlock)(id responseObj);

typedef NS_ENUM(NSUInteger, SexType) {
    Sex_Female = 0,
    Sex_Male = 1,
    Sex_Secret = 2,
};

typedef NS_ENUM(NSUInteger,LoginType) {
    LoginType_Wechat = 0,
    LoginType_QQ = 1,
};

typedef NS_ENUM(NSUInteger, NormalCellType) {
    NormalCellType_Default,
    NormalCellType_Access,
    NormalCellType_Detail,
    NormalCellType_Switch,
    NormalCellType_Access_Detail,
    NormalCellType_Checked,
};


typedef NS_ENUM(NSUInteger,NormalCellLineType) {
    NormalCellType_None,
    NormalCellType_TopLine,
    NormalCellType_BottomLine,
    NormalCellType_Either
};


typedef NS_ENUM(NSInteger,RCImagePickerType) {
    RCImagePickerType_All,
    RCImagePickerType_Video,
    RCImagePickerType_Photo
};

typedef NS_ENUM(NSInteger,RechargeType) {
    RechargeType_Money = 0,
    RechargeType_Currency = 1,
    RechargeType_Integral = 2,
    RechargeType_Rollgift = 3,
    RechargeType_TravelEnroll = 88,
    RechargeType_OpenMember = 99,
};

typedef NS_ENUM(NSInteger,PayType) {
    PayType_AliPay,
    PayType_WeChat,
};

typedef NS_ENUM(NSInteger,CommonPayViewType) {
    CommonPayViewType_Wallet,
    CommonPayViewType_TravelEnroll,
    CommonPayViewType_OpenMember,
};

typedef NS_ENUM(NSInteger,GiftType) {
    GiftType_ForChat = 1,
    GiftType_ForDynamic = 2,
    GiftType_ForTrip = 3,
    GiftType_ForHomePage = 4,
};


typedef NS_ENUM(NSInteger,GiftSearchType) {
    GiftSearchType_Receive = 0,
    GiftSearchType_Send =1,
};

typedef NS_ENUM(NSInteger,ErrorCodeType) {
    ErrorCodeType_UnkownError = 8888,
    ErrorCodeType_Success = 200,
    ErrorCodeType_Login = 201,
    ErrorCodeType_NetWorkError = 9999,
    ErrorCodeType_None = 0,
    ErrorCodeType_AuthenticationFailed = 401,
    ErrorCodeType_NotEnoughMoney = 453,
    ErrorCodeType_GiftRollNotEnough = 402,//礼物券不足
    ErrorCodeType_StockNotEnough = 404,//库存不足
    
};

typedef NS_ENUM(NSInteger,DiscoverRankType) {
    DiscoverRankType_Like,
    DiscoverRankType_Follow,
    DiscoverRankType_Gift,
    DiscoverRankType_Currency
};


typedef NS_ENUM(NSInteger,AddressType) {
    AddressType_Location,
    AddressType_HomeTown
};

typedef NS_ENUM(NSInteger,RelationType) {
    RelationType_Friend,
    RelationType_Fans,
    RelationType_Follow
};

typedef NS_ENUM(NSInteger,CollectType) {
    CollectType_Dynamic,
    CollectType_Travel,
};

typedef NS_ENUM(NSInteger,MessageCenterType) {
    MessageCenterType_System,
    MessageCenterType_Like,
    MessageCenterType_Comment,
    MessageCenterType_LookAtMe,
    MessageCenterType_Invitation,
};


typedef NS_ENUM(NSInteger,ApproveType) {
    ApproveType_None = 0,
    ApproveType_Success = 1,
    ApproveType_Ing = 2,
    ApproveType_Fail = 3,
};

typedef NS_ENUM(NSInteger,DiscoverySearchType) {
    DiscoverySearchType_User = 0,
    DiscoverySearchType_Travel = 1,
    DiscoverySearchType_Dyanmic = 2,
};

#define AliPayNotification @"AliPayNotification"
#define WeChatPayNotification @"WeChatPayNotification"

#endif /* TypeDefined_h */
