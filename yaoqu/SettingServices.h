//
//  SettingServices.h
//  QBH
//
//  Created by 陳景雲 on 2016/12/25.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SettingServices : NSObject<UIApplicationDelegate>

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

@end
