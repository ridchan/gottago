//
//  RCUserRelationManager.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/12.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RCUserRelationManager : NSObject

+(instancetype)sharedManager;


-(void)followMember:(NSString *)member_id block:(BaseBlock)block;
-(void)unFollowMember:(NSString *)member_id block:(BaseBlock)block;

-(void)blackMember:(NSString *)memebr_id block:(BaseBlock)block;
-(void)unBlackMember:(NSString *)memebr_id block:(BaseBlock)block;


@end
