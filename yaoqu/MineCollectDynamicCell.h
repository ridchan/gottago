//
//  MineCollectDynamicCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/24.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DynamicModel.h"

@interface MineCollectDynamicCell : UITableViewCell

@property(nonatomic,strong) DynamicModel *model;

@end
