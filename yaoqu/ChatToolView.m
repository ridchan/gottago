//
//  ChatToolView.m
//  shiyi
//
//  Created by 陳景雲 on 16/6/24.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import "ChatToolView.h"

#pragma mark --

#pragma mark cell


@implementation ChatToolCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.imageView = [[UIImageView alloc]init];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.imageView];
        
        self.label = [[UILabel alloc]init];
        self.label.font = [UIFont systemFontOfSize:12];
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.textColor = [UIColor lightGrayColor];
        [self.contentView addSubview:self.label];
        
        [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left);
            make.right.equalTo(self.contentView.mas_right);
            make.bottom.equalTo(self.contentView.mas_bottom);
            make.height.mas_equalTo(20);
        }];
        
        [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left);
            make.right.equalTo(self.contentView.mas_right);
            make.top.equalTo(self.contentView.mas_top);
            make.bottom.equalTo(self.label.mas_top);
        }];
 
        
    }
    return self;
}

@end

#pragma mark --

#pragma mark view

@implementation ChatToolView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.items = @[@{@"image":@"对话相册",@"name":@"相册"},@{@"image":@"对话相机",@"name":@"相机"}];
        [self createViews];
    }
    return self;
}


-(void)createViews{
    self.backgroundColor =  [UIColor groupTableViewBackgroundColor];
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:[self layout]];
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.pagingEnabled = YES;
    self.collectionView.contentInset = UIEdgeInsetsMake(10, 10, 10, 10);
    [self.collectionView registerClass:[ChatToolCell class] forCellWithReuseIdentifier:@"Cell"];
    [self addSubview:self.collectionView];
    
    [self.collectionView reloadData];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
        make.right.equalTo(self.mas_right);
    }];
    

    
}

-(UICollectionViewFlowLayout *)layout{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    layout.itemSize = CGSizeMake(50, 60);
    return layout;
}


#pragma mark -
#pragma mark collection delegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.item == 1){
       // [self routerEventWithType:EventMessageToolCarema userInfo:nil];
    }else{
       // [self routerEventWithType:EventMessageToolAlbum userInfo:nil];
    }
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.items.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ChatToolCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.label.text = [self.items[indexPath.item] objectForKey:@"name"];
    cell.imageView.image = [UIImage imageNamed:[self.items[indexPath.item] objectForKey:@"image"]];
    
    return cell;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
