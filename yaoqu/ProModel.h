//
//  ProModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/4.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"


@interface DisModel : BaseModel

@property(nonatomic,strong) NSString *id;
@property(nonatomic,strong) NSString *dis_id;
@property(nonatomic,strong) NSString *dis;
@property(nonatomic,strong) NSString *father_id;

@end

@interface CityModel : BaseModel

@property(nonatomic,strong) NSString *city_id;
@property(nonatomic,strong) NSString *city;
@property(nonatomic,strong) NSString *city_name;
@property(nonatomic,strong) NSString *address;
@property(nonatomic,strong) NSString *father_id;
@property(nonatomic,strong) NSString *latitude;
@property(nonatomic,strong) NSString *longitude;
@property(nonatomic,strong) NSMutableArray <DisModel *>* dis_list;

@end



@interface ProModel : BaseModel

@property(nonatomic,strong) NSString *pro_id;
@property(nonatomic,strong) NSString *pro;
@property(nonatomic,strong) NSMutableArray <CityModel *>* city_list;

@end

