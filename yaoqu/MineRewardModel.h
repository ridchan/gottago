//
//  MineRewardModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/10.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserModel.h"

@interface MineRewardModel : UserModel

@property(nonatomic,strong) NSString *time;

@end
