//
//  MineLevelVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/7.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineLevelVC.h"
#import "UserImageView.h"
#import "SexView.h"
#import "LevelView.h"
#import "SexAgeView.h"
#import "UserLevelView.h"

@interface MineLevelVC ()

@property(nonatomic,strong) UserLevelView *levelView;

@end

@implementation MineLevelVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self commonInit];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)commonInit{
    
    self.title = LS(@"等级");
    [self setRightItemWithTitle:LS(@"描述") selector:nil];
    
    [self.levelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top).offset(70);
        make.right.equalTo(self.view.mas_right);
        make.height.mas_equalTo(200);
    }];
}



-(UserLevelView *)levelView{
    if (!_levelView) {
        _levelView = [[UserLevelView alloc]init];
        [self.view addSubview:_levelView];
    }
    
    return _levelView;
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
