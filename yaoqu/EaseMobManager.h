//
//  EaseMobManager.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UserModel;
@interface EaseMobManager : NSObject

+(instancetype)sharedManager;

-(void)login:(UserModel *)userModel;
-(void)logout;

@end
