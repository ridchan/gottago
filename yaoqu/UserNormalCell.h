//
//  UserNormalCell.h
//  yaoqu
//
//  Created by ridchan on 2017/7/17.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SexAgeView.h"
#import "UserImageView.h"

typedef NS_ENUM(NSInteger,UserCellType) {
    UserCellType_Normal,
    UserCellType_Date,
    UserCellType_Date_Desc,
    UserCellType_Date_Image
};

@interface UserNormalCell : UITableViewCell

@property(nonatomic) UserCellType cellType;

@property(nonatomic,strong) UserImageView *userImage;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) SexAgeView *sexView;
@property(nonatomic,strong) UILabel *dateLbl;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UIImageView *contentImage;
@property(nonatomic,strong) UIView *line;



+(UserNormalCell *)normalCellType:(UserCellType)cellType identify:(NSString *)identify;

@end
