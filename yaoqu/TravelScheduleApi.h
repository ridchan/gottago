//
//  TravelScheduleApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/18.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface TravelScheduleApi : BaseApi

-(id)initWithSchedule:(NSString *)schedule_id;

@end
