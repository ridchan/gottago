//
//  ContactAddVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/12.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "ContactAddVC.h"
#import "TravelContactEditCell.h"
#import "STPickerView.h"
#import "STPickerSingle.h"
#import "STPickerDate.h"
#import "CommonConfigManager.h"
#import "TravelContactAddApi.h"
#import "ContactApi.h"

@interface ContactAddVC ()<UITableViewDelegate,UITableViewDataSource,STPickerDateDelegate,STPickerSingleDelegate>


@property(nonatomic,strong) NSArray *cardTypeArray;
@property(nonatomic,strong) TravelContactModel *editModel;

@end

@implementation ContactAddVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"添加联系人");
    
    self.cardTypeArray = [CommonConfigManager sharedManager].hostModel.card_type;
    
    if (self.paramObj){
        self.editModel = self.paramObj;
    }else{
        self.editModel = [[TravelContactModel alloc]init];
        self.editModel.sex = @"1";
        self.editModel.is_default = 1;
        CardTypeModel *model = [self.cardTypeArray firstObject];
        self.editModel.card_type = model.card_type_name;
    }
    
    
    
    
    
    [self setRightItemWithTitle:LS(@"保存") selector:@selector(saveBtnClick:)];
    
    self.tableView.contentInset = UIEdgeInsetsMake(10, 0, 0, 0);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.separatorColor = AppLineColor;
    
    self.datas = [@[@{@"title":LS(@"姓名"),
                     @"placeholder":LS(@"姓名"),
                     @"type":@(EditCellType_TextField)
                     },
                   @{@"title":LS(@"性别"),
                     @"type":@(EditCellType_Sex)
                     },
                   @{@"title":LS(@"手机号码"),
                     @"placeholder":LS(@"手机号码"),
                     @"type":@(EditCellType_TextField)
                     },
                   @{@"title":LS(@"邮箱"),
                     @"placeholder":LS(@"邮箱"),
                     @"type":@(EditCellType_TextField)
                     },
                   @{@"title":LS(@"证件类型"),
                     @"placeholder":LS(@"证件类型"),
                     @"text":LS(@"ID Card"),
                     @"type":@(EditCellType_IdCard)
                     },
                   @{@"title":LS(@"证件编号"),
                     @"placeholder":LS(@"证件编号"),
                     @"type":@(EditCellType_TextField)
                     },
                   @{@"title":LS(@"生日"),
                     @"placeholder":LS(@"出生日期"),
                     @"type":@(EditCellType_Birthday)
                     },
                   ] mutableCopy];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)saveBtnClick:(id)sender{
    WEAK_SELF;
    ContactApi *api = [[ContactApi alloc]initWitObject:self.editModel];
    api.method = YTKRequestMethodPOST;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
        [LLUtils showCenterTextHUD:info.message];
    }];
//    [[[TravelContactAddApi alloc]initWithModel:self.editModel] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
//        
//    }];
    NSLog(@"%@",[self.editModel keyValues]);
}

-(NSString *)cardTypeDescription:(NSString *)card_type{
    for (CardTypeModel *model in self.cardTypeArray) {
        if ([model.card_type_id integerValue] == [card_type integerValue]) {
            return model.card_type_name;
        }
    }
    return @"";
}


#pragma mark -


-(void)pickerDate:(STPickerDate *)pickerDate year:(NSInteger)year month:(NSInteger)month day:(NSInteger)day{
    self.editModel.birthday = [NSString stringWithFormat:@"%ld/%ld/%ld",year,month,day];
    NSLog(@"%ld/%ld/%ld",year,month,day);
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:6 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)pickerSingle:(STPickerSingle *)pickerSingle selectedTitle:(NSString *)selectedTitle AtIndex:(NSInteger)index{
    CardTypeModel *model = self.cardTypeArray[index];
    self.editModel.card_type = model.card_type_name;
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:4 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}



#pragma mark -
#pragma mark table view


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
    if (indexPath.row == 6){
        STPickerDate *pickerDate = [[STPickerDate alloc]init];
        [pickerDate setYearLeast:1900];
        [pickerDate setYearSum:117];
        [pickerDate setDelegate:self];
        [pickerDate show];
    }else if (indexPath.row == 4){
        
        NSMutableArray *type = [NSMutableArray array];
        for (CardTypeModel *model in self.cardTypeArray){
            [type addObject:model.card_type_name];
        }
        
        STPickerSingle *pickerSingle = [[STPickerSingle alloc]init];
        pickerSingle.widthPickerComponent = APP_WIDTH;
        [pickerSingle setArrayData:type];
        [pickerSingle setTitle:@""];
        [pickerSingle setDelegate:self];
        [pickerSingle show];

    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [self footerView];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TravelContactEditCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[TravelContactEditCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    NSDictionary *dic = self.datas[indexPath.row];
    cell.cellType = [dic[@"type"] integerValue];
    cell.titleLbl.text = [dic objectForKey:@"title"];
    cell.textField.placeholder = [dic objectForKey:@"placeholder"];
//    cell.textField.text = [dic objectForKey:@"text"];
    
    
    switch (indexPath.row) {
        case 0:
            cell.textField.text = self.editModel.fullname;
            break;
        case 1:
            cell.sex = self.editModel.sex;
            break;
        case 2:
            cell.textField.text = self.editModel.phone;
            break;
        case 3:
            cell.textField.text = self.editModel.email;
            break;
        case 4:
            cell.textField.text = [self cardTypeDescription:self.editModel.card_type];
            break;
        case 5:
            cell.textField.text = self.editModel.card_num;
            break;
        case 6:
            cell.textField.text = self.editModel.birthday;
            break;
        default:
            break;
    }
        
    
    [[cell.textField.rac_textSignal distinctUntilChanged] subscribeNext:^(id x) {
        switch (indexPath.row) {
            case 0:
                self.editModel.fullname = x;
                break;
            case 2:
                self.editModel.phone = x;
                break;
            case 3:
                self.editModel.email = x;
                break;
            case 5:
                self.editModel.card_num = x;
                break;
            default:
                break;
        }
    }];
    
    
    [RACObserve(cell, sex) subscribeNext:^(id x) {
        if (indexPath.row == 1){
            self.editModel.sex = x;
        }
    }];

    
    
    return cell;
}

-(UIView *)footerView{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor whiteColor];
    
    ImageButton *imageBtn = [[ImageButton alloc]init];
    imageBtn.contentImage.image = [UIImage imageNamed:@"ic_id_select"];
    imageBtn.titleLbl.text = LS(@"默认联系人");
    [imageBtn addTarget:self action:@selector(defaultBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:imageBtn];
    
    [imageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view.mas_right).offset(-5);
        make.centerY.equalTo(view);
        make.height.mas_equalTo(30);
    }];
    
    
    return view;
}


-(void)defaultBtnClick:(ImageButton *)imageBtn{
    self.editModel.is_default = !self.editModel.is_default;
    imageBtn.contentImage.image = self.editModel.is_default ? [UIImage imageNamed:@"ic_id_select"] : [UIImage imageNamed:@"ic_id_select_no"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
