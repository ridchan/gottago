//
//  HistoryModel.h
//  yaoqu
//
//  Created by ridchan on 2017/9/4.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface HistoryModel : BaseModel

@property(nonatomic,strong) NSString *content;
@property(nonatomic,strong) NSString *count;
@property(nonatomic,strong) NSString *keyword;
@property(nonatomic,strong) NSString *search_id;
@property(nonatomic) long long time;
@property(nonatomic) long long update_time;

@end
