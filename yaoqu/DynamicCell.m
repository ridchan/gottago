//
//  DynamicCell.m
//  yaoqu
//
//  Created by ridchan on 2017/6/28.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicCell.h"
#import "UserImageView.h"
#import "SexAgeView.h"
#import "LevelView.h"
#import "UploadCollectionCell.h"
#import "DynamicLikeApi.h"
#import "DynamicApi.h"
#import "DynamicDetailVC.h"

@interface DynamicCell()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UserImageView *userImage;
@property(nonatomic,strong) SexAgeView *sexView;
@property(nonatomic,strong) UIImageView *goldView;
@property(nonatomic,strong) LevelView *levelView;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) UILabel *dateLbl;
@property(nonatomic,strong) UILabel *locationLbl;
@property(nonatomic,strong) UILabel *descLbl;

@property(nonatomic,strong) ImageButton *commentBtn;
@property(nonatomic,strong) ImageButton *likeBtn;
@property(nonatomic,strong) ImageButton *viewBtn;

@property(nonatomic,strong) UICollectionView *collectionView;

@property(nonatomic,strong) NSArray *imgDatas;

@property(nonatomic,strong) UIImageView *playImage;

@end

@implementation DynamicCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self commonInit2];
        [self rac_prepareForReuseSignal];
        [RACObserve(self, model) subscribeNext:^(DynamicModel *model) {
            self.nameLbl.text = model.username;
            self.descLbl.text = model.content;
            self.levelView.level = model.level;

            self.userImage.userModel = [model objectWithClass:@"UserModel"];
            self.sexView.sex = [model.sex integerValue];
            self.sexView.age = [model.age integerValue];
            self.sexView.content = nil;
            
            self.commentBtn.titleLbl.text = model.comment;
            
            self.viewBtn.titleLbl.text = model.preview;
            if ([model.dynamic_city_name length] > 0){
                self.dateLbl.text = [NSString stringWithFormat:@"%@.%@",[NSString compareCurrentTime:model.time],model.dynamic_city_name];
            }else{
                self.dateLbl.text = [NSString compareCurrentTime:model.time];
            }
            
            
            [self.collectionView reloadData];
        }];
        
        [RACObserve(self, model.is_like) subscribeNext:^(id x) {
            self.likeBtn.contentImage.image = [x boolValue] ? [UIImage imageNamed:@"ic_dynamic_like_on"] : [[UIImage imageNamed:@"ic_dynamic_like"] imageToColor:AppBlack];
        }];
        
        RAC(self.likeBtn.titleLbl,text) =  RACObserve(self, model.like);
    }
    return self;
}


-(void)setModel:(DynamicModel *)model{
    self.imgDatas = [model.content_images JSONObject];
    if ([model.video_image length] > 0){
        self.imgDatas = @[model.video_image];
    }
    _model = model;
    [self.collectionView reloadData];
    
    CGFloat row = ceilf(self.imgDatas.count / 3.0);
    CGFloat rowHeight = (APP_WIDTH - 30) / 3 + 10;
    CGFloat height = self.imgDatas.count > 0  ?
                        row * rowHeight :
                        0;
    
    
    self.collectionView.sd_resetLayout
    .leftSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 0)
    .topSpaceToView(self.descLbl, 10)
    .heightIs(height);
    
    
    
//    self.sexView.sd_resetLayout
//    .leftSpaceToView(self.userImage, 5)
//    .topSpaceToView(self.nameLbl, 6)
//    .heightIs(12);
    
    
}


-(void)commonInit2{
    self.userImage.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .topSpaceToView(self.contentView, 20)
    .heightIs(50)
    .widthIs(50);
    
    self.nameLbl.sd_layout
    .topSpaceToView(self.contentView, 20)
    .leftSpaceToView(self.userImage, 5)
    .heightIs(25);
    [self.nameLbl setSingleLineAutoResizeWithMaxWidth:200];
    
    self.sexView.sd_layout
    .leftSpaceToView(self.nameLbl, 5)
    .centerYEqualToView(self.nameLbl)
    .widthIs(35)
    .heightIs(15);
    
//    [self.sexView setSingleLineAutoResizeWithMaxWidth:200];
    
    
//    [self.sexView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.userImage.mas_left).offset(5);
//        make.right.equalTo(self.nameLbl.mas_bottom).offset(6);
//        make.height.mas_equalTo(12);
//    }];
    
    
    self.levelView.sd_layout
    .leftSpaceToView(self.sexView, 5)
    .centerYEqualToView(self.nameLbl)
    .heightIs(45)
    .widthIs(45);
    
    self.dateLbl.sd_layout
    .leftSpaceToView(self.userImage, 5)
    .topSpaceToView(self.nameLbl, 1)
    .heightIs(22)
    .widthIs(0);
    

    [self.dateLbl setSingleLineAutoResizeWithMaxWidth:APP_WIDTH - 100];
    
//    self.locationLbl.sd_layout
//    .rightSpaceToView(self.contentView, 10)
//    .centerYEqualToView(self.sexView)
//    .heightIs(25)
//    .autoWidthRatio(0);
//    
//    [self.locationLbl setSingleLineAutoResizeWithMaxWidth:APP_WIDTH / 2];
    

    
    self.descLbl.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .rightSpaceToView(self.contentView, 10)
    .topSpaceToView(self.userImage, 20)
    .autoHeightRatio(0);
    
    self.collectionView.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .rightSpaceToView(self.contentView, 10)
    .topSpaceToView(self.descLbl, 0)
    .heightIs(30);
    
   
    
    self.likeBtn.sd_layout
    .topSpaceToView(self.collectionView, 10)
    .rightSpaceToView(self.contentView, 10)
    .widthIs(50)
    .heightIs(26);
    

    self.commentBtn.sd_layout
    .topSpaceToView(self.collectionView, 10)
    .rightSpaceToView(self.likeBtn, 10)
    .widthIs(50)
    .heightIs(26);
    

    
    
//    self.viewBtn.sd_layout
//    .topSpaceToView(self.collectionView, 10)
//    .rightSpaceToView(self.contentView, 10)
//    .heightIs(25)
//    .widthIs(50);
//    
    
    [self setupAutoHeightWithBottomViewsArray:@[self.commentBtn, self.likeBtn] bottomMargin:10];
    
}

-(void)commonInit{
    [self.userImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.top.equalTo(self.contentView.mas_top).offset(20);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userImage.mas_right).offset(5);
        make.bottom.equalTo(self.userImage.mas_centerY);
        make.height.mas_equalTo(30);
    }];
    
    [self.sexView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.nameLbl.mas_bottom).offset(5);
        make.height.mas_equalTo(15);
        make.width.mas_equalTo(30);
    }];
    
    
    [self.levelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.sexView.mas_right).offset(5);
        make.centerY.equalTo(self.sexView.mas_centerY);
        make.height.mas_equalTo(45);
        make.width.mas_equalTo(45);
    }];
    
    [self.dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
    }];
    
    [self.locationLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.top.equalTo(self.dateLbl.mas_bottom).offset(5);
    }];
    
    
    
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userImage.mas_left);
        make.top.equalTo(self.userImage.mas_bottom).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.bottom.equalTo(self.collectionView.mas_top).offset(-10);
    }];
    
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
        make.bottom.equalTo(self.commentBtn.mas_top).offset(-10);
//        make.top.equalTo(self.descLbl.mas_bottom).offset(10);
    }];

    
    
    [self.commentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userImage.mas_left);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-10);
//        make.top.equalTo(self.collectionView.mas_bottom).offset(-10);
        make.height.mas_equalTo(25);
    }];
    
    [self.likeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.commentBtn.mas_right).offset(20);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-10);
//        make.top.equalTo(self.collectionView.mas_bottom).offset(-10);
        make.height.mas_equalTo(25);
    }];
    
    [self.viewBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-10);
//        make.top.equalTo(self.collectionView.mas_bottom).offset(-10);
        make.height.mas_equalTo(25);
    }];
    
    
    
}


#pragma mark -
#pragma mark collection view

-(UICollectionViewFlowLayout *)layout{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.minimumLineSpacing = 5;
    layout.minimumInteritemSpacing = 5;
    layout.sectionInset = UIEdgeInsetsMake(5, 10, 5, 10);
    CGFloat width = (APP_WIDTH - 30) / 3;
    layout.itemSize = CGSizeMake(width, width);
    
//    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    
    return layout;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.imgDatas count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UploadCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    [cell.imageView setImageName:[self.imgDatas[indexPath.item] fixImageString:DynamicImageFix] placeholder:nil];
    cell.playImage.hidden = [self.model.video_image length] == 0;
    return cell;
}


#pragma mark -
#pragma mark lazy layout


-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc]init];
        _nameLbl.font = DynamicNameFont;
        _nameLbl.textColor = DynamicNameColor;
        [self.contentView addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(SexAgeView *)sexView{
    if (!_sexView) {
        _sexView = [[SexAgeView alloc]init];
        [self.contentView addSubview:_sexView];
    }
    return _sexView;
}

-(UserImageView *)userImage{
    if (!_userImage) {
        _userImage = [[UserImageView alloc]init];
        [self.contentView addSubview:_userImage];
    }
    return _userImage;
}

-(UIImageView *)goldView{
    if (!_goldView) {
        _goldView = [[UIImageView alloc]init];
        _goldView.contentMode = UIViewContentModeScaleAspectFit;
        _goldView.image = [UIImage imageNamed:@"ic_user_level_1"];
        [self.contentView addSubview:_goldView];
    }
    return _goldView;
}

-(LevelView *)levelView{
    if (!_levelView) {
        _levelView = [[LevelView alloc]init];
        _levelView.levelLbl.font = self.sexView.ageLbl.font;
        [self.contentView addSubview:_levelView];
    }
    return _levelView;
}

-(UILabel *)dateLbl{
    if (!_dateLbl) {
        _dateLbl = [[UILabel alloc]init];
        _dateLbl.textAlignment = NSTextAlignmentLeft;
        _dateLbl.font = DynamicTimeFont;
        _dateLbl.textColor = DynamicTimeColor;
        [_dateLbl setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal | UILayoutConstraintAxisVertical];
        [self.contentView addSubview:_dateLbl];
    }
    return _dateLbl;
}

-(UILabel *)locationLbl{
    if (!_locationLbl) {
        _locationLbl = [ControllerHelper autoFitLabel];
        _locationLbl.font = DynamicTimeFont;
        _locationLbl.textColor = DynamicTimeColor;
        _locationLbl.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_locationLbl];
    }
    return _locationLbl;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.numberOfLines = 0;
        _descLbl.font = DynamicDescFont;
        _descLbl.textColor = DynamicDescColor;
        [self.contentView addSubview:_descLbl];
    }
    return _descLbl;
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:[self layout]];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.scrollEnabled = NO;
        _collectionView.userInteractionEnabled = NO;
        [_collectionView registerClass:[UploadCollectionCell class] forCellWithReuseIdentifier:@"Cell"];
        [self.contentView addSubview:_collectionView];
    }
    return _collectionView;
}

-(ImageButton *)commentBtn{
    if (!_commentBtn) {
        _commentBtn = [[ImageButton alloc]init];
        _commentBtn.contentImage.image = [[UIImage imageNamed:@"ic_dynamic_comment"] imageToColor:AppBlack];
        _commentBtn.titleLbl.textColor = AppBlack;
        _commentBtn.titleLbl.font = SystemFont(12);
        [_commentBtn addTarget:self action:@selector(commentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_commentBtn];
    }
    return _commentBtn;
}

-(ImageButton *)likeBtn{
    if (!_likeBtn) {
        _likeBtn = [[ImageButton alloc]init];
        _likeBtn.contentImage.image = [[UIImage imageNamed:@"ic_dynamic_like"] imageToColor:AppBlack];
        _likeBtn.titleLbl.textColor = AppBlack;
        _likeBtn.titleLbl.font = SystemFont(12);
        [_likeBtn addTarget:self action:@selector(likeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_likeBtn];
    }
    return _likeBtn;
}

-(ImageButton *)viewBtn{
    if (!_viewBtn) {
        _viewBtn = [[ImageButton alloc]init];
        _viewBtn.contentImage.image = [UIImage imageNamed:@"ic_dynamic_view"];
        [self.contentView addSubview:_viewBtn];
    }
    return _viewBtn;
}

-(UIImageView *)playImage{
    if (!_playImage) {
        _playImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_play"]];
        [self.contentView addSubview:_playImage];
    }
    return _playImage;
}


#pragma mark -
#pragma mark  action

-(void)userImageTap:(id)sender{
    
}

-(void)commentBtnClick:(id)sender{
    DynamicDetailVC *vc = [[DynamicDetailVC alloc]init];
    vc.paramObj = self.model;
    vc.isShowComment = YES;
    [[RouteController sharedManager] pushVC:vc];
}

-(void)likeBtnClick:(id)sender{
    WEAK_SELF;
    self.likeBtn.enabled = NO;
    
    NSDictionary *dict = @{@"dynamic_id":self.model.dynamic_id,
                           @"state":@(!self.model.is_like)
                           };
    
    DynamicApi *api = [[DynamicApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodOPTION;
    
    
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            weakSelf.model.is_like = !weakSelf.model.is_like;
            if (weakSelf.model.is_like)
                weakSelf.model.like = [weakSelf.model.like autoIncrice];
            else
                weakSelf.model.like =[weakSelf.model.like autoReduce];
        }
        weakSelf.likeBtn.enabled = YES;
    }];
}


@end
