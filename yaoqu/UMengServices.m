//
//  UMengServices.m
//  QBH
//
//  Created by 陳景雲 on 2017/1/8.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UMengServices.h"
#import <UMMobClick/MobClick.h>
#import <UMSocialCore/UMSocialCore.h>


@implementation UMengServices

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    
    UMConfigInstance.appKey = @"59b6391f07fe6517b300095c";//@"5871f70145297d120f001e9e";
#ifdef DEBUG
    UMConfigInstance.channelId = @"IOS DEBUG";
#else
    UMConfigInstance.channelId = @"APP Store";
#endif
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [MobClick setAppVersion:appVersion];
    
    [MobClick startWithConfigure:UMConfigInstance];
    
    [[UMSocialManager defaultManager] openLog:YES];
    
    
    
    /* 设置友盟appkey */
    [[UMSocialManager defaultManager] setUmSocialAppkey:@"5871f70145297d120f001e9e"];
    
    
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:WX_APP_ID appSecret:WX_APP_SECRET redirectURL:@"http://mobile.umeng.com/social"];
    /*
     * 移除相应平台的分享，如微信收藏
     */
    //[[UMSocialManager defaultManager] removePlatformProviderWithPlatformTypes:@[@(UMSocialPlatformType_WechatFavorite)]];
    
    /* 设置分享到QQ互联的appID
     * U-Share SDK为了兼容大部分平台命名，统一用appKey和appSecret进行参数设置，而QQ平台仅需将appID作为U-Share的appKey参数传进即可。
     */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:QQ_APP_ID/*设置QQ平台的appID*/  appSecret:nil redirectURL:@"http://mobile.umeng.com/social"];
    
    return YES;
}
    
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
    
        
}


@end
