//
//  NetVideoPreviewVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/5.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseViewController.h"
#import "SBPlayer.h"

@interface NetVideoPreviewVC : BaseViewController

@property(nonatomic,strong) NSString *link;

@end
