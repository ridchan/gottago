//
//  EasemobServices.h
//  shiyi
//
//  Created by 陳景雲 on 16/5/4.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface EasemobServices : NSObject<UIApplicationDelegate>


-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
- (void)applicationDidEnterBackground:(UIApplication *)application;
- (void)applicationWillEnterForeground:(UIApplication *)application;

@end
