//
//  UserHeaderView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/16.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserHeaderView.h"
#import "UserImageView.h"
#import "SexAgeView.h"
#import "LevelView.h"
#import "UserHeaderTipBar.h"
#import "RCUserCacheManager.h"
#import "RouteController.h"
@interface UserHeaderView()

@property(nonatomic,strong) UIImageView *backgroundImage;
@property(nonatomic,strong) UserImageView *userImage;
@property(nonatomic,strong) SexAgeView *sexView;
@property(nonatomic,strong) UIImageView *goldView;
@property(nonatomic,strong) LevelView *levelView;
@property(nonatomic,strong) UserHeaderTipBar *tipBar;
@property(nonatomic,strong) UILabel *nameLbl;

@property(nonatomic,strong) UILabel *tagLbl;

@property(nonatomic,strong) UIButton *giftBtn;
@property(nonatomic,strong) UIButton *historyBtn;

@property(nonatomic,strong) UIImageView *rightAccess;

@property(nonatomic,strong) UIButton *rightBtn;

@property(nonatomic,strong) UIView *userBgView;

@end

@implementation UserHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
        [self bindUserInfo];
        
    }
    return self;
}

-(void)bindUserInfo{
    WEAK_SELF;
    [RACObserve([RCUserCacheManager sharedManager], currentUser) subscribeNext:^(UserModel *x) {
        weakSelf.nameLbl.text = x.username;
        weakSelf.levelView.level = x.level;
        weakSelf.sexView.sex = [x.sex integerValue];
        weakSelf.sexView.age = [x.age integerValue];
        weakSelf.sexView.content = x.constellation;
        weakSelf.userImage.userModel = x;
        weakSelf.goldView.hidden = !x.is_member;
//        weakSelf.tagLbl.text = x.sex_orientation;
        [weakSelf.backgroundImage setImageName:x.background placeholder:[UIImage imageNamed:@"ic_user_bg"]];
        
        NSString *order = @"0";
        NSString *partner = @"0";
        NSString *follow = @"0";
        NSString *fans = @"0";
        NSString *dynamic = @"0";
        
        if (x.info){
            order = x.info.order;
            partner = x.info.partner;
            follow = x.info.follow;
            fans = x.info.fans;
            dynamic = x.info.dynamic;
        }
        
        weakSelf.tipBar.dataSource = @[@{@"title":LS(@"好友"),@"count":partner,@"vc":@"MineRelationVC",@"param":@(RelationType_Friend)},
                                       @{@"title":LS(@"关注"),@"count":follow,@"vc":@"MineRelationVC",@"param":@(RelationType_Follow)},
                                       @{@"title":LS(@"粉丝"),@"count":fans,@"vc":@"MineRelationVC",@"param":@(RelationType_Fans)}
                                       ];
    }];
    
    
}

-(void)setIsShowMember:(BOOL)isShowMember{
    _isShowMember = isShowMember;
    self.userImage.isShowMember = _isShowMember;
}

-(void)setIsShowTalent:(BOOL)isShowTalent{
    _isShowTalent = isShowTalent;
    self.userImage.isShowTalent = _isShowTalent;
}


-(void)commonInit{
    
    self.backgroundColor = [UIColor whiteColor];
    
    [self.tipBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom);
        make.height.mas_equalTo(80);
    }];
    
    [self.backgroundImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.top.equalTo(self.mas_top);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.tipBar.mas_top).offset(-80);
    }];
    
    [self.userBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.userImage.mas_centerX);
        make.centerY.equalTo(self.userImage.mas_centerY).offset(-4);
        make.size.mas_equalTo(CGSizeMake(90, 90));
    }];
    
    self.userBgView.layer.masksToBounds = YES;
    self.userBgView.layer.cornerRadius = 45.0;
    
    [self.userImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15);
        make.top.equalTo(self.backgroundImage.mas_bottom).offset(-40);
        make.size.mas_equalTo(CGSizeMake(80, 80));
    }];
    
    
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userBgView.mas_right).offset(10);
        make.top.equalTo(self.backgroundImage.mas_bottom).offset(5);
    }];
    
    
    [self.sexView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.nameLbl.mas_bottom).offset(5);
        make.height.mas_equalTo(15);
    }];
    
//    [self.tagLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.sexView.mas_right).offset(2);
//        make.centerY.equalTo(self.sexView.mas_centerY);
//    }];
    
    [self.levelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.sexView.mas_right).offset(5);
        make.centerY.equalTo(self.sexView.mas_centerY);
        make.height.mas_equalTo(45);
        make.width.mas_equalTo(45);
    }];
    
//    
    [self.goldView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_right).offset(5);
        make.top.equalTo(self.nameLbl.mas_top);
        make.bottom.equalTo(self.nameLbl.mas_bottom);
        make.width.mas_equalTo(18);
    }];

    
    [self.rightAccess mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.top.equalTo(self.backgroundImage.mas_bottom);
        make.bottom.equalTo(self.tipBar.mas_top)    ;
        make.width.mas_equalTo(RightAccessWidth);
    }];
    
    [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backgroundImage.mas_bottom);
        make.bottom.equalTo(self.tipBar.mas_top);
        make.right.equalTo(self.mas_right);
        make.width.mas_equalTo(APP_WIDTH / 2);
    }];
    
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)openSetting:(id)sender{
//    [[RouteController sharedManager]openClassVC:@"UserEditVC"];
    [[RouteController sharedManager]openClassVC:@"UserHomePageVC"  withObj:[RCUserCacheManager sharedManager].currentUser];
}

-(void)userImageTap:(id)sender{
//    [[RouteController sharedManager]openClassVC:@"UserHomePageVC"  withObj:[RCUserCacheManager sharedManager].currentUser];
}

-(UIButton *)rightBtn{
    if (!_rightBtn) {
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightBtn addTarget:self action:@selector(openSetting:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_rightBtn];
    }
    return _rightBtn;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc]init];
        [_nameLbl setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        _nameLbl.font = [UIFont systemFontOfSize:16];
        _nameLbl.text = @"我的帐号";
        _nameLbl.textColor = [UIColor blackColor];
        [self addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(UIImageView *)backgroundImage{
    if (!_backgroundImage ) {
        _backgroundImage = [[UIImageView alloc]init];
        _backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
        _backgroundImage.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _backgroundImage.image = [UIImage imageNamed:@"ic_user_bg"];
        _backgroundImage.clipsToBounds = YES;
        [self addSubview:_backgroundImage];
    }
    return _backgroundImage;
}

-(UserImageView *)userImage{
    if (!_userImage) {
        _userImage = [[UserImageView alloc]init];
        _userImage.isShowMember = NO;

        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userImageTap:)];
        [_userImage addGestureRecognizer:tap];
        
        [self addSubview:_userImage];
    }
    return _userImage;
}

-(UIView *)userBgView{
    if (!_userBgView) {
        _userBgView = [[UIView alloc]init];
        _userBgView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_userBgView];
    }
    return _userBgView;
}

-(SexAgeView *)sexView{
    if (!_sexView) {
        _sexView = [[SexAgeView alloc]init];
        [self addSubview:_sexView];
    }
    return _sexView;
}

-(UIImageView *)goldView{
    if (!_goldView) {
        _goldView = [[UIImageView alloc]init];
        _goldView.contentMode = UIViewContentModeScaleAspectFit;
        _goldView.image = [UIImage imageNamed:@"ic_user_level_1"];
        [self addSubview:_goldView];
    }
    return _goldView;
}

-(LevelView *)levelView{
    if (!_levelView) {
        _levelView = [[LevelView alloc]init];
        [self addSubview:_levelView];
    }
    return _levelView;
}

-(UserHeaderTipBar *)tipBar{
    if (!_tipBar) {
        _tipBar = [[UserHeaderTipBar alloc]init];
        _tipBar.titleFont = SystemFont(16);
        _tipBar.countFont = SystemFont(18);
        [self addSubview:_tipBar];
    }
    return _tipBar;
}

-(UIButton *)giftBtn{
    if (!_giftBtn) {
        _giftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_giftBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_giftBtn setTitle:LS(@"Gift") forState:UIControlStateNormal];
        _giftBtn.titleLabel.font = [UIFont boldSystemFontOfSize:12];
        [self addSubview:_giftBtn];
    }
    return _giftBtn;
}

-(UIButton *)historyBtn{
    if (!_historyBtn) {
        _historyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_historyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_historyBtn setTitle:LS(@"History") forState:UIControlStateNormal];
        _historyBtn.titleLabel.font = [UIFont boldSystemFontOfSize:12];
        [self addSubview:_historyBtn];
    }
    
    return _historyBtn;
}

-(UIImageView *)rightAccess{
    if (!_rightAccess) {
        _rightAccess = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_forward"]];
        _rightAccess.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_rightAccess];
    }
    return _rightAccess;
}

-(UILabel *)tagLbl{
    if (!_tagLbl) {
        _tagLbl = [ControllerHelper autoFitLabel];
        _tagLbl.backgroundColor = AppGray;
        _tagLbl.layer.cornerRadius = 3;
        _tagLbl.layer.masksToBounds = YES;
        _tagLbl.font = SystemFont(11);
        [self addSubview:_tagLbl];
    }
    return _tagLbl;
}

@end
