//
//  MineToCashVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/11.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineToCashVC.h"
#import "NormalTextField.h"
#import "RCUserCacheManager.h"
#import "WithDrawApi.h"
#import "YQWebViewVC.h"
#import "CommonConfigManager.h"

@interface MineToCashVC ()

@property(nonatomic,strong) UIView *headerView;
@property(nonatomic,strong) UIView *cashView;
@property(nonatomic,strong) ImageButton *bankBtn;
@property(nonatomic,strong) NormalButton *comfirmBtn;
@property(nonatomic,strong) UILabel *tipLbl;

@property(nonatomic,strong) NormalTextField *cashFld;
@property(nonatomic,strong) NormalTextField *nameFld;
@property(nonatomic,strong) NormalTextField *numFld;



@end

@implementation MineToCashVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"提现");
    [self setRightItemWithTitle:@"提现记录" selector:@selector(recordBtnClick:)];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top).offset(74);
        make.right.equalTo(self.view.mas_right);
        make.height.mas_equalTo(100);
    }];
    
    [self.bankBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(10);
        make.top.equalTo(self.headerView.mas_bottom).offset(10);
        make.height.mas_equalTo(15);
    }];
    
    [self.cashView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.bankBtn.mas_bottom).offset(10);
        make.right.equalTo(self.view.mas_right);
        make.height.mas_equalTo(50);
    }];
    
    [self.tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(10);
        make.top.equalTo(self.cashView.mas_bottom).offset(5);
        make.height.mas_equalTo(15);
    }];
    
    [self.comfirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(10);
        make.top.equalTo(self.tipLbl.mas_bottom).offset(10);
        make.right.equalTo(self.view.mas_right).offset(-10);
        make.height.mas_equalTo(50);
    }];
    
    
    RACSignal *sign1 = [self.nameFld.rac_textSignal distinctUntilChanged];
    RACSignal *sign2 = [self.numFld.rac_textSignal distinctUntilChanged];
    RACSignal *sign3 = [self.cashFld.rac_textSignal distinctUntilChanged];
    
    RAC(self.comfirmBtn,enabled) = [RACSignal combineLatest:@[sign1,sign2,sign3] reduce:^id(NSString *text1,NSString *text2,NSString *text3){
        return @([text1 length] > 0 && [text2 length] > 0 && [text3 length] > 0);
    }];
    
    
    [[[WithDrawApi alloc]init] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {

        }else{
            [LLUtils showCenterTextHUD:info.message];
        }
        
    }];

    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)recordBtnClick:(id)sender{
    YQWebViewVC *vc = [[YQWebViewVC alloc]init];
    vc.link = [[CommonConfigManager sharedManager].api_host stringByAppendingPathComponent:@"withdraw-log.html"];
    [self.navigationController pushViewController:vc animated:YES];
}


-(ImageButton *)bankBtn{
    if (!_bankBtn) {
        _bankBtn = [[ImageButton alloc]init];
        _bankBtn.titleLabel.font = SystemFont(11);
        _bankBtn.titleLbl.text = LS(@"查看可支持银行");
        _bankBtn.contentImage.image = [UIImage imageNamed:@"ic_question"];
        [_bankBtn setAutoFit];
        [self.view addSubview:_bankBtn];
    }
    return _bankBtn;
}

-(UIView *)headerView{
    if (!_headerView) {
        _headerView = [[UIView alloc]init];
        _headerView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_headerView];
        
        self.nameFld = [[NormalTextField alloc]init];
        self.nameFld.title = @"提款帐号";
        self.nameFld.placeholder = @"姓名";
        self.nameFld.centerLine.hidden = YES;
        self.nameFld.textColor = RGB16(0x333333);
        self.nameFld.titleLabel.textColor = RGB16(0x666666);
//        RAC(self.nameFld,text) = RACObserve(self, approveModel.fullname);
        [_headerView addSubview:self.nameFld];
        
        self.numFld = [[NormalTextField alloc]init];
        self.numFld.title = @"银行卡号";
        self.numFld.centerLine.hidden = YES;
        self.numFld.placeholder = @"卡号";
        self.numFld.textColor = RGB16(0x333333);
        self.numFld.titleLabel.textColor = RGB16(0x666666);
//        RAC(self.numFld,text) = RACObserve(self, approveModel.idcard);
        [_headerView addSubview:self.numFld];
        
        [self.nameFld mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_headerView.mas_top);
            make.left.equalTo(_headerView.mas_left).offset(10);
            make.right.equalTo(_headerView.mas_right);
            make.height.mas_equalTo(50);
        }];
        
        
        [self.numFld mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.nameFld.mas_bottom);
            make.left.equalTo(_headerView.mas_left).offset(10);
            make.right.equalTo(_headerView.mas_right);
            make.height.mas_equalTo(50);
            make.bottom.equalTo(_headerView.mas_bottom);
        }];
        
    }
    return _headerView;
}


-(UIView *)cashView{
    if (!_cashView) {
        _cashView = [[UIView alloc]init];
        _cashView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_cashView];
        
        self.cashFld = [[NormalTextField alloc]init];
        self.cashFld.title = @"提现金额";
        self.cashFld.textColor = RGB16(0x333333);
        self.cashFld.titleLabel.textColor = RGB16(0x666666);
        
        self.cashFld.placeholder = @"请输入提现金额(元)";
        
        [_cashView addSubview:self.cashFld];
        
        [self.cashFld mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_cashView.mas_top);
            make.left.equalTo(_cashView.mas_left).offset(10);
            make.right.equalTo(_cashView.mas_right);
            make.height.mas_equalTo(50);
            make.bottom.equalTo(_cashView.mas_bottom);
        }];

    }
    return _cashView;
}

-(UILabel *)tipLbl{
    if (!_tipLbl) {
        _tipLbl = [ControllerHelper autoFitLabel];
        _tipLbl.font = SystemFont(11);
        _tipLbl.textColor = AppGray;
        _tipLbl.text = [NSString stringWithFormat:@"本次可提现%0.2f元",[[RCUserCacheManager sharedManager].currentUser.money floatValue]];
        
        [self.view addSubview:_tipLbl];
    }
    return _tipLbl;
}


-(UIButton *)comfirmBtn{
    if (!_comfirmBtn) {
        _comfirmBtn = [NormalButton normalButton:@"确定提现"];
//        _comfirmBtn.enabled = NO;
        [_comfirmBtn addTarget:self action:@selector(comfirmBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_comfirmBtn];
    }
    return _comfirmBtn;
}

-(void)comfirmBtnClick:(id)sender{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:self.cashFld.text forKey:@"val"];
    [dict setValue:self.numFld.text forKey:@"bank_card"];
    [dict setValue:self.nameFld.text forKey:@"username"];
    [dict setValue:@"rmb" forKey:@"type"];
    
    __block MBProgressHUD *hud = [LLUtils showActivityIndicatiorHUDWithTitle:nil];
    WEAK_SELF;
    WithDrawApi *api = [[WithDrawApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [hud hideAnimated:NO];
        if (info.error == ErrorCodeType_None) {
            
            [LLUtils showActionSuccessHUD:@"提现成功"];
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
        }else{
            [LLUtils showCenterTextHUD:info.message];
        }
        
    }];
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
