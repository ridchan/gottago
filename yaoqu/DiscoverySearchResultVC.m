//
//  DiscoverySearchResultVC.m
//  yaoqu
//
//  Created by ridchan on 2017/9/4.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DiscoverySearchResultVC.h"
#import "DiscoverySearchUserCell.h"
#import "TravelScheduleListCell.h"
#import "DynamicCell.h"
#import "SearchApi.h"
#import "UserModel.h"
#import "TravelScheduleModel.h"
#import "DynamicModel.h"
#import "BaseNavigationController.h"

#import "DiscoverSearchAllVC.h"
#import "DiscoverSearchUserVC.h"
#import "DiscoverSearchTravelVC.h"
#import "DiscoverSearchDynamicVC.h"

@interface DiscoverySearchResultVC ()<UITextFieldDelegate>

@property(nonatomic,strong) UIView *searchView;
@property(nonatomic,strong) UITextField *textField;
@property(nonatomic,strong) UIView *typeView;

@property(nonatomic,strong) NSArray *members;
@property(nonatomic,strong) NSArray *travels;
@property(nonatomic,strong) NSArray *dynamics;

@property(nonatomic) BOOL isSection1More;
@property(nonatomic) BOOL isSection2More;

@property(nonatomic,strong) UIButton *selectBtn;

@property(nonatomic,strong) UIButton *button1;
@property(nonatomic,strong) UIButton *button2;
@property(nonatomic,strong) UIButton *button3;
@property(nonatomic,strong) UIButton *button4;



@property(nonatomic,strong) DiscoverSearchAllVC *allVC;
@property(nonatomic,strong) DiscoverSearchUserVC *userVC;
@property(nonatomic,strong) DiscoverSearchTravelVC *travelVC;
@property(nonatomic,strong) DiscoverSearchDynamicVC *dynamicVC;
@property(nonatomic,strong) UIView *subView;

@end

@implementation DiscoverySearchResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.isloading = NO;
    
    self.navigationItem.title = @"";
    
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -20;
    
    UIBarButtonItem *emptyBtn = [[UIBarButtonItem alloc]initWithCustomView:[[UIView alloc]initWithFrame:CGRectZero]];
    self.navigationItem.leftBarButtonItems = @[negativeSpacer,emptyBtn];
    
    self.navigationItem.titleView = self.searchView;
    

    self.textField.text = self.paramObj;
   
    [self.typeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top).offset(64);
        make.right.equalTo(self.view.mas_right);
        make.height.mas_equalTo(40);
    }];
    
    [self.subView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.typeView.mas_bottom);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    
    self.allVC = [[DiscoverSearchAllVC alloc]init];
    self.allVC.paramObj = self.paramObj;
    self.allVC.resultVC = self;
    self.userVC = [[DiscoverSearchUserVC alloc]init];
    self.travelVC = [[DiscoverSearchTravelVC alloc]init];
    self.dynamicVC = [[DiscoverSearchDynamicVC alloc]init];
    
    [self addChildViewController:self.allVC];
    [self addChildViewController:self.userVC];
    [self addChildViewController:self.travelVC];
    [self addChildViewController:self.dynamicVC];
    
    [self.subView addSubview:self.allVC.view];
    [self.subView addSubview:self.userVC.view];
    [self.subView addSubview:self.travelVC.view];
    [self.subView addSubview:self.dynamicVC.view];

    
    
    [self showVC:self.allVC];
    
    // Do any additional setup after loading the view.
}

-(UIView *)subView{
    if (!_subView) {
        _subView = [[UIView alloc]init];
        [self.view addSubview:_subView];
    }
    return _subView;
}

-(void)showVC:(UIViewController *)vc{
    [vc didMoveToParentViewController:self];
    [self.subView bringSubviewToFront:vc.view];
    if ([vc respondsToSelector:@selector(search:)]){
        [vc performSelector:@selector(search:) withObject:self.paramObj];
    }
    [vc.view mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.subView);
    }];
}


-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.backBarButtonItem = nil;
    self.navigationItem.title = nil;
    [super viewWillAppear:animated];

}


-(UIView *)typeView{
    if (!_typeView) {
        _typeView = [[UIView alloc]init];
        _typeView.backgroundColor = [UIColor whiteColor];
        
        _button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [_button1 setTitle:@"全部" forState:UIControlStateNormal];
        [_button1 setTitleColor:AppGray forState:UIControlStateNormal];
        [_button1 setTitleColor:AppOrange forState:UIControlStateSelected];
        _button1.titleLabel.font = SystemFont(12);
        [_button1 addTarget:self action:@selector(btnTypeClick:) forControlEvents:UIControlEventTouchUpInside];
        _button1.tag = 99;
        _button1.selected = YES;
        self.selectBtn = _button1;
        [_typeView addSubview:_button1];
        
        _button2 = [UIButton buttonWithType:UIButtonTypeCustom];
        [_button2 setTitle:@"用户" forState:UIControlStateNormal];
        [_button2 setTitleColor:AppGray forState:UIControlStateNormal];
        [_button2 setTitleColor:AppOrange forState:UIControlStateSelected];
        _button2.titleLabel.font = SystemFont(12);
        _button2.tag = 0;
        [_button2 addTarget:self action:@selector(btnTypeClick:) forControlEvents:UIControlEventTouchUpInside];
        [_typeView addSubview:_button2];
        
        _button3 = [UIButton buttonWithType:UIButtonTypeCustom];
        [_button3 setTitle:@"旅游" forState:UIControlStateNormal];
        [_button3 setTitleColor:AppGray forState:UIControlStateNormal];
        [_button3 setTitleColor:AppOrange forState:UIControlStateSelected];
        _button3.titleLabel.font = SystemFont(12);
        _button3.tag = 1;
        [_button3 addTarget:self action:@selector(btnTypeClick:) forControlEvents:UIControlEventTouchUpInside];
        [_typeView addSubview:_button3];
        
        _button4 = [UIButton buttonWithType:UIButtonTypeCustom];
        [_button4 setTitle:@"动态" forState:UIControlStateNormal];
        [_button4 setTitleColor:AppGray forState:UIControlStateNormal];
        [_button4 setTitleColor:AppOrange forState:UIControlStateSelected];
        _button4.titleLabel.font = SystemFont(12);
        _button4.tag = 2;
        [_button4 addTarget:self action:@selector(btnTypeClick:) forControlEvents:UIControlEventTouchUpInside];
        [_typeView addSubview:_button4];
        
        [_button1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_typeView.mas_left);
            make.top.equalTo(_typeView.mas_top);
            make.bottom.equalTo(_typeView.mas_bottom);
            make.width.equalTo(_typeView.mas_width).multipliedBy(0.25);
        }];
        
        [_button2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_button1.mas_right);
            make.top.equalTo(_typeView.mas_top);
            make.bottom.equalTo(_typeView.mas_bottom);
            make.width.equalTo(_typeView.mas_width).multipliedBy(0.25);
        }];
        
        [_button3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_button2.mas_right);
            make.top.equalTo(_typeView.mas_top);
            make.bottom.equalTo(_typeView.mas_bottom);
            make.width.equalTo(_typeView.mas_width).multipliedBy(0.25);
        }];
        
        [_button4 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_button3.mas_right);
            make.top.equalTo(_typeView.mas_top);
            make.bottom.equalTo(_typeView.mas_bottom);
            make.width.equalTo(_typeView.mas_width).multipliedBy(0.25);
        }];
        
        
        [self.view addSubview:_typeView];
    }
    return _typeView;
}

-(void)showAtIndex:(NSInteger)index{
    switch (index) {
        case 0:
            [self btnTypeClick:_button2];
            break;
        case 1:
            [self btnTypeClick:_button3];
            break;
        case 2:
            [self btnTypeClick:_button4];
            break;
        default:
            break;
    }
}

-(void)btnTypeClick:(UIButton *)button{
    if (button != self.selectBtn) {
        self.selectBtn.selected = NO;
        button.selected = YES;
        self.selectBtn = button;
        
        switch (button.tag) {
            case 0:
                [self showVC:self.userVC];
                break;
            case 1:
                
                [self showVC:self.travelVC];
                break;
            case 2:
                [self showVC:self.dynamicVC];
                break;
            default:
                [self showVC:self.allVC];
                break;
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *)searchView{
    if (!_searchView) {
        _searchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 44)];
        
        _textField = [[UITextField alloc]init];
        _textField.leftViewMode = UITextFieldViewModeAlways;
        _textField.layer.masksToBounds = YES;
        _textField.layer.cornerRadius = 3.0;
        _textField.leftView = [self leftView];
        _textField.placeholder = @"搜索";
        _textField.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _textField.font = SystemFont(12);
        _textField.delegate = self;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.returnKeyType = UIReturnKeySearch;
        
        [_searchView addSubview:_textField];
        
        UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        [cancelBtn setTitleColor:AppBlack forState:UIControlStateNormal];
        cancelBtn.titleLabel.font = SystemFont(14);
        [cancelBtn addTarget:self action:@selector(cancelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_searchView addSubview:cancelBtn];
        
        UIImageView *backBtn = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_back"]];
        backBtn.userInteractionEnabled = YES;
        backBtn.contentMode = UIViewContentModeScaleAspectFit;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelBtnClick:)];
        [backBtn addGestureRecognizer:tap];
        
        [_searchView addSubview:backBtn];
        
        [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_searchView.mas_left);
            make.top.equalTo(_searchView.mas_top).offset(5);
            make.bottom.equalTo(_searchView.mas_bottom).offset(-5);
            make.width.mas_equalTo(20);
        }];
        
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(backBtn.mas_right).offset(10);
            make.height.mas_equalTo(30);
            make.centerY.equalTo(_searchView.mas_centerY);
            make.right.equalTo(cancelBtn.mas_left).offset(-10);
        }];
        
        [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_searchView.mas_top).offset(3);
            make.bottom.equalTo(_searchView.mas_bottom).offset(-3);
            make.right.equalTo(_searchView.mas_right);
            make.width.mas_equalTo(50);
        }];
        
        
    }
    return _searchView;
}

-(UIView *)leftView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_search"]];
    imageView.frame = CGRectMake(7, 7, 16, 16);
    
    [view addSubview:imageView];
    
    return view;
}


#pragma mark -

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self.navigationController popViewControllerAnimated:NO];
    return NO;
}

-(void)cancelBtnClick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
