//
//  UserChargeAddApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserChargeAddApi.h"

@implementation UserChargeAddApi

-(id)initWithVal:(NSString *)val payType:(PayType)payType{
    if (self = [super init]) {
    
        [self.params setValue:val forKey:@"val"];
        [self.params setValue:[NSString intValue:payType] forKey:@"pay_type"];
        
    }
    return self;
}

-(NSString *)requestUrl{
    return UserChargeAddUrl;
}


@end
