//
//  UserNameEditVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/22.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseViewController.h"
#import "CurrentUserModel.h"

@interface UserNameEditVC : BaseViewController

@property(nonatomic,strong) UserModel *editModel;

@end
