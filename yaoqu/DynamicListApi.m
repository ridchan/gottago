//
//  DynamicListApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/28.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicListApi.h"

@interface DynamicListApi()

@end

@implementation DynamicListApi

-(id)initWithMemberID:(NSString *)member_id page:(NSInteger)page size:(NSInteger)size{
    if (self = [super init]) {
        [self.params setValue:member_id forKey:@"member_id"];
        [self.params setValue:@(page) forKey:@"page"];
        [self.params setValue:@(size) forKey:@"size"];
    }
    
    return self;
}

-(NSString *)requestUrl{
    return DynamicHomeUrl;
}


@end
