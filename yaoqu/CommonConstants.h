//
//  CommonConstants.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/12.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#define FontName            @"Helvetica Neue"
#define BoldFontName        @"Helvetica-Bold"

// Main Screen
#define APP_HEIGHT [[UIScreen mainScreen]bounds].size.height
#define APP_WIDTH [[UIScreen mainScreen]bounds].size.width
#define ORIGINAL_MAX_WIDTH 640.0f

// RGB Color
#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define RGB(r,g,b) RGBA(r,g,b,1.0f)
//RGB 16Color
#define RGB16(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define RGB16withAlpha(rgbValue,alphaVal) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:alphaVal]

#define UIColorFromHexA(hexValue, a)     [UIColor colorWithRed:(((hexValue & 0xFF0000) >> 16))/255.0f green:(((hexValue & 0xFF00) >> 8))/255.0f blue:((hexValue & 0xFF))/255.0f alpha:a]
#define UIColorFromHex(hexValue)        UIColorFromHexA(hexValue, 1.0f)

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_MAX_LENGTH (MAX(APP_WIDTH, APP_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(APP_WIDTH, APP_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)


#define LS(val) NSLocalizedString(val, nil)

#define PostNotification(name,obj) [[NSNotificationCenter defaultCenter] postNotificationName:name object:obj];
#define AddDynamciNotification @"AddDynamciNotication"
#define RemoveDynamciNotification @"RemoveDynamciNotication"


//常用宏定义

#define WEAK_SELF __weak typeof(self) weakSelf = self
#define STRONG_SELF if (!weakSelf) return; \
__strong typeof(weakSelf) strongSelf = weakSelf

//[-Wshadow]
//当局部变量遮蔽(shadow)了参数、全局变量或者是其他局部变量时，该警告选项会给我们以警告信息。

#ifndef    weakify
#define weakify( x ) \\
_Pragma("clang diagnostic push") \\
_Pragma("clang diagnostic ignored \\"-Wshadow\\"") \\
autoreleasepool{} __weak __typeof__(x) __weak_##x##__ = x; \\
_Pragma("clang diagnostic pop")
#endif

#ifndef    strongify
#define strongify( x ) \\
_Pragma("clang diagnostic push") \\
_Pragma("clang diagnostic ignored \\"-Wshadow\\"") \\
try{} @finally{} __typeof__(x) x = __weak_##x##__; \\
_Pragma("clang diagnostic pop")
#endif

#define SAFE_SEND_MESSAGE(obj, msg) if ((obj) && [(obj) respondsToSelector:@selector(msg)])

#define RADIANS_TO_DEGREES(radian) ((radian)/M_PI*180.0)
#define DEGREES_TO_RADIANS(degree) ((degree)/180.0*M_PI)

#define TABLE_SECTION_HEIGHT_ZERO 0.000001f



#define THE_GOLDEN_RATIO  0.618

#define CREATE_SHARED_MANAGER(CLASS_NAME) \
+ (instancetype)sharedManager { \
static CLASS_NAME *_instance; \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [[CLASS_NAME alloc] init]; \
}); \
\
return _instance; \
}

#define CREATE_SHARED_INSTANCE(CLASS_NAME) \
+ (instancetype)sharedInstance { \
static CLASS_NAME *_instance; \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [[CLASS_NAME alloc] init]; \
}); \
\
return _instance; \
}

#define FONT_OF_SIZE(size) \
+ (UIFont *)fontOfSize##size { \
static UIFont *font; \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
font = [UIFont systemFontOfSize:size]; \
}); \
\
return font; \
}

//控制向右臭箭头大小
#define RightAccessWidth 15

//Main Color

#define NoNilString(str) str == nil ? @"" : str
#define AppColor UIColorFromHex(0x3793df)
#define AppRed UIColorFromHex(0xF65b66)
#define AppYellow UIColorFromHex(0xFE8B26)
#define AppBlack UIColorFromHex(0x2C3038)
#define AppGray UIColorFromHex(0x8b939F)
#define AppOrange UIColorFromHex(0xff9c00)
#define AppLineColor RGB(232,232,232)

#define AppTitleColor RGB16(0x211b1d)
#define AppNavBarColor RGB16(0x5e95b1)
#define AppBlue RGB16(0x1493db)
#define AppRedPoint RGB16(0xd94a10)

//Main Font
#define SystemFont(size) [UIFont systemFontOfSize:size]

#define UserNameFont  [UIFont systemFontOfSize:14]
#define DetailFont [UIFont systemFontOfSize:12];
#define FontGray UIColorFromHex(0x8b939F)

//Dynamic Font

//动态列表
#define DynamicNameFont SystemFont(16)
#define DynamicTimeFont SystemFont(12)
#define DynamicDescFont SystemFont(16)

#define DynamicNameColor [UIColor blackColor]
#define DynamicTimeColor [UIColor lightGrayColor]
#define DynamicDescColor [UIColor blackColor]

//动态评论
#define DynamicCommentNameFont SystemFont(14)
#define DynamicCommentTimeFont SystemFont(12)
#define DynamicCommentDescFont SystemFont(14)

#define DynamicCommentNameColor [UIColor blackColor]
#define DynamicCommentTimeColor [UIColor lightGrayColor]
#define DynamicCommentDescColor [UIColor blackColor]





//UI颜色控制
#define kUIToneBackgroundColor UIColorFromHex(0x00bd8c) //UI整体背景色调 与文字颜色一一对应
#define kUIToneTextColor UIColorFromHex(0xffffff) //UI整体文字色调 与背景颜色对应
#define kStatusBarStyle UIStatusBarStyleLightContent //状态栏样式
#define kViewBackgroundColor UIColorFromHex(0xf5f5f5) //界面View背景颜色


//每页默认10个
#define PageSize 10

//金额符号

#define MoneySign @"￥"

//七牛缩略图

#define DynamicImageFix @"dynamicimage_m"

#define TravelImageFix @"travelimage_m"

//?imageView2/1/w/360/h/200

#define MemberLogoMiddleFix @"memberlogo_m"

#define MemberLogoSmallFix @"memberlogo_s"




