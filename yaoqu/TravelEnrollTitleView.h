//
//  TravelEnrollTitleView.h
//  yaoqu
//
//  Created by ridchan on 2017/7/16.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TravelEnrollModel.h"
#import "TravelScheduleModel.h"

@interface TravelEnrollTitleView : UIView


@property(nonatomic,strong) TravelScheduleModel *scheduleModel;


@end
