//
//  DiscoverSearchDynamicVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/6.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NormalTableViewController.h"

@interface DiscoverSearchDynamicVC : NormalTableViewController

-(void)search:(NSString *)keyword;

@end
