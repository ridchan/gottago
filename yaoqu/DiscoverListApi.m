//
//  DiscoverListApi.m
//  yaoqu
//
//  Created by ridchan on 2017/7/23.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DiscoverListApi.h"

@implementation DiscoverListApi{
    DiscoverRankType _type;
}

-(id)initWithType:(DiscoverRankType)type page:(NSInteger)page size:(NSInteger)size{
    if (self = [super init]) {
        _type = type;
        [self.params setValue:@(page) forKey:@"page"];
        [self.params setValue:@(size) forKey:@"size"];
    }
    return self;
}

-(NSString *)requestUrl{
    switch (_type) {
        case DiscoverRankType_Follow:
            return DiscoverWeekFollowUrl;
            break;
        case DiscoverRankType_Like:
            return DiscoverWeekLikeUrl;
            break;
        case DiscoverRankType_Gift:
            return DiscoverWeekGiftUrl;
            break;
        case DiscoverRankType_Currency:
            return DiscoverWeekCurrencyUrl;
            break;
        default:
            break;
    }
}

@end
