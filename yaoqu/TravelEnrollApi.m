//
//  TravelEnrollApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelEnrollApi.h"

@implementation TravelEnrollApi

-(id)initWithModel:(TravelEnrollModel *)model{
    if (self = [super init]) {
        [self.params addEntriesFromDictionary:[model keyValues]];
    }
    return self;
}

-(NSString *)requestUrl{
    return TraveEnrollUrl;
}



@end
