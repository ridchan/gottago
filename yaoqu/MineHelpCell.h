//
//  MineHelpCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/18.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionHelpModel.h"
@interface MineHelpCell : UITableViewCell

@property(nonatomic,strong) QuestionHelpModel *model;
@property(nonatomic,strong) BaseBlock actionBlock;

@end
