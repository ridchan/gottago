//
//  ConversationListCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LLConversationModel.h"

@interface ConversationListCell : UITableViewCell

@property(nonatomic,strong) LLConversationModel *conversationModel;

- (void)markAllMessageAsRead;

@end
