//
//  ChatInputView.m
//  shiyi
//
//  Created by ridchan on 16/6/24.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import "ChatInputView.h"





#pragma mark -
#pragma mark ChatView

@implementation ChatInputView


- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}



-(instancetype)initWithChat{
    if (self = [super init]) {
        [self createView];
    }
    return self;
}

-(instancetype)initWithPublish{
    if (self = [super init]) {
        [self createPublish];
    }
    return self;
}

-(void)dealloc{
    
}

#pragma mark -
#pragma mark 输入框 

-(void)createPublish{
    self.textField = [[UITextField alloc]init];
    //self.textField.borderStyle = UITextBorderStyleRoundedRect;
    self.textField.layer.masksToBounds = YES;
    self.textField.layer.cornerRadius = 3.0;
    self.textField.backgroundColor = [UIColor whiteColor];
    self.textField.layer.borderColor = RGB16(0xe8e8e8).CGColor;
    self.textField.layer.borderWidth = 0.5;
    self.textField.returnKeyType = UIReturnKeySend;
    self.textField.font = [UIFont systemFontOfSize:14];
    self.textField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 1)];
    self.textField.leftViewMode = UITextFieldViewModeAlways;
    
    [self.textField addTarget:self action:@selector(valueChange:) forControlEvents:UIControlEventAllEvents];
//    self.textField.delegate = self;
    [self addSubview:self.textField];
    
    self.emjoyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.emjoyBtn setImage:[UIImage imageNamed:@"ic_review_emjo"] forState:UIControlStateNormal];
    [self addSubview:self.emjoyBtn];
    
    self.sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sendBtn.backgroundColor = [UIColor orangeColor];
    self.sendBtn.layer.cornerRadius = 5;
    [self.sendBtn setTitle:@"回复" forState:UIControlStateNormal];
    [self.sendBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.sendBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    self.sendBtn.enabled = NO;
    self.sendBtn.backgroundColor = [UIColor lightGrayColor];
    [self.sendBtn addTarget:self action:@selector(sendBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.sendBtn];
    
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = RGB16(0xe8e8e8);
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(0.5);
    }];
    
//    [self.emjoyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.mas_top).offset(10);
//        make.bottom.equalTo(self.mas_bottom).offset(-10);
//        make.left.equalTo(self.mas_left).offset(12);
//        make.width.mas_equalTo(self.emjoyBtn.mas_height);
//    }];
    
    [self.sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(5);
        make.bottom.equalTo(self.mas_bottom).offset(-5);
        make.right.equalTo(self.mas_right).offset(-12);
        make.width.mas_equalTo(60);
    }];
    
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.right.equalTo(self.sendBtn.mas_left).offset(-10);
        make.top.equalTo(self.mas_top).offset(5);
        make.bottom.equalTo(self.mas_bottom).offset(-5);
    }];
    


}

-(void)valueChange:(id)sender{
    if ([self.textField.text length] > 0) {
        self.sendBtn.enabled = YES;
        self.sendBtn.backgroundColor = [UIColor orangeColor];
    }else{
        self.sendBtn.enabled = NO;
        self.sendBtn.backgroundColor = [UIColor lightGrayColor];
    }
}

-(void)createView{

    self.textField = [[UITextField alloc]init];
    self.textField.borderStyle = UITextBorderStyleRoundedRect;
    self.textField.returnKeyType = UIReturnKeySend;
    self.textField.font = [UIFont systemFontOfSize:14];
    [self addSubview:self.textField];
    
    self.emjoyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.emjoyBtn setImage:[UIImage imageNamed:@"ic_review_emjo"] forState:UIControlStateNormal];
//    [self.emjoyBtn setImage:[UIImage imageNamed:@"搞笑"] contentMode:UIViewContentModeScaleAspectFit];
    [self addSubview:self.emjoyBtn];
    
    self.toolBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.toolBtn setImage:[UIImage imageNamed:@"加号1"] forState:UIControlStateNormal];
//    [self.toolBtn setImage:[UIImage imageNamed:@"加号1"] contentMode:UIViewContentModeScaleAspectFit];
    [self addSubview:self.toolBtn];
    
    [self.emjoyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(10);
        make.bottom.equalTo(self.mas_bottom).offset(-10);
        make.left.equalTo(self.mas_left).offset(12);
        make.width.mas_equalTo(30);
    }];
    
    [self.sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
        make.left.equalTo(self.mas_left).offset(12);
        make.width.mas_equalTo(30);
    }];
    
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.emjoyBtn).offset(10);
        make.right.equalTo(self.sendBtn).offset(-10);
        make.top.equalTo(self.mas_top).offset(5);
        make.bottom.equalTo(self.mas_bottom).offset(-5);
    }];
    
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)sendBtnClick:(id)sender{
    [self.textField resignFirstResponder];
    if ([self.delegate respondsToSelector:@selector(chatInputSend:)]){
        [self.delegate chatInputSend:self];
    }
}





@end
