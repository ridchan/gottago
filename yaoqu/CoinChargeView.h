//
//  CoinChargeView.h
//  yaoqu
//
//  Created by ridchan on 2017/7/10.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonConfigManager.h"

@interface CoinChargeCell : UITableViewCell

@property(nonatomic,strong) UIImageView *coinImage;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UILabel *priceLbl;
@property(nonatomic,strong) UIView *line;


@property(nonatomic,strong) CurrencyValueModel *model;

@end



@protocol CoinChargeDelegate <NSObject>

@optional
-(void)didSelectCoin:(id)obj;

@end

@interface CoinChargeView : UIView


@property(nonatomic) id<CoinChargeDelegate>delegate;
@property(nonatomic,strong) NSArray *datas;


+(void)show:(NSArray *)datas inView:(UIView *)view delegate:(id)delegate;
+(void)show:(NSArray *)datas inView:(UIView *)view;
+(void)show:(NSArray *)datas;

@property(nonatomic) CGFloat autoHeight;

@end
