//
//  ChatManagerView.m
//  shiyi
//
//  Created by 陳景雲 on 16/6/24.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import "ChatManagerView.h"

@implementation ChatManagerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(instancetype)initWithType:(ChatViewType)type{
    if (self = [super init]) {
        self.backgroundColor = RGBA(255, 255, 255, 0.001);
        self.frame = CGRectMake(0, APP_HEIGHT - 44, APP_WIDTH, APP_HEIGHT);
        
//        self.backgroundColor = RGBA(0, 0, 0, 0.5);
        self.moveOffset = 0;
        _viewType = type;
        [self createView];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardDidHideNotification object:nil];
    }
    return self;
}

-(instancetype)initWithHidden{
    if (self = [super init]) {
        self.backgroundColor = RGBA(255, 255, 255, 0.001);
        self.frame = CGRectMake(0, APP_HEIGHT , APP_WIDTH, APP_HEIGHT );
        
        //        self.backgroundColor = RGBA(0, 0, 0, 0.5);
        
        _viewType = ChatViewType_Publish;
        _offset = -44;
        [self createView];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardDidHideNotification object:nil];
    }
    return self;

}


-(void)keyboardShow:(NSNotification *)notification{
    
    NSDictionary *info = notification.userInfo;
    CGRect rect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat off = - rect.size.height;
    
    
    self.inputType = ChatInputType_Text;
    
    
    self.bottomHeight = 0;
    
    self.emjoyView.hidden = YES;
    self.toolView.hidden = YES;
    
    
    self.transform = CGAffineTransformMakeTranslation(0, -APP_HEIGHT + 44 + _offset  / 2 - self.moveOffset);
    self.inputView.transform = CGAffineTransformMakeTranslation(0,  APP_HEIGHT - 44 + off + _offset / 2);
    
    
}

-(void)chageViewFrame{
    
    self.transform = CGAffineTransformMakeTranslation(0, -APP_HEIGHT + 44 + _offset);
    self.inputView.transform = CGAffineTransformMakeTranslation(0, APP_HEIGHT - 44 + self.bottomHeight - self.moveOffset );
    self.emjoyView.transform = CGAffineTransformMakeTranslation(0, APP_HEIGHT - 44 + self.bottomHeight - self.moveOffset);
    self.toolView.transform = CGAffineTransformMakeTranslation(0, APP_HEIGHT - 44 + self.bottomHeight - self.moveOffset);
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.inputView.textField resignFirstResponder];
    [UIView animateWithDuration:0.35 animations:^{
        self.transform = CGAffineTransformIdentity;
        self.inputView.transform = CGAffineTransformIdentity;
        self.emjoyView.transform = CGAffineTransformIdentity;
        self.toolView.transform = CGAffineTransformIdentity;
    }];
    
    if ([self.delegate respondsToSelector:@selector(dismissWithInfo:)]){
        [self.delegate dismissWithInfo:nil];
    }
}

-(void)keyboardHide:(NSNotification *)notification{
    
    if (self.inputType == ChatInputType_Text){
        self.transform = CGAffineTransformIdentity;
        self.inputView.transform = CGAffineTransformIdentity;
        self.emjoyView.transform = CGAffineTransformIdentity;
        self.toolView.transform = CGAffineTransformIdentity;
        
        if ([self.delegate respondsToSelector:@selector(dismissWithInfo:)]){
            [self.delegate dismissWithInfo:nil];
        }
        [self.inputView valueChange:nil];
    }

//    if ([self.delegate respondsToSelector:@selector(chatViewFrameChange:)]){
//        [self.delegate chatViewFrameChange:0];
//    }
}

-(void)emjoyBtnClick:(id)sender{
    
    self.bottomHeight = -200;
    self.inputType = ChatInputType_Emjoi;
    
    [self.inputView.textField resignFirstResponder];
    self.emjoyView.hidden = NO;
    self.toolView.hidden = YES;
    [self bringSubviewToFront:self.emjoyView];
    
    [self chageViewFrame];
    

}

-(void)toolBtnClick:(id)sender{
    
    self.bottomHeight = -80;
    self.inputType = ChatInputType_Tool;
    
    [self.inputView.textField resignFirstResponder];
    self.emjoyView.hidden = YES;
    self.toolView.hidden = NO;
    [self bringSubviewToFront:self.toolView];
    
    [self chageViewFrame];

}

-(void)coverTap:(id)sender{
    [self.inputView.textField resignFirstResponder];
    self.transform = CGAffineTransformIdentity;
    self.emjoyView.hidden = YES;
    self.toolView.hidden = YES;

}

-(void)createView{
    
    self.clipsToBounds = NO;
    
    
    if (_viewType == ChatViewType_Chat){
        self.inputView = [[ChatInputView alloc]initWithChat];
    }else{
        self.inputView = [[ChatInputView alloc]initWithPublish];
    }
    
    self.inputView.textField.delegate = self;
    self.inputView.delegate = self;
    

    [self.inputView.emjoyBtn addTarget:self action:@selector(emjoyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.inputView.toolBtn addTarget:self action:@selector(toolBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.emjoyView = [[ChatEmjoyView alloc]init];
    self.emjoyView.delegate = self;
    [self addSubview:self.emjoyView];
    self.toolView = [[ChatToolView alloc]init];
    [self addSubview:self.toolView];
    
    self.inputView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:self.inputView];

    
    [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.right.equalTo(self.mas_right);
        make.left.equalTo(self.mas_left);
        make.height.mas_equalTo(44);
    }];
    
    [self.emjoyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.inputView.mas_bottom);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(200);
    }];
    

    [self.toolView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.inputView.mas_bottom);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(80);
    }];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if ([self.delegate respondsToSelector:@selector(sendWithInfo:)]) {
        [self.delegate sendWithInfo:self.inputView.textField.text];
        [self.inputView.textField resignFirstResponder];
    }
    return YES;
}

-(void)chatEmjoyText:(NSString *)emjoy{
    self.inputView.textField.text = [self.inputView.textField.text stringByAppendingString:emjoy];
    [self.inputView valueChange:nil];
}

-(void)chatEmjoyBack{
    if ([self.inputView.textField.text length] > 3){
        self.inputView.textField.text = [self.inputView.textField.text substringToIndex:self.inputView.textField.text.length -2];
    }
}

-(void)chatEmjoySend{
    self.inputType = ChatInputType_Text;
    
    if ([self.delegate respondsToSelector:@selector(sendWithInfo:)]) {
        [self.delegate sendWithInfo:self.inputView.textField.text];
        [self.inputView.textField resignFirstResponder];
        [UIView animateWithDuration:0.35 animations:^{
            self.transform = CGAffineTransformIdentity;
            self.inputView.transform = CGAffineTransformIdentity;
            self.emjoyView.transform = CGAffineTransformIdentity;
            self.toolView.transform = CGAffineTransformIdentity;
        }];
        
    }
}

-(void)chatInputSend:(id)obj{
    if ([self.delegate respondsToSelector:@selector(sendWithInfo:)]) {
        [self.delegate sendWithInfo:self.inputView.textField.text];
        [UIView animateWithDuration:0.35 animations:^{
            self.transform = CGAffineTransformIdentity;
            self.inputView.transform = CGAffineTransformIdentity;
            self.emjoyView.transform = CGAffineTransformIdentity;
            self.toolView.transform = CGAffineTransformIdentity;
            [self.inputView.textField resignFirstResponder];
        }];
        [self.inputView valueChange:nil];
    }
}

@end
