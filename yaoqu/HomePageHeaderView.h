//
//  HomePageHeaderView.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/27.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"

@interface HomePageHeaderView : UIView

@property(nonatomic,strong) UserModel *userModel;

@property(nonatomic,strong) NSString *frameHeight;

@end
