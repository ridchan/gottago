//
//  TravelEnrollVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseViewController.h"
#import "TravelScheduleModel.h"

@interface TravelEnrollVC : BaseViewController

@property(nonatomic,strong) TravelScheduleModel *scheduleModel;

@end
