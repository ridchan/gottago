//
//  DiscoverSearchVC.m
//  yaoqu
//
//  Created by ridchan on 2017/9/2.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DiscoverSearchVC.h"
#import "YJTagView.h"
#import "HistorySearchView.h"
#import "SearchApi.h"
#import "HistoryModel.h"

@interface DiscoverSearchVC ()<UITextFieldDelegate,HistorySearchViewDelegate>

@property(nonatomic,strong) UISearchBar *searchBar;
@property(nonatomic,strong) UIView *searchView;
@property (nonatomic, strong) NSArray *titleArray;
@property(nonatomic,strong) UITextField *textField;

@property(nonatomic,strong) HistorySearchView *historyView;

@property(nonatomic,strong) HistorySearchView *hotView;

@property(nonatomic,strong) UIScrollView *scrollView;

@property(nonatomic,strong) UIView *typeView;

@property(nonatomic,strong) NSArray *historyArr;
@property(nonatomic,strong) NSArray *hotArr;
@property(nonatomic,strong) UIButton *selectBtn;
@property(nonatomic,strong) NSString *selectType;

@property(nonatomic) BOOL runningSearch;

@end

@implementation DiscoverSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"";

    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -20;
    
    UIBarButtonItem *emptyBtn = [[UIBarButtonItem alloc]initWithCustomView:[[UIView alloc]initWithFrame:CGRectZero]];
    self.navigationItem.leftBarButtonItems = @[negativeSpacer,emptyBtn];
    self.navigationItem.titleView = self.searchView;
    
    
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    [self.historyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView.mas_left);
        make.top.equalTo(self.scrollView.mas_top);
        make.width.mas_equalTo(APP_WIDTH);
    }];
    

    
    WEAK_SELF;
    [[[SearchApi alloc]init] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        weakSelf.hotArr = [HistoryModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        [weakSelf setHot];
    }];
    
    [RACObserve(self, historyArr) subscribeNext:^(id x) {
        self.historyView.titleArray = x;
    }];
    
//    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:nil action:nil];
//    leftItem.width = 0.01;
//    self.navigationItem.leftBarButtonItem = leftItem;
//    
//    leftItem.title = @"";
    
    
    
}


-(void)setHot{
    self.hotView.titleArray = self.hotArr;
    [self.hotView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView.mas_left);
        //        make.top.equalTo(self.scrollView.mas_top);
        make.top.equalTo(self.historyView.mas_bottom).offset(10);
        make.width.mas_equalTo(APP_WIDTH);
        make.bottom.equalTo(self.scrollView.mas_bottom);
    }];
}



-(void)historyViewDidSelect:(HistoryModel *)obj{
    self.textField.text = obj.keyword;
    [self.textField resignFirstResponder];
    
    if (_runningSearch) return;
    self.runningSearch = YES;
    
    WEAK_SELF;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf pushViewControllerWithName:@"DiscoverySearchResultVC" params:weakSelf.textField.text];
        weakSelf.runningSearch = NO;
    });
}

-(void)historyViewDeleteBtnClick:(id)obj{
    [HistoryModel deleteWithWhere:@" 1 = 1"];
    self.historyArr = nil;
}

-(UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]init];
        [self.view addSubview:_scrollView];
    }
    return _scrollView;
}

-(HistorySearchView *)historyView{
    if (!_historyView) {
        _historyView = [[HistorySearchView alloc]init];
        _historyView.delegate = self;
        [self.scrollView addSubview:_historyView];
    }
    return _historyView;
}

-(HistorySearchView *)hotView{
    if (!_hotView) {
        _hotView = [[HistorySearchView alloc]init];
        _hotView.titleLbl.text = @"热门搜索";
        _hotView.delBtn.hidden = YES;
        _hotView.delegate = self;
        [self.scrollView addSubview:_hotView];
    }
    return _hotView;
}




-(UIView *)leftView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_search"]];
    imageView.frame = CGRectMake(7, 7, 16, 16);
    
    [view addSubview:imageView];
    
    return view;
}

-(UIView *)searchView{
    if (!_searchView) {
        _searchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 44)];
        
        _textField = [[UITextField alloc]init];
        _textField.leftViewMode = UITextFieldViewModeAlways;
        _textField.layer.masksToBounds = YES;
        _textField.layer.cornerRadius = 3.0;
        _textField.leftView = [self leftView];
        _textField.placeholder = @"搜索";
        _textField.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _textField.font = SystemFont(12);
        _textField.delegate = self;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.returnKeyType = UIReturnKeySearch;
        
        [_searchView addSubview:_textField];
        
        UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        [cancelBtn setTitleColor:AppBlack forState:UIControlStateNormal];
        cancelBtn.titleLabel.font = SystemFont(14);
        [cancelBtn addTarget:self action:@selector(cancelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_searchView addSubview:cancelBtn];
        
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_searchView.mas_left).offset(10);
            make.height.mas_equalTo(30);
            make.centerY.equalTo(_searchView.mas_centerY);
            make.right.equalTo(cancelBtn.mas_left).offset(-10);
        }];
        
        [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_searchView.mas_top).offset(3);
            make.bottom.equalTo(_searchView.mas_bottom).offset(-3);
            make.right.equalTo(_searchView.mas_right);
            make.width.mas_equalTo(50);
        }];
        
        
        
    }
    return _searchView;
}


#pragma mark -


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if ([textField.text length] > 0){
        
        if  (self.runningSearch) return YES;
        self.runningSearch = YES;
        [textField resignFirstResponder];
        
        
        [HistoryModel deleteWithWhere:[NSString stringWithFormat:@"keyword='%@'",textField.text]];
        
        HistoryModel *model = [[HistoryModel alloc]init];
        model.keyword = textField.text;
        [model saveToDB];
        
        WEAK_SELF;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            weakSelf.runningSearch = NO;
            [weakSelf pushViewControllerWithName:@"DiscoverySearchResultVC" params:textField.text];
        });
        
        
    }
    return YES;
}

-(void)cancelBtnClick:(id)sender{
//    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.textField becomeFirstResponder];
    self.runningSearch = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    
    
    
    [super viewWillAppear:animated];
    
    self.historyArr = [HistoryModel searchWithWhere:@" 1 = 1 "];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
