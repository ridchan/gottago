//
//  RCTextView.m
//  QBH
//
//  Created by 陳景雲 on 2017/2/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "RCTextView.h"
#import "NSString+Extend.h"

@implementation RCTextView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self createViews];
    }
    return self;
}

-(void)createViews{
    self.label = [[UILabel alloc]init];
    self.label.textColor = [UIColor lightGrayColor];
    self.label.font = self.font;
    self.label.numberOfLines = 2;
    
    [self addSubview:self.label];
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(5);
        make.top.equalTo(self.mas_top).offset(7);
        make.right.equalTo(self.mas_right);
        
    }];
    
    
    self.maxLbl = [[UILabel alloc]init];
//    [self.maxLbl setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    self.maxLbl.textColor = [UIColor lightGrayColor];
    self.maxLbl.textAlignment = NSTextAlignmentRight;
    self.maxLbl.font = self.font;
    [self addSubview:self.maxLbl];
    [self.maxLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-5);
        make.bottom.equalTo(self.mas_bottom).offset(-7);
        make.left.equalTo(self.mas_left).offset(5);
        make.height.mas_equalTo(15);
    }];
    
    self.delegate = self;
    
}

-(void)setMaxNum:(NSInteger)maxNum{
    _maxNum = maxNum;
    self.maxLbl.text = [NSString intValue:maxNum];
}

-(void)setFont:(UIFont *)font{
    [super setFont:font];
    self.label.font = font;
    self.maxLbl.font = font;
}

-(void)setText:(NSString *)text{
    [super setText:text];
    self.label.hidden = [text length] > 0;
}

-(void)textViewDidChange:(UITextView *)textView{
    self.label.hidden =  [textView.text length] > 0;
    if (self.maxNum > 0){
        self.maxLbl.text = [NSString intValue:self.maxNum - [textView.text length]];
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if (self.maxNum > 0) {
        if ([textView.text length] < self.maxNum) {
            return YES;
        }else{
            return (range.location + range.length) <= self.maxNum;
        }
    }else{
        return YES;
    }
}

-(void)setPlaceholder:(NSString *)placeholder{
    _placeholder = placeholder;
    self.label.text = placeholder;
}

@end
