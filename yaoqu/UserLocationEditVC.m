//
//  UserLocationEditVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/26.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserLocationEditVC.h"
#import "AddressPickerView.h"
#import "UserModel.h"
#import "CommonConfigManager.h"
#import "AddressProvinceVC.h"

@interface UserLocationEditVC ()<UIPickerViewDelegate,UIPickerViewDataSource>

//@property(nonatomic,strong) AddressPickerView *pickerView;

@property(nonatomic,strong) UIPickerView *pickerView;
@property(nonatomic,strong) NSArray <ProModel *>* addressArray;
@property(nonatomic,strong) UserModel *editModel;

@property(nonatomic,strong) NSString *pro_id;
@property(nonatomic,strong) NSString *pro_name;
@property(nonatomic,strong) NSString *city_id;
@property(nonatomic,strong) NSString *city_name;
@property(nonatomic,strong) NSString *isSecret;




@end

@implementation UserLocationEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"位置");
    self.datas = [@[@"你的位置",@"保密"] mutableCopy];
    self.tableView.scrollEnabled = NO;
    [self setRightItemWithTitle:LS(@"保存") selector:@selector(saveBtnClick:)];
    
    self.editModel = self.paramObj;

    if (self.addressType == AddressType_Location) {
        self.pro_id = self.editModel.pro_id;
        self.pro_name = self.editModel.pro_name;
        self.city_id = self.editModel.city_id;
        self.city_name = self.editModel.city_name;
        self.isSecret = [NSString boolValue:self.editModel.is_show_location];
    }else{
        self.pro_id = self.editModel.home_pro_id;
        self.pro_name = self.editModel.home_pro_name;
        self.city_id = self.editModel.home_city_id;
        self.city_name = self.editModel.home_city_name;
        self.isSecret = [NSString boolValue:self.editModel.is_show_from];
    }
    
    self.addressArray = [CommonConfigManager sharedManager].addressArray;
    
    
    self.pickerView = [[UIPickerView alloc]init];
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    [self.view addSubview:self.pickerView];
    
    [self.pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    // Do any additional setup after loading the view.
}

-(void)saveBtnClick:(id)sender{
    
    if (self.addressType == AddressType_Location) {
        self.editModel.pro_id  = self.pro_id;
        self.editModel.pro_name  = self.pro_name;
        self.editModel.city_name  = self.city_name;
        self.editModel.city_id  = self.city_id;
        self.editModel.is_show_location = [self.isSecret boolValue];
    }else{
        self.editModel.home_pro_id  = self.pro_id;
        self.editModel.home_pro_name  = self.pro_name;
        self.editModel.home_city_name  = self.city_name;
        self.editModel.home_city_id  = self.city_id;
        self.editModel.is_show_from = [self.isSecret boolValue];
    }
    
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark address view

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return [self.addressArray count] > 0 ? 2 : 0;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0){
        return self.addressArray.count;
    }else{
        ProModel *model = self.addressArray[[pickerView selectedRowInComponent:0]];
        return model.city_list.count;
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0){
        ProModel *pro = self.addressArray[row];
        return pro.pro;
    }else{
        ProModel *pro = self.addressArray[[pickerView selectedRowInComponent:0]];
        if ([pro.city_list count] > row){
            CityModel *city  = pro.city_list[row];
            return city.city;
        }else{
            return nil;
        }
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == 0){
        [pickerView reloadComponent:1];
        ProModel *pro = self.addressArray[row];
        self.pro_id = pro.pro_id;
        self.pro_name = pro.pro;
        self.city_id = @"";
        self.city_name = @"";
        if ([pro.city_list count] > 0)
            [self pickerView:self.pickerView didSelectRow:0 inComponent:1];
    }else{
        ProModel *pro = self.addressArray[[pickerView selectedRowInComponent:0]];
        CityModel *city  = pro.city_list[row];
        self.city_id = city.city_id;
        self.city_name = city.city;
    }
    
    
//    self.tempModel.pro_id = pro.pro_id;
//    self.tempModel.pro_name = pro.pro;
//    self.tempModel.city_id = city.city_id;
//    self.tempModel.city_name = city.city;
}


-(void)cancelBtnClick{
//    [self.pickerView hide];
}

-(void)sureBtnClickReturnProvince:(NSString *)province City:(NSString *)city Area:(NSString *)area{
//    [self.pickerView hide];
}


#pragma mark -
#pragma mark tableview delegate

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 20)];
    UILabel *label = [[UILabel alloc]init];
    label.textColor = FontGray;
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 2;
    label.text = section == 0 ? LS(@"选择你的位置")
    : LS(@"是否隐藏你的位置信息")  ;
    label.frame = CGRectMake(10, 0, APP_WIDTH - 20, 20);
    
    [view addSubview:label];
    return view;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [self.pickerView show];
//    AddressProvinceVC *vc = [[AddressProvinceVC alloc]init];
//    vc.model = self.tempModel;
//    [self.navigationController pushViewController:vc animated:YES];
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 20;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NormalTableViewCell *cell = [NormalTableViewCell cellWithType:NormalCellType_Access_Detail identifier:@"Cell" tableView:tableView];
    cell.celltype = indexPath.section ==  0 ? NormalCellType_Access_Detail : NormalCellType_Switch;
    cell.textLabel.text = self.datas[indexPath.section];
    
    if (indexPath.section == 0){
        RACSignal *signA = RACObserve(self,pro_name);
        RACSignal *signB = RACObserve(self,city_name);
        
        RACSignal *reduceSignal = [RACSignal combineLatest:@[signA,signB] reduce:^id(NSString *pro ,NSString *city){
            return [NSString stringWithFormat:@"%@ %@",pro,city];
        }];
        
        [reduceSignal subscribeNext:^(id x) {
            cell.detailLbl.text = x;
        }];
    }else{
        cell.isSwitch = self.isSecret;
        [RACObserve(cell, isSwitch) subscribeNext:^(id x) {
            self.isSecret = x;
        }];
        
    }
    
    

    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
