//
//  DynamicModel.m
//  yaoqu
//
//  Created by ridchan on 2017/6/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicModel.h"


@implementation DynamicModel

-(CGFloat)listHeight{
    if (_listHeight == 0) {
        
        CGFloat fontHeight =  [self.desc heightWithFont:SystemFont(17) inWidth:APP_WIDTH - 20];
        CGFloat count = [self.video_image length] > 0 ? 1 : [[self.content_images JSONObject] count] ;
        CGFloat width = (APP_WIDTH - 30) / 3;
        CGFloat imageHieght = count  > 0  ?  ceil(count / 3.0) * width : 0;
        _listHeight = 70 + 10 + fontHeight + imageHieght + 35 + 30 + 20;
    }
    
    return _listHeight;
}


@end


@implementation DynamicUploadModel



@end
