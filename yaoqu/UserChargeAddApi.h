//
//  UserChargeAddApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface UserChargeAddApi : BaseApi

-(id)initWithVal:(NSString *)val payType:(PayType)payType;

@end
