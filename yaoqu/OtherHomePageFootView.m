//
//  OtherHomePageFootView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/24.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "OtherHomePageFootView.h"
#import "NormalCellLayout.h"
#import "ImageCollectionViewCell.h"
#import "UserHomeDynamicCell.h"

@interface OtherHomePageFootView()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) UIImageView *accessImage;
@property(nonatomic,strong) UICollectionView *collectionView;

@end

@implementation OtherHomePageFootView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.mas_top);
        make.height.mas_equalTo(40);
    }];
    
    [self.accessImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.top.equalTo(self.mas_top);
        make.size.mas_equalTo(CGSizeMake(RightAccessWidth, 40));
    }];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.titleLbl.mas_bottom);
        make.bottom.equalTo(self.mas_bottom).offset(-10);
        make.height.mas_equalTo(150);
    }];
}

-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        _titleLbl.text = LS(@"他的足迹");
        [self addSubview:_titleLbl];
    }
    return _titleLbl;
}

-(UIImageView *)accessImage{
    if (!_accessImage) {
        _accessImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_forward"]];
        _accessImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_accessImage];
    }
    return _accessImage;
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        UserHomeDynamicCellLayout *layout = [[UserHomeDynamicCellLayout alloc]init];
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[ImageCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
        _collectionView.backgroundColor = [UIColor clearColor];
        [self addSubview:_collectionView];

    }
    return _collectionView;
}


#pragma mark -
#pragma mark collection view

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 0;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 0;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ImageCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    return cell;
}

@end
