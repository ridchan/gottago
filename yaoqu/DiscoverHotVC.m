//
//  DiscoverHotVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DiscoverHotVC.h"
#import "DiscoverApi.h"
#import "DynamicCell.h"
#import "DiscoverDynamicHeader.h"
#import "TravelBannerModel.h"
#import "TravelScheduleListCell.h"
#import "TravelScheduleModel.h"
#import "TravelScheduleDetailVC.h"
#import "BaseNavigationController.h"

@interface DiscoverHotVC ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) DiscoverDynamicHeader *headerView;
@property(nonatomic,strong) NSMutableArray *travels;
@property(nonatomic,strong) NSMutableArray *dynamics;

@end

@implementation DiscoverHotVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //    self.navigationController.delegate = self;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.separatorInset = UIEdgeInsetsMake(10, 0, 0, 0);
    
    [self.tableView registerClass:[DynamicCell class] forCellReuseIdentifier:@"Cell"];
    [self.tableView registerClass:[TravelScheduleListCell class] forCellReuseIdentifier:@"TravelCell"];
    
    self.headerView = [[DiscoverDynamicHeader alloc]init];
    self.headerView.frame = CGRectMake(0, 0, APP_WIDTH, 180);
    
    //    self.tableView.emptyDataSetSource = self;
    //    self.tableView.emptyDataSetDelegate = self;
    
    [self startRefresh];
    

    
    // Do any additional setup after loading the view.
}


-(void)startRefresh{
    self.page = 1;
    WEAK_SELF;
    [[[DiscoverApi alloc]initWitObject:@{@"page":@(self.page),@"size":@(self.size)}] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        weakSelf.headerView.datas = [TravelBannerModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"banner"]];
        weakSelf.tableView.tableHeaderView = weakSelf.headerView;
        
        NSArray *array = [DynamicModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"dynamic"]];
        weakSelf.travels = [TravelScheduleModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"travel_schedule"]];
        [weakSelf refreshComplete:info response:array];
        
    }];
    
}

-(void)startloadMore{
    WEAK_SELF;
    [[[DiscoverApi alloc]initWitObject:@{@"page":@(++self.page),@"size":@(self.size)}] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        
        NSArray *array = [DynamicModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"dynamic"]];
        [weakSelf loadMoreComplete:info response:array];
    }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSLog(@"view appear");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





#pragma mark -
#pragma mark  table view

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 50)];
    
    UIView *bgView =[[UIView alloc]initWithFrame:CGRectMake(0, 10, APP_WIDTH , 40)];
    bgView.backgroundColor = [UIColor whiteColor];
    [view addSubview:bgView];
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 39.5, APP_WIDTH, 0.5)];
    line.backgroundColor = RGB16(0xe8e8e8);
    [bgView addSubview:line];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = section == 0 ? @"热门行程" : @"热门动态";
    label.font = SystemFont(16);
    label.frame = CGRectMake(10, 0, APP_WIDTH - 10, 40);
    [bgView addSubview:label];
    
    return view;

}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [tableView cellHeightForIndexPath:indexPath model:self.travels[indexPath.row] keyPath:@"model" cellClass:[TravelScheduleListCell class] contentViewWidth:APP_WIDTH];
    }else{
        id model = self.datas[indexPath.row];
        return [tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[DynamicCell class] contentViewWidth:APP_WIDTH];
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        TravelScheduleDetailVC *vc = [[TravelScheduleDetailVC alloc]init];
        
        vc.model = self.travels[indexPath.row];
        
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [self pushViewControllerWithName:@"DynamicDetailVC" params:self.datas[indexPath.row]];
    }
}



-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return .001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return section == 1 ? self.datas.count : self.travels.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1){
        DynamicCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        cell.model = self.datas[indexPath.row];
        return cell;
    }else{
        TravelScheduleListCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"TravelCell" forIndexPath:indexPath];
        cell.model = self.travels[indexPath.row];
        return cell;
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
