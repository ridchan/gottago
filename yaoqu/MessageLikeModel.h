//
//  MessageLikeModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"
#import "DynamicModel.h"

@interface MessageLikeModel : BaseModel


@property(nonatomic,strong) NSString *message_like_id;
@property(nonatomic,strong) NSString *type;
@property(nonatomic,strong) NSString *key_id;
@property(nonatomic,strong) DynamicUploadModel *data;
@property(nonatomic) BOOL is_del;
@property(nonatomic) long long time;
@property(nonatomic,strong) NSString *show_time;
@property(nonatomic,strong) NSString *member_id;
@property(nonatomic,strong) NSString *phone;
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *image;
@property(nonatomic,strong) NSString *sex;
@property(nonatomic,strong) NSString *age;
@property(nonatomic,strong) NSString *constellation;
@property(nonatomic) BOOL is_age;
@property(nonatomic,strong) NSString *desc;
@property(nonatomic,strong) NSString *pro_id;
@property(nonatomic,strong) NSString *pro_name;
@property(nonatomic,strong) NSString *city_id;
@property(nonatomic,strong) NSString *city_name;
@property(nonatomic,strong) NSString *home_pro_id;
@property(nonatomic,strong) NSString *home_pro_name;
@property(nonatomic,strong) NSString *home_city_id;
@property(nonatomic,strong) NSString *home_city_name;
@property(nonatomic,strong) NSString *exp;
@property(nonatomic,strong) NSString *level;
@property(nonatomic) BOOL is_member;
@property(nonatomic) BOOL is_talent;
@property(nonatomic) BOOL is_gold;
@property(nonatomic,strong) NSString *qty;

@end
