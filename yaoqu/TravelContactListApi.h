//
//  TravelContactListApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface TravelContactListApi : BaseApi

-(id)initWithPage:(NSInteger)page size:(NSInteger)size;

@end
