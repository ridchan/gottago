//
//  MineSettingVC.m
//  yaoqu
//
//  Created by ridchan on 2017/6/26.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineSettingVC.h"
#import "RCUserCacheManager.h"
#import "SRActionSheet.h"

@interface MineSettingVC ()<UITableViewDelegate,UITableViewDataSource>



@end

@implementation MineSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"设置");
    self.tableView.separatorColor = AppLineColor;
    self.datas = [@[LS(@"消息设置"),LS(@"帐号与安全"),LS(@"清除缓存"),LS(@"关于我们")] mutableCopy];
    self.tableView.tableFooterView = [self logoutView];
//    [self commonInit];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 3){
        [self pushViewControllerWithName:@"AboutUsVC"];
    }else if (indexPath.row == 2){
        __block MBProgressHUD *hud = [LLUtils showActivityIndicatiorHUDWithTitle:@"清除中"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [hud hideAnimated:NO];
            [LLUtils showActionSuccessHUD:@"清除成功"];
        });
        
        [[SDImageCache sharedImageCache] clearDisk];
        [[SDImageCache sharedImageCache] clearMemory];//可有可无
        
        
    }else if (indexPath.row == 1){
        [self pushViewControllerWithName:@"UserSafeEditVC"];
    }else{
        [self pushViewControllerWithName:@"MineNotificationCenterVC"];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NormalTableViewCell *cell = [NormalTableViewCell cellWithType:NormalCellType_Access_Detail identifier:@"Cell" tableView:tableView];
    cell.textLabel.text = self.datas[indexPath.row];
    cell.type = indexPath.row == 0 ? NormalCellType_None : NormalCellType_TopLine;
    return cell;
}

-(UIView *)logoutView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 80)];
    NormalButton *button = [NormalButton normalButton:LS(@"退出登录")];
    [button addTarget:self action:@selector(logoutBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left).offset(20);
        make.right.equalTo(view.mas_right).offset(-20);
        make.bottom.equalTo(view.mas_bottom);
        make.height.mas_equalTo(50);
    }];
    
    return view;
}


-(void)logoutBtnClick:(id)sender{
    
    [[SRActionSheet sr_actionSheetViewWithTitle:@"真的要退出登录不和我玩了吗？" cancelTitle:nil destructiveTitle:@"退出登录" otherTitles:@[@"取消"] otherImages:nil selectSheetBlock:^(SRActionSheet *actionSheet, NSInteger index) {
        if (index == 1){
            [RCUserCacheManager sharedManager].currentUser = nil;
        }
        
    }] show];
    
//
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
