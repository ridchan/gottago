//
//  UserCommentCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageCommentModel.h"

@interface UserCommentCell : UITableViewCell

@property(nonatomic,strong) MessageCommentModel *model;

@end
