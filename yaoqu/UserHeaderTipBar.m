//
//  UserHeaderTipBar.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/16.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserHeaderTipBar.h"
#import "RouteController.h"
#import "UserModel.h"
#pragma mark -
#pragma mark cell

@implementation UserHeaderTipBarCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    
    self.backgroundColor = [UIColor whiteColor];
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.mas_centerY).offset(2);
    }];
    
    [self.countLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_centerY).offset(-2);
    }];
}


-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc]init];
        _titleLbl.minimumScaleFactor = 0.5;
        _titleLbl.textAlignment = NSTextAlignmentCenter;
        _titleLbl.textColor = FontGray;
        _titleLbl.font = [UIFont systemFontOfSize:12];
        [self addSubview:_titleLbl];
    }
    return _titleLbl;
}

-(UILabel *)countLbl{
    if (!_countLbl) {
        _countLbl = [[UILabel alloc]init];
        _countLbl.minimumScaleFactor = 0.5;
        _countLbl.textAlignment = NSTextAlignmentCenter;
        _countLbl.font = [UIFont systemFontOfSize:14];
        [self addSubview:_countLbl];
    }
    return _countLbl;
}

@end

#pragma mark -
#pragma mark layout

@implementation UserHeaderTipBarLayout

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.minimumInteritemSpacing = 0;
        self.minimumLineSpacing = 0;
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    return self;
}

-(CGSize)itemSize{
    CGFloat width = self.collectionView.frame.size.width;
    NSInteger count = [self.collectionView numberOfItemsInSection:0];
    return CGSizeMake(width / count, self.collectionView.frame.size.height);
}

@end

#pragma mark -
#pragma mark  collection view

@interface UserHeaderTipBar()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) UIView *line;

@end

@implementation UserHeaderTipBar

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}


-(void)commonInit{
//    self.dataSource = @[@{@"title":LS(@"Order"),@"count":@"3"},
//                        @{@"title":LS(@"Friends"),@"count":@"23"},
//                        @{@"title":LS(@"Follow"),@"count":@"121"},
//                        @{@"title":LS(@"Fans"),@"count":@"250"},
//                        @{@"title":LS(@"Dynamic"),@"count":@"63"}
//                        ];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(0.5);
    }];
}

-(void)setDataSource:(NSArray *)dataSource{
    _dataSource = dataSource;
    [self.collectionView reloadData];
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        
        UserHeaderTipBarLayout *layout = [[UserHeaderTipBarLayout alloc]init];
        
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        [_collectionView registerClass:[UserHeaderTipBarCell class] forCellWithReuseIdentifier:@"Cell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_collectionView];
    }
    
    return _collectionView;
}

-(UIView *)line{
    if (!_line) {
        _line = [[UIView alloc]init];
        _line.backgroundColor = AppLineColor;
        [self addSubview:_line];
    }
    return _line;
}


#pragma mark -

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dict = self.dataSource[indexPath.item];
    if (![[dict objectForKey:@"param"] isKindOfClass:[UserModel class]])
        [[RouteController sharedManager] openClassVC:[dict objectForKey:@"vc"] withObj:dict];
    else
        [[RouteController sharedManager] openClassVC:[dict objectForKey:@"vc"] withObj:[dict objectForKey:@"param"]];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UserHeaderTipBarCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dict = self.dataSource[indexPath.item];
    cell.titleLbl.text = dict[@"title"];
    cell.countLbl.text = dict[@"count"];
    if (_titleFont) {
        cell.titleLbl.font = _titleFont;
    }
    if (_countFont) {
        cell.countLbl.font = _countFont;
    }
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataSource.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
