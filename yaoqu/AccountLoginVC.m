//
//  AccountLoginVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/8.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "AccountLoginVC.h"
#import "ImageTextField.h"
#import "AccountLoginView.h"
#import "AccountRegistView.h"
#import "AccountInfoView.h"

#import <UMSocialCore/UMSocialCore.h>
#import "RCUserCacheManager.h"
#import "RouteController.h"
#import "MemberApi.h"
#import "LoginApi.h"
#import "CommonApi.h"
#import "LoginInfoModel.h"
#import "AccessTokenModel.h"
#import "LoginPostModel.h"
#import "RegisterVC.h"
#import <Qiniu/QiniuSDK.h>


@interface AccountLoginVC()

@property(nonatomic,strong) UIImageView *backgroundImage;
@property(nonatomic,strong) AccountLoginView *loginView;
@property(nonatomic,strong) AccountRegistView *registView;
@property(nonatomic,strong) AccountInfoView *infoView;
@property(nonatomic,strong) UINavigationBar *navBar;
@property(nonatomic,strong) UINavigationItem *navItem;
@property(nonatomic,strong) UIView *lastView;

@property(nonatomic,strong) LoginPostModel *postModel;

@property(nonatomic) BOOL isFindPassword;
@property(nonatomic) BOOL isOtherRegist;

@end

@implementation AccountLoginVC

-(void)viewDidLoad{
    [super viewDidLoad];
    
    
    
    
    self.postModel = [[LoginPostModel alloc]init];
    
    [self.backgroundImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.bottom.equalTo(self.view.mas_bottom);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top);

    }];
    
    
    self.navBar = [[UINavigationBar alloc]init];
    self.navBar.backgroundColor = RGBA(0, 0, 0, 0);
    
    [self.navBar setBackgroundImage:[UIImage imageWithColor:RGBA(0, 0, 0, 0) size:CGSizeMake(APP_WIDTH, 64)]
                       forBarPosition:UIBarPositionAny
                           barMetrics:UIBarMetricsDefault];
    [self.navBar setShadowImage:[UIImage new]];
    [self.navBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [self.view addSubview:self.navBar];
    
    [self.navBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top);
        make.right.equalTo(self.view.mas_right);
        make.height.mas_equalTo(64);
    }];
    
    self.navItem = [[UINavigationItem alloc]init];
    
    self.navBar.items = @[self.navItem];

    
    
    [self.loginView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.bottom.equalTo(self.view.mas_bottom);
        make.right.equalTo(self.view.mas_right);
    }];
//
    [self.registView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.bottom.equalTo(self.view.mas_bottom);
        make.right.equalTo(self.view.mas_right);
    }];
    
    
    [self.infoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.bottom.equalTo(self.view.mas_bottom);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    self.lastView = self.loginView;
    
    [self wr_setNavBarBackgroundAlpha:0.0];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark -
#pragma mark  登陆页按钮

-(void)registBtnClick:(id)sender{
    WEAK_SELF;
    self.isFindPassword = NO;
    self.registView.isFindPassword = NO;
    [UIView animateWithDuration:0.5 animations:^{
        weakSelf.loginView.alpha = .0;
    } completion:^(BOOL finished) {
        
        weakSelf.navItem.leftBarButtonItem = [self ittemLeftItemWithIcon:[[UIImage imageNamed:@"ic_back"] imageToColor:[UIColor whiteColor]] title:nil selector:@selector(registBack:)];
        weakSelf.navItem.title = @"注 册";
        
        [weakSelf.registView.nextBtn setTitle:@"下一步" forState:UIControlStateNormal];
        weakSelf.registView.alpha = 0.0;
        weakSelf.registView.hidden = NO;
        [UIView animateWithDuration:0.5 animations:^{
            weakSelf.registView.alpha = 1.0;
        }];
        
    }];
    
}

-(void)forgetBtnClick:(id)sender{
    WEAK_SELF;
    self.isFindPassword = YES;
    self.registView.isFindPassword = YES;
    [UIView animateWithDuration:0.5 animations:^{
        weakSelf.loginView.alpha = .0;
    } completion:^(BOOL finished) {
        
        weakSelf.navItem.leftBarButtonItem = [self ittemLeftItemWithIcon:[[UIImage imageNamed:@"ic_back"] imageToColor:[UIColor whiteColor]] title:nil selector:@selector(registBack:)];
        weakSelf.navItem.title = @"找回密码";
        
        [weakSelf.registView.nextBtn setTitle:@"找回密码" forState:UIControlStateNormal];
        weakSelf.registView.alpha = 0.0;
        weakSelf.registView.hidden = NO;
        [UIView animateWithDuration:0.5 animations:^{
            weakSelf.registView.alpha = 1.0;
        }];
        
    }];

}

#pragma mark -
#pragma mark 注册页按钮

-(void)registBack:(id)sender{
    WEAK_SELF;
    self.isOtherRegist = NO;
    [UIView animateWithDuration:0.5 animations:^{
        weakSelf.registView.alpha = .0;
        weakSelf.navItem.leftBarButtonItem = nil;
        weakSelf.navItem.title = @"";
    } completion:^(BOOL finished) {
        weakSelf.loginView.alpha = 0.0;
        weakSelf.loginView.hidden = NO;
        [UIView animateWithDuration:0.5 animations:^{
            weakSelf.loginView.alpha = 1.0;
        }];
    }];

}

-(void)registNextBtnClick:(id)sender{
    WEAK_SELF;
    
    if (self.isOtherRegist){
        [self regist];
        return;
    }
    
    if (!self.isFindPassword){
        
        [UIView animateWithDuration:0.5 animations:^{
            weakSelf.registView.alpha = .0;
        } completion:^(BOOL finished) {
            weakSelf.navItem.leftBarButtonItem = [self ittemLeftItemWithIcon:[[UIImage imageNamed:@"ic_back"] imageToColor:[UIColor whiteColor]] title:nil selector:@selector(infoViewBack:)];
            weakSelf.navItem.title = @"填写资料";
            
            weakSelf.infoView.alpha = 0.0;
            weakSelf.infoView.hidden = NO;
            [UIView animateWithDuration:0.5 animations:^{
                weakSelf.infoView.alpha = 1.0;
            }];
            
        }];
    }else{
        __weak MBProgressHUD *hud = [LLUtils showActivityIndicatiorHUDWithTitle:nil];
        
        NSMutableDictionary *dict = [self.postModel keyValues];
        if (self.postModel.phone) [dict setValue:self.postModel.phone forKey:@"account"];
        if (self.postModel.email) [dict setValue:self.postModel.email forKey:@"account"];
        
        if ([self.postModel.phone rangeOfString:@"%@"].location == NSNotFound){
            [dict setValue:@"0" forKey:@"type"];
        }else{
            [dict setValue:@"1" forKey:@"type"];
        }
        
        LoginApi *loginApi = [[LoginApi alloc]initWitObject:dict];
        loginApi.method = YTKRequestMethodPUT;
        [loginApi startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            [hud hideAnimated:NO];
            if (info.error == ErrorCodeType_None) {
                [LLUtils showActionSuccessHUD:@""];
                [weakSelf registBack:nil];
            }else{
                [LLUtils showCenterTextHUD:info.message];
            }
        }];
    }

}


#pragma mark -
#pragma mark - 登陆

-(void)loginWithInfo:(NSDictionary *)dic{
    [[[LoginApi alloc]initWitObject:dic] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        
        if (info.error != ErrorCodeType_None){
            
            return ;
        }
        
        LoginInfoModel *model = [LoginInfoModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
        
        [[RCUserCacheManager sharedManager]setCurrentLoginInfo:model];
        
        [[[CommonApi alloc]initWithModel:@"accessToken" object:model] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            
            AccessTokenModel *model = [AccessTokenModel objectWithKeyValues:responseObj];
            [[RCUserCacheManager sharedManager] setCurrentToken:model];
            
            [[[MemberApi alloc]initWitObject:responseObj] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                
                UserModel *userModel = [UserModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
                [[RCUserCacheManager sharedManager] setCurrentUser:userModel];
                [[RouteController sharedManager] openMainController];
                
            }];
            
        }];
        
    }];

}

#pragma mark -
#pragma mark 填写信息页按钮

-(void)doneBtnClick:(id)sender{
    
}

-(void)infoViewBack:(id)sender{
    WEAK_SELF;

    
    self.navItem.leftBarButtonItem = [self ittemLeftItemWithIcon:[[UIImage imageNamed:@"ic_back"] imageToColor:[UIColor whiteColor]] title:nil selector:@selector(registBack:)];
    self.navItem.title = @"注 册";
    [UIView animateWithDuration:0.5 animations:^{
        weakSelf.infoView.alpha = .0;
        
    } completion:^(BOOL finished) {
        weakSelf.registView.alpha = 0.0;
        weakSelf.registView.hidden = NO;
        [UIView animateWithDuration:0.5 animations:^{
            weakSelf.registView.alpha = 1.0;
        }];
    }];
}


#pragma mark -

-(void)regist{
    __block MBProgressHUD *hud =  [LLUtils showActivityIndicatiorHUDWithTitle:nil];
    
    WEAK_SELF;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        __block NSString *token = nil;
        
        [[[CommonApi alloc]initWithModel:@"common/uptoken" object:nil]startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            if (info.error == ErrorCodeType_None){
                token = [responseObj objectForKey:@"data"];
            }else{
                token = @"";
            }
        }];
        
        while (token == nil) {
            [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }
        
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.postModel.imagelink]];
        
        NSString *key = [NSString randomString];
        __block NSString *imageName = nil;
        [[[QNUploadManager alloc]init] putData:data key:key token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
            if (!info.error) {
                imageName = key;
            }else{
                imageName = @"";
            }
            NSLog(@"info %@",info.error.description);
        } option:nil];
        
        while (imageName == nil) {
            [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }
        
        //        WEAK_SELF;
        weakSelf.postModel.image = imageName;
        LoginApi *loginApi = [[LoginApi alloc]initWitObject:self.postModel];
        loginApi.method = YTKRequestMethodPOST;
        
        
        [loginApi startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            [hud hideAnimated:NO];
            
            if (info.error != ErrorCodeType_None){
                [hud hideAnimated:NO];
                [LLUtils showCenterTextHUD:info.message];
                return ;
            }
            
            LoginInfoModel *model = [LoginInfoModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
            
            [[RCUserCacheManager sharedManager]setCurrentLoginInfo:model];
            
            
            
            [[[CommonApi alloc]initWithModel:@"accessToken" object:model] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                
                if (info.error != ErrorCodeType_None){
                    [hud hideAnimated:NO];
                    [LLUtils showCenterTextHUD:info.message];
                    return ;
                }
                
                AccessTokenModel *model = [AccessTokenModel objectWithKeyValues:responseObj];
                [[RCUserCacheManager sharedManager] setCurrentToken:model];
                
                [[[MemberApi alloc]initWitObject:responseObj] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                    
                    if (info.error != ErrorCodeType_None){
                        [hud hideAnimated:NO];
                        [LLUtils showCenterTextHUD:info.message];
                        return ;
                    }
                    
                    UserModel *userModel = [UserModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
                    [[RCUserCacheManager sharedManager] setCurrentUser:userModel];
                    [[RouteController sharedManager] openMainController];
                    
                }];
                
            }];
            
        }];
        
        
    });
    
}




#pragma mark -
#pragma mark lazy layout

-(AccountLoginView *)loginView{
    if (!_loginView) {
        _loginView = [[AccountLoginView alloc]init];
//        _loginView.hidden = YES;
        [_loginView.registBtn addTarget:self action:@selector(registBtnClick:)];
        [_loginView.forgetBtn addTarget:self action:@selector(forgetBtnClick:)];
        _loginView.postModel = self.postModel;
        WEAK_SELF;
        _loginView.loginFailBlock = ^(id responseObj) {
            weakSelf.isOtherRegist = YES;
            [weakSelf registBtnClick:nil];
//            RegisterVC *vc = [[RegisterVC alloc]init];
//            vc.viewModel.model = weakSelf.loginView.postModel;
//            [weakSelf.navigationController pushViewController:vc animated:YES];
        };
        [self.view addSubview:_loginView];
    }
    return _loginView;
}

-(AccountRegistView *)registView{
    if (!_registView) {
        _registView = [[AccountRegistView alloc]init];
        _registView.hidden = YES;
        _registView.postModel = self.postModel;
        [_registView.nextBtn addTarget:self action:@selector(registNextBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_registView];
    }
    return _registView;
}

-(AccountInfoView *)infoView{
    if (!_infoView) {
        _infoView = [[AccountInfoView alloc]init];
        _infoView.hidden = YES;
        _infoView.postModel = self.postModel;
        [_infoView.doneBtn addTarget:self action:@selector(doneBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_infoView];
    }
    return _infoView;
}


-(UIImageView *)backgroundImage{
    if (!_backgroundImage) {
        _backgroundImage = [[UIImageView alloc]init];
        _backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
        _backgroundImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _backgroundImage.image = [UIImage imageNamed:@"login_backgroundImage"];
        [self.view addSubview:_backgroundImage];
    }
    return _backgroundImage;
}

@end
