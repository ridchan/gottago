//
//  UserHomePageGetApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/25.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserHomePageGetApi.h"

@implementation UserHomePageGetApi

-(id)initWithMember:(NSString *)member_id{
    if (self = [super init]) {
        [self.params setValue:member_id forKey:@"member_id"];
    }
    return self;
}


-(NSString *)requestUrl{
    return UserHomePageUrl;
}

@end
