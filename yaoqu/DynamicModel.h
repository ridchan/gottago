//
//  DynamicModel.h
//  yaoqu
//
//  Created by ridchan on 2017/6/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface DynamicModel : BaseModel

@property(nonatomic,strong) NSString *dynamic_id;
@property(nonatomic,strong) NSString *desc;
@property(nonatomic,strong) NSString *video;
@property(nonatomic,strong) NSString *video_image;
@property(nonatomic) long long time;
@property(nonatomic) BOOL is_location;
@property(nonatomic,strong) NSString *city_id;
@property(nonatomic,strong) NSString *city_name;
@property(nonatomic,strong) NSString *dynamic_city_name;
@property(nonatomic,strong) NSString *address;
@property(nonatomic,strong) NSString *comment;
@property(nonatomic,strong) NSString *like;
@property(nonatomic,strong) NSString *preview;
@property(nonatomic,strong) NSString *member_id;
@property(nonatomic,strong) NSString *phone;
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *image;
@property(nonatomic,strong) NSString *images;
@property(nonatomic,strong) NSString *sex;
@property(nonatomic,strong) NSString *age;
@property(nonatomic,strong) NSString *constellation;
@property(nonatomic,strong) NSString *gift_data;
@property(nonatomic,strong) NSString *gift_qty;
@property(nonatomic) BOOL is_age;
@property(nonatomic,strong) NSString *pro_id;
@property(nonatomic,strong) NSString *pro_name;
@property(nonatomic,strong) NSString *home_pro_id;
@property(nonatomic,strong) NSString *home_pro_name;
@property(nonatomic,strong) NSString *home_city_id;
@property(nonatomic,strong) NSString *home_city_name;
@property(nonatomic,strong) NSString *exp;
@property(nonatomic,strong) NSString *level;
@property(nonatomic) BOOL is_member;
@property(nonatomic) BOOL is_talent;
@property(nonatomic) BOOL is_gold;
@property(nonatomic) BOOL is_like;
@property(nonatomic) BOOL is_video;
@property(nonatomic,strong) NSString *content;
@property(nonatomic,strong) NSArray *content_images;

@property(nonatomic) CGFloat listHeight;



@end


@interface DynamicUploadModel : BaseModel

@property(nonatomic,strong) NSString *desc;
@property(nonatomic,strong) NSString *images;
@property(nonatomic,strong) NSString *video;
@property(nonatomic,strong) NSString *video_image;
@property(nonatomic) BOOL is_video;
@property(nonatomic) NSInteger is_location;
@property(nonatomic,strong) NSString *latitude;
@property(nonatomic,strong) NSString *longitude;
@property(nonatomic,strong) NSString *address;

@property(nonatomic,strong) NSString *city_name;
@property(nonatomic,strong) NSArray *imageDatas;


@end
