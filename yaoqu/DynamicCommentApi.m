//
//  DynamicCommentApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/29.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicCommentApi.h"

@implementation DynamicCommentApi{
    NSString *_dynamic_id;
    NSString *_comment_id;
    NSString *_desc;
}

-(id)initWithDynamic_id:(NSString *)dynamic_id comment_id:(NSString *)comment_id desc:(NSString *)desc{
    if (self = [super init]) {
        _dynamic_id  =  dynamic_id;
        _comment_id = comment_id;
        _desc = desc;
    }
    return self;
}


-(NSString *)requestUrl{
    return DynamicCommentUrl;
}

-(id)requestArgument{
    NSMutableDictionary *dic = [self baseConfig];
    [dic setValue:_dynamic_id forKey:@"dynamic_id"];
    [dic setValue:_comment_id forKey:@"comment_id"];
    [dic setValue:_desc forKey:@"desc"];
    return dic;
}

@end
