//
//  MineRewardCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineRewardCell.h"

@interface MineRewardCell()

@property(nonatomic,strong) UIImageView *userImage;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UILabel *dateLbl;

@property(nonatomic,strong) ImageButton *coinBtn;
@property(nonatomic,strong) ImageButton *scoreBtn;

@property(nonatomic,strong) UILabel *coinLbl;
@property(nonatomic,strong) UILabel *scoreLbl;

@end

@implementation MineRewardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self commonInit];
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    
}

-(void)commonInit{
    [self.userImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.top.equalTo(self.contentView.mas_top).offset(20);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-20);
        make.width.mas_equalTo(self.userImage.mas_height);
    }];

    
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userImage.mas_right).offset(10);
        make.top.equalTo(self.userImage.mas_top);
    }];
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.userImage.mas_centerY);
    }];
    
    [self.dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.bottom.equalTo(self.userImage.mas_bottom);
    }];
    
    [self.coinLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.bottom.equalTo(self.scoreLbl.mas_bottom).offset(-20);
        make.width.mas_equalTo(60);
    }];
    
    [self.scoreLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.bottom.equalTo(self.userImage.mas_bottom);
        make.width.mas_equalTo(60);
    }];
    
    [self.coinBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.centerY.equalTo(self.coinLbl.mas_centerY);
        make.right.equalTo(self.coinLbl.mas_left).offset(10);
    }];
    
    [self.scoreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.centerY.equalTo(self.scoreLbl.mas_centerY);
        make.left.equalTo(self.coinBtn.mas_left);
    }];
    
}


-(void)setModel:(MineRewardModel *)model{
    _model = model;
    [self.userImage setImageName:model.image placeholder:nil];
    self.nameLbl.text = model.username;
    self.dateLbl.text = model.time;
    self.coinLbl.text = [NSString stringWithFormat:@"+%@",model.currency];
    self.scoreLbl.text = [NSString stringWithFormat:@"+%@",model.integral];
    
}

-(UIImageView *)userImage{
    if (!_userImage) {
        _userImage = [[UIImageView alloc]init];
        _userImage.clipsToBounds = YES;
        _userImage.contentMode = UIViewContentModeScaleAspectFill;
        _userImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        _userImage.layer.cornerRadius = 30;
        _userImage.layer.masksToBounds = YES;
        [self.contentView addSubview:_userImage];
    }
    return _userImage;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        _nameLbl.font = SystemFont(14);
        [self.contentView addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.font = SystemFont(11);
        _descLbl.text = LS(@"奖励时间");
        _descLbl.textColor = AppGray;
        [self.contentView addSubview:_descLbl];
    }
    return _descLbl;
}

-(UILabel *)dateLbl{
    if (!_dateLbl) {
        _dateLbl = [ControllerHelper autoFitLabel];
        _dateLbl.font = SystemFont(11);
        _dateLbl.textColor = AppGray;
        [self.contentView addSubview:_dateLbl];
    }
    return _dateLbl;
}

-(ImageButton *)coinBtn{
    if (!_coinBtn) {
        _coinBtn = [[ImageButton alloc]init];
        _coinBtn.contentImage.image = [UIImage imageNamed:@"ic_coin"];
        _coinBtn.titleLbl.font = SystemFont(12);
        _coinBtn.titleLbl.text = @"旅游币";
        [self.contentView addSubview:_coinBtn];
    }
    return _coinBtn;
}

-(ImageButton *)scoreBtn{
    if (!_scoreBtn) {
        _scoreBtn = [[ImageButton alloc]init];
        _scoreBtn.contentImage.image = [UIImage imageNamed:@"ic_score"];
        _scoreBtn.titleLbl.font = SystemFont(12);
        _scoreBtn.titleLbl.text = @"积分";
        [self.contentView addSubview:_scoreBtn];
    }
    return _scoreBtn;
}

-(UILabel *)coinLbl{
    if (!_coinLbl) {
        _coinLbl = [ControllerHelper autoFitLabel];
        _coinLbl.font = SystemFont(12);
        _coinLbl.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_coinLbl];
    }
    return _coinLbl;
}

-(UILabel *)scoreLbl{
    if (!_scoreLbl) {
        _scoreLbl = [ControllerHelper autoFitLabel];
        _scoreLbl.font = SystemFont(12);
        _scoreLbl.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_scoreLbl];
    }
    return _scoreLbl;
}


@end
