//
//  MineHelpCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/18.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineHelpCell.h"

@interface MineHelpCell()

@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UIImageView *arrowImage;
@property(nonatomic,strong) UIView *centerLine;
@property(nonatomic,strong) UIView *bottomLine;
@property(nonatomic,strong) UIButton *expandBtn;


@end

@implementation MineHelpCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.clipsToBounds = YES;
        self.clipsToBounds = YES;
        [self commonInit];
    }
    return self;
}

-(void)commonInit{

    
    
    self.titleLbl.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .topSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 40)
    .autoHeightRatio(0);
    

    
    self.arrowImage.sd_layout
    .rightSpaceToView(self.contentView,10)
    .centerYEqualToView(self.titleLbl)
    .heightIs(40)
    .widthIs(15);
    
    
    self.centerLine.sd_layout
    .leftEqualToView(self.contentView)
    .rightEqualToView(self.contentView)
    .topSpaceToView(self.titleLbl, 15)
    .heightIs(0.5);
    
    
    self.expandBtn.sd_layout
    .topEqualToView(self.contentView)
    .leftEqualToView(self.contentView)
    .rightEqualToView(self.contentView)
    .bottomEqualToView(self.centerLine);
    
    
    self.descLbl.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .topSpaceToView(self.centerLine, 15)
    .rightSpaceToView(self.arrowImage, 10)
    .autoHeightRatio(0);
    
    self.bottomLine.sd_layout
    .leftEqualToView(self.contentView)
    .rightEqualToView(self.contentView)
    .topSpaceToView(self.titleLbl, 15)
    .heightIs(0.5);
    
    
    

    
    [self setupAutoHeightWithBottomView:self.bottomLine bottomMargin:0];
    
}


-(void)setModel:(QuestionHelpModel *)model{
    _model = model;
    self.titleLbl.text = model.title;
    self.descLbl.text = model.desc;
    
    if (!self.model.isExpand) {
        self.arrowImage.transform = CGAffineTransformMakeRotation(M_PI / 2.0);
        
        self.bottomLine.sd_resetLayout
        .leftEqualToView(self.contentView)
        .rightEqualToView(self.contentView)
        .topSpaceToView(self.titleLbl, 15)
        .heightIs(0.5);
    }else{
        
        self.arrowImage.transform = CGAffineTransformMakeRotation(M_PI_2 * 3.0);
        self.bottomLine.sd_resetLayout
        .leftEqualToView(self.contentView)
        .rightEqualToView(self.contentView)
        .topSpaceToView(self.descLbl, 15)
        .heightIs(0.5);
    }

}



-(void)expandBtnClick:(id)sender{

    self.model.isExpand = !self.model.isExpand;
    if (self.actionBlock) {
        self.actionBlock(@(self.model.isExpand));
    }
}

#pragma mark -

-(UIButton *)expandBtn{
    if (!_expandBtn) {
        _expandBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_expandBtn addTarget:self action:@selector(expandBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_expandBtn];
    }
    return _expandBtn;
}

-(UIImageView *)arrowImage{
    if (!_arrowImage) {
        _arrowImage = [[UIImageView alloc]init];
        _arrowImage.contentMode = UIViewContentModeScaleAspectFit;
        _arrowImage.image = [UIImage imageNamed:@"ic_arrow_forward"];
        [self.contentView addSubview:_arrowImage];
    }
    return _arrowImage;
}

-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        _titleLbl.font = SystemFont(14);
        _titleLbl.numberOfLines = 0;
        [self.contentView addSubview:_titleLbl];
    }
    return _titleLbl;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.font = SystemFont(12);
        _descLbl.textColor = AppGray;
        _descLbl.numberOfLines = 0;
        [self.contentView addSubview:_descLbl];
    }
    return _descLbl;
}

-(UIView *)centerLine{
    if (!_centerLine) {
        _centerLine = [[UIView alloc]init];
        _centerLine.backgroundColor = AppLineColor;
        [self.contentView addSubview:_centerLine];
    }
    return _centerLine;
}

-(UIView *)bottomLine{
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc]init];
        _bottomLine.backgroundColor = AppLineColor;
        [self.contentView addSubview:_bottomLine];
    }
    return _bottomLine;
}

@end
