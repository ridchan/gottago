//
//  AccountRegistView.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/8.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginPostModel.h"

@interface AccountRegistView : UIView

@property(nonatomic,strong) LoginPostModel *postModel;
@property(nonatomic,strong) NormalButton *nextBtn;
@property(nonatomic) BOOL isFindPassword;

@end
