//
//  GiftPostModel.h
//  yaoqu
//
//  Created by ridchan on 2017/8/24.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface GiftPostModel : BaseModel

@property(nonatomic,strong)  NSString *gift_id;
@property(nonatomic,strong) NSString *key_id;
@property(nonatomic) NSInteger type;
@property(nonatomic,strong) NSString *member_id;
@property(nonatomic,strong) NSString *desc;

@property(nonatomic) NSInteger qty;

@end
