//
//  TravelModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/18.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface TravelModel : BaseModel



@property(nonatomic,strong) NSString *signup_id;
@property(nonatomic,strong) NSString *travel_id;
@property(nonatomic,strong) NSString *travel_schedule_id;
@property(nonatomic,strong) NSString *out_trade_no;
@property(nonatomic,strong) NSString *date;
@property(nonatomic,strong) NSString *total;
@property(nonatomic,strong) NSString *integral;
@property(nonatomic,strong) NSString *currency;
@property(nonatomic,strong) NSString *pay_total;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *image;
@property(nonatomic,strong) NSString *share_image;
@property(nonatomic,strong) NSString *city;
@property(nonatomic,strong) NSString *address;
@property(nonatomic,strong) NSString *chatroom_id;
@property(nonatomic,strong) NSString *is_travels;
@property(nonatomic,strong) NSArray *contact_data;

@end
