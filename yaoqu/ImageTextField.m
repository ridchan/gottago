//
//  ImageTextField.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/8.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "ImageTextField.h"


@interface ImageTextField()


@property(nonatomic) CGSize imgSize;


@end

@implementation ImageTextField



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+(ImageTextField *)imageText:(NSString *)imageName placeHolder:(NSString *)placeHolder{
    ImageTextField *textField = [[ImageTextField alloc]init];
//    textField.placeholder = placeHolder;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.textColor = [UIColor whiteColor];
    NSAttributedString *att = [[NSAttributedString alloc]initWithString:placeHolder attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    textField.attributedPlaceholder = att;
    textField.imgSize = CGSizeZero;
    UIImage *image = [UIImage imageNamed:imageName];
    if (image) {
        textField.imgSize = image.size;
    }
    
    UIView *leftView = [textField leftViewWithImage:image];
    textField.leftView = leftView;
    
    [[textField lineInColor:RGB16(0xfffeff)] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(textField.mas_left);
        make.right.equalTo(textField.mas_right);
        make.bottom.equalTo(textField.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];
    
    return textField;
}


-(void)layoutSubviews{
    [super layoutSubviews];
    
    if (self.leftView) {
        CGFloat height = self.frame.size.height;
        self.leftView.frame = CGRectMake(0, 0, height , height);
    }
}

-(UIView *)leftViewWithImage:(UIImage *)image{
    UIView *view = [[UIView alloc]init];
    UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    
    [view addSubview:imageView];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left);
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerY.equalTo(view.mas_centerY);
    }];
    
    return view;
}





@end
