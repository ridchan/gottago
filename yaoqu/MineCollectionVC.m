//
//  MineCollectionVC.m
//  yaoqu
//
//  Created by ridchan on 2017/8/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineCollectionVC.h"
#import "NavTitleView.h"
#import "RCVCScrollView.h"

@interface MineCollectionVC ()

@property(nonatomic,strong) NavTitleView *titleView;
@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) RCVCScrollView *vcScroll;

@end

@implementation MineCollectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = LS(@"我的收藏");
    
    [self titleView];
    
    [self.vcScroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleView.mas_bottom);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NavTitleView *)titleView{
    if (!_titleView) {
        _titleView = [[NavTitleView alloc]initWithFrame:CGRectMake(0, 65, APP_WIDTH, 40)];
        _titleView.backgroundColor = [UIColor whiteColor];

        _titleView.titles = @[@"动态",@"套餐"];

        WEAK_SELF;
        _titleView.selectBlock = ^(id responseObj) {
            weakSelf.vcScroll.selectIndex = [responseObj integerValue];
        };
        [self.view addSubview:_titleView];
    }
    return _titleView;
}

-(RCVCScrollView *)vcScroll{
    if (!_vcScroll) {
        _vcScroll = [[RCVCScrollView alloc]init];
        _vcScroll.vcNames = @[@"MineCollectDynamicVC",@"MineCollectTravelVC"];
        WEAK_SELF;
        _vcScroll.selectBlock = ^(id responseObj) {
            weakSelf.titleView.contentOffset = [responseObj floatValue];
        };
        [self.view addSubview:_vcScroll];
    }
    return _vcScroll;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
