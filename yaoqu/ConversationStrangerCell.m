//
//  ConversationStrangerCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/9.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "ConversationStrangerCell.h"
#import "LLConversationModel.h"

@interface ConversationStrangerCell()

@property(nonatomic,strong) UIImageView *userImg;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UILabel *dateLbl;
@property(nonatomic,strong) UIView *bgView;
@property(nonatomic,strong) UIView *yellowView;


@end

@implementation ConversationStrangerCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self commonInit];
        
        self.nameLbl.text = @"来自陌生人的消息";
        
//        [self rac_prepareForReuseSignal];
        
//        WEAK_SELF;
//        [RACObserve(self,conversationModel.model.autoid) subscribeNext:^(NSString *x) {
//            NSLog(@"auto id %@",x);
//            dispatch_async(dispatch_get_main_queue(), ^{
//                weakSelf.userImg.userModel = weakSelf.conversationModel.model;
//                weakSelf.nameLbl.text = weakSelf.conversationModel.model.username;
//
//            });
//            
//        }];
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    self.bgView.layer.cornerRadius = self.bgView.frame.size.width / 2;
    self.bgView.layer.masksToBounds = YES;
    
}


-(void)commonInit{
    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.mas_top).offset(10);
        make.bottom.equalTo(self.mas_bottom).offset(-10);
        make.width.equalTo(self.bgView.mas_height);
    }];
    
    [self.userImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.top.equalTo(self.mas_top).offset(20);
        make.bottom.equalTo(self.mas_bottom).offset(-20);
        make.width.equalTo(self.userImg.mas_height);
    }];

    
    
    
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bgView.mas_right).offset(5);
        make.bottom.equalTo(self.bgView.mas_centerY).offset(-5);
    }];
    
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bgView.mas_right).offset(5);
        make.top.equalTo(self.bgView.mas_centerY).offset(5);
    }];
    
    [self.dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
    }];
    
    [self.yellowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.centerY.equalTo(self.descLbl.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(6, 6));
    }];
    self.yellowView.layer.masksToBounds = YES;
    self.yellowView.layer.cornerRadius = 3.0;
}

-(UIView *)bgView{
    if (!_bgView) {
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = AppOrange;
        
        
        [self addSubview:_bgView];
    }
    return _bgView;
}

-(UIImageView *)userImg{
    if (!_userImg) {
        _userImg = [[UIImageView alloc]init];
        _userImg.image = [UIImage imageNamed:@"ic_chat_stranger"];
        [self addSubview:_userImg];
    }
    return _userImg;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        _nameLbl.font = UserNameFont;
        [self addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.font = DetailFont;
        _descLbl.textColor = AppGray;
        [self addSubview:_descLbl];
    }
    return _descLbl;
}

-(UILabel *)dateLbl{
    if (!_dateLbl) {
        _dateLbl = [ControllerHelper autoFitLabel];
        _dateLbl.font = [UIFont systemFontOfSize:10];
        _dateLbl.textAlignment = NSTextAlignmentRight;
        _dateLbl.textColor = RGB16(0x979797);
        [self addSubview:_dateLbl];
    }
    return _dateLbl;
}


-(UIView *)yellowView{
    if (!_yellowView) {
        _yellowView = [[UIView alloc]init];
        _yellowView.backgroundColor = AppRedPoint;
        _yellowView.hidden = YES;
        [self addSubview:_yellowView];
    }
    return _yellowView;
}

-(void)setConversationModels:(NSArray *)conversationModels{
    _conversationModels = conversationModels;
    
    LLConversationModel *conversationModel = [conversationModels firstObject];
    
    for (LLConversationModel *model in conversationModels){
        NSLog(@"model:%@",model.model.username);
    }
    self.dateLbl.text = conversationModel.latestMessageTimeString;
    self.yellowView.hidden = conversationModel.unreadMessageNumber == 0;
    self.descLbl.text = [NSString stringWithFormat:@"%@：%@",conversationModel.model.username,conversationModel.latestMessage];
        
}


- (void)markAllMessageAsRead {
    
    self.yellowView.hidden = YES;
//    [[LLChatManager sharedManager] markAllMessagesAsRead:self.conversationModel];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
