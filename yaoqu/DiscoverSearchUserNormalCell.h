//
//  DiscoverSearchUserNormalCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/6.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"

@interface DiscoverSearchUserNormalCell : UITableViewCell

@property(nonatomic,strong) UserModel *userModel;

@end
