//
//  TravelEnrollDetailView.m
//  yaoqu
//
//  Created by ridchan on 2017/7/16.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelEnrollDetailView.h"
#import "RCUserCacheManager.h"

@interface TravelEnrollDetailView()

@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) UIView *line;
@property(nonatomic,strong) UILabel *titleLbl1;
@property(nonatomic,strong) UILabel *titleLbl2;
@property(nonatomic,strong) UILabel *titleLbl3;

@property(nonatomic,strong) UIImageView *img1;
@property(nonatomic,strong) UILabel *nameLbl1;
@property(nonatomic,strong) UILabel *priceLbl1;
@property(nonatomic,strong) UITextField *textFld1;

@property(nonatomic,strong) UIImageView *img2;
@property(nonatomic,strong) UILabel *nameLbl2;
@property(nonatomic,strong) UILabel *priceLbl2;
@property(nonatomic,strong) UITextField *textFld2;


@property(nonatomic,strong) UIView *line2;
@property(nonatomic,strong) UIView *line3;


@property(nonatomic,strong) UILabel *totalTitleLbl;
@property(nonatomic,strong) UILabel *payTitleLbl;
@property(nonatomic,strong) UILabel *totalLbl;
@property(nonatomic,strong) UILabel *payLbl;

@end

@implementation TravelEnrollDetailView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}


-(void)commonInit{
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.mas_top);
        make.height.mas_equalTo(40);
    }];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.titleLbl.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];
    
    [self.titleLbl1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.line.mas_bottom).offset(5);
        make.height.mas_equalTo(30);
    }];
    
    [self.titleLbl2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.line.mas_bottom).offset(5);
        make.height.mas_equalTo(30);
    }];
    
    [self.titleLbl3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.line.mas_bottom).offset(5);
        make.right.equalTo(self.mas_right).offset(-10);
        make.height.mas_equalTo(30);
    }];
    
    //=========
    [self.img1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.line.mas_bottom).offset(50);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    [self.nameLbl1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.img1.mas_right).offset(5);
        make.centerY.equalTo(self.img1.mas_centerY);
    }];
    
    [self.priceLbl1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl2.mas_left);
        make.centerY.equalTo(self.img1.mas_centerY);
    }];
    
    [self.textFld1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.titleLbl3.mas_right);
        make.centerY.equalTo(self.img1.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50, 20));
    }];
    
    //=========
    [self.img2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.img1.mas_bottom).offset(15);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    [self.nameLbl2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.img2.mas_right).offset(5);
        make.centerY.equalTo(self.img2.mas_centerY);
    }];
    
    [self.priceLbl2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl2.mas_left);
        make.centerY.equalTo(self.img2.mas_centerY);
    }];
    
    [self.textFld2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.titleLbl3.mas_right);
        make.centerY.equalTo(self.img2.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50, 20));
    }];
    
    //====
    [self.line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.img2.mas_bottom).offset(10);
        make.height.mas_equalTo(0.5);
    }];
    
    [self.totalTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.line2.mas_bottom);
        make.height.mas_equalTo(40);
    }];
    
    [self.totalLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.centerY.equalTo(self.totalTitleLbl.mas_centerY);
    }];
    
    //====
    [self.line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.totalTitleLbl.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];
    
    [self.payTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.line3.mas_bottom);
        make.height.mas_equalTo(40);
        make.bottom.equalTo(self.mas_bottom);
    }];
    
    [self.payLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.centerY.equalTo(self.payTitleLbl.mas_centerY);
    }];
    
}


-(UILabel *)totalTitleLbl{
    if (!_totalTitleLbl) {
        _totalTitleLbl = [ControllerHelper autoFitLabel];
        _totalTitleLbl.text = LS(@"总计");
        _totalTitleLbl.font = SystemFont(14);
        [self addSubview:_totalTitleLbl];
    }
    return _totalTitleLbl;
}

-(UILabel *)payTitleLbl{
    if (!_payTitleLbl) {
        _payTitleLbl = [ControllerHelper autoFitLabel];
        _payTitleLbl.text = LS(@"实付");
        _payTitleLbl.font = SystemFont(14);
        [self addSubview:_payTitleLbl];
    }
    return _payTitleLbl;
}

-(UILabel *)totalLbl{
    if (!_totalLbl) {
        _totalLbl = [ControllerHelper autoFitLabel];
        _totalLbl.text = LS(@"0");
        _totalLbl.font = SystemFont(14);
        [self addSubview:_totalLbl];
    }
    return _totalLbl;
}

-(UILabel *)payLbl{
    if (!_payLbl) {
        _payLbl = [ControllerHelper autoFitLabel];
        _payLbl.text = LS(@"0");
        _payLbl.textColor = [UIColor orangeColor];
        _payLbl.font = SystemFont(14);
        [self addSubview:_payLbl];
    }
    return _payLbl;
}



-(UIView *)line2{
    if (!_line2) {
        _line2 = [[UIView alloc]init];
        _line2.backgroundColor = AppLineColor;
        [self addSubview:_line2];
    }
    return _line2;
}

-(UIView *)line3{
    if (!_line3) {
        _line3 = [[UIView alloc]init];
        _line3.backgroundColor = AppLineColor;
        [self addSubview:_line3];
    }
    return _line3;
}




-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        _titleLbl.text = LS(@"积分+旅游币兑现");
        [self addSubview:_titleLbl];
    }
    return _titleLbl;
}



-(UIView *)line{
    if (!_line) {
        _line = [[UIView alloc]init];
        _line.backgroundColor = AppLineColor;
        [self addSubview:_line];
    }
    return _line;
}

-(UILabel *)titleLbl1{
    if (!_titleLbl1) {
        _titleLbl1 = [ControllerHelper autoFitLabel];
        _titleLbl1.text = LS(@"方法");
        _titleLbl1.font = SystemFont(12);
        [self addSubview:_titleLbl1];
    }
    return _titleLbl1;
}

-(UILabel *)titleLbl2{
    if (!_titleLbl2) {
        _titleLbl2 = [ControllerHelper autoFitLabel];
        _titleLbl2.text = LS(@"可用");
        _titleLbl2.font = SystemFont(12);
        [self addSubview:_titleLbl2];
    }
    return _titleLbl2;
}

-(UILabel *)titleLbl3{
    if (!_titleLbl3) {
        _titleLbl3 = [ControllerHelper autoFitLabel];
        _titleLbl3.text = LS(@"数量");
        _titleLbl3.font = SystemFont(12);
        [self addSubview:_titleLbl3];
    }
    return _titleLbl3;
}

-(UIImageView *)img1{
    if (!_img1) {
        _img1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_score_yellow"]];
        [self addSubview:_img1];
    }
    return _img1;
}


-(UIImageView *)img2{
    if (!_img2) {
        _img2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_coin_yellow"]];
        [self addSubview:_img2];
    }
    return _img2;
}

-(UILabel *)nameLbl1{
    if (!_nameLbl1) {
        _nameLbl1 = [ControllerHelper autoFitLabel];
        _nameLbl1.text = LS(@"积分");
        _nameLbl1.font = SystemFont(12);
        [self addSubview:_nameLbl1];
    }
    return _nameLbl1;
}

-(UILabel *)nameLbl2{
    if (!_nameLbl2) {
        _nameLbl2 = [ControllerHelper autoFitLabel];
        _nameLbl2.text = LS(@"旅游币");
        _nameLbl2.font = SystemFont(12);
        [self addSubview:_nameLbl2];
    }
    return _nameLbl2;
}

-(UILabel *)priceLbl1{
    if (!_priceLbl1) {
        _priceLbl1 = [ControllerHelper autoFitLabel];
        _priceLbl1.text = @"0";
        _priceLbl1.font = SystemFont(12);
        [self addSubview:_priceLbl1];
    }
    return _priceLbl1;
}

-(UILabel *)priceLbl2{
    if (!_priceLbl2) {
        _priceLbl2 = [ControllerHelper autoFitLabel];
        _priceLbl2.text = LS(@"0");
        _priceLbl2.font = SystemFont(12);
        [self addSubview:_priceLbl2];
    }
    return _priceLbl2;
}

-(UITextField *)textFld1{
    if (!_textFld1) {
        _textFld1 = [[UITextField alloc]init];
        _textFld1.font = SystemFont(12);
//        _textFld1.placeholder = @"555";
        _textFld1.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _textFld1.textAlignment = NSTextAlignmentRight;
        _textFld1.layer.cornerRadius = 3;
        _textFld1.layer.masksToBounds = YES;
        _textFld1.layer.borderWidth = 0.5;
        _textFld1.keyboardType = UIKeyboardTypeNumberPad;
        _textFld1.layer.borderColor = [UIColor lightGrayColor].CGColor;
        [_textFld1 addTarget:self action:@selector(integralFldChange:) forControlEvents:UIControlEventAllEvents];
        [self addSubview:_textFld1];
    }
    return _textFld1;
}


-(UITextField *)textFld2{
    if (!_textFld2) {
        _textFld2 = [[UITextField alloc]init];
//        _textFld2.placeholder = @"555";
        _textFld2.font = SystemFont(12);
        _textFld2.textAlignment = NSTextAlignmentRight;
        _textFld2.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _textFld2.layer.cornerRadius = 3;
        _textFld2.layer.masksToBounds = YES;
        _textFld2.layer.borderWidth = 0.5;
        _textFld2.keyboardType = UIKeyboardTypeNumberPad;
        _textFld2.layer.borderColor = [UIColor lightGrayColor].CGColor;
        [_textFld2 addTarget:self action:@selector(coinFldChange:) forControlEvents:UIControlEventAllEvents];
        [self addSubview:_textFld2];
    }
    return _textFld2;
}


-(void)setScheduleModel:(TravelScheduleModel *)scheduleModel{
    _scheduleModel = scheduleModel;
    
    UserModel *userModel = [RCUserCacheManager sharedManager].currentUser;
    
    
    self.payTotal = scheduleModel.price;
    self.totalLbl.text = scheduleModel.total;
    self.payLbl.text = scheduleModel.price;
    
    self.priceLbl1.text = [userModel.integral integerValue] > [scheduleModel.integral integerValue]
    ? scheduleModel.integral
    : userModel.integral;
    
    self.priceLbl2.text  = userModel.currency;
    
    [RACObserve(_scheduleModel, total) subscribeNext:^(id x) {
        self.totalLbl.text = _scheduleModel.total;
        [self integralFldChange:nil];
    }];
    
}


-(void)integralFldChange:(id)sender{
    if ([self.textFld1.text floatValue] > [self.priceLbl1.text floatValue]) {
        self.textFld1.text = self.priceLbl1.text;
    }
    self.integral = self.textFld1.text;
    self.payTotal = [NSString stringWithFormat:@"%.0f",[self.totalLbl.text floatValue] - [self.textFld1.text floatValue] - [self.textFld2.text floatValue]];
    self.payLbl.text = [NSString stringWithFormat:@"%@%@",MoneySign,self.payTotal];
}

-(void)coinFldChange:(id)sender{
    if ([self.textFld2.text floatValue] > [self.priceLbl2.text floatValue]) {
        self.textFld2.text = self.priceLbl2.text;
    }
    self.currency = self.textFld2.text;
    self.payTotal = [NSString stringWithFormat:@"%.0f",[self.totalLbl.text floatValue] - [self.textFld1.text floatValue] - [self.textFld2.text floatValue]];
    self.payLbl.text = [NSString stringWithFormat:@"%@%@",MoneySign,self.payTotal];
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
