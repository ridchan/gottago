//
//  UserIdentifyVC.m
//  yaoqu
//
//  Created by ridchan on 2017/7/8.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserIdentifyVC.h"
#import "UserIdentifyCell.h"

@interface UserIdentifyVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UIView *headerView;

@end

@implementation UserIdentifyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"My Identify");
    [self.tableView registerClass:[UserIdentifyCell class] forCellReuseIdentifier:@"Cell"];
    self.tableView.tableHeaderView = self.headerView;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIView *)headerView{
    if (!_headerView) {
        _headerView = [[UIView alloc]init];
//        _headerView.backgroundColor = [UIColor redColor];
        
        UILabel *label =  [ControllerHelper autoFitLabel];
        label.font = DetailFont;
        label.textColor = AppGray;
        label.textAlignment = NSTextAlignmentCenter;
        label.text = @"- 已拥有 -";
        [_headerView addSubview:label];
        
        label.sd_layout
        .centerXEqualToView(_headerView)
        .centerYEqualToView(_headerView)
        .topSpaceToView(_headerView, 10)
        .heightIs(20)
        .widthIs(200);
        
        UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_user_level_1"]];
        [_headerView addSubview:imageView];
        
        imageView.sd_layout
        .centerXEqualToView(_headerView)
        .centerYEqualToView(_headerView)
        .topSpaceToView(label, 10)
        .heightIs(30)
        .widthIs(30);
        
        
        UILabel *label2 =  [ControllerHelper autoFitLabel];
        label2.font = DetailFont;
        label2.textColor = AppGray;
        label2.numberOfLines = 2;
        label2.textAlignment = NSTextAlignmentCenter;
        label2.text = @"你还未获取身分标识哦\n继续努力吧";
        [_headerView addSubview:label2];
        
        label2.sd_layout
        .centerXEqualToView(_headerView)
        .centerYEqualToView(_headerView)
        .topSpaceToView(imageView, 10)
        .heightIs(50)
        .widthIs(APP_WIDTH);
        
        [_headerView setupAutoHeightWithBottomView:label2 bottomMargin:0];
        [_headerView layoutSubviews];
        
        
    }
    
    return _headerView;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    id model = self.datas[indexPath.section];
    return [tableView cellHeightForIndexPath:indexPath model:nil keyPath:nil cellClass:[UserIdentifyCell class] contentViewWidth:APP_WIDTH];
 
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserIdentifyCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    return cell;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
