//
//  ImageCollectionViewCell.m
//  QBH
//
//  Created by 陳景雲 on 2017/2/18.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "ImageCollectionViewCell.h"

@implementation ImageCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createViews];
    }
    return self;
}

-(void)createViews{
    self.imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.imageView.clipsToBounds = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:self.imageView];
    
    self.delBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.delBtn setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
    self.delBtn.frame = CGRectMake(self.frame.size.width - 30, 0, 30, 30);
    self.delBtn.hidden = YES;
    [self.delBtn addTarget:self action:@selector(delBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.delBtn];
}

-(void)delBtnClick:(id)sender{
    if ([self.delegate respondsToSelector:@selector(deleteCell:)]){
        [self.delegate deleteCell:self];
    }
}

@end
