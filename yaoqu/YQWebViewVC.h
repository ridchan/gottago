//
//  YQWebViewVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseViewController.h"

@interface YQWebViewVC : BaseViewController

@property(nonatomic,strong) NSString *link;


-(void)webViewRefresh;

@end
