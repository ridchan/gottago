//
//  OtherHomePageDynamicView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/24.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "OtherHomePageDynamicView.h"
#import "DynamicModel.h"

@interface OtherHomePageDynamicView()

@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) UIImageView *accessImage;
@property(nonatomic,strong) UIImageView *contentImage;
@property(nonatomic,strong) UILabel *descLbl;


@end

@implementation OtherHomePageDynamicView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self commonInit];
    }
    return self;
}

-(void)setDynamics:(NSArray *)dynamics{
    _dynamics = dynamics;
    if ([_dynamics count] > 0) {
        DynamicModel *model = [_dynamics firstObject];
        self.descLbl.text = model.desc;
        [self.contentImage setImageName:model.image placeholder:nil];
    }
}


-(void)commonInit{
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.mas_top);
        make.height.mas_equalTo(40);
    }];
    
    [self.accessImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.top.equalTo(self.mas_top);
        make.size.mas_equalTo(CGSizeMake(RightAccessWidth, 40));
    }];

    [self.contentImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl.mas_left);
        make.top.equalTo(self.titleLbl.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(120, 120));
        make.bottom.equalTo(self.mas_bottom).offset(-10);
    }];
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentImage.mas_right).offset(10);
        make.right.equalTo(self.mas_right).offset(-10);
        make.top.equalTo(self.contentImage.mas_top);
        make.height.lessThanOrEqualTo(self.contentImage.mas_height);
    }];
}

-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        _titleLbl.text = LS(@"他的动态");
        [self addSubview:_titleLbl];
    }
    return _titleLbl;
}

-(UIImageView *)accessImage{
    if (!_accessImage) {
        _accessImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_forward"]];
        _accessImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_accessImage];
    }
    return _accessImage;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.font = SystemFont(11);
        _descLbl.textColor = AppGray;
        [self addSubview:_descLbl];
    }
    return _descLbl;
}

-(UIImageView *)contentImage{
    if (!_contentImage) {
        _contentImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_forward"]];
        _contentImage.contentMode = UIViewContentModeScaleAspectFill;
        _contentImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:_contentImage];
    }
    return _contentImage;
}

@end
