//
//  DiscoverSearchTravelVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/6.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DiscoverSearchTravelVC.h"
#import "SearchApi.h"
#import "TravelScheduleListCell.h"
#import "TravelScheduleModel.h"
#import "TravelScheduleDetailVC.h"
#import "BaseNavigationController.h"

@interface DiscoverSearchTravelVC ()

@end

@implementation DiscoverSearchTravelVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[TravelScheduleListCell class] forCellReuseIdentifier:@"Cell"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)search:(NSString *)keyword{
    self.paramObj = keyword;
    [self startRefresh];
}

-(void)startRefresh{
    self.page = 1;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:@(self.page) forKey:@"page"];
    [dict setValue:@(self.size) forKey:@"size"];
    [dict setValue:@"1" forKey:@"type"];
    [dict setValue:self.paramObj forKey:@"keyword"];
    
    WEAK_SELF;
    SearchApi *api = [[SearchApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [weakSelf refreshComplete:info response:[TravelScheduleModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"travel_schedule"]]];
    }];
}

-(void)startloadMore{
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:@(++self.page) forKey:@"page"];
    [dict setValue:@(self.size) forKey:@"size"];
    [dict setValue:@"1" forKey:@"type"];
    [dict setValue:self.paramObj forKey:@"keyword"];
    
    WEAK_SELF;
    SearchApi *api = [[SearchApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [weakSelf loadMoreComplete:info response:[TravelScheduleModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"travel_schedule"]]];
    }];
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TravelScheduleDetailVC *vc = [[TravelScheduleDetailVC alloc]init];
    
    vc.model = self.datas[indexPath.section];
    
    BaseNavigationController *nav = [[BaseNavigationController alloc]initWithRootViewController:vc];
    
    [self.navigationController presentViewController:nav animated:YES completion:NULL];
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [tableView cellHeightForIndexPath:indexPath model:self.datas[indexPath.section] keyPath:@"model" cellClass:[TravelScheduleListCell class] contentViewWidth:APP_WIDTH];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.datas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TravelScheduleListCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.model = self.datas[indexPath.section];
    return cell;
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
