//
//  DynamicCommentModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/29.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserModel.h"

@interface DynamicCommentModel : UserModel

@property(nonatomic,strong) NSString *comment_id;
@property(nonatomic,strong) NSString *content;
@property(nonatomic,strong) NSString *comment;
@property(nonatomic,strong) NSMutableArray *comment_data;
@property(nonatomic,strong) NSString *like;
@property(nonatomic) BOOL is_like;

@property(nonatomic) long long time;



@end
