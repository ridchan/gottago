//
//  TravelEnrollDetailVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/15.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelEnrollDetailVC.h"
#import "TravelEnrollDetailHeader.h"
#import "TravelEnrollDetailCell.h"

@interface TravelEnrollDetailVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) TravelEnrollDetailHeader *header;
@property(nonatomic,strong) UIButton *returnBtn;
@property(nonatomic,strong) UIButton *commentBtn;
@property(nonatomic,strong) TravelModel *model;

@end

@implementation TravelEnrollDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"详情");
    self.model = self.paramObj;
    self.isloading = NO;
    self.tableView.tableHeaderView = self.header;
    self.header.model = self.model;
    self.datas = self.model.contact_data;
    
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.left.equalTo(self.view.mas_left);
        make.bottom.equalTo(self.view.mas_bottom).offset(-50);
        make.right.equalTo(self.view.mas_right);
    }];
    
    [self setBottomButton];
    
    // Do any additional setup after loading the view.
}

-(void)setBottomButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"退款" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor whiteColor];
    //    [button addTarget:self action:@selector(cashBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    UIButton *chargeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [chargeBtn setTitle:@"评价" forState:UIControlStateNormal];
    [chargeBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    chargeBtn.backgroundColor = [UIColor whiteColor];
    //    [chargeBtn addTarget:self action:@selector(chargeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:chargeBtn];

    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_centerX).offset(-0.5);
        make.bottom.equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(50);
    }];
    
    [chargeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_centerX).offset(0.5);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(50);
    }];
}

-(TravelEnrollDetailHeader *)header{
    if (!_header) {
        _header = [[TravelEnrollDetailHeader alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 370)];
        
    }
    return _header;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 40)];
    
    UIView *bgView =[[UIView alloc]initWithFrame:CGRectMake(0, 10, APP_WIDTH, 30)];
    bgView.backgroundColor = [UIColor whiteColor];
    [view addSubview:bgView];
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 39.5, APP_WIDTH, 0.5)];
    line.backgroundColor = AppLineColor;
    [view addSubview:line];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"报名信息";
    label.font = SystemFont(12);
    label.frame = CGRectMake(10, 10, APP_WIDTH - 10, 30);
    [view addSubview:label];
    
    return view;

}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 140;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TravelEnrollDetailCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[TravelEnrollDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        
    }
    cell.model = self.datas[indexPath.row];
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
