//
//  UserLocationEditVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/26.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NormalTableViewController.h"

@interface UserLocationEditVC : NormalTableViewController

@property(nonatomic) AddressType addressType;

@end
