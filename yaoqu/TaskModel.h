//
//  TaskModel.h
//  yaoqu
//
//  Created by ridchan on 2017/8/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface RewardModel : BaseModel

@property(nonatomic,strong) NSString *integral;
@property(nonatomic,strong) NSString *exp;
@property(nonatomic,strong) NSString *giftroll;

@end

@interface TaskModel : BaseModel

@property(nonatomic,strong) NSString *id;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) RewardModel *reward;
@property(nonatomic,strong) NSString *unit;
@property(nonatomic,strong) NSString *target;
@property(nonatomic,strong) NSString *surplus;
@property(nonatomic) BOOL is_get;
@property(nonatomic) BOOL state;

@end
