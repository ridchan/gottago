//
//  EasemobServices.m
//  shiyi
//
//  Created by 陳景雲 on 16/5/4.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import "EasemobServices.h"
//#import <HyphenateLite/HyphenateLite.h>
#import "EMClient.h"
#import "LLEmotionModelManager.h"

@implementation EasemobServices

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{

    
    //1148170630178880#gottago
    //ridchan#yaoqu
    
    EMOptions *option = [EMOptions optionsWithAppkey:@"1148170630178880#gottago"];
#ifdef DEBUG
    option.apnsCertName = @"gottago_dev";
#else
    option.apnsCertName = @"gottago_pro";
#endif
    
    [[EMClient sharedClient] initializeSDKWithOptions:option];
    
    
    [[LLEmotionModelManager sharedManager] prepareEmotionModel];
    
    
    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[EMClient sharedClient] applicationDidEnterBackground:application];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[EMClient sharedClient] applicationWillEnterForeground:application];
}

// 将得到的deviceToken传给SDK
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[EMClient sharedClient] bindDeviceToken:deviceToken];
    });
}

// 注册deviceToken失败
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
//    [LLUtils showMessageAlertWithTitle:NSLocalizedString(@"apns.failToRegisterApns", @"Fail to register apns") message:error.description];
    NSLog(@"环信注册失败");
    
}




@end
