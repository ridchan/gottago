//
//  TravelBannerModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface TravelBannerModel : BaseModel

@property(nonatomic,strong) NSString *travel_banner_id;
@property(nonatomic) BOOL state;
@property(nonatomic,strong) NSString *image;
@property(nonatomic,strong) NSString *video;
@property(nonatomic,strong) NSString *type;
@property(nonatomic,strong) NSString *link;
@property(nonatomic) NSInteger sort;
@property(nonatomic) long long time;

@end
