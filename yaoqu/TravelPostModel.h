//
//  TravelPostModel.h
//  yaoqu
//
//  Created by ridchan on 2017/8/2.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface TravelPostModel : BaseModel

@property(nonatomic,strong) NSString *integral;//	使用积分	false	int
@property(nonatomic,strong) NSString *icurrency;//	使用旅游币	false	int
@property(nonatomic,strong) NSString *icontact_id;//	联系人	true	json
@property(nonatomic,strong) NSString *ipay_type;//	支付类型	true	int
@property(nonatomic,strong) NSString *iis_return_total;//	返回 当前合计	false	int
@property(nonatomic,strong) NSString *itravel_schedule_id;//	套餐档期	true	int

@end
