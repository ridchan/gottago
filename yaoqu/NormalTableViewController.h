//
//  NormalTableViewController.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/23.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseViewController.h"
#import "NormalTableViewCell.h"

@class MsgModel;
@interface NormalTableViewController : BaseViewController


@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *datas;
@property(nonatomic) BOOL isloading;

@property(nonatomic) NSInteger page;
@property(nonatomic) NSInteger size;

-(void)startRefresh;
-(void)refreshComplete:(MsgModel *)info response:(id)obj;


-(void)startloadMore;
-(void)loadMoreComplete:(MsgModel *)info response:(id)obj;


@end
