//
//  RegisterVC.m
//  QBH
//
//  Created by 陳景雲 on 2016/12/25.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import "RegisterVC.h"
#import "RegisterViewModel.h"
#import "VerifyCodeVC.h"
#import "TTTAttributedLabel.h"
#import "NormalTextField.h"

@interface RegisterVC ()

@property(nonatomic,strong) NormalTextField *phoneFld;
@property(nonatomic,strong) TTTAttributedLabel *tipLbl;
@property(nonatomic,strong) UIButton *nextBtn;


@end

@implementation RegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createViews];
    // Do any additional setup after loading the view.
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.viewModel = [[RegisterViewModel alloc]init];
    }
    return self;
}

-(void)backBtnClick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)createViews{
    
    self.title = @"注册";
    self.view.backgroundColor = [UIColor whiteColor];
    
    UINavigationBar *navBar = [[UINavigationBar alloc]init];
    navBar.barStyle = UIBarStyleDefault;
//    navBar.tintColor = [UIColor whiteColor];
    [self.view addSubview:navBar];
    
    [navBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.height.mas_equalTo(64);
    }];
//    UINavigationItem *item = [[UINavigationItem alloc]initWithTitle:@"注册"];
//    
//    item.leftBarButtonItem =  [[UIBarButtonItem alloc]initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(backBtnClick:)];
//    [navBar setItems:@[item]];
    
    
    self.phoneFld = [[NormalTextField alloc]init];
    self.phoneFld.title = @"手机号码";
    self.phoneFld.placeholder = @"请输入您的手机号";
    self.phoneFld.keyboardType = UIKeyboardTypeNumberPad;
    
    [self.view addSubview:self.phoneFld];
    
    [self.phoneFld mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(84);
        make.left.equalTo(self.view.mas_left).offset(10);
        make.right.equalTo(self.view.mas_right).offset(-10);
        make.height.mas_equalTo(55);
    }];
    
    
    
    self.nextBtn = [NormalButton normalButton:@"下一步"];
    [self.view addSubview:self.nextBtn];
    
    [self.nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.phoneFld.mas_bottom).offset(20);
        make.left.equalTo(self.view.mas_left).offset(10);
        make.right.equalTo(self.view.mas_right).offset(-10);
        make.height.mas_equalTo(50);
    }];
    
    if (!_isFindPassword) {
        self.tipLbl = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
        self.tipLbl.textAlignment = NSTextAlignmentCenter;
        self.tipLbl.numberOfLines = 2;
        self.tipLbl.linkAttributes = nil;
        self.tipLbl.activeLinkAttributes = nil;
        [self.view addSubview:self.tipLbl];
        
        NSString *tip = @"点击上面按钮'下一步'即表示同意\n《服务协议》《隐私政策》";
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc]initWithString:tip];
        NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc]init];
        paragraph.lineSpacing = 5;
        [att addAttribute:NSParagraphStyleAttributeName value:paragraph range:NSMakeRange(0, [tip length])];
        [att addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, [tip length])];
        [att addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:[tip rangeOfString:@"《服务协议》《隐私政策》"]];
        self.tipLbl.attributedText = att;
        [self.tipLbl addLinkToURL:[NSURL URLWithString:@"《服务协议》"] withRange:[tip rangeOfString:@"《服务协议》"]];
        [self.tipLbl addLinkToURL:[NSURL URLWithString:@"《隐私政策》"] withRange:[tip rangeOfString:@"《隐私政策》"]];
        
        [self.tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.nextBtn.mas_bottom).offset(20);
            make.left.equalTo(self.nextBtn.mas_left);
            make.right.equalTo(self.nextBtn.mas_right);
        }];
    }
    
    
    
    self.viewModel.isFindPassword = self.isFindPassword;
    RAC(self.viewModel,phoneNum) = [self.phoneFld.rac_textSignal distinctUntilChanged];
    RAC(self.nextBtn,enabled) = self.viewModel.btnEnableSignal;
    
    self.viewModel.sendCodeSignal = [self.nextBtn rac_signalForControlEvents:UIControlEventTouchUpInside];

    WEAK_SELF;
    
    [self.viewModel.succSubject subscribeNext:^(id x) {
        VerifyCodeVC *vc = [[VerifyCodeVC alloc]init];
        vc.phone = weakSelf.viewModel.phoneNum;
        vc.viewModel = weakSelf.viewModel;
        vc.viewModel.model.phone = vc.phone;
        [weakSelf.navigationController pushViewController:vc animated:YES];

    }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.phoneFld becomeFirstResponder];
}





-(void)backClick:(id)sender{
    
    if (self.navigationController.navigationController) {
        [self.navigationController.navigationController popViewControllerAnimated:YES];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
