//
//  DynamicCommentApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/29.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface DynamicCommentApi : BaseApi

-(id)initWithDynamic_id:(NSString *)dynamic_id
             comment_id:(NSString *)comment_id
                   desc:(NSString *)desc;

@end
