//
//  SettingServices.m
//  QBH
//
//  Created by 陳景雲 on 2016/12/25.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import "SettingServices.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "CommonConfigManager.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import "LLGDConfig.h"

@implementation SettingServices

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    
    [AMapServices sharedServices].apiKey = (NSString *)APIKey;
    
    [IQKeyboardManager sharedManager].enableDebugging = NO;
    [IQKeyboardManager sharedManager].enable = YES;
    
    [[CommonConfigManager sharedManager] startConfig];
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:AppGray,
                                                           NSFontAttributeName:[UIFont systemFontOfSize:18]}];
    

    
    


    
    // Required
    // notice: 3.0.0及以后版本注册可以这样写，也可以继续 旧的注册 式

    
    NSArray *disableClass =  @[@"DynamicDetailVC",@"DynamicCommentListVC",@"LLChatViewController"];
    for (NSString *classVC in disableClass){
        [self disableInClass:classVC];
    }
    
    
    return YES;
}

-(void)disableInClass:(NSString *)classVC{
    [[IQKeyboardManager sharedManager].disabledToolbarClasses addObject:NSClassFromString(classVC)];
    [[IQKeyboardManager sharedManager].disabledTouchResignedClasses addObject:NSClassFromString(classVC)];
    [[IQKeyboardManager sharedManager].disabledDistanceHandlingClasses addObject:NSClassFromString(classVC)];
}




@end
