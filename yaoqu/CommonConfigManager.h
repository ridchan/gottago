//
//  CommonConfigManager.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProModel.h"
#import "CommonConfigModel.h"

@interface CommonConfigManager : NSObject


@property(nonatomic,strong) HostModel *hostModel;

@property(nonatomic,strong) NSString *api_host;
@property(nonatomic,strong) NSString *tpl_host;
@property(nonatomic,strong) NSString *image_host;
@property(nonatomic,strong) NSString *video_host;

@property(nonatomic,strong) NSArray <ProModel *> *addressArray;

+(instancetype)sharedManager;

-(void)startConfig;

-(void)getCommonConfig;

-(void)getAddresss;

@end
