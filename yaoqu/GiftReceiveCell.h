//
//  GiftReceiveCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/30.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonConfigModel.h"
@interface GiftReceiveCell : UITableViewCell

@property(nonatomic) GiftSearchType type;
@property(nonatomic,strong) GiftModel *model;

@end
