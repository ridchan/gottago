//
//  ImageTextField.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/8.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageTextField : UITextField

+(ImageTextField *)imageText:(NSString *)imageName placeHolder:(NSString *)placeHolder;

@end
