//
//  OuthApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/15.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "OuthApi.h"

@interface OuthApi ()

@end

@implementation OuthApi

-(id)initWithOpenID:(NSString *)open_id unionidID:(NSString *)union_id{
    if (self = [super init]) {
        [self.params setValue:open_id forKey:@"openid"];
        [self.params setValue:union_id forKey:@"unionid"];
    }
    return self;
}

-(NSString *)requestUrl{
    return AuthUrl;
}



@end
