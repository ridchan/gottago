//
//  UserModel.h
//  yaoqu
//
//  Created by ridchan on 2017/6/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@class UserInfoModel;

@interface UserModel : BaseModel

@property(nonatomic,strong) NSString *autoid;
@property(nonatomic,strong) NSString *huanXinID;

@property(nonatomic,strong) NSString *phone;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *sex;
@property(nonatomic,strong) NSString *qq_openid;
@property(nonatomic,strong) NSString *mp_openid;
@property(nonatomic,strong) NSString *openid;
@property(nonatomic,strong) NSString *uniondid;
@property(nonatomic,strong) NSString *code;
@property(nonatomic,strong) NSString *image;


@property(nonatomic,strong) NSString *member_id;
@property(nonatomic,strong) NSString *access_token;
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *age;
@property(nonatomic,strong) NSArray *images;
@property(nonatomic,strong) NSString *constellation;
@property(nonatomic) BOOL is_age;
@property(nonatomic,strong) NSString *desc;

@property(nonatomic,strong) NSString *currency;
@property(nonatomic,strong) NSString *integral;
@property(nonatomic,strong) NSString *money;
@property(nonatomic,strong) NSString *giftroll;

@property(nonatomic,strong) NSString *birthday;
@property(nonatomic,strong) NSString *background;

@property(nonatomic,strong) NSString *pro_id;
@property(nonatomic,strong) NSString *pro_name;
@property(nonatomic,strong) NSString *city_id;
@property(nonatomic,strong) NSString *city_name;
@property(nonatomic,strong) NSString *home_pro_id;
@property(nonatomic,strong) NSString *home_pro_name;
@property(nonatomic,strong) NSString *home_city_id;
@property(nonatomic,strong) NSString *home_city_name;

@property(nonatomic,strong) NSString *exp;
@property(nonatomic,strong) NSString *level;


@property(nonatomic) BOOL is_partner;
@property(nonatomic,strong) NSString *sex_orientation;
@property(nonatomic,strong) NSString *make_friends;
@property(nonatomic) NSInteger is_approve;
@property(nonatomic) BOOL is_show_from;
@property(nonatomic) BOOL is_show_location;
@property(nonatomic) long long reg_time;

@property(nonatomic,strong) UserInfoModel *info;


@property(nonatomic) BOOL is_member;
@property(nonatomic) BOOL is_talent;
@property(nonatomic) BOOL is_gold;
@property(nonatomic) BOOL is_follow;
@property(nonatomic) BOOL is_push;
@property(nonatomic) BOOL is_push_chat;
@property(nonatomic) BOOL is_push_dynamic;
@property(nonatomic) BOOL is_push_system;
@property(nonatomic) BOOL is_push_invite;
@property(nonatomic) BOOL is_push_stranger;


@end


@interface UserInfoModel : BaseModel

@property(nonatomic,strong) NSString *order;
@property(nonatomic,strong) NSString *partner;
@property(nonatomic,strong) NSString *follow;
@property(nonatomic,strong) NSString *fans;
@property(nonatomic,strong) NSString *dynamic;

@end
