//
//  GaoDeLocationVC.m
//  QBH
//
//  Created by 陳景雲 on 2017/3/2.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "GaoDeLocationVC.h"
#import <AMapSearchKit/AMapSearchKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import "LLGDConfig.h"
#import "SearchResultVC.h"

@interface GaoDeLocationVC ()<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,AMapSearchDelegate,CLLocationManagerDelegate,UISearchControllerDelegate,UISearchResultsUpdating>

@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) UISearchBar *searchBar;
@property(nonatomic,strong) UISearchController *searchViewController;


@property(nonatomic,strong) SearchResultVC *resultVC;

@property(nonatomic,strong) NSMutableArray *dataSource;

@property(nonatomic,strong) AMapSearchAPI *searchAPI;
@property (nonatomic) AMapPOIAroundSearchRequest *request;
@property (nonatomic) AMapReGeocodeSearchRequest *regeo;
@property(nonatomic,strong) CLLocationManager *locationManager;
@property(nonatomic,strong) CLLocation *location;

@property(nonatomic,strong) NSString *currentCity;


@end

@implementation GaoDeLocationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self createViews];
    
    //在ViewDidLoad里面如下代码
//    self.definesPresentationContext = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.resultVC = [[SearchResultVC alloc]init];
    
    self.searchViewController = [[UISearchController alloc]initWithSearchResultsController:self.resultVC];
    self.searchViewController.active = NO;
    self.searchViewController.dimsBackgroundDuringPresentation = YES;
    self.searchViewController.hidesNavigationBarDuringPresentation = YES;
    [self.searchViewController.searchBar sizeToFit];
    self.searchViewController.searchBar.delegate = self;
//    self.searchViewController.searchBar.barTintColor = AppGray;
    //设置显示搜索结果的控制器
    self.searchViewController.searchResultsUpdater = self; //协议(UISearchResultsUpdating)
    self.searchViewController.delegate = self;
    
    self.tableView.tableHeaderView = self.searchViewController.searchBar;
    self.searchViewController.searchBar.keyboardType = UIKeyboardAppearanceDefault;
    self.searchViewController.searchBar.placeholder = @"请输入城市关键字";
    
    // Do any additional setup after loading the view.
}

-(void)willPresentSearchController:(UISearchController *)searchController{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.35];
     self.tableView.contentInset = UIEdgeInsetsMake(-44, 0, 0, 0);
    [UIView commitAnimations];
    
}



-(void)willDismissSearchController:(UISearchController *)searchController{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.35];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.tableView.contentOffset = CGPointMake(0, 0);
    [UIView commitAnimations];
    
  

}

-(void)viewDidLayoutSubviews {
    if(self.searchViewController.active) {
        [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
            make.top.equalTo(self.view.mas_top).offset(44);
            make.bottom.equalTo(self.view.mas_bottom);
        }];
    }else {
        [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
            make.top.equalTo(self.view.mas_top).offset(64);
            make.bottom.equalTo(self.view.mas_bottom);
        }];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)cancelBtnClick:(id)sender{
//    [self dismissViewControllerAnimated:YES completion:NULL];
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}

-(void)createViews{
    
    self.title = @"所在位置";
    
    self.navigationItem.leftBarButtonItem = [self ittemLeftItemWithIcon:nil title:LS(@"取消") selector:@selector(cancelBtnClick:)];
//    [self setLeftItemWithIcon:nil title:LS(@"Cancel") selector:@selector(cancelBtnClick:)];
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top).offset(64);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    self.dataSource = [NSMutableArray array];
    
    [self setupLocation];
}

-(void)setupLocation{
    
    _locationManager = [[CLLocationManager alloc]init];
    
    if ([CLLocationManager locationServicesEnabled]) {
        //IOS8以及以上版本需要设置，弹出是否允许使用定位提示
        if ([[UIDevice currentDevice].systemVersion floatValue] > 8.0) {
            [_locationManager requestWhenInUseAuthorization];
        }
        _locationManager.delegate = self;
        
        [_locationManager startUpdatingLocation];
    }
    
    
    
    
    _request = [[AMapPOIAroundSearchRequest alloc] init];
    _request.types = (NSString *)allPOISearchTypes;
    _request.sortrule = 1;
    _request.requireExtension = YES;
    _request.requireSubPOIs = NO;
    _request.radius = 5000;
    _request.page = 1;
    _request.offset = 20;
    
//    _regeo = [[AMapReGeocodeSearchRequest alloc] init];
//    _regeo.radius = 3000;
//    _regeo.requireExtension = NO;
    
    
    self.searchAPI = [[AMapSearchAPI alloc]init];
    self.searchAPI.delegate = self;

}



-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    self.request.page = 1;
    self.request.keywords = searchBar.text;
    [self.searchAPI AMapPOIAroundSearch:self.request];
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    self.searchAPI.delegate = nil;
    self.searchAPI = nil;
    _locationManager.delegate = self;
    _locationManager = nil;
}


#pragma mark -

-(void)addressLoadMore{
    self.request.page ++;
    [self.searchAPI AMapPOIAroundSearch:self.request];
}

-(void)setTableViewFooter{
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(addressLoadMore)];
}


-(void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response{
    
    if (request.page == 1) {
        [self.dataSource removeAllObjects];
        if (response.pois.count == request.offset){
            [self setTableViewFooter];
        }else{
            self.tableView.mj_footer = nil;
        }

    }else{
        if (response.pois.count == request.offset){
            [self.tableView.mj_footer endRefreshing];
        }else{
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        
    }
    

    for (AMapPOI *p in response.pois){
        [self.dataSource addObject:p];
    }
    [self.tableView reloadData];
    
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    if (!self.location) {
        self.location = [locations firstObject];
        
        WEAK_SELF;
        CLGeocoder *geocoder = [[CLGeocoder alloc]init];
        [geocoder reverseGeocodeLocation:_location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
            
            for (CLPlacemark *placemark in placemarks){
                weakSelf.currentCity = placemark.locality;
                
                [weakSelf.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
        }];

        
        
        self.request.location = [AMapGeoPoint locationWithLatitude:self.location.coordinate.latitude longitude:self.location.coordinate.longitude];
        self.request.page = 1;
        
        [self.searchAPI AMapPOIAroundSearch:self.request];
    }
}


#pragma mark -
#pragma mark search bar



- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
//    NSLog(@"search text %@",searchController.searchBar.text);
    [self.resultVC searchWithText:searchController.searchBar.text];
}




#pragma mark -
#pragma mark  tableview

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self.delegate respondsToSelector:@selector(didSelectAddress:city:)]) {
        if (indexPath.row == 0){
            [self.delegate didSelectAddress:nil city:nil];
        }else if (indexPath.row == 1) {
            AMapPOI *poi = [[AMapPOI alloc]init];
            poi.uid = self.currentCity;
            poi.location = [AMapGeoPoint locationWithLatitude:self.location.coordinate.latitude longitude:self.location.coordinate.longitude];
            [self.delegate didSelectAddress:poi city:self.currentCity];
        }else{
            AMapPOI *poi = [self.dataSource objectAtIndex:indexPath.row - 2];
            [self.delegate didSelectAddress:poi city:self.currentCity];
        }
        
    }
    
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataSource.count + 2;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    }
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    if (indexPath.row == 0){
        cell.textLabel.text = @"不显示";
        cell.textLabel.textColor = [UIColor blueColor];
        cell.detailTextLabel.text = nil;
    }else if (indexPath.row == 1){
        cell.textLabel.text = self.currentCity;
        cell.detailTextLabel.text = nil;
        cell.textLabel.textColor = [UIColor blackColor];
        if ([self.selectPOI.uid isEqualToString:self.currentCity]){
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }

    }else{
        AMapPOI *poi = [self.dataSource objectAtIndex:indexPath.row - 2];
        cell.textLabel.text = poi.name;
        cell.detailTextLabel.text = poi.address;
        cell.textLabel.textColor = [UIColor blackColor];
        if ([self.selectPOI.uid isEqualToString:poi.uid]){
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    
    return cell;

    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
