//
//  UserSexEditVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/22.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NormalTableViewController.h"
#import "CurrentUserModel.h"

@interface UserSexEditVC : NormalTableViewController

@property(nonatomic,strong) UserModel *editModel;

@end
