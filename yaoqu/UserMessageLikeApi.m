//
//  UserMessageLikeApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserMessageLikeApi.h"

@implementation UserMessageLikeApi


-(id)initWithPage:(NSInteger)page sie:(NSInteger)size{
    if (self = [super init]) {
        [self.params setValue:@(page) forKey:@"page"];
        [self.params setValue:@(size) forKey:@"size"];
    }
    return self;
}

-(id)initWithMessageLikeID:(NSString *)message_like_id{
    if (self = [super init]) {
        [self.params setValue:message_like_id forKey:@"message_like_id"];
    }
    return self;
}

-(NSString *)requestUrl{
    switch (self.apiType) {
        case APIType_List:
            return MessageCenterLikeListUrl;
        case APIType_Delete:
            return MessageCenterLikeDelUrl;
        case APIType_Clear:
            return MessageCenterLikeClearUrl;
        default:
            break;
    }
    return nil;
    
}

@end
