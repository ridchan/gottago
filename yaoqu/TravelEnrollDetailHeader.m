//
//  TravelEnrollDetailHeader.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/15.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelEnrollDetailHeader.h"

@interface TravelEnrollDetailHeader()


@property(nonatomic,strong) UIView *headerView;

@property(nonatomic,strong) UIImageView *contentImage;
@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) UILabel *statuLbl;
@property(nonatomic,strong) ImageButton *localLbl;
@property(nonatomic,strong) ImageButton *dateLbl;

@property(nonatomic,strong) ImageButton *marchBtn;

@property(nonatomic,strong) UIView *orderView;
@property(nonatomic,strong) ImageButton *depositLbl;
@property(nonatomic,strong) ImageButton *coinLbl;
@property(nonatomic,strong) UILabel *payLbl;
@property(nonatomic,strong) UILabel *totalLbl;

@end

@implementation TravelEnrollDetailHeader


- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.mas_top);
//        make.height.mas_equalTo(220);
    }];
    
    [self.contentImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.headerView.mas_left);
        make.top.equalTo(self.headerView.mas_top);
        make.right.equalTo(self.headerView.mas_right);
        make.height.mas_equalTo(160);
    }];
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.headerView.mas_left).offset(10);
        make.right.equalTo(self.headerView.mas_right).offset(-120);
        make.top.equalTo(self.contentImage.mas_bottom).offset(5);
    }];
    
    [self.localLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.headerView.mas_left).offset(10);
        make.top.equalTo(self.titleLbl.mas_bottom).offset(20);
        make.height.mas_equalTo(20);
        make.bottom.equalTo(self.headerView.mas_bottom).offset(-5);//?????
    }];
    
    [self.dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.localLbl.mas_right).offset(5);
        make.top.equalTo(self.localLbl.mas_top);
        make.height.mas_equalTo(20);
    }];
    
    
    [self.statuLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.headerView.mas_right).offset(-10);
        make.top.equalTo(self.titleLbl.mas_top);
        make.size.mas_equalTo(CGSizeMake(50, 20));
    }];
    
    [self.marchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.headerView.mas_right).offset(-10);
        make.bottom.equalTo(self.localLbl.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(80, 25));
    }];
    
    ///

    [self.orderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerView.mas_bottom).offset(10);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.headerView.mas_right);
//        make.height.mas_equalTo(150);
    }];
    
    [self.totalLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.orderView.mas_right).offset(-10);
        make.height.mas_equalTo(20);
        make.top.equalTo(self.orderView.mas_top).offset(10);
    }];
    
    [self.depositLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.orderView.mas_right).offset(-10);
        make.height.mas_equalTo(20);
        make.top.equalTo(self.totalLbl.mas_bottom).offset(10);
    }];
    
    [self.coinLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.orderView.mas_right).offset(-10);
        make.height.mas_equalTo(20);
        make.top.equalTo(self.depositLbl.mas_bottom).offset(10);
    }];
    
    [self.payLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.orderView.mas_right).offset(-10);
        make.height.mas_equalTo(20);
        make.top.equalTo(self.coinLbl.mas_bottom).offset(10);
        make.bottom.equalTo(self.orderView.mas_bottom).offset(-10);
    }];
    
    [[self labelWithTitle:@"订单总额"] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.orderView.mas_left).offset(10);
        make.height.mas_equalTo(20);
        make.centerY.equalTo(self.totalLbl.mas_centerY);
    }];
    
    [[self labelWithTitle:@"使用积分"] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.orderView.mas_left).offset(10);
        make.height.mas_equalTo(20);
        make.centerY.equalTo(self.depositLbl.mas_centerY);
    }];
    
    [[self labelWithTitle:@"使用旅游币"] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.orderView.mas_left).offset(10);
        make.height.mas_equalTo(20);
        make.centerY.equalTo(self.coinLbl.mas_centerY);
    }];
    
    [[self labelWithTitle:@"支付现金"] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.orderView.mas_left).offset(10);
        make.height.mas_equalTo(20);
        make.centerY.equalTo(self.payLbl.mas_centerY);
    }];

}

-(UIView *)headerView{
    if (!_headerView) {
        _headerView = [[UIView alloc]init];
        _headerView.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:_headerView];
    }
    return _headerView;
}

-(UIImageView *)contentImage{
    if (!_contentImage) {
        _contentImage = [[UIImageView alloc]init];
        _contentImage.clipsToBounds = YES;
        _contentImage.contentMode = UIViewContentModeScaleAspectFill;
        _contentImage.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self.headerView addSubview:_contentImage];
    }
    return _contentImage;
}


-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        [self.headerView addSubview:_titleLbl];
    }
    return _titleLbl;
}

-(ImageButton *)localLbl{
    if (!_localLbl) {
        _localLbl = [[ImageButton alloc]init];
        _localLbl.contentImage.image = [UIImage imageNamed:@"ic_location_gray"];
        _localLbl.titleLbl.text = @"位置";
        [self.headerView addSubview:_localLbl];
    }
    return _localLbl;
}

-(ImageButton *)dateLbl{
    if (!_dateLbl) {
        _dateLbl = [[ImageButton alloc]init];
        _dateLbl.contentImage.image = [UIImage imageNamed:@"ic_travel_icon"];
        _dateLbl.titleLbl.text = @"日期";
        [self.headerView addSubview:_dateLbl];
    }
    return _dateLbl;
}

-(UILabel *)statuLbl{
    if (!_statuLbl) {
        _statuLbl = [ControllerHelper autoFitLabel];
        _statuLbl.textColor = [UIColor orangeColor];
        _statuLbl.text = LS(@"未开始");
        _statuLbl.font = SystemFont(11);
        _statuLbl.layer.borderColor = [UIColor orangeColor].CGColor;
        _statuLbl.layer.borderWidth = 1.0;
        _statuLbl.layer.masksToBounds = YES;
        _statuLbl.layer.cornerRadius = 3;
        _statuLbl.textAlignment = NSTextAlignmentCenter;
        [self.headerView addSubview:_statuLbl];
    }
    return _statuLbl;
}

-(ImageButton *)marchBtn{
    if (!_marchBtn) {
        _marchBtn = [[ImageButton alloc]init];
        _marchBtn.contentImage.image = [[UIImage imageNamed:@"ic_dynamic_comment"] imageToColor:[UIColor whiteColor]];
        _marchBtn.titleLbl.text = LS(@"行程主页");
        _marchBtn.titleLbl.textColor = [UIColor whiteColor];
        _marchBtn.backgroundColor = [UIColor orangeColor];
        _marchBtn.layer.cornerRadius = 3;
        _marchBtn.layer.masksToBounds = YES;
        [self.headerView addSubview:_marchBtn];
    }
    return _marchBtn;
}

#pragma mark -


-(UIView *)orderView{
    if (!_orderView) {
        _orderView = [[UIView alloc]init];
        _orderView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_orderView];
    }
    return _orderView;
}

-(UILabel *)totalLbl{
    if (!_totalLbl) {
        _totalLbl = [ControllerHelper autoFitLabel];
        _totalLbl.text = @"abcde";
        [self.orderView addSubview:_totalLbl];
    }
    return _totalLbl;
}



-(ImageButton *)depositLbl{
    if (!_depositLbl) {
        _depositLbl = [[ImageButton alloc]init];
        _depositLbl.contentImage.image = [UIImage imageNamed:@"ic_score"];
        _depositLbl.titleLbl.text = @"150 积分";
        [self.orderView addSubview:_depositLbl];
    }
    return _depositLbl;
}

-(ImageButton *)coinLbl{
    if (!_coinLbl) {
        _coinLbl = [[ImageButton alloc]init];
        _coinLbl.contentImage.image = [UIImage imageNamed:@"ic_coin"];
        _coinLbl.titleLbl.text = @"150 旅游币";
        [self.orderView addSubview:_coinLbl];
    }
    return _coinLbl;
}


-(UILabel *)payLbl{
    if (!_payLbl) {
        _payLbl = [ControllerHelper autoFitLabel];
        _payLbl.font = SystemFont(11);
        _payLbl.text = @"实付:1923";
        _payLbl.textColor = [UIColor orangeColor];
        [self.orderView addSubview:_payLbl];
    }
    return _payLbl;
}

-(UILabel *)labelWithTitle:(NSString *)title{
    UILabel *label = [ControllerHelper autoFitLabel];
    label.text = title;
    label.font = SystemFont(14);
    
    [self.orderView addSubview:label];
    
    return label;
}


-(void)setModel:(TravelModel *)model{
    _model = model;
    [self.contentImage setImageName:model.image placeholder:nil];
    self.titleLbl.text = model.title;
    self.localLbl.titleLbl.text = model.address;
    self.dateLbl.titleLbl.text = model.date;
    self.totalLbl.text = model.total;
    self.depositLbl.titleLbl.text = [NSString stringWithFormat:@"%@积分",model.integral];
    self.coinLbl.titleLbl.text = [NSString stringWithFormat:@"%@旅游币",model.currency];
    self.payLbl.text = [NSString stringWithFormat:@"实付:%@%@",MoneySign,model.pay_total];
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
