//
//  MineHomeCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/28.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MineCollectionCell.h"

@protocol MineHomeCellDelegate <NSObject>

@optional
-(void)homeCellDidSelectAtIndex:(NSInteger)index;

@end


@interface MineHomeCell : UITableViewCell

@property(nonatomic,weak) id<MineHomeCellDelegate> delegate;
@property(nonatomic,strong) NSArray *datas;

-(MineCollectionCell *)detailCellAtIndex:(NSInteger)index;



@end
