//
//  TravelContactListVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/12.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NormalTableViewController.h"

@protocol TravelContactListVCDelegate <NSObject>

@optional
-(void)didSelectContact:(id)obj;

@end

@interface TravelContactListVC : NormalTableViewController

@property(nonatomic) id<TravelContactListVCDelegate>delegate;

@end
