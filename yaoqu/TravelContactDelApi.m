//
//  TravelContactDelApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelContactDelApi.h"

@implementation TravelContactDelApi


-(id)initWithContactID:(NSString *)contact_id{
    if (self = [super init]) {
        [self.params setValue:contact_id forKey:@"contact_id"];
    }
    return self;
}

-(NSString *)requestUrl{
    return TravelContactDelUrl;
}



@end
