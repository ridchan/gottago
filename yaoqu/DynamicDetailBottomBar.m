//
//  DynamicDetailBottomBar.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/4.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicDetailBottomBar.h"


@interface DynamicDetailBottomBar()

@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UIImageView *commentImage;
@property(nonatomic,strong) UILabel *countLbl;
@property(nonatomic,strong) UIImageView *giftImage;

@end

@implementation DynamicDetailBottomBar



- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self commonInit];
        
        [RACObserve(self, model.comment) subscribeNext:^(id x) {
            if ([x integerValue] > 0) {
                if ([x integerValue] < 1000){
                    self.countLbl.text = x;
                }else{
                    self.countLbl.text = [NSString stringWithFormat:@"%0.0fk+",floor([x integerValue] / 1000.0)];
                }
            }else{
                self.countLbl.text = @"0";
            }
        }];
    }
    return self;
}

-(void)commonInit{
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    [self.giftImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-20);
        make.top.equalTo(self.mas_top).offset(15);
        make.bottom.equalTo(self.mas_bottom).offset(-15);
        make.width.equalTo(self.giftImage.mas_height);
    }];
    
    [self.commentImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.giftImage.mas_left).offset(-40);
        make.top.equalTo(self.mas_top).offset(15);
        make.bottom.equalTo(self.mas_bottom).offset(-15);
        make.width.equalTo(self.giftImage.mas_height);
    }];
    
    
    [self.countLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.commentImage.mas_right).offset(3);
        make.centerY.equalTo(self.commentImage.mas_centerY);
//        make.top.equalTo(self.mas_top).offset(10);
//        make.right.equalTo(self.giftImage.mas_left);
//        make.height.mas_equalTo(20);
    }];
    
    [[self lineInColor:RGB16(0xe8e8e8)] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.mas_top);
        make.height.mas_equalTo(0.5);
    }];

}


-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.textColor = AppGray;
        _descLbl.font = SystemFont(14);
        _descLbl.text = @"送礼物更能表达心意哟~";
        _descLbl.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textTap:)];
        [_descLbl addGestureRecognizer:tap];
        [self addSubview:_descLbl];
    }
    return _descLbl;
}

-(UILabel *)countLbl{
    if (!_countLbl) {
        _countLbl = [ControllerHelper autoFitLabel];
        _countLbl.font = SystemFont(14);
        _countLbl.text = @"";
        _countLbl.textColor = [UIColor blackColor];
        [self addSubview:_countLbl];
    }
    return _countLbl;
}

-(UIImageView *)commentImage{
    if (!_commentImage) {
        _commentImage = [[UIImageView alloc]init];
        _commentImage.userInteractionEnabled = YES;
        _commentImage.contentMode = UIViewContentModeScaleAspectFit;
        _commentImage.image = [[UIImage imageNamed:@"ic_dynamic_comment"] imageToColor:RGB16(0xa3a3a3)];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(commentTap:)];
        [_commentImage addGestureRecognizer:tap];
        [self addSubview:_commentImage];
    }
    return _commentImage;
}

-(UIImageView *)giftImage{
    if (!_giftImage) {
        _giftImage = [[UIImageView alloc]init];
        _giftImage.contentMode = UIViewContentModeScaleAspectFit;
        _giftImage.image = [[UIImage imageNamed:@"ic_gift"] imageToColor:RGB16(0xa3a3a3)];
        _giftImage.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(giftTap:)];
        [_giftImage addGestureRecognizer:tap];
        
        [self addSubview:_giftImage];
    }
    return _giftImage;
}



-(void)textTap:(id)sender{
    if ([self.delegate respondsToSelector:@selector(bottomBarTextClick:)]) {
        [self.delegate bottomBarTextClick:nil];
    }
}

-(void)giftTap:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(bottomBarGiftClick:)]) {
        [self.delegate bottomBarGiftClick:nil];
    }
}

-(void)commentTap:(id)sender{
    if ([self.delegate respondsToSelector:@selector(bottomBarCommentClick:)]) {
        [self.delegate bottomBarCommentClick:nil];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
