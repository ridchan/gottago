//
//  NavTitleView.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavTitleView : UIView

@property(nonatomic,strong) NSArray *titles;

@property(nonatomic) CGFloat contentOffset;

@property(nonatomic,copy) BaseBlock selectBlock;

@end
