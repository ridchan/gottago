//
//  QuestionHelpModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/18.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface QuestionHelpModel : BaseModel

@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *desc;
@property(nonatomic) BOOL isExpand;

@end
