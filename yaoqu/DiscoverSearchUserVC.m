//
//  DiscoverSearchUserVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/6.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DiscoverSearchUserVC.h"
#import "SearchApi.h"
#import "DiscoverSearchUserNormalCell.h"
@interface DiscoverSearchUserVC ()

@end

@implementation DiscoverSearchUserVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[DiscoverSearchUserNormalCell class] forCellReuseIdentifier:@"Cell"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)search:(NSString *)keyword{
    self.paramObj = keyword;
    [self startRefresh];
}

-(void)startRefresh{
    self.page = 1;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:@(self.page) forKey:@"page"];
    [dict setValue:@(self.size) forKey:@"size"];
    [dict setValue:@"0" forKey:@"type"];
    [dict setValue:self.paramObj forKey:@"keyword"];
    
    WEAK_SELF;
    SearchApi *api = [[SearchApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [weakSelf refreshComplete:info response:[UserModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"member"]]];

    }];
}

-(void)startloadMore{
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:@(++self.page) forKey:@"page"];
    [dict setValue:@(self.size) forKey:@"size"];
    [dict setValue:@"0" forKey:@"type"];
    [dict setValue:self.paramObj forKey:@"keyword"];
    
    WEAK_SELF;
    SearchApi *api = [[SearchApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [weakSelf loadMoreComplete:info response:[UserModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"member"]]];
    }];

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return .001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  self.datas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DiscoverSearchUserNormalCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.userModel = self.datas[indexPath.section];
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
