//
//  PhoneCodeApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/16.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface PhoneCodeApi : BaseApi

-(id)initWithPhone:(NSString *)phone;

@end
