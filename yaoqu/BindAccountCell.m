//
//  BindAccountCell.m
//  yaoqu
//
//  Created by ridchan on 2017/8/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BindAccountCell.h"

#import "LoginApi.h"
#import "MemberApi.h"
#import "LLUtils.h"
@interface BindAccountCell()

@end

@implementation BindAccountCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    [self.iconImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    }];
    
    [self.titlLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.iconImage.mas_right).offset(5);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    [self.bindBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(60, 25));
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(UIImageView *)iconImage{
    if (!_iconImage) {
        _iconImage = [[UIImageView alloc]init];
        _iconImage.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_iconImage];
    }
    return _iconImage;
}

-(UILabel *)titlLbl{
    if (!_titlLbl) {
        _titlLbl = [ControllerHelper autoFitLabel];
        _titlLbl.font = SystemFont(16);
        [self.contentView addSubview:_titlLbl];
    }
    return _titlLbl;
}

-(UIButton *)bindBtn{
    if (!_bindBtn) {
        _bindBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _bindBtn.layer.borderColor = [UIColor orangeColor].CGColor;
        _bindBtn.layer.borderWidth = 1.0;
        _bindBtn.layer.cornerRadius = 3.0;
        [_bindBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        [_bindBtn setTitle:LS(@"绑定") forState:UIControlStateNormal];
        [_bindBtn addTarget:self action:@selector(bindBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        _bindBtn.titleLabel.font = SystemFont(12);
        [self.contentView addSubview:_bindBtn];
    }
    return _bindBtn;
}

-(void)setIsBindAccount:(BOOL)isBindAccount{
    _isBindAccount = isBindAccount;
    if (_isBindAccount) {
        _bindBtn.layer.borderColor = AppGray.CGColor;
        [_bindBtn setTitleColor:AppGray forState:UIControlStateNormal];
        [_bindBtn setTitle:LS(@"已绑定") forState:UIControlStateNormal];
    }else{
        _bindBtn.layer.borderColor = [UIColor orangeColor].CGColor;
        [_bindBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        [_bindBtn setTitle:LS(@"绑定") forState:UIControlStateNormal];
    }
}


-(void)bindBtnClick:(id)sender{
    if (self.isBindAccount) return;
    WEAK_SELF;
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:self.type currentViewController:[[RouteController sharedManager] currentNav] completion:^(id result, NSError *error) {
        if (!error) {
            [weakSelf userAouth:result];
        }else{
            [LLUtils showCenterTextHUD:error.description];
        }
        
    }];

}


-(void)userAouth:(UMSocialUserInfoResponse *)response{
    
    //    [MBProgressHUD showProgressWithText:@"验证中..."];
    
    WEAK_SELF;
    NSDictionary *dic = nil;// @{@"type":@"wechat",@"openid":@"1B6D68D1E8E5277585392BD7559CC9DA"};
    
    if (response.platformType == UMSocialPlatformType_QQ) {
        dic = @{@"type":@"qq",@"qq_openid":response.openid,@"unionid":response.uid};
    }else{
        dic = @{@"type":@"wechat",@"openid":response.openid,@"mp_openid":response.openid,@"unionid":response.uid};
    }
    
    
    [[[LoginApi alloc]initWitObject:dic] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        
        
        if (info.error != ErrorCodeType_None){
            MemberApi *api = [[MemberApi alloc]initWitObject:dic];
            api.method = YTKRequestMethodPOST;
            [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                if (info.error == ErrorCodeType_None)
                    weakSelf.isBindAccount = YES;
                else
                    [LLUtils showTextHUD:info.message];
            }];
            
        }else{
            [LLUtils showTextHUD:@"该帐号已注册,请绑定其他帐号"];
        }
    }];
    
    
}


@end
