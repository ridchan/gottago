//
//  MineCodeVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/18.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineCodeVC.h"
#import "ShareView.h"
#import "SexAgeView.h"
#import "RCUserCacheManager.h"
#import "CommonConfigManager.h"
@interface MineCodeVC ()

@property(nonatomic,strong) UIImageView *headerImage;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) UILabel *numLbl;
@property(nonatomic,strong) SexAgeView *sexView;


@property(nonatomic,strong) UIImageView *codeImage;
@property(nonatomic,strong) UILabel *tipLbl;
@property(nonatomic,strong) UIButton *saveBtn;
@property(nonatomic,strong) UIImageView *centerImage;

@property(nonatomic,strong) UIView *headerView;
@property(nonatomic,strong) UIView *codeView;
@property(nonatomic,strong) MBProgressHUD *hud;


@end

@implementation MineCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createViews];
    [self setRightItemWithIcon:[UIImage imageNamed:@"ic_share"] selector:@selector(shareBtnClick:)];
    [self wr_setNavBarShadowImageHidden:NO];
    
    [RACObserve([RCUserCacheManager sharedManager], currentUser) subscribeNext:^(UserModel *x) {
//        [self.headerImage setImageName:x.image placeholder:nil];
//        self.nameLbl.text = x.username;
//        self.sexView.sex = [x.sex integerValue];
//        self.sexView.age = [x.age integerValue];
//        self.sexView.content = nil;
        [self.centerImage setImageName:x.image placeholder:nil];
        
        
        
        NSString *link = [NSString stringWithFormat:@"homepage.html?member_id=%@",x.member_id];
        link = [[CommonConfigManager sharedManager].hostModel.tpl_host stringByAppendingPathComponent:link];

        self.codeImage.image = [self createCoreImage:link];
    }];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *)headerView1{
    if (self.headerView) return self.headerView;
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor whiteColor];
    
    self.headerImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"yaoqu_icon"]];
    self.headerImage.contentMode = UIViewContentModeScaleAspectFill;
    self.headerImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.headerImage.layer.cornerRadius = 30;
    self.headerImage.layer.masksToBounds = YES;
    [view addSubview:self.headerImage];
    
    self.nameLbl = [ControllerHelper autoFitLabel];
    self.nameLbl.font = SystemFont(14);
    self.nameLbl.text = @"成双成对Gottogo";
    [view addSubview:self.nameLbl];
    
//    self.sexView = [[SexAgeView alloc]init];
//    [view addSubview:_sexView];
    
    [self.headerImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(view.mas_centerY).offset(-10);
        make.centerX.equalTo(view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(60, 60));
    }];
    
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.headerImage.mas_centerX);
        make.top.equalTo(self.headerImage.mas_bottom).offset(5);
    }];
    
//    [self.sexView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.headerImage.mas_right).offset(10);
//        make.bottom.equalTo(self.headerImage.mas_bottom).offset(-5);
//        make.height.mas_equalTo(12);
//    }];
    

    self.headerView = view;
    [self.view addSubview:view];
    
    return view;
}

-(UIView *)codeView1{
    
    if (self.codeView) return self.codeView;
    
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor whiteColor];
    
    self.codeImage = [[UIImageView alloc]init];
    [view addSubview:self.codeImage];
    
    [self.codeImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(view).insets(UIEdgeInsetsMake(1, 1, 1, 1));
    }];
    
    CGFloat width = APP_WIDTH / 5.0;
//    width  = 70;
    self.centerImage = [[UIImageView alloc]init];
    self.centerImage.contentMode = UIViewContentModeScaleAspectFill;
    self.centerImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.centerImage.layer.cornerRadius = width / 2.0;
    self.centerImage.layer.masksToBounds = YES;
    self.centerImage.layer.borderColor = [UIColor whiteColor].CGColor;
    self.centerImage.layer.borderWidth = 3.0;
    [self.codeImage addSubview:self.centerImage];
    
    
    [self.centerImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.codeImage.mas_centerX);
        make.centerY.equalTo(self.codeImage.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(width, width));
    }];

    
    [self.view addSubview:view];
    self.codeView = view;
    
    return view;
}


-(void)createViews{
    self.title = @"我的二维码";
    
    [[self headerView1] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(10);
        make.right.equalTo(self.view.mas_right).offset(-10);
        make.top.equalTo(self.view.mas_top).offset(74);
        make.height.mas_equalTo(120);
    }];
    
    CGFloat width = APP_WIDTH / 5.0 * 3.0 + 20;
    [[self codeView1] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerView.mas_bottom).offset(20);
        make.centerX.equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(width, width));
    }];
    
    
    
    self.tipLbl = [[UILabel alloc]init];
    self.tipLbl.numberOfLines = 0;
    self.tipLbl.textAlignment = NSTextAlignmentCenter;
    //    self.tipLbl.text = @"分享我的二维码\n就可以邀请更多好友加入企博汇";
    self.tipLbl.font = [UIFont systemFontOfSize:12];
    self.tipLbl.textColor = RGB16(0x636365);
    
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc]initWithString:@"扫一扫上面的二维码图案，加我为好友"];
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc]init];
    paragraph.alignment = NSTextAlignmentCenter;
    paragraph.lineSpacing = 21;
    
    [att addAttribute:NSParagraphStyleAttributeName value:paragraph range:NSMakeRange(0, [att.string length])];
    
    self.tipLbl.attributedText = att;
    
    [self.view addSubview:self.tipLbl];
    
    self.saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.saveBtn.backgroundColor = RGB16(0xff9e00);
    [self.saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.saveBtn setTitle:@"保存二维码手机" forState:UIControlStateNormal];
    
    [self.saveBtn addTarget:self action:@selector(saveBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.saveBtn];
    
    
    
    
    [self.tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.codeView.mas_bottom).offset(20);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
    }];
    
    
    [self.saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.bottom.equalTo(self.view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(APP_WIDTH, 50));
    }];
    
    
    
//    WS(weakSelf);
//    [RACObserve([UserInfoManager manger], currentUser) subscribeNext:^(UserInfoModel *userInfo) {
//        [weakSelf.headerImage setImageLink:userInfo.image placeholderImage:UserDefaultImage];
//        weakSelf.nameLbl.text = userInfo.username;
//        weakSelf.numLbl.text = [NSString stringWithFormat:@"企博号 : %@",userInfo.number];
//        //        NSString *link = [NSString stringWithFormat:@"http://m.qibohao.com/n/%@",userInfo.number];
//        weakSelf.codeImage.image = [weakSelf createCoreImage:[UserInfoManager manger].qrCodelink];
//    }];
}

#pragma mark -

-(void)shareBtnClick:(id)sender{
    
    UserModel *model = [RCUserCacheManager sharedManager].currentUser;
    
    NSString *link = [NSString stringWithFormat:@"reg.html?type=user&member_id=%@",model.member_id];
    link = [[CommonConfigManager sharedManager].tpl_host stringByAppendingString:link];
    
    ShareView *shareView = [[ShareView alloc]init];
    
//    UserInfoModel *_model = [UserInfoManager manger].currentUser;
    
    
    
    //    NSString *link = [NSString stringWithFormat:@"http://m.qibohao.com/n/%@",_model.number];
    
    __block NSMutableDictionary *shareInfo = [NSMutableDictionary dictionary];
//    [shareInfo setValue:[NSString stringWithFormat:@"%@邀请你加入企博汇",_model.username] forKey:@"title"];
    
    [shareInfo setValue:[RCUserCacheManager sharedManager].currentUser.username forKey:@"title"];
    [shareInfo setValue:[RCUserCacheManager sharedManager].currentUser.desc forKey:@"desc"];
    [shareInfo setValue:self.headerImage.image forKey:@"imgUrl"];
    [shareInfo setValue:link forKey:@"link"];
    [shareInfo setValue:model.image forKey:@"image"];
    
    shareView.shareInfo = shareInfo;
    
    WEAK_SELF;
    shareView.handleBlock = ^(id Obj){
        if ([Obj integerValue] == ShareType_Copy){
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = link;
            [LLUtils showActionSuccessHUD:@"已复制"];
        }else{
            
        }
        
    };
    
    [shareView show];
    
}

-(void)saveBtnClick:(id)sender{
    self.hud = [LLUtils showActivityIndicatiorHUDWithTitle:@"保存中"];
    UIImageWriteToSavedPhotosAlbum([self.codeView snapshotImage], self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    
    [self.hud hideAnimated:NO];
    if(!error){
        [LLUtils showActionSuccessHUD:@"保存成功"];
    }else{
        [LLUtils showCenterTextHUD:@"保存失败"];
    }
}

#pragma mark - 生成二维码
- (UIImage *)createCoreImage:(NSString *)codeStr{
    
    //1.生成coreImage框架中的滤镜来生产二维码
    CIFilter *filter=[CIFilter filterWithName:@"CIQRCodeGenerator"];
    [filter setDefaults];
    
    [filter setValue:[codeStr dataUsingEncoding:NSUTF8StringEncoding] forKey:@"inputMessage"];
    [filter setValue:@"H" forKey: @"inputCorrectionLevel"];
    //4.获取生成的图片
    CIImage *ciImg=filter.outputImage;
    //放大ciImg,默认生产的图片很小
    
    //5.设置二维码的前景色和背景颜色
    CIFilter *colorFilter=[CIFilter filterWithName:@"CIFalseColor"];
    //5.1设置默认值
    [colorFilter setDefaults];
    [colorFilter setValue:ciImg forKey:@"inputImage"];
    [colorFilter setValue:[CIColor colorWithRed:0 green:0 blue:0] forKey:@"inputColor0"];
    [colorFilter setValue:[CIColor colorWithRed:1 green:1 blue:1] forKey:@"inputColor1"];
    //5.3获取生存的图片
    ciImg=colorFilter.outputImage;
    
    CGAffineTransform scale=CGAffineTransformMakeScale(10, 10);
    ciImg=[ciImg imageByApplyingTransform:scale];
    
    //    self.imgView.image=[UIImage imageWithCIImage:ciImg];
    
    //6.在中心增加一张图片
    UIImage *img=[UIImage imageWithCIImage:ciImg];
    //7.生存图片
    //7.1开启图形上下文
    UIGraphicsBeginImageContext(img.size);
    //7.2将二维码的图片画入
    //BSXPCMessage received error for message: Connection interrupted   why??
    //    [img drawInRect:CGRectMake(10, 10, img.size.width-20, img.size.height-20)];
    [img drawInRect:CGRectMake(0, 0, img.size.width, img.size.height)];
    //7.3在中心划入其他图片
    
//    UIImage *centerImg=[UIImage imageNamed:@"ic_user_bg"];
//    
//    CGFloat centerW=70;
//    CGFloat centerH=70;
//    CGFloat centerX=(img.size.width-70)*0.5;
//    CGFloat centerY=(img.size.height -70)*0.5;
//    
//    [centerImg drawInRect:CGRectMake(centerX, centerY, centerW, centerH)];
    
    //7.4获取绘制好的图片
    UIImage *finalImg=UIGraphicsGetImageFromCurrentImageContext();
    
    //7.5关闭图像上下文
    UIGraphicsEndImageContext();
    
    return finalImg;
    
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
