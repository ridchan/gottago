//
//  VerifyCodeVC.m
//  QBH
//
//  Created by 陳景雲 on 2016/12/26.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import "VerifyCodeVC.h"
#import "TTTAttributedLabel.h"


@interface VerifyCodeVC ()<WLUnitFieldDelegate>

@property(nonatomic,strong) UILabel *tipLbl;
@property(nonatomic,strong) WLUnitField *unitField;
@property(nonatomic,strong) UIButton *nextBtn;
@property(nonatomic,strong) TTTAttributedLabel *resendLbl;


@end

@implementation VerifyCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createViews];
    // Do any additional setup after loading the view.
}
-(void)backBtnClick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)createViews{
    
    
    self.title = @"注册";
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UINavigationBar *navBar = [[UINavigationBar alloc]init];
    navBar.barStyle = UIBarStyleDefault;
//    navBar.tintColor = [UIColor whiteColor];
    [self.view addSubview:navBar];
    
    [navBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.height.mas_equalTo(64);
    }];

//    UINavigationItem *item = [[UINavigationItem alloc]initWithTitle:@"注册"];
//    
//    item.leftBarButtonItem =  [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(backBtnClick:)];
//    [navBar setItems:@[item]];
    
    
    self.tipLbl = [[UILabel alloc]init];
    self.tipLbl.textColor = [UIColor lightGrayColor];
    NSString *tip = [NSString stringWithFormat:@"验证码已经发送到手机 : %@",self.phone];
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc]initWithString:tip];
    [att addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:[tip rangeOfString:self.phone]];
    self.tipLbl.attributedText = att;
    self.tipLbl.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:self.tipLbl];
    
    [self.tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(10);
        make.top.equalTo(navBar.mas_bottom).offset(10);
        make.right.equalTo(self.view.mas_right).offset(10);
        make.height.mas_equalTo(60);
    }];
    
    self.unitField = [[WLUnitField alloc] initWithInputUnitCount:4];
    self.unitField.unitSpace = 12;
    self.unitField.borderRadius = 4;
    self.unitField.delegate = self;
    
    [self.unitField sizeToFit];
    [self.view addSubview:self.unitField];
    
    [self.unitField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tipLbl.mas_bottom).offset(10);
        make.left.equalTo(self.view.mas_left).offset(20);
        make.right.equalTo(self.view.mas_right).offset(-20);
        make.height.mas_equalTo(60);
    }];
    
    [self.unitField becomeFirstResponder];
    
    
    
    self.nextBtn = [NormalButton normalButton:@"下一步"];
    self.nextBtn.enabled = NO;
    [self.nextBtn addTarget:self action:@selector(nextBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.nextBtn];
    
    [self.nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.unitField.mas_bottom).offset(20);
        make.left.equalTo(self.view.mas_left).offset(10);
        make.right.equalTo(self.view.mas_right).offset(-10);
        make.height.mas_equalTo(50);
    }];
    
    
    self.resendLbl = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
    self.resendLbl.textAlignment = NSTextAlignmentCenter;
    self.resendLbl.numberOfLines = 1;
    self.resendLbl.linkAttributes = nil;
    self.resendLbl.activeLinkAttributes = nil;
    [self.view addSubview:self.resendLbl];
    
    NSString *sendtip = @"收不到验证码？重新发送验证码";
    
    NSMutableAttributedString *attSend = [[NSMutableAttributedString alloc]initWithString:sendtip];
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc]init];
    paragraph.lineSpacing = 5;
    [attSend addAttribute:NSParagraphStyleAttributeName value:paragraph range:NSMakeRange(0, [sendtip length])];
    [attSend addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, [sendtip length])];
    [attSend addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:[sendtip rangeOfString:@"重新发送验证码"]];
    self.resendLbl.attributedText = attSend;
    [self.resendLbl addLinkToURL:[NSURL URLWithString:@"重新发送验证码"] withRange:[tip rangeOfString:@"重新发送验证码"]];

    [self.resendLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nextBtn.mas_bottom).offset(20);
        make.left.equalTo(self.nextBtn.mas_left);
        make.right.equalTo(self.nextBtn.mas_right);
    }];
    

}

-(BOOL)unitField:(WLUnitField *)uniField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([uniField.text length] == 3 && [string length] > 0) {
        self.nextBtn.enabled = YES;
    }else{
        self.nextBtn.enabled = NO;
    }
    
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

-(void)nextBtnClick:(id)sender{
    self.viewModel.model.code = self.unitField.text;
    [self.viewModel.registSubject subscribeNext:^(id x) {
        [[RouteController sharedManager] openMainController];
    }];
    [self.viewModel regist];
//    [[RouteController sharedManager] openMainController];
}

-(void)backClick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
