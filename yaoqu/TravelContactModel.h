//
//  TravelContactModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/12.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface TravelContactModel : BaseModel

//param : { contact_id : int }  //联系人编号( 选传 )
//param : { is_default : int }  //默认( 选传 默认 为0 )
//param : { fullname : String }  //姓名( * )
//param : { sex : int }  //性别( * )
//param : { phone : String }  //手机号码( * )
//param : { card_type : int }  //证件类型( * )
//param : { card_num : String }  //证件号码( * )
//param : { birthday : String }  //出生日期( *  YYYY/MM/DD )
//param : { email : Stirng }  //邮箱( * )

@property(nonatomic,strong) NSString *contact_id;
@property(nonatomic) BOOL is_default;
@property(nonatomic,strong) NSString *fullname;
@property(nonatomic,strong) NSString *sex;
@property(nonatomic,strong) NSString *phone;
@property(nonatomic,strong) NSString *card_type;
@property(nonatomic,strong) NSString *card_num;
@property(nonatomic,strong) NSString *birthday;
@property(nonatomic,strong) NSString *email;
@property(nonatomic,strong) NSString *age;



@end
