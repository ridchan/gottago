//
//  UploadCollectionCell.h
//  yaoqu
//
//  Created by ridchan on 2017/7/5.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadObject.h"

@interface UploadCollectionCell : UICollectionViewCell


@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UIImageView *playImage;
@property(nonatomic,strong) UploadObject *uploadObj;

@end
