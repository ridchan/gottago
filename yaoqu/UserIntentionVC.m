//
//  UserIntentionVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/7.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserIntentionVC.h"
#import "CommonConfigManager.h"
@interface UserIntentionVC ()

@property(nonatomic,strong) NSString *makefriends;

@end

@implementation UserIntentionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.editModel = self.paramObj;
    
    
    self.title = LS(@"交友意向");
    self.datas = [[CommonConfigManager sharedManager].hostModel.sex_orientation mutableCopy];
    self.makefriends = self.editModel.make_friends;
    [self setRightItemWithTitle:LS(@"保存") selector:@selector(saveBtnClick:)];
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NormalTableViewCell *cell = [NormalTableViewCell cellWithType:NormalCellType_Checked identifier:@"Cell" tableView:tableView];
    cell.type = indexPath.row == 0 ? NormalCellType_None : NormalCellType_TopLine;
    cell.textLabel.text = self.datas[indexPath.item];
    cell.bChecked = [self.makefriends isEqualToString:cell.textLabel.text];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.makefriends = self.datas[indexPath.item];
    [self.tableView reloadData];
}

-(void)saveBtnClick:(id)sender{
//    self.editModel.sex = self.sex;
    self.editModel.make_friends = self.makefriends;
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
