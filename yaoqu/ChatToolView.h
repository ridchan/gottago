//
//  ChatToolView.h
//  shiyi
//
//  Created by 陳景雲 on 16/6/24.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark --

#pragma mark cell


@interface ChatToolCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UILabel *label;

@end

#pragma mark --

#pragma mark view



@interface ChatToolView : UIView<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) NSArray *items;

@end
