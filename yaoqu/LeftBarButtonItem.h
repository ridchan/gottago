//
//  LeftBarButtonItem.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftBarButtonItem : UIBarButtonItem


@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UIButton *btn;


-(id)initWithIcon:(UIImage *)icon title:(NSString *)title;

-(void)setTitleColor:(UIColor *)color;
-(void)setImageColor:(UIColor *)color;

@end
