//
//  NormalCellLayout.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NormalCellLayout.h"

@implementation NormalCellLayout

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.numOfRow = 1;
        self.numOfColumn = 1;
        self.minimumLineSpacing = 0;
        self.minimumInteritemSpacing = 0;
    }
    return self;
}

-(CGSize)itemSize{
    CGSize size = self.collectionView.frame.size;
    CGFloat width = ((size.width - self.sectionInset.left - self.sectionInset.right) - self.minimumLineSpacing * (self.numOfColumn - 1)) / self.numOfColumn;
    CGFloat height = self.cellHeight;
    if (height == 0)
        height = ((size.height - self.sectionInset.top - self.sectionInset.bottom) - self.minimumInteritemSpacing * (self.numOfRow - 1)) / self.numOfRow;
    return CGSizeMake(width, height);
}

@end
