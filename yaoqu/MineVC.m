//
//  MineVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/16.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineVC.h"
#import "UserHeaderView.h"
#import "UserEditVC.h"
#import "NormalTableViewCell.h"
#import "MineCollectionCell.h"
#import "MineHomeCell.h"
#import "YQWebViewVC.h"
#import "CommonConfigManager.h"
#import "RCUserCacheManager.h"

#define NAVBAR_COLORCHANGE_POINT (IMAGE_HEIGHT - NAV_HEIGHT*2)
#define IMAGE_HEIGHT 220
#define NAV_HEIGHT 64

@interface MineVC ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,UIScrollViewDelegate>

@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) NSArray *datas;

@property(nonatomic,strong) RightBarButtonItem *settingItem;
@property(nonatomic,strong) RightBarButtonItem *codeItem;
@property(nonatomic,strong) LeftBarButtonItem *messageItem;



@end

@implementation MineVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self commonInit];
    
    NSString *orderLink = [[CommonConfigManager sharedManager].api_host stringByAppendingPathComponent:@"order.html"];
    NSString *walletLink = [[CommonConfigManager sharedManager].api_host stringByAppendingPathComponent:@"wallet.html"];
    NSString *rewardLink = [[CommonConfigManager sharedManager].api_host stringByAppendingPathComponent:@"reward.html"];
    self.datas = @[@[@{@"name":LS(@"我的钱包"),@"img":@"ic_my_wallet",@"vc":@"YQWebViewVC",@"param":walletLink},
                     @{@"name":LS(@"我的订单"),@"img":@"ic_my_order",@"vc":@"YQWebViewVC",@"param":orderLink},
                     @{@"name":LS(@"我的动态"),@"img":@"ic_my_dynamic",@"vc":@"DynamicListVC"},
                     @{@"name":LS(@"我的收藏"),@"img":@"ic_my_collect",@"vc":@"MineCollectionVC"},
                     @{@"name":LS(@"我的奖励"),@"img":@"ic_my_reward",@"vc":@"YQWebViewVC",@"param":rewardLink},
                     @{@"name":LS(@"我的礼物"),@"img":@"ic_my_gift",@"vc":@"MineGiftVC"},
                     @{@"name":LS(@"会员中心"),@"img":@"ic_my_member_center",@"vc":@"MemberCenterVC"},
                     @{@"name":LS(@"我的任务"),@"img":@"ic_my_reward",@"vc":@"MineTaskVC"},
                    ]
                   ];
    
    
    RACSignal *singal1 = RACObserve([RCUserCacheManager sharedManager], currentBubble);
    RACSignal *singal2 = RACObserve([RCUserCacheManager sharedManager], lastBubble);
    
    [[RACSignal combineLatest:@[singal1,singal2] reduce:^id(BubbleModel *currentBubble,BubbleModel *lastBubble){
        
        NSLog(@"当前%@\n之前%@",[currentBubble keyValues],[lastBubble keyValues]);
        BOOL b1 = [currentBubble.message_comment integerValue] > [lastBubble.message_comment integerValue];
        BOOL b2 = [currentBubble.message_system integerValue] > [lastBubble.message_system integerValue];
        BOOL b3 = [currentBubble.message_like integerValue] > [lastBubble.message_like integerValue];
        BOOL b4 = [currentBubble.gift_send integerValue] > [lastBubble.gift_send integerValue];
        BOOL b5 = [currentBubble.preview integerValue] > [lastBubble.preview integerValue];
        
        return @(b1 | b2 | b3 | b4 | b5);
    }] subscribeNext:^(id x) {
        [((LeftBarButtonItem *)self.navigationItem.leftBarButtonItem).imageView setBubble:[x stringValue] offset:CGSizeMake(0, 2)];
    }];
    
//  @{@"name":LS(@"我的游记"),@"img":@"ic_my_log"}
//  @{@"name":LS(@"New 约"),@"img":@"ic_my_new_yue",@"vc":@"TravelContactListVC"},
    [self wr_setNavBarBackgroundAlpha:0];

    self.navigationItem.title = @"";
    // Do any additional setup after loading the view.
}

-(void)viewFirstAppear{
    [self.codeItem setImageColor:[UIColor whiteColor]];
    [self.settingItem setImageColor:[UIColor whiteColor]];
    [self.messageItem setImageColor:[UIColor whiteColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)openMineCode{
    [[RouteController sharedManager] openClassVC:@"MineCodeVC"];
}

-(void)openSetting{
    [[RouteController sharedManager] openSetting];
}

-(void)openMessageCenter{
    [[RouteController sharedManager]openClassVC:@"MessageCenterVC"];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.y <= -64) {
        scrollView.contentOffset = CGPointMake(0, -64);
        return;
    }
    
    if (scrollView.contentOffset.y < 64) {
        [self wr_setNavBarBackgroundAlpha:scrollView.contentOffset.y / 64];
        [self wr_setStatusBarStyle:UIStatusBarStyleDefault];
        
        [self.messageItem setImageColor:[UIColor whiteColor]];
        [self.codeItem setImageColor:[UIColor whiteColor]];
        [self.settingItem setImageColor:[UIColor whiteColor]];
    }else{
        
        [self wr_setNavBarBackgroundAlpha:1];
        [self wr_setNavBarTintColor:[UIColor whiteColor]];
        [self wr_setNavBarTitleColor:[UIColor whiteColor]];
        [self wr_setStatusBarStyle:UIStatusBarStyleLightContent];
        
        [self.messageItem setImageColor:AppGray];
        [self.codeItem setImageColor:AppGray];
        [self.settingItem setImageColor:AppGray];
    }
    
}

-(void)commonInit{
    
//    [self setLeftItemWithIcon:[UIImage imageNamed:@"ic_chat_message"] title:nil selector:@selector(openMessageCenter)];
    
    self.messageItem = (LeftBarButtonItem *)[self ittemLeftItemWithIcon:[UIImage imageNamed:@"ic_chat_message"] title:nil selector:@selector(openMessageCenter)];
    
    
    self.settingItem = [RightBarButtonItem itemRightItemWithIcon:[UIImage imageNamed:@"ic_user_setting"]];
    [self.settingItem.btn addTarget:self action:@selector(openSetting) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *centerItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    centerItem.width = 15;
    
    self.codeItem = [RightBarButtonItem itemRightItemWithIcon:[UIImage imageNamed:@"ic_user_code"]];
    [self.codeItem.btn addTarget:self action:@selector(openMineCode) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItems = @[self.settingItem,centerItem,self.codeItem];
    self.navigationItem.leftBarButtonItem = self.messageItem;
    
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
        make.top.equalTo(self.view.mas_top).offset(-64);
    }];
    
    UserHeaderView *headerView = [[UserHeaderView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 350)];
    
    self.tableView.tableHeaderView = headerView;
    

    
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

-(UICollectionViewFlowLayout *)layout{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(APP_WIDTH / 3.0 - 2, APP_WIDTH / 3.0 - 2);
    layout.minimumLineSpacing = 1.0;
    layout.minimumInteritemSpacing = 1.0;
//    layout.headerReferenceSize = CGSizeMake(APP_WIDTH, 280);
    return layout;
}







#pragma mark -
#pragma mark tableview delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.datas.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *arr =  self.datas[indexPath.section];
    CGFloat row = ceilf(arr.count  / 3.0);
    CGFloat height  = (APP_WIDTH - 1.0 ) / 3;
    return height * row + (row - 1) * 0.5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0001;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MineHomeCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[MineHomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.datas = self.datas[indexPath.section];
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
