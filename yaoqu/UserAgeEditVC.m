//
//  UserAgeEditVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/28.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserAgeEditVC.h"
#import "UserModel.h"

@interface UserAgeEditVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UIDatePicker *picker;
@property(nonatomic,strong) UserModel *editModel;
@property(nonatomic,strong) NSArray *bottomTips;

@property(nonatomic,strong) NSString *age;
@property(nonatomic,strong) NSString *constellation;
@property(nonatomic,strong) NSString *is_age;

@end

@implementation UserAgeEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"年龄");
    
    self.editModel = self.paramObj;
    
    self.age = self.editModel.age;
    self.constellation = self.editModel.constellation;
    self.is_age = [[NSNumber numberWithBool:self.editModel.is_age] stringValue];
    
    
    
    [self setRightItemWithTitle:LS(@"保存") selector:@selector(saveBtnClick:)];
    
    self.picker = [[UIDatePicker alloc]init];
    self.picker.datePickerMode = UIDatePickerModeDate;
    self.picker.maximumDate = [NSDate date];
    
    [self.picker addTarget:self action:@selector(valueChange:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.picker];
    [self.picker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top);
        make.bottom.equalTo(self.picker.mas_top);
    }];
    
    self.tableView.scrollEnabled = NO;
    
    self.datas = [@[@[LS(@"年龄"),LS(@"星座")],
                   @[LS(@"保密")]
                   ] mutableCopy];
    
    
    self.bottomTips = @[@"输入您的出生日期，系统会自动算出您的年龄和星座。您的个人页面中，只会显示年龄和星座，不会显示具体的出生日期",
                        @"开启密保后，此项资料不会出到在您的个人页面中"
                        ];
    // Do any additional setup after loading the view.
}

-(void)saveBtnClick:(id)sender{
    self.editModel.age = self.age;
    self.editModel.constellation = self.constellation;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)valueChange:(id)obj{
    [self ageWithDateOfBirth:self.picker.date];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)ageWithDateOfBirth:(NSDate *)date;
{
    
    
    // 出生日期转换 年月日
    NSDateComponents *components1 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    NSInteger brithDateYear  = [components1 year];
    NSInteger brithDateDay   = [components1 day];
    NSInteger brithDateMonth = [components1 month];
    
    // 获取系统当前 年月日
    NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger currentDateYear  = [components2 year];
    NSInteger currentDateDay   = [components2 day];
    NSInteger currentDateMonth = [components2 month];
    
    // 计算年龄
    NSInteger iAge = currentDateYear - brithDateYear - 1;
    if ((currentDateMonth > brithDateMonth) || (currentDateMonth == brithDateMonth && currentDateDay >= brithDateDay)) {
        iAge++;
    }
    
    self.age = [NSString intValue:iAge];
    self.constellation = [self getConstellationWithMonth:brithDateMonth day:brithDateDay];
    NSLog(@"星座：%@",self.constellation);
    return iAge;
}

- (NSString *)getConstellationWithMonth:(NSInteger)m_ day:(NSInteger)d_
{
    NSString * astroString = @"摩羯座水瓶座双鱼座白羊座金牛座双子座巨蟹座狮子座处女座天秤座天蝎座射手座摩羯座";
    NSString * astroFormat = @"102123444543";
    NSString * result;
    
    result = [NSString stringWithFormat:@"%@",[astroString substringWithRange:NSMakeRange(m_*3-(d_ < [[astroFormat substringWithRange:NSMakeRange((m_-1), 1)] intValue] - (-19))*3, 3)]];
    
    return result;
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [self bottomView1:self.bottomTips[section]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return [self.bottomTips[section] heightWithFont:SystemFont(11) inWidth:APP_WIDTH - 60] + 10;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return section == 0 ? 2 : 1;
}






-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0){
        NormalTableViewCell *cell =  [NormalTableViewCell cellWithType:NormalCellType_Detail identifier:@"Cell" tableView:tableView];
        cell.textLabel.text =  self.datas[indexPath.section][indexPath.row];
        cell.type = indexPath.row == 0 ? NormalCellType_BottomLine : NormalCellType_None;
        [cell rac_prepareForReuseSignal];
        if (indexPath.row == 0){
            [RACObserve(self,age) subscribeNext:^(id x) {
                cell.detailLbl.text = [NSString stringWithFormat:@"%@岁",x];
            }];
        }else if (indexPath.row == 1){
            [RACObserve(self, constellation) subscribeNext:^(id x) {
                cell.detailLbl.text = x;
            }];
        }
        
        return cell;
    }else{
        NormalTableViewCell *cell =  [NormalTableViewCell cellWithType:NormalCellType_Switch identifier:@"Cell2" tableView:tableView];
        cell.textLabel.text =  self.datas[indexPath.section][indexPath.row];
        [RACObserve(self,is_age) subscribeNext:^(id x) {
            cell.switchBtn.on = [x boolValue];
        }];
        
        
        return cell;
    }
}


-(UIView *)bottomView1:(NSString *)title{
    UIView *view = [[UIView alloc]init];
    
    UILabel *label =  [ControllerHelper autoFitLabel];
    label.textColor = AppGray;
    label.font = SystemFont(11);
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 0;
    label.text = title;
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left).offset(30);
        make.right.equalTo(view.mas_right).offset(-30);
        make.top.equalTo(view.mas_top).offset(5);
        make.bottom.equalTo(view.mas_bottom).offset(-5);
    }];
    
    return view;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
