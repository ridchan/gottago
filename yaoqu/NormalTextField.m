//
//  NormalTextField.m
//  QBH
//
//  Created by 陳景雲 on 2016/12/25.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import "NormalTextField.h"

@implementation NormalTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (instancetype)init
{
    self = [super init];
    if (self) {
        [self createView];
        self.leftViewMode = UITextFieldViewModeAlways;
    }
    return self;
}

//-(void)change:(id)sender{
//    self.title = self.textField.text;
//}

-(void)createView{
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.text = @"标题";
    self.titleLabel.textColor = RGB16(0x333333);
    [self.titleLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
//    [self addSubview:self.titleLabel];
    self.titleLabel.frame = CGRectMake(0, 0, 100, 30);
    self.leftView = self.titleLabel;
    

    self.textColor = RGB16(0xc1c1c1);
    
    
    
    self.centerLine = [[UIView alloc]init];
    self.centerLine.backgroundColor = RGB16(0xe6e6e6);
    [self addSubview:self.centerLine];
    
    self.bottomLine = [[UIView alloc]init];
    self.bottomLine.backgroundColor = RGB16(0xe6e6e6);
    [self addSubview:self.bottomLine];

    
    [self.centerLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).mas_offset(80);
        make.width.mas_equalTo(0.5);
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(35.0);
    }];
    
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.height.mas_equalTo(0.5);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom);
    }];
    
    
}

#pragma mark -

//-(void)setText:(NSString *)text{
//    self.textField.text = text;
//}
//
//-(NSString *)text{
//    return self.textField.text;
//}

-(void)setTitle:(NSString *)title{
    self.titleLabel.text = title;
}

-(NSString *)title{
    return self.titleLabel.text;
}

//-(void)setPlaceholder:(NSString *)placeholder{
//    self.textField.placeholder = placeholder;
//}
//
//-(NSString *)placeholder{
//    return self.textField.placeholder;
//}

@end
