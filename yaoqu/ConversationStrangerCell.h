//
//  ConversationStrangerCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/9.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ConversationStrangerCell : UITableViewCell

@property(nonatomic,strong) NSArray *conversationModels;

- (void)markAllMessageAsRead;

@end
