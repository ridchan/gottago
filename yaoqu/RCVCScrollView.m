//
//  RCVCScrollView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/24.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "RCVCScrollView.h"
#import "BaseViewController.h"

@interface RCVCScrollView()<UIScrollViewDelegate>

@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) NSMutableDictionary *vcs;

@end

@implementation RCVCScrollView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.vcs = [NSMutableDictionary dictionary];
        [self commonInit];
        
    }
    return self;
}

-(void)commonInit{
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

-(UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]init];
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.bounces = NO;
        [self addSubview:_scrollView];
    }
    return _scrollView;
}

-(void)setVcNames:(NSArray *)vcNames{
    _vcNames = vcNames;
}

-(void)layoutSubviews{
    self.scrollView.contentSize = CGSizeMake(self.frame.size.width * self.vcNames.count, 1);
    [self addVC:0];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    int idx = (int)ceilf(scrollView.contentOffset.x / scrollView.frame.size.width);
    [self addVC:idx];
}


-(void)addVC:(NSInteger )idx{
    if (idx < 0 | idx >= [_vcNames count]) return;
    
    
    
    NSString *name = _vcNames[idx];
    
    if ([self.vcs objectForKey:@(idx)]) return;
    
    [self.vcs setObject:name forKey:@(idx)];

    BaseViewController *vc = [[NSClassFromString(name) alloc] init];
    if (self.vcParams)
        vc.paramObj = self.vcParams[idx];
    [[self ex_viewController] addChildViewController:vc];
    [self.scrollView addSubview:vc.view];
    [vc.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView.mas_left).offset(self.frame.size.width * idx);
        make.top.equalTo(self.scrollView.mas_top);
        make.width.mas_equalTo(self.frame.size.width);
        make.height.mas_equalTo(self.frame.size.height);
    }];
    
    NSLog(@"scroll vc : %@  frame : %@",[[self ex_viewController] class],NSStringFromCGRect(self.frame));

}

-(void)setSelectIndex:(NSInteger)selectIndex{
    _selectIndex = selectIndex;

    CGSize size = self.bounds.size;
    [self.scrollView scrollRectToVisible:CGRectMake(size.width * _selectIndex, 0, size.width, size.height) animated:NO];
    
    [self addVC:_selectIndex];

}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"did scroll ");
    self.contentOffsetX = scrollView.contentOffset.x / scrollView.frame.size.width;
    if (self.selectBlock){
        self.selectBlock(@(self.contentOffsetX));
    };
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
