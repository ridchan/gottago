//
//  DynamicDetailVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/28.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NormalTableViewController.h"
#import "DynamicModel.h"

@interface DynamicDetailVC : NormalTableViewController


@property(nonatomic,strong) DynamicModel *model;
@property(nonatomic) BOOL isShowComment;

@end
