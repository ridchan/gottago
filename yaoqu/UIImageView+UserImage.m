//
//  UIImageView+UserImage.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UIImageView+UserImage.h"
#import "UIImageView+WebCache.h"
#import "CommonConfigManager.h"

@implementation UIImageView (UserImage)

-(void)setImageName:(NSString *)name placeholder:(UIImage *)placeholder{
    NSString *link = nil;
    if (![name hasPrefix:@"http"]) {
        link = [[CommonConfigManager sharedManager].image_host stringByAppendingPathComponent:name];
    }else{
        link = name;
    }
    
    [self sd_setImageWithURL:[NSURL URLWithString:link] placeholderImage:placeholder];
    
}

@end
