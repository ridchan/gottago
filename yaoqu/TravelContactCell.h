//
//  TravelContactCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/12.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TravelContactModel.h"

@interface TravelContactCell : UITableViewCell


@property(nonatomic,strong) TravelContactModel *model;

@end
