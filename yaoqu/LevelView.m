//
//  LevelView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/16.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "LevelView.h"

@implementation LevelView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}


-(void)setLevel:(NSString *)level{
    _level = level;
    self.levelLbl.text = _level ? _level : @"1";
}

-(void)commonInit{
    [self.levelImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [self.levelLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
        make.left.equalTo(self.mas_centerX);
    }];
    
}

-(UIImageView *)levelImg{
    
    if (!_levelImg) {
        _levelImg = [[UIImageView alloc]init];
        _levelImg.contentMode = UIViewContentModeScaleAspectFit;
        _levelImg.image = [UIImage imageNamed:@"ic_level"];
        [self addSubview:_levelImg];
    }
    return _levelImg;
}

-(UILabel *)levelLbl{
    if (!_levelLbl) {
        _levelLbl = [[UILabel alloc]init];
        _levelLbl.minimumScaleFactor = 0.5;
        _levelLbl.textAlignment = NSTextAlignmentCenter;
        _levelLbl.textColor = [UIColor whiteColor];
        _levelLbl.font = [UIFont systemFontOfSize:9];
        _levelLbl.text = @"15";
        [self addSubview:_levelLbl];
    }
    return _levelLbl;

}

@end
