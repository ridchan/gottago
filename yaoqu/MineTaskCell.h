//
//  MineTaskCell.h
//  yaoqu
//
//  Created by ridchan on 2017/8/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TaskModel.h"

@interface MineTaskCell : UITableViewCell

@property(nonatomic,strong) TaskModel *model;

@end
