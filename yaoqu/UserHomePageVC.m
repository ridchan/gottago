//
//  UserHomePageVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/27.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserHomePageVC.h"
#import "HomePageHeaderView.h"
#import "UserHomeDynamicCell.h"
#import "DynamicUploadManager.h"
#import "RCUserCacheManager.h"
#import "DynamicApi.h"
#import "HomePageApi.h"

@interface UserHomePageVC ()

@property(nonatomic,strong) HomePageHeaderView *headerView;
@property(nonatomic,strong) NSArray *dynamic;


@end

@implementation UserHomePageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"个人主页");
    
//    [self setRightItemWithTitle:LS(@"编辑") selector:@selector(editBtnClick:)];
    [self setRightItemWithIcon:[UIImage imageNamed:@"ic_user_edit"] selector:@selector(editBtnClick:)];
    
    self.headerView = [[HomePageHeaderView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 400)];
    self.tableView.tableHeaderView = self.headerView;
    
    self.datas = [@[LS(@"身份标识"),LS(@"等级")]mutableCopy];
    
    self.userModel = [RCUserCacheManager sharedManager].currentUser;
//    self.headerView.userModel = [RCUserCacheManager sharedManager].currentUser;
    
    [RACObserve([RCUserCacheManager sharedManager], currentUser) subscribeNext:^(id x) {
        self.headerView.userModel =  x;
    }];
    
    WEAK_SELF;
    [[[HomePageApi alloc]initWitObject:@{@"member_id":self.userModel.member_id}] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        NSLog(@"用户个人主页 %@",[responseObj keyValues]);
        if (info.error == ErrorCodeType_None){
            
            NSArray *array = [DynamicModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"dynamic"]];
            weakSelf.dynamic = array;
        }
    }];

    

    [RACObserve(self.headerView, frameHeight) subscribeNext:^(id x) {
        [self.tableView reloadData];
    }];
    // Do any additional setup after loading the view.
}

//-(void)resloveDynamicImage:(NSArray *)dynamicList{
//    NSMutableArray *images = [NSMutableArray array];
//    for (DynamicModel *model in dynamicList){
//        NSArray *imgs = [model.content_images JSONObject];
//        if ([imgs count] > 0) {
//            [images addObject:imgs[0]];
//        }
//    }
//    self.dynamicImages = images;
//}


-(UIView *)rightView{
    UIView *view = [[UIView alloc]init];
    
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_user_level_1"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:imageView];
    
    UILabel *label = [ControllerHelper autoFitLabel];
    label.font = SystemFont(12);
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"达人";
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = AppBlue;
    label.layer.cornerRadius = 3;
    label.layer.masksToBounds = YES;
    
    [view addSubview:label];
    
    [RACObserve([RCUserCacheManager sharedManager], currentUser) subscribeNext:^(UserModel *x) {
        [imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(view.mas_top);
            make.bottom.equalTo(view.mas_bottom);
            if (x.is_member){
                make.right.equalTo(view.mas_right).offset(-5);
                make.width.mas_equalTo(20);
            }else{
                make.right.equalTo(view.mas_right);
                make.width.mas_equalTo(0);
            }
        }];
        
        [label mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(imageView.mas_left).offset(-5);
            make.top.equalTo(view.mas_top).offset(2);
            make.bottom.equalTo(view.mas_bottom).offset(-2);
            if (x.is_talent){
                make.width.mas_equalTo(40);
            }else{
                make.width.mas_equalTo(0);
            }
        }];
    }];
    
    return view;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)editBtnClick:(id)sender{
    [[RouteController sharedManager] openClassVC:@"UserEditVC"];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return section == 0 ? 2 : 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return indexPath.section == 0 ? 50 : 120;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        NormalTableViewCell *cell = [NormalTableViewCell cellWithType:NormalCellType_Access_Detail identifier:@"Cell" tableView:tableView];
        cell.textLabel.text = self.datas[indexPath.row];
        cell.type = indexPath.row == 0 ? NormalCellType_BottomLine :NormalCellType_None;
        [cell rac_prepareForReuseSignal];
        
        [RACObserve(self, userModel) subscribeNext:^(UserModel *x) {
            if (indexPath.row == 0) {
                cell.rightView = [self rightView];
            }else{
                cell.detailLbl.text = x.level;
            }
        }];
        return cell;
    }else{
        UserHomeDynamicCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil) {
            cell = [[UserHomeDynamicCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
            [cell rac_prepareForReuseSignal];
        }
        [RACObserve(self, dynamic) subscribeNext:^(id x) {
            cell.datas = x;
        }];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        [[RouteController sharedManager] openClassVC:@"DynamicListVC" withObj:self.userModel];
    }else if (indexPath.section == 0){
        if (indexPath.row == 1)
            [self pushViewControllerWithName:@"MineLevelVC"];
        else
            [self pushViewControllerWithName:@"UserIdentifyVC"];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
