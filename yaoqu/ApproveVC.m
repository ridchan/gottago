//
//  ApproveVC.m
//  yaoqu
//
//  Created by ridchan on 2017/8/9.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "ApproveVC.h"
#import "NormalTextField.h"
#import "DashBorderImageView.h"
#import "ApproveApi.h"
#import "SRActionSheet.h"
#import "DynamicUploadManager.h"
#import "RCUserCacheManager.h"

@interface ApproveVC ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(nonatomic,strong) UIButton *submitBtn;
@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) UIView *nameView;
@property(nonatomic,strong) UIView *idView1;
@property(nonatomic,strong) UIView *idView2;
@property(nonatomic,strong) UILabel *tipLbl1;
@property(nonatomic,strong) UILabel *tipLbl2;

@property(nonatomic,strong) NormalTextField *nameFld;
@property(nonatomic,strong) NormalTextField *numFld;

@property(nonatomic,strong) UIImageView *positiveImage;
@property(nonatomic,strong) UIImageView *obverseImage;
@property(nonatomic,strong) UIImageView *selectedImage;

@property(nonatomic,strong) UIImagePickerController *picker;


@property(nonatomic,strong) ApproveModel *approveModel;

@end

@implementation ApproveVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = LS(@"实名认证");
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom).offset(-50);
    }];
    
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.scrollView.mas_bottom);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    WEAK_SELF;
    [[[ApproveApi alloc]init] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        weakSelf.approveModel = [ApproveModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
    }];
    
    
    switch ([RCUserCacheManager sharedManager].currentUser.is_approve) {
        case ApproveType_None:
            self.submitBtn.backgroundColor = [UIColor orangeColor];
            [self.submitBtn setTitle:LS(@"提交认证") forState:UIControlStateNormal];
            break;
        case ApproveType_Ing:
            self.obverseImage.userInteractionEnabled = NO;
            self.positiveImage.userInteractionEnabled = NO;
            self.numFld.enabled = NO;
            self.nameFld.enabled = NO;
            self.submitBtn.enabled = NO;
            self.submitBtn.backgroundColor = AppGray;
            [self.submitBtn setTitle:LS(@"认证审核中") forState:UIControlStateNormal];
            break;
        case ApproveType_Fail:
            self.submitBtn.backgroundColor = [UIColor orangeColor];
            [self.submitBtn setTitle:LS(@"重新提交") forState:UIControlStateNormal];
            break;
        default:
            break;
    }

    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
//    [self wr_setStatusBarStyle:UIStatusBarStyleDefault];
    [super viewDidAppear:animated];
    
}

-(void)viewWillAppear:(BOOL)animated{
//    [self wr_setStatusBarStyle:UIStatusBarStyleDefault];
    [super viewWillAppear:animated];
}

-(UIButton *)submitBtn{
    if (!_submitBtn) {
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [_submitBtn addTarget:self action:@selector(submitBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
        [self.view addSubview:_submitBtn];
    }
    return _submitBtn;
}


-(UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]init];
        _scrollView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_scrollView];
        
        [_scrollView addSubview:self.nameView];
        
        [self.nameView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.scrollView.mas_top).offset(10);
            make.left.equalTo(self.scrollView.mas_left);
            make.width.mas_equalTo(APP_WIDTH);
        }];
        
        
        
        [self.tipLbl1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.scrollView.mas_left).offset(10);
            make.top.equalTo(self.nameView.mas_bottom).offset(20);
        }];
        
        
        [_scrollView addSubview:self.idView1];
        
        
        
        [self.idView1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.tipLbl1.mas_bottom).offset(10);
            make.left.equalTo(self.scrollView.mas_left);
            make.width.mas_equalTo(APP_WIDTH);
        }];
        
        
        [self.tipLbl2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.scrollView.mas_left).offset(10);
            make.top.equalTo(self.idView1.mas_bottom).offset(20);
        }];
        
        [_scrollView addSubview:self.idView2];
        
        
        [self.idView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.tipLbl2.mas_bottom).offset(10);
            make.left.equalTo(self.scrollView.mas_left);
            make.width.mas_equalTo(APP_WIDTH);
            make.bottom.equalTo(self.scrollView.mas_bottom);
        }];
    }
    return _scrollView;
}

-(UIView *)nameView{
    if (!_nameView) {
        _nameView = [[UIView alloc]init];
        _nameView.backgroundColor = [UIColor whiteColor];
        
        
        self.nameFld = [[NormalTextField alloc]init];
        self.nameFld.textColor = RGB16(0x333333);
        self.nameFld.titleLabel.textColor = RGB16(0x666666);
        self.nameFld.title = @"身份证姓名";
        self.nameFld.centerLine.hidden = YES;
        self.nameFld.placeholder = @"必须和证件上一致";
        RAC(self.nameFld,text) = RACObserve(self, approveModel.fullname);
        [_nameView addSubview:self.nameFld];
        
        self.numFld = [[NormalTextField alloc]init];
        self.numFld.textColor = RGB16(0x333333);
        self.numFld.titleLabel.textColor = RGB16(0x666666);
        self.numFld.title = @"身份证号码";
        self.numFld.centerLine.hidden = YES;
        self.numFld.placeholder = @"请输入正确的身份证号码";
        RAC(self.numFld,text) = RACObserve(self, approveModel.idcard);
        [_nameView addSubview:self.numFld];
        
        [self.nameFld mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_nameView.mas_top);
            make.left.equalTo(_nameView.mas_left).offset(10);
            make.right.equalTo(_nameView.mas_right);
            make.height.mas_equalTo(65);
        }];
        
        [self.numFld mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.nameFld.mas_bottom);
            make.left.equalTo(_nameView.mas_left).offset(10);
            make.right.equalTo(_nameView.mas_right);
            make.height.mas_equalTo(65);
            make.bottom.equalTo(_nameView.mas_bottom);
        }];
        
    }
    return _nameView;
}

-(UILabel *)tipLbl1{
    if (!_tipLbl1) {
        UILabel *tipLbl1 = [ControllerHelper autoFitLabel];
        tipLbl1.text = LS(@"身份证上传(正面)");
        tipLbl1.textColor = RGB16(0x666666);
        [self.scrollView addSubview:tipLbl1];
        
        _tipLbl1 = tipLbl1;
        
    }
    return _tipLbl1;
}

-(UIView *)idView1{
    if (!_idView1) {
        _idView1 = [[UIView alloc]init];
        _idView1.backgroundColor = [UIColor whiteColor];
        
        
        
        UIImageView *addImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_approve_add"]];
        [_idView1 addSubview:addImage];
        [addImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_idView1.mas_centerX);
            make.centerY.equalTo(_idView1.mas_centerY).offset(-20);
            make.size.mas_equalTo(CGSizeMake(50, 50));
        }];
        
        UILabel *tipLbl2 = [ControllerHelper autoFitLabel];
        tipLbl2.text = LS(@"身份证正面");
        [_idView1 addSubview:tipLbl2];
        
        [tipLbl2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(addImage.mas_bottom).offset(10);
            make.centerX.equalTo(_idView1.mas_centerX);
        }];
        
        
        
        UILabel *tipLbl3 = [ControllerHelper autoFitLabel];
        tipLbl3.text = LS(@"请保证图片清晰可读");
        tipLbl3.font = SystemFont(12);
        tipLbl3.textColor = AppGray;
        [_idView1 addSubview:tipLbl3];
        
        [tipLbl3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(tipLbl2.mas_bottom).offset(10);
            make.centerX.equalTo(_idView1.mas_centerX);
        }];
        
        self.positiveImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_approve_dashline"]];
        
        self.positiveImage.userInteractionEnabled = YES;
        [_idView1 addSubview:self.positiveImage];
        
        [RACObserve(self, approveModel.positive) subscribeNext:^(id x) {
            [self.positiveImage setImageName:x placeholder:[UIImage imageNamed:@"ic_approve_dashline"]];
        }];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageTap:)];
        [self.positiveImage addGestureRecognizer:tap];
        
        
        
        [self.positiveImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_idView1.mas_left).offset(15);
            make.right.equalTo(_idView1.mas_right).offset(-15);
            make.top.equalTo(_idView1.mas_top).offset(20);
            make.bottom.equalTo(_idView1.mas_bottom).offset(-15);
            make.height.mas_equalTo(200);
        }];
        
    }
    return _idView1;
}

-(UILabel *)tipLbl2{
    if (!_tipLbl2) {
        UILabel *tipLbl1 = [ControllerHelper autoFitLabel];
        tipLbl1.text = LS(@"身份证上传(反面)");
//        tipLbl1.font = SystemFont(14);
        tipLbl1.textColor = RGB16(0x666666);
        [self.scrollView addSubview:tipLbl1];
        
        
        _tipLbl2 = tipLbl1;
    }
    return _tipLbl2;
}

-(UIView *)idView2{
    if (!_idView2) {
        _idView2 = [[UIView alloc]init];
        _idView2.backgroundColor = [UIColor whiteColor];
        
        
        
        UIImageView *addImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_approve_add"]];
        [_idView2 addSubview:addImage];
        [addImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_idView2.mas_centerX);
            make.centerY.equalTo(_idView2.mas_centerY).offset(-20);
            make.size.mas_equalTo(CGSizeMake(50, 50));
        }];
        
        UILabel *tipLbl2 = [ControllerHelper autoFitLabel];
        tipLbl2.text = LS(@"身份证反面");
        [_idView2 addSubview:tipLbl2];
        
        [tipLbl2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(addImage.mas_bottom).offset(10);
            make.centerX.equalTo(_idView2.mas_centerX);
        }];
        
        
        UILabel *tipLbl3 = [ControllerHelper autoFitLabel];
        tipLbl3.text = LS(@"请保证图片清晰可读");
        tipLbl3.font = SystemFont(12);
        tipLbl3.textColor = AppGray;
        [_idView2 addSubview:tipLbl3];
        
        [tipLbl3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(tipLbl2.mas_bottom).offset(10);
            make.centerX.equalTo(_idView2.mas_centerX);
        }];
        
        self.obverseImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_approve_dashline"]];
        self.obverseImage.userInteractionEnabled = YES;
        [_idView2 addSubview:self.obverseImage];
        
        [RACObserve(self, approveModel.obverse) subscribeNext:^(id x) {
            [self.obverseImage setImageName:x placeholder:[UIImage imageNamed:@"ic_approve_dashline"]];
        }];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageTap:)];
        [self.obverseImage addGestureRecognizer:tap];
        
        
        
        [self.obverseImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_idView2.mas_left).offset(15);
            make.right.equalTo(_idView2.mas_right).offset(-15);
            make.top.equalTo(_idView2.mas_top).offset(20);
            make.bottom.equalTo(_idView2.mas_bottom).offset(-15);
            make.height.mas_equalTo(200);
        }];
        
    }
    return _idView2;
}


-(UIImagePickerController *)picker{
    if (!_picker) {
        _picker = [[UIImagePickerController alloc]init];
        _picker.modalPresentationStyle= UIModalPresentationOverFullScreen;
//        _picker.modalPresentationCapturesStatusBarAppearance = YES;
        _picker.delegate = self;
    }
    return _picker;
}


-(void)submitBtnClick:(id)sender{
    ApproveUploadModel *model = [[ApproveUploadModel alloc]init];
    model.positiveImage = self.positiveImage.image;
    model.obverseImage = self.obverseImage.image;
    model.fullname = self.nameFld.text;
    model.idcard = self.numFld.text;
    
    WEAK_SELF;
    [[DynamicUploadManager sharedManager] putApprove:model block:^(id responseObj) {
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
}

-(void)imageTap:(UITapGestureRecognizer *)recognizer{
    
    WEAK_SELF;
    
    self.selectedImage = (DashBorderImageView *)recognizer.view;
    [[SRActionSheet sr_actionSheetViewWithTitle:nil cancelTitle:LS(@"取消") destructiveTitle:nil otherTitles:@[LS(@"相机"),LS(@"相册")] otherImages:nil selectSheetBlock:^(SRActionSheet *actionSheet, NSInteger index) {
        if (index < 0) return ;
        
        weakSelf.picker.sourceType =  index == 0 ? UIImagePickerControllerSourceTypeCamera :UIImagePickerControllerSourceTypePhotoLibrary;
        weakSelf.picker.editing = NO;
        [weakSelf presentViewController:weakSelf.picker animated:YES completion:NULL];
        
    }] show];

    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:^{
//        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
//        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
//        self.navigationController.navigationBar.frame = CGRectMake(0, 0, APP_WIDTH, 64);
//         [self setNeedsStatusBarAppearanceUpdate];
    }];
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    self.selectedImage.image =  [info objectForKey:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
        self.navigationController.navigationBar.frame = CGRectMake(0, 0, APP_WIDTH, 64);
         [self setNeedsStatusBarAppearanceUpdate];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
