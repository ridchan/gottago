//
//  MineGiftRollVC.m
//  yaoqu
//
//  Created by ridchan on 2017/8/9.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineGiftRollVC.h"

#import "CoinChargeView.h"
#import "RCUserCacheManager.h"
#import "WalletApi.h"
#import "RechargeModel.h"

@interface MineGiftRollVC ()

@property(nonatomic,strong) UIView *headerView;

@end

@implementation MineGiftRollVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self commonInit];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)commonInit{
    
    
    self.title = LS(@"礼券明细");
    
    [self setRightItemWithIcon:[UIImage imageNamed:@"ic_question"] selector:nil];
    
    
    self.tableView.tableHeaderView = self.headerView;

    
    
    WEAK_SELF;
    [[[WalletApi alloc]initWitObject:@{@"type":@(RechargeType_Rollgift)}] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        weakSelf.isloading = NO;
        weakSelf.datas = [RechargeModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        [weakSelf.tableView reloadData];
    }];
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom).offset(-50);
    }];
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"充值" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(payBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = AppOrange;
    [self.view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(50);
    }];
}


-(void)chargeBtnClick:(id)sender{
    [CoinChargeView show:nil inView:self.view delegate:self];
}

-(void)didSelectCoin:(id)obj{
    [self pushViewControllerWithName:@"TripCoinPayVC" params:obj];
}

-(void)payBtnClick:(id)sender{
    [self pushViewControllerWithName:@"MineTicketVC"];
}

#pragma mark -
#pragma mark lazy layout

-(UIView *)headerView{
    if (!_headerView) {
        _headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 150)];
        _headerView.backgroundColor = [UIColor orangeColor];
        
        UIImage *image = [UIImage imageNamed:@"bg_yellow_buble"];
        UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
        [_headerView addSubview:imageView];
        
        CGFloat height = APP_WIDTH  * image.size.height / image.size.width;
        _headerView.frame = CGRectMake(0, 0, APP_WIDTH, height);
        imageView.frame = CGRectMake(0, 0, APP_WIDTH, height);
        
        
        UILabel *priceLbl = [ControllerHelper autoFitLabel];
        priceLbl.font = SystemFont(18);
        priceLbl.textAlignment = NSTextAlignmentCenter;
        priceLbl.textColor = [UIColor whiteColor];
        priceLbl.text = @"200";
        
        [RACObserve([RCUserCacheManager sharedManager], currentUser.giftroll) subscribeNext:^(NSString *x) {
            NSString *str =[NSString stringWithFormat:@"当前 %@",x];
            NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:str];
            [att addAttribute:NSFontAttributeName value:SystemFont(28) range:[str rangeOfString:x]];
            priceLbl.attributedText = att;
        }];
        [_headerView addSubview:priceLbl];
        
        
        UILabel *tipLbl = [ControllerHelper autoFitLabel];
        tipLbl.font = SystemFont(11);
        tipLbl.textColor = [UIColor whiteColor];
        tipLbl.text = @"礼物券可以购买礼物，礼物可以赠送也可以兑现旅游币";
        [_headerView addSubview:tipLbl];
        
        [priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_headerView.mas_centerX);
            make.centerY.equalTo(_headerView.mas_centerY);
        }];
        
        
        UIView *line1 = [[UIView alloc]init];
        line1.backgroundColor = [UIColor whiteColor];
        [_headerView addSubview:line1];
        
        UIView *line2 = [[UIView alloc]init];
        line2.backgroundColor = [UIColor whiteColor];
        [_headerView addSubview:line2];
        
        
        [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(priceLbl.mas_left).offset(-5);
            make.centerY.equalTo(priceLbl.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(60, 0.5));
        }];
        
        [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(priceLbl.mas_right).offset(5);
            make.centerY.equalTo(priceLbl.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(60, 0.5));
        }];
        
        [tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_headerView.mas_centerX);
            make.bottom.equalTo(_headerView.mas_bottom).offset(-20);
        }];
        
    }
    return _headerView;
}


-(UIView *)sectionTipView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 40)];
    
    UIView *bgView =[[UIView alloc]initWithFrame:CGRectMake(0, 10, APP_WIDTH, 30)];
    bgView.backgroundColor = [UIColor whiteColor];
    [view addSubview:bgView];
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 39.5, APP_WIDTH, 0.5)];
    line.backgroundColor = AppLineColor;
    [view addSubview:line];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"礼物券明细";
    label.textColor = AppGray;
    label.font = SystemFont(11);
    label.frame = CGRectMake(10, 10, APP_WIDTH - 10, 30);
    [view addSubview:label];
    
    return view;
}

-(UIView *)sectionBottomView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 40)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = view.bounds;
    [button addTarget:self action:@selector(moreBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"查看更多>>";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = AppGray;
    label.font = SystemFont(11);
    label.frame = CGRectMake(10, 0, APP_WIDTH - 20, 40);
    [view addSubview:label];
    [view addSubview:button];
    return view;
}

#pragma mark -

-(void)moreBtnClick:(id)sender{
    [self pushViewControllerWithName:@"MineWalletDetailVC" params:@(RechargeType_Currency)];
}

#pragma mark -
#pragma mark table view

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return self.datas.count > 0 ? [self sectionTipView] : nil;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return self.datas.count > 0 ? [self sectionBottomView] : nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 40;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
    return [tableView cellHeightForIndexPath:indexPath model:nil keyPath:nil cellClass:[NormalTableViewCell class] contentViewWidth:APP_WIDTH - 20];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return MIN(self.datas.count,10);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NormalTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil ) {
        cell = [[NormalTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        cell.textLabel.font = UserNameFont;
        cell.detailTextLabel.font = DetailFont;
        cell.detailTextLabel.textColor = AppGray;
        cell.celltype = NormalCellType_Detail;
    }
    RechargeModel *model = [self.datas objectAtIndex:indexPath.row];
    cell.textLabel.text = model.desc;
    cell.detailTextLabel.text = model.time;
    cell.detailLbl.text = model.val;
    
    return cell;
}

@end
