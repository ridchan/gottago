//
//  DynamicDetailBottomBar.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/4.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DynamicModel.h"

@protocol DynaicDetailBottomBarDelegate <NSObject>

-(void)bottomBarTextClick:(id)obj;
-(void)bottomBarGiftClick:(id)obj;
-(void)bottomBarCommentClick:(id)obj;

@end

@interface DynamicDetailBottomBar : UIView

@property(nonatomic,strong) DynamicModel *model;

@property(nonatomic) id<DynaicDetailBottomBarDelegate> delegate;

@end
