//
//  LoginPostModel.h
//  yaoqu
//
//  Created by ridchan on 2017/8/1.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface LoginPostModel : BaseModel

@property(nonatomic,strong) NSString *email;
@property(nonatomic,strong) NSString *phone;//	手机号码	true	string
@property(nonatomic,strong) NSString *password;//	密码	false	String
@property(nonatomic,strong) NSString *password2;//	确认密码	false	String
@property(nonatomic,strong) NSString *code;//	验证码	true	int		4
@property(nonatomic,strong) NSString *username;//	昵称	true	String
@property(nonatomic,strong) NSString *sex;//	性别	true	int		0-2
@property(nonatomic,strong) NSString *image;//	头像	true	String
@property(nonatomic,strong) NSString *openid;//	openid	false	String
@property(nonatomic,strong) NSString *unionid;//	unionid	false	String
@property(nonatomic,strong) NSString *registration_id;//	极光 注册ID 如果没有 后期要补充哦要不收不到推送	false	String
@property(nonatomic,strong) NSString *father_id;//分销 上级 ID
@property(nonatomic,strong) NSString *qq_openid;

@property(nonatomic,strong) NSString *imagelink;

@end
