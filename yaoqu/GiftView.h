//
//  GiftView.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/30.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GiftPostModel;
@protocol GiftViewDelegate <NSObject>

-(void)giftView:(id)view didSelectObj:(id)obj;
-(void)didSendGiftSuccess:(id)obj;

@end

@interface GiftIconButton : UIButton

@property(nonatomic,strong) UIImageView *iconImage;
@property(nonatomic,strong) UIImageView *accessImage;
@property(nonatomic,strong) UILabel *label;

@end

@interface GiftView : UIView

+(void)show;
+(void)showWithDelegate:(id)delegate;
+(void)showWithDelegate:(id)delegate postModel:(GiftPostModel *)postModel;
-(void)dismiss;

@end



