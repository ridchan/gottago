//
//  YQWebViewVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "YQWebViewVC.h"
#import "RCUserCacheManager.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "RechargeModel.h"
#import "TripCoinPayVC.h"
#import <WebKit/WebKit.h>
#import "CoinChargeView.h"
#import "ShareView.h"

@interface YQWebViewVC ()<UIScrollViewDelegate,UIWebViewDelegate,CoinChargeDelegate> //WKNavigationDelegate,WKScriptMessageHandler,WKUIDelegate,

@property(nonatomic,strong) UIWebView *webView;
@property(nonatomic,strong) UIActivityIndicatorView *indicatorView;


@end

@implementation YQWebViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    if (!self.link) self.link = self.paramObj;
    
    
//    self.webView.frame = CGRectMake(0, 64, APP_WIDTH, APP_HEIGHT - 64);
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top).offset(64);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];

    
    self.webView.backgroundColor = RGB16(0xf6f6f6);
    self.view.backgroundColor = RGB16(0xf6f6f6);
    
    if ([self.link hasSuffix:@"reward.html"]){
        [self setRightItemWithIcon:[UIImage imageNamed:@"ic_question"] selector:@selector(helpBtnClick:)];
    }
    

    self.link = [NSString stringWithFormat:@"%@&access_token=%@",self.link,
            [RCUserCacheManager sharedManager].currentToken.access_token];
    
    
    
    
    NSString *oldAgent = [self.webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];


    NSString *customUserAgent = nil;


    if ([oldAgent rangeOfString:@"gottago"].location == NSNotFound){
        customUserAgent = [NSString stringWithFormat:@"%@( gottago )",oldAgent];
    }else{
        customUserAgent = oldAgent;
    }

    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"UserAgent":customUserAgent}];
    
    
    
    NSURL *url = [NSURL URLWithString:self.link];
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    
    [self.indicatorView startAnimating];
    self.webView.hidden = YES;
    
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardShow:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardHide:) name:UIKeyboardWillHideNotification object:nil];
    // Do any additional setup after loading the view.
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)keyBoardShow:(NSNotification *)note{
    
//    CGRect begin = [[[note userInfo] objectForKey:@"UIKeyboardFrameBeginUserInfoKey"] CGRectValue];
    
    CGRect end = [[[note userInfo] objectForKey:@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
    
    [self.webView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top).offset(64);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom).offset(-end.size.height);
    }];
    
}

-(void)keyBoardHide:(NSNotification *)note{
    [self.webView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(self.view.mas_top).offset(64);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
}

-(void)closeMe{
    if (self.navigationController) {
        if ([self.navigationController.viewControllers firstObject] == self) {
            [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }else{
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}


#pragma mark -

-(void)helpBtnClick:(id)sender{
    YQWebViewVC *vc = [[YQWebViewVC alloc]init];
    vc.link = [[CommonConfigManager sharedManager].api_host stringByAppendingPathComponent:@"help.html?type=reward"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    NSHTTPCookie *cookie;
    
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    NSArray *cookieAry = [cookieJar cookies];
    
    for (cookie in cookieAry) {
        NSLog(@"cookies %@ = %@",cookie.name,cookie.value);
    }
}


-(UIActivityIndicatorView *)indicatorView{
    if (!_indicatorView) {
        _indicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _indicatorView.hidesWhenStopped = YES;
        [self.view addSubview:_indicatorView];
        [_indicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view.mas_centerX);
            make.centerY.equalTo(self.view.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(15, 15));
        }];
    }
    return _indicatorView;
}


-(void)webViewRefresh{
    [self.webView stringByEvaluatingJavaScriptFromString:@"app_refresh()"];
    if ([self.link rangeOfString:@"wallet"].location != NSNotFound) {
        NSURL *url = [NSURL URLWithString:self.link];
        [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
        [[RCUserCacheManager sharedManager] renewAccount];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self webViewRefresh];
}


-(void)webViewDidFinishLoad:(UIWebView *)webView{
    webView.hidden = NO;
    [self.indicatorView stopAnimating];
    if (!self.title){
        NSString *theTitle=[webView stringByEvaluatingJavaScriptFromString:@"document.title"];
        self.title = theTitle;
    }
}



-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    webView.hidden = NO;
    [self.indicatorView stopAnimating];
    NSLog(@"webview  error %@",error);
//    NSLog(@"webview load %@",[self.webView stringByEvaluatingJavaScriptFromString:@"returnData()"]);
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSLog(@"request %@",request.URL.absoluteString);
    if ([request.URL.absoluteString hasPrefix:@"app://open/link/"]){
        NSString *link = [request.URL.absoluteString substringFromString:@"open/link/"];
        YQWebViewVC *vc = [[YQWebViewVC alloc]init];
        vc.paramObj = self.paramObj;
        vc.link = link;
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([request.URL.absoluteString hasPrefix:@"app://open/back"]){
        
        [self.navigationController popViewControllerAnimated:YES];
    }else if ([request.URL.absoluteString hasPrefix:@"app://run/returnData"]){
        
        
        
        
        NSString *value = [self.webView stringByEvaluatingJavaScriptFromString:@"returnData()"];
        
        
        NSDictionary *dict = [value JSONObject];
        if ([[dict allKeys] count] == 1){
            RechargeModel *model = [[RechargeModel alloc]init];
            model.title_desc = LS(@"余额充值");
            model.title_value = [dict objectForKey:@"val"];
            model.val = [dict objectForKey:@"val"];
            model.pay_total = [dict objectForKey:@"val"];
            model.type = [NSString intValue:RechargeType_Money];
            model.actionType = [NSString intValue:RechargeType_Money];
            
            
            [self pushViewControllerWithName:@"TripCoinPayVC" params:model];
        }else{
            TravelEnrollModel *model = [TravelEnrollModel objectWithKeyValues:[value JSONObject]];
            RechargeModel *rechargeModel = [[RechargeModel alloc]init];
            rechargeModel.title_desc = LS(@"旅游报名");
            rechargeModel.title_value = model.title;
            rechargeModel.val = [dict objectForKey:@"show_pay_total"];
            rechargeModel.pay_total = [dict objectForKey:@"show_pay_total"];
            rechargeModel.type = [NSString intValue:RechargeType_TravelEnroll];
            rechargeModel.actionType = [NSString intValue:RechargeType_TravelEnroll];
            //
            
            
            TripCoinPayVC *vc = [[TripCoinPayVC alloc]init];
            vc.payViewType = CommonPayViewType_TravelEnroll;
            vc.paramObj = rechargeModel;
            vc.titleDesc = model.title;
            vc.enrollModel = model;
            vc.enrollObj  = [[value JSONObject] mutableCopy];
            
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }else if ([request.URL.absoluteString hasPrefix:@"app://open/view/withdraw"]){//提现
        switch ([RCUserCacheManager sharedManager].currentUser.is_approve) {
            case ApproveType_Success:
                [self pushViewControllerWithName:@"MineToCashVC"];
                break;
            default:
                [self pushViewControllerWithName:@"ApproveVC"];
                break;
        }
    }else if ([request.URL.absoluteString hasPrefix:@"app://open/view/currency/recharge"]){//旅游币
        [CoinChargeView show:nil inView:self.view delegate:self];
    }else if ([request.URL.absoluteString hasPrefix:@"app://open/view/giftroll/recharge"]){//礼品券
        [[RouteController sharedManager] openClassVC:@"MineTicketVC"];
    }else if ([request.URL.absoluteString hasPrefix:@"app://open/returnShare"]){
        NSString *result = [self.webView stringByEvaluatingJavaScriptFromString:@"returnShare()"];

        NSDictionary *dict = [result JSONObject];
        if (dict){
            ShareView *shareView = [[ShareView alloc]init];
            shareView.shareInfo = dict;
            shareView.handleBlock = ^(id Obj){
                if ([Obj integerValue] == ShareType_Copy){
                    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                    pasteboard.string = [[result JSONObject] objectForKey:@"link"];
                    [LLUtils showActionSuccessHUD:@"已复制"];
                }else{
                    
                }
                
            };
            [shareView show];
        }
        
        
    }else if ([request.URL.absoluteString hasPrefix:@"app://open/returnShare"]){
        
    }

        
    
    return YES;
}



-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.y <= 0) {
        scrollView.contentOffset = CGPointMake(0, 0);
    }else if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height){
        scrollView.contentOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.frame.size.height);
    }
}

#pragma mark -
#pragma mark delegate

-(void)didSelectCoin:(CurrencyValueModel *)obj{
    RechargeModel *model = [[RechargeModel alloc]init];
    model.title_desc = LS(@"旅游币充值");
    model.title_value = obj.currency;
    model.val = obj.currency;
    model.pay_total = obj.price;
    model.type = [NSString intValue:RechargeType_Currency];
    model.actionType = [NSString intValue:RechargeType_Currency];
    
    [self pushViewControllerWithName:@"TripCoinPayVC" params:model];
}




-(UIWebView *)webView{
    if (!_webView) {
        _webView = [[UIWebView alloc]init];
        _webView.delegate = self;
//        _webView.navigationDelegate = self;
//        _webView.UIDelegate = self;
//        _webView.customUserAgent = @" gottago ";
        _webView.scrollView.delegate = self;
        [self.view addSubview:_webView];
    }
    return _webView;
}


/*

-(void)webViewRefresh{
    //app_refresh()
    [self.webView evaluateJavaScript:@"app_refresh()" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        NSLog(@"refresh result %@",result);
    }];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self webViewRefresh];
}

#pragma mark -

// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    
}
// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation{
    
}
// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    self.webView.hidden = NO;
    [self.indicatorView stopAnimating];
    self.title = self.webView.title;
    
//    NSHTTPCookie *cookie;
//    
//    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//    
//    NSArray *cookieAry = [cookieJar cookies];
//    
//    for (cookie in cookieAry) {
//        NSLog(@"cookies %@ = %@",cookie.name,cookie.value);
//    }
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation{
    self.webView.hidden = NO;
    [self.indicatorView stopAnimating];
}


- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    //下面这句话一定不能少一少就报错
    decisionHandler(WKNavigationActionPolicyAllow);
    
    NSLog(@"run hear %@",navigationAction.request.URL.absoluteString);
    
    NSString *link = navigationAction.request.URL.absoluteString;
    
    
    if ([link hasPrefix:@"app://open/link/"]){
        YQWebViewVC *vc = [[YQWebViewVC alloc]init];
        vc.paramObj = self.paramObj;
        vc.link = [link substringFromString:@"open/link/"];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([link hasPrefix:@"app://open/back"]){
        [self.navigationController popViewControllerAnimated:YES];
    }else if ([link hasPrefix:@"app://run/returnData"]){
        
        WEAK_SELF;
        
        [self.webView evaluateJavaScript:@"returnData()" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
            TravelEnrollModel *model = [TravelEnrollModel objectWithKeyValues:result];
            
            RechargeModel *rechargeModel = [[RechargeModel alloc]init];
            rechargeModel.title_desc = LS(@"旅游报名");
            rechargeModel.title_value = model.total;
            rechargeModel.val = model.pay_total;
            rechargeModel.pay_total = model.pay_total;
            rechargeModel.type = [NSString intValue:RechargeType_TravelEnroll];
            //
            
            
            TripCoinPayVC *vc = [[TripCoinPayVC alloc]init];
            vc.payViewType = CommonPayViewType_TravelEnroll;
            vc.paramObj = rechargeModel;
            vc.titleDesc = model.title;
            vc.enrollModel = model;
            [weakSelf.navigationController pushViewController:vc animated:YES];

        }];
        
        
        
    }
    
}


- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler{
    //下面这句话一定不能少一少就报错
    decisionHandler(WKNavigationResponsePolicyAllow);
    NSLog(@"run there");
}

// 接收到服务器跳转请求之后调用
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation{
    NSLog(@"%@",navigation.description);
}



-(void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    NSLog(@"cmd %@",NSStringFromSelector(_cmd));
    NSLog(@"message body %@",message.body);
}


*/
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
