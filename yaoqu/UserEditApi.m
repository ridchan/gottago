//
//  UserEditApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserEditApi.h"

@implementation UserEditApi{
    UserModel *_userModel;
}

-(id)initWithUserModel:(UserModel *)userModel{
    if (self = [super init]) {
        _userModel = userModel;
    }
    return self;
}

-(NSString *)requestUrl{
    return UserInfoEditUrl;
}

-(id)requestArgument{
    NSMutableDictionary *dict = [self baseConfig];
    [dict setValue:_userModel.images forKey:@"images"];
    [dict setValue:_userModel.username forKey:@"username"];
    [dict setValue:_userModel.sex forKey:@"sex"];
    [dict setValue:_userModel.desc forKey:@"desc"];
    [dict setValue:_userModel.age forKey:@"age"];
    [dict setValue:_userModel.constellation forKey:@"constellation"];
    [dict setValue:_userModel.pro_id forKey:@"pro_id"];
    [dict setValue:_userModel.pro_name forKey:@"pro_name"];
    [dict setValue:_userModel.city_id forKey:@"city_id"];
    [dict setValue:_userModel.city_name forKey:@"city_name"];
    [dict setValue:_userModel.home_pro_id forKey:@"home_pro_id"];
    [dict setValue:_userModel.home_pro_name forKey:@"home_pro_name"];
    [dict setValue:_userModel.home_city_id forKey:@"home_city_id"];
    [dict setValue:_userModel.home_city_name forKey:@"home_city_name"];
    [dict setValue:_userModel.birthday forKey:@"birthday"];
    return dict;
}

@end
