//
//  DiscoverRankSubVC.h
//  yaoqu
//
//  Created by ridchan on 2017/7/23.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NormalTableViewController.h"

@interface DiscoverRankSubVC : NormalTableViewController

@property(nonatomic) DiscoverRankType rankType;

@end
