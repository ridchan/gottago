//
//  RCVedioDisplayVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/26.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseViewController.h"
#import "LLAssetModel.h"
#import "LLAssetsGroupModel.h"


@interface RCVedioDisplayVC : BaseViewController

@property (nonatomic) LLAssetModel *assetModel;

@property (nonatomic) LLAssetsGroupModel *assetGroupModel;

@end
