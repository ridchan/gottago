//
//  UserHomeDynamicCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/27.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UserHomeDynamicCellLayout : UICollectionViewFlowLayout

@end

@interface UserHomeDynamicCell : UITableViewCell

@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) NSMutableArray *datas;
@property(nonatomic) NSInteger dynamicCount;

@end
