//
//  DynamicListApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/28.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface DynamicListApi : BaseApi

-(id)initWithMemberID:(NSString *)member_id page:(NSInteger)page size:(NSInteger)size;

@end
