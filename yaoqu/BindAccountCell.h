//
//  BindAccountCell.h
//  yaoqu
//
//  Created by ridchan on 2017/8/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UMSocialCore/UMSocialCore.h>

@interface BindAccountCell : UITableViewCell

@property(nonatomic,strong) UIImageView *iconImage;
@property(nonatomic,strong) UILabel *titlLbl;
@property(nonatomic,strong) UIButton *bindBtn;

@property(nonatomic) UMSocialPlatformType type;
@property(nonatomic) BOOL isBindAccount;

@end
