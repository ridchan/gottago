//
//  DynamicListVC.m
//  yaoqu
//
//  Created by ridchan on 2017/6/28.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicListVC.h"
#import "DynamicListApi.h"
#import "DynamicCell.h"
#import "DynamicListApi.h"
#import "DynamicModel.h"
#import "RCUserCacheManager.h"
#import "DynamicUploadManager.h"
#import "DynamicApi.h"

@interface DynamicListVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UserModel *userModel;

@end

@implementation DynamicListVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"动态");
    
    [self.tableView registerClass:[DynamicCell class] forCellReuseIdentifier:@"Cell"];
    
    
    self.userModel = self.paramObj ? self.paramObj : [RCUserCacheManager sharedManager].currentUser;
    self.member_id = self.userModel.member_id;
    
    

    [self startRefresh];
    // Do any additional setup after loading the view.
}


-(void)startRefresh{
    self.page = 1;
    
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:self.member_id forKey:@"member_id"];
    [dict setValue:@(self.page) forKey:@"page"];
    [dict setValue:@(self.size) forKey:@"size"];
    
    
    WEAK_SELF;
    [[[DynamicApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        
        NSArray *array = [DynamicModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        [weakSelf refreshComplete:info response:array];
    }];

}

-(void)startloadMore{
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:self.member_id forKey:@"member_id"];
    [dict setValue:@(++self.page) forKey:@"page"];
    [dict setValue:@(self.size) forKey:@"size"];
    
    WEAK_SELF;
    
    [[[DynamicApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        
        NSArray *array = [DynamicModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        [weakSelf loadMoreComplete:info response:array];
    }];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    id model = self.datas[indexPath.section];
    return [tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[DynamicCell class] contentViewWidth:APP_WIDTH];

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self pushViewControllerWithName:@"DynamicDetailVC" params:self.datas[indexPath.section]];
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return .001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  self.datas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DynamicCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.model = self.datas[indexPath.section];
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
