//
//  MineTicketVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/7.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineTicketVC.h"

#import "RechargeInfoCell.h"
#import "TTTAttributedLabel.h"
#import "CommonConfigManager.h"
#import "RCUserCacheManager.h"
#import "RechargeModel.h"

@interface MineTicketVC ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) NSArray *dataSource;
@property(nonatomic,strong) NormalButton *payBtn;
@property(nonatomic,strong) UIButton *tipBtn;
@property(nonatomic,strong) TTTAttributedLabel *tipLbl;

@property(nonatomic) NSInteger selectIndex;

@end

@implementation MineTicketVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"礼品券充值");
    
    self.dataSource = [CommonConfigManager sharedManager].hostModel.giftroll_val;
    
    [self createViews];
    
    
    
    self.selectIndex = -1;
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createViews{
    
    UILabel *titleLbl = [[UILabel alloc]init];
    titleLbl.textAlignment = NSTextAlignmentCenter;
    
    titleLbl.text = @"当前礼品券";
    titleLbl.textColor = AppGray;
    [self.view addSubview:titleLbl];
    
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(100);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        
    }];
    
    UIView *rollView = [self rollView];
    [self.view addSubview:rollView];
    
//    __weak typeof(chargeLbl) weakLbl = chargeLbl;
//    [RACObserve([UserInfoManager manger].currentUser, integral) subscribeNext:^(NSString *integral) {
//        weakLbl.text = integral;
//    }];
    
    [rollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLbl.mas_bottom).offset(30);
        make.centerX.equalTo(self.view.mas_centerX);
    }];
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:[self layout]];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.collectionView registerClass:[RechargeInfoCell class] forCellWithReuseIdentifier:@"Cell"];
    
    [self.view addSubview:self.collectionView];
    CGFloat height = ceilf(self.dataSource.count / 3.0) * 110  ;
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(rollView.mas_bottom).offset(30);
        make.height.mas_equalTo(height);
    }];
    
    self.payBtn = [NormalButton normalButton:@"立即充值"];
    [self.payBtn addTarget:self action:@selector(payBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    RAC(self.payBtn,enabled) = self.viewModel.btnEnableSignal;
    [self.view addSubview:self.payBtn];
    [self.payBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.collectionView.mas_bottom).offset(20);
        make.left.equalTo(self.view.mas_left).offset(20);
        make.right.equalTo(self.view.mas_right).offset(-20);
        make.height.mas_equalTo(50);
    }];
    
    
    
    self.tipBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.tipBtn setImage:[UIImage imageNamed:@"ic_not_checked"] forState:UIControlStateNormal];
    [self.tipBtn setImage:[UIImage imageNamed:@"ic_checked_yellow"] forState:UIControlStateSelected];
    [self.tipBtn addTarget:self action:@selector(tipBtnClick:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:self.tipBtn];
    [self.tipBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.payBtn.mas_bottom).offset(10);
        make.left.equalTo(self.payBtn.mas_left);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    
    self.tipLbl = [[TTTAttributedLabel alloc]initWithFrame:CGRectZero];
    self.tipLbl.linkAttributes = nil;
    self.tipLbl.activeLinkAttributes = nil;
    [self.view addSubview:self.tipLbl];
    
    NSString *tip = @"我已阅读并同意《用户服务协议》";
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc]initWithString:tip];
    
    [att addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, [tip length])];
    [att addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:[tip rangeOfString:@"《用户服务协议》"]];
    self.tipLbl.attributedText = att;
    [self.tipLbl addLinkToURL:[NSURL URLWithString:@"《用户服务协议》"] withRange:[tip rangeOfString:@"《用户服务协议》"]];
    
    [self.tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tipBtn.mas_top).offset(5);
        make.left.equalTo(self.tipBtn.mas_right).offset(5);
        make.height.mas_equalTo(10);
    }];
    
    
}


-(UIView *)rollView{
    UIView *view = [[UIView alloc]init];
    
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = [UIImage imageNamed:@"ic_gift_roll"];
    [view addSubview:imageView];
    
    UILabel *rollLbl = [ControllerHelper autoFitLabel];
    rollLbl.font = SystemFont(28);
    RAC(rollLbl,text) = RACObserve([RCUserCacheManager sharedManager], currentUser.giftroll);
    [view addSubview:rollLbl];
    
    [rollLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view.mas_right);
        make.top.equalTo(view.mas_top);
        make.bottom.equalTo(view.mas_bottom);
        make.left.equalTo(imageView.mas_right).offset(5);
    }];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left);
        make.top.equalTo(rollLbl.mas_top).offset(3);
        make.bottom.equalTo(rollLbl.mas_bottom).offset(-3);
        make.width.equalTo(imageView.mas_height);
    }];
    
    return view;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(UICollectionViewFlowLayout *)layout{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.minimumLineSpacing = 30;
    layout.minimumInteritemSpacing = 30;
    CGFloat width = (APP_WIDTH - 20 - 60)  / 3.0 ;
    layout.itemSize = CGSizeMake(width, 80);
    return layout;
}

-(void)detailBtnClick:(id)sender{
//    CashRecordVC *vc = [[CashRecordVC alloc]init];
//    vc.type = MoneyRecord_Pay;
//    [self.navigationController pushViewController:vc animated:YES];
}


-(void)tipBtnClick:(UIButton *)button{
    button.selected = !button.selected;
//    self.viewModel.agree = button.selected;
}

-(void)payBtnClick:(id)sender{
    if (_selectIndex < 0) return;
    CurrencyValueModel *obj = self.dataSource[_selectIndex];
    
    RechargeModel *model = [[RechargeModel alloc]init];
    model.title_desc = LS(@"礼品券充值");
    model.title_value = obj.giftroll;
    model.val = obj.giftroll;
    model.pay_total = obj.price;
    model.type = [NSString intValue:RechargeType_Rollgift];
    model.actionType = [NSString intValue:RechargeType_Rollgift];
    
    [self pushViewControllerWithName:@"TripCoinPayVC" params:model];
}

-(void)payResult:(NSNotification *)obj{

}

#pragma mark - collection view


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    self.viewModel.selectIndex = indexPath.item;
    self.selectIndex = indexPath.item;
    [collectionView reloadData];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataSource.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    RechargeInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.model = [self.dataSource objectAtIndex:indexPath.item];
    
    cell.bSelected = self.selectIndex == indexPath.item;
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
