//
//  BaseApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/15.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"
#import "RCUserCacheManager.h"
#import <AFNetworking/AFNetworking.h>

ErrorCodeType lastErrorCodeType;
NSString *outDateCode;

@interface BaseApi()

@end

@implementation BaseApi


CREATE_SHARED_INSTANCE(BaseApi)


-(id)initWitObject:(id)obj{
    if (self = [self init]) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            [self.params addEntriesFromDictionary:obj];
        }else{
            [self.params addEntriesFromDictionary:[obj keyValues]];
        }
    }
    return self;
    
}



- (instancetype)init
{
    self = [super init];
    if (self) {
        [[YTKNetworkConfig sharedConfig] setBaseUrl:MainDomain];
        self.params = [NSMutableDictionary dictionary];
        self.method = YTKRequestMethodGET;
    }
    return self;
}

-(YTKRequestMethod)requestMethod{
    return _method;
}

//
//-(NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary{
//    return @{@"Content-Type":@"application/json"};
//}

-(id)requestArgument{
    
    [self.baseConfig addEntriesFromDictionary:self.params];
    return self.baseConfig;
}

-(NSMutableDictionary *)baseConfig{
    
    if (!_baseConfig) {
        _baseConfig = [NSMutableDictionary dictionary];
        
        NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        
        [_baseConfig setValue:appVersion forKey:@"version"];
        [_baseConfig setValue:@"1" forKey:@"device"];
        [_baseConfig setValue:@"zh-CN" forKey:@"lang"];
        
        
        
    }
    
    if ([RCUserCacheManager sharedManager].currentToken) {
        [_baseConfig setValue:[RCUserCacheManager sharedManager].currentToken.access_token
                forKey:@"access_token"];
    }
    
    
    
    
    return _baseConfig;
}


-(void)startWithCompleteBlock:(NetWorkBlock)block{
//    WEAK_SELF;
    [self startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSLog(@"接口: %@ , 参数: %@ ,方法 : %ld",request.requestUrl,request.requestArgument,request.requestMethod);
        
        NSLog(@"返回结果: %@",request.responseObject);
        
        
        MsgModel *model = [MsgModel objectWithKeyValues:request.responseObject];
        
        
        if (model.code == ErrorCodeType_AuthenticationFailed) {
            
//            if ([RCUserCacheManager sharedManager].currentToken.statu != AccessTokenStatu_Refreshing){
//                [RCUserCacheManager sharedManager].currentToken.statu = AccessTokenStatu_Refreshing;
//                [[RCUserCacheManager sharedManager] renewAccessToken];
//            
//            }
//            [RACObserve([RCUserCacheManager sharedManager], currentToken) subscribeNext:^(id x) {
//                NSLog(@"token 更新，重新发送请求");
//                [weakSelf startWithCompleteBlock:block];
//            }];
            block(nil,nil);
        }else{
            block(model,request.responseObject);
        }
        
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSLog(@"接口错误: %@ , 参数: %@",request.requestUrl,request.requestArgument);
        NSLog(@"返回错误: %@",request.error.description);
        MsgModel *model = [[MsgModel alloc]init];
        model.error = ErrorCodeType_NetWorkError;
        block(model,nil);

    }];
    
    
    
//    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc]initWithBaseURL:[NSURL URLWithString:MainDomain]];
//    
//    
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    manager.requestSerializer.timeoutInterval = 10.0f;
//    
//    
//    manager.responseSerializer = [AFJSONResponseSerializer serializer];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
//    
//    
//    NSLog(@"check link: %@接口: %@ , 参数: %@",[self executeLink],[self requestUrl],[self requestArgument]);
//    
//    
//    
//    
//    [manager POST:[self requestUrl] parameters:[self requestArgument] progress:^(NSProgress * _Nonnull uploadProgress) {
//        
//    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
////        NSString *responseString = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
//        NSLog(@"response %@ %@ \n",responseObject,[responseObject objectForKey:@"msg"]);
////        NSLog(@"执行信息:%@ \n",[responseString json]);
//        MsgModel *model = [MsgModel objectWithKeyValues:responseObject];
//        block(model,responseObject);
//
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        MsgModel *model = [[MsgModel alloc]init];
//        model.code = ErrorCodeType_NetWorkError;
//        block(model,nil);
//        NSLog(@"error %@",error.description);
//        
//    }];
    
}


-(NSString *)executeLink{
    NSMutableString *str = [[[self requestUrl] stringByAppendingString:@"?"] mutableCopy];
    NSDictionary *dic = [self requestArgument];
    for (NSString *key in dic){
        [str appendFormat:@"%@=%@&",key,[dic objectForKey:key]];
    }
    return str;
}



@end
