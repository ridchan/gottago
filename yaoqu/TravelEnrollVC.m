//
//  TravelEnrollVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelEnrollVC.h"
#import "TravelEnrollTitleView.h"
#import "TravelEnrollDetailView.h"
#import "TravelEnrollContactView.h"
#import "TravelEnrollApi.h"
#import "TravelPayTypeView.h"
#import "TravelScheduleApi.h"
#import "TravelContactModel.h"
#import "TravelApi.h"
#import "TripCoinPayVC.h"
#import "CommonConfigModel.h"
#import "RechargeModel.h"
@interface TravelEnrollVC ()

@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) TravelEnrollTitleView *titleView;
@property(nonatomic,strong) TravelEnrollContactView *contactView;
@property(nonatomic,strong) TravelEnrollDetailView *detailView;

@property(nonatomic,strong) UIView *bottomView1;
@property(nonatomic,strong) UIView *bottomView2;

@property(nonatomic,strong) UIButton *comfirmBtn;

@property(nonatomic,strong) TravelEnrollModel *model;


@end

@implementation TravelEnrollVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self commonInit];
    self.title = LS(@"报名");
    
    self.model = [[TravelEnrollModel alloc]init];
    
    WEAK_SELF;
    
    
    [[[TravelApi alloc]initWitObject:@{@"travel_schedule_id":self.scheduleModel.travel_schedule_id}] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            weakSelf.scheduleModel = [TravelScheduleModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
            weakSelf.titleView.scheduleModel = weakSelf.scheduleModel;
            weakSelf.detailView.scheduleModel = weakSelf.scheduleModel;
            [weakSelf.comfirmBtn setTitle:[NSString stringWithFormat:@"确认并支付(%@%@)",MoneySign,weakSelf.scheduleModel.price] forState:UIControlStateNormal];
        };
    }];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)comfirmBtnClick:(id)sender{
    
    NSMutableArray *contact_id = [NSMutableArray arrayWithCapacity:self.contactView.contacts.count];
    for (TravelContactModel *contact in self.contactView.contacts){
        [contact_id addObject:contact.contact_id];
    }
    if ([contact_id count] == 0){
        [LLUtils showCenterTextHUD:@"请选择联系人"];
        return;
    }
    
//    self.model.contact_id = [contact_id JSONString] ;
    self.model.integral = self.detailView.integral;
    self.model.currency = self.detailView.currency;
    self.model.travel_schedule_id = self.scheduleModel.travel_schedule_id;
    self.model.is_return_total = self.detailView.payTotal;
    
//    CurrencyValueModel *model = [[CurrencyValueModel alloc]init];
//    model.currency = self.detailView.payTotal;
//    model.price = self.detailView.payTotal;
    
    
    //支付model
    RechargeModel *rechargeModel = [[RechargeModel alloc]init];
    rechargeModel.title_desc = LS(@"旅游报名");
    rechargeModel.title_value = self.scheduleModel.title;
    rechargeModel.val = self.detailView.payTotal;
    rechargeModel.pay_total = self.detailView.payTotal;
    rechargeModel.type = [NSString intValue:RechargeType_TravelEnroll];
    rechargeModel.actionType = [NSString intValue:RechargeType_TravelEnroll];
    //
    
    
    TripCoinPayVC *vc = [[TripCoinPayVC alloc]init];
    vc.payViewType = CommonPayViewType_TravelEnroll;
    vc.paramObj = rechargeModel;
    vc.titleDesc = self.scheduleModel.title;
    vc.enrollModel = self.model;
    [self.navigationController pushViewController:vc animated:YES];
    
//    WEAK_SELF;
//    [TravelPayTypeView showInView:self.navigationController.view compelteBlock:^(id responseObj) {
//        weakSelf.model.pay_type = [responseObj integerValue];
//        [[[TravelEnrollApi alloc]initWithModel:weakSelf.model] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
//            
//        }];
//    }];
    
}

-(void)commonInit{
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).insets(UIEdgeInsetsMake(0, 0, 50, 0));
    }];
    
    [self.comfirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(50);
    }];
    
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scrollView.mas_top).offset(10);
        make.left.equalTo(self.scrollView.mas_left);
        make.width.mas_equalTo(APP_WIDTH);
    }];
    
    [self.contactView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView.mas_left);
        make.right.equalTo(self.titleView.mas_right);
        make.top.equalTo(self.titleView.mas_bottom).offset(10);
       
    }];
    
    [self.detailView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView.mas_left);
        make.right.equalTo(self.titleView.mas_right);
        make.top.equalTo(self.contactView.mas_bottom).offset(10);
//        make.bottom.equalTo(self.scrollView.mas_bottom);
    }];
    
    [self.bottomView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView.mas_left);
        make.right.equalTo(self.titleView.mas_right);
        make.top.equalTo(self.detailView.mas_bottom).offset(10);
        make.height.mas_equalTo(40);
    }];
    
    [self.bottomView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView.mas_left);
        make.right.equalTo(self.titleView.mas_right);
        make.top.equalTo(self.bottomView1.mas_bottom).offset(10);
        make.height.mas_equalTo(40);
        make.bottom.equalTo(self.scrollView.mas_bottom).offset(-10);
    }];
}


-(UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]init];
        [self.view addSubview:_scrollView];
    }
    return _scrollView;
}

-(UIButton *)comfirmBtn{
    if (!_comfirmBtn) {
        _comfirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_comfirmBtn setTitle:@"确认报名" forState:UIControlStateNormal];
        _comfirmBtn.backgroundColor = [UIColor orangeColor];
        [_comfirmBtn addTarget:self action:@selector(comfirmBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [RACObserve(self.detailView, payTotal) subscribeNext:^(id x) {
            [self.comfirmBtn setTitle:[NSString stringWithFormat:@"确认并支付(%@%@)",MoneySign,x] forState:UIControlStateNormal];
        }];
        [self.view addSubview:_comfirmBtn];
        
    }
    return _comfirmBtn;
}

-(TravelEnrollTitleView *)titleView{
    if (!_titleView) {
        _titleView = [[TravelEnrollTitleView alloc]init];
        [self.scrollView addSubview:_titleView];
    }
    return _titleView;
}

-(TravelEnrollContactView *)contactView{
    if (!_contactView) {
        _contactView = [[TravelEnrollContactView alloc]init];
        [RACObserve(_contactView, contactNum) subscribeNext:^(id x) {
            self.scheduleModel.total = [NSString stringWithFormat:@"%0.0f",[x integerValue] * [self.scheduleModel.price floatValue]];
        }];
        [self.scrollView addSubview:_contactView];
    }
    return _contactView;
}

-(TravelEnrollDetailView *)detailView{
    if (!_detailView) {
        _detailView = [[TravelEnrollDetailView alloc]init];
        [self.scrollView addSubview:_detailView];
    }
    return _detailView;
}

-(UIView *)bottomView1{
    if (!_bottomView1) {
        _bottomView1 = [[UIView alloc]init];
        _bottomView1.backgroundColor = [UIColor whiteColor];
        [self.scrollView addSubview:_bottomView1];
        
        UILabel *label = [ControllerHelper autoFitLabel];
        label.text = LS(@"政策");
        [_bottomView1 addSubview:label];
        
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bottomView1.mas_left).offset(10);
            make.top.equalTo(_bottomView1.mas_top);
            make.height.mas_equalTo(40);
        }];
        
        UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_forward"]];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [_bottomView1 addSubview:imageView];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_bottomView1.mas_right).offset(-10);
            make.top.equalTo(_bottomView1.mas_top);
            make.height.mas_equalTo(40);
            make.width.mas_equalTo(RightAccessWidth);
        }];


    }
    
    return _bottomView1;
}

-(UIView *)bottomView2{
    if (!_bottomView2) {
        _bottomView2 = [[UIView alloc]init];
        _bottomView2.backgroundColor = [UIColor whiteColor];
        [self.scrollView addSubview:_bottomView2];
        
        UILabel *label = [ControllerHelper autoFitLabel];
        label.text = LS(@"确认是否同意免责条款");
        [_bottomView2 addSubview:label];
        
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_bottomView2.mas_left).offset(10);
            make.top.equalTo(_bottomView2.mas_top);
            make.height.mas_equalTo(40);
        }];
        
        UIImageView *leftImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_checked_yellow"]];
        leftImg.contentMode = UIViewContentModeScaleAspectFit;
        [_bottomView2 addSubview:leftImg];
        
        [leftImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(label.mas_right).offset(5);
            make.top.equalTo(_bottomView2.mas_top);
            make.height.mas_equalTo(40);
            make.width.mas_equalTo(20);
        }];

        
        
        
        UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_question"]];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [_bottomView2 addSubview:imageView];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_bottomView2.mas_right).offset(-10);
            make.top.equalTo(_bottomView2.mas_top).offset(10);
            make.bottom.equalTo(_bottomView2.mas_bottom).offset(-10);
            make.width.mas_equalTo(imageView.mas_height);
        }];
        
        
    }
    
    return _bottomView2;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
