//
//  DiscoverDynamicVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DiscoverDynamicVC.h"
#import "DynamicCell.h"
#import "DynamicUploadManager.h"
#import "XZB_Transition.h"
#import "DynamicApi.h"


@interface DiscoverDynamicVC ()<DZNEmptyDataSetSource,DZNEmptyDataSetDelegate,UINavigationControllerDelegate>{
    
    
}

@property(nonatomic,strong) UIButton *dynamicBtn;


@end

@implementation DiscoverDynamicVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.navigationController.delegate = self;
    
    
    [self.tableView registerClass:[DynamicCell class] forCellReuseIdentifier:@"Cell"];
    
    
    
//    self.tableView.emptyDataSetSource = self;
//    self.tableView.emptyDataSetDelegate = self;
    
    [self startRefresh];

//    [self.dynamicBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.view.mas_right).offset(-10);
//        make.bottom.equalTo(self.view.mas_bottom).offset(-10);
//        make.size.mas_equalTo(CGSizeMake(50, 50));
//    }];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addDynamic:) name:AddDynamciNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(removeDynamic:) name:RemoveDynamciNotification object:nil];
    
    
    // Do any additional setup after loading the view.
}

-(void)removeDynamic:(NSNotification *)notification{
    if ([notification.object isKindOfClass:[DynamicModel class]]) {
        [self.datas removeObject:notification.object];
        [self.tableView reloadData];
    }
}

-(void)addDynamic:(NSNotification *)notification{
    if ([notification.object isKindOfClass:[DynamicModel class]]) {
        if (self.datas.count == 0){
            self.datas = [NSMutableArray array];
        }
        [self.datas insertObject:notification.object atIndex:0];
        [self.tableView reloadData];
    }
}

-(void)dynamicBtnClick:(id)sender{
    [[RouteController sharedManager] openNewDynamicController];
}

-(UIButton *)dynamicBtn{
    if (!_dynamicBtn) {
        _dynamicBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_dynamicBtn setImage:[[UIImage imageNamed:@"ic_chat_add"] imageToColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        _dynamicBtn.backgroundColor = [UIColor orangeColor];
        _dynamicBtn.layer.masksToBounds = YES;
        _dynamicBtn.layer.cornerRadius = 25;
        [_dynamicBtn addTarget:self action:@selector(dynamicBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_dynamicBtn];
    }
    return _dynamicBtn;
}

-(void)startRefresh{
    self.page = 1;
    WEAK_SELF;
    [super startRefresh];
    [[[DynamicApi alloc]initWitObject:@{@"page":@(self.page),@"size":@(self.size)}] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        
        NSArray *array = [DynamicModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        [weakSelf refreshComplete:info response:array];
    }];
    
}

-(void)startloadMore{
    WEAK_SELF;
    [[[DynamicApi alloc]initWitObject:@{@"page":@(++self.page),@"size":@(self.size)}] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        
        NSArray *array = [DynamicModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        [weakSelf loadMoreComplete:info response:array];
    }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSLog(@"view appear");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -

#pragma mark <UINavigationControllerDelegate>
- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                   animationControllerForOperation:(UINavigationControllerOperation)operation
                                                fromViewController:(UIViewController *)fromVC
                                                  toViewController:(UIViewController *)toVC{
    
    if ([toVC isKindOfClass:NSClassFromString(@"DynamicDetailVC")]) {
        
        XZB_Transition *transition = [XZB_Transition transitionWithViewArray:@[@[@"tableView",@"dynamicView"]]];
        
        return transition;
    }else{
        return nil;
    }
}



#pragma mark -
#pragma mark  table view

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    id model = self.datas[indexPath.section];
    return [tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[DynamicCell class] contentViewWidth:APP_WIDTH];

    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self pushViewControllerWithName:@"DynamicDetailVC" params:self.datas[indexPath.section]];
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return .001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  self.datas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DynamicCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.model = self.datas[indexPath.section];
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
