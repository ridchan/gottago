//
//  SexView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/30.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "SexView.h"

@interface SexView()

@property(nonatomic,strong) UIImageView *imageView;

@end

@implementation SexView

@synthesize sexType = _sexType;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.imageView = [[UIImageView alloc]init];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.imageView];
    }
    return self;
}

-(void)setSexType:(SexType)sexType{
    _sexType = sexType;
    if (_sexType == Sex_Male){
        self.backgroundColor = RGB16(0x78d9ff);
        self.imageView.image = [UIImage imageNamed:@"ic_aboutme_male"];
    }else{
        self.backgroundColor = RGB16(0xffb4b5);
        self.imageView.image = [UIImage imageNamed:@"ic_aboutme_female"];
    }
}

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    
    self.layer.cornerRadius = self.frame.size.width / 2;
    self.layer.masksToBounds = YES;
    CGFloat inset = self.frame.size.width / 8;
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).insets(UIEdgeInsetsMake(inset, inset, inset, inset));
    }];

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
