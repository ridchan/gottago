//
//  TravelScheduleListApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface TravelScheduleListApi : BaseApi

-(id)initWithPage:(NSInteger)page size:(NSInteger)size;

@end
