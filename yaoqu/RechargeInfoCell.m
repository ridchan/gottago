//
//  RechargeInfoCell.m
//  QBH
//
//  Created by 陳景雲 on 2017/1/17.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "RechargeInfoCell.h"

@implementation RechargeInfoCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createViews];
    }
    return self;
}

-(void)createViews{
//    self.bgView = [[UIView alloc]init];
//    self.bgView.layer.masksToBounds = YES;
//    self.bgView.layer.cornerRadius = self.frame.size.height / 2;
//    self.bgView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    self.bgView.layer.borderWidth = 0.5;
//    [self addSubview:self.bgView];
//    
//    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.mas_top);
//        make.left.equalTo(self.mas_left);
//        make.bottom.equalTo(self.mas_bottom);
//        make.right.equalTo(self.mas_right);
//    }];
    
    self.imageView = [[UIImageView alloc]init];
    self.imageView.image = [UIImage imageNamed:@"bg_giftroll_gray"];
    [self addSubview:self.imageView];
    
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left);
        make.bottom.equalTo(self.mas_bottom);
        make.right.equalTo(self.mas_right);
    }];
    
    self.presendLbl = [ControllerHelper autoFitLabel];
    self.presendLbl.font = SystemFont(9);
    self.presendLbl.textColor = [UIColor whiteColor];
    self.presendLbl.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:self.presendLbl];
    [self.presendLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-1);
        make.top.equalTo(self.mas_top).offset(1);
    }];
    
    UIView *subView = [[UIView alloc]init];
    [self addSubview:subView];
    
    self.chargeLbl = [[UILabel alloc]init];
    self.chargeLbl.text = @"100积分";
    self.chargeLbl.textAlignment = NSTextAlignmentCenter;
    [subView addSubview:self.chargeLbl];
    
    self.giftImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_gift_roll"]];
    self.giftImage.contentMode = UIViewContentModeScaleAspectFit;
    [subView addSubview:self.giftImage];
    
    self.totalLbl = [[UILabel alloc]init];
    self.totalLbl.text = @"￥30.0";
    self.totalLbl.font = [UIFont systemFontOfSize:12];
    self.totalLbl.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.totalLbl];
    
    [self.giftImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(subView.mas_left);
        make.top.equalTo(subView.mas_top);
        make.bottom.equalTo(subView.mas_bottom);
        make.width.mas_equalTo(15);
    }];
    
    [self.chargeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.giftImage.mas_right).offset(2);
        make.top.equalTo(subView.mas_top);
        make.bottom.equalTo(subView.mas_bottom);
        make.right.equalTo(subView.mas_right);
    }];
    

    
    [subView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.bottom.equalTo(self.mas_centerY).offset(-2);
        make.height.mas_equalTo(15);
    }];

    [self.totalLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.mas_centerY).offset(2);
    }];
    
    
}

-(void)setModel:(CurrencyValueModel *)model{
    _model = model;
    
    self.chargeLbl.text = _model.giftroll;
    self.totalLbl.text = [NSString stringWithFormat:@"￥%0.2f",[_model.price floatValue]];
    self.presendLbl.text = [NSString stringWithFormat:@" %@ ",_model.give] ;
    self.presendLbl.hidden = [_model.give integerValue] == 0;
}

-(void)setBSelected:(BOOL)bSelected{
    _bSelected = bSelected;
    self.imageView.image = _bSelected ? [UIImage imageNamed:@"bg_giftroll_yellow"] : [UIImage imageNamed:@"bg_giftroll_gray"];
    
//    self.bgView.layer.borderWidth = bSelected ? 0.0 : 0.5;
//    self.bgView.backgroundColor = bSelected ? [UIColor orangeColor] : [UIColor clearColor];
    self.chargeLbl.textColor = bSelected ? AppOrange : AppBlack;
    self.totalLbl.textColor = bSelected ? AppOrange : [UIColor lightGrayColor];
}

@end
