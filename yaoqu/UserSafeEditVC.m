//
//  UserSafeEditVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/23.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserSafeEditVC.h"
#import "SRActionSheet.h"
#import "UserPhoneEditVC.h"
#import "BindAccountCell.h"
#import "RCUserCacheManager.h"
@interface UserSafeEditVC ()


@property(nonatomic,strong) NSString *phone;

@end

@implementation UserSafeEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.editModel = [RCUserCacheManager sharedManager].currentUser;
    
    self.title = LS(@"帐号与安全");
    self.datas = [@[
                    @[@{@"title":LS(@"QQ"),@"image":@"ic_qq_gray"},
                      @{@"title":LS(@"微信"),@"image":@"ic_wechat_gray"}
                      ]
                    ] mutableCopy];
    
//    @[@{@"title":LS(@"手机号码")},
//      @{@"title":LS(@"修改密码")}
//      ],
    self.phone = self.editModel.phone;
    
    self.tableView.separatorColor = AppLineColor;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section == 0 ? 20 : 20;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    if (section != 1) return  nil;
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 20)];
    UILabel *label = [[UILabel alloc]init];
    label.textColor = FontGray;
    label.font = [UIFont systemFontOfSize:12];
    label.textAlignment = NSTextAlignmentLeft;
    label.text = LS(@"绑定第三方");
    label.frame = CGRectMake(10, 0, APP_WIDTH - 20, 20);
    [view addSubview:label];
    return view;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.datas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.datas[section] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dict = self.datas[indexPath.section][indexPath.row];
    
    if (indexPath.section == 1) {
        NormalTableViewCell *cell = [NormalTableViewCell cellWithType:NormalCellType_Access_Detail identifier:@"Cell" tableView:tableView];
        
        cell.textLabel.text = dict[@"title"];
        if (indexPath.row == 0) {
            [cell rac_prepareForReuseSignal];
            RAC(cell.detailLbl,text) = RACObserve(self, phone);
        }
        
        return cell;
    }else{
        BindAccountCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"accountCell"];
        if (cell == nil) {
            cell = [[BindAccountCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        }
        cell.iconImage.image = [UIImage imageNamed:dict[@"image"]];
        cell.titlLbl.text = dict[@"title"];
        if (indexPath.row == 0){
            cell.isBindAccount = [self.editModel.qq_openid length] > 0;
            cell.type = UMSocialPlatformType_QQ;
        }else{
            cell.isBindAccount = ([self.editModel.openid length] > 0) || ([self.editModel.mp_openid length] > 0);
            cell.type = UMSocialPlatformType_WechatSession;
        }
        return cell;
    }

}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            WEAK_SELF;
            [[SRActionSheet sr_actionSheetViewWithTitle:nil cancelTitle:LS(@"取消") destructiveTitle:nil otherTitles:@[@"更换手机号码"] otherImages:nil selectSheetBlock:^(SRActionSheet *actionSheet, NSInteger index) {
                if (index == 0) {
                    [weakSelf pushViewControllerWithName:@"UserPhoneEditVC" params:weakSelf.editModel];
                }
            }] show];
        }else{
            [self pushViewControllerWithName:@"UserPasswordEditVC"];
        }
    }

}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
