//
//  ChatEmjoyView.h
//  shiyi
//
//  Created by ridchan on 16/6/24.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Emoji.h"

@interface ChatEmjoyCell : UICollectionViewCell

@property(nonatomic,strong) UILabel *label;
@property(nonatomic,strong) UIImageView *imageView;

@end


@protocol ChatEmjoyViewDelegate <NSObject>

@optional
-(void)chatEmjoyText:(NSString *)emjoy;
-(void)chatEmjoyBack;
-(void)chatEmjoySend;

@end



@interface ChatEmjoyView : UIView<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property(nonatomic,weak) id<ChatEmjoyViewDelegate> delegate;
@property(nonatomic,strong) NSMutableArray *emjoyArray;
@property(nonatomic,strong) UICollectionView *collectionView;

@property(nonatomic,strong) UIView *toolView;
@property(nonatomic,strong) UIPageControl *pageControl;
@property(nonatomic,strong) UIButton *backBtn;

@end
