//
//  UserImageView.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleUserModel.h"
#import "UserModel.h"

@interface UserImageView : UIView

@property(nonatomic,strong) UIImageView *userImg;
@property(nonatomic,strong) UILabel *levelLbl;
@property(nonatomic,strong) UIImageView *levelImg;
@property(nonatomic,strong) UIView *memberImg;

@property(nonatomic,strong) UserModel *userModel;


@property(nonatomic) BOOL isShowMember;
@property(nonatomic) BOOL isShowTalent;
@property(nonatomic) CGFloat boardWidth;

@end
