//
//  DynamicCommentListHeader.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/30.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DynamicCommentModel.h"

@interface DynamicCommentListHeader : UIView

@property(nonatomic,strong) DynamicCommentModel *model;

@end
