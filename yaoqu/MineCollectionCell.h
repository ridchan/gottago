//
//  MineCollectionCell.h
//  yaoqu
//
//  Created by ridchan on 2017/7/31.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineCollectionCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UILabel *label;
@property(nonatomic,strong) UILabel *detailLbl;

@end
