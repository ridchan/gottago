//
//  AccountInfoView.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/8.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginPostModel.h"

@interface AccountInfoView : UIView

@property(nonatomic,strong) LoginPostModel *postModel;
@property(nonatomic,strong) NormalButton *doneBtn;


@end
