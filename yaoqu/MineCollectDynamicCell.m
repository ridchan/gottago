//
//  MineCollectDynamicCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/24.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineCollectDynamicCell.h"
#import "UserImageView.h"
#import "SexAgeView.h"

@interface MineCollectDynamicCell()

@property(nonatomic,strong) UserImageView *userImage;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) SexAgeView *sexView;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UIImageView *contentImage;


@end

@implementation MineCollectDynamicCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(DynamicModel *)model{
    _model = model;
    self.userImage.userModel = [model objectWithClass:@"UserModel"];
    self.nameLbl.text = model.username;
    self.sexView.sex = [model.sex integerValue];
    self.sexView.age = [model.age integerValue];
    self.descLbl.text = model.desc;
    [self.contentImage setImageName:model.image placeholder:nil];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    [self.userImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-10);
        make.top.equalTo(self.contentView.mas_top).offset(10);
        make.width.mas_equalTo(self.userImage.mas_height);
    }];
    
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userImage.mas_right).offset(10);
        make.top.equalTo(self.userImage.mas_top);
    }];
    
    [self.sexView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userImage.mas_right).offset(5);
        make.centerY.equalTo(self.userImage.mas_centerY);
        make.height.mas_equalTo(15);
    }];
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.userImage.mas_centerY);
        make.right.equalTo(self.contentImage.mas_left).offset(-10);
    }];
    
    [self.contentImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-10);
        make.top.equalTo(self.contentView.mas_top).offset(10);
        make.width.mas_equalTo(self.userImage.mas_height);
    }];
}

-(UserImageView *)userImage{
    if (!_userImage) {
        _userImage = [[UserImageView alloc]init];
        [self.contentView addSubview:_userImage];
    }
    return _userImage;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        _nameLbl.font = SystemFont(14);
        [self.contentView addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(SexAgeView *)sexView{
    if (!_sexView) {
        _sexView = [[SexAgeView alloc]init];
        [self.contentView addSubview:_sexView];
    }
    return _sexView;
}

-(UIImageView *)contentImage{
    if (!_contentImage) {
        _contentImage = [[UIImageView alloc]init];
        _contentImage.contentMode = UIViewContentModeScaleAspectFill;
        _contentImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.contentView addSubview:_contentImage];
    }
    return _contentImage;
}

@end
