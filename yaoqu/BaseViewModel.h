//
//  BaseViewModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/16.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReactiveCocoa.h"

@interface BaseViewModel : NSObject

-(void)initialize;

@property(nonatomic,strong) RACSubject *succSubject;
@property(nonatomic,strong) RACSubject *failSubject;
@property(nonatomic,strong) RACSubject *erroSUbject;


@end
