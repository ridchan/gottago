//
//  SOAComponentAppDelegate.m
//  shiyi
//
//  Created by 陳景雲 on 16/5/2.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import "SOAComponentAppDelegate.h"


#import "SettingServices.h"
#import "EasemobServices.h"
#import "UMengServices.h"
#import "AliPayServices.h"
#import "TencentServices.h"

@implementation SOAComponentAppDelegate
{
    NSMutableArray* allServices;
}

#pragma mark - 服务静态注册

//需要运行程序之前，手工增加根据需要的新服务

-(void)registeServices
{

    [self registeService:[[EasemobServices alloc]init]];
    [self registeService:[[SettingServices alloc]init]];
    [self registeService:[[UMengServices alloc]init]];
    [self registeService:[[AliPayServices alloc]init]];
    [self registeService:[[TencentServices alloc]init]];
    
}

#pragma mark - 获取SOAComponent单实例

+ (instancetype)instance {
    
    static SOAComponentAppDelegate *insance = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        insance = [[SOAComponentAppDelegate alloc] init];
    });
    
    
    return insance;
}

#pragma mark - 获取全部服务

-(NSMutableArray *)services
{
    
    if (!allServices) {
        allServices = [[NSMutableArray alloc]init];
        [self registeServices];
    }
    
    return allServices;
}

#pragma mark - 服务动态注册

-(void)registeService:(id)service
{
    if (![allServices containsObject:service])
    {
        [allServices addObject:service];
    }
    
}

@end
