//
//  MineCollectTravelVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/24.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineCollectTravelVC.h"
#import "CollectApi.h"
#import "MineCollectTravelCell.h"
#import "TravelModel.h"
@interface MineCollectTravelVC ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation MineCollectTravelVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[MineCollectTravelCell class] forCellReuseIdentifier:@"Cell"];
    [self startRefresh];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)startRefresh{
    
    [super startRefresh];
    
    WEAK_SELF;
    
    self.page = 1;
    NSDictionary *dict = @{@"type":@(CollectType_Travel),
                           @"page":@(self.page),
                           @"size":@(self.size)
                           };
    
    
    
    
    [[[CollectApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        NSMutableArray *array = nil;
        if ([responseObj isKindOfClass:[NSDictionary class]]){
            array = [TravelModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        }
        
        [weakSelf refreshComplete:info response:array];
    }];
}


-(void)startloadMore{
    WEAK_SELF;
    NSDictionary *dict = @{@"type":@(CollectType_Travel),
                           @"page":@(++self.page),
                           @"size":@(self.size)
                           };
    
    [[[CollectApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        NSMutableArray *array = nil;
        if ([responseObj isKindOfClass:[NSDictionary class]]){
            array = [TravelModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@""]];
        }
        [weakSelf loadMoreComplete:info response:array];
    }];
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 200;
    //    id model = self.datas[indexPath.section];
    //    return [tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[DiscoverRankListCell class] contentViewWidth:APP_WIDTH];
}





-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.datas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MineCollectTravelCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.model = self.datas[indexPath.section];
    return cell;
}

@end
