//
//  DynamicUploadManager.m
//  yaoqu
//
//  Created by ridchan on 2017/6/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicUploadManager.h"
#import "QinniuCodeApi.h"
#import "QiniuSDK.h"
#import "NSString+Extend.h"
#import "UserInfoGetApi.h"
#import "DynamicListApi.h"
#import "UploadObject.h"
#import "CommonApi.h"
#import "DynamicApi.h"
#import "ApproveApi.h"
#import "LLUtils.h"
#import "MemberApi.h"

@interface DynamicUploadManager(){
    NSMutableArray *myDynamic;
    NSMutableArray *allDynamic;
}

@end

@implementation DynamicUploadManager

CREATE_SHARED_MANAGER(DynamicUploadManager)

- (instancetype)init
{
    self = [super init];
    if (self) {
        myDynamic = [NSMutableArray array];
        allDynamic = [NSMutableArray array];
    }
    return self;
}


//发布动态
-(void)putDynamicModel:(DynamicUploadModel *)upModel block:(BaseBlock)block{
    
    __block MBProgressHUD *hud = [LLUtils showActivityIndicatiorHUDWithTitle:nil];
    
    __block  NSString *token = nil;
    [[[CommonApi alloc]initWithModel:@"common/uptoken" object:nil]startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None){
            token = [responseObj objectForKey:@"data"];
        }else{
            token = @"";
        }
    }];
    
    while (token == nil) {
        [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    
    if (upModel.is_video) {
        UploadObject *upObj = upModel.imageDatas[0];
        
//        __block NSString *imageName = nil;
//        __block NSMutableArray *imageNames = [NSMutableArray array];
        
        NSString *key = [[NSString randomString] stringByAppendingString:@".jpg"];
        [[[QNUploadManager alloc]init] putData:UIImageJPEGRepresentation(upObj.image,1.0) key:key token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
            
//            imageName = key;
//            [imageNames addObject:key];
            upModel.video_image = key;
        } option:nil];
        
        while (upModel.video_image == nil) {
            [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }
        
        __block  NSString *video_token = nil;
        [[[CommonApi alloc]initWithModel:@"common/uptoken" object:nil]startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            if (info.error == ErrorCodeType_None){
                video_token = [responseObj objectForKey:@"data"];
            }else{
                video_token = @"";
            }
        }];
        
        while (video_token == nil) {
            [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }

        
        key = [[NSString randomString] stringByAppendingString:@".mp4"];
        [[[QNUploadManager alloc]init] putFile:upObj.videoUrl key:key token:video_token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
            NSLog(@"上传视频,%@",info.error.description);
            upModel.video = key;
        } option:nil];
        
        while (upModel.video == nil) {
            [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }


    }else{
        WEAK_SELF;
        __block NSMutableArray *imageNames = [NSMutableArray array];
        __block NSInteger i = 0;
        for (UploadObject *upObj in upModel.imageDatas){
            NSString *key = [[NSString randomString] stringByAppendingString:@".jpg"];
            __block NSString *imageName = nil;
            
            [[[QNUploadManager alloc]init] putData:[weakSelf imageData:upObj.image] key:key token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
                if (!info.error) {
                    imageName = key;
                    [imageNames addObject:imageName];
                }else{
                    imageName = @"";
                }
                NSLog(@"error %@",info.error.description);
                
                i  ++;
            } option:nil];
        }
        
        while (i < upModel.imageDatas.count) {
            [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }
        
        if ([imageNames count] > 0){
            upModel.images = [imageNames JSONString];
            MemberApi *api = [[MemberApi alloc]initWitObject:@{@"recent_image_data":upModel.images}];
            api.method = YTKRequestMethodPOST;
            [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                
            }];
        }
    
    
    }
    NSLog(@"上传图片数量 %@",upModel.images);
    upModel.imageDatas = nil;
    
    DynamicApi *api = [[DynamicApi alloc]initWitObject:upModel];
    api.method = YTKRequestMethodPOST;
    
    
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [hud hideAnimated:NO];
        
        if (info.error == ErrorCodeType_None) {
            
            block(responseObj);
            [LLUtils showActionSuccessHUD:@""];
        }else{
            [LLUtils showCenterTextHUD:info.message];
        }
    }];

}


//上传认证

-(void)putApprove:(ApproveUploadModel *)upModel block:(BaseBlock)block{

    
    __block MBProgressHUD *hud = [LLUtils showActivityIndicatiorHUDWithTitle:@"提交中..."];
    __block NSString *token = nil;
    [[[CommonApi alloc]initWithModel:@"common/uptoken" object:nil]startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None){
            token = [responseObj objectForKey:@"data"];
        }else{
            token = @"";
        }
    }];
    
    while (token == nil) {
        [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    
    
    
    
    NSString *key1 = [[NSString randomString] stringByAppendingString:@".jpg"];
    [[[QNUploadManager alloc]init] putData:UIImageJPEGRepresentation(upModel.obverseImage,1.0) key:key1 token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        if (!info.error) {
            upModel.obverse = key;
        }else{
            upModel.obverse = @"";
        }
        
    } option:nil];
    
    
    NSString *key2 = [[NSString randomString] stringByAppendingString:@".jpg"];
    [[[QNUploadManager alloc]init] putData:UIImageJPEGRepresentation(upModel.positiveImage,1.0) key:key2 token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        if (!info.error) {
            upModel.positive = key;
        }else{
            upModel.positive = @"";
        }
        
    } option:nil];

    
    
    while (upModel.positive == nil || upModel.obverse == nil) {
        [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    
    
    upModel.positiveImage = nil;
    upModel.obverseImage = nil;
    
    
    ApproveApi *api = [[ApproveApi alloc]initWitObject:upModel];
    api.method = YTKRequestMethodPOST;
    
    
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        block(info);
        [hud hideAnimated:NO];
        [LLUtils showActionSuccessHUD:info.message];
    }];
    
}


//发布视频
-(void)putDynamicVedio:(ALAsset *)asset{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        __block  NSString *token = nil;
        [[[QinniuCodeApi alloc]init]startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            if (info.state){
                token = [responseObj objectForKey:@"data"];
            }else{
                token = @"";
            }
        }];
        
        while (token == nil) {
            [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }
        
        [[[QNUploadManager alloc]init] putALAsset:asset key:@"aaa.mov" token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
            NSLog(@"上传视频,%@",info.error.description);
        } option:nil];
        
        
    });

}


- (UIImage*)imageWithImage:(UIImage*)image scaledToSize: (CGSize)newSize
{
    //下面方法，第一个参数表示区域大小。第二个参数表示是否是非透明的。如果需要显示半透明效果，需要传NO，否则传YES。第三个参数就是屏幕密度了
    UIGraphicsBeginImageContextWithOptions(newSize, NO, [UIScreen mainScreen].scale);

    
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledImage;   //返回的就是已经改变的图片
}


-(NSData *)imageData:(UIImage *)myimage{
    NSData *data=UIImageJPEGRepresentation(myimage, 1.0);
    if (data.length>100*1024) {
        if (data.length>1024*1024) {//1M以及以上
            data=UIImageJPEGRepresentation(myimage, 0.1);
        }else if (data.length>512*1024) {//0.5M-1M
            data=UIImageJPEGRepresentation(myimage, 0.5);
        }else if (data.length>200*1024) {//0.25M-0.5M
            data=UIImageJPEGRepresentation(myimage, 0.9);
        }
    }
    return data;
}
    




-(void)putImage:(id)asset block:(BaseBlock)block{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        __block  NSString *token = nil;
        [[[CommonApi alloc]initWithModel:@"common/uptoken" object:nil] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            if (info.error == ErrorCodeType_None){
                token = [responseObj objectForKey:@"data"];
            }else{
                token = @"";
            }
        }];
        
        while (token == nil) {
            [[NSRunLoop currentRunLoop]runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }
        
        NSString *key = [NSString stringWithFormat:@"%@.jpg",[NSString randomString]];
        [[[QNUploadManager alloc]init] putData:[self imageData:asset] key:key token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
            if (!info.error) {
                block(key);
                
            }else{
                block(nil);
            }
            NSLog(@"上传 %@",info.error.description);
        } option:nil];
        
        
        
    });

}



-(void)getAllDynamicList:(NSInteger)page block:(BaseBlock)block{
    WEAK_SELF;
    [[[DynamicListApi alloc]initWithMemberID:nil page:page size:20] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.state) {
            if (page == 1) [allDynamic removeAllObjects];
            NSArray *array = [DynamicModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
            [allDynamic addObjectsFromArray:array];
            weakSelf.allDynamicList = allDynamic;
        }
        if (block) block(nil);
    }];

}



-(void)getMyDynamicLis:(NSString *)member_id page:(NSInteger)page block:(BaseBlock)block{
    WEAK_SELF;
    [[[DynamicListApi alloc]initWithMemberID:member_id page:page size:20] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.state) {
            if (page == 1) [myDynamic removeAllObjects];
            NSArray *array = [DynamicModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
            
            [myDynamic addObjectsFromArray:array];
            weakSelf.myDynamicList = myDynamic;
            weakSelf.dynamicImages = [weakSelf resloveDynamicImage];
            
        }
        if (block) block(nil);
    }];
}

-(NSMutableArray *)resloveDynamicImage{
    NSMutableArray *images = [NSMutableArray array];
    for (DynamicModel *model in self.myDynamicList){
        NSArray *imgs = [model.content_images JSONObject];
        if ([imgs count] > 0) {
            [images addObject:imgs[0]];
        }
    }
    return images;
}

//
                   


//压缩

-(NSData *)expressImage:(UIImage *)image size:(CGFloat)size{
    CGFloat length = 0.0;
    CGFloat flactor = 1.0;
    NSData *data = UIImageJPEGRepresentation(image, flactor);
    length = data.length;
    while(length > size){
        data = UIImageJPEGRepresentation(image, flactor);
        length = data.length;
        flactor -= 0.05;
    }
    
    return data;
}



@end
