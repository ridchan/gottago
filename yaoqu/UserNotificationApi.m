//
//  UserNotificationApi.m
//  yaoqu
//
//  Created by ridchan on 2017/7/23.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserNotificationApi.h"

@implementation UserNotificationApi

-(id)initWithPage:(NSInteger)page size:(NSInteger)size{
    if (self = [super init]) {
        [self.params setValue:@(page) forKey:@"page"];
        [self.params setValue:@(size) forKey:@"size"];
    }
    return self;
}

-(id)initWithNotificationID:(NSString *)notification_id{
    if (self = [super init]) {
        [self.params setValue:notification_id forKey:@"message_system_id"];
    }
    return self;
}

-(NSString *)requestUrl{
    switch (self.apiType) {
        case APIType_List:
            return MessageCenterNotificationListUrl;
        case APIType_Delete:
            return MessageCenterNotificationDelUrl;
        case APIType_Clear:
            return MessageCenterNotificationClearUrl;
        default:
            break;
    }
    return nil;
    
}

@end
