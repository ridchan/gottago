//
//  CommonApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/1.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "CommonApi.h"

@implementation CommonApi{
    NSString *_model;
}



-(id)initWithModel:(NSString *)model object:(id)obj{
    if (self = [super initWitObject:obj]) {
        _model = model;
    }
    return self;
}

-(NSString *)requestUrl{
    return _model;
}

@end
