//
//  RightBarButtonItem.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "RightBarButtonItem.h"

@implementation RightBarButtonItem

-(id)initWithTitle:(NSString *)title{
    if (self = [super init]) {
        
    }
    return self;
}



+ (RightBarButtonItem *)itemRightItemWithIcon:(UIImage *)icon {
    RightBarButtonItem *item;
    if (!icon) {
        item = [[RightBarButtonItem new] initWithCustomView:[UIView new]];
        return item;
    }
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = [UIColor clearColor];
    float leight = icon.size.width;
    [btn setImage:icon forState:UIControlStateNormal];
    [btn setImage:icon forState:UIControlStateHighlighted];
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5);
    [btn setFrame:CGRectMake(0, 0, leight, 30)];
    
    item = [[RightBarButtonItem alloc] initWithCustomView:btn];
    item.btn = btn;
    return item;
}

+ (RightBarButtonItem *)itemRightItemWithTitle:(NSString *)title{
    RightBarButtonItem *item;
    if (title.length == 0) {
        item = [[RightBarButtonItem new] initWithCustomView:[UIView new]];
        return item;
    }
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = [UIColor clearColor];
    btn.titleLabel.font = [UIFont systemFontOfSize:16];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;

    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateHighlighted];
    [btn setTitleColor:AppTitleColor forState:UIControlStateNormal];
    [btn setTitleColor:AppTitleColor forState:UIControlStateHighlighted];
    CGSize titleSize = [title ex_sizeWithFont:btn.titleLabel.font constrainedToSize:CGSizeMake(APP_WIDTH, MAXFLOAT)];
    float leight = titleSize.width;
    [btn setFrame:CGRectMake(0, 0, leight, 30)];
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5);
    item = [[RightBarButtonItem alloc] initWithCustomView:btn];
    item.btn = btn;
    return item;
}


-(void)setTitleColor:(UIColor *)color{
    [self.btn setTitleColor:color forState:UIControlStateNormal];
    [self.btn setTitleColor:color forState:UIControlStateHighlighted];
}

-(void)setImageColor:(UIColor *)color{
    UIImage *newImage = [self.btn.imageView.image imageToColor:color];
    [self.btn setImage:newImage forState:UIControlStateNormal];
    [self.btn setImage:newImage forState:UIControlStateHighlighted];
}


@end
