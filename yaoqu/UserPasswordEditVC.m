//
//  UserPasswordEditVC.m
//  yaoqu
//
//  Created by ridchan on 2017/8/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserPasswordEditVC.h"
#import "NormalTextField.h"
#import "RCUserCacheManager.h"
@interface UserPasswordEditVC ()

@property(nonatomic,strong) UIView *headerView;
@property(nonatomic,strong) NormalTextField *phoneFld;
@property(nonatomic,strong) NormalTextField *codeFld;
@property(nonatomic,strong) UIButton *codeBtn;
@property(nonatomic) NSInteger countTime;

@property(nonatomic,strong) UIView *passwordView;
@property(nonatomic,strong) NormalTextField *psdFld1;
@property(nonatomic,strong) NormalTextField *psdFld2;

@property(nonatomic,strong) UIButton *comfirnBtn;

@end


@implementation UserPasswordEditVC



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self commonInit];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)doneBtnClick:(id)sender{
    
}

-(void)commonInit{
//    [self setRightItemWithTitle:LS(@"完成") selector:@selector(doneBtnClick:)];
    
    self.title = LS(@"修改密码");
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(70);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.height.mas_equalTo(100);
    }];
    
    [self.passwordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerView.mas_bottom).offset(10);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.height.mas_equalTo(100);
    }];
    
    [self.comfirnBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passwordView.mas_bottom).offset(20);
        make.left.equalTo(self.view.mas_left).offset(20);
        make.right.equalTo(self.view.mas_right).offset(-20);
        make.height.mas_equalTo(50);
    }];
}

#pragma mark -

-(UIView *)headerView{
    if (!_headerView) {
        _headerView = [[UIView alloc]init];
        _headerView.backgroundColor = [UIColor whiteColor];
        [self.phoneFld mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.headerView.mas_top);
            make.left.equalTo(self.headerView.mas_left).offset(10);
            make.right.equalTo(self.headerView.mas_right);
            make.height.mas_equalTo(50);
        }];
        
        [self.codeFld mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.phoneFld.mas_bottom);
            make.left.equalTo(self.headerView.mas_left).offset(10);
            make.right.equalTo(self.headerView.mas_right);
            make.height.mas_equalTo(50);
        }];
        
        [self.view addSubview:_headerView];
        
    }
    return _headerView;
}


-(NormalTextField *)phoneFld{
    if (!_phoneFld) {
        _phoneFld = [[NormalTextField alloc]init];
        _phoneFld.backgroundColor = [UIColor whiteColor];
        _phoneFld.placeholder = LS(@"手机号码");
        _phoneFld.title = LS(@"手机号码");
        _phoneFld.centerLine.hidden = YES;
        _phoneFld.enabled = NO;
        _phoneFld.text = [RCUserCacheManager sharedManager].currentUser.phone;
        [self.headerView addSubview:_phoneFld];
    }
    return _phoneFld;
}

-(NormalTextField *)codeFld{
    if (!_codeFld) {
        _codeFld = [[NormalTextField alloc]init];
        _codeFld.placeholder = LS(@"验证码");
        _codeFld.title = LS(@"验证码");
        _codeFld.centerLine.hidden = YES;
        _codeFld.rightView = [self rightView];
        _codeFld.backgroundColor = [UIColor whiteColor];
        _codeFld.rightViewMode = UITextFieldViewModeAlways;
        [self.headerView addSubview:_codeFld];
    }
    return _codeFld;
}

-(UIView *)rightView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 85, 40)];
    
    [view addSubview:self.codeBtn];
    
    [self.codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view.mas_right).offset(-5);
        make.centerY.equalTo(view.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(80, 25));
    }];
    
    return view;
}

-(UIButton *)codeBtn{
    if (!_codeBtn) {
        _codeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_codeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _codeBtn.backgroundColor = [UIColor orangeColor];
        _codeBtn.titleLabel.font = SystemFont(12);
        [_codeBtn addTarget:self action:@selector(codeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_codeBtn setTitle:LS(@"获取验证码") forState:UIControlStateNormal];
    }
    return _codeBtn;
}


#pragma mark -


-(UIView *)passwordView{
    if (!_passwordView) {
        _passwordView = [[UIView alloc]init];
        _passwordView.backgroundColor = [UIColor whiteColor];
        [self.psdFld1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.passwordView.mas_top);
            make.left.equalTo(self.passwordView.mas_left).offset(10);
            make.right.equalTo(self.passwordView.mas_right);
            make.height.mas_equalTo(50);
        }];
        
        [self.psdFld2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.psdFld1.mas_bottom);
            make.left.equalTo(self.passwordView.mas_left).offset(10);
            make.right.equalTo(self.passwordView.mas_right);
            make.height.mas_equalTo(50);
        }];
        
        [self.view addSubview:_passwordView];
        
    }
    return _passwordView;
}


-(NormalTextField *)psdFld1{
    if (!_psdFld1) {
        _psdFld1 = [[NormalTextField alloc]init];
        _psdFld1.backgroundColor = [UIColor whiteColor];
        _psdFld1.title = LS(@"新密码");
        _psdFld1.placeholder = LS(@"6 - 18位字符");
        _psdFld1.secureTextEntry = YES;
        _psdFld1.centerLine.hidden = YES;
        [self.passwordView addSubview:_psdFld1];
    }
    return _psdFld1;
}

-(NormalTextField *)psdFld2{
    if (!_psdFld2) {
        _psdFld2 = [[NormalTextField alloc]init];
        _psdFld2.title = LS(@"确认密码");
        _psdFld2.placeholder = LS(@"再一次输入密码");
        _psdFld2.secureTextEntry = YES;
        _psdFld2.centerLine.hidden = YES;
        _psdFld2.backgroundColor = [UIColor whiteColor];
        [self.passwordView addSubview:_psdFld2];
    }
    return _psdFld2;
}

#pragma mark -

-(UIButton *)comfirnBtn{
    if (!_comfirnBtn) {
        _comfirnBtn = [NormalButton normalButton:LS(@"完成")];
        [self.view addSubview:_comfirnBtn];
    }
    return _comfirnBtn;
}



-(void)codeBtnClick:(id)sender{
    
    //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    self.countTime  = 60;
    while (self.countTime > 0) {
        self.countTime --;
        NSLog(@"i %ld",self.countTime);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.codeBtn setTitle:[NSString intValue:self.countTime] forState:UIControlStateNormal];
        });
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:1]];
        
        [_codeBtn setTitle:LS(@"获取验证码") forState:UIControlStateNormal];
    }
    //    });
}

-(void)dealloc{
    self.countTime = 0;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
