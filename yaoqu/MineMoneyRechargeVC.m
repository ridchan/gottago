//
//  MineMoneyRechargeVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/9.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineMoneyRechargeVC.h"
#import "TravelPayTypeView.h"
#import "RechargeApi.h"
#import "LLUtils.h"
#import <AlipaySDK/AlipaySDK.h>
#import "RechargeModel.h"

@interface MineMoneyRechargeVC ()

@property(nonatomic,strong) UIView *whiteView;
@property(nonatomic,strong) UILabel *chargeLbl;
@property(nonatomic,strong) UITextField *payFld;
@property(nonatomic,strong) UIView *line;
@property(nonatomic,strong) UIButton *payBtn;

@end

@implementation MineMoneyRechargeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"余额充值");
    
    
    [self setRightItemWithTitle:LS(@"充值记录") selector:@selector(recordBtnClick:)];
    [self setLeftItemWithIcon:nil title:LS(@"取消") selector:@selector(cancelBtnClick:)];
    [(RightBarButtonItem *)self.navigationItem.rightBarButtonItem setTitleColor:[UIColor orangeColor]];
    [(LeftBarButtonItem *)self.navigationItem.leftBarButtonItem setTitleColor:[UIColor orangeColor]];
    
    
    [self.whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(20);
        make.right.equalTo(self.view.mas_right).offset(-20);
        make.top.equalTo(self.view.mas_top).offset(100);
    }];

    // Do any additional setup after loading the view.
}

-(void)cancelBtnClick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)recordBtnClick:(id)sender{
    [self pushViewControllerWithName:@"MineRecordVC" params:@(RechargeType_Money)];
}


-(void)payBtnClick:(id)sender{
    
    RechargeModel *model = [[RechargeModel alloc]init];
    model.title_desc = LS(@"余额充值");
    model.title_value = self.payFld.text;
    model.val = self.payFld.text;
    model.pay_total = self.payFld.text;
    model.type = [NSString intValue:RechargeType_Money];
    model.actionType = [NSString intValue:RechargeType_Money];
    
    
    [self pushViewControllerWithName:@"TripCoinPayVC" params:model];
    
//    return;
//    
//    [TravelPayTypeView showInView:self.view compelteBlock:^(id responseObj) {
//        NSDictionary *info = @{@"pay_type":[responseObj stringValue],
//                               @"val":self.payFld.text,
//                               @"type":@(RechargeType_Money)
//                               };
//        __block MBProgressHUD *hud = [LLUtils showActivityIndicatiorHUDWithTitle:@"支付中..."];
//        RechargeApi *api = [[RechargeApi alloc]initWitObject:info];
//        api.method = YTKRequestMethodPOST;
//        [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
//            
//            
//            [hud hideAnimated:NO afterDelay:0];
//            
////            [LLUtils showActionSuccessHUD:@"支付成功"];
//            
//            [[AlipaySDK defaultService] payOrder:[[responseObj objectForKey:@"data"] objectForKey:@"pay_data"] fromScheme:@"yaoqu" callback:^(NSDictionary *resultDic) {
//            }];
//
//        }];
//    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIView *)whiteView{
    if (!_whiteView) {
        _whiteView = [[UIView alloc]init];
        _whiteView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_whiteView];
        
        
        [self.chargeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_whiteView.mas_left).offset(20);
            make.top.equalTo(_whiteView.mas_top).offset(20);
        }];
        
        [self.payFld mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_whiteView.mas_left).offset(20);
            make.top.equalTo(self.chargeLbl.mas_bottom).offset(10);
            make.right.equalTo(_whiteView.mas_right).offset(-20);
            make.height.mas_equalTo(40);
        }];
        
        [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_whiteView.mas_left).offset(20);
            make.top.equalTo(self.payFld.mas_bottom).offset(10);
            make.right.equalTo(_whiteView.mas_right).offset(-20);
            make.height.mas_equalTo(0.5);
        }];
        
        [self.payBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_whiteView.mas_left).offset(20);
            make.top.equalTo(self.line.mas_bottom).offset(20);
            make.right.equalTo(_whiteView.mas_right).offset(-20);
            make.height.mas_equalTo(50);
            make.bottom.equalTo(_whiteView.mas_bottom).offset(-20);
        }];
        
    }
    return _whiteView;
}

-(UILabel *)chargeLbl{
    if (!_chargeLbl) {
        _chargeLbl = [ControllerHelper autoFitLabel];
        _chargeLbl.font = DetailFont;
        _chargeLbl.text = LS(@"充值金额");
        _chargeLbl.textColor = AppGray;
        [self.whiteView addSubview:_chargeLbl];
    }
    return _chargeLbl;
}

-(UITextField *)payFld{
    if (!_payFld) {
        _payFld = [[UITextField alloc]init];
        _payFld.font = SystemFont(28);
//        _payFld.keyboardType = UIKeyboardTypeNumberPad;
        
        UILabel *label = [ControllerHelper autoFitLabel];
        label.textColor = [UIColor orangeColor];
        label.font = SystemFont(14);
        label.text = MoneySign;
        label.frame = CGRectMake(0, 5, 10, 20);
        
        UIView *leftView = [[UIView alloc]init];
        leftView.frame = CGRectMake(0, 0, 20, 40);
        [leftView addSubview:label];
        
        _payFld.leftView = leftView;
        _payFld.leftViewMode = UITextFieldViewModeAlways;
        
        [self.whiteView addSubview:_payFld];
    }
    
    return _payFld;
}


-(UIView *)line{
    if (!_line) {
        _line = [[UIView alloc]init];
        _line.backgroundColor = AppLineColor;
        [self.whiteView addSubview:_line];
    }
    return _line;
}

-(UIButton *)payBtn{
    if (!_payBtn) {
        _payBtn = [NormalButton normalButton:LS(@"立即充值")];
        [_payBtn addTarget:self action:@selector(payBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.whiteView addSubview:_payBtn];
    }
    return _payBtn;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
