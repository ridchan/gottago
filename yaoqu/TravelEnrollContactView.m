//
//  TravelEnrollContactView.m
//  yaoqu
//
//  Created by ridchan on 2017/7/17.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelEnrollContactView.h"
#import "TravelContactModel.h"
#import "TravelContactListVC.h"
#import "RouteController.h"

@interface TravelEnrollContactView()<UITableViewDelegate,UITableViewDataSource,TravelContactListVCDelegate>

@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) UIButton *btn;

@end

@implementation TravelEnrollContactView


- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
        self.contacts = [NSMutableArray array];
        self.backgroundColor = [UIColor whiteColor];
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
//        [self addGestureRecognizer:tap];
    }
    return self;
}

-(void)tap:(id)sender{
    TravelContactListVC *vc = [[TravelContactListVC alloc]init];
    vc.delegate = self;
    [[RouteController sharedManager] pushVC:vc];
}

-(void)didSelectContact:(id)obj{
    [self.contacts addObject:obj];
    self.contactNum = [NSString intValue:self.contacts.count];
    [self.tableView reloadData];
    [self resetTableView];
}

-(void)resetTableView{
    CGFloat height = 40 * self.contacts.count;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.titleLbl.mas_bottom);
        make.height.mas_equalTo(height);
        make.bottom.equalTo(self.mas_bottom);
    }];
}

-(void)commonInit{
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.mas_top);
        make.height.mas_equalTo(40);
    }];
    
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.top.equalTo(self.mas_top);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(RightAccessWidth);
        
    }];
    
    [self.btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.top.equalTo(self.mas_top);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(40);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.titleLbl.mas_bottom);
        make.height.mas_equalTo(0);
        make.bottom.equalTo(self.mas_bottom);
    }];
}

-(UIButton *)btn{
    if (!_btn) {
        _btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btn addTarget:self action:@selector(tap:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_btn];
    }
    return _btn;
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorColor = AppLineColor;
        [self addSubview:_tableView];
    }
    return _tableView;
}

-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        _titleLbl.text = LS(@"添加联系人");
        [self addSubview:_titleLbl];
    }
    return _titleLbl;
}

-(UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_forward"]];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_imageView];
    }
    return _imageView;
}


#pragma mark -
#pragma mark tableview

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0001;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.contacts.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    TravelContactModel *model = self.contacts[indexPath.row];
    cell.textLabel.text = model.fullname;
    cell.detailTextLabel.text = model.phone;
    
    return cell;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
