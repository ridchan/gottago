//
//  LLMessageStrangerCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/9.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LLMessageModel.h"

@protocol LLMessageStrangerCellDelegate <NSObject>

-(void)strangerCellAgreeBtnClick:(id)sender;

-(void)strangerCellBlackBtnClick:(id)sender;



@end

@interface LLMessageStrangerCell : UITableViewCell

@property (nonatomic) LLMessageModel *messageModel;
@property (nonatomic) id<LLMessageStrangerCellDelegate>strangerDelegate;

+ (CGFloat)heightForModel:(LLMessageModel *)model;

@end
