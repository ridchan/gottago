//
//  LLMessageArticleCell.m
//  QBH
//
//  Created by 陳景雲 on 2017/2/27.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "LLMessageArticleCell.h"
#import "LLConfig.h"
#import "LLUtils.h"
#import "LLColors.h"

#define LABEL_BUBBLE_LEFT 12
#define LABEL_BUBBLE_RIGHT 12
#define LABEL_BUBBLE_TOP 14
#define LABEL_BUBBLE_BOTTOM 12

#define CONTENT_MIN_WIDTH  53
#define CONTENT_MIN_HEIGHT 41


@interface LLMessageArticleCell()

@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UIImageView *contentImage;

@end

@implementation LLMessageArticleCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _contentImage = [[UIImageView alloc] initWithFrame:CGRectMake(60, 60, 40, 40)];
        _contentImage.contentMode = UIViewContentModeScaleAspectFit;
        [self.bubbleImage addSubview:_contentImage];
        
        _nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 5 , 100, 20)];
        _nameLbl.font = [UIFont systemFontOfSize:14];
        _nameLbl.textColor = kLLTextColor_lightBlack;
        _nameLbl.textAlignment = NSTextAlignmentLeft;
        [self.bubbleImage addSubview:_nameLbl];
        
        
        _descLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 40, 60, 40)];
        _descLbl.font = [UIFont systemFontOfSize:12];
        _descLbl.textColor = kLLTextColor_lightGray_7;
        _descLbl.numberOfLines = 3;
        _descLbl.textAlignment = NSTextAlignmentLeft;
        [self.bubbleImage addSubview:_descLbl];


        
        self.bubbleImage.frame = CGRectMake(0, -BUBBLE_TOP_BLANK + 5, 100, AVATAR_HEIGHT + BUBBLE_TOP_BLANK + BUBBLE_BOTTOM_BLANK);
    }
    return self;
}

-(void)prepareForUse:(BOOL)isFromMe{
    [super prepareForUse:isFromMe];
    self.bubbleImage.image = isFromMe ? SenderArticelBkg : ReceiverArticelBkg;
}

-(void)setSubViewFrame{
    _contentImage.frame = CGRectMake(20, self.bubbleImage.frame.size.height - 70, 50, 50);
    _nameLbl.frame = CGRectMake(20, 10, self.bubbleImage.frame.size.width - 40, 20);
    _descLbl.frame = CGRectMake(80, self.bubbleImage.frame.size.height - 70, self.bubbleImage.frame.size.width - 40 - 70, 40);
}

- (void)layoutMessageContentViews:(BOOL)isFromMe {
    CGSize textSize = CGSizeMake(200, 100);
    CGSize size = textSize;
    size.width += LABEL_BUBBLE_LEFT + LABEL_BUBBLE_RIGHT;
    size.height += LABEL_BUBBLE_TOP + LABEL_BUBBLE_BOTTOM;
    if (size.width < CONTENT_MIN_WIDTH) {
        size.width = CONTENT_MIN_WIDTH;
    }else {
        size.width = ceil(size.width);
    }
    
    if (size.height < CONTENT_MIN_HEIGHT) {
        size.height = CONTENT_MIN_HEIGHT;
    }else {
        size.height = ceil(size.height);
    }
    
    if (isFromMe) {
        CGRect frame = CGRectMake(0,
                                  CONTENT_SUPER_TOP - BUBBLE_TOP_BLANK,
                                  size.width + BUBBLE_LEFT_BLANK + BUBBLE_RIGHT_BLANK,
                                  size.height - 20);
        frame.origin.x = CGRectGetMinX(self.avatarImage.frame) - CGRectGetWidth(frame) - CONTENT_AVATAR_MARGIN;
        self.bubbleImage.frame = frame;
        
        
    }else {
        self.bubbleImage.frame = CGRectMake(CONTENT_AVATAR_MARGIN + CGRectGetMaxX(self.avatarImage.frame),
                                            CONTENT_SUPER_TOP - BUBBLE_TOP_BLANK, size.width + BUBBLE_LEFT_BLANK + BUBBLE_RIGHT_BLANK, size.height - 20);
        
        
    }
    [self setSubViewFrame];
    
}


-(void)setMessageModel:(LLMessageModel *)messageModel{
    _messageModel = messageModel;
    NSDictionary *dict = [messageModel.text JSONObject];
    [_contentImage setImageName:[dict objectForKey:@"gift_image"] placeholder:nil];
    _nameLbl.text = [dict objectForKey:@"gift_name"];
    _descLbl.text = [dict objectForKey:@"gift_desc"];
    [super setMessageModel:_messageModel];
}

+ (CGFloat)heightForModel:(LLMessageModel *)model{
    return 100;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
