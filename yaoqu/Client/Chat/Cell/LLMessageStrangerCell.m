//
//  LLMessageStrangerCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/9.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#define HorizontalMargin 3
#define VerticalMargin 3

#import "LLMessageStrangerCell.h"
#import "NSDate+LLExt.h"
#import "LLColors.h"
#import "LLConfig.h"
#import "LLUtils.h"


@interface LLMessageStrangerCell()


@property (nonatomic) UIView *bgView;
@property (nonatomic) UILabel *dateLabel;
@property (nonatomic) UIButton *addBtn;
@property (nonatomic) UIButton *blackBtn;
@property (nonatomic) UILabel *agressLbl;
@property (nonatomic) UILabel *contentLbl;


@end

@implementation LLMessageStrangerCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor clearColor];// LL_MESSAGE_CELL_BACKGROUND_COLOR;
        self.backgroundColor = [UIColor clearColor];
        self.bgView = [[UIView alloc]init];
        self.bgView.backgroundColor = kLLBackgroundColor_darkGray;
        
        [self.contentView addSubview:self.bgView];
        
        
        
        self.agressLbl = [ControllerHelper autoFitLabel];
        self.agressLbl.font = [UIFont systemFontOfSize:14];
        self.agressLbl.textColor = AppBlack;
        self.agressLbl.text = @"是否同意";
        [self.bgView addSubview:self.agressLbl];
        
        self.contentLbl = [ControllerHelper autoFitLabel];
        self.contentLbl.font = [UIFont systemFontOfSize:14];
        self.contentLbl.textColor = AppBlack;
        self.contentLbl.textAlignment = NSTextAlignmentCenter;
        self.contentLbl.text = @"回复对方可以快速成为好友哦！";
        [self.bgView addSubview:self.contentLbl];
        
        self.addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.addBtn addTarget:self action:@selector(addBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        self.addBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [self.addBtn setTitle:@"加为好友" forState:UIControlStateNormal];
        [self.addBtn setTitleColor:AppOrange forState:UIControlStateNormal];
        [self.addBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
        [self.bgView addSubview:self.addBtn];
        
        self.blackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.blackBtn addTarget:self action:@selector(blackBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        self.blackBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [self.blackBtn setTitle:@"拉黑" forState:UIControlStateNormal];
        [self.blackBtn setTitleColor:AppOrange forState:UIControlStateNormal];
        [self.blackBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
        [self.bgView addSubview:self.blackBtn];
        
        self.bgView.frame = CGRectMake(20, 0, APP_WIDTH - 40, 60);
        self.bgView.layer.masksToBounds = YES;
        self.bgView.layer.cornerRadius = 30;
        
 
        
    }
    
    return self;
}


-(void)addBtnClick:(id)sender{
    self.addBtn.enabled = NO;
    if ([self.strangerDelegate respondsToSelector:@selector(strangerCellAgreeBtnClick:)]) {
        [self.strangerDelegate strangerCellAgreeBtnClick:nil];
    }
}


-(void)blackBtnClick:(id)sender{
    self.blackBtn.enabled = NO;
    if ([self.strangerDelegate respondsToSelector:@selector(strangerCellBlackBtnClick:)]) {
        [self.strangerDelegate strangerCellBlackBtnClick:nil];
    }
}

- (void)setMessageModel:(LLMessageModel *)messageModel {
    if (_messageModel != messageModel) {
        _messageModel = messageModel;
        self.addBtn.enabled = ![[_messageModel.ext objectForKey:@"is_friend"] boolValue];
        self.blackBtn.enabled = ![[_messageModel.ext objectForKey:@"is_black"] boolValue];
        [self layoutContentView];
    }
    
    [messageModel clearNeedsUpdateForReuse];
}

- (void)layoutContentView {
    self.agressLbl.frame = CGRectMake((APP_WIDTH - 40) / 2 - 90, 10, 60, 20);
    
    self.addBtn.frame = CGRectMake((APP_WIDTH - 40) / 2 - 30, 10, 60, 20);
    
    self.blackBtn.frame = CGRectMake((APP_WIDTH - 40) / 2 + 30, 10, 60, 20);
    
    self.contentLbl.frame = CGRectMake(0, 30, APP_WIDTH - 40, 20);
}


-(void)layoutSubviews{
    [super layoutSubviews];
}

+ (CGFloat)heightForModel:(LLMessageModel *)model {
    return 80;
}

@end
