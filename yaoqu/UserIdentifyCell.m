//
//  UserIdentifyCell.m
//  yaoqu
//
//  Created by ridchan on 2017/7/8.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserIdentifyCell.h"


@implementation UserIdentifyDetailCell

-(UIImageView *)selectImage{
    if (!_selectImage) {
        _selectImage = [[UIImageView alloc]init];
        _selectImage.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_selectImage];
    }
    return _selectImage;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        [self.contentView addSubview:_descLbl];
    }
    return _descLbl;
}

-(void)commonInit{
    [self.selectImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.selectImage.mas_right).offset(5);
    }];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self commonInit];
    }
    return self;
}

@end

#pragma mark -


@interface UserIdentifyCell()<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong) UIImageView *idImage;
@property(nonatomic,strong) UILabel *idLbl;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UILabel *tipLbl;
@property(nonatomic,strong) UIView *line;
@property(nonatomic,strong) UITableView *tableView;

@property(nonatomic,strong) UIButton *openBtn;

@end

@implementation UserIdentifyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    self.idImage.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .topSpaceToView(self.contentView, 10)
    .widthIs(20)
    .heightIs(20);
    
    self.idLbl.sd_layout
    .leftSpaceToView(self.idImage, 10)
    .centerYEqualToView(self.idImage)
    .widthIs(300)
    .heightIs(20);
    
    self.descLbl.sd_layout
    .leftSpaceToView(self.idImage,10)
    .topSpaceToView(self.idLbl, 5)
    .rightSpaceToView(self.contentView, 10)
    .heightIs(20);
    
    self.tipLbl.sd_layout
    .leftSpaceToView(self.idImage,10)
    .topSpaceToView(self.descLbl, 5)
    .rightSpaceToView(self.contentView, 10)
    .heightIs(20);
    
    self.tableView.sd_layout
    .leftSpaceToView(self.idImage,10)
    .topSpaceToView(self.tipLbl, 5)
    .rightSpaceToView(self.contentView, 10)
    .heightIs(80);
    
    self.line.sd_layout
    .leftSpaceToView(self.idImage,10)
    .bottomEqualToView(self.contentView)
    .rightEqualToView(self.contentView)
    .heightIs(0.5);
    
    [self setupAutoHeightWithBottomViewsArray:@[self.tableView] bottomMargin:10];
    
}

#pragma mark -
#pragma mark tableview data source




-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 20;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserIdentifyDetailCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil ) {
        cell = [[UserIdentifyDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.descLbl.font = DetailFont;
        cell.descLbl.textColor = AppGray;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    if (indexPath.row == 0)
        cell.selectImage.image = [UIImage imageNamed:@"ic_id_select"];
    else
        cell.selectImage.image = [UIImage imageNamed:@"ic_id_select_no"];
    cell.descLbl.attributedText = [self descString:@"已送出300旅游币"];
    return cell;
}

-(NSAttributedString *)descString:(NSString *)str{
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:str];
    
    NSString *num = [str numberString];
    [string addAttribute:NSForegroundColorAttributeName value:AppBlue range:[str rangeOfString:num]];
    
    return string;
}

#pragma mark -
#pragma mark  lazy layout

-(UIView *)line{
    if (!_line) {
        _line = [[UIView alloc]init];
        _line.backgroundColor = AppGray;
        [self.contentView addSubview:_line];
    }
    
    return _line;
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.scrollEnabled = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.contentView addSubview:_tableView];
    }
    return _tableView;
}


-(UILabel *)idLbl{
    if (!_idLbl) {
        _idLbl = [ControllerHelper autoFitLabel];
        _idLbl.font = DetailFont;
        _idLbl.textColor = [UIColor blackColor];
        _idLbl.text = @"土豪";
        [self.contentView addSubview:_idLbl];
    }
    return _idLbl;
}

-(UIImageView *)idImage{
    if (!_idImage) {
        _idImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_user_level_1"]];
        _idImage.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_idImage];
    }
    return _idImage;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.font = DetailFont;
        _descLbl.textColor = AppGray;
        _descLbl.text = @"送出礼物达10万旅游币获得称号";
        [self.contentView addSubview:_descLbl];
    }
    return _descLbl;
}

-(UILabel *)tipLbl{
    if (!_tipLbl) {
        _tipLbl = [ControllerHelper autoFitLabel];
        _tipLbl.font = DetailFont;
        _tipLbl.textColor = AppGray;
        _tipLbl.text = @"当前进度：";
        [self.contentView addSubview:_tipLbl];
    }
    return _tipLbl;
}

-(UIButton *)openBtn{
    if (!_openBtn) {
        _openBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.contentView addSubview:_openBtn];
    }
    return _openBtn;
}

@end
