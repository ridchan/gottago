//
//  UserFollowApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserFollowListApi.h"

@implementation UserFollowListApi{
    
}

-(id)initWithMember:(NSString *)member_id page:(NSInteger)page size:(NSInteger)size{
    if (self = [super init]) {
        [self.params setValue:member_id forKey:@"member_id"];
        [self.params setValue:@(page) forKey:@"page"];
        [self.params setValue:@(size) forKey:@"size"];
    }
    return self;
}

-(NSString *)requestUrl{
    return UserFollowListUrl;
}


@end
