//
//  RechargeInfoCell.h
//  QBH
//
//  Created by 陳景雲 on 2017/1/17.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonConfigModel.h"

@interface RechargeInfoCell : UICollectionViewCell

@property(nonatomic,strong) UIView *bgView;
@property(nonatomic,strong) UILabel *chargeLbl;
@property(nonatomic,strong) UILabel *totalLbl;
@property(nonatomic,strong) UILabel *presendLbl;
@property(nonatomic,strong) UIImageView *imageView;

@property(nonatomic,strong) UIImageView *giftImage;

@property(nonatomic) BOOL bSelected;

@property(nonatomic,strong) CurrencyValueModel *model;

@end
