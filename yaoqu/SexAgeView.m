//
//  SexAgeView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "SexAgeView.h"



@interface SexAgeView (){
    UIImage *maleImg;
    UIImage *femaleImg;
}

@end

@implementation SexAgeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.layer.cornerRadius = 2;
        self.layer.masksToBounds = YES;
        
        [self commonInit];
    }
    return self;
}

-(void)setSex:(SexType)sex{
    _sex = sex;
    if (_sex == Sex_Female) {
        self.backgroundColor = RGB16(0xf57d81);
        self.sexImg.image = [UIImage imageNamed:@"ic_aboutme_female"];
    }else if (_sex == Sex_Male){
        self.backgroundColor = RGB16(0x99aef7);
        self.sexImg.image = [UIImage imageNamed:@"ic_aboutme_male"];
    }else{
//        self.backgroundColor = [UIColor yellowColor];
        self.sexImg.image = [UIImage imageNamed:@""];
    }
}

-(void)layoutSubviews{
    [super layoutSubviews];
}

-(void)setAge:(NSInteger)age{
    self.ageLbl.text = [NSString stringWithFormat:@"%ld",(long)age];
}

-(void)setContent:(NSString *)content{
    self.contentLbl.text = content;
    [self setNeedsUpdateConstraints];
 
}




-(void)commonInit{
    [self.sexImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
        make.width.equalTo(self.sexImg.mas_height);
    }];
    
    [self.ageLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.sexImg.mas_right).offset(3);
        make.top.equalTo(self.mas_top).offset(3);
        make.bottom.equalTo(self.mas_bottom).offset(-3);
//        make.width.mas_offset(15);
    }];
    
    [self.contentLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.ageLbl.mas_right);
        make.top.equalTo(self.mas_top).offset(2);
        make.bottom.equalTo(self.mas_bottom).offset(-2);
        make.right.equalTo(self.mas_right).offset(-2);
    }];
}

-(UIImageView *)sexImg{
    if (!_sexImg) {
        _sexImg = [[UIImageView alloc]init];
        _sexImg.contentMode = UIViewContentModeScaleAspectFit;
//        _sexImg.image = [UIImage imageNamed:@"ic_user_female_bg"];
        [self addSubview:_sexImg];
    }
    return _sexImg;
}

-(UILabel *)ageLbl{
    if (!_ageLbl) {
        _ageLbl = [ControllerHelper autoFitLabel];
        _ageLbl.minimumScaleFactor = 0.5;
        _ageLbl.textAlignment = NSTextAlignmentCenter;
        _ageLbl.textColor = [UIColor whiteColor];
        _ageLbl.text = @"11";
        _ageLbl.font = [UIFont systemFontOfSize:11];
//        _ageLbl.backgroundColor = [UIColor redColor];
        _ageLbl.minimumScaleFactor = 0.5;
        [self addSubview:_ageLbl];
    }
    return _ageLbl;
}

-(UILabel *)contentLbl{
    if (!_contentLbl) {
        _contentLbl = [ControllerHelper autoFitLabel];
        _contentLbl.textColor = [UIColor whiteColor];
        _contentLbl.textAlignment = NSTextAlignmentCenter;
        _contentLbl.font = SystemFont(11);
        _contentLbl.text = nil;
//        _contentLbl.backgroundColor = [UIColor yellowColor];
        _contentLbl.minimumScaleFactor = 0.5;
        [self addSubview:_contentLbl];
    }
    return _contentLbl;
}



@end
