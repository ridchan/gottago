//
//  AddressProvinceVC.h
//  yaoqu
//
//  Created by ridchan on 2017/7/5.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NormalTableViewController.h"
#import "UserModel.h"

@interface AddressProvinceVC : NormalTableViewController

@property(nonatomic,strong) UserModel *model;

@end
