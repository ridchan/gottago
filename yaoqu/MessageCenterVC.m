//
//  MessageCenterVC.m
//  yaoqu
//
//  Created by ridchan on 2017/6/26.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MessageCenterVC.h"
#import "RCUserCacheManager.h"
@interface MessageCenterVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) BubbleModel *currentBubble;
@property(nonatomic,strong) BubbleModel *lastBubble;

@end

@implementation MessageCenterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = LS(@"消息中心");
    
//    @{@"name":LS(@"联系客服"),@"img":@"ic_message_client",@"vc":@""}
    
    self.datas = [@[@{@"name":LS(@"系统通知"),@"img":@"ic_chat_message",@"vc":@"MineNotificationVC"},
                   @{@"name":LS(@"点赞"),@"img":@"ic_message_like",@"vc":@"MinePraiseVC"},
                   @{@"name":LS(@"评论"),@"img":@"ic_dynamic_comment",@"vc":@"MineCommentVC"},
                   @{@"name":LS(@"谁看了我"),@"img":@"ic_message_eye",@"vc":@"MineLookAtMeVC"},
                    @{@"name":LS(@"收到的礼物"),@"img":@"ic_message_gift",@"vc":@"MineGiftReceiveVC"}
                   ] mutableCopy];
    
    [self setRightItemWithIcon:[UIImage imageNamed:@"ic_user_setting"] selector:@selector(btnClick:)];
    
    
//    [self commonInit];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.currentBubble = [RCUserCacheManager sharedManager].currentBubble;
    self.lastBubble = [RCUserCacheManager sharedManager].lastBubble;
    [self.tableView reloadData];
}

-(void)btnClick:(id)sender{
    [self pushViewControllerWithName:@"MineNotificationCenterVC"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dict = self.datas[indexPath.row];
    [self pushViewControllerWithName:dict[@"vc"]];
    if (indexPath.row == 0){
        self.lastBubble.message_system = [RCUserCacheManager sharedManager].currentBubble.message_system;
    }else if (indexPath.row == 1){
        self.lastBubble.message_like = [RCUserCacheManager sharedManager].currentBubble.message_like;
    }else if (indexPath.row == 2){
        self.lastBubble.message_comment = [RCUserCacheManager sharedManager].currentBubble.message_comment;
    }else if (indexPath.row == 3){
        self.lastBubble.preview = [RCUserCacheManager sharedManager].currentBubble.preview;
    }else if (indexPath.row == 4){
        self.lastBubble.gift_send = [RCUserCacheManager sharedManager].currentBubble.gift_send;
    }
//    self.lastBubble = [[RCUserCacheManager sharedManager].lastBubble mutableCopy];
    [RCUserCacheManager sharedManager].lastBubble = self.lastBubble;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NormalTableViewCell *cell = [NormalTableViewCell cellWithType:NormalCellType_Access_Detail identifier:@"Cell" tableView:tableView];
    NSDictionary *dict = self.datas[indexPath.row];
    
    cell.contentImage.image = [[UIImage imageNamed:dict[@"img"]] imageToColor:AppBlack];
    
    [cell.contentImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(cell.mas_left).offset(10);
        make.centerY.equalTo(cell.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    cell.textLbl.text = dict[@"name"];
    cell.type = indexPath.row == 0 ? NormalCellType_None : NormalCellType_TopLine;
    
    
    if (indexPath.row == 0){
        [cell.contentImage setBubble:[@([self.currentBubble.message_system integerValue] > [self.lastBubble.message_system integerValue]) stringValue] offset:CGSizeZero];
    }else if (indexPath.row == 1){
        [cell.contentImage setBubble:[@([self.currentBubble.message_like integerValue] > [self.lastBubble.message_like integerValue]) stringValue] offset:CGSizeZero];
    }else if (indexPath.row == 2){
        [cell.contentImage setBubble:[@([self.currentBubble.message_comment integerValue] > [self.lastBubble.message_comment integerValue]) stringValue] offset:CGSizeZero];
    }else if (indexPath.row == 3){
        [cell.contentImage setBubble:[@([self.currentBubble.preview integerValue] > [self.lastBubble.preview integerValue]) stringValue] offset:CGSizeZero];
    }else if (indexPath.row == 4) {
        [cell.contentImage setBubble:[@([self.currentBubble.gift_send integerValue] > [self.lastBubble.gift_send integerValue]) stringValue] offset:CGSizeZero];
    }else{
        [cell.contentImage setBubble:nil offset:CGSizeZero];
    }

    return cell;
}


-(void)bubble:(NormalTableViewCell *)cell index:(NSInteger)index{
    RACSignal *singal1 = RACObserve([RCUserCacheManager sharedManager], currentBubble);
    RACSignal *singal2 = RACObserve([RCUserCacheManager sharedManager], lastBubble);
    [cell.contentImage setBubble:nil offset:CGSizeZero];
    [[RACSignal combineLatest:@[singal1,singal2] reduce:^id(BubbleModel *currentBubble,BubbleModel *lastBubble){
        if (index == 0){
            [cell.contentImage setBubble:[NSString boolValue:[currentBubble.message_system integerValue] > [lastBubble.message_system integerValue]] offset:CGSizeZero];
        }else if (index == 1){
            [cell.contentImage setBubble:[NSString boolValue:[currentBubble.message_like integerValue] > [lastBubble.message_like integerValue]] offset:CGSizeZero];
        }else if (index == 2){
            [cell.contentImage setBubble:[NSString boolValue:[currentBubble.message_comment integerValue] > [lastBubble.message_comment integerValue]] offset:CGSizeZero];
        }else if (index == 3){
            [cell.contentImage setBubble:[NSString boolValue:[currentBubble.preview integerValue] > [lastBubble.preview integerValue]] offset:CGSizeZero];
        }else{
            [cell.contentImage setBubble:[NSString boolValue:[currentBubble.gift_send integerValue] > [lastBubble.gift_send integerValue]] offset:CGSizeZero];
        }
        return @(NO);
    }] subscribeNext:^(id x) {
        
    }];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
