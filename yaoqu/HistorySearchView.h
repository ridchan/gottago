//
//  HistorySearchView.h
//  yaoqu
//
//  Created by ridchan on 2017/9/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HistorySearchViewDelegate <NSObject>

-(void)historyViewDidSelect:(id)obj;
-(void)historyViewDeleteBtnClick:(id)obj;

@end

@interface HistorySearchView : UIView


@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) UIButton *delBtn;
@property (nonatomic, strong) NSArray *titleArray;

@property(nonatomic,weak) id<HistorySearchViewDelegate>delegate;

@end
