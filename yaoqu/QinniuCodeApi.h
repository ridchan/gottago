//
//  QinniuCodeApi.h
//  yaoqu
//
//  Created by ridchan on 2017/6/19.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface QinniuCodeApi : BaseApi

-(id)initWithType:(NSString *)type;

@end
