//
//  DynamicCommentListHeader.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/30.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicCommentListHeader.h"
#import "SexView.h"
#import "CommentApi.h"

@interface DynamicCommentListHeader()

@property(nonatomic,strong) UIImageView *userImage;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) SexView *sexImage;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UILabel *dateLbl;
@property(nonatomic,strong) ImageButton *likeBtn;
@property(nonatomic,strong) ImageButton *commentBtn;

@property(nonatomic,strong) UIView *bottomLine;

@end

@implementation DynamicCommentListHeader

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit2];
        self.backgroundColor = [UIColor whiteColor];
        
        [RACObserve(self, model) subscribeNext:^(DynamicCommentModel *x) {
            [self.userImage setImageName:x.image placeholder:nil];
            self.nameLbl.text = x.username;
            self.descLbl.text = x.content;
            self.dateLbl.text = [NSString compareCurrentTime:x.time];
            self.likeBtn.titleLbl.text = x.like;
            self.commentBtn.titleLbl.text = x.comment;
            self.sexImage.sexType = [x.sex integerValue];
        }];
        
        [RACObserve(self, model.is_like) subscribeNext:^(id x) {
            self.likeBtn.contentImage.image = [x boolValue] ? [UIImage imageNamed:@"ic_dynamic_like_on"] : [[UIImage imageNamed:@"ic_dynamic_like"] imageToColor:AppBlack];
        }];
        
        
        
        RAC(self.likeBtn.titleLbl,text) = RACObserve(self, model.like);

    }
    return self;
}


-(void)commonInit2{
    
    self.userImage.sd_layout
    .leftSpaceToView(self, 10)
    .topSpaceToView(self, 10)
    .heightIs(40)
    .widthIs(40);
    self.userImage.layer.cornerRadius = 20;
    self.userImage.layer.masksToBounds = YES;
    
    self.nameLbl.sd_layout
    .topEqualToView(self.userImage)
    .leftSpaceToView(self.userImage, 5)
    .heightIs(20);
    [self.nameLbl setSingleLineAutoResizeWithMaxWidth:200];
    
    self.sexImage.sd_layout
    .leftSpaceToView(self.nameLbl, 5)
    .centerYEqualToView(self.nameLbl)
    .heightIs(15)
    .widthIs(15);
    
    
    self.dateLbl.sd_layout
    .leftSpaceToView(self.userImage, 5)
    .topSpaceToView(self.nameLbl, 0)
    .heightIs(20)
    .autoWidthRatio(100);
    
    
    self.commentBtn.sd_layout
    .centerYEqualToView(self.nameLbl)
    .rightSpaceToView(self,10)
    .widthIs(50)
    .heightIs(25);
    
    self.likeBtn.sd_layout
    .centerYEqualToView(self.nameLbl)
    .rightSpaceToView(self.commentBtn,10)
    .widthIs(50)
    .heightIs(25);
    
    
    self.descLbl.sd_layout
    .leftEqualToView(self.dateLbl)
    .topSpaceToView(self.dateLbl, 10)
    .rightSpaceToView(self, 10)
    .autoHeightRatio(0);
    

    self.bottomLine.sd_layout
    .leftEqualToView(self)
    .rightEqualToView(self)
    .topSpaceToView(self.descLbl, 10)
    .heightIs(0.5);
    
    [self setupAutoHeightWithBottomView:self.bottomLine bottomMargin:0.1];
//    [self setupAutoHeightWithBottomViewsArray:@[self.bottomLine,self.descLbl] bottomMargin:0];
    [self layoutSubviews];
}


-(void)commonInit{
    [self.userImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.equalTo(self).offset(10);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    self.userImage.layer.cornerRadius = 20;
    self.userImage.layer.masksToBounds = YES;
    
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.userImage.mas_centerY);
        make.left.equalTo(self.userImage.mas_right).offset(5);
    }];
    
    
    [self.sexImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_right).offset(5);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    [self.dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.userImage.mas_centerY).offset(10);
    }];
    
    
    [self.commentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
        make.height.mas_equalTo(25);
    }];
    
    [self.likeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.commentBtn.mas_left).offset(-10);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
        make.height.mas_equalTo(25);
    }];
    
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.userImage.mas_bottom).offset(10);
        make.right.equalTo(self.mas_right).offset(-10);
        make.bottom.equalTo(self.mas_bottom).offset(-10);
    }];
    
    
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.bottom.equalTo(self.mas_bottom);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(0.5);
    }];
}

#pragma mark -
#pragma mark lazy layout

-(UIImageView *)userImage{
    if (!_userImage) {
        _userImage = [[UIImageView alloc]init];
        _userImage.contentMode = UIViewContentModeScaleAspectFill;
        _userImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:_userImage];
    }
    return _userImage;
}

-(UILabel *)nameLbl{
    if (!_nameLbl) {
        _nameLbl = [ControllerHelper autoFitLabel];
        _nameLbl.font = DynamicCommentNameFont;
        _nameLbl.textColor = DynamicCommentNameColor;
        [self addSubview:_nameLbl];
    }
    return _nameLbl;
}

-(SexView *)sexImage{
    if (!_sexImage) {
        _sexImage = [[SexView alloc]init];
        [self addSubview:_sexImage];
    }
    return _sexImage;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.numberOfLines = 0;
        _descLbl.font = DynamicCommentDescFont;
        _descLbl.textColor = DynamicCommentDescColor;
        [self addSubview:_descLbl];
    }
    return _descLbl;
}

-(UILabel *)dateLbl{
    if (!_dateLbl) {
        _dateLbl = [ControllerHelper autoFitLabel];
        _dateLbl.font = DynamicCommentTimeFont;
        _dateLbl.textColor = DynamicCommentTimeColor;
        [self addSubview:_dateLbl];
    }
    return _dateLbl;
}

-(ImageButton *)likeBtn{
    if (!_likeBtn) {
        _likeBtn = [[ImageButton alloc]init];
        _likeBtn.contentImage.image = [UIImage imageNamed:@"ic_dynamic_like"];
        [_likeBtn addTarget:self action:@selector(likeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_likeBtn];
    }
    return _likeBtn;
}

-(ImageButton *)commentBtn{
    if (!_commentBtn) {
        _commentBtn = [[ImageButton alloc]init];
        _commentBtn.contentImage.image = [UIImage imageNamed:@"ic_dynamic_comment"];
        [_commentBtn addTarget:self action:@selector(commentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_commentBtn];
    }
    return _commentBtn;
}



-(UIView *)bottomLine{
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc]init];
        _bottomLine.backgroundColor = AppLineColor;
        [self addSubview:_bottomLine];
    }
    return _bottomLine;
}

#pragma mark -
#pragma mark button action


-(void)commentBtnClick:(id)sender{
    
}


-(void)likeBtnClick:(id)sender{
    [self.likeBtn.contentImage.layer addAnimation:[self.likeBtn keyAnimation] forKey:nil];
    
    WEAK_SELF;
    NSDictionary *dict = @{@"comment_id":self.model.comment_id,
                           @"state":@(!self.model.is_like)
                           };
    CommentApi *api = [[CommentApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodOPTION;
    
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            weakSelf.model.is_like = !weakSelf.model.is_like;
            weakSelf.model.like = weakSelf.model.is_like ?
            [weakSelf.model.like autoIncrice] :
            [weakSelf.model.like autoReduce];
        }else{
            
        }
    }];

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
