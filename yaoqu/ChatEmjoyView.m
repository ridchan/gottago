//
//  ChatEmjoyView.m
//  shiyi
//
//  Created by ridchan on 16/6/24.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import "ChatEmjoyView.h"


@implementation ChatEmjoyCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.label = [[UILabel alloc]init];
        self.label.font = [UIFont systemFontOfSize:36];
        self.label.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.label];
        
        self.imageView = [[UIImageView alloc]init];
        self.imageView.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:self.imageView];
        
        [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left);
            make.top.equalTo(self.mas_top);
            make.bottom.equalTo(self.mas_bottom);
            make.right.equalTo(self.mas_right);
        }];
  
        [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left);
            make.top.equalTo(self.mas_top);
            make.bottom.equalTo(self.mas_bottom);
            make.right.equalTo(self.mas_right);
        }];
        

        
        [self.contentView setNeedsUpdateConstraints];
    }
    return self;
}

@end



@implementation ChatEmjoyView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.emjoyArray = [Emoji allEmoji];
        NSInteger count = 28 - self.emjoyArray.count % 28;
        
        NSInteger max = self.emjoyArray.count + count;
        
        NSInteger page = max / 28;
        
        for (int i = 0 ; i < count - page ; i ++){
            [self.emjoyArray addObject:@"<space>"];
        }
        
        
        for (int i = 1 ; i <= page ; i ++){
            if (i < page)
                [self.emjoyArray insertObject:@"<back>" atIndex: (i * 28) - 1 ];
            else
                [self.emjoyArray addObject:@"<back>"];
        }
        

        
        [self createViews];
        
    }
    return self;
}

-(void)createViews{
    self.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.userInteractionEnabled = YES;
    
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:[self layout]];
    self.collectionView.userInteractionEnabled = YES;
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.pagingEnabled = YES;
    [self.collectionView registerClass:[ChatEmjoyCell class] forCellWithReuseIdentifier:@"Cell"];
    [self addSubview:self.collectionView];
    
    [self.collectionView reloadData];
    
    
    UIView *view = [[UIView alloc]init];
    [self addSubview:view];
    self.pageControl = [[UIPageControl alloc]init];
    self.pageControl.numberOfPages = ceilf(self.emjoyArray.count / 28); //+ 1;
    
    [self.pageControl setPageIndicatorTintColor:[UIColor lightGrayColor]];
    [self.pageControl setCurrentPageIndicatorTintColor:AppOrange];
    self.pageControl.currentPage = 0;
    [view addSubview:self.pageControl];
    
    self.backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.backBtn setTitle:@"发送" forState:UIControlStateNormal];
    [self.backBtn setBackgroundColor:[UIColor orangeColor]];
    [self.backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.backBtn addTarget:self action:@selector(sendBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    self.backBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [view addSubview:self.backBtn];
    
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view.mas_right);
        make.bottom.equalTo(view.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(60, 30));
    }];
    
    
    [self.pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left);
        make.bottom.equalTo(view.mas_bottom);
        make.top.equalTo(view.mas_top);
        make.centerX.equalTo(view);
    }];
    

    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom);
        make.height.mas_equalTo(30);
    }];
    

    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(10);
        make.left.equalTo(self.mas_left).offset(10);
        make.right.equalTo(self.mas_right).offset(-10);
        make.bottom.equalTo(view.mas_top);
    }];
    
    
}


-(void)sendBtnClick:(id)sender{
    if ([self.delegate respondsToSelector:@selector(chatEmjoySend)]) {
        [self.delegate chatEmjoySend];
    }
}

-(void)layoutSubviews{
    
}

#pragma mark -
#pragma mark collection view 


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    self.pageControl.currentPage = roundf(scrollView.contentOffset.x / scrollView.frame.size.width);
}

-(UICollectionViewFlowLayout *)layout{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    flowLayout.minimumInteritemSpacing = 2;
    flowLayout.minimumLineSpacing = 2;
    flowLayout.scrollDirection =  UICollectionViewScrollDirectionHorizontal;
    return flowLayout;
}



-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height = (collectionView.frame.size.height - 2 * (4 - 1)) / 4 ;
    CGFloat width = (collectionView.frame.size.width - 2 * 7 ) / 7 ;
    if ([self.emjoyArray[indexPath.item] isEqualToString:@"<back>"])
    {
        height = 36;
    
    }
    return CGSizeMake(width, height);
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"emjoy %@",self.emjoyArray[indexPath.item]);
    
    if ([self.emjoyArray[indexPath.item] isEqualToString:@"<back>"]){
        if ([self.delegate respondsToSelector:@selector(chatEmjoyBack)]) {
            [self.delegate chatEmjoyBack];
        }
    } else if ([self.emjoyArray[indexPath.item] isEqualToString:@"<space>"]){
        
    }else{
        if ([self.delegate respondsToSelector:@selector(chatEmjoyText:)]) {
            [self.delegate chatEmjoyText:self.emjoyArray[indexPath.item]];
        }
    }
    
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.emjoyArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ChatEmjoyCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    if ([self.emjoyArray[indexPath.item] isEqualToString:@"<back>"]){
        cell.label.text = @"";
        cell.imageView.image = [UIImage imageNamed:@"删除回退"];
    } else if ([self.emjoyArray[indexPath.item] isEqualToString:@"<space>"]){
        cell.label.text = @"";
        cell.imageView.image = nil;
    }else{
        cell.imageView.image = nil;
        cell.label.text = self.emjoyArray[indexPath.item];
    }
    
    
    return cell;
}




#pragma mark -
#pragma mark  获取系统emjoy 表情


#define EMOJI_CODE_TO_SYMBOL(x) ((((0x808080F0 | (x & 0x3F000) >> 4) | (x & 0xFC0) << 10) | (x & 0x1C0000) << 18) | (x & 0x3F) << 24);

- (NSArray *)defaultEmoticons {
    NSMutableArray *array = [NSMutableArray new];
    for (int i=0x1F600; i<=0x1F64F; i++) {
        if (i < 0x1F641 || i > 0x1F644) {
            int sym = EMOJI_CODE_TO_SYMBOL(i);
            NSLog(@"i %d",sym);
            NSString *emoT = [[NSString alloc] initWithBytes:&sym length:sizeof(sym) encoding:NSUTF8StringEncoding];
            [array addObject:emoT];
        }
    }
    return array;
}


@end
