//
//  TravelHomePageHeader.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/21.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelHomePageHeader.h"
#import "NavTitleView.h"
#import "TravelBannerModel.h"
#import "CommonConfigManager.h"
#import <AVFoundation/AVFoundation.h>
#import "NetVideoPreviewVC.h"
#import "CommonConfigManager.h"
@interface TravelHomePageHeader()<SDCycleScrollViewDelegate>

@property(nonatomic,strong) SDCycleScrollView *bannerView;
@property(nonatomic,strong) NavTitleView *titleView;
@property(nonatomic,strong) UILabel *titleLbl;


@end

@implementation TravelHomePageHeader


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self commonInit];
    }
    return self;
}



-(void)commonInit{
    self.bannerView.sd_layout
    .leftEqualToView(self)
    .rightEqualToView(self)
    .topEqualToView(self)
    .heightIs(180);
    
//    self.titleLbl.sd_layout
//    .leftSpaceToView(self, 10)
//    .topSpaceToView(self.bannerView, 0)
//    .heightIs(80)
//    .widthIs(120);
    
    
//    [self titleView];
//    self.titleView.sd_layout
//    .rightSpaceToView(self, 10)
//    .topSpaceToView(self.bannerView, 0);
    
//    [self setupAutoHeightWithBottomView:self.titleLbl bottomMargin:0];
}


-(void)setDatas:(NSMutableArray *)datas{
    _datas = datas;
    
    NSString *baseUrl = [CommonConfigManager sharedManager].image_host;
    if (baseUrl == nil) return;
    NSMutableArray *imageGroup = [NSMutableArray array];
    
    [_datas sortUsingComparator:^NSComparisonResult(TravelBannerModel *obj1, TravelBannerModel *obj2) {
        if (obj1.sort > obj2.sort) {
            return NSOrderedDescending;
        }else{
            return NSOrderedAscending;
        }
    }];
    
    for (TravelBannerModel *model in _datas){
        if ([model.type integerValue] == 0)
            [imageGroup addObject:[baseUrl stringByAppendingPathComponent:model.image]];
        else
            [imageGroup addObject:[baseUrl stringByAppendingPathComponent:model.video]];
    }
    self.bannerView.imageURLStringsGroup =  imageGroup;
}

#pragma mark -
#pragma makr delegate


-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    TravelBannerModel *model = self.datas[index];
    if (model.video){
        NetVideoPreviewVC *vc = [[NetVideoPreviewVC alloc]init];
        vc.link = model.video;
        [[RouteController sharedManager] presentVC:vc];
    }
}


#pragma mark -
#pragma mark lazy layout

-(SDCycleScrollView *)bannerView{
    if (!_bannerView) {
        _bannerView = [[SDCycleScrollView alloc]init];
        _bannerView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
        _bannerView.delegate = self;
        [self addSubview:_bannerView];
    }
    return _bannerView;
}

-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        _titleLbl.text = LS(@"线下旅游");
        _titleLbl.textColor = RGB16(0x5d5b5b);
        _titleLbl.font = SystemFont(20);
        [self addSubview:_titleLbl];
    }
    return _titleLbl;
}

-(NavTitleView *)titleView{
    if (!_titleView) {
        _titleView = [[NavTitleView alloc]init];
        _titleView.frame = CGRectMake(APP_WIDTH - 210, 300 - 30 , 200, 20);
        _titleView.titles = @[@"最新",@"人气",@"热门",@"时间"];
        [self addSubview:_titleView];
    }
    return _titleView;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
