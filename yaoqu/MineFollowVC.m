//
//  MineFollowVC.m
//  yaoqu
//
//  Created by ridchan on 2017/7/17.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineFollowVC.h"
#import "UserNormalCell.h"
#import "UserModel.h"
#import "UserFollowListApi.h"
#import "UserFollowApi.h"
#import "RelationApi.h"

@interface MineFollowVC ()<UITableViewDelegate,UITableViewDataSource>



@end

@implementation MineFollowVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LS(@"我的关注");
    
    
    [self startRefresh];
    // Do any additional setup after loading the view.
}

-(void)startRefresh{
    WEAK_SELF;
    self.page = 1;
    
    NSDictionary *dict = @{@"page":@(self.page),
                           @"size":@(self.size),
                           @"type":@"follow"
                           };
    [[[RelationApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [weakSelf refreshComplete:info response:[UserModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]]];
    }];
    
}

-(void)startloadMore{
    WEAK_SELF;
    NSDictionary *dict = @{@"page":@(++self.page),
                           @"size":@(self.size),
                           @"type":@"follow"
                           };
    
    [[[RelationApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        NSArray *array = [UserModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        [weakSelf loadMoreComplete:info response:array];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return LS(@"取消关注");
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UserModel *model = [self.datas objectAtIndex:indexPath.row];
        [[[UserFollowApi alloc]initWithMember:model.member_id isFollow:NO] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            
        }];
        [self.datas removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserNormalCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ImageCell"];
    if (cell == nil) {
        cell = [UserNormalCell normalCellType:UserCellType_Normal identify:@"ImageCell"];
    }
    
    UserModel *model = [self.datas objectAtIndex:indexPath.row];
    
    cell.nameLbl.text = model.username;
    
    cell.sexView.sex = [model.sex integerValue];
    cell.sexView.age = [model.age integerValue];
    cell.sexView.content = nil;
    cell.userImage.userModel = model;

    
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
