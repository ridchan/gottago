//
//  UserFollowApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface UserFollowApi : BaseApi

-(id)initWithMember:(NSString *)member_id isFollow:(BOOL)is_Follow;

@end
