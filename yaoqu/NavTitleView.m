//
//  NavTitleView.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "NavTitleView.h"
#import "UIView+Extension.h"
#import "NSString+Size.h"
#import "CALayer+LXAdd.h"

@interface NavTitleView()

@property(nonatomic,strong) UIView *bottomLine;

@property(nonatomic) NSInteger currentIndex;
@property(nonatomic) CGFloat buttonWidth;
@property(nonatomic) CGFloat lineWidth;

@property(nonatomic,strong) NSMutableArray *titlesStrWidthArray;
@property (nonatomic,strong) NSMutableArray *btns;

@end

@implementation NavTitleView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.currentIndex = 0;
        self.bottomLine.tag = 99;
        self.lineWidth = 20;
        self.titlesStrWidthArray = [NSMutableArray array];
        
    }
    return self;
}

-(UIView *)bottomLine{
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc]init];
        _bottomLine.backgroundColor = [UIColor orangeColor];
        [self addSubview:_bottomLine];
    }
    return _bottomLine;
}


-(UIButton  *)titleBtn:(NSString *)title{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.titleLabel.font = [UIFont systemFontOfSize:17];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:AppTitleColor forState:UIControlStateNormal];
    return button;
}


-(void)setTitles:(NSArray *)titles{
    _titles = titles;

    
    [self.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[UIButton class]]) {
            [obj removeFromSuperview];
        }
    }];
    
    self.buttonWidth = self.frame.size.width /  [_titles count];
    CGFloat height = self.frame.size.height;
    NSInteger i = 0 ;
    self.btns = [NSMutableArray array];
    self.titlesStrWidthArray = [NSMutableArray array];
    for(NSString *title in _titles){
        
        //计算字串长度
        CGFloat strW = [self getlineWidth:i];
        [self.titlesStrWidthArray addObject:@(strW)];
        
        UIButton *btn = [self titleBtn:title];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = i + 1;
        btn.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        
        btn.frame = CGRectMake(self.buttonWidth * i ++ , 0, self.buttonWidth, height);
        [self addSubview:btn];
        [self.btns addObject:btn];
        
        
        
    }
    self.currentIndex = 0;
    [self setBottomLineInView:0];

}


-(void)layoutSubviews{
    [super layoutSubviews];
}


-(void)setBottomLineInView:(NSInteger)index{
    
    CGFloat height = self.frame.size.height;
    CGFloat width = [self getlineWidth:index];
    self.bottomLine.frame = CGRectMake((self.buttonWidth - width) / 2 + self.buttonWidth * index , height - 3.5, width, 3.5);
    
//    self.currentIndex = 0;
    
}



-(void)setContentOffset:(CGFloat)contentOffset{
    if (rintf(contentOffset) == contentOffset){
        self.currentIndex = contentOffset;
    }
    
    CGFloat d_value = contentOffset - self.currentIndex; //差值
    
    
    CGFloat curBtnWidth = [_titlesStrWidthArray[_currentIndex] floatValue];
    CGFloat nextBtnWidth = 0.0;
    CGFloat totalWidth = self.buttonWidth + curBtnWidth / 2;
    CGFloat buttonOffset = 0;
    
    if(d_value > 0){
        buttonOffset = self.currentIndex * self.buttonWidth;
        nextBtnWidth = [_titlesStrWidthArray[_currentIndex + 1] floatValue];
        totalWidth += nextBtnWidth / 2;
        
        if (d_value < 0.5){
            self.bottomLine.layer.left = self.buttonWidth / 2 - curBtnWidth / 2 + buttonOffset;
            self.bottomLine.layer.width = curBtnWidth  + (totalWidth - curBtnWidth) * d_value / 0.5;
             [_btns[_currentIndex + 1] setTitleColor:AppTitleColor forState:UIControlStateNormal];
        }else if (d_value > 0.5 && d_value < 1.0){
            self.bottomLine.layer.width = nextBtnWidth + (totalWidth - nextBtnWidth) * (1 - d_value) / 0.5;
            self.bottomLine.layer.right = self.buttonWidth * 3 / 2  + nextBtnWidth / 2 + buttonOffset;
             [_btns[_currentIndex + 1] setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
            
        }
       
        
    }else if (d_value < 0){
        
        buttonOffset = (self.currentIndex - 1) * self.buttonWidth;
        nextBtnWidth = [_titlesStrWidthArray[_currentIndex - 1] floatValue];
        totalWidth += nextBtnWidth / 2;
        
        if (fabs(d_value) < 0.5){
            
            self.bottomLine.layer.width = curBtnWidth  + (totalWidth - curBtnWidth) * fabs(d_value) / 0.5;
            self.bottomLine.layer.right = self.buttonWidth * 3 / 2  + curBtnWidth / 2 + buttonOffset;
            [_btns[_currentIndex - 1] setTitleColor:AppTitleColor forState:UIControlStateNormal];
            
        }else if (fabs(d_value) > 0.5 && fabs(d_value) < 1.0){
            
            self.bottomLine.layer.left = self.buttonWidth / 2 - nextBtnWidth / 2 + buttonOffset;
            self.bottomLine.layer.width = nextBtnWidth + (totalWidth - nextBtnWidth) * (1 - fabs(d_value)) / 0.5;
            
//            
//            [_btns[_currentIndex - 1] setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
            [_btns[_currentIndex - 1] setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
            
        }
        

    
    }
    
    _contentOffset = contentOffset;
}


//获取线的宽度
-(CGFloat)getlineWidth:(NSInteger )strIndex{
    NSString *firstStr = _titles[strIndex];
    CGFloat lineW = [firstStr widthWithFont:[UIFont systemFontOfSize:17] constrainedToHeight:self.frame.size.height-2]+4;
    
    return lineW;
}



-(void)setCurrentIndex:(NSInteger)currentIndex{
    
    for (int i = 0 ; i < self.titles.count ; i++){
        if (i != currentIndex)
            [_btns[i] setTitleColor:AppTitleColor forState:UIControlStateNormal];
    }
    [_btns[currentIndex] setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    
    _currentIndex = currentIndex;
    
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    CGFloat curBtnWidth = [_titlesStrWidthArray[_currentIndex] floatValue];
    CGFloat buttonOffset = _currentIndex * self.buttonWidth;
    
    self.bottomLine.layer.left = self.buttonWidth / 2 - curBtnWidth / 2 + buttonOffset;
    self.bottomLine.layer.width = curBtnWidth;
    [UIView commitAnimations];
}


-(void)btnClick:(UIButton *)button{
    self.currentIndex = button.tag - 1;
    if (self.selectBlock) {
        self.selectBlock(@(self.currentIndex));
    }
}


@end
