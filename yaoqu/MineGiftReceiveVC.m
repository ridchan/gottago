//
//  MineGiftReceiveVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/30.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineGiftReceiveVC.h"
#import "GiftApi.h"
#import "GiftReceiveCell.h"
@interface MineGiftReceiveVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic) GiftSearchType type;

@end

@implementation MineGiftReceiveVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.type = [self.paramObj integerValue];
    self.title = self.type == GiftSearchType_Receive ? @"收到的礼物" : @"送出的礼物";
    [self.tableView registerClass:[GiftReceiveCell class] forCellReuseIdentifier:@"Cell"];
    [self startRefresh];
    // Do any additional setup after loading the view.
}

-(void)startRefresh{
    self.page = 1;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:@(self.page) forKey:@"page"];
    [dict setValue:@(self.size) forKey:@"size"];
    [dict setValue:@(self.type) forKey:@"type"];
    
    WEAK_SELF;
     [[[GiftApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
         if (info.error == ErrorCodeType_None) {
             
             [weakSelf refreshComplete:info response:[GiftModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]]];
         }
     }];
    
}

-(void)startloadMore{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:@(++self.page) forKey:@"page"];
    [dict setValue:@(self.size) forKey:@"size"];
    [dict setValue:@(self.type) forKey:@"type"];
    
    WEAK_SELF;
    [[[GiftApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            [weakSelf loadMoreComplete:info response:[GiftModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]]];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat width = APP_WIDTH / 2.0;
    return MAX(width, 150);
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.datas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GiftReceiveCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.type = self.type;
    cell.model = self.datas[indexPath.section];
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
