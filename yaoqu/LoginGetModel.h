//
//  LoginGetModel.h
//  yaoqu
//
//  Created by ridchan on 2017/8/2.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface LoginGetModel : BaseModel

@property(nonatomic,strong) NSString *type;//	登录类型	true	string
@property(nonatomic,strong) NSString *openid;//	openid	false	string
@property(nonatomic,strong) NSString *unionid;//	unionid	false	string
@property(nonatomic,strong) NSString *phone;//	手机号码	false	int
@property(nonatomic,strong) NSString *password;//	密码	false	int

@end
