//
//  LeftBarButtonItem.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "LeftBarButtonItem.h"

@implementation LeftBarButtonItem


-(id)initWithIcon:(UIImage *)icon title:(NSString *)title{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    if (self =  [super initWithCustomView:view]) {
        
        self.imageView =[[UIImageView alloc]init];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.imageView.image = icon;
        
        self.btn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btn.backgroundColor = [UIColor clearColor];
        self.btn.titleLabel.font = [UIFont systemFontOfSize:16];
        self.btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;

        [self.btn setTitle:title forState:UIControlStateNormal];
        [self.btn setTitle:title forState:UIControlStateHighlighted];
        [self.btn setTitleColor:AppTitleColor forState:UIControlStateNormal];
        [self.btn setTitleColor:AppTitleColor forState:UIControlStateHighlighted];
        
        
        CGSize titleSize = [title ex_sizeWithFont:self.btn.titleLabel.font constrainedToSize:CGSizeMake(APP_WIDTH, MAXFLOAT)];
        float leight = titleSize.width;
        if (icon) {
            leight += icon.size.width;
            //        [btn setImage:icon forState:UIControlStateNormal];
            //        [btn setImage:icon forState:UIControlStateHighlighted];
            if (title.length == 0) {
                //文字没有的话，点击区域+10
                self.btn.imageEdgeInsets = UIEdgeInsetsMake(0, -13, 0, 13);
            } else {
                self.btn.imageEdgeInsets = UIEdgeInsetsMake(0, -3, 0, 3);
            }
        }
        if (title.length == 0) {
            //文字没有的话，点击区域+10
            leight = leight + 10;
        }
        view.frame = CGRectMake(0, 0, leight, 30);
        self.btn.frame = CGRectMake(-5, 0, leight, 30);
        self.imageView.frame = CGRectMake(0, 0, 20, 30);
        [self.btn addSubview:self.imageView];
        [view addSubview:self.btn];

        
    }
    return self;
}

-(void)setTitleColor:(UIColor *)color{
    [self.btn setTitleColor:color forState:UIControlStateNormal];
    [self.btn setTitleColor:color forState:UIControlStateHighlighted];
}


-(void)setImageColor:(UIColor *)color{
    self.imageView.image = [self.imageView.image imageToColor:color];
}

@end
