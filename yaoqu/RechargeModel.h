//
//  RechargeModel.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/9.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface RechargeModel : BaseModel

@property(nonatomic,strong) NSString *recharge_id;
@property(nonatomic,strong) NSString *actionType;
@property(nonatomic,strong) NSString *type;
@property(nonatomic,strong) NSString *val;
@property(nonatomic,strong) NSString *money;
@property(nonatomic,strong) NSString *currency;
@property(nonatomic,strong) NSString *total;
@property(nonatomic,strong) NSString *pay_total;
@property(nonatomic,strong) NSString *pay_type;
@property(nonatomic,strong) NSString *desc;
@property(nonatomic,strong) NSString *time;

@property(nonatomic,strong) NSString *log_desc;


@property(nonatomic,strong) NSString *title_desc;
@property(nonatomic,strong) NSString *title_value;

@property(nonatomic) BOOL bOpenMember;

@end
