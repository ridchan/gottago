//
//  UploadObject.h
//  yaoqu
//
//  Created by ridchan on 2017/7/5.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseModel.h"

@interface UploadObject : BaseModel

@property(nonatomic,strong) UIImage *image;
@property(nonatomic,strong) NSString *videoUrl;


@end
