//
//  OpenMemberView.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/11.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonConfigModel.h"


@interface OpenMemberCell : UITableViewCell

@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) UILabel *detailLbl;

@property(nonatomic) BOOL bCheck;

@end

@interface OpenMemberView : UIView

@property(nonatomic,strong) MemberValueModel *model;

@end
