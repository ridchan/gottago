//
//  RCImagePickerVC.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/26.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseViewController.h"
#import "BaseNavigationController.h"
#import "LLAssetModel.h"
#import "LLAssetsGroupModel.h"




@class RCImagePickerVC;

@protocol RCImagePickerVCDelegate <NSObject>

/**
 *  选取结束，返回选择的照片数据
 *  由ImagePickerController的调用者负责dismissImagePickerController
 *
 */
- (void)imagePickerController:(nonnull  RCImagePickerVC*)picker didFinishPickingImages:(nullable NSArray<LLAssetModel *> *)assets withError:(nullable NSError *)error;

/**
 *  取消选择
 *  由ImagePickerController的调用者负责dismissImagePickerController
 *
 */
- (void)imagePickerControllerDidCancel:(nonnull RCImagePickerVC *)picker;


- (void)imagePickerController:(nonnull RCImagePickerVC *)picker didFinishPickingVideo:(nonnull NSString *)videoPath;

//- (void)imagePickerController:(nonnull RCImagePickerVC *)picker didFinishPickingVideo:(nonnull NSString *)videoPath firstImage:(UIImage *)image;


@end

@interface RCImagePickerVC : BaseNavigationController


@property (nullable, nonatomic,strong) NSMutableArray<LLAssetModel *> *selectAssets;

@property (nullable, nonatomic, weak) id <RCImagePickerVCDelegate> pickerDelegate;

@property (nonatomic) RCImagePickerType pickerType;


/**
 *  选取照片完成回调
 *
 *  @param assets 具体返回什么比如UIImage、AssetURL等根据项目需要决定，此处简单返回assetModels
 *  @param error
 */
- (void)didFinishPickingImages:(nonnull NSArray<LLAssetModel *> *)assets WithError:(nullable NSError *)error assetGroupModel:(nonnull LLAssetsGroupModel *)assetGroupModel;

- (void)didCancelPickingImages;

- (void)didFinishPickingVideo:(nonnull NSString *)videoPath assetGroupModel:(nonnull LLAssetsGroupModel *)assetGroupModel;




@end
