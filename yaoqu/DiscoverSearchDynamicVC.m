//
//  DiscoverSearchDynamicVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/9/6.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DiscoverSearchDynamicVC.h"
#import "SearchApi.h"
#import "DynamicCell.h"

@interface DiscoverSearchDynamicVC ()

@end

@implementation DiscoverSearchDynamicVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[DynamicCell class] forCellReuseIdentifier:@"Cell"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)search:(NSString *)keyword{
    self.paramObj = keyword;
    [self startRefresh];
}


-(void)startRefresh{
    self.page = 1;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:@(self.page) forKey:@"page"];
    [dict setValue:@(self.size) forKey:@"size"];
    [dict setValue:@"2" forKey:@"type"];
    [dict setValue:self.paramObj forKey:@"keyword"];
    
    WEAK_SELF;
    SearchApi *api = [[SearchApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [weakSelf refreshComplete:info response:[DynamicModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"dynamic"]]];

    }];
}

-(void)startloadMore{
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:@(++self.page) forKey:@"page"];
    [dict setValue:@(self.size) forKey:@"size"];
    [dict setValue:@"2" forKey:@"type"];
    [dict setValue:self.paramObj forKey:@"keyword"];
    
    WEAK_SELF;
    SearchApi *api = [[SearchApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [weakSelf loadMoreComplete:info response:[DynamicModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"dynamic"]]];
    }];
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    id model = self.datas[indexPath.section];
    return [tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[DynamicCell class] contentViewWidth:APP_WIDTH];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self pushViewControllerWithName:@"DynamicDetailVC" params:self.datas[indexPath.section]];
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return .001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  self.datas.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DynamicCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.model = self.datas[indexPath.section];
    return cell;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
