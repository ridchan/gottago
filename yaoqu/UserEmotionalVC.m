//
//  UserEmotionalVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/7.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserEmotionalVC.h"
#import "NormalTableViewCell.h"
#import "CommonConfigManager.h"

@interface UserEmotionalVC ()

@property(nonatomic,strong) NSString *emotional;


@end

@implementation UserEmotionalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.editModel = self.paramObj;
    
    
    self.title = LS(@"情感状态");
    self.datas = [[CommonConfigManager sharedManager].hostModel.make_friends mutableCopy];
    self.emotional = self.editModel.sex_orientation;
    [self setRightItemWithTitle:LS(@"保存") selector:@selector(saveBtnClick:)];
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NormalTableViewCell *cell = [NormalTableViewCell cellWithType:NormalCellType_Checked identifier:@"Cell" tableView:tableView];
    cell.type = indexPath.row == 0 ? NormalCellType_None : NormalCellType_TopLine;
    cell.textLabel.text = self.datas[indexPath.item];
    cell.bChecked = [self.emotional isEqualToString:cell.textLabel.text];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.emotional = self.datas[indexPath.item];
    [self.tableView reloadData];
}

-(void)saveBtnClick:(id)sender{
//    self.editModel.sex = self.sex;
    self.editModel.sex_orientation = self.emotional;
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
