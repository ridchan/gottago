//
//  BaseRequetOperation.m
//  QBH
//
//  Created by 陳景雲 on 2016/12/28.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import "BaseRequetOperation.h"



@implementation BaseRequetOperation


#pragma mark ---- 获取基本配置

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark--------调用方法

//注意－－－－－－－－－－－－－－text/html和text/plain两种｀看看具体需要的是html还是plain｀｀｀看情况

-(void)execute_POST:(NSString*)uri params:(NSDictionary*)params RequestSuccess:(RequestDataSuccess )_success failure:(RequestDataFailure )_failure{
    
    __block   Responese* response = [[Responese alloc]init];
    
    if (![self beforeExecute:response]) {  //判断网络
        return;
    };
    
    [self open];
    
    NSString *baseUrl = nil;// configModel ? configModel.api_host : BaseUrl;
    self.manager = [[AFHTTPSessionManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    

    self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
    self.manager.requestSerializer.timeoutInterval = 10.0f;
    
    self.manager.responseSerializer = [AFJSONResponseSerializer serializer];
    response.afom = _manager;
    
//    NSMutableDictionary *newParams = [NSMutableDictionary dictionaryWithDictionary:params];
//    [newParams addEntriesFromDictionary:[self baseConfig]];
    
    NSLog(@"\npost url= %@%@  传入字典参数params= %@ \n ",baseUrl,uri,params);
    
    self.task = [_manager POST:uri parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error %@",error.description);
        
        [self afterExecute:response];
    }];
    
}



-(NSMutableDictionary *)baseConfig{
    NSMutableDictionary *config = [NSMutableDictionary dictionary];
    
    
    
    //version
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *curVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    //imei
    NSString *identifierForVendor = [[UIDevice currentDevice].identifierForVendor UUIDString];
    
    [config setValue:curVersion forKey:@"version"];
    [config setValue:identifierForVendor forKey:@"imei"];
    [config setValue:@"1" forKey:@"device"];
    
    return config;
}




+(BOOL)isRequestDataSuccess:(Responese *)respone{
    if([respone.code integerValue] == 9999999){
        return YES;
    }
    return NO;
    
    //    10001：成功
    //    20001：失败未知错误
    //    20003：找不到内容
    //    20004：参数错误
    //    21001：身份认证失效，需重新登录。
    //    20005：其他自定义状态消息
}
-(BOOL)afterExecute:(Responese*)response {
    if (response.error == nil) {
        return TRUE;
    }
    if ([response.error code] == NSURLErrorNotConnectedToInternet) {
        [self alertErrorMessage:@"网络不给力"];
        return FALSE;
    }
    //[self alertErrorMessage:@"服务器故障，请稍后再试。"];
    return true;
}




-(void)alertErrorMessage:(NSString *)message
{
    [[[UIAlertView alloc]initWithTitle:@"错误提示" message:message delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]show];
}

#pragma mark------检测状态

-(BOOL)beforeExecute:(Responese*)response {
    
    NSOperationQueue *operationQueue = response.afom.operationQueue;
    
    __block BOOL isNo = YES;
    
    [response.afom.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
                [operationQueue setSuspended:NO];
                break;
            case AFNetworkReachabilityStatusNotReachable:{
                isNo = NO;
            }
            default:
                [operationQueue setSuspended:YES];
                
                break;
        }
    }];
    
    [response.afom.reachabilityManager startMonitoring];
    
    return isNo;
}

-(void)cancelAllOperations{
    [self.manager.operationQueue cancelAllOperations];
    [self.task cancel];
}

#pragma mark - private methods
// 开启状态栏菊花
- (void)open
{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

// 关闭状态栏菊花
- (void)close
{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}


- (void)isConnection
{
    
}




@end
