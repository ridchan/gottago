//
//  UserDescEditVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/23.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserDescEditVC.h"
#import "RCTextView.h"

@interface UserDescEditVC ()

@property(nonatomic,strong) RCTextView *textView;

@end

@implementation UserDescEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.editModel = self.paramObj;
    [self commonInit];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)commonInit{
    self.title = LS(@"Description");
    [self setRightItemWithTitle:LS(@"Save") selector:@selector(saveBtnClick:)];
    
    self.textView = [[RCTextView alloc]init];
//    self.textView.maxNum = 10;
    self.textView.font =  SystemFont(14);
    self.textView.placeholder = LS(@"Descripiton");
    [self.view addSubview:self.textView];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(69);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.height.mas_equalTo(120);
    }];
    self.textView.text = self.editModel.desc;
}

-(void)saveBtnClick:(id)sender{
    self.editModel.desc = self.textView.text;
    [self.navigationController popViewControllerAnimated:YES];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
