//
//  TravelContactListApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/13.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelContactListApi.h"

@implementation TravelContactListApi{
    NSInteger _page;
}

-(id)initWithPage:(NSInteger)page size:(NSInteger)size{
    if (self = [super init]) {
        [self.params setValue:@(page) forKey:@"page"];
        [self.params setValue:@(size) forKey:@"size"];
    }
    return self;
}

-(NSString *)requestUrl{
    return TravelContactListUrl;
}



@end
