//
//  SearchResultVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/11.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "SearchResultVC.h"
#import "LLUtils.h"
#import "LLColors.h"
#import "LLLocationTableViewCell.h"
#import "LLGDConfig.h"

#define Style_NoResult 0
#define Style_CanLoadMore 1
#define Style_isLoading 2
#define Style_LoadAll 3
#define Style_HintSearch 4

@interface SearchResultVC ()<AMapSearchDelegate,CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource>


@property(nonatomic,strong) NSMutableArray *dataSource;
@property(nonatomic,strong) AMapSearchAPI *searchAPI;
@property (nonatomic) AMapPOIKeywordsSearchRequest *request;
@property (nonatomic) AMapReGeocodeSearchRequest *regeo;
@property(nonatomic,strong) CLLocationManager *locationManager;
@property(nonatomic,strong) UITableView *tableView;


@end

@implementation SearchResultVC {
    NSInteger curPage;
    NSString *originSearchText;
    NSInteger footerStyle;
    UIView *blankView;
    
}

-(void)setupLocation{
    
    _locationManager = [[CLLocationManager alloc]init];
    
    if ([CLLocationManager locationServicesEnabled]) {
        //IOS8以及以上版本需要设置，弹出是否允许使用定位提示
        if ([[UIDevice currentDevice].systemVersion floatValue] > 8.0) {
            [_locationManager requestWhenInUseAuthorization];
        }
        _locationManager.delegate = self;
        
        [_locationManager startUpdatingLocation];
    }
    
    
    
    
    _request = [[AMapPOIKeywordsSearchRequest alloc] init];
    _request.types = (NSString *)allPOISearchTypes;
    _request.sortrule = 1;
    _request.cityLimit = YES;
    _request.requireExtension = YES;
    _request.requireSubPOIs = NO;
//    _request.radius = 50000;
    _request.page = 1;
    _request.offset = 20;
    
    //    _regeo = [[AMapReGeocodeSearchRequest alloc] init];
    //    _regeo.radius = 3000;
    //    _regeo.requireExtension = NO;
    
    
    self.searchAPI = [[AMapSearchAPI alloc]init];
    self.searchAPI.delegate = self;
    
}

-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.dataSource = [NSMutableArray array];
    [self setupLocation];
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
        make.top.equalTo(self.view.mas_top).offset(-44);
    }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
 
}


-(void)searchWithText:(NSString *)text{
    
    originSearchText = text;
//    self.request.location = [AMapGeoPoint locationWithLatitude:self.location.coordinate.latitude longitude:self.location.coordinate.longitude];
    self.request.page = 1;
    self.request.keywords = text;
    [self.searchAPI AMapPOIKeywordsSearch:self.request];

    
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    self.request.page = 1;
    self.request.keywords = searchBar.text;
    [self.searchAPI AMapPOIKeywordsSearch:self.request];
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    self.searchAPI.delegate = nil;
    self.searchAPI = nil;
    _locationManager.delegate = self;
    _locationManager = nil;
}


#pragma mark -

-(void)addressLoadMore{
    self.request.page ++;
    [self.searchAPI AMapPOIKeywordsSearch:self.request];
}

-(void)setTableViewFooter{
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(addressLoadMore)];
}


-(void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response{
    
    if (request.page == 1) {
        [self.dataSource removeAllObjects];
        if (response.pois.count == request.offset){
            [self setTableViewFooter];
        }else{
            self.tableView.mj_footer = nil;
        }
        
    }else{
        if (response.pois.count == request.offset){
            [self.tableView.mj_footer endRefreshing];
        }else{
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        
    }
    
    
    for (AMapPOI *p in response.pois){
        [self.dataSource addObject:p];
    }
    [self.tableView reloadData];
    
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
//    if (!self.location) {
//        self.location = [locations firstObject];
//        
//        WEAK_SELF;
//        CLGeocoder *geocoder = [[CLGeocoder alloc]init];
//        [geocoder reverseGeocodeLocation:_location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
//            
//            for (CLPlacemark *placemark in placemarks){
//                weakSelf.currentCity = placemark.locality;
//                
//                [weakSelf.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
//            }
//        }];
//        
//        
//        
//        self.request.location = [AMapGeoPoint locationWithLatitude:self.location.coordinate.latitude longitude:self.location.coordinate.longitude];
//        self.request.page = 1;
//        
//        [self.searchAPI AMapPOIAroundSearch:self.request];
//    }
}


#pragma mark -
#pragma mark search bar

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    [self searchWithText:searchBar.text];
    return YES;
}

-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    
    return YES;
}


- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
}




#pragma mark -
#pragma mark  tableview

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    if ([self.delegate respondsToSelector:@selector(didSelectAddress:city:)]) {
//        if (indexPath.row == 0){
//            [self.delegate didSelectAddress:nil city:nil];
//        }else if (indexPath.row == 1) {
//            AMapPOI *poi = [[AMapPOI alloc]init];
//            poi.uid = self.currentCity;
//            poi.location = [AMapGeoPoint locationWithLatitude:self.location.coordinate.latitude longitude:self.location.coordinate.longitude];
//            [self.delegate didSelectAddress:poi city:self.currentCity];
//        }else{
//            AMapPOI *poi = [self.dataSource objectAtIndex:indexPath.row - 2];
//            [self.delegate didSelectAddress:poi city:self.currentCity];
//        }
//        
//    }
    
    if ([self.delegate respondsToSelector:@selector(searchResult:didSelectPOI:)]) {
        [self.delegate searchResult:self didSelectPOI:self.dataSource[indexPath.row]];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataSource.count ;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    }
    
    AMapPOI *poi = [self.dataSource objectAtIndex:indexPath.row ];
    cell.textLabel.attributedText = [self showGreenText:poi.name];
    cell.detailTextLabel.attributedText = [self showGreenText:poi.address];
    
//    if ([self.selectPOI.uid isEqualToString:poi.uid]){
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
//,l    }
    
    
    
    return cell;
}


-(NSMutableAttributedString *)showGreenText:(NSString *)title{
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc]initWithString:title];
    [att addAttribute:NSForegroundColorAttributeName value:AppBlue range:[title rangeOfString:originSearchText]];
    return att;
}


@end
