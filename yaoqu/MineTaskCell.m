//
//  MineTaskCell.m
//  yaoqu
//
//  Created by ridchan on 2017/8/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineTaskCell.h"
#import "TaskApi.h"

@interface MineTaskCell()

@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UIButton *contentBtn;

@end

@implementation MineTaskCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.bottom.equalTo(self.contentView.mas_centerY).offset(-2);
    }];
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.top.equalTo(self.contentView.mas_centerY).offset(2);
    }];
    
    [self.contentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(60, 25));
    }];
}

-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        _titleLbl.font = SystemFont(14);
        [self.contentView addSubview:_titleLbl];
    }
    return _titleLbl;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.font = SystemFont(12);
        _descLbl.textColor = AppGray;
        [self.contentView addSubview:_descLbl];
    }
    return _descLbl;
}

-(UIButton *)contentBtn{
    if (!_contentBtn) {
        _contentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _contentBtn.titleLabel.font = SystemFont(12);
        _contentBtn.layer.borderColor = [UIColor orangeColor].CGColor;
        [_contentBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        _contentBtn.layer.borderWidth = 1.0;
        [_contentBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_contentBtn];
    }
    return _contentBtn;
}

-(void)setModel:(TaskModel *)model{
    _model = model;
    
    self.titleLbl.text = model.name;
    
    NSString *str1 = [model.reward.exp integerValue] > 0 ? [NSString stringWithFormat:@"经验 + %@",model.reward.exp] : @"";
    NSString *str2 = [model.reward.giftroll integerValue] > 0 ? [NSString stringWithFormat:@"礼物券 + %@",model.reward.giftroll] : @"";
    NSString *str3 = [model.reward.integral integerValue] > 0 ? [NSString stringWithFormat:@"积分 + %@",model.reward.integral] : @"";
    
    self.descLbl.text = [NSString stringWithFormat:@"%@   %@   %@",str1,str2,str3];
    
    [self setState];
}

-(void)setState{
    if (self.model.state) {
        _contentBtn.layer.borderColor = [UIColor orangeColor].CGColor;
        [_contentBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        if (self.model.is_get) {
            [_contentBtn setTitle:LS(@"已领取") forState:UIControlStateNormal];
        }else{
            [_contentBtn setTitle:LS(@"领取奖励") forState:UIControlStateNormal];
        }
    }else{
        _contentBtn.layer.borderColor = AppGray.CGColor;
        [_contentBtn setTitleColor:AppGray forState:UIControlStateNormal];
        
        if ([self.model.target integerValue] > 1){
            [_contentBtn setTitle:[NSString stringWithFormat:@"%ld/%ld",[self.model.surplus integerValue],[self.model.target integerValue]] forState:UIControlStateNormal];
        }else{
            [_contentBtn setTitle:LS(@"未完成") forState:UIControlStateNormal];
        }
    }
}

-(void)btnClick:(id)sender{
    if (self.model.state) {
        if (!self.model.is_get) {
            WEAK_SELF;
            TaskApi *api = [[TaskApi alloc]initWitObject:@{@"id":self.model.id}];
            api.method = YTKRequestMethodPOST;
            [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
                weakSelf.model.is_get = info.error == ErrorCodeType_None;
                [weakSelf setState];
            }];
        }
    }
}

@end
