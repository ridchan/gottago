//
//  BaseApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/15.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YTKNetwork.h"
#import "MsgModel.h"


typedef void(^NetWorkBlock)(MsgModel *info,id responseObj);

typedef NS_ENUM(NSInteger,APIType){
    APIType_List,
    APIType_Delete,
    APIType_Clear
};

extern ErrorCodeType lastErrorCodeType;
extern NSString *outDateCode;


@interface BaseApi : YTKRequest

@property(nonatomic,strong) NSMutableDictionary *baseConfig;
@property(nonatomic,strong) NSMutableDictionary *params;

@property(nonatomic) YTKRequestMethod method;
@property(nonatomic) APIType apiType;

-(id)initWitObject:(id)obj;

+(instancetype)sharedInstance;

-(void)startWithCompleteBlock:(NetWorkBlock)block;




@end
