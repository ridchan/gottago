//
//  DynamicDetailApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/29.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicDetailApi.h"

@implementation DynamicDetailApi

-(id)initWithDynamic_id:(NSString *)dynamic_id{
    if (self = [super init]) {
        [self.params setValue:dynamic_id forKey:@"dynamic_id"];
    }
    return self;
}

-(id)initWithDynamic_id:(NSString *)dynamic_id comment_id:(NSString *)comment_id{
    if (self = [super init]) {
        [self.params setValue:dynamic_id forKey:@"dynamic_id"];
        [self.params setValue:comment_id forKey:@"comment_id"];
    }
    return self;
}

-(NSString *)requestUrl{
    return DynamicDetailUrl;
}



@end
