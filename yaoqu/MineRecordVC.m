//
//  MineRecordVC.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/9.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "MineRecordVC.h"
#import "RechargeApi.h"
#import "RechargeModel.h"

@interface MineRecordVC ()


@property(nonatomic) RechargeType rechargeType;

@end

@implementation MineRecordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = LS(@"充值记录");
    
    self.rechargeType = [self.paramObj integerValue];
    
    [self startRefresh];
}


-(void)startRefresh{
    WEAK_SELF;
    self.page = 1;
    
    NSDictionary *dict = @{@"page":@(self.page),
                           @"size":@(self.size),
                           @"type":@(self.rechargeType)
                           };
    [[[RechargeApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        NSArray *array = [RechargeModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        [weakSelf refreshComplete:info response:array];
    }];

}


-(void)startloadMore{
    WEAK_SELF;
    NSDictionary *dict = @{@"page":@(++self.page),
                           @"size":@(self.size),
                           @"type":@(self.rechargeType)
                           };
    
    [[[RechargeApi alloc]initWitObject:dict] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        NSArray *array = [RechargeModel objectArrayWithKeyValuesArray:[responseObj objectForKey:@"data"]];
        [weakSelf loadMoreComplete:info response:array];
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
    return [tableView cellHeightForIndexPath:indexPath model:nil keyPath:nil cellClass:[NormalTableViewCell class] contentViewWidth:APP_WIDTH - 20];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NormalTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil ) {
        cell = [[NormalTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        cell.textLabel.font = UserNameFont;
        cell.detailTextLabel.font = DetailFont;
        cell.detailTextLabel.textColor = AppGray;
        cell.celltype = NormalCellType_Detail;
    }
    RechargeModel *model = [self.datas objectAtIndex:indexPath.row];
    cell.textLabel.text = model.desc;
    cell.detailTextLabel.text = model.time;
    cell.detailLbl.text = model.val;    
    
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
