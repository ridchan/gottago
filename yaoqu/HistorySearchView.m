//
//  HistorySearchView.m
//  yaoqu
//
//  Created by ridchan on 2017/9/3.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "HistorySearchView.h"
#import "YJTagView.h"
#import "SearchApi.h"
#import "HistoryModel.h"
@interface HistorySearchView()<YJTagViewDataSource,YJTagViewDelegate>

@property(nonatomic,strong) YJTagView *tagView;

@end

@implementation HistorySearchView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.clipsToBounds = YES;
        [self commonInit];
    }
    return self;
}

-(void)setTitleArray:(NSArray *)titleArray{
    _titleArray = titleArray;
//    if ([self.titleArray count] == 0) {
//        [self.tagView mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.mas_left);
//            make.top.equalTo(self.mas_top);
//            make.right.equalTo(self.mas_right);
//            make.height.mas_equalTo(0);
//            make.bottom.equalTo(self.mas_bottom);
//        }];
//    }else{
//        [self.tagView mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.mas_left).offset(10);
//            make.top.equalTo(self.titleLbl.mas_bottom).offset(20);
//            make.right.equalTo(self.mas_right).offset(-10);
//            make.height.mas_equalTo(0);
//            make.bottom.equalTo(self.mas_bottom);
//        }];
//    }
    
    [self.tagView reloadData];
    [self.tagView layoutSubviews];
}

-(void)commonInit{
    
    
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.mas_top).offset(10);
    }];
    
    [self.delBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right);
        make.size.mas_equalTo(CGSizeMake(50, 20));
        make.centerY.equalTo(self.titleLbl.mas_centerY);
    }];
    
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.mas_top);
        make.right.equalTo(self.mas_right).offset(-10);
        make.height.mas_equalTo(0);
        make.bottom.equalTo(self.mas_bottom);
    }];
    
    [RACObserve(self.tagView, viewHeight) subscribeNext:^(id x) {
        if ([x floatValue] == 0){
            [self.tagView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.mas_left);
                make.top.equalTo(self.mas_top);
                make.right.equalTo(self.mas_right);
                make.height.mas_equalTo(0);
                make.bottom.equalTo(self.mas_bottom);
            }];
        }else{
            [self.tagView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.mas_left).offset(10);
                make.top.equalTo(self.titleLbl.mas_bottom).offset(20);
                make.right.equalTo(self.mas_right).offset(-10);
                make.height.mas_equalTo([x floatValue]);
                make.bottom.equalTo(self.mas_bottom);
            }];
        }

    }];
    
    
}

-(YJTagView *)tagView{
    if (!_tagView) {
        _tagView = [[YJTagView alloc]initWithFrame:CGRectZero];
//        _tagView = [[YJTagView alloc] initWithFrame:CGRectMake(15, 64, self.frame.size.width -15, 20)];
        _tagView.dataSource = self;
        _tagView.delegate = self;
        _tagView.themeColor = [UIColor whiteColor];
        _tagView.titleColor = AppBlack;
        _tagView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tagView.tagCornerRadius = 5;
        
        
        [self addSubview:_tagView];
    }
    return _tagView;
}

-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        _titleLbl.font = SystemFont(12);
        _titleLbl.textColor = AppBlack;
        _titleLbl.text = @"历史搜索";
        [self addSubview:_titleLbl];
    }
    return _titleLbl;
}

-(UIButton *)delBtn{
    if (!_delBtn) {
        _delBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_delBtn setImage:[UIImage imageNamed:@"ic_delete"] forState:UIControlStateNormal];
        [_delBtn addTarget:self action:@selector(deleteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_delBtn];
    }
    return _delBtn;
}


- (NSInteger)numOfItems {
    return self.titleArray.count;
}

- (NSString *)tagView:(YJTagView *)tagView titleForItemAtIndex:(NSInteger)index {
    HistoryModel *model = self.titleArray[index];
    return model.keyword;
}

- (void)tagView:(YJTagView *)tagView didSelectedItemAtIndex:(NSInteger)index {
    if ([self.delegate respondsToSelector:@selector(historyViewDidSelect:)]) {
        [self.delegate historyViewDidSelect:self.titleArray[index]];
    }
//    NSLog(@"点击%@", self.titleArray[index]);
}

-(void)deleteBtnClick:(id)sender{
    if ([self.delegate respondsToSelector:@selector(historyViewDeleteBtnClick:)]) {
        [self.delegate historyViewDeleteBtnClick:nil];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
