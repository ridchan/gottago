//
//  DynamicCommentLikeApi.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/10.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "BaseApi.h"

@interface DynamicCommentLikeApi : BaseApi

-(id)initWithComment:(NSString *)comment_id like:(BOOL)is_like;

@end
