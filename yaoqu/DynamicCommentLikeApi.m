//
//  DynamicCommentLikeApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/10.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DynamicCommentLikeApi.h"

@implementation DynamicCommentLikeApi{
    BOOL _is_like;
}

-(id)initWithComment:(NSString *)comment_id like:(BOOL)is_like{
    if (self = [super init]) {
        [self.params setValue:comment_id forKey:@"comment_id"];
        _is_like = is_like;
    }
    return self;
}


-(NSString *)requestUrl{
    return _is_like ? DynamicCommentLikeUrl : DynamicCommentUnLikeUrl;
}





@end
