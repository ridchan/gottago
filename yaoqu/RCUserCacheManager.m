//
//  RCUserCacheManager.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "RCUserCacheManager.h"
#import "LKDBHelper.h"
#import "UserInfoGetApi.h"
#import "EMClient.h"
#import "RouteController.h"
#import "ChatUserInfoGetApi.h"
#import "UserInfoGetApi.h"
#import "CommonApi.h"
#import "MemberApi.h"
#import <JPUSHService.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import "BubbleApi.h"

#define HuanXinPassword @"@&1234567"

#define HuanXinLoginSuccess @"HuxanLoginSuccessNotification"

@interface RCUserCacheManager()<CLLocationManagerDelegate>

@property(nonatomic,strong) NSMutableDictionary *userCache;
@property(nonatomic,strong) CLLocationManager *locationManager;
@property(nonatomic,strong) CLLocation *location;
@property(nonatomic) AMapReGeocodeSearchRequest *request;

@end

@implementation RCUserCacheManager

@synthesize currentUser = _currentUser;
@synthesize currentLoginInfo = _currentLoginInfo;
@synthesize currentToken = _currentToken;
@synthesize currentCity = _currentCity;
@synthesize lastBubble = _lastBubble;
@synthesize currentBubble = _currentBubble;

CREATE_SHARED_MANAGER(RCUserCacheManager)


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.userCache = [NSMutableDictionary dictionary];
        
    }
    return self;
}

-(void)startLocation{
    self.location = nil;
    self.locationManager = [[CLLocationManager alloc]init];
    if ([CLLocationManager locationServicesEnabled]) {
        //IOS8以及以上版本需要设置，弹出是否允许使用定位提示
        if ([[UIDevice currentDevice].systemVersion floatValue] > 8.0) {
            [_locationManager requestWhenInUseAuthorization];
        }
        _locationManager.delegate = self;
        
        [_locationManager startUpdatingLocation];
    }else{
        [self loadCurrentCity:nil];
    }

}

-(void)getAddress{
    self.location = nil;
    self.locationManager = [[CLLocationManager alloc]init];
    if ([CLLocationManager locationServicesEnabled]) {
        //IOS8以及以上版本需要设置，弹出是否允许使用定位提示
        if ([[UIDevice currentDevice].systemVersion floatValue] > 8.0) {
            [_locationManager requestWhenInUseAuthorization];
        }
        _locationManager.delegate = self;
        
        [_locationManager startUpdatingLocation];
    }
    
}


-(void)loadCurrentCity:(NSDictionary *)location{
    WEAK_SELF;
    [[[CommonApi alloc]initWithModel:@"common/current_city" object:location] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            weakSelf.currentCity = [CityModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
        }
    }];

}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    [self loadCurrentCity:nil];
    [self.locationManager stopUpdatingLocation];
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    if (!self.location) {
        self.location = [locations firstObject];
        [self.locationManager stopUpdatingLocation];
        self.locationManager = nil;
        
        NSDictionary *dict = @{@"longitude":@(self.location.coordinate.longitude),
                               @"latitude":@(self.location.coordinate.latitude)
                               };
        [self loadCurrentCity:dict];
        
        WEAK_SELF;
        CLGeocoder *geocoder = [[CLGeocoder alloc]init];
        [geocoder reverseGeocodeLocation:_location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
            
            for (CLPlacemark *placemark in placemarks){
                
                CityModel *model = [[CityModel alloc]init];
                model.latitude = [[NSNumber numberWithDouble:_location.coordinate.latitude] stringValue];
                model.longitude = [[NSNumber numberWithDouble:_location.coordinate.longitude] stringValue];
                model.city_name = placemark.locality;
                model.address = [NSString stringWithFormat:@"%@%@",NoNilString(placemark.subLocality),NoNilString(placemark.name)];
                NSLog(@"位置信息 %@,%@,%@,%@,%@,%@,%@,%@,%@",placemark.subLocality,placemark.subLocality,placemark.administrativeArea,placemark.subAdministrativeArea,placemark.thoroughfare,placemark.subThoroughfare,placemark.name,placemark.inlandWater,placemark.areasOfInterest);
                weakSelf.addressPoi = model;
            }
        }];
        
        

    }
}

-(void)openLink:(NSString *)link{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    
    self.webView = [[UIWebView alloc]init];

    
    link = [NSString stringWithFormat:@"%@&access_token=%@",link,
                 [RCUserCacheManager sharedManager].currentToken.access_token];
    
    
    NSString *oldAgent = [self.webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    
    
    NSString *customUserAgent = nil;
    
    
    if ([oldAgent rangeOfString:@"gottago"].location == NSNotFound){
        customUserAgent = [NSString stringWithFormat:@"%@( gottago )",oldAgent];
    }else{
        customUserAgent = oldAgent;
    }
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"UserAgent":customUserAgent}];
    
    
    
    NSURL *url = [NSURL URLWithString:link];
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    

}

#pragma mark -


-(void)bubbleChange{
//    RACSignal *signal1 = RACObserve(self, currentBubble.message_system);
//    RACSignal *signal2 = RACObserve(self, currentBubble.message_like);
//    RACSignal *signal3 = RACObserve(self, currentBubble.message_comment);
//    RACSignal *signal4 = RACObserve(self, currentBubble.gift_send);
}

#pragma mark -
#pragma mark 保存各种信息

-(void)fetchBubble{
    WEAK_SELF;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [[[BubbleApi alloc] initWitObject:@{@"type":@"message"}] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
            if (info.error == ErrorCodeType_None) {
                weakSelf.currentBubble = [BubbleModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
                if (!weakSelf.lastBubble){
                    NSLog(@"设置之前 %@",[weakSelf.currentBubble keyValues]);
                    weakSelf.currentBubble.member_id = weakSelf.currentUser.member_id;
                    weakSelf.lastBubble = weakSelf.currentBubble;
                }
            }
        }];
    });
    
}

-(void)setLastBubble:(BubbleModel *)lastBubble{
    if (lastBubble) {
        [BubbleModel deleteWithWhere:[NSString stringWithFormat:@"member_id='%@'",self.currentUser.member_id]];
        _lastBubble = lastBubble;
        [_lastBubble updateToDB];
    }

}


-(BubbleModel *)lastBubble{
    if (!_lastBubble) {
        
        BubbleModel *curModel = [[BubbleModel searchWithWhere:[NSString stringWithFormat:@"member_id='%@'",self.currentUser.member_id]] firstObject];
        _lastBubble = curModel;
        NSLog(@"获取之前 %@",[_lastBubble keyValues]);
    }else{
        
    }
    return _lastBubble;
}



-(void)setCurrentLoginInfo:(LoginInfoModel *)currentLoginInfo{
    [LoginInfoModel deleteWithWhere:@"1 = 1"];
    if (currentLoginInfo) {
        _currentLoginInfo = currentLoginInfo;
        [_currentLoginInfo updateToDB];
    }
    
    
}

-(LoginInfoModel *)currentLoginInfo{
    if (!_currentLoginInfo) {
        
        LoginInfoModel *curModel = [LoginInfoModel searchSingleWithWhere:@"1 = 1" orderBy:@"client_id asc"];
        _currentLoginInfo = curModel;
    }else{
        
    }
    return _currentLoginInfo;
}


-(void)setCurrentToken:(AccessTokenModel *)currentToken{
    [AccessTokenModel deleteWithWhere:@" 1 = 1"];
    if (currentToken) {
        _currentToken = currentToken;
        [_currentToken updateToDB];
    }else{
        _currentToken = nil;
    }
}

-(AccessTokenModel *)currentToken{
    if (!_currentToken) {
        AccessTokenModel *curModel = [AccessTokenModel searchSingleWithWhere:@"1 = 1" orderBy:@"access_token asc"];
        _currentToken = curModel;
    }
    return _currentToken;
}

-(void)setCurrentUser:(UserModel *)currentUser{
    [CurrentUserModel deleteWithWhere:@" 1 = 1"];
    if (currentUser) {
        _currentUser = currentUser;
        [[_currentUser objectWithClass:@"CurrentUserModel"] updateToDB];

        
        
        [self setRegistID];
        [self huanXinAutoLogin];
        [self fetchBubble];
        
    }else{
        [self clearRegistID];
        [self huxnXinAoutLogout];
        self.currentBubble = nil;
        self.currentToken = nil;
        self.lastBubble = nil;
        [[RouteController sharedManager] openLoginController];
    }
    
}



-(UserModel *)currentUser{
    if (!_currentUser) {
        
        CurrentUserModel *curModel = [CurrentUserModel searchSingleWithWhere:@"1 = 1" orderBy:@"member_id asc"];
        if (curModel != nil){
            _currentUser = [curModel objectWithClass:@"UserModel"];
            [self renewAccount];
        }
    }else{
        
    }
    return _currentUser;
}


-(CityModel *)currentCity{
    if (!_currentCity) {
        _currentCity = [CityModel searchSingleWithWhere:@" 1 = 1" orderBy:@"city_id desc"];
    }
    return _currentCity;
}

-(void)setCurrentCity:(CityModel *)currentCity{
    [CityModel deleteWithWhere:@"1 = 1"];
    if (currentCity) {
        _currentCity = currentCity;
        [_currentCity updateToDB];
    }
}


-(void)clearRegistID{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:@"" forKey:@"registration_id"];
    MemberApi *api = [[MemberApi alloc] initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        
    }];
}

-(void)setRegistID{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:[JPUSHService registrationID] forKey:@"registration_id"];
    MemberApi *api = [[MemberApi alloc] initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        NSLog(@"设置推送信息 %@",responseObj);
    }];
}

#pragma mark -
#pragma mark -----


-(void)renewAccessToken{
    NSLog(@"自动更新accessToken");
    WEAK_SELF;
    [[[CommonApi alloc]initWithModel:@"accessToken" object:self.currentLoginInfo] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            weakSelf.currentToken = [AccessTokenModel objectWithKeyValues:responseObj];
        }else{
            [weakSelf huxnXinAoutLogout];
            [[RouteController sharedManager] openLoginController];
            [[[UIAlertView alloc]initWithTitle:nil message:@"Token 过期，请重新登陆" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil] show];
        }
    }];
}


-(void)renewAccount{
    WEAK_SELF;
    [[[MemberApi alloc]init] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        if (info.error == ErrorCodeType_None) {
            weakSelf.currentUser = [CurrentUserModel objectWithKeyValues:[responseObj objectForKey:@"data"]];
//            [weakSelf.currentUser setValuesForKeysWithDictionary:[responseObj objectForKey:@"data"]];
        }
    }];
}


-(UserModel *)cacheUser:(NSString *)member_id{
    NSString *trueID = [self trueID:member_id];
    UserModel *model = [self.userCache objectForKey:trueID];
    
    return [model.member_id integerValue] > 0 ? model : nil;
}

-(NSString *)trueID:(NSString *)member_id{
    if ([member_id integerValue] < 10000) return member_id;
    CGFloat trueid = [member_id floatValue] / 10000;
    return [NSString stringWithFormat:@"%.0f",trueid];
}

-(void)getUserWithID:(NSString *)member_id block:(BaseBlock)block{
    
    NSString *trueID = [self trueID:member_id];
    if (trueID == 0) return;
    
    if ([self.userCache objectForKey:trueID]) {
        block([self.userCache objectForKey:trueID]);
    }else{
        __block UserModel *userModel = [[UserModel alloc]init];
        [self.userCache setValue:userModel forKey:trueID];
        [self fillUserWithMemberID:trueID model:userModel];
        block(userModel);
    }
}

//完善信息
-(void)fillUserWithMemberID:(NSString *)member_id model:(UserModel *)userModel{
    
    WEAK_SELF;
    [[[MemberApi alloc]initWitObject:@{@"member_id":@([member_id integerValue])}] startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        
        NSLog(@"完善用户信息 ID：%@ ============= \n %@ ================ \n",member_id,responseObj);
        dispatch_async(dispatch_get_main_queue(), ^{
            if (info.error == ErrorCodeType_None) {
                [userModel setValuesForKeysWithDictionary:[responseObj objectForKey:@"data"]];
       
            }else{
                //失败重试
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(30.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf fillUserWithMemberID:member_id model:userModel];
                });
            }
            userModel.autoid = [userModel.autoid autoIncrice];
        });
        
    }];

}

-(void)huanXinAutoLogin{
    WEAK_SELF;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        EMError *error = [[EMClient sharedClient] loginWithUsername:weakSelf.currentUser.huanXinID password:HuanXinPassword];
        if (!error) {
            NSLog(@"登陆成功");
        }else{
            NSLog(@"error %@",error.errorDescription);
            if (error.code == EMErrorUserNotFound) {
                [[EMClient sharedClient] asyncRegisterWithUsername:weakSelf.currentUser.huanXinID password:HuanXinPassword success:^{
                    NSLog(@"注册成功");
                    PostNotification(HuanXinLoginSuccess, nil);
                } failure:^(EMError *aError) {
                    
                }];
            }
        }
    });
}

-(void)huxnXinAoutLogout{
    [[EMClient sharedClient] asyncLogout:YES success:^{
        
    } failure:^(EMError *aError) {
        
    }];
}

@end
