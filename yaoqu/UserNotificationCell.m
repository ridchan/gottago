//
//  UserNotificationCell.m
//  yaoqu
//
//  Created by ridchan on 2017/7/23.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserNotificationCell.h"
#import "MessageNotificationModel.h"
@interface UserNotificationCell()

@property(nonatomic,strong) UILabel *titleLbl;
@property(nonatomic,strong) UILabel *descLbl;
@property(nonatomic,strong) UILabel *dateLbl;
@property(nonatomic,strong) UIView *bottomLine;

@end

@implementation UserNotificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self commonInit];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)commonInit{
    self.titleLbl.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .topSpaceToView(self.contentView, 10)
    .heightIs(20);
    
    [self.titleLbl setSingleLineAutoResizeWithMaxWidth:200];
    
    self.dateLbl.sd_layout
    .rightSpaceToView(self.contentView, 10)
    .centerYEqualToView(self.titleLbl)
    .heightIs(20)
    .autoWidthRatio(0);
    [self.dateLbl setSingleLineAutoResizeWithMaxWidth:100];
    
    self.descLbl.sd_layout
    .leftSpaceToView(self.contentView, 10)
    .rightSpaceToView(self.contentView, 10)
    .topSpaceToView(self.titleLbl, 10)
    .autoHeightRatio(0);
    
    self.bottomLine.sd_layout
    .leftEqualToView(self.contentView)
    .rightEqualToView(self.contentView)
    .bottomEqualToView(self.contentView)
    .heightIs(0.5);
    
    [self setupAutoHeightWithBottomView:self.descLbl bottomMargin:10];
}

-(UIView *)bottomLine{
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc]init];
        _bottomLine.backgroundColor = AppLineColor;
        [self.contentView addSubview:_bottomLine];
    }
    
    return _bottomLine;
    
}

-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [ControllerHelper autoFitLabel];
        _titleLbl.font = SystemFont(16);
        [self.contentView addSubview:_titleLbl];
    }
    return _titleLbl;
}

-(UILabel *)descLbl{
    if (!_descLbl) {
        _descLbl = [ControllerHelper autoFitLabel];
        _descLbl.font = SystemFont(14);
        _descLbl.numberOfLines = 0;
        _descLbl.textColor = AppGray;
        [self.contentView addSubview:_descLbl];
    }
    return _descLbl;
}

-(UILabel *)dateLbl{
    if (!_dateLbl) {
        _dateLbl = [ControllerHelper autoFitLabel];
        _dateLbl.font = SystemFont(12);
        _dateLbl.textColor = AppGray;
        [self.contentView addSubview:_dateLbl];
    }
    return _dateLbl;
}

-(void)setModel:(MessageNotificationModel *)model{
    _model = model;
    self.titleLbl.text = _model.title;
    self.descLbl.text = _model.desc;
    self.dateLbl.text =  [NSString stringFromTimeMark:_model.time format:kDateFormatTypeDDMM_CN];
}

@end
