//
//  DiscoverySearchResultVC.m
//  yaoqu
//
//  Created by ridchan on 2017/9/4.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "DiscoverSearchAllVC.h"
#import "DiscoverySearchUserCell.h"
#import "TravelScheduleListCell.h"
#import "DynamicCell.h"
#import "SearchApi.h"
#import "UserModel.h"
#import "TravelScheduleModel.h"
#import "DynamicModel.h"
#import "BaseNavigationController.h"
#import "TravelScheduleDetailVC.h"
#import "DiscoverySearchResultVC.h"

@interface DiscoverSearchAllVC ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,DiscoverySearchUserCellDelegate>

@property(nonatomic,strong) UIView *searchView;
@property(nonatomic,strong) UITextField *textField;
@property(nonatomic,strong) UIView *typeView;

@property(nonatomic,strong) NSArray *members;
@property(nonatomic,strong) NSArray *travels;
@property(nonatomic,strong) NSArray *dynamics;

@property(nonatomic) BOOL isSection1More;
@property(nonatomic) BOOL isSection2More;

@property(nonatomic,strong) UIButton *selectBtn;

@end

@implementation DiscoverSearchAllVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.view endEditing:YES];
    self.navigationItem.titleView = self.searchView;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.separatorInset = UIEdgeInsetsMake(10, 0, 0, 0);
    self.tableView.separatorColor = AppLineColor;
    
    
    
    self.textField.text = self.paramObj;
    
    [self.tableView registerClass:[DiscoverySearchUserCell class] forCellReuseIdentifier:@"Cell1"];
    [self.tableView registerClass:[TravelScheduleListCell class] forCellReuseIdentifier:@"Cell2"];
    [self.tableView registerClass:[DynamicCell class] forCellReuseIdentifier:@"Cell3"];
    
    
    WEAK_SELF;
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setValue:self.paramObj forKey:@"keyword"];
    
    SearchApi *api = [[SearchApi alloc]initWitObject:dict];
    api.method = YTKRequestMethodPOST;
    [api startWithCompleteBlock:^(MsgModel *info, id responseObj) {
        [weakSelf refreshComplete:info response:nil];
        if (info.error == ErrorCodeType_None) {
            weakSelf.members = [UserModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"member"]];
            weakSelf.dynamics = [DynamicModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"dynamic"]];
            weakSelf.travels = [TravelScheduleModel objectArrayWithKeyValuesArray:[[responseObj objectForKey:@"data"] objectForKey:@"travel_schedule"]];
            [weakSelf.tableView reloadData];
        }
    }];
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.backBarButtonItem = nil;
    self.navigationItem.title = nil;
    [super viewWillAppear:animated];
    
}


-(UIView *)typeView{
    if (!_typeView) {
        _typeView = [[UIView alloc]init];
        _typeView.backgroundColor = [UIColor whiteColor];
        
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setTitle:@"全部" forState:UIControlStateNormal];
        [button1 setTitleColor:AppGray forState:UIControlStateNormal];
        [button1 setTitleColor:AppOrange forState:UIControlStateSelected];
        button1.titleLabel.font = SystemFont(12);
        [button1 addTarget:self action:@selector(btnTypeClick:) forControlEvents:UIControlEventTouchUpInside];
        button1.tag = 99;
        button1.selected = YES;
        self.selectBtn = button1;
        [_typeView addSubview:button1];
        
        UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button2 setTitle:@"用户" forState:UIControlStateNormal];
        [button2 setTitleColor:AppGray forState:UIControlStateNormal];
        [button2 setTitleColor:AppOrange forState:UIControlStateSelected];
        button2.titleLabel.font = SystemFont(12);
        button2.tag = 0;
        [button2 addTarget:self action:@selector(btnTypeClick:) forControlEvents:UIControlEventTouchUpInside];
        [_typeView addSubview:button2];
        
        UIButton *button3 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button3 setTitle:@"旅游" forState:UIControlStateNormal];
        [button3 setTitleColor:AppGray forState:UIControlStateNormal];
        [button3 setTitleColor:AppOrange forState:UIControlStateSelected];
        button3.titleLabel.font = SystemFont(12);
        button3.tag = 1;
        [button3 addTarget:self action:@selector(btnTypeClick:) forControlEvents:UIControlEventTouchUpInside];
        [_typeView addSubview:button3];
        
        UIButton *button4 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button4 setTitle:@"动态" forState:UIControlStateNormal];
        [button4 setTitleColor:AppGray forState:UIControlStateNormal];
        [button4 setTitleColor:AppOrange forState:UIControlStateSelected];
        button4.titleLabel.font = SystemFont(12);
        button4.tag = 2;
        [button4 addTarget:self action:@selector(btnTypeClick:) forControlEvents:UIControlEventTouchUpInside];
        [_typeView addSubview:button4];
        
        [button1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_typeView.mas_left);
            make.top.equalTo(_typeView.mas_top);
            make.bottom.equalTo(_typeView.mas_bottom);
            make.width.equalTo(_typeView.mas_width).multipliedBy(0.25);
        }];
        
        [button2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(button1.mas_right);
            make.top.equalTo(_typeView.mas_top);
            make.bottom.equalTo(_typeView.mas_bottom);
            make.width.equalTo(_typeView.mas_width).multipliedBy(0.25);
        }];
        
        [button3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(button2.mas_right);
            make.top.equalTo(_typeView.mas_top);
            make.bottom.equalTo(_typeView.mas_bottom);
            make.width.equalTo(_typeView.mas_width).multipliedBy(0.25);
        }];
        
        [button4 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(button3.mas_right);
            make.top.equalTo(_typeView.mas_top);
            make.bottom.equalTo(_typeView.mas_bottom);
            make.width.equalTo(_typeView.mas_width).multipliedBy(0.25);
        }];
        
        [[_typeView lineInColor:RGB16(0xe8e8e8)] mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_typeView.mas_left);
            make.right.equalTo(_typeView.mas_right);
            make.bottom.equalTo(_typeView.mas_bottom);
            make.height.mas_equalTo(0.5);
        }];
        
        
        [self.view addSubview:_typeView];
    }
    return _typeView;
}


-(void)btnTypeClick:(UIButton *)button{
    if (button != self.selectBtn) {
        self.selectBtn.selected = NO;
        button.selected = YES;
        self.selectBtn = button;
        
        //        switch (button.tag) {
        //            case 0:
        //            case 1:
        //            case 2:
        //                self.selectType = [NSString intValue:button.tag];
        //                break;
        //            default:
        //                self.selectType = nil;
        //                break;
        //        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *)searchView{
    if (!_searchView) {
        _searchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 44)];
        
        _textField = [[UITextField alloc]init];
        _textField.leftViewMode = UITextFieldViewModeAlways;
        _textField.layer.masksToBounds = YES;
        _textField.layer.cornerRadius = 3.0;
        _textField.leftView = [self leftView];
        _textField.placeholder = @"搜索";
        _textField.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _textField.font = SystemFont(12);
        _textField.delegate = self;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.returnKeyType = UIReturnKeySearch;
        
        [_searchView addSubview:_textField];
        
        UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        [cancelBtn setTitleColor:AppBlack forState:UIControlStateNormal];
        cancelBtn.titleLabel.font = SystemFont(14);
        [cancelBtn addTarget:self action:@selector(cancelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_searchView addSubview:cancelBtn];
        
        UIImageView *backBtn = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_back"]];
        backBtn.userInteractionEnabled = YES;
        backBtn.contentMode = UIViewContentModeScaleAspectFit;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelBtnClick:)];
        [backBtn addGestureRecognizer:tap];
        [_searchView addSubview:backBtn];
        
        [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_searchView.mas_left).offset(5);
            make.top.equalTo(_searchView.mas_top).offset(5);
            make.bottom.equalTo(_searchView.mas_bottom).offset(-5);
            make.width.mas_equalTo(20);
        }];
        
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(backBtn.mas_right).offset(5);
            make.height.mas_equalTo(30);
            make.centerY.equalTo(_searchView.mas_centerY);
            make.right.equalTo(cancelBtn.mas_left).offset(-10);
        }];
        
        [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_searchView.mas_top).offset(3);
            make.bottom.equalTo(_searchView.mas_bottom).offset(-3);
            make.right.equalTo(_searchView.mas_right);
            make.width.mas_equalTo(50);
        }];
        
        
        
    }
    return _searchView;
}

-(UIView *)leftView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_search"]];
    imageView.frame = CGRectMake(7, 7, 16, 16);
    
    [view addSubview:imageView];
    
    return view;
}


#pragma mark -

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self.navigationController popViewControllerAnimated:NO];
    return NO;
}

-(void)cancelBtnClick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        [self.resultVC showAtIndex:1];
    }else if (indexPath.section == 1) {
        TravelScheduleDetailVC *vc = [[TravelScheduleDetailVC alloc]init];
        
        vc.model = self.travels[indexPath.row];
        
        BaseNavigationController *nav = [[BaseNavigationController alloc]initWithRootViewController:vc];
        
        [self.navigationController presentViewController:nav animated:YES completion:NULL];
    }else if (indexPath.section == 2){
        [self pushViewControllerWithName:@"DynamicDetailVC" params:self.dynamics[indexPath.row]];
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return nil;
    }else if (section == 1){
        return [self.travels count] > 0 ? [self sectionTopView:@"旅游套餐"] : nil;;
    }else{
        return [self.dynamics count] > 0 ? [self sectionTopView:@"动态"] : nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 0.0001;
    }else if (section == 1){
        return [self.travels count] == 0 ? 0.0001 : 60;
    }else{
        return [self.dynamics count] == 0 ? 0.0001 : 60.0;
    }
    
}



-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0){
        return 0.0001;
    }else if (section == 1){
        return _isSection1More ? 0.0001 :([self.travels count] == 0 ? 0.0001 : 30.0);
    }else{
        return _isSection2More ? 0.0001 :([self.dynamics count] == 0 ? 0.0001 : 30.0);
    }
}

-(UIView *)sectionTopView:(NSString *)title{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 60)];
    view.backgroundColor = [UIColor clearColor];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = view.bounds;
    [button addTarget:self action:@selector(moreBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *label = [[UILabel alloc]init];
    label.text = title;
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor = AppGray;
    label.font = SystemFont(17);
    label.frame = CGRectMake(10, 30, APP_WIDTH - 20, 30);
    
    [view addSubview:label];
    [view addSubview:button];
    return view;
}

-(UIView *)sectionBottomView:(NSInteger)tag{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, APP_WIDTH, 30)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = view.bounds;
    button.tag = tag;
    [button addTarget:self action:@selector(moreBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *label = [[UILabel alloc]init];
    label.text = @"查看更多>>";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = AppGray;
    label.font = SystemFont(14);
    label.frame = CGRectMake(10, 0, APP_WIDTH - 20, 30);
    
    [view addSubview:label];
    [view addSubview:button];
    return view;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return nil;
    }else if (section == 1){
        return _isSection1More ? nil :([self.travels count] > 2 ? [self sectionBottomView:1] : nil);
    }else{
        return _isSection2More ? nil : ([self.dynamics count] > 2 ? [self sectionBottomView:2] : nil);
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 100;
    }else if (indexPath.section == 1){
        return [tableView cellHeightForIndexPath:indexPath model:self.travels[indexPath.row] keyPath:@"model" cellClass:[TravelScheduleListCell class] contentViewWidth:APP_WIDTH];
    }else{
        return [tableView cellHeightForIndexPath:indexPath model:self.dynamics[indexPath.row] keyPath:@"model" cellClass:[DynamicCell class] contentViewWidth:APP_WIDTH];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return [self.members count] > 0 ? 1 :0;
    }else if (section == 1){
        return _isSection1More ? [self.travels count] : MIN([self.travels count],2);
    }else{
        return _isSection2More ? [self.dynamics count] : MIN([self.dynamics count],2);
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        DiscoverySearchUserCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell1" forIndexPath:indexPath];
        cell.members = self.members;
        cell.delegate = self;
        return cell;
        
    }else if (indexPath.section == 1){
        TravelScheduleListCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell2" forIndexPath:indexPath];
        cell.model = self.travels[indexPath.row];
        
        return cell;
    }else{
        
        DynamicCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell3" forIndexPath:indexPath];
        cell.model = self.dynamics[indexPath.row];
        
        return cell;
    }
    return nil;
}

-(void)discoverySearchUserCell:(id)obj moreBtnClick:(id)obj2{
    [self.resultVC showAtIndex:0];
}

-(void)moreBtnClick:(UIButton *)sender{
    if (sender.tag == 1) {
        [self.resultVC showAtIndex:1];
//        self.isSection1More = YES;
//        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationBottom];
    }else{
        [self.resultVC showAtIndex:2];
//        self.isSection2More = YES;
//        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationBottom];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
