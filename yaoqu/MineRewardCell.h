//
//  MineRewardCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/8/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MineRewardModel.h"
@interface MineRewardCell : UITableViewCell

@property(nonatomic,strong) MineRewardModel *model;

@end
