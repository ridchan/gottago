//
//  TravelDetailApi.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/7/14.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "TravelDetailApi.h"

@implementation TravelDetailApi

-(id)initWithTravel:(NSString *)travel_schedule_id{
    if (self = [super init]) {
        [self.params setValue:travel_schedule_id forKey:@"travel_schedule_id"];
    }
    return self;
}

-(NSString *)requestUrl{
    return TravelScheduleUrl;
}



@end
