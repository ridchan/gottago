//
//  NormalTableViewCell.h
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/22.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NormalTableViewCell : UITableViewCell

@property(nonatomic) NormalCellLineType type;
@property(nonatomic) NormalCellType celltype;

@property(nonatomic) BOOL bChecked;
@property(nonatomic,strong) NSString *isSwitch;

@property(nonatomic,strong) UIImageView *contentImage;
@property(nonatomic,strong) UILabel *textLbl;
@property(nonatomic,strong) UILabel *detailLbl;
@property(nonatomic,strong) UISwitch *switchBtn;
@property(nonatomic,strong) UIView *rightView;
@property(nonatomic,strong) UIImageView *checkImage;

+(instancetype)cellWithType:(NormalCellType)cellType identifier:(NSString *)identifier tableView:(UITableView *)tableView;

@end
