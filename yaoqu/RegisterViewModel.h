//
//  RegisterViewModel.h
//  QBH
//
//  Created by 陳景雲 on 2016/12/25.
//  Copyright © 2016年 ridchan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewModel.h"
#import "UserModel.h"
#import "LoginPostModel.h"


@interface RegisterViewModel : BaseViewModel

@property(nonatomic,strong) NSString *phoneNum;
@property(nonatomic) BOOL isFindPassword;

@property(nonatomic,strong) RACSignal *btnEnableSignal;

@property(nonatomic,strong) RACSignal *sendCodeSignal;

@property(nonatomic,strong) RACSignal *registSignal;

@property(nonatomic,strong) RACSubject *registSubject;

@property(nonatomic,strong) LoginPostModel *model;

-(void)regist;


@end
