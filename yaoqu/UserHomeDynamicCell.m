//
//  UserHomeDynamicCell.m
//  yaoqu
//
//  Created by 陳景雲 on 2017/6/27.
//  Copyright © 2017年 ridchan. All rights reserved.
//

#import "UserHomeDynamicCell.h"
#import "ImageCollectionViewCell.h"
#import "DynamicModel.h"

#pragma mark -
#pragma mark layout


@implementation UserHomeDynamicCellLayout

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.minimumLineSpacing = 5;
        self.minimumInteritemSpacing = 5;
        self.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
    }
    return self;
}

-(CGSize)itemSize{
    CGFloat maxWith = self.collectionView.frame.size.width - self.sectionInset.left - self.sectionInset.right;
    CGFloat width = (maxWith - self.minimumInteritemSpacing *  3) / 4;
    CGFloat height = self.collectionView.frame.size.height - self.sectionInset.top - self.sectionInset.bottom;
    CGFloat size = MIN(height, width);
    return CGSizeMake(size, size);
    
}

@end




#pragma mark -
#pragma makr Cell

@interface UserHomeDynamicCell()<UICollectionViewDelegate,UICollectionViewDataSource>


@property(nonatomic,strong) UIView *line;
@property(nonatomic,strong) UILabel *tipLbl;
@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) UIImageView *rightAccess;
@property(nonatomic,strong) NSMutableArray *dynamicImages;

@end

@implementation UserHomeDynamicCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.collectionView.userInteractionEnabled = NO;
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left).offset(10);
        make.height.mas_equalTo(40);
        make.right.equalTo(self.mas_right);
    }];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.titleLbl.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];
    
    [self.rightAccess mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-10);
        make.top.equalTo(self.line.mas_bottom);
        make.bottom.equalTo(self.mas_bottom);
        make.width.mas_equalTo(RightAccessWidth);
    }];
    


}

-(void)setDatas:(NSMutableArray *)datas{
    _datas = datas;
    [self resloveDynamicImage:datas];
    if (self.datas.count > 0){
        self.tipLbl.hidden = YES;
        if ([self.dynamicImages count] > 0){
            [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.mas_left);
                make.top.equalTo(self.line.mas_bottom);
                make.bottom.equalTo(self.mas_bottom);
                make.right.equalTo(self.rightAccess.mas_left);
            }];
        }else{
            self.tipLbl.hidden = NO;
            self.tipLbl.text = @"用户还没有发布照片";
//            [self.rightAccess mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.right.equalTo(self.mas_right).offset(-10);
//                make.top.equalTo(self.mas_top);
//                make.height.mas_equalTo(40);
//                make.width.mas_equalTo(RightAccessWidth);
//            }];
        }
    }else{
        self.tipLbl.hidden = NO;
        
        [self.tipLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left);
            make.top.equalTo(self.line.mas_bottom);
            make.bottom.equalTo(self.mas_bottom);
            make.right.equalTo(self.rightAccess.mas_left);
        }];
    }
    
    
    [self.collectionView reloadData];
}

-(void)resloveDynamicImage:(NSArray *)dynamicList{
    NSMutableArray *images = [NSMutableArray array];
    for (DynamicModel *model in dynamicList){
        NSArray *imgs = [model.content_images JSONObject];
        if ([imgs count] > 0) {
            [images addObject:imgs[0]];
        }
    }
    self.dynamicImages = images;
}

#pragma mark -
#pragma mark lazy controller

-(UILabel *)tipLbl{
    if (!_tipLbl) {
        _tipLbl = [ControllerHelper autoFitLabel];
        _tipLbl.textColor = AppBlack;
        _tipLbl.text = @"用户还没有动态";
        _tipLbl.font = SystemFont(14);
        _tipLbl.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_tipLbl];
    }
    return _tipLbl;
}

-(UIImageView *)rightAccess{
    if (!_rightAccess) {
        _rightAccess = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_arrow_forward"]];
        _rightAccess.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_rightAccess];
    }
    return _rightAccess;
}

-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc]init];
        _titleLbl.font = SystemFont(16);
        _titleLbl.text = LS(@"动态");
        [self addSubview:_titleLbl];
    }
    return _titleLbl;
}

-(UIView *)line{
    if (!_line) {
        _line = [[UIView alloc]init];
        _line.backgroundColor = AppLineColor;
        [self addSubview:_line];
    }
    return _line;
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        UserHomeDynamicCellLayout *layout = [[UserHomeDynamicCellLayout alloc]init];
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[ImageCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
        _collectionView.backgroundColor = [UIColor clearColor];
        [self addSubview:_collectionView];
    }
    return _collectionView;
}

#pragma mark -
#pragma mark collection view

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return MIN(self.dynamicImages.count, 4);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    [cell.imageView setImageName:self.dynamicImages[indexPath.item] placeholder:nil];
    
    return cell;
}

@end
